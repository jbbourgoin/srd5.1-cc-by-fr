
# Origines


## Traits raciaux

La description de chaque origine comprend les traits raciaux qui sont
communs aux membres de cette origine. Les entrées suivantes figurent parmi
les traits de la plupart des origines.


### Augmentation de la valeur de caractéristiques

Chaque origine augmente une ou plusieurs des valeurs de caractéristiques du
personnage.



### Âge

L'entrée âge indique l'âge auquel un membre de l'origine est considéré
comme un adulte, ainsi que l'espérance de vie de l'origine. Ces
informations peuvent vous aider à déterminer l'âge de votre personnage
au début du jeu. Vous pouvez choisir n'importe quel âge pour votre
personnage, ce qui peut expliquer certaines de vos valeurs de
caractéristiques. Par exemple, si vous jouez un personnage jeune ou très
vieux, votre âge pourrait expliquer un score de Force ou de Constitution
particulièrement bas, tandis qu'un âge avancé pourrait expliquer une
Intelligence ou une Sagesse élevée.



### Alignement

La plupart des origines ont des tendances vers certains alignements,
décrits dans cette entrée. Ces tendances ne sont pas contraignantes pour
les personnages joueurs, mais le fait de se demander pourquoi votre nain
est chaotique, par exemple, au mépris de la société naine légale, peut
vous aider à mieux définir votre personnage.



### Taille

Les personnages de la plupart des origines sont de taille moyenne, une
catégorie de taille qui comprend les créatures mesurant entre 1,5 et 2,5
mètres. Les membres de quelques origines sont Petits (entre 2 et 4 pieds de
haut), ce qui signifie que certaines règles du jeu les affectent
différemment. La plus importante de ces règles est que les personnages
de Petit gabarit ont du mal à manier les armes lourdes, comme expliqué
dans la section \"*Équipement*\".



### Vitesse

Votre vitesse détermine la distance que vous pouvez parcourir lorsque
vous voyagez et lorsque vous combattez.



### Langues

En vertu de votre origine, votre personnage peut parler, lire et écrire
certaines langues.



### sous-origines

Certaines origines ont des sous-origines. Les membres d'une sous-origine ont les
traits de l'origine parente en plus des traits spécifiés pour leur
sous-origine. Les relations entre les sous-origines varient considérablement
d'une origine à l'autre et d'un monde à l'autre.




## Drakonide


### Traits du drakonide

Votre héritage draconique se manifeste par une variété de traits que
vous partagez avec les autres drakéens.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Force augmente de 2, et votre score de Charisme augmente de 1.

***Âge.*** Les jeunes drakonides grandissent rapidement. Ils marchent
quelques heures après l'éclosion, atteignent la taille et le
développement d'un enfant humain de 10 ans à l'âge de 3 ans, et
deviennent adultes à 15 ans. Ils vivent jusqu'à 80 ans environ.

***Alignement.*** Les drakonides ont tendance à aller vers les extrêmes,
choisissant consciemment un camp ou l'autre dans la guerre cosmique
entre le bien et le mal. La plupart des drakonides sont bons, mais ceux
qui se rangent du côté du mal peuvent être de terribles méchants.

***Taille.*** Les drakonides sont plus grands et plus lourds que les
humains, mesurant plus d'un mètre quatre-vingt et pesant en moyenne
près de 250 livres. Votre taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

  --------------------------------------------------
  Dragon   Type de      Soufflarme
           dommage      
  -------- ------------ ----------------------------
  Noir     Acide        5 par ligne de 30 pieds
                        (sauvegarde Dex.)

  Bleu     La foudre    5 par ligne de 30 pieds
                        (sauvegarde Dex.)

  Laiton   Feu          5 par ligne de 30 pieds
                        (sauvegarde Dex.)

  Bronze   La foudre    5 par ligne de 30 pieds
                        (sauvegarde Dex.)

  Cuivre   Acide        5 par ligne de 30 pieds
                        (sauvegarde Dex.)

  Or       Feu          Cône de 15 pieds (sauvegarde
                        Dex.)

  Vert     Empoisonné   cône de 15 pieds (sauvegarde
                        Con.)

  Rouge    Feu          Cône de 15 pieds (sauvegarde
                        Dex.)

  Argent   Froid        cône de 15 pieds (sauvegarde
                        Con.)

  Blanc    Froid        cône de 15 pieds (sauvegarde
                        Con.)
  --------------------------------------------------

  : Ascendance draconique

***Ascendance draconique.*** Vous avez une ascendance draconique.
Choisissez un type de dragon dans le tableau d'Ascendance draconique.
Votre arme à souffle et votre résistance aux dégâts sont déterminées par
le type de dragon, comme indiqué dans la table.

***Soufflarme.*** Vous pouvez utiliser votre action pour expirer une
énergie destructrice. Votre ascendance draconique détermine la taille,
la forme et le type de dégâts de l'exhalaison.

Lorsque vous utilisez votre arme de Soufflarme, chaque créature dans la
zone de l'expiration doit effectuer un jet de sauvegarde, dont le type
est déterminé par votre Ascendance draconique. Le résultat de ce jet de
sauvegarde est égal à 8 + votre modificateur de Constitution + votre
bonus de maîtrise. Une créature subit 2d6 points de dégâts en cas
d'échec, et la moitié en cas de réussite. Les dégâts augmentent à 3d6
au 6ème niveau, 4d6 au 11ème niveau, et 5d6 au 16ème niveau.

Après avoir utilisé votre arme de souffle, vous ne pouvez plus
l'utiliser avant d'avoir effectué un repos court ou long.

***Résistance aux dégâts.*** Vous avez une résistance au type de dégâts
associé à votre ascendance draconique.

***Langues.*** Vous pouvez parler, lire et écrire le commun et le
draconique. Le draconique est considéré comme l'une des langues les
plus anciennes et est souvent utilisé dans l'étude de la magie. La
langue semble dure pour la plupart des autres créatures et comprend de
nombreuses consonnes dures et sibilantes.




## Nain


### Traits nains

Votre personnage nain possède un assortiment de capacités innées, qui
font partie intégrante de la nature naine.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Constitution augmente de 2.

***Âge.*** Les nains mûrissent au même rythme que les humains, mais ils
sont considérés comme jeunes jusqu'à ce qu'ils atteignent l'âge de 50
ans. En moyenne, ils vivent environ 350 ans.

***Alignement.*** La plupart des nains sont de droite, et croient
fermement aux bienfaits d'une société bien ordonnée. Ils tendent
également vers le bien, avec un sens aigu du fair-play et la conviction
que chacun mérite de partager les bénéfices d'un ordre juste.

***Taille.*** Les nains mesurent entre 1,5 et 2 mètres et pèsent en
moyenne 150 livres. Votre taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 25 pieds. Votre
vitesse n'est pas réduite par le port d'une armure lourde.

***Vision dans le noir.*** Habitué à la vie sous terre, vous avez une
vision supérieure dans l'obscurité et la pénombre. Vous pouvez voir
dans une lumière faible à moins de 60 pieds de vous comme si c'était
une lumière vive, et dans l'obscurité comme si c'était une lumière
faible. Vous ne pouvez pas discerner les couleurs dans l'obscurité,
seulement les nuances de gris.

***Résiliente naine.*** Vous avez un avantage aux jets de sauvegarde
contre le poison, et vous avez une résistance aux dégâts du poison.

***Entraînement aux armes naines.*** Vous maîtrisez la hachette, la
hachette, le marteau léger et le marteau de guerre.

***Maîtrise des outils.*** Vous gagnez la maîtrise des outils de
l'artisan de votre choix : outils de forgeron, fournitures de brasseur
ou outils de maçon.

***Connaissance de la pierre.*** Chaque fois que vous effectuez un test
d'Intelligence (Histoire) relatif à l'origine de la pierre, vous êtes
considéré comme compétent dans la compétence Histoire et vous ajoutez le
double de votre bonus de maîtrise à ce test, au lieu de votre bonus de
maîtrise normal.

***Langues.*** Vous pouvez parler, lire et écrire le commun et le nain.
Le nain est plein de consonnes dures et de sons gutturaux, et ces
caractéristiques se retrouvent dans n'importe quelle autre langue
qu'un nain pourrait parler.


#### Nain des collines

En tant que nain des collines, vous avez des sens aiguisés, une profonde
intuition et une remarquable résilience.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Sagesse augmente de 1.

***Robustesse naine.*** Votre maximum de points de vie augmente de 1, et
il augmente de 1 à chaque fois que vous gagnez un niveau.





## Elfe


### Traits des elfes

Votre personnage elfe possède une variété de capacités naturelles,
résultat de milliers d'années de raffinement elfique.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Dextérité augmente de 2.

***L'âge.*** Bien que les elfes atteignent la maturité physique à peu
près au même âge que les humains, la conception elfique de l'âge adulte
va au-delà de la croissance physique pour englober l'expérience du
monde. Un elfe atteint généralement l'âge adulte et un nom d'adulte
vers l'âge de 100 ans et peut vivre jusqu'à 750 ans.

***Alignement.*** Les elfes aiment la liberté, la variété et
l'expression de soi, aussi penchent-ils fortement vers les aspects les
plus doux du chaos. Ils apprécient et protègent la liberté des autres
ainsi que la leur, et ils sont plus souvent bons que mauvais.

***Taille.*** Les elfes mesurent entre moins d'un mètre cinquante et
plus d'un mètre quatre-vingt-dix et sont minces. Votre taille est
moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

***Vision dans le noir.*** Habitué aux forêts crépusculaires et au ciel
nocturne, vous avez une vision supérieure dans l'obscurité et la
pénombre. Vous pouvez voir dans une lumière faible à moins de 60 pieds
de vous comme s'il s'agissait d'une lumière vive, et dans
l'obscurité comme s'il s'agissait d'une lumière faible. Vous ne
pouvez pas discerner les couleurs dans l'obscurité, seulement les
nuances de gris.

***Sens aiguisés.*** Vous avez la maîtrise de la compétence Perception.

***Ascendance féerique.*** Vous avez un avantage aux jets de sauvegarde
contre le charme, et la magie ne peut pas vous endormir.

***Transe.*** Les elfes n'ont pas besoin de dormir. Au lieu de cela,
ils méditent profondément, en restant semi-conscients, pendant 4 heures
par jour. (Le mot commun pour cette méditation est \"transe\".) Pendant
que vous méditez, vous pouvez rêver d'une certaine façon ; ces rêves
sont en fait des exercices mentaux qui sont devenus réflexes avec des
années de pratique. Après s'être reposé de cette façon, vous obtenez le
même bénéfice qu'un humain après 8 heures de sommeil.

***Langues.*** Vous pouvez parler, lire et écrire le commun et l'elfe.
L'elfe est fluide, avec des intonations subtiles et une grammaire
complexe. La littérature elfique est riche et variée, et leurs chansons
et poèmes sont célèbres parmi les autres origines. De nombreux bardes
apprennent leur langue pour pouvoir ajouter des ballades elfiques à leur
répertoire.


#### Haut-elfe

En tant que haut-elfe, vous avez un esprit vif et vous maîtrisez au
moins les bases de la magie. Dans de nombreux univers de jeux
fantastiques, il existe deux types de hauts elfes. L'un est hautain et
reclus, et se croit supérieur aux non-elfes et même aux autres elfes.
L'autre type est plus commun et plus amical, et on le rencontre souvent
parmi les humains et les autres origines.

***Augmentation de la valeur de caractéristiques.*** Votre score
d'Intelligence augmente de 1.

***Entraînement aux armes elfiques.*** Vous maîtrisez l'épée longue,
l'épée courte, l'arc court et l'arc long.

***Tour de magie.*** Vous connaissez un tour de magie de votre choix
dans la liste des sorts de magicien. L'Intelligence est votre
caractéristique d'incantation pour ce dernier.

***Langue supplémentaire.*** Vous pouvez parler, lire et écrire une
langue supplémentaire de votre choix.





## Gnome


### Traits du gnome

Votre personnage gnome a certaines caractéristiques en commun avec tous
les autres gnomes.

***Augmentation de la valeur de caractéristiques.*** Votre score
d'Intelligence augmente de 2.

***Âge.*** Les gnomes mûrissent au même rythme que les humains, et la
plupart d'entre eux sont censés s'installer dans une vie d'adulte
vers l'âge de 40 ans. Ils peuvent vivre de 350 à près de 500 ans.

***Alignement.*** Les gnomes sont le plus souvent de bon aloi. Ceux qui
tendent vers la loi sont les sages, les ingénieurs, les chercheurs, les
érudits, les investigateurs ou les inventeurs. Ceux qui tendent vers le
chaos sont les ménestrels, les filous, les vagabonds ou les bijoutiers
fantaisistes. Les gnomes ont bon cœur, et même les filous parmi eux sont
plus enjoués que vicieux.

***Taille.*** Les gnomes mesurent entre 1 et 2 mètres et pèsent en
moyenne 40 livres. Votre taille est Petit.

***Vitesse.*** Votre vitesse de marche de base est de 25 pieds.

***Vision dans le noir.*** Habitué à la vie sous terre, vous avez une
vision supérieure dans l'obscurité et la pénombre. Vous pouvez voir
dans une lumière faible à moins de 60 pieds de vous comme si c'était
une lumière vive, et dans l'obscurité comme si c'était une lumière
faible. Vous ne pouvez pas discerner les couleurs dans l'obscurité,
seulement les nuances de gris.

***Ruse gnome.*** Vous avez un avantage sur tous les jets de sauvegarde
d'Intelligence, Sagesse et Charisme contre la magie.

***Langues.*** Vous pouvez parler, lire et écrire le commun et le gnome.
La langue gnome, qui utilise l'écriture naine, est réputée pour ses
traités techniques et ses catalogues de connaissances sur le monde
naturel.


#### Gnome des roches

En tant que gnome des roches, vous êtes doté d'une inventivité et
d'une robustesse naturelles supérieures à celles des autres gnomes.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Constitution augmente de 1.

***Lore de l'artificier.*** Chaque fois que vous effectuez un test
d'Intelligence (Histoire) relatif aux objets magiques, aux objets
alchimiques ou aux dispositifs technologiques, vous pouvez ajouter deux
fois votre bonus de maîtrise, au lieu du bonus de maîtrise que vous
appliquez normalement.

***Bricoleur.*** Vous avez la maîtrise des outils d'artisan (outils de
bricoleur). En utilisant ces outils, vous pouvez dépenser 1 heure et 10
gp de matériaux pour construire un Très petite (TP) horloge (AC 5, 1
hp). Le dispositif cesse de fonctionner après 24 heures (à moins que
vous ne dépensiez 1 heure pour le réparer afin qu'il continue de
fonctionner), ou lorsque vous utilisez votre action pour le démonter ; à
ce moment-là, vous pouvez récupérer les matériaux utilisés pour le
créer. Vous pouvez avoir jusqu'à trois dispositifs de ce type actifs à
la fois.

Lorsque vous créez un appareil, choisissez l'une des options suivantes
:

*Jouet mécanique.* Ce jouet est un animal, un monstre ou une personne
mécanique, comme une grenouille, une souris, un oiseau, un dragon ou un
soldat. Lorsqu'il est placé sur le sol, le jouet se déplace de 5 pieds
sur le sol à chacun de vos tours dans une direction aléatoire. Il émet
des bruits appropriés à la créature qu'il représente.

*Démarreur de feu.* L'appareil produit une flamme miniature, que vous
pouvez utiliser pour allumer une bougie, une torche ou un feu de camp.
L'utilisation de l'appareil nécessite votre action.

*Boîte à musique.* Lorsqu'elle est ouverte, cette boîte à musique joue
une seule chanson à un volume modéré. La boîte s'arrête lorsqu'elle
atteint la fin de la chanson ou lorsqu'elle est fermée.





## Demi-elfe


### Traits des demi-elfes

Votre personnage demi-elfe a des qualités communes avec les elfes et
d'autres qui sont uniques aux demi-elfes.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Charisme augmente de 2, et deux autres scores de capacité de votre choix
augmentent de 1.

***Âge.*** Les demi-elfes grandissent au même rythme que les humains et
atteignent l'âge adulte vers 20 ans. Ils vivent cependant beaucoup plus
longtemps que les humains, souvent plus de 180 ans.

***Alignement.*** Les demi-elfes partagent la tendance chaotique de leur
héritage elfique. Ils apprécient à la fois la liberté personnelle et
l'expression créative, et ne manifestent ni amour des chefs ni désir
d'être suivis. Ils s'irritent des règles, n'apprécient pas les
exigences des autres et se montrent parfois peu fiables, ou du moins
imprévisibles.

***Taille.*** Les demi-elfes font à peu près la même taille que les
humains, entre 1,5 et 2,5 mètres. Votre taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

***Vision dans le noir.*** Grâce à votre sang d'elfe, vous avez une
vision supérieure dans l'obscurité et la pénombre. Vous pouvez voir
dans une lumière faible à moins de 60 pieds de vous comme s'il
s'agissait d'une lumière vive, et dans l'obscurité comme s'il
s'agissait d'une lumière faible. Vous ne pouvez pas discerner les
couleurs dans l'obscurité, seulement les nuances de gris.

***Ascendance féerique.*** Vous avez un avantage aux jets de sauvegarde
contre le charme, et la magie ne peut pas vous endormir.

***Polyvalence des compétences.*** Vous gagnez la maîtrise de deux
compétences de votre choix.

***Langues.*** Vous pouvez parler, lire et écrire le commun, l'elfe et
une autre langue de votre choix.




## Demi-orque


### Traits des demi-orques

Votre personnage demi-orc possède certains traits de caractère qui
découlent de votre ascendance orc.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Force augmente de 2, et votre score de Constitution augmente de 1.

***Âge.*** Les demi-orques mûrissent un peu plus vite que les humains,
atteignant l'âge adulte vers 14 ans. Ils vieillissent sensiblement plus
vite et vivent rarement plus de 75 ans.

***Alignement.*** Les demi-orques ont hérité de leurs parents orcs une
tendance au chaos et ne sont pas très enclins au bien. Les demi-orques
élevés parmi les orcs et désireux de vivre leur vie parmi eux sont
généralement mauvais.

***Taille.*** Les demi-orques sont un peu plus grands et plus corpulents
que les humains, et leur portée varie de 1,5 à plus de 1,8 mètre. Votre
taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

***Vision dans le noir.*** Grâce à votre sang d'Orc, vous avez une
vision supérieure dans l'obscurité et la pénombre. Vous pouvez voir
dans une lumière faible à moins de 60 pieds de vous comme si c'était
une lumière vive, et dans l'obscurité comme si c'était une lumière
faible. Vous ne pouvez pas discerner les couleurs dans l'obscurité,
seulement les nuances de gris.

***Menaçante.*** Vous gagnez la maîtrise de la compétence Intimidation.

***Endurance implacable.*** Lorsque vous êtes rapetissé à 0 points de
vie mais pas tué tout de suite, vous pouvez tomber à 1 point de vie à la
place. Vous ne pouvez pas utiliser cette caractéristique à nouveau avant
d'avoir terminé un long repos.

***Attaques sauvages.*** Lorsque vous obtenez un coup critique avec une
attaque avec une arme de mêlée, vous pouvez lancer un des dés de dégâts
de l'arme une fois supplémentaire et l'ajouter aux dégâts
supplémentaires du coup critique.

***Langues.*** Vous pouvez parler, lire et écrire le commun et l'orc.
L'Orc est une langue rude, grinçante, avec des consonnes dures. Elle
n'a pas d'écriture propre mais s'écrit avec l'écriture naine.




## Halfelin


### Traits des halfelins

Votre personnage halfelin a un certain nombre de traits communs avec
tous les autres halflings.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Dextérité augmente de 2.

***Âge.*** Un halfelin atteint l'âge adulte à 20 ans et vit
généralement jusqu'au milieu de son deuxième siècle.

***Alignement.*** La plupart des halfelins sont de bon droit. En règle
générale, ils ont bon cœur et sont gentils, détestent voir les autres
souffrir et ne tolèrent pas l'oppression. Ils sont également très
ordonnés et traditionnels, s'appuyant fortement sur le soutien de leur
communauté et le confort de leurs anciennes habitudes.

***Taille.*** Les halfelins mesurent en moyenne 1 mètre et pèsent
environ 40 livres. Votre taille est Petit.

***Vitesse.*** Votre vitesse de marche de base est de 25 pieds.

***Chanceux.*** Lorsque vous obtenez un 1 sur le d20 pour un jet
d'attaque, un test de capacité ou un jet de sauvegarde, vous pouvez
relancer le dé et devez utiliser le nouveau jet.

***Brave.*** Vous avez l'avantage sur les jets de sauvegarde contre
l'effroi.

***Agilité halfeline.*** Vous pouvez vous déplacer dans l'espace de
toute créature d'une taille supérieure à la vôtre.

***Langues.*** Vous pouvez parler, lire et écrire le commun et
l'halfelin. La langue halfelin n'est pas secrète, mais les halflings
sont peu enclins à la partager avec d'autres. Ils écrivent très peu,
donc ils n'ont pas une littérature riche. Leur tradition orale,
cependant, est très forte. Presque tous les halfelins parlent le commun
pour converser avec les gens sur les terres qu'ils habitent ou qu'ils
traversent.


#### Pied-léger

En tant que halfelin au pied-léger, vous pouvez facilement vous cacher,
même en utilisant d'autres personnes comme abri. Vous avez tendance à
être affable et à bien vous entendre avec les autres.

Les pieds-légers sont plus enclins à l'errance que les autres
halfelings, et s'installent souvent aux côtés d'autres origines ou
adoptent une vie nomade.

***Augmentation de la valeur de caractéristiques.*** Votre score de
Charisme augmente de 1.

***Discrétion naturelle.*** Vous pouvez tenter de vous cacher même
lorsque vous n'êtes masqué que par une créature d'au moins une taille
supérieure à la vôtre.





## Humain


### Traits humains

Il est difficile de faire des généralisations sur les humains, mais
votre personnage humain présente ces caractéristiques.

***Augmentation de la valeur des caractéristiques.*** Vos scores de
capacité augmentent chacun de 1.

***Âge.*** Les humains atteignent l'âge adulte à la fin de leur
adolescence et vivent moins d'un siècle.

***Alignement.*** Les humains n'ont pas d'alignement particulier. On
trouve parmi eux le meilleur et le pire.

***Taille.*** Les humains sont de taille et de corpulence très
variables, allant d'à peine 1,5 mètre à plus d'1,80 mètre. Quelle que
soit votre position dans cette gamme, votre taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

***Langues.*** Vous pouvez parler, lire et écrire le commun et une autre
langue de votre choix. Les humains apprennent généralement les langues
des autres peuples qu'ils côtoient, y compris les dialectes obscurs.
Ils aiment parsemer leur discours de mots empruntés à d'autres langues
: Les jurons orcs, les expressions musicales elfes, les phrases
militaires naines, et ainsi de suite.




## Tiefelin


### Traits de tieffelin

Les tieffelins partagent certains traits raciaux en raison de leur
ascendance infernale.

***Augmentation de la valeur de caractéristiques.*** Votre score
d'Intelligence augmente de 1, et votre score de Charisme augmente de 2.

***Âge.*** Les tieffelins ont le même âge que les humains mais vivent
quelques années de plus.

***Alignement.*** Les tieffelins n'ont peut-être pas une tendance innée
au mal, mais beaucoup d'entre eux y finissent. Qu'ils soient mauvais
ou non, leur nature indépendante pousse de nombreux tieffelins à
s'aligner sur le chao.

***Taille.*** Les tieffelins ont à peu près la même taille et la même
carrure que les humains. Votre taille est moyenne.

***Vitesse.*** Votre vitesse de marche de base est de 30 pieds.

***Vision dans le noir.*** Grâce à votre héritage infernal, vous avez
une vision supérieure dans l'obscurité et la pénombre. Vous pouvez voir
dans une lumière faible à moins de 60 pieds de vous comme s'il
s'agissait d'une lumière vive, et dans l'obscurité comme s'il
s'agissait d'une lumière faible. Vous ne pouvez pas discerner les
couleurs dans l'obscurité, seulement les nuances de gris.

***Résistance infernale.*** Vous avez une résistance aux dégâts du feu.

***Ascendance infernale.*** Vous connaissez le tour de magie de la
*thaumaturgie*. Lorsque vous atteignez le 3e niveau,
vous pouvez lancer le sort Représailles *infernales*
comme un sort de 2e niveau une fois avec ce trait et regagner la
capacité de le faire lorsque vous terminez un repos long. Lorsque vous
atteignez le 5e niveau, vous pouvez lancer le sort
*ténèbres* une fois avec ce trait et regagner la capacité
de le faire lorsque vous terminez un long repos. Le Charisme est votre
caractéristique d'incantation pour ces sorts.

***Langues.*** Vous pouvez parler, lire et écrire le commun et
l'infernal.


