#!/bin/bash -

preamb="00-srdfr-preambule.md"
declare -a documents=( "01-srdfr-origines.md" "02-srdfr-classes.md" "03-srdfr-progression.md" "04-srdfr-equipment.md" "05-srdfr-dons.md" "06-srdfr-caracteristiques.md" "07-srdfr-aventures.md" "08-srdfr-combat.md" "09-srdfr-magie.md" "10-srdfr-objets-magiques.md" "11-srdfr-creatures.md" "12-srdfr-univers.md")

declare -a formats=("pdf" "html" "epub" "odt")

for i in "${formats[@]}"
do
    if [ "$i" = "pdf" ]
    then
	pandoc --pdf-engine xelatex -o docs_uniques/srd51-ccby-fr.$i $preamb ${documents[@]}
    else
	pandoc -o docs_uniques/srd51-ccby-fr.$i $preamb ${documents[@]}
    fi
done

for i in "${documents[@]}"
do
    for y in "${formats[@]}"
    do
	if [ "$y" = "pdf" ]
	then
	    pandoc --pdf-engine xelatex -o docs_multis/$i.$y $preamb $i
	else
	    pandoc -o docs_multis/$i.$y $preamb $i
	fi
    done
done
