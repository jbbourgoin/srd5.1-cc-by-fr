---
title: Document de référence du système 5.1
documentclass: book
fontsize: 12pt
geometry: "left=3cm,right=2cm,top=2cm,bottom=2cm"
output: pdf_document
toc-title: "Table des Matières"
header-includes:
   - \usepackage{fontspec}
   - \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
   - \usepackage{libertine}
   - \addfontfeatures{RawFeature=+dlig;+pnum;+onum;+hlig;+liga;+clig;+alt}
   - \usepackage{setspace}
#   - \linespread{1.5}
toc: true
---

# Informations légales


> Ce travail comprend des éléments tirés du Document de référence du
> système 5.1 (\"SRD 5.1\") de Wizards of the Coast LLC, disponible sur
> https://dnd.wizards.com/resources/systems-reference-document. Le SRD
> 5.1 est sous licence Creative Commons Attribution 4.0 International
> License disponible sur
> https://creativecommons.org/licenses/by/4.0/legalcode.
