
# Objets magiques

Les objets magiques sont glanés dans les trésors des monstres conquis ou
découverts dans des coffres perdus depuis longtemps. Ces objets
confèrent des capacités qu'un personnage pourrait rarement avoir
autrement, ou ils complètent les capacités de leur propriétaire de façon
merveilleuse.


## Lié

Certains objets magiques nécessitent qu'une créature forme un lien avec
eux avant que leurs propriétés magiques puissent être utilisées. Ce lien
est appelé syntonisation, et certains objets ont un prérequis pour cela.
Si le prérequis est une classe, une créature doit être membre de cette
classe pour s'accorder avec l'objet. (Si la classe est une classe
d'incantation, un monstre est qualifié s'il possède des emplacements
de sorts et utilise la liste de sorts de cette classe). Si le prérequis
est d'être un lanceur de sorts, une créature est qualifiée si elle peut
lancer au moins un sort en utilisant ses traits ou caractéristiques,
sans utiliser d'objet magique ou autre.

Si elle ne s'accorde pas avec un objet qui nécessite un accord, une
créature ne bénéficie que de ses avantages non magiques, sauf si sa
description indique le contraire. Par exemple, un bouclier magique qui
nécessite d'être accordé offre les avantages d'un bouclier normal à
une créature qui n'y est pas liée, mais aucune de ses propriétés
magiques.

Pour s'accorder à un objet, une créature doit passer un repos court en
se concentrant uniquement sur cet objet tout en étant en contact
physique avec lui (il ne peut s'agir du même repos court utilisé pour
apprendre les propriétés de l'objet). Cette concentration peut prendre
la forme d'un entraînement à l'arme (pour une arme), d'une méditation
(pour un objet merveilleux) ou de toute autre activité appropriée. Si le
repos court est interrompu, la tentative d'harmonisation échoue. Sinon,
à la fin du court repos, la créature acquiert une compréhension
intuitive de la manière d'activer les propriétés magiques de l'objet,
y compris les mots de commande nécessaires.

Un objet ne peut être lié qu'à une seule créature à la fois, et une
créature ne peut être liée à plus de trois objets magiques à la fois.
Toute tentative d'accorder un quatrième objet échoue ; la créature doit
d'abord mettre fin à son accord avec un objet. De plus, une créature ne
peut pas s'accorder avec plus d'une copie d'un objet. Par exemple,
une créature ne peut pas s'accorder avec plus d'un anneau *de
protection* à la fois.

L'accord d'une créature avec un objet prend fin si la créature ne
remplit plus les prérequis de l'accord, si l'objet a été éloigné de
plus de 100 pieds pendant au moins 24 heures, si la créature meurt, ou
si une autre créature s'accorde avec l'objet. Une créature peut
également mettre volontairement fin à l'harmonisation en passant un
autre court repos concentré sur l'objet, sauf si l'objet est maudit.



## Porter et manier des objets

Utiliser les propriétés d'un objet magique peut signifier le porter ou
le manier. Un objet magique destiné à être porté doit être enfilé de la
manière prévue : les bottes vont aux pieds, les gants aux mains, les
chapeaux et les casques à la tête, et les bagues aux doigts. Une armure
magique doit être enfilée, un bouclier attaché au bras, une cape
attachée aux épaules. Une arme doit être tenue.

Dans la plupart des cas, un objet magique destiné à être porté peut
convenir à une créature, quelle que soit sa taille ou sa corpulence. De
nombreux vêtements magiques sont conçus pour être facilement ajustables,
ou bien ils s'ajustent magiquement à la personne qui les porte. Il
existe de rares exceptions. Si l'histoire suggère une bonne raison pour
qu'un objet ne s'adapte qu'aux créatures d'une certaine taille ou
d'une certaine forme, vous pouvez décider qu'il ne s'ajuste pas. Par
exemple, une armure fabriquée par les Drows pourrait ne convenir qu'aux
elfes. Les nains pourraient fabriquer des objets utilisables uniquement
par des personnes de taille et de forme naines.

Lorsqu'un non-humanoïde essaie de porter un objet, utilisez votre
jugement pour savoir si l'objet fonctionne comme prévu. Un anneau placé
sur un tentacule peut fonctionner, mais un serpentfolk avec une queue en
forme de serpent à la place des jambes ne peut pas porter de bottes.


### Plusieurs articles du même type

Utilisez votre bon sens pour déterminer si vous pouvez porter plus d'un
objet magique d'un type donné. Un personnage ne peut normalement pas
porter plus d'une paire de chaussures, une paire de gants ou de
gantelets, une paire de bracelets, une armure, un couvre-chef et une
cape. Vous pouvez faire des exceptions ; un personnage pourrait être
capable de porter un cercle sous un casque, par exemple, ou de
superposer deux capes.



### Articles appariés

Les objets qui vont par paire - comme les bottes, les bracelets, les
gantelets et les gants - ne confèrent leurs avantages que si les deux
éléments de la paire sont portés. Par exemple, un personnage portant une
*botte de marche et de* sept
*lieues* sur un pied et une *botte
elfique* sur l'autre ne bénéficie d'aucun des
deux.




## Activation d'un élément

Pour activer certains objets magiques, l'utilisateur doit faire quelque
chose de spécial, comme tenir l'objet et prononcer un mot
d'injonction. La description de chaque catégorie d'objet ou de chaque
objet individuel détaille comment un objet est activé. Certains objets
utilisent les règles suivantes pour leur activation.

Si un objet nécessite une action pour être activé, cette action n'est
pas une fonction de l'action Utiliser un objet, donc une
caractéristique comme les Gestes précis du roublard ne peut pas être
utilisée pour activer l'objet.


### Injonction

Un mot de commande est un mot ou une phrase qui doit être prononcé pour
qu'un objet fonctionne. Un objet magique qui nécessite un mot de
commande ne peut pas être activé dans une zone où le son est empêché,
comme dans la zone du sort Silence.



### Consommables

Certains objets s'épuisent lorsqu'ils sont activés. Une potion ou un
élixir doit être avalé, ou une huile appliquée sur le corps. L'écriture
disparaît d'un parchemin lorsqu'il est lu. Une fois utilisé, un objet
consommable perd sa magie.



### Sorts

Certains objets magiques permettent à l'utilisateur de lancer un sort à
partir de l'objet. Le sort est lancé au niveau de sort le plus bas
possible, ne dépense aucun des emplacements de sort de l'utilisateur et
ne nécessite aucun composant, sauf si la description de l'objet indique
le contraire. Le sort utilise son temps d'incantation, sa portée et sa
durée normaux, et l'utilisateur de l'objet doit se concentrer si le
sort l'exige. De nombreux objets, tels que les
potions, permettent d'éviter
l'incantation d'un sort et confèrent les effets du sort, avec leur
durée habituelle. Certains objets font exception à ces règles, en
modifiant le temps d'incantation, la durée ou d'autres éléments d'un
sort.

Un objet magique, comme certains bâtons, peut vous obliger à utiliser
votre propre caractéristique d'incantation lorsque vous lancez un sort
à partir de l'objet. Si vous avez plus d'une caractéristique
d'incantation, vous choisissez laquelle utiliser avec l'objet. Si vous
n'avez pas de capacité d'incantation (vous êtes peut-être un
roublard avec la caractéristique Utilisation
d'objets magiques, votre modificateur de capacité
d'incantation est de +0 pour l'objet, et votre bonus de maîtrise
s'applique.



### Charges

Certains objets magiques ont des charges qui doivent être dépensées pour
activer leurs propriétés. Le nombre de charges restantes d'un objet est
révélé lorsqu'un sort d'identification est lancé sur lui, ainsi que
lorsqu'une créature s'accorde avec lui. De plus, lorsqu'un objet
regagne des charges, la créature qui y est liée apprend combien de
charges il a regagné.




## Descriptions des objets magiques


### Armure d'adamantium

*Armure (moyenne ou lourde, mais pas en cuir), peu commune*

Cette armure est renforcée d'adamantine, l'une des substances les plus
dures qui existent. Tant que vous la portez, tout coup critique contre
vous devient un coup normal.



### Munitions, +1, +2, ou +3

*Arme (toutes munitions), peu commune (+1), rare (+2), ou très rare
(+3)*

Vous bénéficiez d'un bonus aux jets d'attaque et de dégâts effectués
avec cette pièce de munitions magiques. Ce bonus est déterminé par la
rareté de la munition. Une fois qu'elle a touché une cible, la munition
n'est plus magique.



### Amulette de santé

*Objet merveilleux, rare (à accorder)*

Votre score de Constitution est de 19 lorsque vous portez cette
amulette. Elle n'a aucun effet sur vous si votre Constitution est déjà
de 19 ou plus.



### Amulette d'anti-télédétection et de localisation

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez cette amulette, vous êtes protégé de la magie de
divination. Vous ne pouvez pas être ciblé par une telle magie ou perçu
par des capteurs de scrutation magiques.



### Amulette des plans

*Objet merveilleux, très rare (nécessite d'être accordé).*

Lorsque vous portez cette amulette, vous pouvez utiliser une action pour
nommer un lieu qui vous est familier sur un autre plan d'existence.
Faites ensuite un test d'Intelligence DC 15. En cas de réussite, vous
lancez le sort de *changement de plan*. En cas d'échec,
vous et chaque créature et objet dans un rayon de 15 pieds de vous
voyagez vers une destination aléatoire. Lancez un d100. Sur un résultat
de 1 à 60, vous voyagez vers un endroit aléatoire du plan que vous avez
nommé.

Sur un 61-100, vous voyagez vers un plan d'existence déterminé
aléatoirement.



### Bouclier animé

*Armure (Bouclier), très rare (nécessite d'être accordé)*

Lorsque vous tenez ce bouclier, vous pouvez prononcer son mot
d'Injonction comme action bonus pour le faire s'animer. Le Bouclier
bondit dans les airs et plane dans votre espace pour vous protéger comme
si vous le brandissiez, laissant vos mains libres. Le bouclier reste
animé pendant 1 minute, jusqu'à ce que vous utilisiez une action bonus
pour mettre fin à cet effet, ou jusqu'à ce que vous soyez frappé
d'incapacité ou mouriez, auquel cas le bouclier tombe au sol ou dans
votre main si vous en avez une de libre.



### Appareils du Crabe

*Objet merveilleux, légendaire*

Cet objet se présente d'abord comme un Grand baril de fer scellé pesant
500 livres. Le baril a un loquet caché, qui peut être trouvé avec un
test d'Intelligence (Investigation) DC 20 réussi. La libération du
loquet déverrouille une trappe à une extrémité du tonneau, permettant à
deux créatures de taille moyenne ou plus petites de ramper à
l'intérieur. Dix leviers sont alignés à l'autre extrémité, chacun en
position neutre, pouvant être déplacé vers le haut ou le bas.

Lorsque certains leviers sont utilisés, l'appareil se transforme pour
ressembler à un homard géant.

L'appareil du Crabe est un objet Grand avec les statistiques suivantes
:

**Classe d'armure :** 20

**Points de vie :** 200

**Vitesse :** 30 pieds, nager 30 pieds (ou 0 pied pour les deux si les
jambes et la queue ne sont pas étendues)

**Immunités aux dommages :** poison, psychique

Pour être utilisé comme véhicule, l'appareil nécessite un pilote.
Lorsque la trappe de l'appareil est fermée, le compartiment est étanche
à l'air et à l'eau. Le compartiment contient suffisamment d'air pour
10 heures de respiration, divisé par le nombre de créatures respirant à
l'intérieur.

L'appareil flotte sur l'eau. Il peut également aller sous l'eau
jusqu'à une profondeur de 900 pieds. En dessous, le véhicule subit 2d6
dégâts contondants par minute dus à la pression.

Une créature dans le compartiment peut utiliser une action pour déplacer
jusqu'à deux des leviers de l'appareil vers le haut ou vers le bas.
Après chaque utilisation, un levier revient à sa position neutre. Chaque
levier, de gauche à droite, fonctionne comme indiqué dans l'image
ci-dessous.

  ------------------------------------------------------------------------
   Levier  Up                              Duvet
  -------- ------------------------------- -------------------------------
     1     Les pattes et la queue          Les pattes et la queue se
           s'étendent, permettant à       rétractent, réduisant la
           l'appareil de marcher et de    vitesse de l'appareil à 0 et
           nager.                          le rendant incapable de
                                           bénéficier des bonus de
                                           vitesse.

     2     Le volet de la fenêtre avant    Le volet de la fenêtre avant se
           s'ouvre.                       ferme.

     3     Les volets des fenêtres         Les volets des fenêtres
           latérales sont ouverts (deux    latérales se ferment (deux par
           par côté).                      côté).

     4     Deux griffes s'étendent sur    Les griffes se rétractent.
           les faces avant de l'appareil. 

     5     Chaque griffe étendue effectue  Chaque griffe étendue effectue
           l'attaque de corps-à-corps     l'attaque de corps-à-corps
           suivante : +8 pour toucher,     suivante : +8 pour toucher,
           portée de 1,5 m, une cible.     portée de 1,5 m, une cible.
           Touché : 7 (2d6) points de      Touché : La cible est agrippée
           dégâts contondants.             (fuite DC 15).

     6     L'appareil marche ou nage en   L'appareil marche ou nage à
           avant.                          reculons.

     7     L'appareil tourne de 90 degrés L'appareil tourne de 90 degrés
           à gauche.                       à droite.

     8     Les fixations en forme d'yeux  La lumière s'éteint.
           émettent une lumière vive dans  
           un rayon de 30 pieds et une     
           lumière chétive sur 30 pieds    
           supplémentaires.                

     9     L'appareil s'enfonce jusqu'à L'appareil s'élève jusqu'à
           6 mètres dans le liquide.       20 pieds dans le liquide.

     10    Le hayon arrière se             La trappe arrière se ferme et
           déverrouille et s'ouvre.       s'étanche.
  ------------------------------------------------------------------------

  : Appareil des leviers du Crabe



### Armure, +1, +2, ou +3

*Armure (légère, moyenne ou lourde), rare (+1), très rare (+2) ou
légendaire (+3).*

Vous bénéficiez d'un bonus à la CA lorsque vous portez cette armure. Ce
bonus est déterminé par sa rareté.



### Armure d'invulnérabilité

*Armure (harnois), légendaire (nécessite une harmonisation)*

Vous avez une résistance aux dégâts non-magiques lorsque vous portez
cette armure. De plus, vous pouvez utiliser une action pour vous rendre
immunisé aux dégâts non magiques pendant 10 minutes ou jusqu'à ce que
vous ne portiez plus cette armure. Une fois que cette action spéciale
est utilisée, elle ne peut plus être utilisée jusqu'à l'aube suivante.



### Armure de résistance

*Armure (légère, moyenne ou lourde), rare (nécessite d'être lié).*

Vous avez une résistance à un type de dégâts lorsque vous portez cette
armure. Le MJ choisit le type ou le détermine au hasard parmi les
options ci-dessous.

  ------------------
   d10  Type de
        dommage
  ----- ------------
    1   Acide

    2   Froid

    3   Feu

    4   Force

    5   La foudre

    6   Nécrotique

    7   Empoisonné

    8   Psychique

    9   Radiant

   10   Tonnerre
  ------------------



### Armure de vulnérabilité

*Armure (harnois), rare (nécessite une harmonisation)*

Lorsque vous portez cette armure, vous avez une résistance à l'un des
types de dégâts suivants : contondant, perforant ou tranchant. Le MJ
choisit le type ou le détermine au hasard.

***Malédiction.*** Cette armure est maudite, un fait qui n'est révélé
que lorsqu'un sort d'*identification* est lancé sur
l'armure ou que vous vous y accordez. S'accorder à l'armure vous
maudit jusqu'à ce que vous soyez ciblé par le sort *supprimer la
malédiction* ou une magie similaire ; retirer l'armure
ne met pas fin à la malédiction. Tant que vous êtes maudit, vous êtes
vulnérable à deux des trois types de dégâts associés à l'armure (pas à
celui auquel elle confère une résistance).



### Bouclier pareprojectiles

*Armure (Bouclier), rare (nécessite d'être syntonisé)*

Vous bénéficiez d'un bonus de +2 à la CA contre les attaques à distance
lorsque vous portez ce bouclier. Ce bonus s'ajoute au bonus normal du
bouclier à la CA. De plus, lorsqu'un attaquant effectue une attaque à
distance contre une cible située à moins de 1,5 m de vous, vous pouvez
utiliser votre réaction pour devenir la cible de l'attaque à la place.



### Flèche ravageuse

*Arme (flèche), très rare*

Une flèche ravageuse est une arme magique destinée à tuer un type
particulier de créature. Certaines sont plus ciblées que d'autres ; par
exemple, il existe à la fois des flèches de la mort du dragon et des
flèches de la mort du dragon bleu. Si une créature appartenant au type,
à l'origine ou au groupe associé à une flèche ravageuse subit des dégâts
de la flèche, elle doit effectuer un jet de sauvegarde de Constitution
DC 17, subissant 6d10 dégâts perforants supplémentaires en cas d'échec,
ou la moitié des dégâts supplémentaires en cas de réussite.

Lorsqu'une flèche ravageuse inflige ses dégâts supplémentaires à une
créature, elle devient une flèche non magique.

Il existe d'autres types de munitions magiques de ce type, comme les
flèches de la mort destinées à une arbalète, bien que les flèches soient
les plus communes.



### Sac de haricots

*Objet merveilleux, rare*

Dans ce lourd sac en tissu se trouvent 3d4 haricots secs. Le sac pèse
1/2 livre plus 1/4 de livre pour chaque haricot qu'il contient.

Si vous jetez le contenu du sac sur le sol, il explose dans un rayon de
3 mètres, en partant des haricots. Chaque créature dans la zone, y
compris vous, doit effectuer un jet de sauvegarde de Dextérité DC 15,
subissant 5d4 dégâts de feu en cas d'échec, ou la moitié des dégâts en
cas de réussite. Le feu enflamme les objets inflammables de la zone qui
ne sont pas portés ou transportés.

Si vous retirez un haricot du sac, le plantez dans de la terre ou du
sable, puis l'arrosez, le haricot produit un effet 1 minute plus tard
depuis le sol où il a été planté. Le MJ peut choisir un effet dans le
tableau suivant, le déterminer au hasard ou en créer un.

  ------------------------------------------------------------------------
   d100   Effet
  ------- ----------------------------------------------------------------
    01    5d4 toadstools poussent. Si une créature mange un champignon
          crapaud, lancez n'importe quel dé. Sur un jet impair, le
          mangeur doit réussir un jet de sauvegarde de Constitution DC 15
          ou subir 5d6 dégâts de poison et s'empoisonner pendant 1 heure.
          Sur un résultat pair, le mangeur gagne 5d6 points de vie
          temporaires pendant 1 heure.

   02-10  Un geyser entre en éruption et projette de l'eau, de la bière,
          du jus de baies, du thé, du vinaigre, du vin ou de l'huile (au
          choix du MJ) à 10 mètres dans les airs pendant 1d12 rounds.

   11-20  Un Sylvanide pousse. Il y a 50% de chances que le Sylvanide soit
          chaotique et qu'il attaque.

   21-30  Une statue de pierre animée et immobile à votre effigie se lève.
          Elle profère des menaces verbales à votre encontre. Si vous la
          quittez et que d'autres s'approchent, elle vous décrit comme
          le plus odieux des méchants et ordonne aux nouveaux venus de
          vous trouver et de vous attaquer. Si vous vous trouvez sur le
          même plan d'existence que la statue, elle sait où vous êtes. La
          statue devient inanimée après 24 heures.

   31-40  Un feu de camp aux flammes bleues jaillit et brûle pendant 24
          heures (ou jusqu'à ce qu'il soit éteint).

   41-50  1d6 + 6 Criards en herbe.

   51-60  1d4 + 8 crapauds rose vif rampent en avant. Chaque fois qu'un
          crapaud est touché, il se transforme en un monstre Grand ou plus
          petit au choix du MJ. Le monstre reste pendant 1 minute, puis
          disparaît dans une bouffée de fumée rose vif.

   61-70  Une bulette affamée s'enfouit et attaque.

   71-80  Un arbre fruitier pousse. Il contient 1d10 + 20 fruits, dont 1d8
          agissent comme des potions magiques déterminées aléatoirement,
          tandis qu'un agit comme un poison ingéré au choix du MJ.
          L'arbre disparaît au bout d'une heure. Les fruits cueillis
          restent, conservant toute la magie pendant 30 jours.

   81-90  Un nid de 1d4 + 3 œufs jaillit. Toute créature qui mange un œuf
          doit effectuer un jet de sauvegarde de Constitution DC 20. En
          cas de sauvegarde réussie, la créature augmente de façon
          permanente son score de capacité le plus faible de 1, en
          choisissant au hasard parmi des scores également faibles. En cas
          d'échec, la créature subit 10d6 dégâts de force dus à une
          explosion magique interne.

   91-99  Une pyramide avec une base de 60 pieds carrés éclate vers le
          haut. A l'intérieur se trouve un sarcophage contenant un
          seigneur momie. La pyramide est considérée comme le repaire du
          seigneur momie, et son sarcophage contient un trésor au choix du
          MJ.

    00    Une tige de haricot géante pousse, atteignant la hauteur choisie
          par le MJ. Le sommet mène là où le MJ le souhaite, par exemple
          vers une vue magnifique, le château d'un géant des nuages ou un
          autre plan d'existence.
  ------------------------------------------------------------------------



### Sac dévoreur

*Objet merveilleux, très rare*

Ce sac ressemble superficiellement à un sac sans fond, mais c'est un
orifice d'alimentation pour une gigantesque créature
extradimensionnelle. Retourner le sac à l'envers ferme l'orifice.

La créature extradimensionnelle attachée au sac peut sentir tout ce qui
est placé à l'intérieur du sac. Toute matière animale ou végétale
placée entièrement dans le sac est dévorée et perdue à jamais.
Lorsqu'une partie d'une créature vivante est placée dans le sac, comme
c'est le cas lorsque quelqu'un y met la main, il y a 50 % de chances
que la créature soit attirée à l'intérieur du sac. Une créature à
l'intérieur du sac peut utiliser son action pour tenter de s'échapper
en réussissant un test de Force DC 15. Une autre créature peut utiliser
son action pour atteindre le sac et en sortir une créature, en
réussissant un test de Force DC 20 (à condition qu'elle ne soit pas
tirée dans le sac en premier). Toute créature qui commence son tour à
l'intérieur du sac est dévorée, son corps détruit.

Les objets inanimés peuvent être stockés dans le sac, qui peut contenir
un pied cube de ces matériaux. Cependant, une fois par jour, le sac
avale les objets qu'il contient et les recrache dans un autre plan
d'existence. Le MJ détermine le moment et le plan.

Si le sac est perforé ou déchiré, il est détruit, et tout ce qu'il
contient est transporté à un endroit aléatoire du plan astral.



### Sac sans fond

*Objet merveilleux, peu commun*

Ce sac a un espace intérieur considérablement plus grand que ses
dimensions extérieures, environ 2 pieds de diamètre à l'embouchure et 4
pieds de profondeur. Le sac peut contenir jusqu'à 500 livres, sans
dépasser un volume de 64 pieds cubes. Le sac pèse 15 livres, quel que
soit son contenu. Récupérer un objet dans le sac nécessite une action.

Si le sac est surchargé, perforé ou déchiré, il se rompt et est détruit,
et son contenu est dispersé dans le plan astral. Si le sac est retourné,
son contenu se répand, indemne, mais le sac doit être remis en état
avant de pouvoir être réutilisé. Les créatures respirant à l'intérieur
du sac peuvent survivre jusqu'à un nombre de minutes égal à 10 divisé
par le nombre de créatures (minimum 1 minute), après quoi elles
commencent à suffoquer.

Placer un sac sans fond dans un espace extradimensionnel créé par un
havresac, un Puis portatif ou un objet similaire détruit instantanément
les deux objets et ouvre un portail vers le plan Astral. Le portail
prend naissance à l'endroit où l'un des objets a été placé dans
l'autre. Toute créature se trouvant à moins de 3 mètres du portail est
aspirée à travers celui-ci vers un endroit aléatoire du plan astral. Le
Portail se referme ensuite. Le portail est à sens unique et ne peut pas
être rouvert.



### Sac à malices

*Objet merveilleux, peu commun*

Ce sac ordinaire, fait de tissu gris, rouille ou beige, semble vide. En
l'atteignant à l'intérieur du sac, on découvre la présence d'un petit
objet flou. Le sac pèse une demi-livre.

Vous pouvez utiliser une action pour tirer l'objet flou du sac et le
lancer jusqu'à 6 mètres. Lorsque l'objet atterrit, il se transforme en
une créature que vous déterminez en lançant un d8 et en consultant la
table qui correspond à la couleur du sac. La créature disparaît à
l'aube suivante ou lorsqu'elle est réduite à 0 points de vie.

La créature est amicale envers vous et vos compagnons, et elle agit à
votre tour. Vous pouvez utiliser une action bonus pour commander la
façon dont la créature se déplace et quelle action elle entreprend à son
prochain tour, ou pour lui donner des ordres généraux, comme attaquer
vos ennemis. En l'absence de tels ordres, la créature agit d'une
manière appropriée à sa nature.

Une fois que trois objets flous ont été retirés du sac, celui-ci ne peut
plus être utilisé jusqu'à l'aube suivante.

  ------------------
   d8  Créature
  ---- -------------
   1   Belette

   2   Rat géant

   3   Blaireau

   4   Sanglier

   5   Panthère

   6   Blaireau
       géant

   7   Loup
       sanguinaire

   8   Élan géant
  ------------------

  : Sac à malices de Gray

  -----------------
   d8  Créature
  ---- ------------
   1   Rat

   2   Chouette

   3   Molosse

   4   Chèvre

   5   Chèvre
       géante

   6   Sanglier
       géant

   7   Lion

   8   Ours brun
  -----------------

  : Sac à malices en rouille

  -----------------
   d8  Créature
  ---- ------------
   1   Chacal

   2   Grand singe

   3   Babouin

   4   Bec de hache

   5   Ours noir

   6   Belette
       géante

   7   Hyène géante

   8   Tigre
  -----------------

  : Sac à malices Tan



### Perle de force

*Objet merveilleux, rare*

Cette petite sphère noire mesure 3/4 de pouce de diamètre et pèse une
once. On trouve généralement 1d4 + 4 perles de force ensemble.

Vous pouvez utiliser une action pour lancer la perle jusqu'à 60 pieds.
La perle explose à l'impact et est détruite. Chaque créature dans un
rayon de 3 mètres de l'endroit où la perle a atterri doit réussir un
jet de sauvegarde de Dextérité DC 15 ou subir 5d4 dégâts de force. Une
sphère de force transparente entoure ensuite la zone pendant 1 minute.
Toute créature ayant échoué au jet de sauvegarde et se trouvant
entièrement dans la zone est piégée dans cette sphère. Les créatures qui
ont réussi leur sauvegarde, ou qui se trouvent partiellement dans la
zone, sont repoussées du centre de la sphère jusqu'à ce qu'elles n'y
soient plus. Seul l'air respirable peut passer à travers la paroi de la
sphère. Aucune attaque ou autre effet ne le peut.

Une créature enfermée peut utiliser son action pour pousser contre la
paroi de la sphère, déplaçant la sphère jusqu'à la moitié de la vitesse
de marche de la créature. La sphère peut être ramassée, et sa magie fait
qu'elle ne pèse qu'un kilo, quel que soit le poids des créatures à
l'intérieur.



### Ceinturon des nains

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez cette ceinture, vous obtenez les avantages suivants
:

-   Votre score de Constitution augmente de 2, jusqu'à un maximum de
    20.
-   Vous avez un avantage sur les tests de Charisme (Persuasion)
    effectués pour interagir avec les nains.

En outre, tant que vous êtes lié à la ceinture, vous avez 50 % de
chances chaque jour à l'aube de vous faire pousser une barbe complète
si vous en êtes capable, ou une barbe visiblement plus épaisse si vous
en avez déjà une.

Si vous n'êtes pas un nain, vous obtenez les avantages supplémentaires
suivants lorsque vous portez la ceinture :

-   Vous avez un avantage aux jets de sauvegarde contre le poison, et
    vous avez une résistance aux dégâts du poison.
-   Vous avez une vision dans le noir jusqu'à une portée de 60 pieds.
-   Vous pouvez parler, lire et écrire le nain.



### Ceinturon de force de géant

*Objet merveilleux, rareté variable (nécessite d'être accordé).*

Lorsque vous portez cette ceinture, votre score de Force se transforme
en un score accordé par la ceinture. Si votre Force est déjà égale ou
supérieure au score de la ceinture, l'objet n'a aucun effet sur vous.

Il existe six variétés de cette ceinture, qui correspondent aux six
types de véritables géants et dont la rareté est fonction de ceux-ci. La
ceinture de force du géant de pierre et la ceinture de force du géant du
givre ont l'air différentes, mais elles ont le même effet.

  ------------------------------------------
  Type                 Force       Rarity
  ------------------ ---------- ------------
  Géant des collines     21         Rare

  Géant de pierre/de     23      Très rare
  givre                         

  Géant du feu           25      Très rare

  Géant des nuages       27      Légendaire

  Géant des tempêtes     29      Légendaire
  ------------------------------------------



### Hache du berserker

*Arme (n'importe quelle hache), rare (nécessite d'être accordé)*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique. De plus, tant que vous êtes lié à cette arme,
votre maximum de points de vie augmente de 1 pour chaque niveau atteint.

***Malédiction.*** Cette hache est maudite, et le fait d'y être lié
étend la malédiction à vous. Tant que vous restez maudit, vous ne voulez
pas vous séparer de la hache, et vous la gardez à portée de main à tout
moment. Vous avez également un désavantage aux jets d'attaque avec des
armes autres que celle-ci, à moins qu'aucun ennemi ne se trouve à moins
de 60 pieds de vous et que vous puissiez le voir ou l'entendre.

Lorsqu'une créature hostile vous endommage alors que la hache est en
votre possession, vous devez réussir un jet de sauvegarde de Sagesse DC
15 ou devenir berserk. Pendant que vous êtes berserk, vous devez
utiliser votre action chaque round pour attaquer la créature la plus
proche de vous avec la hache. Si vous pouvez effectuer des attaques
supplémentaires dans le cadre de l'action Attaquer, vous utilisez ces
attaques supplémentaires, en vous déplaçant pour attaquer la créature la
plus proche après avoir abattu votre cible actuelle. Si vous avez
plusieurs cibles possibles, vous en attaquez une au hasard. Vous êtes
berserk jusqu'à ce que vous commenciez votre tour sans aucune créature
à moins de 60 pieds de vous que vous pouvez voir ou entendre.



### Bottes elfiques

*Objet merveilleux, peu commun*

Lorsque vous portez ces bottes, vos pas ne font aucun bruit, quelle que
soit la surface sur laquelle vous vous déplacez. Vous avez également un
avantage sur les tests de Dextérité (Discrétion) qui reposent sur le
déplacement silencieux.



### Bottes de lévitation

*Objet merveilleux, rare (à accorder)*

Tant que vous portez ces bottes, vous pouvez utiliser une action pour
lancer le sort de *lévitation* sur vous-même à volonté.



### Bottes de rapidité

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez ces bottes, vous pouvez utiliser une action bonus et
faire claquer les talons des bottes ensemble. Si vous faites cela, les
bottes doublent votre vitesse de marche, et toute créature qui effectue
une attaque d'opportunité contre vous a un désavantage au jet
d'attaque. Si vous cliquez à nouveau les talons l'un contre l'autre,
l'effet prend fin.

Lorsque la propriété des bottes a été utilisée pendant un total de 10
minutes, la magie cesse de fonctionner jusqu'à ce que vous ayez terminé
un repos long.



### Bottes de sept lieues

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Tant que vous portez ces bottes, votre vitesse de marche devient 30
pieds, à moins que votre vitesse de marche ne soit supérieure, et votre
vitesse n'est pas réduite si vous êtes encombré ou si vous portez une
armure lourde. De plus, vous pouvez sauter trois fois la distance
normale, bien que vous ne puissiez pas sauter plus loin que ce que votre
mouvement restant vous permettrait.



### Bottes des terres gelées

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Ces bottes en fourrure sont confortables et chaudes au toucher.

Lorsque vous les portez, vous obtenez les avantages suivants :

-   Vous avez une résistance aux dégâts du froid.
-   Vous ignorez les terrains difficiles créés par la glace ou la neige.
-   Vous pouvez tolérer des températures aussi basses que -50 degrés
    Fahrenheit sans aucune protection supplémentaire. Si vous portez des
    vêtements lourds, vous pouvez tolérer des températures aussi basses
    que -100 degrés Fahrenheit.



### Jatte de contrôle des élémentaux de l'eau

*Objet merveilleux, rare*

Tant que cette jatte est remplie d'eau, vous pouvez utiliser une action
pour prononcer le mot de commande de la jatte et invoquer un élémentaire
d'eau, comme si vous aviez lancé le sort invocation
*d'élémentaire*. Le bol ne peut plus être utilisé
de cette façon jusqu'à la prochaine aube.

Le bol fait environ 30 cm de diamètre et la moitié de sa profondeur. Il
pèse 3 livres et contient environ 3 gallons.



### Bracelets d'archerie

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ces bracelets, vous maîtrisez l'arc long et l'arc
court, et vous bénéficiez d'un bonus de +2 aux jets de dégâts des
attaques à distance effectuées avec ces armes.



### Bracelets de défense

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez ces bracelets, vous gagnez un bonus de +2 à la CA si
vous ne portez pas d'armure et n'utilisez pas de bouclier.



### Brasero de contrôle des élémentaux de feu

*Objet merveilleux, rare*

Tant qu'un feu brûle dans ce brasero en laiton, vous pouvez utiliser
une action pour prononcer le mot de commande du brasero et invoquer un
élémentaire du feu, comme si vous aviez lancé le sort *conjuration
élémentaire*. Le brasero ne peut plus être utilisé
de cette façon jusqu'à la prochaine aube. Le brasero pèse 5 livres.



### Broche bouclier

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez cette broche, vous avez une résistance aux dégâts de
force, et vous êtes immunisé aux dégâts du sort de *projectile
magique*.



### Balai volant

*Objet merveilleux, peu commun*

Ce balai en bois, qui pèse 1,5 kg, fonctionne comme un balai ordinaire
jusqu'à ce que vous vous teniez à califourchon sur lui et prononciez
son mot d'ordre. Il plane alors en dessous de vous et peut être monté
dans les airs. Sa vitesse de vol est de 15 mètres. Il peut porter
jusqu'à 400 livres, mais sa vitesse de vol passe à 30 pieds s'il porte
plus de 200 livres. Le balai cesse de planer lorsque vous atterrissez.

Vous pouvez envoyer le balai voyager seul jusqu'à une destination
située à moins d'un kilomètre de vous si vous prononcez le mot de
commande, si vous nommez l'endroit et si vous le connaissez bien. Le
balai revient vers vous lorsque vous prononcez un autre mot de commande,
à condition qu'il soit toujours à moins d'un kilomètre de vous.



### Cierge d'invocation

*Objet merveilleux, très rare (nécessite d'être accordé).*

Cette bougie effilée est dédiée à une divinité et partage l'alignement
de cette dernière. L'alignement de la bougie peut être détecté avec le
sort Détection du mal et du *bien*. Le MJ
choisit le dieu et l'alignement associé ou détermine l'alignement au
hasard.

  -----------------------
    d20   Alignement
  ------- ---------------
    1-2   Le mal
          chaotique

    3-4   Chaotique
          neutre

    5-7   Bien chaotique

    8-9   Mal neutre

   10-11  Neutre

   12-13  Neutre bon

   14-15  Le mal licite

   16-17  Légalement
          neutre

   18-20  Bien licite
  -----------------------

La magie de la bougie est activée lorsque la bougie est allumée, ce qui
nécessite une action. Après avoir brûlé pendant 4 heures, la bougie est
détruite. Vous pouvez l'éteindre plus tôt pour l'utiliser plus tard.
Déduisez le temps qu'elle a brûlé par incréments d'une minute de la
durée totale de combustion de la bougie.

Lorsqu'elle est allumée, la bougie diffuse une faible lumière dans un
rayon de 30 pieds. Toute créature se trouvant dans cette lumière et dont
l'alignement correspond à celui de la bougie effectue des jets
d'attaque, des jets de sauvegarde et des tests de capacité avec un
avantage. De plus, un clerc ou un druide dans la lumière dont
l'alignement correspond à celui de la bougie peut lancer des sorts de
1er niveau qu'il a préparés sans dépenser d'emplacements de sorts,
bien que l'effet du sort soit comme s'il était lancé avec un
emplacement de 1er niveau.

Sinon, lorsque vous allumez la bougie pour la première fois, vous pouvez
lancer le sort *Portail* avec elle. Ce faisant, vous détruisez
la bougie.



### Cape du prestidigitateur

*Objet merveilleux, rare*

Cette cape a une légère odeur de soufre. Lorsque vous la portez, vous
pouvez l'utiliser pour lancer le sort de *porte
dimensionnelle* en tant qu'action. Cette propriété de
la cape ne peut pas être réutilisée avant la prochaine aube.

Lorsque vous disparaissez, vous laissez derrière vous un nuage de fumée,
et vous apparaissez dans un nuage de fumée similaire à votre
destination. La fumée obscurcit légèrement l'espace que vous avez
quitté et celui où vous apparaissez, et elle se dissipe à la fin de
votre prochain tour. Un vent léger ou plus fort disperse la fumée.



### Tapis volant

*Objet merveilleux, très rare*

Vous pouvez prononcer le mot d'Injonction du tapis comme une action
pour faire planer et voler le tapis. Il se déplace selon vos
instructions vocales, à condition que vous soyez à moins de 10 mètres de
lui.

Il existe quatre tailles de tapis volant. Le MJ choisit la taille d'un
tapis donné ou la détermine au hasard.

  -------------------------------------------------
    d100       Taille       Capacité   Vitesse de
                                           vol
  -------- --------------- ---------- -------------
   01-20    3 ft. × 5 ft.   200 lb.     80 pieds

   21-55    4 ft. × 6 ft.   400 lb.     60 pieds

   56-80    5 ft. × 7 ft.   600 lb.     40 pieds

   81-100   6 ft. × 9 ft.   800 lb.     30 pieds
  -------------------------------------------------

Un tapis peut porter jusqu'à deux fois le poids indiqué sur la table,
mais il vole à demi-vitesse s'il porte plus que sa capacité normale.



### Encensoir de contrôle des élémentaux de l'air

*Objet merveilleux, rare*

Tant que de l'encens brûle dans cet encensoir, vous pouvez utiliser une
action pour prononcer le mot de commande de l'encensoir et invoquer un
élémentaire de l'air, comme si vous aviez lancé le sort invocation
élémentaire. L'encensoir ne peut plus être utilisé de cette façon
jusqu'à l'aube suivante.

Ce récipient de 6 pouces de large et de 1 pied de haut ressemble à un
calice avec un couvercle décoré. Il pèse 1 livre.



### Carillon d'ouverture

*Objet merveilleux, rare*

Ce tube métallique creux mesure environ 1 pied de long et pèse 1 livre.
Vous pouvez le frapper comme une action, en le pointant vers un objet
dans un rayon de 120 pieds de vous qui peut être ouvert, comme une
porte, un couvercle ou un verrou. Le carillon émet un son clair, et une
serrure ou un verrou de l'objet s'ouvre, à moins que le son ne puisse
atteindre l'objet. S'il ne reste aucun verrou ou loquet, l'objet
lui-même s'ouvre.

Le carillon peut être utilisé dix fois. Après la dixième fois, il se
fissure et devient inutile.



### Diadème de destruction

*Objet merveilleux, peu commun*

Lorsque vous portez ce bracelet, vous pouvez utiliser une action pour
lancer le sort *rayon ardent* avec lui. Lorsque vous
effectuez les attaques du sort, vous le faites avec un bonus d'attaque
de +5. Le circlet ne peut plus être utilisé de cette façon jusqu'à la
prochaine aube.



### Cape d'Aranéide

*Objet merveilleux, très rare (nécessite d'être accordé).*

Ce fin vêtement est fait de soie noire entrelacée de légers fils
argentés. Lorsque vous le portez, vous obtenez les avantages suivants :

-   Vous avez une résistance aux dégâts du poison.
-   Votre vitesse d'ascension est égale à votre vitesse de marche.
-   Vous pouvez vous déplacer vers le haut, vers le bas et sur des
    surfaces verticales et à l'envers le long des plafonds, tout en
    gardant les mains libres.
-   Vous ne pouvez pas être pris dans des toiles d'araignées de toutes
    sortes et vous pouvez vous déplacer à travers les toiles comme s'il
    s'agissait d'un terrain difficile.
-   Vous pouvez utiliser une action pour lancer le sort de
    *toile*. La toile créée par le sort
    remplit deux fois sa surface normale. Une fois utilisée, cette
    propriété de la cape ne peut plus être utilisée avant l'aube
    suivante.



### Cape de déplacement

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez cette cape, elle projette une illusion qui vous
donne l'impression de vous tenir dans un endroit proche de votre
emplacement réel, ce qui fait que toute créature a un désavantage aux
jets d'attaque contre vous. Si vous subissez des dégâts, la propriété
cesse de fonctionner jusqu'au début de votre prochain tour. Cette
propriété est supprimée lorsque vous êtes frappé d'incapacité, entravé
ou autrement incapable de vous déplacer.



### Cape elfique

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez cette cape avec son capuchon relevé, les tests de
Sagesse (Perception) effectués pour vous voir ont un désavantage, et
vous avez un avantage sur les tests de Dextérité (Discrétion) effectués
pour vous cacher, car la couleur de la cape change pour vous camoufler.
Relever ou abaisser la capuche nécessite une action.



### Cape de protection

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Vous gagnez un bonus de +1 à la CA et aux jets de sauvegarde lorsque
vous portez cette cape.



### Cape de la chauve-souris

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez cette cape, vous avez un avantage sur les tests de
Dextérité (Discrétion). Dans une zone de lumière chaotique ou de
ténèbres, vous pouvez saisir les bords de la cape à deux mains et
l'utiliser pour voler à une vitesse de 40 pieds. Si vous n'arrivez pas
à saisir les bords de la cape pendant que vous volez de cette façon, ou
si vous n'êtes plus dans la lumière tamisée ou les ténèbres, vous
perdez cette vitesse de vol.

Lorsque vous portez la cape dans une zone de lumière chétive ou de
ténèbres, vous pouvez utiliser votre action pour vous lancer un sort de
*polymorphie* et vous transformer en chauve-souris.
Pendant que vous êtes sous la forme de la chauve-souris, vous conservez
vos scores d'Intelligence, de Sagesse et de Charisme. La cape ne peut
plus être utilisée de cette façon jusqu'à la prochaine aube.



### Cape de la raie manta

*Objet merveilleux, peu commun*

Lorsque vous portez cette cape avec sa capuche relevée, vous pouvez
respirer sous l'eau et vous avez une vitesse de nage de 60 pieds. Tirer
la capuche vers le haut ou vers le bas nécessite une action.



### Boule de cristal

*Objet merveilleux, très rare ou légendaire (nécessite d'être
accordé).*

La boule de cristal typique, un objet très rare, mesure environ 15 cm de
diamètre. En la touchant, vous pouvez lancer le sort de
*scrutation* avec elle.

Les variantes suivantes de la boule de cristal sont des objets
légendaires et possèdent des propriétés supplémentaires.

***Boule de cristal de lecture des pensées.*** Vous pouvez utiliser une
action pour lancer le sort *détection des pensées*
(sauvegarde DC 17) pendant que vous faites de la scrutation avec la
boule de cristal, en ciblant les créatures que vous pouvez voir dans un
rayon de 30 pieds du capteur du sort. Vous n'avez pas besoin de vous
concentrer sur cette *détection des pensées* pour la
maintenir pendant sa durée, mais elle prend fin si la
*scrutation* prend fin.

***Boule de cristal de Télépathie.*** Pendant la Scrutation avec la
boule de cristal, vous pouvez communiquer par télépathie avec les
créatures que vous pouvez voir dans un rayon de 30 pieds du capteur du
sort. Vous pouvez également utiliser une action pour lancer le sort de
*suggestion* à travers le capteur sur
une de ces créatures. Vous n'avez pas besoin de vous concentrer sur
cette *suggestion* pour la maintenir pendant sa durée,
mais elle prend fin si la *scrutation* prend fin. Une fois
utilisé, le pouvoir de *suggestion* de la boule de
cristal ne peut plus être utilisé avant l'aube suivante.

***Boule de cristal de vision suprême.*** Pendant la Scrutation avec la
boule de cristal, vous avez une vision véritable dans un rayon de 120
pieds centré sur le capteur du sort.



### Cube de force

*Objet merveilleux, rare (à accorder)*

Ce cube fait environ un pouce de diamètre. Chaque face comporte une
marque distincte sur laquelle on peut appuyer. Le cube commence avec 36
charges, et il regagne 1d20 charges dépensées chaque jour à l'aube.

Vous pouvez utiliser une action pour appuyer sur l'une des faces du
cube, en dépensant un certain nombre de charges en fonction de la face
choisie, comme indiqué dans la table des faces du Cube de force. Chaque
face a un effet différent. Si le cube n'a plus assez de charges, rien
ne se passe.

Sinon, une barrière de force invisible prend naissance, formant un cube
de 15 pieds de côté. La barrière est centrée sur vous, se déplace avec
vous, et dure 1 minute, jusqu'à ce que vous utilisiez une action pour
appuyer sur la sixième face du cube, ou que le cube soit à court de
charges.

Vous pouvez modifier l'effet de la barrière en appuyant sur une autre
face du cube et en dépensant le nombre de charges requis, ce qui
réinitialise la durée.

Si votre mouvement fait entrer la barrière en contact avec un objet
solide qui ne peut pas passer à travers le cube, vous ne pouvez pas vous
rapprocher de cet objet tant que la barrière demeure.

  ----------------------------------------------------------------------------
   Visage   Charges  Effet
  -------- --------- ---------------------------------------------------------
     1         1     Les gaz, le vent et le brouillard ne peuvent pas passer à
                     travers la barrière.

     2         2     La matière non vivante ne peut pas passer à travers la
                     barrière. Les murs, les sols et les plafonds peuvent
                     passer à votre gré.

     3         3     La matière vivante ne peut pas passer la barrière.

     4         4     Les effets des sorts ne peuvent pas passer à travers la
                     barrière.

     5         5     Rien ne peut passer à travers la barrière. Les murs, les
                     planchers et les plafonds peuvent passer à travers à
                     votre discrétion.

     6         0     La barrière se désactive.
  ----------------------------------------------------------------------------

  : Cube de force Faces

Le cube perd des charges lorsque la barrière est ciblée par certains
sorts ou entre en contact avec certains effets de sorts ou d'objets
magiques, comme indiqué dans le tableau ci-dessous.

  -------------------------------------------------------
  Sort ou objet                              Charges
                                             perdues
  ------------------------------------------ ------------
  *Désintégration*          1d12

  *Cor de dévastation*  1d10

  *Passe-muraille*              1d6

  Rayons*prismatiques*   1d20

  *Mur*de feu               1d4
  -------------------------------------------------------



### Portail Cube des plans

*Objet merveilleux, légendaire*

Ce cube fait 10 cm de diamètre et dégage une énergie magique palpable.
Les six côtés du cube sont chacun liés à un plan d'existence différent,
dont l'un est le plan matériel. Les autres côtés sont liés à des plans
déterminés par le MJ.

Vous pouvez utiliser une action pour appuyer sur un côté du cube pour
lancer le sort de *portail* avec lui, ouvrant un portail vers
le plan correspondant à ce côté. Alternativement, si vous utilisez une
action pour appuyer deux fois sur un côté, vous pouvez lancer le sort de
*changement de plan* avec le cube et
transporter les cibles vers le plan correspondant à ce côté.

Le cube possède 3 charges. Chaque utilisation du cube consomme 1 charge.
Le cube récupère 1d3 charges dépensées par jour à l'aube.



### Dague venimeuse

*Arme (dague), rare*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

Vous pouvez utiliser une action pour faire en sorte qu'un poison noir
et épais recouvre la lame. Le poison reste pendant 1 minute ou jusqu'à
ce qu'une attaque utilisant cette arme touche une créature.

Cette créature doit réussir un jet de sauvegarde de Constitution DC 15
ou subir 2d10 points de dégâts de poison et être empoisonnée pendant 1
minute. La dague ne peut plus être utilisée de cette façon jusqu'à
l'aube suivante.



### Épée dansante

*Arme (n'importe quelle épée), très rare (requiert une syntonisation)*

Vous pouvez utiliser une action bonus pour lancer cette épée magique
dans les airs et prononcer le mot d'Injonction. Lorsque vous faites
cela, l'épée commence à planer, vole jusqu'à 30 pieds et attaque une
créature de votre choix dans un rayon de 1,5 mètre. L'épée utilise
votre jet d'attaque et votre modificateur de valeur caractéristique
pour les jets de dégâts.

Pendant que l'épée plane, vous pouvez utiliser une action bonus pour la
faire voler jusqu'à 10 mètres jusqu'à un autre endroit situé à 10
mètres de vous. Dans le cadre de la même action bonus, vous pouvez faire
en sorte que l'épée attaque une créature située à moins de 1,5 mètre
d'elle.

Après la quatrième attaque de l'épée planante, elle vole jusqu'à 30
pieds et tente de revenir dans votre main. Si vous n'avez pas de main
libre, elle tombe au sol à vos pieds. Si l'épée n'a pas de chemin
libre jusqu'à vous, elle se déplace aussi près de vous qu'elle le
peut, puis tombe au sol. Elle cesse également de planer si vous la
saisissez ou si vous vous éloignez d'elle de plus de 10 mètres.



### Carafe intarissable

*Objet merveilleux, peu commun*

Ce flacon bouché fait du bruit lorsqu'on le secoue, comme s'il
contenait de l'eau. La carafe pèse 2 livres.

Vous pouvez utiliser une action pour retirer le bouchon et prononcer
l'un des trois mots d'injonction, après quoi une quantité d'eau douce
ou d'eau salée (au choix) se déverse de la fiole. L'eau cesse de
s'écouler au début de votre prochain tour. Choisissez parmi les options
suivantes :

-   \"Stream\" produit 1 gallon d'eau.
-   \"Fontaine\" produit 5 gallons d'eau.
-   \"Geyser\" produit 30 gallons d'eau qui jaillissent en un geyser de
    30 pieds de long et 1 pied de large. Comme action bonus lorsque vous
    tenez la carafe, vous pouvez diriger le geyser vers une créature que
    vous pouvez voir dans un rayon de 30 pieds autour de vous. La cible
    doit réussir un jet de sauvegarde de Force DC 13 ou subir 1d4 points
    de dégâts de contondant et tomber à plat ventre. Au lieu d'une
    créature, vous pouvez cibler un objet qui n'est pas porté et qui ne
    pèse pas plus de 200 livres. L'objet est soit renversé, soit poussé
    jusqu'à 15 pieds de vous.



### Cartes d'illusion

*Objet merveilleux, peu commun*

Cette boîte contient un jeu de cartes en parchemin. Un jeu complet
comporte 34 cartes. Un jeu trouvé comme trésor est généralement dépourvu
de 1d20 - 1 cartes.

La magie du jeu ne fonctionne que si les cartes sont tirées au hasard
(vous pouvez utiliser un jeu de cartes à jouer modifié pour simuler le
jeu). Vous pouvez utiliser une action pour tirer une carte au hasard du
jeu et la jeter au sol en un point situé à 30 pieds de vous.

Une illusion d'une ou plusieurs créatures se forme sur la carte lancée
et reste jusqu'à ce qu'elle soit dissipée. Une créature illusoire
semble réelle, de taille appropriée, et se comporte comme si elle était
une créature réelle, sauf qu'elle ne peut pas faire de mal. Tant que
vous êtes à moins de 120 pieds de la créature illusoire et que vous
pouvez la voir, vous pouvez utiliser une action pour la déplacer
magiquement n'importe où dans un rayon de 30 pieds autour de sa carte.
Toute interaction physique avec la créature illusoire révèle qu'elle
est une illusion, car les objets la traversent. Une personne qui utilise
une action pour inspecter visuellement la créature l'identifie comme
illusoire en réussissant un test d'Intelligence (Investigation) DC 15.
La créature apparaît alors translucide.

L'illusion dure jusqu'à ce que sa carte soit déplacée ou que
l'illusion soit dissipée. Lorsque l'illusion prend fin, l'image sur
sa carte disparaît, et cette carte ne peut plus être utilisée.

  -----------------------------------------------
  Carte à jouer      Illusion
  ------------------ ----------------------------
  As de cœur         Dragon rouge

  Roi des cœurs      Chevalier et quatre gardes.

  Reine de cœur      Succube ou incubus.

  Valet de cœur      Druide

  Dix de cœur        Géant des nuages

  Neuf de cœur       Ettin

  Huit de cœur       Gobelours

  Deux coeurs        Gobelin

  As de carreau      Eyestalker

  Le roi du diamant  Archimage et apprenti mage

  Reine des diamants Guenaude nocturne

  Valet de carreau   Assassinat

  Dix de carreau     Géant du feu

  Neuf de carreau    Ogre mage

  Huit de carreau    Gnoll

  Deux de carreau    Kobold

  As de pique        Liche

  Roi de pique       Prêtre et deux acolytes

  Reine de pique     Méduse

  Valet de pique     Vétéran

  Dix de pique       Géant du givre

  Neuf de pique      Troll

  Huit de pique      Hobgobelin

  Deux de pique      Gobelin

  As de trèfle       Golem de fer

  Roi des gourdins   Capitaine bandit et trois
                     bandits.

  Reine des gourdins Érinye

  Valet de trèfle    Berserker

  Dix gourdins       Géant des collines

  Neuf gourdins      Ogre

  Huit de gourdin    Orc

  Deux gourdins      Kobold

  Jokers (2)         Vous (le propriétaire de la
                     terrasse)
  -----------------------------------------------



### Cartes merveilleuses

*Objet merveilleux, légendaire*

Généralement présenté dans une boîte ou une pochette, ce jeu contient un
certain nombre de cartes en ivoire ou en vélin. La plupart (75 %) de ces
jeux ne comportent que treize cartes, mais les autres en comportent
vingt-deux.

Avant de tirer une carte, vous devez déclarer combien de cartes vous
avez l'intention de tirer, puis les tirer au hasard (vous pouvez
utiliser un jeu de cartes à jouer modifié pour simuler le jeu). Toute
carte tirée au-delà de ce nombre n'a aucun effet. Dans le cas
contraire, dès que vous tirez une carte du jeu, sa magie prend effet.
Vous devez tirer chaque carte au maximum 1 heure après le tirage
précédent. Si vous ne tirez pas le nombre choisi, les cartes restantes
s'envolent toutes seules du paquet et font effet en une seule fois.

Une fois qu'une carte est tirée, elle disparaît de la circulation. Sauf
s'il s'agit du Fou ou du Bouffon, la carte réapparaît dans le jeu, ce
qui permet de tirer deux fois la même carte.

  -------------------------------
  Carte à jouer       Carte
  ------------------- -----------
  As de carreau       Vizir\*

  Le roi du diamant   Soleil

  Reine des diamants  Lune

  Valet de carreau    Étoile

  Deux de carreau     Comet\*

  As de cœur          The Fates\*

  Roi des cœurs       Trône

  Reine de cœur       Clé

  Valet de cœur       Chevalier

  Deux coeurs         Gem\*

  As de trèfle        Talons\*

  Roi des gourdins    Le vide

  Reine des gourdins  Flammes

  Valet de trèfle     Crâne

  Deux gourdins       Idiot\*

  As de pique         Donjon\*

  Roi de pique        Ruine

  Reine de pique      Euryale

  Valet de pique      Roublard

  Deux de pique       Balance\*

  Joker (avec TM)     Fool\*

  Joker (sans TM)     Bouffon
  -------------------------------

\* On ne le trouve que dans un jeu de vingt-deux cartes.

***L'équilibre.*** Votre esprit subit une altération déchirante,
entraînant un changement d'alignement. Le licite devient chaotique, le
bien devient le mal, et vice versa. Si vous êtes vraiment neutre ou sans
alignement, cette carte n'a aucun effet sur vous.

***Comète.*** Si vous vainquez à vous seul le prochain monstre ou groupe
de monstres hostiles que vous rencontrez, vous gagnez suffisamment de
points d'expérience pour gagner un niveau. Sinon, cette carte n'a
aucun effet.

***Donjon.*** Vous disparaissez et êtes enseveli dans un état
d'animation suspendue dans une sphère extradimensionnelle. Tout ce que
vous portiez reste dans l'espace que vous occupiez au moment de votre
disparition. Vous restez emprisonné jusqu'à ce que vous soyez trouvé et
retiré de la sphère. Vous ne pouvez être localisé par aucune magie de
divination, mais un sort de *Souhait* peut révéler
l'emplacement de votre prison. Vous ne tirez plus de cartes.

***Euryale.*** Le visage de la carte, semblable à une méduse, vous
maudit. Vous subissez un malus de -2 aux jets de sauvegarde tant que
vous êtes maudit de cette façon. Seul un dieu ou la magie de la carte
Destinées peut mettre fin à cette malédiction.

***Les destins.*** Le tissu de la réalité s'effiloche et tourne à
nouveau, ce qui vous permet d'éviter ou d'effacer un événement comme
s'il n'était jamais arrivé. Vous pouvez utiliser la magie de la carte
dès que vous la tirez ou à tout autre moment avant de mourir.

***Flammes.*** Un diable puissant devient votre ennemi. Le diable
cherche ta ruine et te pourrit la vie, savourant ta souffrance avant de
tenter de te tuer. Cette inimitié dure jusqu'à votre mort ou celle du
démon.

***Fou.*** Vous perdez 10 000 XP, défaussez cette carte et piochez à
nouveau dans la pioche, en comptant les deux tirages comme un de vos
tirages déclarés. Si la perte de cette quantité d'XP vous fait perdre
un niveau, vous perdez à la place une quantité qui vous laisse juste
assez d'XP pour conserver votre niveau.

***Gemme.*** Vingt-cinq bijoux d'une valeur de 2 000 gp chacun ou
cinquante gemmes d'une valeur de 1 000 gp chacune apparaissent à vos
pieds.

***Idiot.*** Diminuez de façon permanente votre Intelligence de 1d4 + 1
(avec un score minimum de 1). Vous pouvez tirer une carte supplémentaire
en plus de vos tirages déclarés.

***Bouffon.*** Vous gagnez 10 000 XP, ou vous pouvez tirer deux cartes
supplémentaires en plus de vos tirages déclarés.

***Clé.*** Une arme magique rare ou plus rare avec laquelle vous êtes
compétent apparaît dans vos mains. Le MJ choisit l'arme.

***Chevalier.*** Vous gagnez le service d'un guerrier de 4e niveau qui
apparaît dans un espace que vous choisissez à moins de 30 pieds de vous.
Le guerrier est de la même origine que vous et vous sert loyalement
jusqu'à la mort, croyant que le destin l'a attiré vers vous. Vous
contrôlez ce personnage.

***Lune.*** On vous accorde la capacité de lancer le sort
*Souhait* 1d3 fois.

***Roublard.*** Un personnage non joueur choisi par le MJ devient
hostile à votre égard. L'identité de votre nouvel ennemi n'est pas
connue avant que le PNJ ou quelqu'un d'autre ne la révèle. Rien de
moins qu'un sort de *Souhait* ou une intervention divine ne
peut mettre fin à l'hostilité du PNJ à votre égard.

***Ruine.*** Toutes les formes de richesse que vous portez ou possédez,
autres que les objets magiques, sont perdues pour vous. Les biens
portables disparaissent. Les entreprises, les bâtiments et les terrains
que vous possédez sont perdus d'une manière qui altère le moins la
réalité. Toute documentation prouvant que vous êtes propriétaire d'un
objet perdu disparaît également.

***Crâne.*** Vous invoquez un avatar de la mort - un squelette humanoïde
fantomatique vêtu d'une robe noire en lambeaux et portant une faux
spectrale. Il apparaît dans un espace au choix du MJ à moins de 3 mètres
de vous et vous attaque, avertissant tous les autres que vous devez
gagner la bataille seul. L'avatar se bat jusqu'à ce que vous mouriez
ou qu'il tombe à 0 points de vie, après quoi il disparaît. Si
quelqu'un tente de vous aider, il invoque son propre avatar de la mort.
Une créature tuée par un avatar de la mort ne peut pas être ramenée à la
vie. (Voir \"Avatar de la mort.

***Étoile.*** Augmentez l'un de vos scores de capacité de 2. Le score
peut dépasser 20 mais ne peut pas dépasser 24.

***Soleil.*** Vous gagnez 50 000 XP, et un objet merveilleux (que le MJ
détermine au hasard) apparaît dans vos mains.

***Talons.*** Chaque objet magique que vous portez ou transportez se
désintègre. Les artefacts en votre possession ne sont pas détruits mais
disparaissent.

***Trône.*** Vous gagnez la maîtrise de la compétence Persuasion, et
vous doublez votre bonus de compétence aux contrôles effectués avec
cette compétence. De plus, vous obtenez la propriété légitime d'un
petit donjon quelque part dans le monde. Cependant, le donjon est
actuellement entre les mains de monstres, que vous devez éliminer avant
de pouvoir vous l'approprier.

***Vizir.*** À tout moment, dans l'année qui suit le tirage de cette
carte, vous pouvez poser une question en méditation et recevoir
mentalement une réponse véridique à cette question. Outre
l'information, la réponse vous aide à résoudre un problème déroutant ou
un autre dilemme. En d'autres termes, la connaissance est accompagnée
d'une sagesse sur la manière de l'appliquer.

***Le Néant.*** Cette carte noire est synonyme de désastre. Votre âme
est extraite de votre corps et contenue dans un objet dans un lieu
choisi par le MJ. Un ou plusieurs êtres puissants gardent l'endroit.
Pendant que votre âme est ainsi piégée, votre corps est frappé
d'incapacité. Un sort de *Souhait* ne peut pas restaurer votre
âme, mais le sort révèle l'emplacement de l'objet qui la contient.
Vous ne tirez plus de cartes.



### Gardienne

*Arme (n'importe quelle épée), légendaire (nécessite d'être accordé)*

Vous gagnez un bonus de +3 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

La première fois que vous attaquez avec l'épée à chacun de vos tours,
vous pouvez transférer une partie ou la totalité du bonus de l'épée à
votre classe d'armure, au lieu d'utiliser le bonus sur toutes les
attaques de ce tour. Par exemple, vous pouvez réduire le bonus à vos
jets d'attaque et de dégâts à +1 et gagner un bonus de +2 à la CA. Les
bonus ajustés restent en vigueur jusqu'au début de votre prochain tour,
bien que vous deviez tenir l'épée pour obtenir un bonus à la CA.



### Armure démoniaque

*Armure (harnois), très rare (nécessite une harmonisation)*

Lorsque vous portez cette armure, vous gagnez un bonus de +1 à la CA, et
vous pouvez comprendre et parler l'Abyssal. De plus, les gantelets
griffus de l'armure transforment les frappes sans arme avec vos mains
en armes magiques qui infligent des dégâts tranchants, avec un bonus de
+1 aux jets d'attaque et aux jets de dégâts et un dé de dégâts de 1d8.

***Malédiction.*** Une fois que vous avez revêtu cette armure maudite,
vous ne pouvez plus l'enlever à moins d'être visé par le sort
*supprimer la malédiction* ou une magie similaire.
Lorsque vous portez l'armure, vous avez un désavantage aux jets
d'attaque contre les démons et aux jets de sauvegarde contre leurs
sorts et capacités spéciales.



### Menottes dimensionnelles

*Objet merveilleux, rare*

Vous pouvez utiliser une action pour placer ces entraves sur une
créature incapacitée. Les entraves s'ajustent à une créature de taille
Petit à Grand. En plus de servir de manilles mondaines, les manilles
empêchent une créature liée par elles d'utiliser toute méthode de
déplacement extradimensionnel, y compris la téléportation ou le voyage
vers un autre plan d'existence. Elles n'empêchent pas la créature de
passer par un portail interdimensionnel.

Vous et toute créature que vous désignez lorsque vous utilisez les
entraves pouvez utiliser une action pour les retirer. Une fois tous les
30 jours, la créature liée peut effectuer un test de Force (Athlétisme)
DC 30. En cas de réussite, la créature se libère et détruit les
entraves.



### Armure d'écailles de dragon

*Armure d'écailles, très rare (nécessite d'être lié).*

L'armure d'écailles de dragon est fabriquée à partir des écailles
d'une espèce de dragon. Parfois, les dragons récupèrent leurs écailles
et les offrent aux humanoïdes. D'autres fois, les chasseurs écaillent
et conservent soigneusement la peau d'un dragon mort. Dans les deux
cas, l'armure d'écailles de dragon a une grande valeur.

Lorsque vous portez cette armure, vous bénéficiez d'un bonus de +1 à la
CA, vous avez un avantage aux jets de sauvegarde contre la Présence
effrayante et les armes à souffle des dragons, et vous avez une
résistance à un type de dégâts qui est déterminé par le type de dragon
qui a fourni les écailles (voir la table).

De plus, vous pouvez concentrer vos sens comme une action pour discerner
magiquement la distance et la direction du dragon le plus proche dans un
rayon de 30 miles de vous et qui est du même type que l'armure. Cette
action spéciale ne peut pas être utilisée à nouveau avant l'aube
suivante.

  ---------------------
  Dragon   Résistance
  -------- ------------
  Noir     Acide

  Bleu     La foudre

  Laiton   Feu

  Bronze   La foudre

  Cuivre   Acide

  Or       Feu

  Vert     Empoisonné

  Rouge    Feu

  Argent   Froid

  Blanc    Froid
  ---------------------



### Tueuse de dragons

*Arme (n'importe quelle épée), rare*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

Lorsque vous touchez un dragon avec cette arme, celui-ci subit 3d6
points de dégâts supplémentaires du type de l'arme. Dans le cadre de
cette arme, le terme \" dragon \" désigne toute créature de type dragon,
y compris les tortues-dragons et les wyvernes.



### Poussière de disparition

*Objet merveilleux, peu commun*

Présentée dans un petit sachet, cette poudre ressemble à du sable très
fin. Il y en a assez pour une seule utilisation. Lorsque vous utilisez
une action pour jeter la poussière en l'air, vous et chaque créature et
objet se trouvant à 3 mètres de vous devenez invisibles pendant 2d4
minutes. La durée est la même pour tous les sujets, et la poussière est
consommée lorsque sa magie fait effet. Si une créature affectée par la
poussière attaque ou lance un sort, l'invisibilité prend fin pour cette
créature.



### Poussière de dessication

*Objet merveilleux, peu commun*

Ce petit paquet contient 1d6 + 4 pincées de poussière. Vous pouvez
utiliser une action pour en saupoudrer une pincée sur de l'eau. La
poussière transforme un cube d'eau de 15 pieds de côté en une boulette
de la taille d'une bille, qui flotte ou repose près de l'endroit où la
poussière a été saupoudrée. Le poids de la boulette est négligeable.

Quelqu'un peut utiliser une action pour frapper la pastille contre une
surface dure, ce qui la fait se briser et libérer l'eau que la
poussière a absorbée. Cette action met fin à la magie de la pastille.

Un élémentaire composé principalement d'eau qui est exposé à une pincée
de poussière doit effectuer un jet de sauvegarde de Constitution DC 13,
subissant 10d6 dégâts nécrotiques en cas d'échec, ou la moitié en cas
de réussite.



### Poudre à éternuer et à étouffer

*Objet merveilleux, peu commun*

Trouvée dans un petit récipient, cette poudre ressemble à du sable très
fin. Elle semble être de la Poussière de disparition, et un sort
d'*identification* révèle qu'elle l'est. Il y en a assez
pour une utilisation.

Lorsque vous utilisez une action pour lancer une poignée de cette
poussière dans l'air, vous et chaque créature qui a besoin de respirer
dans un rayon de 30 pieds autour de vous devez réussir un jet de
sauvegarde de Constitution DC 15 ou devenir incapable de respirer, tout
en éternuant de manière incontrôlable. Une créature affectée de cette
manière est frappée d'incapacité et suffoque.

Tant qu'elle est consciente, une créature peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet sur
elle en cas de réussite. Le sort *Restauration
partielle* peut également mettre fin à l'effet
sur une créature.



### Harnois nain

*Armure (harnois), très rare*

Lorsque vous portez cette armure, vous gagnez un bonus de +2 à la CA. De
plus, si un effet vous déplace contre votre volonté le long du sol, vous
pouvez utiliser votre réaction pour réduire la distance de déplacement
jusqu'à 10 pieds.



### Marteau de lancer nain

*Arme (marteau de guerre), très rare (doit être accordée par un nain).*

Vous gagnez un bonus de +3 aux jets d'attaque et de dégâts effectués
avec cette arme magique. Elle a la propriété lancée avec une portée
normale de 20 pieds et une portée longue de 60 pieds. Lorsque vous
touchez avec une attaque à distance utilisant cette arme, elle inflige
1d8 points de dégâts supplémentaires ou, si la cible est un géant, 2d8
points de dégâts. Immédiatement après l'attaque, l'arme revient dans
votre main en volant.



### Carquois efficace

*Objet merveilleux, peu commun*

Chacun des trois compartiments du carquois est relié à un espace
extradimensionnel qui permet au carquois de contenir de nombreux objets
sans jamais peser plus de 2 livres. Le compartiment le plus court peut
contenir jusqu'à soixante flèches, boulons ou objets similaires. Le
compartiment de taille moyenne peut contenir jusqu'à dix-huit javelots
ou objets similaires. Le compartiment le plus long peut contenir
jusqu'à six objets longs, tels que des arcs, des bâtons ou des lances.

Vous pouvez tirer n'importe quel objet contenu dans le carquois comme
si vous le faisiez à partir d'un carquois ou d'un fourreau ordinaire.



### Bouteille de l'éfrit

*Objet merveilleux, très rare*

Cette bouteille en laiton peint pèse 1 livre. Lorsque vous utilisez une
action pour retirer le bouchon, un nuage de fumée épaisse s'échappe de
la bouteille. À la fin de votre tour, la fumée disparaît avec un éclair
de feu inoffensif, et un Éfrit apparaît dans un espace inoccupé à moins
de 30 pieds de vous.

La première fois que la bouteille est ouverte, le MJ effectue un jet
pour déterminer ce qui se passe.

  ------------------------------------------------------------------------
   d100   Effet
  ------- ----------------------------------------------------------------
   01-10  L'Éfrit vous attaque. Après avoir combattu pendant 5 rounds,
          l'éfrit disparaît, et la bouteille perd sa magie.

   11-90  L'Éfrit vous sert pendant une heure, en faisant ce que vous lui
          ordonnez. Puis l'éfrit retourne dans la bouteille, et un
          nouveau bouchon le contient. Le bouchon ne peut pas être enlevé
          pendant 24 heures. Les deux fois suivantes où la bouteille est
          ouverte, le même effet se produit. Si la bouteille est ouverte
          une quatrième fois, l'Éfrit s'échappe et disparaît, et la
          bouteille perd sa magie.

   91-00  L'Éfrit peut lancer le sort de *souhaits* trois fois
          pour vous. Il disparaît lorsqu'il accorde le dernier souhait ou
          après 1 heure, et la bouteille perd sa magie.
  ------------------------------------------------------------------------



### Gemme élémentaire

*Objet merveilleux, peu commun*

Cette gemme contient une mote d'énergie élémentaire. Lorsque vous
utilisez une action pour briser la gemme, un élémentaire est invoqué
comme si vous aviez lancé le sort *conjuration
élémentaire*, et la magie de la gemme est perdue.
Le type de gemme détermine l'élémentaire invoqué par le sort.

  ----------------------------------
  Gem             Élémentaire
                  invoqué
  --------------- ------------------
  Saphir bleu     Élémentaire de
                  l'air

  Diamant jaune   Élémentaire de la
                  terre

  Corindon rouge  Élémentaire du feu

  Émeraude        Élémentaire d'eau
  ----------------------------------



### Côte de mailles elfique

*Armure (chemise de mailles), rare*

Vous gagnez un bonus de +1 à la CA lorsque vous portez cette armure.
Vous êtes considéré comme compétent avec cette armure même si vous
n'êtes pas compétent avec les armures moyennes.



### Bouteille fumigène

*Objet merveilleux, peu commun*

De la fumée s'échappe de l'embouchure bouchée par du plomb de cette
bouteille en laiton, qui pèse 1 livre. Lorsque vous utilisez une action
pour retirer le bouchon, un nuage de fumée épaisse se déverse dans un
rayon de 60 pieds autour de la bouteille. La zone du nuage est fortement
obscurcie. Chaque minute que la bouteille reste ouverte et à
l'intérieur du nuage, le rayon augmente de 10 pieds jusqu'à atteindre
son rayon maximum de 120 pieds.

Le nuage persiste tant que la bouteille est ouverte. Pour fermer la
bouteille, vous devez prononcer son mot d'Injonction en guise
d'action. Une fois la bouteille fermée, le nuage se disperse au bout de
10 minutes. Un vent modéré (11 à 20 miles par heure) peut également
disperser la fumée après 1 minute, et un vent fort (21 miles par heure
ou plus) peut le faire après 1 round.



### Regard charmeur

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Ces lentilles de cristal s'adaptent sur les yeux. Elles possèdent 3
charges. Lorsque vous les portez, vous pouvez dépenser 1 charge en tant
qu'action pour lancer le sort *Charme-personne*
(sauvegarde DC 13) sur un humanoïde à moins de 30 pieds de vous, à
condition que vous et la cible puissiez vous voir. Les lentilles
récupèrent toutes les charges dépensées chaque jour à l'aube.



### Yeux grossissants

*Objet merveilleux, peu commun*

Ces lentilles de cristal s'adaptent sur les yeux. Lorsque vous les
portez, vous pouvez voir beaucoup mieux que la normale jusqu'à une
portée de 1 pied. Vous avez un avantage sur les tests d'Intelligence
(Investigation) qui reposent sur la vue lorsque vous fouillez une zone
ou étudiez un objet dans cette portée.



### Yeux d'Aigle

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Ces lentilles de cristal s'adaptent sur les yeux. Lorsque vous les
portez, vous avez un avantage sur les tests de Sagesse (Perception) qui
reposent sur la vue. Dans des conditions de visibilité claire, vous
pouvez distinguer les détails de créatures même extrêmement éloignées et
d'objets d'un diamètre de 60 cm.



### Jeton Plume

*Objet merveilleux, rare*

Ce très petite objet ressemble à une plume. Il existe différents types
de jetons plumes, chacun ayant un effet différent à usage unique. Le MJ
choisit le type de jeton ou le détermine au hasard.

  ----------------------
    d100   Jeton Plume
  -------- -------------
   01-20   Ancrage

   21-35   Oiseau

   36-50   Ventilateur

   51-65   Bateau cygne

   66-90   Arbre

   91-00   Fouet
  ----------------------

***Ancrage.*** Vous pouvez utiliser une action pour toucher le jeton à
un bateau ou un navire. Pendant les 24 heures suivantes, le navire ne
peut être déplacé par aucun moyen. Toucher à nouveau le jeton sur le
navire met fin à l'effet. Lorsque l'effet prend fin, le jeton
disparaît.

***Oiseau.*** Vous pouvez utiliser une action pour lancer le jeton à 5
pieds dans les airs. Le jeton disparaît et un énorme oiseau multicolore
prend sa place. L'oiseau a les statistiques d'un roc, mais il obéit à
vos simples injonctions et ne peut pas attaquer. Il peut porter jusqu'à
500 livres en volant à sa vitesse maximale (16 miles par heure pour un
maximum de 144 miles par jour, avec un repos d'une heure toutes les 3
heures de vol), ou 1 000 livres à la moitié de cette vitesse. L'oiseau
disparaît après avoir parcouru sa distance maximale pendant une journée
ou s'il tombe à 0 point de vie. Vous pouvez renvoyer l'oiseau par une
action.

***Éventail.*** Si vous êtes sur un bateau ou un navire, vous pouvez
utiliser une action pour lancer le jeton jusqu'à 3 mètres dans les
airs. Le jeton disparaît, et un éventail géant prend sa place.
L'éventail flotte et crée un vent assez fort pour remplir les voiles
d'un navire, augmentant sa vitesse de 5 miles par heure pendant 8
heures. Vous pouvez renvoyer l'éventail comme une action.

***Bateau cygne.*** Vous pouvez utiliser une action pour toucher le
jeton dans une étendue d'eau d'au moins 60 pieds de diamètre. Le jeton
disparaît, et un bateau de 50 pieds de long et 20 pieds de large en
forme de cygne prend sa place. Le bateau est autopropulsé et se déplace
sur l'eau à une vitesse de 6 miles par heure. Vous pouvez utiliser une
action lorsque vous êtes sur le bateau pour lui ordonner de se déplacer
ou de tourner jusqu'à 90 degrés. Le bateau peut transporter jusqu'à
trente-deux créatures moyennes ou plus petites. Une créature Grande
compte pour quatre créatures moyennes, tandis qu'une créature Très
grande compte pour neuf. Le bateau reste en place pendant 24 heures,
puis disparaît. Vous pouvez renvoyer le bateau en tant qu'action.

***Arbre.*** Vous devez être à l'extérieur pour utiliser ce jeton. Vous
pouvez utiliser une action pour le toucher dans un espace inoccupé sur
le sol. Le jeton disparaît, et à sa place, un chêne non magique prend
naissance. L'arbre mesure 60 pieds de haut et possède un tronc de 5
pieds de diamètre, et ses branches au sommet s'étendent dans un rayon
de 20 pieds.

***Fouet .*** Vous pouvez utiliser une action pour lancer le jeton vers
un point situé à 3 mètres de vous. Le jeton disparaît, et un fouet
flottant prend sa place. Vous pouvez ensuite utiliser une action bonus
pour effectuer une attaque de sort en mêlée contre une créature située à
moins de 3 mètres du fouet, avec un bonus d'attaque de +9. En cas de
succès, la cible subit 1d6 + 5 dégâts de force.

Comme action bonus à votre tour, vous pouvez diriger le fouet pour voler
jusqu'à 20 pieds et répéter l'attaque contre une créature dans un
rayon de 10 pieds. Le fouet disparaît au bout d'une heure, lorsque vous
utilisez une action pour l'écarter, ou lorsque vous êtes frappé
d'incapacité ou mourrez.



### Figurine merveilleuse

*Objet merveilleux, rareté par figurine*

Une Figurine merveilleuse est une statuette d'une bête assez petite
pour tenir dans une poche. Si vous utilisez une action pour prononcer le
mot de commande et lancez la figurine vers un point du sol situé à moins
de 60 pieds de vous, la figurine devient une créature vivante. Si
l'espace où la créature apparaîtrait est occupé par d'autres créatures
ou objets, ou s'il n'y a pas assez d'espace pour la créature, la
figurine ne devient pas une créature.

La créature est amicale envers vous et vos compagnons. Elle comprend vos
langues et obéit à vos ordres oraux. Si vous ne donnez aucun ordre, la
créature se défend mais ne fait aucune autre action.

La créature existe pendant une durée propre à chaque figurine. À la fin
de cette durée, la créature reprend sa forme de figurine. Elle redevient
une figurine prématurément si elle tombe à 0 points de vie ou si vous
utilisez une action pour prononcer à nouveau le mot d'Injonction en la
touchant.

Lorsque la créature redevient une figurine, sa propriété ne peut plus
être utilisée avant un certain temps, comme indiqué dans la description
de la figurine.

***Griffon de bronze (Rare).*** Cette statuette de bronze représente un
griffon rampant. Elle peut se transformer en griffon pour une durée
maximale de 6 heures. Une fois utilisée, elle ne peut plus l'être avant
5 jours.

***Vol d'ébène (Rare).*** Cette statuette d'ébène est sculptée à
l'image d'un taon. Elle peut devenir un Vol géant pendant 12 heures et peut être montée comme une
monture. Une fois qu'elle a été utilisée, elle ne peut plus l'être
avant 2 jours.

***Lions d'or (Rare).*** Ces statuettes en or de lions sont toujours
créées par paires. Vous pouvez utiliser une figurine ou les deux
simultanément. Chacune peut se transformer en lion pendant 1 heure
maximum. Une fois qu'un lion a été utilisé, il ne peut plus l'être
avant 7 jours.

***Chèvres en ivoire (Rare).*** Ces statuettes de chèvres en ivoire sont
toujours créées par lot de trois. Chaque chèvre a un aspect unique et
fonctionne différemment des autres. Leurs propriétés sont les suivantes
:

-   La *chèvre de voyage* peut devenir une Grande chèvre avec les mêmes
    statistiques qu'un cheval de selle. Elle dispose de 24 charges, et
    chaque heure ou portion d'heure qu'elle passe sous forme de bête
    coûte 1 charge. Tant qu'elle a des charges, vous pouvez l'utiliser
    aussi souvent que vous le souhaitez. Lorsqu'elle n'a plus de
    charges, elle redevient une figurine et ne peut plus être utilisée
    avant 7 jours, après quoi elle récupère toutes ses charges.
-   La *chèvre de travail* devient une chèvre géante pour une durée
    maximale de 3 heures. Une fois qu'il a été utilisé, il ne peut plus
    l'être avant 30 jours.
-   La *chèvre de la terreur* devient une chèvre géante pour une durée
    maximale de 3 heures. La chèvre ne peut pas attaquer, mais vous
    pouvez enlever ses cornes et les utiliser comme armes. Une corne
    devient une *lance +1*, et l'autre une *épée longue +2*. Retirer
    une corne nécessite une action, et les armes disparaissent et les
    cornes reviennent lorsque la chèvre reprend sa forme de figurine. De
    plus, la chèvre dégage une aura de terreur dans un rayon de 30 pieds
    tant que vous la chevauchez. Toute créature hostile à vous qui
    commence son tour dans l'aura doit réussir un jet de sauvegarde de
    Sagesse DC 15 ou être effrayée par la chèvre pendant 1 minute, ou
    jusqu'à ce que la chèvre reprenne sa forme de figurine. La créature
    effrayée peut répéter le jet de sauvegarde à la fin de chacun de ses
    tours, mettant fin à l'effet sur elle-même en cas de réussite. Une
    fois qu'elle a réussi son jet de sauvegarde contre l'effet, la
    créature est immunisée contre l'aura de la chèvre pendant les 24
    heures suivantes. Une fois que la figurine a été utilisée, elle ne
    peut plus l'être avant 15 jours.

***Éléphant en marbre (Rare).*** Cette statuette de marbre mesure
environ 10 cm de haut et de long. Elle peut se transformer en éléphant
pendant 24 heures maximum. Une fois qu'elle a été utilisée, elle ne
peut plus l'être avant 7 jours.

***Cheval d'obsidienne (très rare).*** Ce cheval d'obsidienne polie
peut devenir un cauchemar pendant 24 heures. Le cauchemar ne se bat que
pour se défendre.

Une fois qu'il a été utilisé, il ne peut pas être réutilisé avant 5
jours.

Si votre alignement est bon, la figurine a 10 % de chances chaque fois
que vous l'utilisez d'ignorer vos ordres, y compris un ordre de retour
à la forme figurine. Si vous montez le cauchemar alors qu'il ignore vos
ordres, vous et le cauchemar êtes instantanément transportés vers un
emplacement aléatoire sur un plan inférieur neutre d'alignement
mauvais, où le cauchemar reprend sa forme de figurine.

***Chien d'Onyx (Rare).*** Cette statuette de chien en onyx peut se
transformer en molosse pour une durée maximale de 6 heures. Le Molosse a
une Intelligence de 8 et peut parler le commun. Il a également une
vision dans le noir jusqu'à une portée de 60 pieds et peut voir les
créatures et les objets invisibles dans cette portée. Une fois qu'il a
été utilisé, il ne peut plus l'être avant 7 jours.

***Chouette serpentine (Rare).*** Cette statuette serpentine d'une
chouette peut se transformer en chouette géante pour une durée maximale
de 8 heures. Une fois qu'elle a été utilisée, elle ne peut plus l'être
avant 2 jours. La Chouette peut communiquer télépathiquement avec vous à
n'importe quelle portée si vous et elle êtes sur le même plan
d'existence.

***Corbeau d'argent (peu commun).*** Cette statuette en argent d'un
corbeau peut se transformer en corbeau pour une durée maximale de 12
heures. Une fois qu'elle a été utilisée, elle ne peut plus l'être
avant 2 jours. Lorsqu'elle est sous forme de corbeau, la figurine vous
permet de lancer sur elle le sort de *messager
animal* à volonté.



### Épée ardente

*Arme (n'importe quelle épée), rare (nécessite d'être accordé)*

Vous pouvez utiliser une action bonus pour prononcer le mot
d'Injonction de cette épée magique, faisant jaillir des flammes de la
lame. Ces flammes répandent une lumière vive dans un rayon de 12 mètres
et une lumière chaotique sur 12 mètres supplémentaires. Tant que l'épée
est enflammée, elle inflige 2d6 points de dégâts de feu supplémentaires
à toute cible qu'elle touche. Les flammes durent jusqu'à ce que vous
utilisiez une action bonus pour prononcer à nouveau le mot d'Injonction
ou jusqu'à ce que vous lâchiez ou rengainiez l'épée.



### Bateau pliable

*Objet merveilleux, rare*

Cet objet se présente comme une boîte en bois qui mesure 12 pouces de
long, 6 pouces de large et 6 pouces de profondeur. Elle pèse 1,5 kg et
flotte. On peut l'ouvrir pour y ranger des objets. Cet objet possède
également trois mots d'injonction, chacun nécessitant une action pour
être prononcé.

Un mot d'injonction fait que la boîte se déplie en un bateau de 10
pieds de long, 4 pieds de large et 2 pieds de profondeur. Le bateau a
une paire de rames, une ancre, un mât et une voile latine. Le bateau
peut accueillir confortablement jusqu'à quatre créatures moyennes.

Le deuxième mot d'Injonction fait que la boîte se déplie en un bateau
de 24 pieds de long, 8 pieds de large et 6 pieds de profondeur. Le
navire a un pont, des sièges pour ramer, cinq jeux de rames, une rame de
gouvernail, une ancre, une cabine de pont, et un mât avec une voile
carrée. Le navire peut accueillir confortablement quinze créatures
moyennes.

Lorsque la boîte devient un navire, son poids devient celui d'un navire
normal de sa taille, et tout ce qui était stocké dans la boîte reste
dans le bateau.

Le troisième mot d'Injonction fait en sorte que le Bateau pliable se
replie dans une boîte, à condition qu'aucune créature ne soit à bord.
Tous les objets du vaisseau qui ne peuvent pas rentrer dans la boîte
restent à l'extérieur de la boîte pendant qu'elle se replie. Tous les
objets du navire qui peuvent rentrer dans la boîte le font.



### Fer gelé

*Arme (n'importe quelle épée), très rare (requiert une syntonisation)*

Lorsque vous touchez avec une attaque utilisant cette épée magique, la
cible subit 1d6 dégâts de froid supplémentaires. De plus, tant que vous
tenez l'épée, vous avez une résistance aux dégâts de feu.

Par temps de gel, la lame émet une lumière vive dans un rayon de 3
mètres et une lumière chétive sur 3 mètres supplémentaires.

Lorsque vous dégainez cette arme, vous pouvez éteindre toutes les
flammes non magiques dans un rayon de 30 pieds autour de vous. Cette
propriété ne peut être utilisée qu'une fois par heure.



### Gantelets de puissance d'ogre

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Votre score de Force est de 19 lorsque vous portez ces gantelets. Ils
n'ont aucun effet sur vous si votre Force est déjà égale ou supérieure
à 19.



### Gemme d'illumination

*Objet merveilleux, peu commun*

Ce prisme possède 50 charges. Lorsque vous le tenez, vous pouvez
utiliser une action pour prononcer l'un des trois mots d'injonction
pour provoquer l'un des effets suivants :

-   Le premier mot d'Injonction fait que la gemme répand une lumière
    vive dans un rayon de 30 pieds et une lumière chétive sur 30 pieds
    supplémentaires. Cet effet ne nécessite pas de charge. Il dure
    jusqu'à ce que vous utilisiez une action bonus pour répéter le mot
    d'Injonction ou jusqu'à ce que vous utilisiez une autre fonction
    de la gemme.
-   Le second mot d'ordre demande 1 charge et permet à la gemme de
    tirer un brillant faisceau de lumière sur une créature que vous
    pouvez voir dans un rayon de 60 pieds autour de vous. La créature
    doit réussir un jet de sauvegarde de Constitution DC 15 ou devenir
    aveugle pendant 1 minute. La créature peut répéter le jet de
    sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet
    sur elle-même en cas de réussite.
-   Le troisième mot de commande consomme 5 charges et provoque
    l'embrasement de la gemme par une lumière aveuglante dans un cône
    de 30 pieds. Chaque créature dans le cône doit effectuer un jet de
    sauvegarde comme si elle était frappée par le rayon créé par le
    deuxième mot de commande.

Lorsque toutes les charges de la gemme sont dépensées, la gemme devient
un bijou non-magique d'une valeur de 50 gp.



### Gemme de vision

*Objet merveilleux, rare (à accorder)*

Cette gemme possède 3 charges. En tant qu'action, vous pouvez prononcer
le mot d'Injonction de la gemme et dépenser 1 charge. Pendant les 10
minutes suivantes, vous avez une vision véritable jusqu'à 120 pieds
lorsque vous regardez à travers la gemme.

La gemme regagne 1d3 charges dépensées par jour à l'aube.



### Tueuse de géants

*Arme (n'importe quelle hache ou épée), rare*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

Lorsque vous touchez un géant avec cette arme, celui-ci subit 2d6 points
de dégâts supplémentaires du type de l'arme et doit réussir un jet de
sauvegarde de Force DC 15 ou tomber à terre. Dans le cadre de cette
arme, le terme \" géant \" désigne toute créature de type géant, y
compris les ettins et les trolls.



### Armure de cuir clouté enchantée

*Armure (cuir clouté), rare*

Lorsque vous portez cette armure, vous gagnez un bonus de +1 à la CA.

Vous pouvez également utiliser une action bonus pour prononcer le mot de
commande de l'armure et lui faire prendre l'apparence d'un vêtement
normal ou d'un autre type d'armure. Vous décidez de son apparence, y
compris de sa couleur, de son style et de ses accessoires, mais
l'armure conserve son volume et son poids normaux. L'apparence
illusoire dure jusqu'à ce que vous utilisiez à nouveau cette propriété
ou que vous retiriez l'armure.



### Gants piégeurs de projectiles

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Ces gants semblent presque se fondre dans vos mains lorsque vous les
enfilez. Lorsqu'une attaque avec une arme à distance vous touche alors
que vous les portez, vous pouvez utiliser votre réaction pour réduire
les dégâts de 1d10 + votre modificateur de Dextérité, à condition
d'avoir une main libre. Si vous réduisez les dégâts à 0, vous pouvez
attraper le projectile s'il est suffisamment petit pour que vous
puissiez le tenir dans cette main.



### Gants de nage et d'escalade

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ces gants, l'escalade et la natation ne vous
coûtent pas de mouvement supplémentaire, et vous bénéficiez d'un bonus
de +5 aux tests de Force (Athlétisme) effectués pour grimper ou nager.



### Lunettes de nuit

*Objet merveilleux, peu commun*

Lorsque vous portez ces lentilles sombres, vous avez une vision dans le
noir jusqu'à une portée de 18 mètres. Si vous avez déjà la vision dans
le noir, le port des lunettes augmente sa portée de 18 mètres.



### Marteau de tonnerre

*Arme (maillet), légendaire*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

***Fléau des géants (nécessite d'être lié).*** Vous devez porter une
ceinture de force de géant (n'importe quelle variété) et des gantelets
de puissance d'ogre pour vous accorder à cette arme. La syntonisation
prend fin si vous retirez l'un de ces objets. Tant que vous êtes lié à
cette arme et que vous la tenez, votre score de Force augmente de 4 et
peut dépasser 20, mais pas 30. Lorsque vous obtenez un 20 sur un jet
d'attaque effectué avec cette arme contre un géant, ce dernier doit
réussir un jet de sauvegarde de Constitution DC 17 ou mourir.

Le marteau possède également 5 charges. Tant qu'il y est lié, vous
pouvez dépenser 1 charge et effectuer une attaque avec arme à distance
avec le marteau, en le lançant comme s'il avait la propriété lancée
avec une portée normale de 20 pieds et une portée longue de 60 pieds. Si
l'attaque touche, le marteau déclenche un coup de tonnerre audible
jusqu'à 300 pieds. La cible et toutes les créatures situées à moins de
10 mètres d'elle doivent réussir un jet de sauvegarde de Constitution
DC 17 ou être étourdies jusqu'à la fin de votre prochain tour.

Le marteau regagne 1d4 + 1 charges dépensées par jour à l'aube.



### Sac à dos pratique

*Objet merveilleux, rare*

Ce sac à dos comporte une poche centrale et deux poches latérales,
chacune d'entre elles étant un espace extradimensionnel.

Chaque poche latérale peut contenir jusqu'à 20 livres de matériel, sans
dépasser un volume de 2 pieds cubes. La grande poche centrale peut
contenir jusqu'à 8 pieds cubes ou 80 livres de matériel. Le sac à dos
pèse toujours 5 livres, quel que soit son contenu.

Placer un objet dans le havresac suit les règles normales d'interaction
avec les objets. Pour récupérer un objet dans le sac à dos, vous devez
utiliser une action. Lorsque vous mettez la main dans le havresac pour
récupérer un objet spécifique, celui-ci est toujours placé magiquement
sur le dessus.

Le havresac a quelques limites. S'il est surchargé, ou si un objet
pointu le transperce ou le déchire, le havresac se rompt et est détruit.
Si le havresac est détruit, son contenu est perdu à jamais, bien qu'un
artefact réapparaisse toujours quelque part. Si le havresac est
retourné, son contenu se répand, indemne, et le havresac doit être remis
en état avant de pouvoir être réutilisé. Si une créature respirante est
placée dans le havresac, elle peut survivre jusqu'à 10 minutes, après
quoi elle commence à suffoquer.

Placer le havresac dans un espace extradimensionnel créé par un sac sans
fond, un Puis portatif ou un objet similaire détruit instantanément les
deux objets et ouvre un portail vers le plan Astral. Le portail prend
naissance à l'endroit où l'un des objets a été placé dans l'autre.
Toute créature se trouvant à moins de 3 mètres du portail est aspirée à
travers celui-ci et déposée à un endroit aléatoire du plan astral. Le
Portail se referme ensuite. Le portail est à sens unique et ne peut pas
être rouvert.



### Chapeau de déguisement

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ce chapeau, vous pouvez utiliser une action pour
lancer le sort de *déguisement* à partir de celui-ci,
à volonté. Le sort prend fin si le chapeau est retiré.



### Bandeau d'intelligence

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Votre score d'Intelligence est de 19 lorsque vous portez ce bandeau. Il
n'a aucun effet sur vous si votre Intelligence est déjà de 19 ou plus.



### Heaume scintillant

*Objet merveilleux, très rare (nécessite d'être accordé).*

Ce casque éblouissant est serti de 1d10 diamants, 2d10 rubis, 3d10
opales de feu et 4d10 opales. Toute gemme arrachée du casque se réduit
en poussière. Lorsque toutes les gemmes sont retirées ou détruites, le
casque perd sa magie.

Vous obtenez les avantages suivants lorsque vous le portez :

-   Vous pouvez utiliser une action pour lancer l'un des sorts suivants
    (sauvegarde DC 18), en utilisant l'une des gemmes du casque du type
    spécifié comme composante : lumière *du jour*,
    *boule de feu*, *jet
    prismatique* ou *mur de
    feu*. La gemme est détruite lorsque le sort
    est lancé et disparaît du casque.
-   Tant qu'il possède au moins un diamant, le casque émet une lumière
    chétive dans un rayon de 30 pieds lorsqu'au moins un mort-vivant se
    trouve dans cette zone. Tout mort-vivant qui commence son tour dans
    cette zone subit 1d6 points de dégâts radiants.
-   Tant que le casque comporte au moins un rubis, vous avez une
    résistance aux dégâts du feu.
-   Tant que le casque possède au moins une opale de feu, vous pouvez
    utiliser une action et prononcer un mot d'Injonction pour provoquer
    l'embrasement d'une arme que vous tenez. Les flammes émettent une
    lumière vive dans un rayon de 3 mètres et une lumière chétive sur 3
    mètres supplémentaires. Les flammes sont inoffensives pour vous et
    l'arme. Lorsque vous touchez avec une attaque utilisant l'arme
    enflammée, la cible subit 1d6 points de dégâts de feu
    supplémentaires. Les flammes durent jusqu'à ce que vous utilisiez
    une action bonus pour prononcer à nouveau le mot d'Injonction ou
    jusqu'à ce que vous lâchiez ou rangiez l'arme.

Lancez un d20 si vous portez le casque et subissez des dégâts de feu
après avoir raté un jet de sauvegarde contre un sort. Sur un résultat de
1, le casque émet des faisceaux de lumière à partir des gemmes
restantes. Chaque créature située à moins de 60 pieds du casque, à
l'exception de vous, doit réussir un jet de sauvegarde de Dextérité DC
17 ou être frappée par un rayon, subissant des dégâts radiants égaux au
nombre de gemmes du casque. Le casque et ses gemmes sont ensuite
détruits.



### Heaume de compréhension des langues

*Objet merveilleux, peu commun*

Lorsque vous portez ce heaume, vous pouvez utiliser une action pour
lancer le sort *Compréhension des langues* à
volonté.



### Heaume de télépathie

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ce casque, vous pouvez utiliser une action pour
lancer le sort *détection des pensées* (sauvegarde
DC 13) à partir de celui-ci. Tant que vous restez concentré sur le sort,
vous pouvez utiliser une action bonus pour envoyer un message
télépathique à une créature sur laquelle vous êtes concentré. Elle peut
répondre - en utilisant une action bonus pour le faire - tant que votre
concentration sur elle se poursuit.

Lorsque vous vous concentrez sur une créature avec *Détection des
pensées*, vous pouvez utiliser une action pour lancer
le sort *suggestion* du heaume sur
cette créature. Une fois utilisée, la propriété de
*suggestion* ne peut plus être utilisée jusqu'à l'aube
suivante.



### Heaume de téléportation

*Objet merveilleux, rare (à accorder)*

Ce casque possède 3 charges. Lorsque vous le portez, vous pouvez
utiliser une action et dépenser 1 charge pour lancer le sort de
*téléportation* depuis ce casque. Le casque récupère 1d3
charges dépensées chaque jour à l'aube.



### Vengeresse sacrée

*Arme (toute épée), légendaire (nécessite d'être accordée par un
paladin)*

Vous gagnez un bonus de +3 aux jets d'attaque et de dégâts effectués
avec cette arme magique. Lorsque vous touchez un fiélon ou un
mort-vivant avec cette arme, cette créature subit 2d10 points de dégâts
radiants supplémentaires.

Lorsque vous tenez l'épée dégainée, elle crée une aura dans un rayon de
3 mètres autour de vous. Vous et toutes les créatures amies de vous dans
l'aura avez un avantage aux jets de sauvegarde contre les sorts et
autres effets magiques. Si vous avez 17 niveaux ou plus dans la classe
paladin, le rayon de l'aura augmente à 30 pieds.



### Cor de dévastation

*Objet merveilleux, rare*

Vous pouvez utiliser une action pour prononcer le mot de commande de la
corne, puis souffler dans la corne, qui émet un coup de tonnerre dans un
cône de 30 pieds, audible à 600 pieds. Chaque créature dans le cône doit
effectuer un jet de sauvegarde de Constitution DC 15. En cas d'échec,
la créature subit 5d6 points de dégâts de tonnerre et est assourdie
pendant 1 minute. En cas de sauvegarde réussie, la créature subit la
moitié des dégâts et n'est pas assourdie. Les créatures et objets en
verre ou en cristal ont un désavantage au jet de sauvegarde et subissent
10d6 points de dégâts de tonnerre au lieu de 5d6.

Chaque utilisation de la magie du cor a 20 % de chances de provoquer
l'explosion du cor. L'explosion inflige 10d6 points de dégâts de feu
au souffleur et détruit la corne.



### Cor du Valhalla

*Objet merveilleux, rare (argent ou laiton), très rare (bronze) ou
légendaire (fer).*

Vous pouvez utiliser une action pour souffler dans cette corne. En
réponse, des esprits guerriers du Valhalla apparaissent dans un rayon de
60 pieds autour de vous. Ils utilisent les statistiques d'un Berserker.
Ils retournent au Valhalla après 1 heure ou lorsqu'ils sont à zéro
point de vie. Une fois que vous avez utilisé la corne, elle ne peut pas
être utilisée à nouveau avant 7 jours.

Il existe quatre types de cor du Valhalla, chacun fabriqué dans un métal
différent. Le type de la corne détermine le nombre de berserkers qui
répondent à son appel, ainsi que les conditions d'utilisation. Le MJ
choisit le type de corne ou le détermine au hasard.

  ------------------------------------------------------------------
   d100   Type de   Berserkers  Exigence
           corne     invoqués   
  ------- -------- ------------ ------------------------------------
   01-40   Argent    2d4 + 2    Aucun

   41-75   Laiton    3d4 + 3    Maîtrise de toutes les armes
                                courantes.

   76-90   Bronze    4d4 + 4    Maîtrise de toutes les armures
                                moyennes.

   91-00    Fer      5d4 + 5    Maîtrise de toutes les armes de
                                guerre.
  ------------------------------------------------------------------

Si vous soufflez dans le cor sans remplir les conditions requises, les
berserkers invoqués vous attaquent. Si vous remplissez la condition, ils
sont amicaux envers vous et vos compagnons et suivent vos ordres.



### Fers de rapidité

*Objet merveilleux, rare*

Ces fers à cheval en fer se présentent sous la forme d'un jeu de
quatre. Lorsque les quatre fers sont fixés aux sabots d'un cheval ou
d'une créature similaire, ils augmentent la vitesse de marche de la
créature de 30 pieds.



### Fers de zéphyr

*Objet merveilleux, très rare*

Ces fers à cheval en fer sont livrés dans un ensemble de quatre. Lorsque
les quatre fers sont fixés aux sabots d'un cheval ou d'une créature
similaire, ils permettent à la créature de se déplacer normalement tout
en flottant à 10 cm au-dessus du sol. Cet effet signifie que la créature
peut traverser ou se tenir au-dessus de surfaces non solides ou
instables, comme l'eau ou la lave. La créature ne laisse aucune trace
et ignore les terrains difficiles. En outre, la créature peut se
déplacer à vitesse normale jusqu'à 12 heures par jour sans subir
l'épuisement d'une marche forcée.



### Sceptre inamovible

*Rod, peu commun*

Cette tige de fer plat possède un bouton à une extrémité. Vous pouvez
utiliser une action pour appuyer sur le bouton, ce qui a pour effet de
fixer magiquement la tige en place. Jusqu'à ce que vous ou une autre
créature utilisiez une action pour appuyer à nouveau sur le bouton, la
tige ne bouge pas, même si elle défie la gravité. La tige peut supporter
jusqu'à 2 000 kg. Si le poids est supérieur, la tige se désactive et
tombe. Une créature peut utiliser une action pour faire un test de Force
DC 30, déplaçant la tige fixe jusqu'à 10 pieds en cas de succès.



### Forteresse instantanée

*Objet merveilleux, rare*

Vous pouvez utiliser une action pour placer ce cube de métal de 1 pouce
sur le sol et prononcer son mot d'Injonction. Le cube se transforme
rapidement en une forteresse qui reste jusqu'à ce que vous utilisiez
une action pour prononcer le mot de commande qui la démantèle, ce qui ne
fonctionne que si la forteresse est vide.

La forteresse est une tour carrée, de 20 pieds de côté et de 30 pieds de
haut, avec des fentes pour les flèches sur tous les côtés et un créneau
au sommet. Son intérieur est divisé en deux étages, avec une échelle qui
court le long d'un mur pour les relier. L'échelle se termine par une
trappe menant au toit. Lorsqu'elle est activée, la tour comporte une
petite porte sur le côté qui vous fait face. La porte ne s'ouvre que
sur votre ordre, que vous pouvez prononcer comme action bonus. Elle est
immunisée contre le sort de *déblocage* et les magies
similaires, comme celle d'un *carillon* d'*ouverture*.

Chaque créature dans la zone où la forteresse apparaît doit effectuer un
jet de sauvegarde de Dextérité DC 15, subissant 10d10 points de dégâts
de contondant en cas d'échec, ou la moitié des dégâts en cas de
réussite. Dans les deux cas, la créature est poussée vers un espace
inoccupé à l'extérieur mais à côté de la forteresse. Les objets dans la
zone qui ne sont pas portés ou transportés subissent ces dégâts et sont
poussés automatiquement.

La tour est faite d'adamantine, et sa magie l'empêche de se renverser.
Le toit, la porte et les murs ont chacun 100 points de vie, une immunité
aux dégâts des armes non magiques, à l'exception des armes de siège, et
une résistance à tous les autres dégâts. Seul un sort de
*Souhait* peut réparer la forteresse (cette utilisation du sort
compte comme la reproduction d'un sort de 8e niveau ou moins). Chaque
lancement de *Souhait* fait regagner 50 points de vie au toit,
à la porte ou à un mur.



### Pierre de Ioun

*Objet merveilleux, rareté variable (nécessite d'être accordé).*

Il existe de nombreux types de *pierre de Ioun*, chaque type étant une
combinaison distincte de forme et de couleur.

Lorsque vous utilisez une action pour lancer une de ces pierres en
l'air, la pierre tourne autour de votre tête à une distance de 1d3
pieds et vous confère un avantage. Par la suite, une autre créature doit
utiliser une action pour saisir ou attraper la pierre pour la séparer de
vous, soit en réussissant un jet d'attaque contre AC 24, soit en
réussissant un test de Dextérité DC 24 (Acrobaties). Vous pouvez
utiliser une action pour saisir et ranger la pierre, mettant ainsi fin à
son effet.

Une pierre a une CA de 24, 10 points de vie, et une résistance à tous
les dégâts. Elle est considérée comme un objet porté lorsqu'elle orbite
autour de votre tête.

***Absorption (très rare).*** Pendant que cet ellipsoïde lavande pâle
tourne autour de votre tête, vous pouvez utiliser votre réaction pour
annuler un sort de 4e niveau ou moins lancé par une créature que vous
pouvez voir et ne visant que vous.

Une fois que la pierre a annulé 20 niveaux de sorts, elle se consume et
devient gris terne, perdant sa magie. Si vous êtes visé par un sort dont
le niveau est supérieur au nombre de niveaux de sorts qu'il reste à la
pierre, celle-ci ne peut pas l'annuler.

***Agilité (très rare).*** Votre score de Dextérité augmente de 2,
jusqu'à un maximum de 20, tandis que cette sphère rouge profond tourne
autour de votre tête.

***Conscience (Rare).*** Vous ne pouvez pas être surpris lorsque ce
rhomboïde bleu foncé tourne autour de votre tête.

***Fortitude (Très Rare).*** Votre score de Constitution augmente de 2,
jusqu'à un maximum de 20, pendant que ce rhomboïde rose tourne autour
de votre tête.

***Grande Absorption (Légendaire).*** Pendant que cet ellipsoïde marbré
lavande et vert tourne autour de votre tête, vous pouvez utiliser votre
réaction pour annuler un sort de 8e niveau ou moins lancé par une
créature que vous pouvez voir et ne visant que vous.

Une fois que la pierre a annulé 50 niveaux de sorts, elle se consume et
devient gris terne, perdant sa magie. Si vous êtes visé par un sort dont
le niveau est supérieur au nombre de niveaux de sorts qu'il reste à la
pierre, celle-ci ne peut pas l'annuler.

***Intuition (très rare).*** Votre score de Sagesse augmente de 2,
jusqu'à un maximum de 20, tandis que cette sphère bleue incandescente
tourne autour de votre tête.

***Intellect (très rare).*** Votre score d'Intelligence augmente de 2,
jusqu'à un maximum de 20, tandis que cette sphère marbrée écarlate et
bleue tourne autour de votre tête.

***Leadership (très rare).*** Votre score de Charisme augmente de 2,
jusqu'à un maximum de 20, tandis que cette sphère marbrée rose et verte
tourne autour de votre tête.

***Maîtrise (Légendaire).*** Votre bonus de maîtrise augmente de 1
pendant que ce prisme vert pâle tourne autour de votre tête.

***Protection (Rare).*** Vous gagnez un bonus de +1 à la CA tant que ce
prisme rose poussiéreux tourne autour de votre tête.

***Régénération (Légendaire).*** Vous regagnez 15 points de vie à la fin
de chaque heure où ce fuseau blanc nacré tourne autour de votre tête, à
condition que vous ayez au moins 1 point de vie.

***Réserve (Rare).*** Ce prisme violet vibrant stocke les sorts qui y
sont lancés, les conservant jusqu'à ce que vous les utilisiez. La
pierre peut stocker jusqu'à 3 niveaux de sorts à la fois. Lorsqu'elle
est trouvée, elle contient 1d4 - 1 niveaux de sorts stockés choisis par
le MJ.

Toute créature peut lancer un sort du 1er au 3e niveau dans la pierre en
la touchant au moment où le sort est lancé. Le sort n'a aucun effet, si
ce n'est d'être stocké dans la pierre. Si la pierre ne peut pas
contenir le sort, celui-ci est dépensé sans effet. Le niveau de
l'emplacement utilisé pour lancer le sort détermine la place qu'il
occupe.

Pendant que cette pierre tourne autour de votre tête, vous pouvez lancer
n'importe quel sort qui s'y trouve. Le sort utilise le niveau de
l'emplacement, la sauvegarde contre les sorts, le bonus d'attaque des
sorts et la capacité d'incantation du lanceur d'origine, mais il est
traité comme si vous aviez lancé le sort. Le sort lancé à partir de la
pierre n'est plus stocké dans celle-ci, libérant ainsi de l'espace.
Force (Très Rare). Votre score de Force augmente de 2, jusqu'à un
maximum de 20, tandis que ce rhomboïde bleu pâle tourne autour de votre
tête.

***Sustenance (Rare).*** Vous n'avez pas besoin de manger ou de boire
pendant que ce fuseau clair tourne autour de votre tête.



### Bandes de fer de liaison

*Objet merveilleux, rare*

Cette sphère en fer rouillé mesure 3 pouces de diamètre et pèse 1 livre.
Vous pouvez utiliser une action pour prononcer le mot d'injonction et
lancer la sphère sur une créature Très grande ou plus petite que vous
pouvez voir dans un rayon de 60 pieds autour de vous. Lorsque la sphère
se déplace dans l'air, elle s'ouvre en un enchevêtrement de bandes
métalliques.

Effectuez un jet d'attaque à distance avec un bonus d'attaque égal à
votre modificateur de Dextérité plus votre bonus de maîtrise. En cas de
succès, la cible est entravée jusqu'à ce que vous preniez une action
bonus pour prononcer à nouveau le mot de commande afin de la libérer. Si
vous faites cela, ou si vous ratez l'attaque, les bandes se contractent
et redeviennent une sphère.

Une créature, y compris celle qui est entravée, peut utiliser une action
pour faire un test de Force DC 20 pour briser les bandes de fer. En cas
de réussite, l'objet est détruit et la créature entravée est libérée.
En cas d'échec, toute nouvelle tentative de cette créature échoue
automatiquement jusqu'à ce que 24 heures se soient écoulées.

Une fois que les bandes sont utilisées, elles ne peuvent plus l'être
jusqu'à l'aube suivante.



### Flasque de fer

*Objet merveilleux, légendaire*

Cette bouteille en fer a un bouchon en laiton. Vous pouvez utiliser une
action pour prononcer le mot d'Injonction de la flasque, en ciblant une
créature que vous pouvez voir dans un rayon de 60 pieds autour de vous.
Si la cible est originaire d'un autre plan d'existence que celui où
vous vous trouvez, elle doit réussir un jet de sauvegarde de Sagesse DC
17 ou être piégée dans le flacon. Si la cible a déjà été piégée par la
fiole, elle a un avantage au jet de sauvegarde. Une fois piégée, une
créature reste dans la fiole jusqu'à ce qu'elle soit libérée. La fiole
ne peut contenir qu'une seule créature à la fois. Une créature piégée
dans la fiole n'a pas besoin de respirer, de manger ou de boire et ne
vieillit pas.

Vous pouvez utiliser une action pour retirer le bouchon de la flasque et
libérer la créature qu'elle contient. La créature est amicale envers
vous et vos compagnons pendant 1 heure et obéit à vos ordres pendant
cette durée. Si vous ne lui donnez aucun ordre ou si vous lui donnez un
ordre qui risque d'entraîner sa mort, elle se défend mais ne fait rien
d'autre. À la fin de la durée, la créature agit conformément à sa
disposition et à son alignement normaux.

Un sort d'*identification* révèle qu'une créature se
trouve à l'intérieur de la fiole, mais le seul moyen de déterminer le
type de créature est d'ouvrir la fiole. Une bouteille nouvellement
découverte peut déjà contenir une créature choisie par le MJ ou
déterminée au hasard.

  -------------------------
   d100   Contenu
  ------- -----------------
   01-50  Vide

   51-54  Démon (type 1)

   55-58  Démon (type 2)

   59-62  Démon (type 3)

   63-64  Démon (type 4)

    65    Démon (type 5)

    66    Démon (type 6)

    67    Déva

   68-69  Diable (plus
          grand)

   70-73  Diable (moindre)

   74-75  Djinn

   76-77  Éfrites

   78-83  Élémentaire
          (tous)

   84-86  Traqueur
          invisible

   87-90  Guenaude nocturne

    91    Planétar

   92-95  Salamandre

    96    Solar

   97-99  Succube/incube

    100   Xorn
  -------------------------



### Javeline de foudre

*Arme (javeline), peu commune*

Ce javelot est une arme magique. Lorsque vous le lancez et prononcez son
mot d'Injonction, il se transforme en un éclair, formant une ligne de
1,5 m de large qui s'étend de vous à une cible située dans un rayon de
120 pieds. Chaque créature dans la ligne, à l'exception de vous et de
la cible, doit effectuer un jet de sauvegarde de Dextérité DC 13,
subissant 4d6 points de dégâts de foudre en cas d'échec, et la moitié
des dégâts en cas de réussite. L'éclair se retransforme en javeline
lorsqu'il atteint la cible. Effectuer une attaque avec une arme à
distance contre la cible. En cas de succès, la cible subit les dégâts du
javelot plus 4d6 points de dégâts de foudre.

La propriété du javelot ne peut pas être réutilisée avant l'aube
suivante. Dans l'intervalle, le javelot peut toujours être utilisé
comme une arme magique.



### Lanterne de révélation

*Objet merveilleux, peu commun*

Lorsqu'elle est allumée, cette lanterne à capuchon brûle pendant 6
heures avec 1 pinte d'huile, diffusant une lumière vive dans un rayon
de 30 pieds et une lumière chétive sur 30 pieds supplémentaires. Les
créatures et objets invisibles sont visibles tant qu'ils se trouvent
dans la lumière vive de la lanterne. Vous pouvez utiliser une action
pour abaisser le capuchon, réduisant la lumière à une lumière tamisée
dans un rayon de 5 pieds.



### Lame porte-chance

*Arme (n'importe quelle épée), légendaire (nécessite d'être accordé)*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique. Tant que l'épée est sur vous, vous bénéficiez
également d'un bonus de +1 aux jets de sauvegarde.

***Chance.*** Si l'épée est sur vous, vous pouvez faire appel à sa
chance (aucune action requise) pour relancer un jet d'attaque, un test
de capacité ou un jet de sauvegarde que vous n'aimez pas. Vous devez
utiliser le deuxième jet. Cette propriété ne peut pas être utilisée à
nouveau avant l'aube suivante.

***Souhait.*** L'épée possède 1d4 - 1 charges. Lorsque vous la tenez,
vous pouvez utiliser une action pour dépenser 1 charge et lancer le sort
*Souhait* à partir de celle-ci. Cette propriété ne peut pas
être utilisée à nouveau avant la prochaine aube. L'épée perd cette
propriété si elle n'a plus de charges.



### Masse d'armes d'anéantissement

*Arme (masse), rare (requiert une harmonisation)*

Lorsque vous touchez un fiélon ou un mort-vivant avec cette arme
magique, cette créature subit 2d6 dégâts radiants supplémentaires. Si la
cible a 25 points de vie ou moins après avoir subi ces dégâts, elle doit
réussir un jet de sauvegarde de Sagesse DC 15 ou être détruite. En cas
de sauvegarde réussie, la créature est effrayée par vous jusqu'à la fin
de votre prochain tour.

Lorsque vous tenez cette arme, elle diffuse une lumière vive dans un
rayon de 20 pieds et une lumière chétive sur 20 pieds supplémentaires.



### Masse destructrice

*Arme (masse), rare*

Vous gagnez un bonus de +1 aux jets d'attaque et de dégâts effectués
avec cette arme magique. Ce bonus passe à +3 lorsque vous utilisez la
masse pour attaquer une construction.

Lorsque vous obtenez un 20 à un jet d'attaque effectué avec cette arme,
la cible subit 2d6 points de dégâts de matraquage supplémentaires, ou
4d6 points de dégâts de matraquage si c'est une construction. Si une
construction a 25 points de vie ou moins après avoir subi ces dégâts,
elle est détruite.



### Masse d'armes terrifiante

*Arme (masse), rare (requiert une harmonisation)*

Cette arme magique possède 3 charges. Lorsque vous la tenez, vous pouvez
utiliser une action et dépenser 1 charge pour libérer une vague de
terreur. Chaque créature de votre choix dans un rayon de 30 pieds
s'étendant depuis vous doit réussir un jet de sauvegarde de Sagesse DC
15 ou être effrayée par vous pendant 1 minute. Tant qu'elle est
effrayée de cette manière, une créature doit passer ses tours à essayer
de s'éloigner de vous autant qu'elle le peut, et elle ne peut pas se
déplacer volontairement vers un espace situé à moins de 30 pieds de
vous. Elle ne peut pas non plus avoir de réactions. Pour son action,
elle ne peut utiliser que l'action Foncer ou essayer d'échapper à un
effet qui l'empêche de se déplacer. Si elle n'a aucun endroit où elle
peut se déplacer, la créature peut utiliser l'action Esquive. À la fin
de chacun de ses tours, une créature peut répéter le jet de sauvegarde,
mettant fin à l'effet sur elle-même en cas de réussite.

La masse d'armes récupère 1d3 charges dépensées par jour à l'aube.



### Manteau de résistance aux sorts

*Objet merveilleux, rare (à accorder)*

Vous avez un avantage sur les jets de sauvegarde contre les sorts
lorsque vous portez cette cape.



### Manuel de vitalité

*Objet merveilleux, très rare*

Ce livre contient des conseils de santé et de régime, et ses mots sont
chargés de magie. Si vous passez 48 heures sur une période de 6 jours ou
moins à étudier le contenu du livre et à mettre en pratique ses
directives, votre score de Constitution augmente de 2, ainsi que votre
maximum pour ce score. Le manuel perd ensuite sa magie, mais la regagne
dans un siècle.



### Manuel d'exercices physiques

*Objet merveilleux, très rare*

Ce livre décrit des exercices de fitness, et ses mots sont chargés de
magie. Si vous passez 48 heures sur une période de 6 jours ou moins à
étudier le contenu du livre et à mettre en pratique ses directives,
votre score de Force augmente de 2, ainsi que votre maximum pour ce
score. Le manuel perd ensuite sa magie, mais la retrouve dans un siècle.



### Manuel des golems

*Objet merveilleux, très rare*

Ce tome contient les informations et les incantations nécessaires à la
fabrication d'un type particulier de golem. Le MJ choisit le type ou le
détermine aléatoirement. Pour déchiffrer et utiliser le manuel, vous
devez être un lanceur de sorts disposant d'au moins deux emplacements
de sorts de 5e niveau. Une créature qui ne peut pas utiliser un manuel
des golems et qui tente de le lire subit 6d6 dégâts psychiques.

  -------------------------------------
    d20    Golem   Temps     Coût
  ------- -------- --------- ----------
    1-5    Argile  30 jours  65 000 gp

   6-17    Chair   60 jours  50 000 gp

    18      Fer    120 jours 100 000 gp

   19-20   Pierre  90 jours  80 000 gp
  -------------------------------------

Pour créer un golem, vous devez consacrer le temps indiqué dans le
tableau, en travaillant sans interruption avec le manuel à portée de
main et en ne vous reposant pas plus de 8 heures par jour. Vous devez
également payer le coût indiqué pour acheter des fournitures.

Une fois que vous avez fini de créer le golem, le livre est consumé par
les flammes eldritch. Le golem s'anime lorsque les cendres du manuel
sont saupoudrées sur lui. Il est sous votre contrôle, et il comprend et
obéit à vos ordres oraux.



### Manuel de vivacité d'action

*Objet merveilleux, très rare*

Ce livre contient des exercices de coordination et d'équilibre, et ses
mots sont chargés de magie. Si vous passez 48 heures sur une période de
6 jours ou moins à étudier le contenu du livre et à mettre en pratique
ses directives, votre score de Dextérité augmente de 2, ainsi que votre
maximum pour ce score. Le manuel perd alors sa magie, mais la regagne
dans un siècle.



### Pigments merveilleux

*Objet merveilleux, très rare*

Généralement trouvés dans 1d4 pots à l'intérieur d'une fine boîte en
bois avec un pinceau (pesant 1 livre au total), ces pigments vous
permettent de créer des objets tridimensionnels en les peignant en deux
dimensions. La peinture s'écoule du pinceau pour former l'objet désiré
lorsque vous vous concentrez sur son image.

Chaque pot de peinture suffit à couvrir une surface de 1 000 pieds
carrés, ce qui vous permet de créer des objets inanimés ou des éléments
de terrain - tels qu'une porte, une fosse, des fleurs, des arbres, des
cellules, des pièces ou des armes - dont la taille peut atteindre 10 000
pieds cubes. Il faut 10 minutes pour couvrir 100 pieds carrés.

Lorsque vous terminez la peinture, l'objet ou l'élément de terrain
représenté devient un objet réel, non magique. Ainsi, peindre une porte
sur un mur crée une porte réelle qui peut être ouverte vers ce qui se
trouve au-delà. Peindre une fosse sur un sol crée une vraie fosse, et sa
profondeur compte dans la surface totale des objets que vous créez.

Aucun objet créé par les pigments ne peut avoir une valeur supérieure à
25 gp. Si vous peignez un objet de plus grande valeur (tel qu'un
diamant ou un tas d'or), l'objet semble authentique, mais un examen
attentif révèle qu'il est fait de pâte, d'os ou d'un autre matériau
sans valeur.

Si vous peignez une forme d'énergie telle que le feu ou la foudre,
l'énergie apparaît mais se dissipe dès que vous avez terminé la
peinture, sans causer de dommages à quoi que ce soit.



### Médaillon des pensées

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Le médaillon possède 3 charges. Lorsque vous le portez, vous pouvez
utiliser une action et dépenser 1 charge pour lancer le sort *Détection
des pensées* à partir de celui-ci.
Le médaillon récupère 1d3 charges dépensées chaque jour à l'aube.



### Miroir d'emprisonnement

*Objet merveilleux, très rare*

Lorsque ce miroir d'1,20 m de haut est regardé indirectement, sa
surface affiche de faibles images de créatures. Le miroir pèse 15 kg, et
il a une CA de 11, 10 points de vie, et une vulnérabilité aux dégâts de
matraquage. Il se brise et est détruit lorsqu'il est réduit à 0 point
de vie.

Si le miroir est suspendu à une surface verticale et que vous vous
trouvez à moins de 1,5 mètre de lui, vous pouvez utiliser une action
pour prononcer son mot de commande et l'activer. Il reste activé
jusqu'à ce que vous utilisiez à nouveau une action pour prononcer le
mot de commande.

Toute créature autre que vous qui voit son reflet dans le miroir activé
alors qu'elle se trouve à moins de 30 pieds de celui-ci doit réussir un
jet de sauvegarde de Charisme DC 15 ou être piégée, ainsi que tout ce
qu'elle porte, dans l'une des douze cellules extradimensionnelles du
miroir. Ce jet de sauvegarde est effectué avec avantage si la créature
connaît la nature du miroir, et les constructions réussissent le jet de
sauvegarde automatiquement.

Une cellule extradimensionnelle est une étendue infinie remplie d'un
épais brouillard qui réduit la visibilité à 3 mètres. Les créatures
piégées dans les cellules du miroir ne vieillissent pas, et elles n'ont
pas besoin de manger, de boire ou de dormir. Une créature piégée dans
une cellule peut s'échapper en utilisant la magie qui permet le voyage
planaire. Sinon, la créature est confinée dans la cellule jusqu'à sa
libération.

Si le miroir piège une créature mais que ses douze cellules
extradimensionnelles sont déjà occupées, le miroir libère une créature
piégée au hasard pour accueillir le nouveau prisonnier. Une créature
libérée apparaît dans un espace inoccupé à portée de vue du miroir mais
orienté dans le sens opposé. Si le miroir est fracassé, toutes les
créatures qu'il contient sont libérées et apparaissent dans des espaces
inoccupés à proximité.

Tant que vous êtes à moins de 1,5 mètre du miroir, vous pouvez utiliser
une action pour prononcer le nom d'une créature qui y est piégée ou
appeler une cellule particulière par son numéro. La créature nommée ou
contenue dans la cellule nommée apparaît comme une image sur la surface
du miroir. Vous et la créature pouvez alors communiquer normalement.

De la même manière, vous pouvez utiliser une action pour prononcer un
second mot d'injonction et libérer une créature piégée dans le miroir.
La créature libérée apparaît, ainsi que ses possessions, dans l'espace
inoccupé le plus proche du miroir et faisant face à celui-ci.



### Armure de mithral

*Armure (moyenne ou lourde, mais pas en cuir), peu commune*

Le mithral est un métal léger et flexible. Une chemise ou une cuirasse
en mithral peut être portée sous des vêtements normaux. Si l'armure
impose normalement un désavantage sur les tests de Dextérité
(Discrétion) ou a une exigence de Force, la version mithral de l'armure
ne le fait pas.



### Collier d'adaptation

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ce collier, vous pouvez respirer normalement dans
n'importe quel environnement, et vous avez un avantage sur les jets de
sauvegarde effectués contre les gaz et les vapeurs nocifs (tels que les
effets du Nuage *mortelle* et du Nuage
*nauséabond*, les poisons inhalés, et les armes à
souffle de certains dragons).



### Collier de boules de feu

*Objet merveilleux, rare*

Ce collier a 1d6 + 3 perles qui y sont suspendues. Vous pouvez utiliser
une action pour détacher une perle et la lancer jusqu'à 60 pieds de
distance. Lorsqu'elle atteint la fin de sa trajectoire, la perle
explose comme un sort de *boule de feu de* 3e niveau
(sauvegarde DC 15).

Vous pouvez lancer plusieurs perles, ou même le collier entier, en une
seule action. Lorsque vous faites cela, augmentez le niveau de la
*boule de feu* de 1 pour chaque perle au-delà de la
première.



### Collier de perles de prière

*Objet merveilleux, rare (doit être accordé par un clerc, un druide ou
un paladin).*

Ce collier comporte 1d4 + 2 perles magiques faites d'aigue-marine, de
perle noire ou de topaze. Il possède également de nombreuses perles non
magiques faites de pierres telles que l'ambre, la pierre de sang, la
citrine, le corail, le jade, la perle ou le quartz. Si une perle magique
est retirée du collier, cette perle perd sa magie.

Il existe six types de perles magiques. Le MJ décide du type de chaque
perle du collier ou le détermine au hasard. Un collier peut avoir plus
d'une perle du même type. Pour en utiliser une, vous devez porter le
collier. Chaque perle contient un sort que vous pouvez lancer à partir
d'elle comme action bonus (en utilisant votre DC de sauvegarde contre
les sorts si une sauvegarde est nécessaire). Une fois que le sort d'une
perle magique est lancé, cette perle ne peut plus être utilisée jusqu'à
l'aube suivante.

  -----------------------------------------------------------------------
    d20   Perle de \...  Sort
  ------- -------------- ------------------------------------------------
    1-6   Bénédiction    *Bénédiction*

   7-12   Durcissement   *Soins* ou
                         *restauration partielle*

   13-16  Favoriser      *Restauration
                         supérieure*

   17-18  Smiting        Une punition qui marque une cible avec des
                         dégâts radiants invisibles.

    19    Sommation      *Allié planaire*

    20    Marche sur le  *Marche*
          vent           
  -----------------------------------------------------------------------



### Voleuse des neuf vies

*Arme (n'importe quelle épée), très rare (requiert une syntonisation)*

Vous gagnez un bonus de +2 aux jets d'attaque et de dégâts effectués
avec cette arme magique.

L'épée possède 1d8 + 1 charges. Si vous obtenez un coup critique contre
une créature qui a moins de 100 points de vie, elle doit réussir un jet
de sauvegarde de Constitution DC 15 ou être tuée instantanément car
l'épée lui arrache sa force vitale (une construction ou un mort-vivant
est immunisé). L'épée perd 1 charge si la créature est tuée. Lorsque
l'épée n'a plus de charge, elle perd cette propriété.



### Arc du serment

*Arme (arc long), très rare (nécessite d'être accordé)*

Lorsque tu encoches une flèche sur cet arc, il murmure en elfe : \"Mort
rapide à mes ennemis\". Lorsque vous utilisez cette arme pour effectuer
une attaque à distance, vous pouvez, comme phrase d'Injonction, dire :
\"Mort rapide à vous qui m'avez fait du tort\". La cible de votre
attaque devient votre ennemi juré jusqu'à sa mort ou jusqu'à l'aube
sept jours plus tard. Vous ne pouvez avoir qu'un seul ennemi juré de ce
type à la fois. Lorsque votre ennemi juré meurt, vous pouvez en choisir
un nouveau après l'aube suivante.

Lorsque vous effectuez un jet d'attaque à distance avec cette arme
contre votre ennemi juré, vous avez un avantage sur le jet. De plus,
votre cible ne bénéficie d'aucun avantage lié à l'abri, autre que
l'abri total, et vous ne subissez aucun désavantage lié à la longue
portée. Si l'attaque touche, votre ennemi juré subit 3d6 points de
dégâts perforants supplémentaires.

Tant que votre ennemi juré vit, vous avez un désavantage sur les jets
d'attaque avec toutes les autres armes.



### Huile éthérée

*Potion, rare*

Des perles de cette huile grise et nuageuse se forment à l'extérieur de
son récipient et s'évaporent rapidement. L'huile peut couvrir une
créature de taille moyenne ou plus petite, ainsi que l'équipement
qu'elle porte (une fiole supplémentaire est nécessaire pour chaque
catégorie de taille supérieure à moyenne). L'application de l'huile
prend 10 minutes. La créature affectée bénéficie alors de l'effet du
sort Forme *éthérée* pendant 1 heure.



### Huile d'affûtage

*Potion, très rare*

Cette huile claire et gélatineuse brille de très petits éclats d'argent
ultrafins. L'huile peut recouvrir une arme tranchante ou perforante ou
jusqu'à 5 munitions tranchantes ou perforantes. L'application de
l'huile prend 1 minute. Pendant 1 heure, l'objet enduit est magique et
bénéficie d'un bonus de +3 aux jets d'attaque et de dégâts.



### Huile d'insaisissabilité

*Potion, peu commune*

Cet onguent noir collant est épais et lourd dans le récipient, mais il
coule rapidement lorsqu'on le verse. L'huile peut couvrir une créature
de taille moyenne ou plus petite, ainsi que l'équipement qu'elle porte
(une fiole supplémentaire est nécessaire pour chaque catégorie de taille
supérieure à moyenne). L'application de l'huile prend 10 minutes. La
créature affectée bénéficie alors de l'effet d'un sort de *liberté de
mouvement* pendant 8 heures.

Alternativement, l'huile peut être versée sur le sol comme une action,
où elle couvre un carré de 3 mètres de côté, dupliquant l'effet du sort
*Graisse* dans cette zone pendant 8 heures.



### Perle de pouvoir

*Objet merveilleux, peu commun (nécessite d'être lié à un lanceur de
sorts).*

Tant que cette perle est sur vous, vous pouvez utiliser une action pour
prononcer son mot de commande et regagner un emplacement de sort
dépensé. Si le slot dépensé était de 4ème niveau ou plus, le nouveau
slot est de 3ème niveau. Une fois que vous avez utilisé la perle, elle
ne peut pas être utilisée à nouveau avant la prochaine aube.



### Amulette de santé

*Objet merveilleux, peu commun*

Vous êtes immunisé contre toute maladie pendant que vous portez ce
pendentif. Si vous êtes déjà infecté par une maladie, les effets de la
maladie sont supprimés pendant que vous portez le pendentif.



### Amulette antidote contre le poison

*Objet merveilleux, rare*

Cette délicate chaîne en argent est ornée d'un pendentif en forme de
pierre précieuse noire taillée en brillant. Tant que vous le portez, les
poisons n'ont aucun effet sur vous. Vous êtes immunisé contre l'état
empoisonné et vous êtes immunisé contre les dégâts du poison.



### Amulette de cicatrisation

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Tant que vous portez ce pendentif, vous vous stabilisez chaque fois que
vous êtes en train de mourir au début de votre tour. De plus, chaque
fois que vous lancez un dé de réussite pour regagner des points de vie,
vous doublez le nombre de points de vie qu'il restaure.



### Philtre d'amour

*Potion, peu commune*

La prochaine fois que vous voyez une créature dans les 10 minutes après
avoir bu ce philtre, vous êtes charmé par cette créature pendant 1
heure. Si la créature est d'une espèce et d'un sexe qui vous attirent
normalement, vous la considérez comme votre grand amour pendant que vous
êtes charmé. Le liquide effervescent de cette potion, de couleur rose,
contient une bulle facile à manquer, en forme de cœur.



### Flûte obsédante

*Objet merveilleux, peu commun*

Vous devez avoir une bonne maîtrise des instruments à vent pour utiliser
ces tuyaux. Ils ont 3 charges. Vous pouvez utiliser une action pour en
jouer et dépenser 1 charge pour créer une mélodie sinistre et
envoûtante. Chaque créature dans un rayon de 30 pieds de vous qui vous
entend jouer doit réussir un jet de sauvegarde de Sagesse DC 15 ou être
effrayée par vous pendant 1 minute. Si vous le souhaitez, toutes les
créatures de la zone qui ne vous sont pas hostiles réussissent
automatiquement le jet de sauvegarde. Une créature qui échoue au jet de
sauvegarde peut le répéter à la fin de chacun de ses tours, mettant fin
à l'effet sur elle-même en cas de réussite. Une créature qui réussit
son jet de sauvegarde est immunisée contre l'effet de ces tuyaux
pendant 24 heures. Les tuyaux regagnent 1d3 charges dépensées chaque
jour à l'aube.



### Flûte des égouts

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Vous devez maîtriser les instruments à vent pour utiliser ces tuyaux.
Tant que vous êtes lié aux cornemuses, les rats ordinaires et les rats
géants sont indifférents à votre égard et ne vous attaqueront que si
vous les menacez ou leur faites du mal.

Les cornemuses ont 3 charges. Si vous jouez de la cornemuse en tant
qu'action, vous pouvez utiliser une action bonus pour dépenser de 1 à 3
charges, appelant une nuée de rats avec chaque charge dépensée, à
condition qu'il y ait suffisamment de rats dans un rayon d'un
demi-mile de vous pour être appelés de cette manière (comme déterminé
par le MJ). S'il n'y a pas assez de rats pour former un essaim, la
charge est gaspillée. Les essaims appelés se déplacent vers la musique
par le chemin le plus court disponible mais ne sont pas sous votre
contrôle par ailleurs. Les pipes récupèrent 1d3 charges dépensées chaque
jour à l'aube.

Chaque fois qu'un essaim de rats qui n'est pas sous le contrôle d'une
autre créature s'approche à moins de 30 pieds de vous pendant que vous
jouez de la cornemuse, vous pouvez faire un test de Charisme contesté
par le test de Sagesse de l'essaim. Si vous perdez le concours,
l'essaim se comporte comme il le ferait normalement et ne peut pas être
influencé par la musique des cornemuses pendant les 24 heures suivantes.
Si vous gagnez le concours, l'essaim est influencé par la musique des
cornemuses et devient amical envers vous et vos compagnons tant que vous
continuez à jouer des cornemuses à chaque tour comme une action. Un
essaim ami obéit à vos injonctions. Si vous ne donnez aucun ordre à un
essaim ami, il se défend mais ne fait aucune autre action. Si un essaim
ami commence son tour et ne peut pas entendre la musique des cornemuses,
votre contrôle sur cet essaim prend fin, et l'essaim se comporte comme
il le ferait normalement et ne peut pas être influencé par la musique
des cornemuses pendant les 24 heures suivantes.



### Armure harnois éthérée

*Armure (harnois), légendaire (nécessite une harmonisation)*

Tant que vous portez cette armure, vous pouvez prononcer son mot de
commande comme une action pour obtenir l'effet du sort Forme éthérée,
qui dure 10 minutes ou jusqu'à ce que vous retiriez l'armure ou que
vous utilisiez une action pour prononcer à nouveau le mot de commande.
Cette propriété de l'armure ne peut pas être utilisée à nouveau avant
la prochaine aube.



### Puis portatif

*Objet merveilleux, rare*

Cette fine toile noire, douce comme de la soie, est pliée aux dimensions
d'un mouchoir de poche. Il se déplie en une feuille circulaire de 6
pieds de diamètre.

Vous pouvez utiliser une action pour déplier un puits portatif et le
placer sur ou contre une surface solide, après quoi le puits portatif
crée un trou extradimensionnel de 3 mètres de profondeur. L'espace
cylindrique à l'intérieur du trou existe sur un plan différent, il ne
peut donc pas être utilisé pour créer des passages ouverts. Toute
créature à l'intérieur d'un trou portatif ouvert peut sortir du trou
en grimpant à l'extérieur.

Vous pouvez utiliser une action pour fermer un trou portatif en
saisissant les bords du tissu et en le repliant. Le pliage du tissu
ferme le trou, et les créatures ou objets qui s'y trouvent restent dans
l'espace extradimensionnel. Peu importe ce qui s'y trouve, le trou ne
pèse presque rien.

Si le trou est replié, une créature se trouvant dans l'espace
extradimensionnel du trou peut utiliser une action pour faire un test de
Force DC 10. Si le test est réussi, la créature force sa sortie et
apparaît à moins de 1,5 mètre du trou portatif ou de la créature qui le
porte. Une créature respirant dans un trou portatif fermé peut survivre
jusqu'à 10 minutes, après quoi elle commence à suffoquer.

Placer un trou portatif à l'intérieur d'un espace extradimensionnel
créé par un sac sans fond, un havresac ou un objet similaire détruit
instantanément les deux objets et ouvre un portail vers le plan Astral.
Le portail prend naissance à l'endroit où l'un des objets a été placé
dans l'autre. Toute créature se trouvant à moins de 3 mètres du portail
est aspirée à travers celui-ci et déposée à un endroit aléatoire du plan
astral. Le Portail se referme ensuite. Le portail est à sens unique et
ne peut pas être rouvert.



### Potion d'amitié avec les animaux

*Potion, peu commune*

Lorsque vous buvez cette potion, vous pouvez lancer le sort *Amitié
avec les animaux* pendant 1
heure à volonté.

L'agitation de ce liquide boueux fait apparaître de petits morceaux :
une écaille de poisson, une langue de colibri, une griffe de chat ou un
poil d'écureuil.



### Potion de Clairvoyance

*Potion, rare*

Lorsque vous buvez cette potion, vous obtenez l'effet du sort de
*clairvoyance*. Un globe oculaire vacille dans ce
liquide jaunâtre mais disparaît lorsque la potion est ouverte.



### Potion d'escalade

*Potion, commune*

Lorsque vous buvez cette potion, vous gagnez une vitesse d'escalade
égale à votre vitesse de marche pendant 1 heure. Pendant ce temps, vous
avez un avantage sur les tests de Force (Athlétisme) que vous effectuez
pour grimper. La potion est séparée en couches brunes, argentées et
grises ressemblant à des bandes de pierre. Secouer la bouteille ne
permet pas de mélanger les couleurs.



### Potion de diminution

*Potion, rare*

Lorsque vous buvez cette potion, vous bénéficiez de l'effet \"réduire\"
du sort *agrandir/réduire* pendant 1d4 heures (aucune
concentration requise). Le rouge contenu dans le liquide de la potion se
contracte continuellement pour former une minuscule perle, puis se
dilate pour colorer le liquide clair qui l'entoure. Secouer la
bouteille ne permet pas d'interrompre ce processus.



### Potion de vol

*Potion, très rare*

Lorsque vous buvez cette potion, vous gagnez une vitesse de vol égale à
votre vitesse de marche pendant 1 heure et pouvez faire du surplace. Si
vous êtes en l'air lorsque la potion se dissipe, vous tombez, à moins
que vous n'ayez un autre moyen de rester en l'air. Le liquide clair de
cette potion flotte au sommet de son récipient et des impuretés blanches
nuageuses y dérivent.



### Potion de forme gazeuse

*Potion, rare*

Lorsque vous buvez cette potion, vous bénéficiez de l'effet du sort
*forme gazeuse* pendant 1 heure (aucune concentration
requise) ou jusqu'à ce que vous mettiez fin à cet effet par une action
bonus. Le récipient de cette potion semble contenir un brouillard qui se
déplace et se déverse comme de l'eau.



### Potion de force de géant

*Potion, rareté variable*

Lorsque vous buvez cette potion, votre score de Force change pendant 1
heure. Le type de géant détermine le score (voir le tableau ci-dessous).
La potion n'a aucun effet sur vous si votre Force est égale ou
supérieure à ce score.

  ------------------------------------------
  Type de géant        Force    Rarity
  ------------------ ---------- ------------
  Géant des collines     21     Uncommon

  Géant du givre/de      23     Rare
  la pierre                     

  Géant du feu           25     Rare

  Géant des nuages       27     Très rare

  Géant des tempêtes     29     Légendaire
  ------------------------------------------

Le liquide transparent de cette potion contient un morceau d'ongle
d'un géant du type approprié. La potion de force du géant du givre et
la potion de force du géant des pierres ont le même effet.



### Potion d'agrandissement

*Potion, peu commune*

Lorsque vous buvez cette potion, vous bénéficiez de l'effet
\"Agrandissement\" du sort *Agrandir/Réduire* pendant
1d4 heures (aucune concentration requise). Le rouge contenu dans le
liquide de la potion se dilate continuellement à partir d'une minuscule
perle pour colorer le liquide clair qui l'entoure, puis se contracte.

Secouer la bouteille ne permet pas d'interrompre ce processus.



### Potion de guérison

*Potion, rareté variable*

Vous regagnez des points de vie lorsque vous buvez cette potion.

Le nombre de points de vie dépend de la rareté de la potion, comme le
montre le tableau des potions de guérison. Quelle que soit sa puissance,
le liquide rouge de la potion scintille lorsqu'il est agité.

  ------------------------------------------
  Potion de\...       Rarity     HP retrouvé
  ------------------- ---------- -----------
  Guérison            Roturier   2d4 + 2

  Guérison accrue     Uncommon   4d4 + 4

  Guérison supérieure Rare       8d4 + 8

  Guérison suprême    Très rare  10d4 + 20
  ------------------------------------------

  : Potion de guérison



### Potion d'héroïsme

*Potion, rare*

Pendant 1 heure après l'avoir bu, vous gagnez 10 points de vie
temporaires qui durent 1 heure. Pour la même durée, vous êtes sous
l'effet du sort de *bénédiction* (aucune concentration
requise). Cette potion bleue fait des bulles et de la vapeur comme si
elle était en ébullition.



### Potion d'invisibilité

*Potion, très rare*

Le récipient de cette potion semble vide mais donne l'impression de
contenir du liquide. Lorsque vous la buvez, vous devenez invisible
pendant 1 heure. Tout ce que vous portez ou transportez est invisible
avec vous. L'effet se termine plus tôt si vous attaquez ou lancez un
sort.



### Potion de lecture des pensées

*Potion, rare*

Lorsque vous buvez cette potion, vous bénéficiez de l'effet du sort
*détection des pensées*. Le
liquide dense et violet de la potion est traversé par un nuage ovoïde de
couleur rose.



### Potion de poison

*Potion, peu commune*

Cette concoction a l'apparence, l'odeur et le goût d'une potion de
guérison ou d'une autre potion bénéfique. Cependant, il s'agit en fait
d'un poison masqué par la magie de l'illusion. Un sort
d'*identification* permet de révéler sa véritable nature.

Si vous le buvez, vous subissez 3d6 dégâts de poison, et vous devez
réussir un jet de sauvegarde de Constitution DC 13 ou être empoisonné.
Au début de chacun de vos tours pendant que vous êtes empoisonné de
cette façon, vous subissez 3d6 points de dégâts de poison. À la fin de
chacun de vos tours, vous pouvez répéter le jet de sauvegarde. En cas de
sauvegarde réussie, les dégâts de poison que vous subissez lors de vos
tours suivants diminuent de 1d6. Le poison prend fin lorsque les dégâts
sont réduits à 0.



### Potion de résistance

*Potion, peu commune*

Lorsque vous buvez cette potion, vous gagnez une résistance à un type de
dégâts pendant 1 heure. Le MJ choisit le type ou le détermine
aléatoirement parmi les options ci-dessous.

  ------------------
   d10  Type de
        dommage
  ----- ------------
    1   Acide

    2   Froid

    3   Feu

    4   Force

    5   La foudre

    6   Nécrotique

    7   Empoisonné

    8   Psychique

    9   Radiant

   10   Tonnerre
  ------------------



### Potion de vitesse

*Potion, très rare*

Lorsque vous buvez cette potion, vous bénéficiez de l'effet du sort de
*hâte*.

Le liquide jaune de la potion est strié de noir et tourbillonne tout
seul.



### Potion de respiration aquatique

*Potion, peu commune*

Vous pouvez respirer sous l'eau pendant une heure après avoir bu cette
potion. Son liquide vert nuageux sent la mer et une bulle ressemblant à
une méduse flotte à l'intérieur.



### Pommade réparatrice

*Objet merveilleux, peu commun*

Ce bocal en verre, de 3 pouces de diamètre, contient 1d4 + 1 doses d'un
mélange épais qui sent légèrement l'aloès.

Le bocal et son contenu pèsent 1/2 livre.

Comme action, une dose de l'onguent peut être avalée ou appliquée sur
la peau. La créature qui la reçoit regagne 2d8 + 2 points de vie, cesse
d'être empoisonnée et est guérie de toute maladie.



### Anneau d'influence sur les animaux

*Bague, rare*

Cet anneau possède 3 charges, et il regagne 1d3 charges dépensées
quotidiennement à l'aube. Lorsque vous portez l'anneau, vous pouvez
utiliser une action pour dépenser 1 de ses charges pour lancer l'un des
sorts suivants :

-   Amitié avec les*animaux*
-   *Peur*, ne visant que les bêtes ayant
    une Intelligence de 3 ou moins.
-   *Parler avec les animaux*



### Anneau de convocation de djinn

*Anneau, légendaire (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous pouvez prononcer son mot de
commande comme une action pour invoquer un Djinn particulier du plan
élémentaire de l'air. Le djinn apparaît dans un espace inoccupé de
votre choix, dans un rayon de 120 pieds autour de vous. Il reste aussi
longtemps que vous vous concentrez (comme si vous vous concentriez sur
un sort), jusqu'à un maximum de 1 heure, ou jusqu'à ce qu'il tombe à
0 points de vie. Il retourne ensuite sur son plan d'origine.

Pendant son invocation, le Djinn est amical envers vous et vos
compagnons. Il obéit à tous les ordres que vous lui donnez, quelle que
soit la langue que vous utilisez. Si vous n'arrivez pas à le commander,
le djinn se défend contre les attaquants mais ne fait aucune autre
action.

Après le départ du djinn, il ne peut plus être invoqué pendant 24
heures, et l'anneau devient non magique si le djinn meurt.



### Anneau de contrôle des élémentaux

*Anneau, légendaire (nécessite d'être accordé)*

Cet anneau est lié à l'un des quatre plans élémentaires. Le MJ choisit
ou détermine aléatoirement le plan lié.

Lorsque vous portez cet anneau, vous avez un avantage sur les jets
d'attaque contre les élémentaires du plan lié, et ils ont un
désavantage sur les jets d'attaque contre vous. De plus, vous avez
accès aux propriétés basées sur le plan lié.

L'anneau a 5 charges. Il regagne 1d4 + 1 charges dépensées chaque jour
à l'aube. Les sorts lancés à partir de l'anneau ont une sauvegarde DC
de 17.

***Anneau de contrôle des élémentaux de l'air.*** Vous pouvez dépenser
2 des charges de l'anneau pour lancer *Domination de
monstre* sur un élémentaire de l'air. De plus,
lorsque vous tombez, vous descendez de 60 pieds par round et ne subissez
aucun dégât de chute. Vous pouvez également parler et comprendre
l'aérien.

Si vous aidez à tuer un élémentaire de l'air alors que vous êtes lié à
l'anneau, vous avez accès aux propriétés supplémentaires suivantes :

-   Vous avez une résistance aux dégâts de la foudre.
-   Votre vitesse de vol est égale à votre vitesse de marche et vous
    pouvez faire du surplace.
-   Vous pouvez lancer les sorts suivants à partir de l'anneau, en
    dépensant le nombre de charges nécessaires : *Chaîne
    d'*,
    *Bourrasque* ou *Mur de
    vent*.

***Anneau de contrôle des élémentaux de la terre.*** Vous pouvez
dépenser 2 des charges de l'anneau pour lancer *Domination de
monstre* sur un élémentaire de terre. De plus, vous
pouvez vous déplacer en terrain difficile composé de gravats, de rochers
ou de terre comme s'il s'agissait d'un terrain normal. Vous pouvez
également parler et comprendre le glaiseux.

Si vous aidez à tuer un élémentaire de terre alors que vous êtes lié à
l'anneau, vous avez accès aux propriétés supplémentaires suivantes :

-   Vous avez une résistance aux dégâts d'acide.
-   Vous pouvez vous déplacer à travers la terre ou la roche solide
    comme si ces zones étaient des terrains difficiles. Si vous y
    terminez votre tour, vous êtes déplacé vers l'espace inoccupé le
    plus proche que vous avez occupé en dernier.
-   Vous pouvez lancer les sorts suivants à partir de l'anneau, en
    dépensant le nombre de charges nécessaires : *Façonnage de la
    pierre*
    (3 charges) ou *Mur de pierre*.

***Anneau de contrôle des élémentaux du feu.*** Vous pouvez dépenser 2
des charges de l'anneau pour lancer *Domination de
monstre* sur un élémentaire de feu. De plus, vous
avez une résistance aux dégâts de feu. Vous pouvez également parler et
comprendre l'Ardent.

Si vous aidez à tuer un élémentaire du feu alors que vous êtes lié à
l'anneau, vous avez accès aux propriétés supplémentaires suivantes :

-   Vous êtes immunisé contre les dégâts du feu.
-   Vous pouvez lancer les sorts suivants à partir de l'anneau, en
    dépensant le nombre de charges nécessaires : *mains
    brûlantes*
    (2 charges) et *mur de feu*.

***Anneau de contrôle des élémentaires de l'eau.*** Vous pouvez
dépenser 2 des charges de l'anneau pour lancer *Domination de
monstre* sur un élémentaire d'eau. De plus, vous
pouvez vous tenir debout et marcher sur des surfaces liquides comme
s'il s'agissait d'un sol solide. Vous pouvez également parler et
comprendre l'aquatique.

Si vous aidez à tuer un élémentaire d'eau alors que vous êtes lié à
l'anneau, vous avez accès aux propriétés supplémentaires suivantes :

-   Vous pouvez respirer sous l'eau et avez une vitesse de nage égale à
    votre vitesse de marche.
-   Vous pouvez lancer les sorts suivants à partir de l'anneau, en
    dépensant le nombre de charges nécessaires : Création *ou
    anéantissement de l'eau*,
    *contrôle de l'eau*, *tempête de
    glace*
    (3 charges).



### Anneau d'esquive totale

*Anneau, rare (nécessite d'être accordé)*

Cet anneau possède 3 charges, et il regagne 1d3 charges dépensées chaque
jour à l'aube. Lorsque vous échouez à un jet de sauvegarde de Dextérité
alors que vous le portez, vous pouvez utiliser votre réaction pour
dépenser 1 de ses charges pour réussir ce jet de sauvegarde à la place.



### Anneau de feuille morte

*Anneau, rare (nécessite d'être accordé)*

Lorsque vous tombez en portant cet anneau, vous descendez de 60 pieds
par round et ne subissez aucun dégât de chute.



### Anneau d'action libre

*Anneau, rare (nécessite d'être accordé)*

Tant que vous portez cet anneau, les terrains difficiles ne vous coûtent
pas de mouvement supplémentaire. De plus, la magie ne peut ni réduire
votre vitesse, ni vous paralyser ou vous entraver.



### Anneau d'invisibilité

*Anneau, légendaire (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous pouvez devenir invisible en une
action. Tout ce que vous portez ou transportez est invisible avec vous.
Vous restez invisible jusqu'à ce que l'anneau soit retiré, que vous
attaquiez ou lanciez un sort, ou que vous utilisiez une action bonus
pour redevenir visible.



### Anneau de saut

*Anneau, peu commun (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous pouvez lancer le sort de saut
qu'il contient en tant qu'action bonus à volonté, mais vous ne pouvez
cibler que vous-même lorsque vous le faites.



### Anneau de barrière mentale

*Anneau, peu commun (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous êtes immunisé contre la magie qui
permet aux autres créatures de lire vos pensées, de déterminer si vous
mentez, de connaître votre alignement ou votre type de créature. Les
créatures peuvent communiquer avec vous par télépathie uniquement si
vous le permettez.

Vous pouvez utiliser une action pour rendre l'anneau invisible jusqu'à
ce que vous utilisiez une autre action pour le rendre visible, jusqu'à
ce que vous retiriez l'anneau, ou jusqu'à votre mort.

Si vous mourez en portant l'anneau, votre âme y entre, à moins qu'il
n'abrite déjà une âme. Vous pouvez rester dans l'anneau ou partir pour
l'au-delà. Tant que votre âme est dans l'anneau, vous pouvez
communiquer par télépathie avec toute créature qui le porte. Un porteur
ne peut pas empêcher cette communication télépathique.



### Anneau de protection

*Anneau, rare (nécessite d'être accordé)*

Vous gagnez un bonus de +1 à la CA et aux jets de sauvegarde lorsque
vous portez cet anneau.



### Anneau de régénération

*Anneau, très rare (nécessite une syntonisation)*

Lorsque vous portez cet anneau, vous regagnez 1d6 points de vie toutes
les 10 minutes, à condition que vous ayez au moins 1 point de vie. Si
vous perdez une partie de votre corps, l'anneau fait repousser la
partie manquante qui retrouve sa pleine fonctionnalité au bout de 1d6 +
1 jours si vous avez au moins 1 point de vie pendant tout ce temps.



### Anneau de résistance

*Anneau, rare (nécessite d'être accordé)*

Vous avez une résistance à un type de dégâts lorsque vous portez cet
anneau. La gemme dans l'anneau indique le type, que le MJ choisit ou
détermine au hasard.

  -------------------------------
   d10               
  ----- ------------ ------------
    1   Acide        Perle

    2   Froid        Tourmaline

    3   Feu          Grenat

    4   Force        Saphir

    5   La foudre    Citrine

    6   Nécrotique   Jet

    7   Empoisonné   Améthyste

    8   Psychique    Jade

    9   Radiant      Topaz

   10   Tonnerre     Spinel
  -------------------------------



### Anneau de feu d'étoiles

*Anneau, très rare (nécessite d'être accordé à l'extérieur, la nuit)*

Lorsque vous portez cet anneau dans la pénombre ou l'obscurité, vous
pouvez lancer des *lumières dansantes* et des
*lumières* à partir de l'anneau à volonté. Lancer un sort à
partir de l'anneau nécessite une action.

L'anneau dispose de 6 charges pour les autres propriétés suivantes.
L'anneau récupère 1d6 charges dépensées chaque jour à l'aube.

***Lueurs féeriques.*** Vous pouvez dépenser 1 charge en tant qu'action
pour lancer le feu *féerique* depuis l'anneau.

***Foudre en boule.*** Vous pouvez dépenser 2 charges en tant qu'action
pour créer une à quatre sphères d'éclairs de 3 pieds de diamètre. Plus
vous créez de sphères, moins chaque sphère est puissante
individuellement.

Chaque sphère apparaît dans un espace inoccupé que vous pouvez voir dans
un rayon de 120 pieds autour de vous. Les sphères durent aussi longtemps
que vous vous concentrez (comme si vous vous concentriez sur un sort),
jusqu'à 1 minute. Chaque sphère diffuse une faible lumière dans un
rayon de 30 pieds.

Comme action bonus, vous pouvez déplacer chaque sphère jusqu'à 30
pieds, mais pas plus loin que 120 pieds de vous.

Lorsqu'une créature autre que vous s'approche à moins de 1,5 mètre
d'une sphère, celle-ci décharge des éclairs sur cette créature et
disparaît. Cette créature doit effectuer un jet de sauvegarde de
Dextérité DC 15. En cas d'échec, la créature subit des dégâts de foudre
basés sur le nombre de sphères que vous avez créées.

  ---------------------------
   Sphères  Dégâts causés par
                la foudre
  --------- -----------------
      4            2d4

      3            2d6

      2            5d4

      1           4d12
  ---------------------------

***Étoiles filantes.*** Vous pouvez dépenser de 1 à 3 charges en tant
qu'action. Pour chaque charge dépensée, vous lancez une tache de
lumière incandescente depuis l'anneau vers un point que vous pouvez
voir dans un rayon de 60 pieds autour de vous. Chaque créature située
dans un cube de 15 pieds partant de ce point est couverte d'étincelles
et doit effectuer un jet de sauvegarde de Dextérité à 15 DC, subissant
5d4 points de dégâts de feu en cas d'échec, ou la moitié des dégâts en
cas de réussite.



### Anneau de stockage de sort

*Anneau, rare (nécessite d'être accordé)*

Cet anneau stocke les sorts qui y sont lancés, les conservant jusqu'à
ce que le porteur lié les utilise. L'anneau peut stocker jusqu'à 5
niveaux de sorts à la fois. Lorsqu'il est retrouvé, il contient 1d6 - 1
niveaux de sorts stockés choisis par le MJ.

Toute créature peut lancer un sort du 1er au 5e niveau dans l'anneau en
le touchant au moment où le sort est lancé. Le sort n'a aucun effet, si
ce n'est d'être stocké dans l'anneau. Si l'anneau ne peut pas
contenir le sort, celui-ci est dépensé sans effet. Le niveau de
l'emplacement utilisé pour lancer le sort détermine la place qu'il
occupe.

Lorsque vous portez cet anneau, vous pouvez lancer n'importe quel sort
qui y est stocké. Le sort utilise le niveau de l'emplacement, la
sauvegarde contre les sorts, le bonus d'attaque des sorts et la
capacité d'incantation du lanceur d'origine, mais il est traité comme
si vous aviez lancé le sort. Le sort lancé depuis l'anneau n'est plus
stocké dans celui-ci, ce qui libère de l'espace.



### Anneau de renvoi des sorts

*Anneau, légendaire (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous avez un avantage aux jets de
sauvegarde contre tout sort qui ne cible que vous (pas dans une zone
d'effet). De plus, si vous obtenez un 20 pour la sauvegarde et que le
sort est de 7ème niveau ou moins, le sort n'a aucun effet sur vous et
cible le lanceur, en utilisant le niveau de l'emplacement, le DC de
sauvegarde du sort, le bonus d'attaque et la caractéristique
d'incantation du lanceur.



### Anneau de nage

*Bague, peu commune*

Vous avez une vitesse de nage de 40 pieds lorsque vous portez cet
anneau.



### Anneau de télékinésie

*Anneau, très rare (nécessite une syntonisation)*

Lorsque vous portez cet anneau, vous pouvez lancer le sort de
*télékinésie* à volonté, mais vous ne pouvez cibler que
les objets qui ne sont pas portés ou transportés.



### Anneau de triple souhaits

*Bague, légendaire*

Lorsque vous portez cet anneau, vous pouvez utiliser une action pour
dépenser 1 de ses 3 charges pour lancer le sort *Souhait* à
partir de celui-ci. L'anneau devient non-magique lorsque vous utilisez
la dernière charge.



### Anneau ardent

*Anneau, peu commun (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous avez une résistance aux dommages
causés par le froid. De plus, vous, ainsi que tout ce que vous portez,
êtes indemnes de températures aussi basses que -50 degrés Fahrenheit.



### Anneau de marche sur l'eau

*Bague, peu commune*

En portant cet anneau, vous pouvez vous tenir debout et vous déplacer
sur n'importe quelle surface liquide comme s'il s'agissait d'un sol
solide.



### Anneau de rayons X

*Anneau, rare (nécessite d'être accordé)*

Lorsque vous portez cet anneau, vous pouvez utiliser une action pour
prononcer son mot d'Injonction. Lorsque vous le faites, vous pouvez
voir dans et à travers la matière solide pendant 1 minute. Cette vision
a un rayon de 30 pieds. Pour vous, les objets solides dans ce rayon
semblent transparents et n'empêchent pas la lumière de les traverser.
La vision peut pénétrer 1 pied de pierre, 1 pouce de métal commun, ou
jusqu'à 3 pieds de bois ou de terre. Les substances plus épaisses
bloquent la vision, tout comme une fine feuille de plomb.

Chaque fois que vous utilisez à nouveau l'anneau avant de prendre un
repos long, vous devez réussir un jet de sauvegarde de Constitution DC
15 ou gagner un niveau d'épuisement.



### Anneau du bélier

*Anneau, rare (nécessite d'être accordé)*

Cet anneau possède 3 charges, et il regagne 1d3 charges dépensées
quotidiennement à l'aube. Lorsque vous portez l'anneau, vous pouvez
utiliser une action pour dépenser 1 à 3 de ses charges pour attaquer une
créature que vous pouvez voir dans un rayon de 60 pieds autour de vous.
L'anneau produit une tête de bélier spectrale et effectue son jet
d'attaque avec un bonus de +7. En cas de succès, pour chaque charge
dépensée, la cible subit 2d10 points de dégâts de force et est repoussée
à 5 pieds de vous.

Vous pouvez également dépenser de 1 à 3 charges de l'anneau en tant
qu'action pour essayer de briser un objet que vous pouvez voir dans un
rayon de 60 pieds autour de vous et qui n'est pas porté ou transporté.
L'anneau effectue un test de Force avec un bonus de +5 pour chaque
charge dépensée.



### Robe panoptique

*Objet merveilleux, rare (à accorder)*

Cette robe est ornée de motifs en forme de paupières. Lorsque vous
portez la robe, vous obtenez les avantages suivants :

-   La robe vous permet de voir dans toutes les directions, et vous avez
    un avantage sur les tests de Sagesse (Perception) qui reposent sur
    la vue.
-   Vous avez une vision dans le noir jusqu'à une portée de 120 pieds.
-   Vous pouvez voir les créatures et les objets invisibles, ainsi que
    voir dans le plan éthéré, jusqu'à une portée de 120 pieds.

Les yeux sur la robe ne peuvent être ni fermés ni détournés.

Bien que vous puissiez fermer ou détourner vos yeux, vous n'êtes jamais
considéré comme le faisant lorsque vous portez cette robe.

Un sort de *lumière* lancé sur la robe ou un sort de *lumière
du jour* lancé à moins de 1,5 m de la robe vous rend aveugle
pendant 1 minute. À la fin de chacun de vos tours, vous pouvez effectuer
un jet de sauvegarde de Constitution (DC 11 pour la *lumière*
ou DC 15 pour la *lumière du jour*, mettant fin à la
Cécité en cas de réussite. Vous réapparaissez dans le dernier espace que
vous avez occupé, ou si cet espace est occupé, dans l'espace inoccupé
le plus proche.



### Robe prismatique

*Objet merveilleux, très rare (nécessite d'être accordé).*

Cette robe a 3 charges, et elle regagne 1d3 charges dépensées
quotidiennement à l'aube. Lorsque vous la portez, vous pouvez utiliser
une action et dépenser 1 charge pour que le vêtement affiche un motif
changeant de teintes éblouissantes jusqu'à la fin de votre prochain
tour. Pendant ce temps, la robe diffuse une lumière vive dans un rayon
de 30 pieds et une lumière chétive sur 30 pieds supplémentaires. Les
créatures qui peuvent vous voir ont un désavantage aux jets d'attaque
contre vous. De plus, toute créature dans la lumière vive qui peut vous
voir lorsque le pouvoir de la robe est activé doit réussir un jet de
sauvegarde de Sagesse DC 15 ou être étourdie jusqu'à la fin de
l'effet.



### Robe aux étoiles

*Objet merveilleux, très rare (nécessite d'être accordé).*

Cette robe noire ou bleu foncé est brodée de petites étoiles blanches ou
argentées. Vous bénéficiez d'un bonus de +1 aux jets de sauvegarde
lorsque vous la portez.

Six étoiles, situées sur la partie supérieure du devant de la robe, sont
particulièrement grandes. Lorsque vous portez cette robe, vous pouvez
utiliser une action pour retirer l'une des étoiles et l'utiliser pour
lancer un *projectile magique* comme un sort de 5e
niveau. Tous les jours au crépuscule, 1d6 étoiles retirées
réapparaissent sur la robe.

Tant que vous portez la robe, vous pouvez utiliser une action pour
entrer dans le plan astral avec tout ce que vous portez et transportez.
Vous y restez jusqu'à ce que vous utilisiez une action pour retourner
dans le plan où vous étiez. Vous



### Robe de camelot

*Objet merveilleux, peu commun*

Cette robe est recouverte de pièces de tissu de formes et de couleurs
variées. Lorsque vous portez la robe, vous pouvez utiliser une action
pour détacher l'un des patchs, le faisant devenir l'objet ou la
créature qu'il représente. Une fois la dernière pièce détachée, la robe
devient un vêtement ordinaire.

La robe comporte deux de chacun des patchs suivants :

-   Dague
-   Lanterne Bullseye (remplie et allumée)
-   Miroir en acier
-   Pôle de 10 pieds
-   Corde de chanvre (50 pieds, enroulée)
-   Sack

De plus, la robe comporte 4d4 autres patchs. Le MJ choisit les patchs ou
les détermine au hasard.

  ------------------------------------------------------------------------
   d100   Patch
  ------- ----------------------------------------------------------------
   01-8   Sac de 100 gp

   09-15  Coffre en argent (1 pied de long, 6 pouces de large et de
          profondeur) d'une valeur de 500 gp

   16-22  Porte en fer (jusqu'à 10 pieds de large et 10 pieds de haut,
          barrée d'un côté de votre choix), que vous pouvez placer dans
          une ouverture que vous pouvez atteindre ; elle s'adapte à
          l'ouverture, se fixe et s'articule toute seule.

   23-30  10 pierres précieuses d'une valeur de 100 gp chacune

   31-44  Échelle en bois (24 pieds de long)

   45-51  Un cheval d'équitation avec des sacs de selle

   52-59  Puits (un cube de 10 pieds de côté), que vous pouvez placer sur
          le sol dans un rayon de 10 pieds autour de vous.

   60-68  4 potions de guérison

   69-75  Bateau à rames (12 pieds de long)

   76-83  Parchemin de sort contenant un sort du 1er au
          3ème niveau

   84-90  2 molosses

   91-96  Fenêtre (2 pieds sur 4 pieds, jusqu'à 2 pieds de profondeur),
          que vous pouvez placer sur une surface verticale que vous pouvez
          atteindre

   97-00  Béliers portables
  ------------------------------------------------------------------------



### Robe de l'archimage

*Objet merveilleux, légendaire (doit être accordé par un sorcier, un
warlock ou un magicien).*

Cet élégant vêtement est fabriqué dans un tissu exquis de couleur
blanche, grise ou noire et orné de runes argentées.

La couleur de la robe correspond à l'alignement pour lequel l'objet a
été créé. Une robe blanche a été créée pour le bien, grise pour le
neutre, et noire pour le mal. Vous ne pouvez pas vous accorder à une
robe de l'archimage qui ne correspond pas à votre alignement.

Vous bénéficiez de ces avantages lorsque vous portez la robe :

-   Si vous ne portez pas d'armure, votre classe d'armure de base est
    de 15 + votre modificateur de Dextérité.
-   Vous avez un avantage aux jets de sauvegarde contre les sorts et
    autres effets magiques.
-   Votre sauvegarde contre les sorts et votre bonus d'attaque
    augmentent chacun de 2.



### Sceptre d'absorption

*Bâton, très rare (nécessite une syntonisation)*

Lorsque vous tenez cette baguette, vous pouvez utiliser votre réaction
pour absorber un sort qui ne vise que vous et non une zone d'effet.
L'effet du sort absorbé est annulé, et l'énergie du sort, et non le
sort lui-même, est stockée dans la baguette. L'énergie a le même niveau
que le sort au moment où il a été lancé. Le bâton peut absorber et
stocker jusqu'à 50 niveaux d'énergie au cours de son existence. Une
fois que la baguette a absorbé 50 niveaux d'énergie, elle ne peut plus
en absorber davantage. Si vous êtes ciblé par un sort que le bâton ne
peut pas stocker, le bâton n'a aucun effet sur ce sort.

Lorsque vous êtes lié au bâton, vous savez combien de niveaux d'énergie
le bâton a absorbé au cours de son existence, et combien de niveaux
d'énergie de sort il a actuellement stocké.

Si vous êtes un lanceur de sorts qui tient la baguette, vous pouvez
convertir l'énergie qui y est stockée en emplacements de sorts pour
lancer des sorts que vous avez préparés ou que vous connaissez. Vous ne
pouvez créer que des emplacements de sorts d'un niveau égal ou
inférieur à vos propres emplacements de sorts, jusqu'à un maximum de 5e
niveau. Vous utilisez les niveaux stockés à la place de vos
emplacements, mais sinon vous lancez le sort normalement. Par exemple,
vous pouvez utiliser 3 niveaux stockés dans la baguette comme un
emplacement de sort de 3ème niveau.

Une baguette nouvellement trouvée possède déjà 1d10 niveaux d'énergie
des sorts stockés en elle. Un bâton qui ne peut plus absorber l'énergie
des sorts et qui n'a plus d'énergie devient non-magique.



### Sceptre de vigilance

*Bâton, très rare (nécessite une syntonisation)*

Cette tige a une tête bridée et les propriétés suivantes.

***Vigilant.*** Lorsque vous tenez la baguette, vous avez un avantage
sur les tests de Sagesse (Perception) et sur les jets d'initiative.

***Sorts.*** Lorsque vous tenez la baguette, vous pouvez utiliser une
action pour lancer l'un des sorts suivants à partir de celle-ci :
Détection du *mal et du bien*, *Détection de
la magie*, *Détection du poison
et* des
*maladies*
l'*invisible*.

***Aura de protection.*** Comme action, vous pouvez planter l'extrémité
du manche de la baguette dans le sol, après quoi la tête de la baguette
diffuse une lumière vive dans un rayon de 60 pieds et une lumière
chétive sur 60 pieds supplémentaires. Tant que vous êtes dans cette
lumière vive, vous et toute créature qui vous est amie bénéficiez d'un
bonus de +1 à la CA et aux jets de sauvegarde, et pouvez détecter la
localisation de toute créature hostile invisible qui se trouve également
dans la lumière vive.

La tête de la baguette cesse de briller et l'effet prend fin au bout de
10 minutes, ou lorsqu'une créature utilise une action pour arracher la
baguette du sol. Cette propriété ne peut pas être utilisée à nouveau
avant la prochaine aube.



### Sceptre de puissance seigneuriale

*Bâton, légendaire (nécessite d'être accordé)*

Cette baguette est dotée d'une tête bridée et fonctionne comme une
masse magique qui confère un bonus de +3 aux jets d'attaque et de
dégâts effectués avec elle. La baguette a des propriétés associées à six
boutons différents qui sont disposés en ligne le long du manche. Elle
possède également trois autres propriétés, détaillées ci-dessous.

***Six boutons.*** Vous pouvez appuyer sur l'un des six boutons de la
baguette en tant qu'action bonus. L'effet d'un bouton dure jusqu'à
ce que vous appuyiez sur un autre bouton ou jusqu'à ce que vous
appuyiez à nouveau sur le même bouton, ce qui fait revenir la baguette à
sa forme normale.

Si vous appuyez sur le **bouton 1**, la baguette devient une langue
ardente, car une lame enflammée jaillit de l'extrémité opposée à la
tête bridée de la baguette.

Si vous appuyez sur le **bouton 2**, la tête de la baguette se replie et
deux lames en forme de croissant en sortent, transformant la baguette en
une hache d'armes magique qui accorde un bonus de +3 aux jets
d'attaque et de dégâts effectués avec elle.

Si vous appuyez sur le **bouton 3**, la tête à rebord de la canne se
replie, une pointe de lance jaillit du bout de la canne et le manche de
la canne s'allonge pour former un manche d'un mètre quatre-vingt,
transformant la canne en une lance magique qui confère une

Bonus de +3 aux jets d'attaque et de dégâts effectués avec.

Si vous appuyez sur le **bouton 4**, la canne se transforme en un poteau
d'escalade d'une longueur maximale de 15 mètres, selon vos
indications. Dans des surfaces aussi dures que le granit, un pic en bas
et trois crochets en haut ancrent le poteau. Des barres horizontales de
3 pouces de long se déploient sur les côtés, espacées d'un pied, pour
former une échelle. Le poteau peut supporter jusqu'à 4 000 livres. Si
le poids augmente ou si l'ancrage n'est pas solide, la tige reprend sa
forme normale.

Si vous appuyez sur le **bouton 5**, la baguette se transforme en bélier
à main et confère à son utilisateur un bonus de +10 aux tests de Force
effectués pour enfoncer des portes, barricades et autres barrières.

Si vous appuyez sur le **bouton 6**, la baguette prend ou reste dans sa
forme normale et indique le nord magnétique. (Rien ne se passe si cette
fonction de la canne est utilisée dans un endroit qui n'a pas de nord
magnétique). La canne vous permet également de connaître votre
profondeur approximative sous le sol ou votre hauteur au-dessus du sol.

***Drain de vie.*** Lorsque vous touchez une créature avec une attaque
de mêlée utilisant la baguette, vous pouvez forcer la cible à effectuer
un jet de sauvegarde de Constitution DC 17. En cas d'échec, la cible
subit 4d6 points de dégâts nécrotiques supplémentaires, et vous regagnez
un nombre de points de vie égal à la moitié de ces dégâts nécrotiques.
Cette propriété ne peut pas être réutilisée avant l'aube suivante.

***Paralysé.*** Lorsque vous touchez une créature avec une attaque de
mêlée utilisant la baguette, vous pouvez forcer la cible à effectuer un
jet de sauvegarde de Force DC 17. En cas d'échec, la cible est
paralysée pendant 1 minute. La cible peut répéter le jet de sauvegarde à
la fin de chacun de ses tours, mettant fin à l'effet en cas de
réussite. Cette propriété ne peut pas être utilisée à nouveau avant
l'aube suivante.

***Terrifier.*** Lorsque vous tenez la baguette, vous pouvez utiliser
une action pour forcer chaque créature que vous pouvez voir dans un
rayon de 30 pieds de vous à faire un jet de sauvegarde de Sagesse DC 17.

En cas d'échec, la cible est effrayée par vous pendant 1 minute. Une
cible effrayée peut répéter le jet de sauvegarde à la fin de chacun de
ses tours, mettant fin à l'effet sur elle-même en cas de réussite.
Cette propriété ne peut pas être utilisée à nouveau avant l'aube
suivante.



### Sceptre de suzeraineté

*Bâton, rare (nécessite une syntonisation)*

Vous pouvez utiliser une action pour présenter la baguette et ordonner
l'obéissance de chaque créature de votre choix que vous pouvez voir
dans un rayon de 120 pieds autour de vous. Chaque cible doit réussir un
jet de sauvegarde de Sagesse DC 15 ou être charmée par vous pendant 8
heures. Tant qu'elle est charmée de cette manière, la créature vous
considère comme son chef de confiance. Si elle est blessée par vous ou
vos compagnons, ou si on lui ordonne de faire quelque chose de contraire
à sa nature, la cible cesse d'être charmée de cette manière. La
baguette ne peut pas être utilisée à nouveau avant l'aube suivante.



### Sceptre de sécurité

*Rod, très rare*

Lorsque vous tenez cette baguette, vous pouvez utiliser une action pour
l'activer. La baguette vous transporte alors instantanément, vous et
jusqu'à 199 autres créatures volontaires que vous pouvez voir, vers un
paradis qui existe dans un espace extraplanaire. Vous choisissez la
forme que prend le paradis. Il peut s'agir d'un jardin tranquille,
d'une charmante clairière, d'une taverne joyeuse, d'un immense
palais, d'une île tropicale, d'un carnaval fantastique ou de tout ce
que vous pouvez imaginer. Quelle que soit sa nature, le paradis contient
suffisamment d'eau et de nourriture pour subvenir aux besoins de ses
visiteurs. Tout ce avec quoi on peut interagir à l'intérieur de
l'espace extraplanaire ne peut exister que là. Par exemple, une fleur
cueillie dans un jardin du paradis disparaît si on l'emporte en dehors
de l'espace extraplanaire.

Pour chaque heure passée dans le paradis, un visiteur regagne des points
de vie comme s'il avait dépensé 1 dé de vie. De plus, les créatures ne
vieillissent pas lorsqu'elles sont dans le paradis, bien que le temps
passe normalement. Les visiteurs peuvent rester dans le paradis jusqu'à
200 jours divisés par le nombre de créatures présentes (arrondir à
l'inférieur).

Lorsque le temps est écoulé ou que vous utilisez une action pour y
mettre fin, tous les visiteurs réapparaissent à l'emplacement qu'ils
occupaient lorsque vous avez activé la baguette, ou dans un espace
inoccupé le plus proche de cet emplacement. La baguette ne peut pas être
utilisée à nouveau avant dix jours.



### Corde d'escalade

*Objet merveilleux, peu commun*

Cette corde de soie de 60 pieds de long pèse 3 livres et peut supporter
jusqu'à 3 000 livres. Si vous tenez une extrémité de la corde et
utilisez une action pour prononcer le mot d'Injonction, la corde
s'anime. Comme action bonus, vous pouvez ordonner à l'autre extrémité
de se déplacer vers la destination de votre choix. Cette extrémité se
déplace de 10 pieds à votre tour lorsque vous la commandez pour la
première fois et de 10 pieds à chacun de vos tours jusqu'à ce qu'elle
atteigne sa destination, jusqu'à sa longueur maximale, ou jusqu'à ce
que vous lui disiez de s'arrêter. Vous pouvez également demander à la
corde de s'attacher solidement à un objet ou de se détacher, de se
nouer ou de se dénouer, ou de s'enrouler pour être portée.

Si vous demandez à la corde de faire des nœuds, de grands nœuds
apparaissent à intervalles d'un pied le long de la corde. Lorsqu'elle
est nouée, la corde se raccourcit à une longueur de 15 mètres et confère
un avantage aux tests effectués pour y grimper.

La corde a un CA de 20 et 20 points de vie. Elle regagne 1 point de vie
toutes les 5 minutes tant qu'elle a au moins 1 point de vie. Si la
corde tombe à 0 point de vie, elle est détruite.



### Corde d'enchevêtrement

*Objet merveilleux, rare*

Cette corde fait 30 pieds de long et pèse 3 livres. Si vous tenez une
extrémité de la corde et utilisez une action pour prononcer son mot
d'Injonction, l'autre extrémité s'élance vers l'avant pour
enchevêtrer une créature que vous pouvez voir dans un rayon de 6 mètres
autour de vous. La cible doit réussir un jet de sauvegarde de Dextérité
DC 15 ou être entravée.

Vous pouvez libérer la créature en utilisant une action bonus pour
prononcer un second mot d'Injonction. Une cible entravée par la corde
peut utiliser une action pour faire un test de Force ou de Dextérité DC
15 (au choix de la cible). En cas de réussite, la créature n'est plus
entravée par la corde.

La corde a un CA de 20 et 20 points de vie. Elle regagne 1 point de vie
toutes les 5 minutes tant qu'elle a au moins 1 point de vie. Si la
corde tombe à 0 point de vie, elle est détruite.



### Scarabée de protection

*Objet merveilleux, légendaire (nécessite d'être accordé)*

Si vous tenez ce médaillon en forme de scarabée dans votre main pendant
1 round, une inscription apparaît à sa surface, révélant sa nature
magique. Il procure deux avantages tant qu'il est sur votre personne :

-   Vous avez un avantage sur les jets de sauvegarde contre les sorts.
-   Le scarabée possède 12 charges. Si vous ratez un jet de sauvegarde
    contre un sort de nécromancie ou un effet néfaste provenant d'une
    créature morte-vivante, vous pouvez utiliser votre réaction pour
    dépenser 1 charge et transformer le jet raté en un jet réussi. Le
    scarabée se réduit en poudre et est détruit lorsque sa dernière
    charge est dépensée.



### Cimeterre de rapidité

*Arme (cimeterre), très rare (nécessite d'être accordé)*

Vous gagnez un bonus de +2 aux jets d'attaque et de dégâts effectués
avec cette arme magique. De plus, vous pouvez effectuer une attaque avec
cette arme en tant qu'action bonus à chacun de vos tours.



### Bouclier, +1, +2, ou +3

*Armure (Bouclier), peu commune (+1), rare (+2), ou très rare (+3)*

Lorsque vous tenez ce bouclier, vous bénéficiez d'un bonus à la CA
déterminé par la rareté du bouclier. Ce bonus s'ajoute au bonus normal
à la CA du bouclier.



### Bouclier d'attraction des projectiles

*Armure (Bouclier), rare (nécessite d'être accordé)*

Lorsque vous tenez ce bouclier, vous avez une résistance aux dégâts des
attaques avec des armes à distance.

***Malédiction.*** Ce Bouclier est maudit. S'y lier vous maudit
jusqu'à ce que vous soyez visé par le sort *supprimer la
malédiction* ou une magie similaire. Le fait de retirer
le bouclier ne met pas fin à la malédiction qui pèse sur vous. Chaque
fois qu'une attaque avec une arme à distance est effectuée contre une
cible située à moins de 3 mètres de vous, la malédiction vous fait
devenir la cible à la place.



### Chaussures de grimpe aranéide

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ces chaussures légères, vous pouvez vous déplacer
vers le haut, vers le bas et sur des surfaces verticales et à l'envers
le long des plafonds, tout en gardant les mains libres. Votre vitesse de
montée est égale à votre vitesse de marche.

Cependant, les chaussons ne permettent pas de se déplacer de cette façon
sur une surface glissante, comme une surface couverte de glace ou
d'huile.



### Colle universelle

*Objet merveilleux, légendaire*

Cette substance visqueuse, de couleur blanc laiteux, peut former un lien
adhésif permanent entre deux objets quelconques.

Il doit être conservé dans un bocal ou une fiole dont l'intérieur a été
enduit d'huile d'insaisissabilité. Lorsqu'on le trouve, un récipient
contient 1d6 + 1 onces.

Une once de colle peut couvrir une surface d'un pied carré. La colle
prend une minute pour se fixer. Une fois qu'elle a durci, le lien
qu'elle crée ne peut être rompu que par l'application d'un solvant
universel ou d'une huile *éthérée*, ou par un sort de
*souhait*.



### Parchemin de sort

*Scroll, varie*

Un *parchemin de sort* porte les mots d'un seul sort, écrits dans un
cryptogramme mystique. Si le sort figure sur la liste des
sorts de votre classe, vous pouvez lire le
parchemin et lancer son sort sans fournir de composantes matérielles.
Sinon, le parchemin est inintelligible. Lancer le sort en lisant le
parchemin nécessite le temps d'incantation normal du sort. Une fois le
sort lancé, les mots du parchemin s'effacent et celui-ci tombe en
poussière. Si l'incantation est ininterrompue, le parchemin n'est pas
perdu.

Si le sort figure sur la liste des sorts de votre classe mais est d'un
niveau supérieur à celui que vous pouvez normalement lancer, vous devez
effectuer un test d'aptitude en utilisant votre capacité d'incantation
pour déterminer si vous le lancez avec succès. Le résultat est égal à
10 + le niveau du sort. En cas d'échec, le sort disparaît du parchemin
sans autre effet.

Le niveau du sort sur le parchemin détermine le jet de sauvegarde et le
bonus d'attaque du sort, ainsi que la rareté du parchemin, comme
indiqué dans le tableau des parchemins de sorts.

  -------------------------------------------------------------------------
    Niveau des   Rarity                          Save DC         Bonus
      sorts                                                    d'attaque
  -------------- ---------------------------- -------------- --------------
  Tour de magie  Roturier                           13             +5

       1er       Roturier                           13             +5

       2ème      Uncommon                           13             +5

       3ème      Uncommon                           15             +7

       4ème      Rare                               15             +7

       5ème      Rare                               17             +9

       6ème      Très rare                          17             +9

       7ème      Très rare                          18            +10

       8ème      Très rare                          18            +10

       9ème      Légendaire                         19            +11
  -------------------------------------------------------------------------

  : Parchemin de sort

Un sort de magicien sur un *parchemin de sort* peut être copié tout
comme les sorts dans les livres de sorts. Lorsqu'un sort est copié à
partir d'un *parchemin de sorts*, le copieur doit réussir un test
d'Intelligence (Arcane) avec un DC égal à 10 + le niveau du sort. Si le
test est réussi, le sort est copié avec succès. Que le test réussisse ou
échoue, le *parchemin de sorts* est détruit.



### Bouclier gardes-maléfices

*Armure (Bouclier), très rare (nécessite d'être accordé)*

Lorsque vous tenez ce bouclier, vous avez un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques, et les attaques
par sorts ont un désavantage contre vous.



### Sphère d'annihilation

*Objet merveilleux, légendaire*

Cette sphère noire de 2 pieds de diamètre est un trou dans le multivers,
planant dans l'espace et stabilisé par un champ magique qui l'entoure.

La sphère oblitère toute matière qu'elle traverse et toute matière qui
la traverse. Les artefacts sont l'exception. A
moins qu'un artefact ne soit susceptible d'être endommagé par une
*sphère d'annihilation*, il traverse la sphère indemne. Tout ce qui
touche la sphère sans être entièrement englouti et anéanti par elle
subit 4d10 points de dégâts de force.

La sphère est immobile jusqu'à ce que quelqu'un la contrôle. Si vous
vous trouvez à moins de 60 pieds d'une sphère non contrôlée, vous
pouvez utiliser une action pour faire un test d'Intelligence (Mystères)
DC 25. En cas de réussite, la sphère lévite dans la direction de votre
choix, jusqu'à un nombre de pieds égal à 5 × votre modificateur
d'Intelligence (minimum 5 pieds). En cas d'échec, la sphère se déplace
de 10 pieds vers vous. Une créature dans l'espace de laquelle la sphère
entre doit réussir un jet de sauvegarde de Dextérité DC 13 ou être
touchée par elle, subissant 4d10 dégâts de force.

Si vous tentez de contrôler une sphère qui est sous le contrôle d'une
autre créature, vous effectuez un test d'Intelligence (Mystères)
contesté par le test d'Intelligence (Arcanes) de l'autre créature. Le
gagnant du concours gagne le contrôle de la sphère et peut la faire
léviter normalement.

Si la sphère entre en contact avec un portail planaire, comme celui créé
par le sort de *portail*, ou un espace extradimensionnel, comme
celui d'un *trou portatif*, le MJ détermine au hasard
ce qui se passe, en utilisant la table suivante.

  ------------------------------------------------------------------------
   d100   Résultat
  ------- ----------------------------------------------------------------
   01-50  La sphère est détruite.

   51-85  La sphère se déplace à travers le portail ou dans l'espace
          extradimensionnel.

   86-00  Une faille spatiale envoie chaque créature et objet dans un
          rayon de 180 pieds de la sphère, y compris la sphère, dans un
          plan d'existence aléatoire.
  ------------------------------------------------------------------------



### Bâton d'envoûtement

*Bâton, rare (doit être lié à un barde, un clerc, un druide, un sorcier,
un invocateur ou un magicien).*

Lorsque vous tenez ce bâton, vous pouvez utiliser une action pour
dépenser 1 de ses 10 charges pour lancer
*Charme-personne* ou
*Compréhension des langues* à partir de ce
bâton en utilisant votre DC de sauvegarde contre les sorts. Ce bâton
peut également être utilisé comme un bâton magique.

Si vous tenez le bâton et que vous échouez à un jet de sauvegarde contre
un sort d'enchantement qui ne cible que vous, vous pouvez transformer
votre sauvegarde ratée en une sauvegarde réussie. Vous ne pouvez plus
utiliser cette propriété du bâton jusqu'à la prochaine aube. Si vous
réussissez un jet de sauvegarde contre un sort d'enchantement qui ne
cible que vous, avec ou sans l'intervention du bâton, vous pouvez
utiliser votre réaction pour dépenser 1 charge du bâton et retourner le
sort contre son lanceur comme si vous aviez lancé le sort.

Le bâton récupère 1d8 + 2 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge, lancez un d20. Sur un 1, le bâton
devient un bâton de quart non-magique.



### Bâton de feu

*Bâton, très rare (doit être accordé par un druide, un sorcier, un
sorcier ou un magicien).*

Vous avez une résistance aux dégâts du feu tant que vous tenez ce bâton.

Le bâton possède 10 charges. Lorsque vous le tenez, vous pouvez utiliser
une action pour dépenser 1 ou plusieurs de ses charges pour lancer l'un
des sorts suivants à partir de celui-ci, en utilisant votre DC de
sauvegarde contre les sorts : *mains brûlantes* (1
charge), *boule de feu* ou *mur de
feu*.

Le bâton récupère 1d6 + 4 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge, lancez un d20. Sur un 1, le bâton
noircit, se réduit en cendres et est détruit.



### Bâton de givre

*Bâton, très rare (doit être accordé par un druide, un sorcier, un
sorcier ou un magicien).*

Vous avez une résistance aux dégâts de froid tant que vous tenez ce
bâton.

Le bâton possède 10 charges. Lorsque vous le tenez, vous pouvez utiliser
une action pour dépenser 1 ou plusieurs de ses charges pour lancer l'un
des sorts suivants, en utilisant votre sauvegarde contre les sorts :
*cône de froid*, *nuage de
brouillard* (4
charges), ou *mur de glace*.

Le bâton récupère 1d6 + 4 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge, lancez un d20. Sur un 1, le bâton se
transforme en eau et est détruit.



### Bâton de guérison

*Bâton, rare (nécessite d'être lié à un barde, un clerc ou un druide).*

Ce bâton possède 10 charges. Lorsque vous le tenez, vous pouvez utiliser
une action pour dépenser 1 ou plusieurs de ses charges pour lancer l'un
des sorts suivants, en utilisant votre DC de sauvegarde contre les sorts
et votre modificateur de capacité d'incantation : *guérison des
blessures*,
*restauration partielle*, ou *soins
de groupe (*.

Le bâton récupère 1d6 + 4 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge, lancez un d20. Sur un 1, le bâton
disparaît dans un éclair de lumière, perdu à jamais.



### Bâton de surpuissance

*Bâton, très rare (doit être accordé par un sorcier, un warlock ou un
magicien).*

Ce bâton peut être manié comme un bâton de quart magique qui confère un
bonus de +2 aux jets d'attaque et de dégâts effectués avec. Lorsque
vous le tenez, vous bénéficiez d'un bonus de +2 à la classe d'armure,
aux jets de sauvegarde et aux jets d'attaque de sorts.

Le bâton possède 20 charges pour les propriétés suivantes. Le bâton
regagne 2d8 + 4 charges dépensées chaque jour à l'aube. Si vous
dépensez la dernière charge, lancez un d20. Sur un 1, le bâton conserve
son bonus de +2 aux jets d'attaque et de dégâts mais perd toutes ses
autres propriétés. Sur un 20, le bâton récupère 1d8 + 2 charges.

***Coup de force.*** Lorsque vous touchez avec une attaque de mêlée
utilisant le bâton, vous pouvez dépenser 1 charge pour infliger 1d6
dégâts de force supplémentaires à la cible.

***Sorts.*** Lorsque vous tenez ce bâton, vous pouvez utiliser une
action pour dépenser 1 ou plusieurs de ses charges pour lancer un des
sorts suivants, en utilisant votre sauvegarde contre les sorts et votre
bonus d'attaque : *cône de froid*, *boule
de feu*, *globe
d'invulnérabilité*,
*immobilisation de monstre*,
*lévitation*
(version de 5e niveau, 5 charges), *missile magique*
(1 charge), *rayon affaiblissant*, ou
*mur*.

***Frappe de rétribution.*** Vous pouvez utiliser une action pour briser
le bâton au-dessus de votre genou ou contre une surface solide, en
effectuant une frappe de rétribution. Le bâton est détruit et libère sa
magie restante dans une explosion qui s'étend jusqu'à remplir une
sphère de 30 pieds de rayon centrée sur lui.

Vous avez 50 % de chances de voyager instantanément vers un plan
d'existence aléatoire, en évitant l'explosion. Si vous ne parvenez pas
à éviter l'effet, vous subissez des dégâts de force égaux à 16 × le
nombre de charges du bâton. Toutes les autres créatures dans la zone
doivent effectuer un jet de sauvegarde de Dextérité DC 17. En cas
d'échec, la créature subit une quantité de dégâts basée sur la distance
qui la sépare du point d'origine, comme indiqué dans le tableau
suivant. En cas de sauvegarde réussie, la créature subit deux fois moins
de dégâts.

  -------------------------------------------------------------
  Distance de l'origine Dégâts
  ---------------------- --------------------------------------
  à une distance de 10   8 × le nombre de charges dans le
  pieds ou moins         personnel

  11 à 20 pieds de       6 × le nombre de charges dans le
  distance               personnel

  21 à 30 pieds de       4 × le nombre de charges dans le
  distance               personnel
  -------------------------------------------------------------



### Bâton percussif

*Bâton, très rare (nécessite une syntonisation)*

Ce bâton peut être manié comme un bâton de quart magique qui confère un
bonus de +3 aux jets d'attaque et de dégâts effectués avec lui.

Le bâton possède 10 charges. Lorsque vous touchez avec une attaque de
mêlée en l'utilisant, vous pouvez dépenser jusqu'à 3 de ses charges.
Pour chaque charge dépensée, la cible subit 1d6 points de dégâts de
force supplémentaires. Le bâton récupère 1d6 + 4 charges dépensées
chaque jour à l'aube. Si vous dépensez la dernière charge, lancez un
d20. Sur un 1, le bâton devient un bâton de quart non-magique.



### Bâton de grand essaim d'insectes

*Bâton, rare (doit être lié à un barde, un clerc, un druide, un sorcier,
un invocateur ou un magicien).*

Ce bâton possède 10 charges et regagne 1d6 + 4 charges dépensées chaque
jour à l'aube. Si vous dépensez la dernière charge, lancez un d20. Sur
un 1, une nuée d'insectes consomme et détruit le bâton, puis se
disperse.

***Sorts.*** Lorsque vous tenez le bâton, vous pouvez utiliser une
action pour dépenser certaines de ses charges afin de lancer l'un des
sorts suivants à partir de celui-ci, en utilisant votre DC de sauvegarde
contre les sorts : *insecte géant* ou
*fléau d'insectes*.

***Nuage d'insectes.*** Tout en tenant le bâton, vous pouvez utiliser
une action et dépenser 1 charge pour provoquer un essaim d'insectes
volants inoffensifs qui se répandent dans un rayon de 30 pieds autour de
vous. Les insectes restent pendant 10 minutes, rendant la zone fortement
obscurcie pour les créatures autres que vous. L'essaim se déplace avec
vous, en restant centré sur vous. Un vent d'au moins 10 miles par heure
disperse l'essaim et met fin à l'effet.



### Bâton de tonnerre et de foudre

*Bâton, très rare (nécessite une syntonisation)*

Ce bâton peut être manié comme un bâton de quart magique qui confère un
bonus de +2 aux jets d'attaque et de dégâts effectués avec lui. Il
possède également les propriétés supplémentaires suivantes. Lorsqu'une
de ces propriétés est utilisée, elle ne peut plus l'être avant l'aube
suivante.

***Foudre.*** Lorsque vous touchez avec une attaque de mêlée utilisant
le bâton, vous pouvez faire subir à la cible 2d6 dégâts supplémentaires
dus à la foudre.

***Tonnerre.*** Lorsque vous touchez avec une attaque de mêlée utilisant
le bâton, vous pouvez faire en sorte que le bâton émette un bruit de
tonnerre, audible jusqu'à 300 pieds. La cible que vous touchez doit
réussir un jet de sauvegarde de Constitution DC 17 ou être étourdie
jusqu'à la fin de votre prochain tour.

***Coup de foudre.*** Vous pouvez utiliser une action pour faire jaillir
un éclair de la pointe du bâton en une ligne de 1,5 m de large et de 120
m de long. Chaque créature de cette ligne doit effectuer un jet de
sauvegarde de Dextérité DC 17, subissant 9d6 points de dégâts de foudre
en cas d'échec, ou la moitié des dégâts en cas de réussite.

***Coup de tonnerre.*** Vous pouvez utiliser une action pour que le
bâton émette un coup de tonnerre assourdissant, audible jusqu'à 600
pieds. Chaque créature située à moins de 60 pieds de vous (vous non
compris) doit effectuer un jet de sauvegarde de Constitution DC 17. En
cas d'échec, la créature subit 2d6 dégâts de tonnerre et devient sourde
pendant 1 minute. En cas de sauvegarde réussie, la créature subit la
moitié des dégâts et n'est pas assourdie.

***Tonnerre et foudre.*** Vous pouvez utiliser une action pour utiliser
les propriétés Coup de foudre et Coup de tonnerre en même temps. Cela
n'entraîne pas de dépenses pour l'utilisation quotidienne de ces
propriétés, mais uniquement pour l'utilisation de celle-ci.



### Bâton de flétrissement

*Bâton, rare (nécessite d'être accordé par un clerc, un druide ou un
invocateur).*

Ce bâton possède 3 charges et récupère 1d3 charges dépensées par jour à
l'aube.

Le bâton peut être brandi comme un bâton de quart magique.

En cas de succès, elle inflige des dégâts comme un bâton normal, et vous
pouvez dépenser 1 charge pour infliger 2d10 dégâts nécrotiques
supplémentaires à la cible. De plus, la cible doit réussir un jet de
sauvegarde de Constitution DC 15 ou être désavantagée pendant 1 heure à
tout test de capacité ou jet de sauvegarde utilisant la Force ou la
Constitution.



### Bâton du sorcelier

*Bâton, légendaire (doit être accordé par un sorcier, un warlock ou un
magicien).*

Ce bâton peut être manié comme un bâton de quart magique qui confère un
bonus de +2 aux jets d'attaque et de dégâts effectués avec lui. Tant
que vous le tenez, vous bénéficiez d'un bonus de +2 aux jets d'attaque
de sorts.

Le bâton possède 50 charges pour les propriétés suivantes. Il regagne
4d6 + 2 charges dépensées par jour à l'aube. Si vous dépensez la
dernière charge, lancez un d20. Sur un 20, le bâton récupère 1d12 + 1
charges.

***Absorption des sorts.*** Lorsque vous tenez le bâton, vous avez un
avantage sur les jets de sauvegarde contre les sorts. De plus, vous
pouvez utiliser votre réaction lorsqu'une autre créature lance un sort
qui ne vise que vous. Si vous le faites, le bâton absorbe la magie du
sort, annulant son effet et gagnant un nombre de charges égal au niveau
du sort absorbé. Cependant, si cette réaction porte le nombre total de
charges du bâton à plus de 50, le bâton explose comme si vous aviez
activé sa frappe de rétribution (voir ci-dessous).

***Sorts.*** Lorsque vous tenez le bâton, vous pouvez utiliser une
action pour dépenser certaines de ses charges afin de lancer l'un des
sorts suivants à partir de celui-ci, en utilisant votre DC de sauvegarde
contre les sorts et votre caractéristique d'incantation : *Invocation
d'élémentaire*, *Dissipation de la
magie* (version
de 7e niveau, 7 charges), *Sphère de feu* (2
charges), *Tempête de glace*,
*Invisibilité* (2
charges), *Éclair*,
*Passe-muraille*, *Changement de
plan* (5
charges), *Mur de feu*
(2 charges).

Vous pouvez également utiliser une action pour lancer l'un des sorts
suivants à partir du bâton, sans utiliser de charges : *verrouillage
des arcanes*,
*agrandissement/réduction*,
*main de mage* ou protection contre le *mal et le
bien*.

***Frappe de rétribution.*** Vous pouvez utiliser une action pour briser
le bâton au-dessus de votre genou ou contre une surface solide, en
effectuant une frappe de rétribution. Le bâton est détruit et libère sa
magie restante dans une explosion qui s'étend jusqu'à remplir une
sphère de 30 pieds de rayon centrée sur lui.

Vous avez 50 % de chances de voyager instantanément vers un plan
d'existence aléatoire, en évitant l'explosion. Si vous ne parvenez pas
à éviter l'effet, vous subissez des dégâts de force égaux à 16 × le
nombre de charges du bâton. Toutes les autres créatures dans la zone
doivent effectuer un jet de sauvegarde de Dextérité DC 17. En cas
d'échec, la créature subit une quantité de dégâts basée sur la distance
qui la sépare du point d'origine, comme indiqué dans le tableau
suivant. En cas de sauvegarde réussie, la créature subit deux fois moins
de dégâts.

  -------------------------------------------------------------
  Distance de l'origine Dégâts
  ---------------------- --------------------------------------
  à une distance de 10   8 × le nombre de charges dans le
  pieds ou moins         personnel

  11 à 20 pieds de       6 × le nombre de charges dans le
  distance               personnel

  21 à 30 pieds de       4 × le nombre de charges dans le
  distance               personnel
  -------------------------------------------------------------



### Bâton du python

*Bâton, peu commun (nécessite d'être lié à un clerc, un druide ou un
invocateur).*

Vous pouvez utiliser une action pour prononcer le mot d'Injonction de
ce bâton et le jeter sur le sol dans un rayon de 3 mètres autour de
vous. Le bâton devient un serpent constricteur géant sous votre contrôle
et agit sur son propre compte d'initiative. En utilisant une action
bonus pour prononcer à nouveau le mot de commande, vous redonnez au
bâton sa forme normale dans un espace précédemment occupé par le
serpent.

À votre tour, vous pouvez commander mentalement le serpent s'il se
trouve à moins de 60 pieds de vous et que vous n'êtes pas frappé
d'incapacité. Vous décidez de l'action du serpent et de son
déplacement au cours de son prochain tour, ou vous pouvez lui donner un
ordre général, comme d'attaquer vos ennemis ou de garder un endroit.

Si le serpent est réduit à 0 points de vie, il meurt et reprend sa forme
de bâton. Le bâton se brise alors et est détruit. Si le serpent reprend
sa forme de bâton avant de perdre tous ses points de vie, il les
récupère tous.



### Bâton des forêts

*Bâton, rare (nécessite d'être accordé par un druide)*

Ce bâton peut être manié comme un bâton de quart magique qui confère un
bonus de +2 aux jets d'attaque et de dégâts effectués avec lui. Lorsque
vous le tenez, vous bénéficiez d'un bonus de +2 aux jets d'attaque de
sorts.

Le bâton dispose de 10 charges pour les propriétés suivantes. Il regagne
1d6 + 4 charges dépensées chaque jour à l'aube. Si vous dépensez la
dernière charge, lancez un d20. Sur un 1, le bâton perd ses propriétés
et devient un bâton non magique.

***Sorts.*** Vous pouvez utiliser une action pour dépenser 1 ou
plusieurs des charges du bâton pour lancer l'un des sorts suivants à
partir de celui-ci, en utilisant votre DC de sauvegarde contre les sorts
: Amitié avec les *animaux*,
*Éveil* (2
charges), *Localiser les animaux ou les
plantes*, *Parler avec les
animaux*, *Parler avec les
plantes*, ou Mur
d'*épines*. Vous pouvez également
utiliser une action pour lancer le sort *passage sans
trace* à partir du bâton sans utiliser de charges.

***Forme d'arbre.*** Vous pouvez utiliser une action pour planter une
extrémité du bâton dans de la terre fertile et dépenser 1 charge pour
transformer le bâton en un arbre sain. L'arbre mesure 60 pieds de haut
et possède un tronc de 5 pieds de diamètre, et ses branches au sommet se
déploient dans un rayon de 20 pieds. L'arbre semble ordinaire mais
dégage une faible aura de magie de transmutation s'il est ciblé par
*détection de la magie*. En touchant l'arbre et en
utilisant une autre action pour prononcer son mot d'Injonction, vous
redonnez au bâton sa forme normale. Toute créature se trouvant dans
l'arbre tombe lorsque celui-ci redevient un bâton.



### Pierre de contrôle des élémentaux de terre

*Objet merveilleux, rare*

Si la pierre touche le sol, vous pouvez utiliser une action pour
prononcer son mot d'Injonction et invoquer un élémentaire de terre,
comme si vous aviez lancé le sort *conjuration
élémentaire*. La pierre ne peut plus être utilisée
de cette façon jusqu'à la prochaine aube. La pierre pèse 5 livres.



### Pierre porte-bonheur (Luckstone)

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Tant que cette agate polie est sur vous, vous bénéficiez d'un bonus de
+1 aux tests d'aptitude et aux jets de sauvegarde.



### Épée radieuse

*Arme (épée longue), rare (requiert une syntonisation)*

Cet objet semble être une poignée d'épée longue. En saisissant la
poignée, vous pouvez utiliser une action bonus pour faire surgir une
lame de pur éclat, ou faire disparaître la lame. Tant que la lame
existe, cette épée longue magique possède la propriété de finesse. Si
vous maîtrisez les épées courtes ou longues, vous maîtrisez l'épée
radieuse.

Vous gagnez un bonus de +2 aux jets d'attaque et de dégâts effectués
avec cette arme, qui inflige des dégâts radiants au lieu de dégâts
tranchants. Lorsque vous touchez un mort-vivant avec cette arme, la
cible subit 1d8 points de dégâts radiants supplémentaires.

La lame lumineuse de l'épée émet une lumière vive dans un rayon de 15
pieds et une lumière chétive sur 15 pieds supplémentaires. La lumière
est celle du soleil. Tant que la lame persiste, vous pouvez utiliser une
action pour étendre ou rapetisser son rayon de lumière vive et faible de
5 pieds chacun, jusqu'à un maximum de 30 pieds chacun ou un minimum de
10 pieds chacun.



### Épée voleuse de vie

*Arme (n'importe quelle épée), rare (nécessite d'être accordé)*

Lorsque vous attaquez une créature avec cette arme magique et que vous
obtenez un 20 au jet d'attaque, cette cible subit 3d6 dégâts
nécrotiques supplémentaires, à condition qu'elle ne soit pas une
construction ou un mort-vivant. Vous gagnez des points de vie
temporaires égaux aux dégâts supplémentaires infligés.



### Épée acérée

*Arme (n'importe quelle épée qui inflige des dégâts tranchants), très
rare (nécessite d'être accordé).*

Lorsque vous attaquez un objet avec cette épée magique et que vous
touchez, maximisez vos dés de dégâts d'arme contre la cible.

Lorsque vous attaquez une créature avec cette arme et que vous obtenez
un 20 au jet d'attaque, cette cible subit 4d6 points de dégâts
cisaillants supplémentaires. Lancez ensuite un autre d20. Si vous
obtenez un 20, vous coupez l'un des membres de la cible, l'effet de
cette perte étant déterminé par le MJ. Si la créature n'a pas de membre
à couper, vous coupez une partie de son corps à la place.

En outre, vous pouvez prononcer le mot d'Injonction de l'épée pour que
la lame diffuse une lumière vive dans un rayon de 3 mètres et une
lumière chétive sur 3 mètres supplémentaires. Prononcer à nouveau le mot
de commande ou rengainer l'épée éteint la lumière.



### Ép Ép Ép Ep épée incuse

*Arme (n'importe quelle épée), rare (nécessite d'être accordé)*

Les points de vie perdus par les dégâts de cette arme ne peuvent être
récupérés que par un repos court ou long, plutôt que par régénération,
magie ou tout autre moyen.

Une fois par tour, lorsque vous touchez une créature avec une attaque
utilisant cette arme magique, vous pouvez blesser la cible. Au début de
chacun des tours de la créature blessée, elle subit 1d4 dégâts
nécrotiques pour chaque fois que vous l'avez blessée, puis elle peut
effectuer un jet de sauvegarde de Constitution DC 15, mettant fin à
l'effet de toutes ces blessures sur elle-même en cas de réussite.
Sinon, la créature blessée, ou une créature à moins de 1,5 m d'elle,
peut utiliser une action pour faire un test de Sagesse (Médecine) DC 15,
mettant fin à l'effet de ces blessures sur elle en cas de réussite.



### Talisman du bien ultime

*Objet merveilleux, légendaire (doit être accordé par une créature
d'alignement bon).*

Ce talisman est un puissant symbole de bonté. Une créature d'alignement
ni bon ni mauvais subit 6d6 points de dégâts radiants lorsqu'elle
touche le talisman. Une créature maléfique subit 8d6 points de dégâts
radiant lorsqu'elle touche le talisman. Ces deux types de créatures
subissent à nouveau des dégâts chaque fois qu'elles terminent leur tour
en tenant ou en portant le talisman.

Si vous êtes un bon clerc ou paladin, vous pouvez utiliser le talisman
comme un symbole sacré, et vous gagnez un bonus de +2 aux jets
d'attaque de sorts lorsque vous le portez ou le tenez.

Le talisman possède 7 charges. Si vous le portez ou le tenez, vous
pouvez utiliser une action pour dépenser 1 charge de celui-ci et choisir
une créature que vous pouvez voir sur le sol dans un rayon de 120 pieds
autour de vous. Si la cible est d'alignement mauvais, une fissure
enflammée s'ouvre sous elle. La cible doit réussir un jet de sauvegarde
de Dextérité DC 20 ou tomber dans la fissure et être détruite, sans
laisser de traces. La fissure se referme ensuite, ne laissant aucune
trace de son existence. Lorsque vous dépensez la dernière charge, le
talisman se disperse en grains de lumière dorée et est détruit.



### Talisman du mal absolu

*Objet merveilleux, légendaire (nécessite d'être accordé par une
créature d'alignement mauvais).*

Cet objet symbolise le mal impénitent. Une créature qui n'est ni bonne
ni mauvaise en alignement subit 6d6 dégâts nécrotiques au contact du
talisman. Une créature bonne subit 8d6 points de dégâts nécrotiques
lorsqu'elle touche le talisman. Les deux types de créatures subissent à
nouveau ces dégâts chaque fois qu'elles terminent leur tour en tenant
ou en portant le talisman.

Si vous êtes un clerc ou un paladin maléfique, vous pouvez utiliser le
talisman comme un symbole sacré, et vous gagnez un bonus de +2 aux jets
d'attaque de sorts lorsque vous le portez ou le tenez.

Le talisman possède 6 charges. Si vous le portez ou le tenez, vous
pouvez utiliser une action pour dépenser 1 charge du talisman et choisir
une créature que vous pouvez voir sur le sol dans un rayon de 120 pieds
autour de vous. Si la cible est d'alignement bon, une fissure enflammée
s'ouvre sous elle. La cible doit réussir un jet de sauvegarde de
Dextérité DC 20 ou tomber dans la fissure et être détruite, sans laisser
de traces. La fissure se referme ensuite, ne laissant aucune trace de
son existence. Lorsque vous dépensez la dernière charge, le talisman se
dissout dans une bave malodorante et est détruit.



### Talisman de la sphère

*Objet merveilleux, légendaire (nécessite d'être accordé)*

Lorsque vous effectuez un test d'Intelligence (Arcanes) pour contrôler
une *sphère d'annihilation* alors que
vous tenez ce talisman, vous doublez votre bonus de maîtrise sur ce
test. De plus, lorsque vous commencez votre tour avec le contrôle d'une
sphère *d'annihilation*, vous pouvez utiliser une action pour la faire
léviter de 10 pieds, plus un nombre de pieds supplémentaires égal à 10 ×
votre modificateur d'Intelligence.



### Traité de perspicacité

*Objet merveilleux, très rare*

Ce livre contient des exercices de mémoire et de logique, et ses mots
sont chargés de magie. Si vous passez 48 heures sur une période de 6
jours ou moins à étudier le contenu du livre et à mettre en pratique ses
directives, votre score d'Intelligence augmente de 2, ainsi que votre
maximum pour ce score. Le manuel perd ensuite sa magie, mais la regagne
dans un siècle.



### Traité d'autorité et d'influence

*Objet merveilleux, très rare*

Ce livre contient des directives pour influencer et charmer les autres,
et ses mots sont chargés de magie. Si vous passez 48 heures sur une
période de 6 jours ou moins à étudier le contenu du livre et à mettre en
pratique ses directives, votre score de Charisme augmente de 2, tout
comme votre maximum pour ce score. Le manuel perd alors sa magie, mais
la regagne dans un siècle.



### Traité de compréhension

*Objet merveilleux, très rare*

Ce livre contient des exercices d'intuition et de perspicacité, et ses
mots sont chargés de magie. Si vous passez 48 heures sur une période de
6 jours ou moins à étudier le contenu du livre et à mettre en pratique
ses directives, votre score de Sagesse augmente de 2, ainsi que votre
maximum pour ce score. Le manuel perd alors sa magie, mais la regagne
dans un siècle.



### Trident de commandement des poissons

*Arme (trident), peu commune (nécessite d'être lié)*

Ce trident est une arme magique. Il possède 3 charges. Tant que vous le
portez, vous pouvez utiliser une action et dépenser 1 charge pour lancer
*Domination de bête* à partir de
celui-ci sur une bête qui a une vitesse de nage innée. Le trident
regagne 1d3 charges dépensées chaque jour à l'aube.



### Solvant universel

*Objet merveilleux, légendaire*

Ce tube contient un liquide laiteux avec une forte odeur d'alcool. Vous
pouvez utiliser une action pour verser le contenu du tube sur une
surface à portée de main. Le liquide dissout instantanément jusqu'à un
pied carré d'adhésif qu'il touche, y compris la colle souveraine.



### Arme vicieuse

*Arme (n'importe laquelle), rare*

Lorsque vous obtenez un 20 à votre jet d'attaque avec cette arme
magique, votre coup critique inflige 2d6 points de dégâts
supplémentaires du type de l'arme.



### Épée vorpale

*Arme (toute épée infligeant des dégâts tranchants), légendaire
(nécessite d'être accordé)*

Vous gagnez un bonus de +3 aux jets d'attaque et de dégâts effectués
avec cette arme magique. De plus, l'arme ignore la résistance aux
dégâts tranchants.

Lorsque vous attaquez une créature qui a au moins une tête avec cette
arme et que vous obtenez un 20 au jet d'attaque, vous coupez une des
têtes de la créature. La créature meurt si elle ne peut pas survivre
sans la tête perdue. Une créature est immunisée contre cet effet si elle
est immunisée contre les dégâts tranchants, si elle n'a pas de tête ou
n'en a pas besoin, si elle a des actions légendaires ou si le MJ décide
que la créature est trop grosse pour que sa tête puisse être coupée avec
cette arme. Une telle créature subit à la place 6d8 points de dégâts
tranchants supplémentaires.



### Baguette d'entraves

*Baguette, rare (nécessite d'être accordée par un lanceur de sorts)*

Cette baguette dispose de 7 charges pour les propriétés suivantes. Elle
regagne 1d6 + 1 charges dépensées par jour à l'aube. Si vous dépensez
la dernière charge de la baguette, lancez un d20. Sur un 1, la baguette
se réduit en cendres et est détruite.

***Sorts.*** Lorsque vous tenez la baguette, vous pouvez utiliser une
action pour dépenser certaines de ses charges pour lancer l'un des
sorts suivants (sauvegarde DC 17) : *immobilisation de
monstre* ou *immobilisation de
personne*.

***Évasion assistée.*** Lorsque vous tenez la baguette, vous pouvez
utiliser votre réaction pour dépenser 1 charge et gagner un avantage sur
un jet de sauvegarde que vous faites pour éviter d'être paralysé ou
entravé, ou vous pouvez dépenser 1 charge et gagner un avantage sur
n'importe quel test que vous faites pour échapper à un agrippé.



### Baguette de détection de l'ennemi

*Baguette, rare (nécessite d'être accordée)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action et dépenser 1 charge pour prononcer son mot
d'injonction. Pendant la minute suivante, vous connaissez la direction
de la créature hostile la plus proche dans un rayon de 60 pieds, mais
pas sa distance par rapport à vous. La baguette peut détecter la
présence de créatures hostiles éthérées, invisibles, déguisées ou
cachées, ainsi que celles qui sont à la vue de tous. L'effet prend fin
si vous cessez de tenir la baguette.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette de peur

*Baguette, rare (nécessite d'être accordée)*

Cette baguette dispose de 7 charges pour les propriétés suivantes. Elle
regagne 1d6 + 1 charges dépensées par jour à l'aube. Si vous dépensez
la dernière charge de la baguette, lancez un d20. Sur un 1, la baguette
se réduit en cendres et est détruite.

***Injonction.*** Tout en tenant la baguette, vous pouvez utiliser une
action pour dépenser 1 charge et ordonner à une autre créature de fuir
ou de ramper, comme avec le sort *commandement* (sauvegarde
DC 15).

***Cône de Peur.*** Lorsque vous tenez la baguette, vous pouvez utiliser
une action pour dépenser 2 charges, ce qui fait que l'extrémité de la
baguette émet un cône de lumière ambrée de 60 pieds. Chaque créature
dans le cône doit réussir un jet de sauvegarde de Sagesse DC 15 ou être
effrayée par vous pendant 1 minute. Tant qu'elle est effrayée de cette
manière, une créature doit passer ses tours à essayer de s'éloigner le
plus possible de vous, et elle ne peut pas se déplacer volontairement
vers un espace situé à moins de 30 pieds de vous. Elle ne peut pas non
plus avoir de réactions. Pour son action, elle ne peut utiliser que
l'action Foncer ou essayer d'échapper à un effet qui l'empêche de se
déplacer. Si elle n'a aucun endroit où elle peut se déplacer, la
créature peut utiliser l'action Esquive. À la fin de chacun de ses
tours, une créature peut répéter le jet de sauvegarde, mettant fin à
l'effet sur elle-même en cas de réussite.



### Baguette de boules de feu

*Baguette, rare (nécessite d'être accordée par un lanceur de sorts)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 ou plusieurs de ses charges pour
lancer le sort *boule de feu* à partir
de celle-ci. Pour 1 charge, vous lancez la version de 3e niveau du sort.
Vous pouvez augmenter le niveau de l'emplacement du sort de 1 pour
chaque charge supplémentaire que vous dépensez.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette d'éclairs

*Baguette, rare (nécessite d'être accordée par un lanceur de sorts)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 ou plusieurs de ses charges pour
lancer le sort *foudre* à partir
de celle-ci. Pour 1 charge, vous lancez la version de 3e niveau du sort.
Vous pouvez augmenter le niveau de l'emplacement de sort de 1 pour
chaque charge supplémentaire que vous dépensez.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette de détection de la magie

*Baguette, peu commune*

Cette baguette possède 3 charges. Lorsque vous la tenez, vous pouvez
dépenser 1 charge en tant qu'action pour lancer le sort de *détection
de la magie* à partir d'elle. La baguette regagne 1d3
charges dépensées chaque jour à l'aube.



### Baguette de projectiles magiques

*Baguette, peu commune*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 ou plusieurs de ses charges pour
lancer le sort de *missile magique* à partir de
celle-ci. Pour 1 charge, vous lancez la version de 1er niveau du sort.
Vous pouvez augmenter le niveau de l'emplacement du sort de 1 pour
chaque charge supplémentaire que vous dépensez.

La baguette regagne 1d6 + 1 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette de paralysie

*Baguette, rare (nécessite d'être accordée par un lanceur de sorts)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 de ses charges afin de faire jaillir
un fin rayon bleu de l'extrémité vers une créature que vous pouvez voir
dans un rayon de 60 pieds autour de vous. La cible doit réussir un jet
de sauvegarde de Constitution DC 15 ou être paralysée pendant 1 minute.
À la fin de chaque tour de la cible, elle peut répéter le jet de
sauvegarde, mettant fin à l'effet sur elle-même en cas de réussite.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette de métamorphose

*Baguette, très rare (nécessite d'être accordée par un lanceur de
sorts).*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 de ses charges pour lancer le sort
de *métamorphose* à partir de celle-ci.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette des secrets

*Baguette, peu commune*

La baguette possède 3 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 de ses charges, et si une porte
secrète ou un piège se trouve à moins de 30 pieds de vous, la baguette
pulse et pointe vers celui qui est le plus proche de vous. La baguette
regagne 1d3 charges dépensées chaque jour à l'aube.



### Baguette de toile d'araignée

*Baguette, peu commune (nécessite d'être accordée par un lanceur de
sorts)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 de ses charges pour lancer le sort
*toile* à partir d'elle.

La baguette regagne 1d6 + 1 charges dépensées par jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette se réduit en cendres et est détruite.



### Baguette des merveilles

*Baguette, rare (nécessite d'être accordée par un lanceur de sorts)*

Cette baguette possède 7 charges. Lorsque vous la tenez, vous pouvez
utiliser une action pour dépenser 1 de ses charges et choisir une cible
dans un rayon de 120 pieds autour de vous. La cible peut être une
créature, un objet ou un point dans l'espace. Lancez un d100 et
consultez la table suivante pour découvrir ce qui se passe.

Si l'effet vous amène à lancer un sort à partir de la baguette, le DC
de sauvegarde du sort est de 15. Si le sort a normalement une portée
exprimée en pieds, sa portée devient 120 pieds si elle ne l'est pas
déjà.

Si un effet couvre une zone, vous devez centrer le sort sur et inclure
la cible. Si un effet a plusieurs sujets possibles, le MJ détermine au
hasard ceux qui sont affectés.

La baguette regagne 1d6 + 1 charges dépensées chaque jour à l'aube. Si
vous dépensez la dernière charge de la baguette, lancez un d20. Sur un
1, la baguette s'effrite en poussière et est détruite.

  ------------------------------------------------------------------------
   d100   Effet
  ------- ----------------------------------------------------------------
   01-05  Vous lancez *Lenteur*.

   06-10  Vous lancez le *feu féerique*.

   11-15  Vous êtes étourdi jusqu'au début de votre prochain tour,
          croyant que quelque chose d'impressionnant vient de se
          produire.

   16-20  Vous lancez *Bourrasque de vent*.

   21-25  Vous lancez *Détection des* pensées sur la
          cible que vous avez choisie. Si vous n'avez pas ciblé de
          créature, vous subissez à la place 1d6 dégâts psychiques.

   26-30  Vous avez jeté un *nuage nauséabond*.

   31-33  Une pluie lourde tombe dans un rayon de 60 pieds centré sur la
          cible. La zone est légèrement obscurcie. La pluie tombe
          jusqu'au début de votre prochain tour.

   34-36  Un animal apparaît dans l'espace inoccupé le plus proche de la
          cible. L'animal n'est pas sous votre contrôle et agit comme il
          le ferait normalement. Lancez un d100 pour déterminer quel
          animal apparaît. Sur un 01-25, un rhinocéros apparaît ; sur un
          26-50, un éléphant apparaît ; et sur un 51-100, un rat apparaît.

   37-46  Vous lancez un *éclair*.

   47-49  Un nuage de 600 papillons surdimensionnés remplit un rayon de 30
          pieds centré sur la cible. La zone est fortement obscurcie. Les
          papillons restent pendant 10 minutes.

   50-53  Vous agrandissez la cible comme si vous aviez lancé
          *Agrandir/Réduire*. Si la cible ne peut pas
          être affectée par ce sort, ou si vous n'avez pas ciblé de
          créature, vous devenez la cible.

   54-58  Vous lancez les *Ténèbres*.

   59-62  L'herbe pousse sur le sol dans un rayon de 60 pieds centré sur
          la cible. Si de l'herbe est déjà présente, elle pousse jusqu'à
          dix fois sa taille normale et reste envahie par la végétation
          pendant 1 minute.

   63-65  Un objet choisi par le MJ disparaît dans le plan éthéré.
          L'objet ne doit pas être porté ou transporté, se trouver à
          moins de 120 pieds de la cible, et ne doit pas dépasser 10 pieds
          dans toutes les dimensions.

   66-69  Vous vous rapetissez comme si vous aviez lancé le sort
          *Agrandir/Réduire* sur vous-même.

   70-79  Vous lancez une *boule de feu*.

   80-84  Vous vous rendez *invisible*.

   85-87  Les feuilles poussent à partir de la cible. Si vous choisissez
          un point dans l'espace comme cible, des feuilles poussent de la
          créature la plus proche de ce point. Si elles ne sont pas
          cueillies, les feuilles deviennent brunes et tombent au bout de
          24 heures.

   88-90  Un flot de 1d4 × 10 gemmes, valant chacune 1 gp, jaillit de
          l'extrémité de la baguette en une ligne de 10 m de long sur 1,5
          m de large. Chaque gemme inflige 1 dégât de contondant, et le
          total des dégâts des gemmes est divisé également entre toutes
          les créatures de la ligne.

   91-95  Une rafale de lumière colorée et chatoyante s'étend de vous
          dans un rayon de 30 pieds. Vous et chaque créature dans la zone
          qui peut voir devez réussir un jet de sauvegarde de Constitution
          DC 15 ou devenir aveuglé pendant 1 minute. Une créature peut
          répéter le jet de sauvegarde à la fin de chacun de ses tours,
          mettant fin à l'effet sur elle-même en cas de réussite.

   96-97  La peau de la cible devient bleu vif pendant 1d10 jours. Si vous
          choisissez un point dans l'espace, la créature la plus proche
          de ce point est affectée.

   98-00  Si vous avez ciblé une créature, celle-ci doit effectuer un jet
          de sauvegarde de Constitution DC 15. Si vous n'avez pas ciblé
          de créature, vous devenez la cible et devez effectuer le jet de
          sauvegarde. Si le jet de sauvegarde échoue par 5 ou plus, la
          cible est instantanément pétrifiée. En cas d'échec à tout autre
          jet de sauvegarde, la cible est entravée et commence à se
          transformer en pierre. Lorsqu'elle est ainsi entravée, la cible
          doit répéter le jet de sauvegarde à la fin de son prochain tour,
          devenant pétrifiée en cas d'échec ou mettant fin à l'effet en
          cas de réussite. La pétrification dure jusqu'à ce que la cible
          soit libérée par le sort Restauration
          *supérieure* ou une magie similaire.
  ------------------------------------------------------------------------



### Baguette du mage de bataille, +1, +2, ou +3

*Baguette, peu commune (+1), rare (+2), ou très rare (+3) (doit être
accordée par un lanceur de sorts).*

Lorsque vous tenez cette baguette, vous gagnez un bonus aux jets
d'attaque de sorts déterminé par la rareté de la baguette. De plus,
vous ignorez la moitié de votre abri lorsque vous effectuez une attaque
par sort.



### Arme, +1, +2, ou +3

*Arme (quelconque), peu commune (+1), rare (+2), ou très rare (+3)*

Vous bénéficiez d'un bonus aux jets d'attaque et de dégâts effectués
avec cette arme magique. Ce bonus est déterminé par la rareté de
l'arme.



### Puits des mondes

*Objet merveilleux, légendaire*

Cette fine toile noire, douce comme de la soie, est pliée aux dimensions
d'un mouchoir de poche. Il se déplie en une feuille circulaire de 6
pieds de diamètre.

Vous pouvez utiliser une action pour déplier et placer le puits des
mondes sur une surface solide, après quoi il crée un portail à double
sens vers un autre monde ou plan d'existence. Chaque fois que l'objet
ouvre un portail, le MJ décide où il mène. Vous pouvez utiliser une
action pour fermer un portail ouvert en saisissant les bords du tissu et
en le repliant. Une fois que le Puits des mondes a ouvert un portail, il
ne peut plus le faire pendant 1d8 heures.



### Éventail des Brises

*Objet merveilleux, peu commun*

Lorsque vous tenez cet éventail, vous pouvez utiliser une action pour
lancer le sort *rafale de vent* à
partir de celui-ci. Une fois utilisé, l'éventail ne doit pas être
réutilisé avant l'aube suivante. Chaque fois qu'il est réutilisé avant
cette date, il a 20 % de chances cumulatives de ne pas fonctionner et de
se déchirer en lambeaux inutiles et non magiques.



### Bottes ailées

*Objet merveilleux, peu commun (nécessite d'être accordé).*

Lorsque vous portez ces bottes, votre vitesse de vol est égale à votre
vitesse de marche. Vous pouvez utiliser les bottes pour voler jusqu'à 4
heures, en une seule fois ou en plusieurs vols plus courts, chacun
utilisant au minimum 1 minute de la durée. Si vous êtes en vol lorsque
la durée expire, vous descendez à une vitesse de 30 pieds par round
jusqu'à ce que vous atterrissiez.

Les bottes regagnent 2 heures de capacité de vol pour chaque tranche de
12 heures où elles ne sont pas utilisées.



### Les ailes du vol

*Objet merveilleux, rare (à accorder)*

Lorsque vous portez cette cape, vous pouvez utiliser une action pour
prononcer son mot d'Injonction. Cela transforme la cape en une paire
d'ailes de chauve-souris ou d'oiseau sur votre dos pendant 1 heure ou
jusqu'à ce que vous répétiez le mot de commande en action. Les ailes
vous donnent une vitesse de vol de 60 pieds. Lorsqu'elles
disparaissent, vous ne pouvez plus les utiliser pendant 1d12 heures.




## Objets de Magie Sentimentale

Certains objets magiques possèdent une sensibilité et une personnalité.
Un tel objet peut être possédé, hanté par l'esprit d'un ancien
propriétaire ou conscient de lui-même grâce à la magie utilisée pour le
créer. Dans tous les cas, l'objet se comporte comme un personnage, avec
ses bizarreries, ses idéaux, ses liens et parfois ses défauts. Un objet
sensible peut être un allié précieux pour son propriétaire ou une épine
dans le pied.

La plupart des objets sensibles sont des armes. D'autres types
d'objets peuvent manifester de la sensibilité, mais les objets
consommables tels que les potions et les parchemins ne sont jamais
sensibles.

Les objets magiques sensibles fonctionnent comme des PNJ sous le
contrôle du MJ. Toute propriété activée de l'objet est sous le contrôle
de l'objet, et non de son porteur. Tant que le porteur maintient une
bonne relation avec l'objet, il peut accéder à ces propriétés
normalement. Si la relation est tendue, l'objet peut supprimer ses
propriétés activées ou même les retourner contre son porteur.


### Création d'objets magiques sensibles

Lorsque vous décidez de rendre un objet magique sensible, vous créez le
personnage de l'objet de la même manière que vous créeriez un PNJ, à
quelques exceptions près décrites ici.


#### Capacités

Un objet magique sensible possède des scores d'Intelligence, de Sagesse
et de Charisme. Vous pouvez choisir les capacités de l'objet ou les
déterminer au hasard. Pour les déterminer au hasard, lancez 4d6 pour
chacune d'entre elles, en laissant tomber le résultat le plus bas et en
totalisant le reste.



#### Communication

Un objet sensible possède une certaine capacité à communiquer, soit en
partageant ses émotions, soit en diffusant ses pensées par télépathie,
soit en parlant à voix haute. Vous pouvez choisir comment il communique
ou faire un jet sur la table suivante.

  ------------------------------------------------------------------------
   d100   Communication
  ------- ----------------------------------------------------------------
   01-60  L'objet communique en transmettant des émotions à la créature
          qui le porte ou le manie.

   61-90  L'article peut parler, lire et comprendre une ou plusieurs
          langues.

   91-00  L'objet peut parler, lire et comprendre une ou plusieurs
          langues. De plus, l'objet peut communiquer par télépathie avec
          tout personnage qui le porte ou le manie.
  ------------------------------------------------------------------------



#### Les sens

Avec la sensibilité vient la conscience. Un objet sensible peut
percevoir son environnement jusqu'à une portée limitée. Vous pouvez
choisir ses sens ou faire un jet sur la table suivante.

  ----------------------------------------------
   d4  Les sens
  ---- -----------------------------------------
   1   Ouïe et vision normale jusqu'à 30 pieds.

   2   Ouïe et vision normale jusqu'à 60 pieds

   3   Ouïe et vision normale jusqu'à 120
       pieds.

   4   Ouïe et vision dans le noir jusqu'à 120
       pieds.
  ----------------------------------------------



#### Alignement

Un objet magique sensible a un alignement. Son créateur ou sa nature
peuvent suggérer un alignement. Si ce n'est pas le cas, vous pouvez
choisir un alignement ou faire un jet sur la table suivante.

  -----------------------
   d100   Alignement
  ------- ---------------
   01-15  Bien licite

   16-35  Neutre bon

   36-50  Bien chaotique

   51-63  Légalement
          neutre

   64-73  Neutre

   74-85  Chaotique
          neutre

   86-89  Le mal licite

   90-96  Mal neutre

   97-00  Le mal
          chaotique
  -----------------------



#### Objectif spécial

Vous pouvez donner à un objet sensible un objectif qu'il poursuit,
peut-être à l'exclusion de tout autre. Tant que l'utilisation de
l'objet par celui qui le manie est conforme à cet objectif particulier,
l'objet reste coopératif. Si vous vous écartez de cette ligne de
conduite, vous risquez d'entrer en conflit avec l'objet, voire de
l'empêcher d'utiliser les propriétés qu'il a activées. Vous pouvez
choisir un objectif spécial ou faire un jet sur la table suivante.

  ------------------------------------------------------------------------
   d10  Objectif
  ----- ------------------------------------------------------------------
    1   *Aligné :* L'objet cherche à vaincre ou à détruire ceux d'un
        alignement diamétralement opposé. (Un tel objet n'est jamais
        neutre).

    2   *Fléau :* l'objet cherche à vaincre ou à détruire des créatures
        d'un type particulier, comme les fiélons, les métamorphes, les
        trolls ou les magiciens.

    3   *Protecteur :* L'objet cherche à défendre une origine ou un type de
        créature particulier, comme les elfes ou les druides.

    4   *Croisé :* L'objet cherche à vaincre, affaiblir ou détruire les
        serviteurs d'une divinité particulière.

    5   *Templier :* L'objet cherche à défendre les serviteurs et les
        intérêts d'une divinité particulière.

    6   *Destructeur :* L'objet a soif de destruction et incite son
        utilisateur à se battre de façon arbitraire.

    7   *Chercheur de gloire :* L'objet cherche à être reconnu comme le
        plus grand objet magique du monde, en faisant de son utilisateur
        un personnage célèbre ou notoire.

    8   *Chercheur de lore :* L'objet a soif de connaissances ou est
        déterminé à résoudre un mystère, à apprendre un secret ou à
        démêler une prophétie cryptique.

    9   *Chercheur de destin :* L'objet est convaincu que lui et son
        possesseur ont des rôles clés à jouer dans les événements futurs.

   10   *Chercheur de créateur :* L'objet cherche son créateur et veut
        comprendre pourquoi il a été créé.
  ------------------------------------------------------------------------




### Conflit

Un objet sensible a une volonté propre, façonnée par sa personnalité et
son alignement. Si son porteur agit d'une manière opposée à
l'alignement ou au but de l'objet, un conflit peut survenir.
Lorsqu'un tel conflit se produit, l'objet effectue un test de Charisme
contesté par le test de Charisme du porteur. Si l'objet gagne le
concours, il fait une ou plusieurs des demandes suivantes :

-   L'objet insiste pour être porté en permanence.
-   L'objet exige que son porteur se débarrasse de tout ce qu'il
    trouve répugnant.
-   L'objet exige que son porteur poursuive les objectifs de l'objet à
    l'exclusion de tout autre objectif.
-   L'objet demande à être donné à quelqu'un d'autre.

Si son porteur refuse de se conformer aux souhaits de l'objet, ce
dernier peut faire tout ou partie des choses suivantes :

-   Rendre impossible à son détenteur de s'y accorder.
-   Supprimer une ou plusieurs de ses propriétés activées.
-   Tenter de prendre le contrôle de son porteur.

Si un objet sensible tente de prendre le contrôle de son porteur, ce
dernier doit effectuer un jet de sauvegarde contre le Charisme, avec un
DC égal à 12 + le modificateur de Charisme de l'objet. En cas d'échec,
le porteur est charmé par l'objet pendant 1d12 heures. Pendant qu'il
est charmé, le porteur doit essayer de suivre les ordres de l'objet. Si
le porteur subit des dégâts, il peut répéter le jet de sauvegarde,
mettant fin à l'effet en cas de succès. Que la tentative de contrôle de
son utilisateur réussisse ou échoue, l'objet ne peut plus utiliser ce
pouvoir avant la prochaine aube.




## Artefacts


### Orbe du Wyrm

*Objet merveilleux, artefact (nécessite d'être accordé)*

Il y a bien longtemps, les elfes et les humains menaient une guerre
terrible contre des dragons maléfiques. Alors que le monde semblait
condamné, de puissants magiciens se réunirent et utilisèrent leur plus
grande magie, forgeant cinq *orbes du Wyrm* (ou *orbes du Wyrm*) pour
les aider à vaincre les dragons. Un orbe a été transporté dans chacune
des cinq tours des magiciens, où il a été utilisé pour accélérer la
guerre vers une fin victorieuse. Les magiciens utilisaient les orbes
pour attirer les dragons vers eux, puis les détruisaient grâce à une
puissante magie.

Avec la chute des tours de magiciens, les orbes ont été détruites ou
sont entrées dans la légende, et seules trois d'entre elles ont
survécu. Leur magie a été déformée au cours des siècles, et bien que
leur but premier, qui est d'appeler les dragons, soit toujours
d'actualité, elles permettent également de contrôler les dragons.

Chaque orbe contient l'essence d'un dragon maléfique, une présence qui
ne supporte pas qu'on tente de lui soutirer de la magie. Ceux qui
manquent de force de caractère peuvent se retrouver asservis à un orbe.

Un orbe est un globe de cristal gravé d'environ 10 pouces de diamètre.
Lorsqu'il est utilisé, il atteint un diamètre d'environ 20 pouces et
de la brume tourbillonne à l'intérieur.

Lorsque vous êtes lié à un orbe, vous pouvez utiliser une action pour
regarder dans les profondeurs de l'orbe et prononcer son mot
d'Injonction. Vous devez alors effectuer un test de Charisme DC 15. Si
vous réussissez, vous contrôlez l'orbe aussi longtemps que vous restez
lié à lui. En cas d'échec, vous êtes charmé par l'orbe tant que vous
restez lié à lui.

Tant que vous êtes charmé par l'orbe, vous ne pouvez pas mettre fin
volontairement à votre accord avec lui, et l'orbe vous lance des
*suggestions*, vous incitant
à travailler aux fins maléfiques qu'il désire. L'essence de dragon
contenue dans l'orbe peut vouloir beaucoup de choses :
l'anéantissement d'un peuple particulier, se libérer de l'orbe,
répandre la souffrance dans le monde, faire progresser le culte d'un
dieu dragon, ou autre chose que le MJ décide.

***Propriétés aléatoires.*** Un *Orbe du Wyrm* a les propriétés
aléatoires suivantes :

-   2 propriétés bénéfiques mineures
-   1 propriété de préjudice mineur
-   1 propriété préjudiciable majeure

***Sorts.*** L'orbe possède 7 charges et regagne 1d4 + 3 charges
dépensées quotidiennement à l'aube. Si vous contrôlez l'orbe, vous
pouvez utiliser une action et dépenser 1 ou plusieurs charges pour
lancer l'un des sorts suivants (sauvegarde DC 18) à partir de celui-ci
: *soins*, *lumière
du jour*, *protection contre la
mort*.

Vous pouvez également utiliser une action pour lancer le sort de
*détection de la magie* à partir de l'orbe sans
utiliser de charges.

***Appelez les dragons.*** Lorsque vous contrôlez l'orbe, vous pouvez
utiliser une action pour que l'artefact émette un appel télépathique
qui s'étend dans toutes les directions sur 40 miles. Les dragons
maléfiques à portée se sentent obligés de venir à l'orbe dès que
possible par la route la plus directe. Les divinités dragons ne sont pas
affectées par cet appel. Les dragons attirés par l'orbe peuvent se
montrer hostiles envers vous pour les avoir contraints contre leur gré.
Une fois que vous avez utilisé cette propriété, elle ne peut plus être
utilisée pendant 1 heure.

***Détruire une orbe.*** Un *orbe du Wyrm* semble fragile mais est
imperméable à la plupart des dégâts, y compris les attaques et les armes
à souffle des dragons. Un sort de *désintégration* ou
un bon coup d'une arme magique +3 suffit à détruire un orbe, cependant.


