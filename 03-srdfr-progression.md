
# Au-delà du 1er niveau


## Avancement du caractère

Au fur et à mesure que votre personnage vit des aventures et surmonte
des épreuves, il acquiert de l'expérience, représentée par des points
d'expérience. Un personnage qui atteint un certain nombre de points
d'expérience progresse en capacité. Cette progression s'appelle le
**gain d'un niveau**.

Lorsque votre personnage gagne un niveau, sa classe lui accorde souvent
des capacités supplémentaires, détaillées dans la description de la
classe. Certaines de ces capacités vous permettent d'augmenter vos
scores de compétence, soit en augmentant deux scores de 1 chacun, soit
en augmentant un score de 2. Vous ne pouvez pas augmenter un score de
compétence au-delà de 20. En outre, le bonus de maîtrise de chaque
personnage augmente à certains niveaux.

Chaque fois que vous gagnez un niveau, vous gagnez 1 Dé de Vie
supplémentaire. Lancez ce dé, ajoutez-y votre modificateur de
Constitution, et ajoutez le total à votre maximum de points de vie. Vous
pouvez également utiliser la valeur fixe indiquée dans votre fiche de
classe, qui correspond au résultat moyen du jet de dé (arrondi au
chiffre supérieur).

Lorsque votre modificateur de Constitution augmente de 1, votre maximum
de points de vie augmente de 1 pour chaque niveau atteint. Par exemple,
si votre guerrier de 7e niveau a un score de Constitution de 18,
lorsqu'il atteint le 8e niveau, il augmente son score de Constitution
de 17 à 18, faisant ainsi passer son modificateur de Constitution de +3
à +4. Son maximum de points de vie augmente alors de 8.

Le tableau d'avancement du personnage récapitule l'XP dont vous avez
besoin pour progresser en niveaux du niveau 1 au niveau 20, ainsi que le
bonus de maîtrise pour un personnage de ce niveau. Consultez les
informations de la description de classe de votre personnage pour voir
quelles autres améliorations vous gagnez à chaque niveau.

  ----------------------------------------------
  Points              Niveau  Bonus de maîtrise
  d'expérience                 supplémentaire
  ------------------ -------- ------------------
  0                     1             +2

  300                   2             +2

  900                   3             +2

  2,700                 4             +2

  6,500                 5             +3

  14,000                6             +3

  23,000                7             +3

  34,000                8             +3

  48,000                9             +4

  64,000                10            +4

  85,000                11            +4

  100,000               12            +4

  120,000               13            +5

  140,000               14            +5

  165,000               15            +5

  195,000               16            +5

  225,000               17            +6

  265,000               18            +6

  305,000               19            +6

  355,000               20            +6
  ----------------------------------------------

  : Avancement du caractère



## Multiclassage

Le multiclassage vous permet de gagner des niveaux dans plusieurs
classes. Cela vous permet de mélanger les capacités de ces classes afin
de réaliser un concept de personnage qui pourrait ne pas être reflété
dans l'une des options de classe standard.

Avec cette règle, vous avez la possibilité de gagner un niveau dans une
nouvelle classe chaque fois que vous avancez en niveau, au lieu de
gagner un niveau dans votre classe actuelle. Vos niveaux dans toutes vos
classes sont additionnés pour déterminer votre niveau de personnage. Par
exemple, si vous avez trois niveaux en magicien et deux en guerrier,
vous êtes un personnage de 5e niveau.

Au fur et à mesure que vous progressez en termes de niveaux, vous pouvez
rester membre de votre classe d'origine et passer quelques niveaux dans
une autre classe, ou changer complètement de cap, sans jamais regarder
la classe que vous avez laissée derrière vous. Vous pouvez même
commencer à progresser dans une troisième ou une quatrième classe. Par
rapport à un personnage mono-classe de même niveau, vous sacrifiez un
peu de concentration en échange de la polyvalence.


### Prérequis

Pour vous qualifier pour une nouvelle classe, vous devez remplir les
prérequis en termes de valeur caractéristique pour votre classe actuelle
et votre nouvelle classe, comme indiqué dans le tableau des prérequis du
multiclassage. Par exemple, un barbare qui décide de se reclasser dans
la classe des druides doit avoir des scores de Force et de Sagesse de 13
ou plus. Sans l'entraînement complet que reçoit un personnage débutant,
vous devez apprendre rapidement dans votre nouvelle classe, en ayant une
aptitude naturelle qui se traduit par des scores de capacité supérieurs
à la moyenne.

  ----------------------------------------
  Classe        Valeur de caractéristique
                minimum
  ------------- --------------------------
  Barbare       Force 13

  Barde      Charisme 13

  Clerc         Sagesse 13

  Druide        Sagesse 13

  Guerrier      Force 13 ou Dextérité 13

  Moine         Dextérité 13 et Sagesse 13

  Paladin       Force 13 et Charisme 13

  Rôdeur        Dextérité 13 et Sagesse 13

  Roublard      Dextérité 13

  Sorcier       Charisme 13

  Invocateur   Charisme 13

  Magicien      Intelligence 13
  ----------------------------------------

  : Prérequis pour le multiclassage



### Points d'expérience

Le coût en points d'expérience pour gagner un niveau est toujours basé
sur votre niveau de personnage total, comme indiqué dans le tableau
d'avancement du personnage, et non sur votre niveau dans une classe
particulière. Ainsi, si vous êtes un clerc 6/combattant 1, vous devez
gagner suffisamment d'XP pour atteindre le 8e niveau avant de pouvoir
prendre votre deuxième niveau en tant que combattant ou votre septième
niveau en tant que clerc.



### Points de vie et dés de réussite

Vous gagnez les points de vie de votre nouvelle classe comme décrit pour
les niveaux après le 1er. Vous gagnez les points de vie du 1er niveau
d'une classe uniquement lorsque vous êtes un personnage de 1er niveau.

Vous additionnez les dés de chocs accordés par toutes vos classes pour
former votre pool de dés de chocs. Si les dés de combat sont du même
type, vous pouvez simplement les regrouper. Par exemple, le guerrier et
le paladin ont tous deux un d10, donc si vous êtes un paladin
5/combattant 5, vous avez dix dés de touche d10. Si vos classes vous
donnent des dés de points de vie de différents types, tenez-en compte
séparément. Si vous êtes un paladin 5/clerc 5, par exemple, vous avez
cinq dés de coups d10 et cinq dés de coups d8.



### Bonus de maîtrise supplémentaire

Votre bonus de maîtrise est toujours basé sur votre niveau de personnage
total, comme indiqué dans le tableau d'avancement du
personnage, et non sur votre niveau
dans une classe particulière. Par exemple, si vous êtes un guerrier
3/rogue 2, vous avez le bonus de maîtrise d'un personnage de 5e niveau,
soit +3.



### Maîtrises

Lorsque vous gagnez votre premier niveau dans une classe autre que votre
classe initiale, vous ne gagnez que certaines des maîtrises de départ de
la nouvelle classe, comme indiqué dans le tableau des maîtrises du
multiclassage.

  ---------------------------------------------------------------------------
  Classe        Maîtrise acquise
  ------------- -------------------------------------------------------------
  Barbare       Boucliers, armes courantes, armes martiales

  Barde      Armure légère, une compétence de votre choix, un instrument
                de musique de votre choix.

  Clerc         Blindage léger, blindage moyen, boucliers

  Druide        Armure légère, armure moyenne, boucliers (les druides ne
                portent pas d'armure et n'utilisent pas de boucliers en
                métal).

  Guerrier      Armure légère, armure moyenne, boucliers, armes simples,
                armes martiales.

  Moine         Armes courantes, épées courtes

  Paladin       Armure légère, armure moyenne, boucliers, armes simples,
                armes martiales.

  Rôdeur        Armure légère, armure moyenne, boucliers, armes courantes,
                armes martiales, une compétence de la liste des compétences
                de la classe.

  Roublard      Armure légère, une compétence de la liste des compétences de
                la classe, outils de voleur.

  Sorcier       \-

  Invocateur   Armure légère, armes simples.

  magicien      \-
  ---------------------------------------------------------------------------

  : Maîtrise du multiclassique



### Capacités de la classe

Lorsque vous gagnez un nouveau niveau dans une classe, vous obtenez ses
caractéristiques pour ce niveau. Cependant, vous ne recevez pas
l'équipement de départ de la classe, et quelques capacités ont des
règles supplémentaires lorsque vous êtes en multiclasse : Canalisation
divine, Attaque supplémentaire, Défense sans armure, et Lanceur de
sorts.


#### Canalisation divine

Si vous avez déjà la caractéristique Canalisation divine et que vous
gagnez un niveau dans une classe qui accorde également cette
caractéristique, vous gagnez les effets de Canalisation divine accordés
par cette classe, mais le fait d'obtenir à nouveau la caractéristique
ne vous donne pas une utilisation supplémentaire de celle-ci. Vous ne
gagnez des utilisations supplémentaires que lorsque vous atteignez un
niveau de classe qui vous les accorde explicitement. Par exemple, si
vous êtes un clerc 6/paladin 4, vous pouvez utiliser Canalisation divine
deux fois entre les repos parce que vous avez un niveau suffisamment
élevé dans la classe des clercs pour avoir plus d'utilisations. Chaque
fois que vous utilisez cette caractéristique, vous pouvez choisir
n'importe lequel des effets de Canalisation divine disponibles dans vos
deux classes.



#### Attaque supplémentaire

Si vous obtenez le trait de classe Attaque supplémentaire dans plus
d'une classe, les traits ne s'additionnent pas. Vous ne pouvez pas
effectuer plus de deux attaques avec ce trait, sauf si cela est indiqué
(comme la version du guerrier de l'Attaque supplémentaire). De même,
l'invocation eldritch Lame insatiable du invocateur ne vous donne pas
d'attaques supplémentaires si vous avez aussi Attaque supplémentaire.



#### Défense sans armure

Si vous avez déjà la caractéristique Défense sans armure, vous ne pouvez
pas l'obtenir à nouveau d'une autre classe.



#### Incantation

Votre capacité d'incantation dépend en partie de vos niveaux combinés
dans toutes vos classes de lanceur de sorts et en partie de vos niveaux
individuels dans ces classes. Si vous avez la caractéristique de lanceur
de sorts de plus d'une classe, utilisez les règles ci-dessous. Si vous
êtes multi-classe mais que vous avez la caractéristique Incantation
d'une seule classe, vous suivez les règles décrites dans cette classe.

***Sorts connus et préparés.*** Vous déterminez les sorts que vous
connaissez et que vous pouvez préparer pour chaque classe
individuellement, comme si vous étiez un membre à classe unique de cette
classe. Si vous êtes un rôdeur 4/un magicien 3, par exemple, vous
connaissez trois sorts de rôdeur de 1er niveau basés sur vos niveaux
dans la classe de rôdeur. En tant que magicien de 3e niveau, vous
connaissez trois sorts de magicien, et votre livre de sorts contient dix
sorts de magicien, dont deux (les deux que vous avez obtenus lorsque
vous avez atteint le 3e niveau de magicien) peuvent être des sorts de 2e
niveau. Si votre Intelligence est de 16, vous pouvez préparer six sorts
de magicien à partir de votre livre de sorts.

Chaque sort que vous connaissez et préparez est associé à l'une de vos
classes, et vous utilisez la caractéristique d'incantation de cette
classe lorsque vous lancez le sort. De même, une focalisation
d'incantation, comme un symbole sacré, ne peut être utilisée que pour
les sorts de la classe associée à cette focalisation.

***Emplacements de sorts.*** Vous déterminez vos emplacements de sorts
disponibles en additionnant tous vos niveaux dans les classes de barde,
clerc, druide, sorcier et magicien, et la moitié de vos niveaux
(arrondis à l'inférieur) dans les classes de paladin et de rôdeur.
Utilisez ce total pour déterminer vos emplacements de sorts en
consultant la table des lanceurs de sorts multiclasses.

Si vous avez plus d'une classe d'incantation, cette table peut vous
donner des emplacements de sorts d'un niveau supérieur aux sorts que
vous connaissez ou que vous pouvez préparer. Vous pouvez utiliser ces
emplacements, mais uniquement pour lancer vos sorts de niveau inférieur.
Si un sort de niveau inférieur que vous lancez, comme *Mains
brûlantes*, a un effet amélioré lorsqu'il est lancé en
utilisant un emplacement de niveau supérieur, vous pouvez utiliser
l'effet amélioré, même si vous n'avez pas de sorts de ce niveau
supérieur.

Par exemple, si vous êtes le rôdeur 4/magicien 3 mentionné plus haut,
vous comptez comme un personnage de 5e niveau pour déterminer vos
emplacements de sorts : vous avez quatre emplacements de 1er niveau,
trois emplacements de 2e niveau et deux emplacements de 3e niveau.
Cependant, vous ne connaissez pas de sorts de 3ème niveau, ni de sorts
de rôdeur de 2ème niveau. Vous pouvez utiliser les emplacements de sorts
de ces niveaux pour lancer les sorts que vous connaissez - et
éventuellement améliorer leurs effets.

***Magie de pacte.*** Si vous possédez à la fois la caractéristique de
classe Lancer des sorts et la caractéristique de classe Magie de pacte
de la classe des sorciers, vous pouvez utiliser les emplacements de
sorts que vous gagnez grâce à la caractéristique Magie de pacte pour
lancer des sorts que vous connaissez ou que vous avez préparés dans des
classes possédant la caractéristique de classe Lancer des sorts, et vous
pouvez utiliser les emplacements de sorts que vous gagnez grâce à la
caractéristique de classe Lancer des sorts pour lancer des sorts de
sorciers que vous connaissez.

  ---------------------------------------------------------------------------------
   Niveau   Niveau                                                          
             des                                                            
            sorts                                                           
  -------- -------- ------- ------- ------- ------- ------- ------- ------- -------
             1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

    1er       2       \-      \-      \-      \-      \-      \-      \-      \-

    2ème      3       \-      \-      \-      \-      \-      \-      \-      \-

    3ème      4        2      \-      \-      \-      \-      \-      \-      \-

    4ème      4        3      \-      \-      \-      \-      \-      \-      \-

    5ème      4        3       2      \-      \-      \-      \-      \-      \-

    6ème      4        3       3      \-      \-      \-      \-      \-      \-

    7ème      4        3       3       1      \-      \-      \-      \-      \-

    8ème      4        3       3       2      \-      \-      \-      \-      \-

    9ème      4        3       3       3       1      \-      \-      \-      \-

   10ème      4        3       3       3       2      \-      \-      \-      \-

   11ème      4        3       3       3       2       1      \-      \-      \-

   12ème      4        3       3       3       2       1      \-      \-      \-

   13ème      4        3       3       3       2       1       1      \-      \-

   14ème      4        3       3       3       2       1       1      \-      \-

   15ème      4        3       3       3       2       1       1       1      \-

   16ème      4        3       3       3       2       1       1       1      \-

   17ème      4        3       3       3       2       1       1       1       1

   18ème      4        3       3       3       3       1       1       1       1

   19ème      4        3       3       3       3       2       1       1       1

   20ème      4        3       3       3       3       2       2       1       1
  ---------------------------------------------------------------------------------

  : Lanceur de sorts multi-classes : Emplacements de sorts par niveau de
  sort





## Alignement

Une créature typique du monde du jeu a un alignement, qui décrit
largement ses attitudes morales et personnelles. L'alignement est une
combinaison de deux facteurs : l'un identifie la moralité (bon, mauvais
ou neutre), et l'autre décrit les attitudes envers la société et
l'ordre (licite, chaotique ou neutre). Ainsi, neuf alignements
distincts définissent les combinaisons possibles.

Ces brefs résumés des neuf alignements décrivent le comportement typique
d'une créature de cet alignement. Les individus peuvent s'écarter
considérablement de ce comportement typique, et peu de gens sont
parfaitement et constamment fidèles aux préceptes de leur alignement.

On peut compter sur les créatures**légalement bonnes** (LG) pour faire
ce que la société attend d'elles. Les dragons d'or, les paladins et la
plupart des nains sont bons.

Les personnes**neutres bonnes** (NG) font de leur mieux pour aider les
autres en fonction de leurs besoins. De nombreux célestes, certains
géants des nuages et la plupart des gnomes sont bons neutres.

Les créatures**chaotiques bonnes** (CG) agissent selon leur conscience,
sans se soucier des attentes des autres. Les dragons de cuivre, de
nombreux elfes et les licornes sont chaotiquement bons.

Les individus**légalement neutres** (LN) agissent en accord avec la loi,
la tradition ou les codes personnels. De nombreux moines et certains
magiciens sont des neutres légaux.

**Neutre** (N) est l'alignement de ceux qui préfèrent éviter les
questions morales et ne prennent pas parti, faisant ce qui leur semble
le mieux sur le moment. Les Homme-lézards, la plupart des druides et de
nombreux humains sont neutres.

Les créatures**chaotiques neutres** (CN) suivent leurs caprices, tenant
leur liberté personnelle par-dessus tout. De nombreux barbares et
roublards, ainsi que certains bardes, sont chaotiques neutres.

Les créatures**légalement mauvaises** (LE) prennent méthodiquement ce
qu'elles veulent, dans les limites d'un code de tradition, de loyauté
ou d'ordre. Les démons, les dragons bleus et les hobgobelins sont des
malfaiteurs légitimes.

Le**mal neutre** (NE) est l'alignement de ceux qui font tout ce qu'ils
peuvent faire, sans compassion ni scrupules. De nombreux Drow, quelques
géants des nuages et des gobelins sont neutres.

Les créatures**maléfiques chaotiques** (CE) agissent avec une violence
arbitraire, poussées par leur avidité, leur haine ou leur soif de sang.
Les démons, les dragons rouges et les orcs sont chaotiques.


### L'alignement dans le multivers

Pour de nombreuses créatures pensantes, l'alignement est un choix
moral. Les humains, les nains, les elfes et les autres origines humanoïdes
peuvent choisir de suivre les voies du bien ou du mal, de la loi ou du
chaos. Selon le mythe, les dieux de l'alignement du bien qui ont créé
ces origines leur ont donné le libre arbitre pour choisir leur voie morale,
sachant que le bien sans le libre arbitre est de l'esclavage.

Les divinités maléfiques qui ont créé d'autres origines, cependant, ont
créé ces origines pour les servir. Ces origines ont de fortes tendances innées
qui correspondent à la nature de leurs dieux. La plupart des orcs
partagent la nature violente et sauvage des dieux orcs, et sont donc
enclins au mal. Même si un orc choisit une bonne orientation, il lutte
toute sa vie contre ses tendances innées. (Même les demi-orques
ressentent l'influence persistante du dieu orc).

L'alignement est une partie essentielle de la nature des célestes et
des fiélons. Un démon ne choisit pas d'être un mal légal, et il ne tend
pas vers le mal légal, mais il est plutôt un mal légal dans son essence.
S'il cessait de l'être, il cesserait d'être un démon.

La plupart des créatures qui n'ont pas la capacité de penser
rationnellement n'ont pas d'alignement - elles sont **sans
alignement**. Une telle créature est incapable de faire un choix moral
ou éthique et agit selon sa nature bestiale. Les requins sont des
prédateurs sauvages, par exemple, mais ils ne sont pas mauvais ; ils ne
sont pas alignés.




## Langues

Votre origine indique les langues que votre personnage peut parler par
défaut, et votre historique peut vous donner accès à une ou plusieurs
langues supplémentaires de votre choix. Notez ces langues sur votre
feuille de personnage.

Choisissez vos langues dans la table des langues standard, ou
choisissez-en une qui est commune dans votre campagne. Avec l'accord de
votre MJ, vous pouvez choisir une langue de la table Langues exotiques
ou une langue secrète, comme le jargon des voleurs ou la langue des
druides.

  --------------------------------------------
  Langue       Haut-parleurs        Script
               typiques             
  ------------ -------------------- ----------
  Roturier     Humains              Roturier

  Nain         Nains                Nain

  Elfe         Elfes                Elfe

  Géant        Ogres, géants        Nain

  Gnome        Gnomes               Nain

  Gobelin      Goblinoïdes          Nain

  Halfelin     Halflings            Roturier

  Orc          Orcs                 Nain
  --------------------------------------------

  : Langues standard

  -----------------------------------------------
  Langue        Haut-parleurs        Script
                typiques             
  ------------- -------------------- ------------
  Abysses       Démons               Infernal

  Céleste       Célestes             Céleste

  Draconique    Dragons, drakonides   Draconique

  Parlé des     Aboleths, Manteleurs \-
  profondeurs                        

  Infernal      Diables              Infernal

  Primordial    Élémentaires         Nain

  Sylvestre     Créatures fées       Elfe

  Commun des    Les commerçants du   Elfe
  profondeurs   monde souterrain     
  -----------------------------------------------

  : Langues exotiques

Certaines de ces langues sont en fait des familles de langues comportant
de nombreux dialectes. Par exemple, la langue primordiale comprend les
dialectes auran, aquatique, glaiseux et terrien, un pour chacun des
quatre plans élémentaires. Les créatures qui parlent différents
dialectes d'une même langue peuvent communiquer entre elles.



## Historiques

Toute histoire a un début. L'historique de votre personnage révèle
d'où vous venez, comment vous êtes devenu un aventurier, et votre place
dans le monde. Votre guerrier peut avoir été un chevalier courageux ou
un soldat grisonnant. Votre magicien a pu être un sage ou un artisan.
Votre roublard peut s'être débrouillé en tant que voleur de guilde ou
avoir attiré les foules en tant que bouffon.

Le choix d'un historique vous fournit des indices importants sur
l'identité de votre personnage. La question la plus importante à poser
sur votre historique est : qu'est-ce qui a changé ? Pourquoi avez-vous
cessé de faire ce que votre passé décrit et commencé à partir à
l'aventure ? Où avez-vous trouvé l'argent pour acheter votre
équipement de départ, ou, si vous venez d'un milieu aisé, pourquoi
n'avez-vous pas plus d'argent ? Comment avez-vous appris les
compétences de votre classe ? Qu'est-ce qui vous distingue des gens
ordinaires qui partagent votre passé ?

Les historiques fournissent à la fois des avantages concrets (capacités,
maîtrises et langues) et des suggestions pour le jeu de rôle.


### Les composantes d'un historique


#### Maîtrises

Chaque historique donne au personnage la maîtrise de deux compétences.

En outre, la plupart des historiques donnent au personnage la maîtrise
d'un ou plusieurs outils .

Si un personnage doit acquérir la même maîtrise à partir de deux sources
différentes, il peut choisir une maîtrise différente du même type
(compétence ou outil) à la place.



#### Langues

Certains historiques permettent également aux personnages d'apprendre
d'autres langues que celles données par la origine.



#### Équipement

Chaque historique fournit un ensemble d'équipement de départ. Si vous
utilisez la règle optionnelle pour dépenser des pièces en équipement,
vous ne recevez pas l'équipement de départ de votre historique.



#### Caractéristiques suggérées

Un historique contient des suggestions de caractéristiques personnelles
basées sur vos antécédents. Vous pouvez choisir des caractéristiques,
lancer des dés pour les déterminer au hasard ou vous inspirer des
suggestions pour créer vos propres caractéristiques.



#### Personnaliser un historique

Vous pouvez modifier certaines des capacités d'un historique pour
qu'il corresponde mieux à votre personnage ou au contexte de la
campagne. Pour personnaliser un personnage, vous pouvez remplacer une
caractéristique par n'importe quelle autre, choisir deux compétences,
et choisir un total de deux compétences d'outils ou langues parmi les
exemples d'arrière-plans. Vous pouvez soit utiliser l'équipement de
votre personnage, soit dépenser de l'argent pour vous équiper comme
indiqué dans la section sur l'équipement. (Si vous dépensez des pièces,
vous ne pouvez pas prendre également le paquet d'équipement suggéré
pour votre classe). Enfin, choisissez deux traits de personnalité, un
idéal, un lien et un défaut. Si vous ne trouvez pas de trait
correspondant à votre historique, travaillez avec votre MJ pour en créer
un.




### Acolyte

Vous avez passé votre vie au service d'un temple dédié à un dieu
spécifique ou à un panthéon de dieux. Vous agissez comme un
intermédiaire entre le royaume des saints et le monde des mortels, en
accomplissant des rites sacrés et en offrant des sacrifices afin de
conduire les adorateurs en présence du divin. Vous n'êtes pas
nécessairement un clerc - exécuter des rites sacrés n'est pas la même
chose que canaliser la puissance divine.

Choisissez un dieu, un panthéon de dieux ou un autre être quasi-divin
parmi ceux listés dans \"Panthéons\" ou ceux
spécifiés par votre MJ, et travaillez avec votre MJ pour détailler la
nature de votre service religieux. Étiez-vous un petit fonctionnaire
d'un temple, élevé dès l'enfance pour assister les prêtres dans les
rites sacrés ? Ou étiez-vous un grand prêtre qui a soudainement ressenti
un appel à servir son dieu d'une manière différente ? Peut-être
étiez-vous le chef d'un petit culte en dehors de la structure établie
d'un temple, ou même d'un groupe occulte qui servait un maître
diabolique que vous reniez maintenant.

**Compétences en matière de maîtrise :** Intuition, Religion

**Langues :** Deux de votre choix

**Equipement :** Un symbole sacré (qui vous a été offert lorsque vous
êtes entré dans la prêtrise), un livre de prières ou un moulin à
prières, 5 bâtons d'encens, des vêtements, un ensemble de vêtements
communs et une pochette contenant 15 gp.


#### Reportage : Abri du fidèle

En tant qu'acolyte, vous inspirez le respect à ceux qui partagent votre
foi et vous pouvez accomplir les cérémonies religieuses de votre
divinité. Vous et vos compagnons d'aventure pouvez vous attendre à
recevoir gratuitement des soins et des guérisons dans un temple, un
sanctuaire ou toute autre présence établie de votre foi, mais vous devez
fournir tous les composants matériels nécessaires aux sorts. Ceux qui
partagent votre religion vous soutiendront (mais seulement vous) à un
niveau de vie modeste.

Vous pouvez également avoir des liens avec un temple spécifique dédié à
la divinité ou au panthéon de votre choix, et y avoir une résidence. Il
peut s'agir du temple où vous serviez auparavant, si vous restez en
bons termes avec lui, ou d'un temple où vous avez trouvé un nouveau
foyer. Lorsque vous êtes près de votre temple, vous pouvez demander
l'aide des prêtres, à condition que l'aide que vous demandez ne soit
pas dangereuse et que vous restiez en bons termes avec votre temple.



#### Caractéristiques suggérées

Les acolytes sont façonnés par leur expérience dans les temples ou
autres communautés religieuses. Leur étude de l'histoire et des
principes de leur foi et leurs relations avec les temples, les
sanctuaires ou les hiérarchies affectent leurs manières et leurs idéaux.
Leurs défauts peuvent être une hypocrisie cachée ou une idée hérétique,
ou encore un idéal ou un lien poussé à l'extrême.

  -----------------------------------------------------------------------
   d8  Trait de personnalité
  ---- ------------------------------------------------------------------
   1   J'idolâtre un héros particulier de ma foi, et je me réfère
       constamment aux actes et à l'exemple de cette personne.

   2   Je peux trouver un terrain commun entre les ennemis les plus
       féroces, en faisant preuve d'empathie à leur égard et en œuvrant
       toujours pour la paix.

   3   Je vois des présages dans chaque événement et action. Les dieux
       essaient de nous parler, nous devons juste les écouter.

   4   Rien ne peut ébranler mon attitude optimiste.

   5   Je cite (ou cite mal) des textes sacrés et des proverbes dans
       presque toutes les situations.

   6   Je suis tolérant (ou intolérant) envers les autres religions et je
       respecte (ou condamne) le culte d'autres dieux.

   7   J'ai apprécié la bonne chère, les boissons et la haute société
       parmi l'élite de mon temple. La vie rude m'agace.

   8   J'ai passé tellement de temps dans le temple que j'ai peu
       d'expérience pratique pour traiter avec les gens du monde
       extérieur.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   d6  Idéal
  ---- ------------------------------------------------------------------
   1   La tradition. Les anciennes traditions de culte et de sacrifice
       doivent être préservées et défendues. (Légitime)

   2   La charité. J'essaie toujours d'aider ceux qui sont dans le
       besoin, quel qu'en soit le coût personnel. (Bon)

   3   Le changement. Nous devons aider à apporter les changements que
       les dieux opèrent constamment dans le monde. (Chaotique)

   4   Le pouvoir. J'espère un jour m'élever au sommet de la hiérarchie
       religieuse de ma foi. (Légitime)

   5   La foi. J'ai confiance que ma divinité guidera mes actions. J'ai
       la foi que si je travaille dur, les choses iront bien. (Légitime)

   6   Aspiration. Je cherche à me montrer digne de la faveur de mon dieu
       en comparant mes actions à ses enseignements. (N'importe lequel)
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   d6  Lien
  ---- ------------------------------------------------------------------
   1   Je mourrais pour retrouver une ancienne relique de ma foi perdue
       depuis longtemps.

   2   Un jour, je me vengerai de la hiérarchie corrompue du temple qui
       m'a traité d'hérétique.

   3   Je dois ma vie au Prêtre qui m'a recueilli à la mort de mes
       parents.

   4   Tout ce que je fais est pour le commun des mortels.

   5   Je ferai tout pour protéger le temple où j'ai servi.

   6   Je cherche à préserver un texte sacré que mes ennemis considèrent
       comme hérétique et cherchent à détruire.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   d6  Défaut
  ---- ------------------------------------------------------------------
   1   Je juge les autres sévèrement, et moi-même encore plus sévèrement.

   2   Je fais trop confiance à ceux qui détiennent le pouvoir dans la
       hiérarchie de mon temple.

   3   Ma piété m'amène parfois à faire aveuglément confiance à ceux qui
       professent leur foi en mon dieu.

   4   Je suis inflexible dans ma façon de penser.

   5   Je me méfie des étrangers et j'attends le pire d'eux.

   6   Une fois que j'ai choisi un objectif, je deviens obsédé par
       celui-ci au détriment de tout le reste de ma vie.
  -----------------------------------------------------------------------


