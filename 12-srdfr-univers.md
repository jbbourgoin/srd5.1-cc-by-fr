
# Les plans d'existence

Le cosmos regorge d'une multitude de mondes ainsi que d'une myriade de
dimensions alternatives de la réalité, appelées les **plans
d'existence.** Il englobe tous les mondes où les MJ organisent leurs
aventures, tous dans le domaine relativement banal du plan matériel.
Au-delà de ce plan se trouvent des domaines de matière et d'énergie
élémentaires brutes, des royaumes de pensée pure et d'ethos, les
maisons des démons et des anges, et les dominations des dieux.

De nombreux sorts et objets magiques peuvent tirer de l'énergie de ces
plans, invoquer les créatures qui les habitent, communiquer avec leurs
habitants et permettre aux aventuriers d'y voyager. Au fur et à mesure
que votre personnage gagne en puissance et en niveau, vous pouvez
marcher dans des rues faites de feu solide ou tester votre courage sur
un champ de bataille où les morts ressuscitent à chaque aube.


## Le plan matériel

Le plan matériel est le point de rencontre où les forces philosophiques
et élémentaires qui définissent les autres plans entrent en collision
dans l'existence désordonnée de la vie des mortels et de la matière
ordinaire. Tous les mondes du jeu fantastique existent dans le plan
matériel, ce qui en fait le point de départ de la plupart des campagnes
et des aventures. Le reste du multivers est défini par rapport au plan
matériel.

Les mondes du plan matériel sont infiniment variés, car ils reflètent
l'imagination créative des MJ qui y placent leurs jeux, ainsi que des
joueurs dont les héros s'y aventurent. Ils comprennent des planètes
désertiques dépourvues de magie et des mondes aquatiques parsemés
d'îles, des mondes où la magie se combine à une technologie avancée et
d'autres piégés dans un âge de pierre sans fin, des mondes où les dieux
marchent et des lieux qu'ils ont abandonnés.



## Au-delà du matériel

Au-delà du plan matériel, les différents plans d'existence sont des
royaumes de mythes et de mystères. Ce ne sont pas simplement d'autres
mondes, mais des qualités d'être différentes, formées et gouvernées par
des principes spirituels et élémentaires abstraits du monde ordinaire.


### Voyage planaire

Lorsque les aventuriers voyagent dans d'autres plans d'existence, ils
entreprennent un voyage légendaire à travers les seuils de l'existence
vers une destination mythique où ils s'efforcent d'accomplir leur
quête. Un tel voyage fait partie de la légende. Braver les royaumes des
morts, rechercher les serviteurs célestes d'une divinité ou négocier
avec un Éfritte dans sa ville natale feront l'objet de chansons et
d'histoires pendant des années.

Le voyage vers les plans au-delà du plan matériel peut être accompli de
deux façons : en lançant un sort ou en utilisant un portail planaire.

***Sorts.*** Un certain nombre de sorts permettent d'accéder
directement ou indirectement à d'autres plans d'existence. Changement
*de plan* peuvent transporter les
aventuriers directement dans n'importe quel autre plan d'existence,
avec différents degrés de précision. Forme *éthérée*
permet aux aventuriers d'entrer dans le plan éthéré et de voyager de là
vers n'importe lequel des plans qu'il touche - comme les plans
élémentaires. Enfin, le sort de *projection
astrale* permet aux aventuriers de se projeter dans
le plan astral et de voyager dans les plans extérieurs.

***Portails.*** Un portail est un terme général pour désigner une
connexion interplanaire stationnaire qui relie un endroit spécifique sur
un plan à un endroit spécifique sur un autre. Certains portails
ressemblent à des portes, à des fenêtres claires ou à des passages
enveloppés de brouillard, et le simple fait de les franchir provoque le
voyage interplanaire. D'autres sont des lieux - cercles de pierres
dressées, tours flamboyantes, voiliers ou même des villes entières - qui
existent dans plusieurs plans à la fois ou passent d'un plan à l'autre
à tour de rôle. Certains sont des vortex, qui relient généralement un
plan élémentaire à un emplacement très similaire sur le plan matériel,
comme le cœur d'un volcan (menant au plan du feu) ou les profondeurs de
l'océan (vers le plan de l'eau).



### Plans transitifs

Le plan éthéré et le plan astral sont appelés les **plans transitifs**.
Ce sont des royaumes sans caractéristiques qui servent principalement à
voyager d'un plan à l'autre. Des sorts tels que Forme
*éthérée*
permettent aux personnages de pénétrer dans ces plans et de les
traverser pour atteindre les plans au-delà.

Le **plan éthéré** est une dimension brumeuse, entourée de brouillard,
parfois décrite comme un grand océan. Ses rivages, appelés frontière
éthérée, chevauchent le plan matériel et les plans intérieurs, de sorte
que chaque emplacement sur ces plans a un emplacement correspondant sur
le plan éthéré. Certaines créatures peuvent voir dans la frontière
éthérée, et les sorts Voir l'*invisible* et Vision
*suprême* leur confèrent cette capacité. Certains effets
magiques s'étendent également du plan matériel à la frontière éthérée,
en particulier les effets qui utilisent l'énergie de la force comme le
*Cage de force* force.
Les profondeurs du plan, les Profondeurs éthérées, sont une région de
brumes tourbillonnantes et de brouillards colorés.

Le **plan astral** est le royaume de la pensée et du songe, où les
visiteurs voyagent en tant qu'âmes désincarnées pour atteindre les
plans du divin et du démoniaque. C'est une grande mer argentée, la même
en haut et en bas, avec des volutes de blanc et de gris qui se faufilent
parmi des grains de lumière ressemblant à des étoiles lointaines. Des
tourbillons de couleur erratiques vacillent dans l'air comme des pièces
de monnaie qui tournent. On y trouve parfois des morceaux de matière
solide, mais la majeure partie du plan astral est un domaine ouvert et
sans fin.



### Plans intérieurs

Les plans intérieurs entourent et enveloppent le plan matériel et ses
échos, fournissant la substance élémentaire brute à partir de laquelle
tous les mondes ont été créés. Les quatre **plans élémentaires -
l'air**, la terre, le feu et l'eau - forment un anneau autour du plan
matériel, suspendu dans le **chaos primordial**.

Sur leurs bords les plus internes, là où ils sont les plus proches du
plan matériel (dans un sens conceptuel sinon géographique littéral), les
quatre plans élémentaires ressemblent à un monde dans le plan matériel.
Les quatre éléments se mêlent ensemble comme ils le font dans le plan
matériel, formant la terre, la mer et le ciel. Plus loin du plan
matériel, cependant, les plans élémentaires sont à la fois étrangers et
hostiles. Ici, les éléments existent dans leur forme la plus pure : de
grandes étendues de terre solide, de feu ardent, d'eau cristalline et
d'air pur. Ces régions sont peu connues, si bien que lorsqu'on parle
du plan du feu, par exemple, on se limite généralement à la région
frontalière. Aux confins des plans intérieurs, les éléments purs se
dissolvent et se mélangent en un tumulte sans fin d'énergies et de
substances qui s'entrechoquent, le chaos primordial.



### Plans extérieurs

Si les plans intérieurs sont la matière brute et l'énergie qui
composent le multivers, les plans extérieurs sont la direction, la
pensée et le but de cette construction. C'est pourquoi de nombreux
sages appellent les plans extérieurs \"plans divins\", \"plans
spirituels\" ou \"plans divins\", car les plans extérieurs sont surtout
connus pour être la demeure des divinités.

Lorsqu'on discute de quoi que ce soit en rapport avec les divinités, le
langage utilisé doit être hautement métaphorique. Leurs demeures
actuelles ne sont pas littéralement des \"lieux\", mais illustrent
l'idée que les plans extérieurs sont des royaumes de la pensée et de
l'esprit. Comme pour les plans élémentaires, on peut imaginer la partie
perceptible des plans extérieurs comme une sorte de région frontière,
tandis que de vastes régions spirituelles se trouvent au-delà de
l'expérience sensorielle ordinaire.

Même dans ces régions perceptibles, les apparences peuvent être
trompeuses. Au départ, de nombreux plans extérieurs semblent
accueillants et familiers aux natifs du plan matériel. Mais le paysage
peut changer selon les caprices des forces puissantes qui vivent sur les
plans extérieurs. Les désirs des forces puissantes qui habitent ces
plans peuvent les remodeler complètement, effaçant et reconstruisant
effectivement l'existence elle-même pour mieux répondre à leurs propres
besoins.

La distance est un concept pratiquement dénué de sens sur les plans
extérieurs. Les régions perceptibles des plans semblent souvent très
petites, mais elles peuvent aussi s'étendre à ce qui semble être
l'infini. Il est possible de faire une visite guidée de l'enfer en une
seule journée, si les puissances de l'enfer le souhaitent. Ou bien, il
peut falloir des semaines aux voyageurs pour faire un voyage éreintant à
travers une seule région.

Les plans extérieurs les plus connus sont un groupe de seize plans qui
correspondent aux huit alignements (à l'exception de la neutralité) et
aux nuances qui les distinguent.


#### Alignement planaire

Les plans dont la nature comporte un certain élément de bien sont
appelés les **plans supérieurs.** Les créatures célestes telles que les
anges et les pégases vivent dans les plans supérieurs. Les plans
comportant un élément de mal sont les **plans inférieurs.** Les fiélons
tels que les démons et les diables vivent dans les plans inférieurs.
L'alignement d'un plan est son essence, et un personnage dont
l'alignement ne correspond pas à celui du plan éprouve un profond
sentiment de dissonance. Lorsqu'une créature bonne visite un plan
supérieur neutre d'alignement bon, par exemple, elle se sent en accord
avec le plan, mais une créature mauvaise se sent en désaccord et plus
que mal à l'aise.



#### Demi-plans

Les Demi-plans sont de petits espaces extradimensionnels avec leurs
propres règles. Ce sont des morceaux de réalité qui ne semblent avoir
leur place nulle part ailleurs. Les Demi-plans sont créés de diverses
manières. Certains sont créés par des sorts, comme
*Demi-plan*, ou générés par la volonté d'une divinité
puissante ou d'une autre force. Ils peuvent exister naturellement,
comme une partie de la réalité existante qui a été séparée du reste du
multivers, ou comme un bébé univers qui grandit en puissance. On peut
entrer dans un Demi-plan donné par un point unique où il touche un autre
plan. En théorie, un sort de *changement de plan* peut
également transporter des voyageurs vers un Demi-plan, mais la fréquence
appropriée requise pour le diapason est extrêmement difficile à obtenir.
Le sort de *portail* est plus fiable, à condition que le
lanceur de sorts connaisse le Demi-plan.






# Panthéons


Les panthéons celte, égyptien, grec et nordique sont des interprétations
fantaisistes des religions historiques de notre monde antique. Ils
comprennent les divinités les plus appropriées pour être utilisées dans
un jeu, séparées de leur contexte historique dans le monde réel et
réunies en panthéons qui servent les besoins du jeu.


## Le panthéon celtique

  --------------------------------------------------------------------------------
  Déité                            Alignement  Domaines        Symbole
                                               suggérés        
  ------------------------------- ------------ --------------- -------------------
  Le Daghdha, dieu du temps et         CG      Nature,         Chaudron ou
  des récoltes                                 tromperie       bouclier
                                                               bouillonnant

  Arawn, dieu de la vie et de la       NE      La vie, la mort Étoile noire sur
  mort                                                         fond gris

  Belenus, dieu du soleil, de la       NG      Lumière         Disque solaire et
  lumière et de la chaleur.                                    pierres dressées

  Brigantia, déesse des rivières       NG      La vie          Passerelle
  et du bétail                                                 

  Diancecht, dieu de la médecine       LG      La vie          Branches de chêne
  et de la guérison.                                           et de gui croisées

  Dunatis, dieu des montagnes et       N       Nature          Pic de montagne
  des sommets                                                  coiffé d'un soleil
                                                               rouge

  Goibhniu, dieu des forgerons et      NG      Connaissance,   Maillet géant sur
  de la guérison.                              vie             l'épée

  Lugh, dieu des arts, des             CN      Connaissance,   Paire de longues
  voyages et du commerce                       vie             mains

  Manannan mac Lir, dieu des           LN      Nature, Tempête Vague d'eau
  océans et des créatures marines                              blanche sur le vert

  Math Mathonwy, dieu de la magie      NE      Connaissances   Personnel

  Morrigan, déesse de la bataille      CE      Guerre          Deux lances
                                                               croisées.

  Nuada, dieu de la guerre et des      N       Guerre          Main d'argent sur
  guerriers                                                    fond noir

  Oghma, dieu de la parole et de       NG      Connaissances   Parchemin déroulé
  l'écriture                                                  

  Silvanus, dieu de la nature et       N       Nature          Chêne d'été
  des forêts                                                   
  --------------------------------------------------------------------------------

  : Déités celtiques

On dit que quelque chose de sauvage se cache au cœur de chaque âme, un
espace qui palpite au son du cri des oies la nuit, au murmure du vent
dans les pins, au rouge inattendu du gui sur un chêne - et c'est dans
cet espace que les dieux celtiques résident.

Ils ont surgi du ruisseau et de la rivière, leur puissance étant
renforcée par la force du chêne et la beauté des forêts et de la lande.
Lorsque le premier forestier a osé mettre un nom sur le visage vu dans
le tronc d'un arbre ou sur la voix qui babille dans un ruisseau, ces
dieux se sont imposés.

Les dieux celtes sont aussi souvent servis par les druides que par les
clercs, car ils sont étroitement alignés avec les forces de la nature
que les druides révèrent.



## Le Panthéon égyptien

  --------------------------------------------------------------------------------
  Déité                            Alignement  Domaines        Symbole
                                               suggérés        
  ------------------------------- ------------ --------------- -------------------
  Re-Horakhty, dieu du soleil,         LG      La vie, la      Disque solaire
  chef des dieux.                              lumière         encerclé par un
                                                               serpent

  Anubis, dieu du jugement et de       LN      Décès           Chacal noir
  la mort                                                      

  Apep, dieu du mal, du feu et         NE      Tromperie       Serpent flamboyant
  des serpents.                                                

  Bast, déesse des chats et de la      CG      Guerre          Chat
  vengeance                                                    

  Bès, dieu de la chance et de la      CN      Tromperie       Image de la
  musique                                                      divinité difforme

  Hathor, déesse de l'amour, de       NG      La vie, la      Tête de vache
  la musique et de la maternité.               lumière         cornue avec disque
                                                               lunaire

  Imhotep, dieu de l'artisanat        NG      Connaissances   Pyramide d'étapes
  et de la médecine                                            

  Isis, déesse de la fertilité et      NG      Connaissance,   Ankh et étoile
  de la magie                                  vie             

  Nephtys, déesse de la mort et        CG      Décès           Cornes autour d'un
  du chagrin.                                                  disque lunaire

  Osiris, dieu de la nature et du      LG      Vie, Nature     Crochet et fléau
  monde souterrain.                                            

  Ptah, dieu de l'artisanat, de       LN      Connaissances   Taureau
  la connaissance et des secrets.                              

  Seth, dieu des Ténèbres et des       CE      Mort, Tempête,  Cobra enroulé
  tempêtes du désert.                          Tromperie       

  Sobek, dieu de l'eau et des         LE      Nature, Tempête Tête de crocodile
  crocodiles.                                                  avec cornes et
                                                               plumes

  Thot, dieu de la connaissance        N       Connaissances   Ibis
  et de la sagesse                                             
  --------------------------------------------------------------------------------

  : Divinités égyptiennes

Ces dieux sont une jeune dynastie d'une ancienne famille divine,
héritiers du règne du cosmos et du maintien du principe divin de Maât -
l'ordre fondamental de la vérité, de la justice, de la loi et de
l'ordre qui place les dieux, les pharaons mortels et les hommes et
femmes ordinaires à leur place logique et légitime dans l'univers.

Le panthéon égyptien a la particularité d'avoir trois dieux
responsables de la mort, chacun avec des alignements différents. Anubis
est le dieu neutre de l'au-delà, qui juge les âmes des morts. Seth est
un dieu maléfique chaotique du meurtre, peut-être plus connu pour avoir
tué son frère Osiris. Et Nephtys est une bonne déesse chaotique du
deuil.



## Le Panthéon grec

  --------------------------------------------------------------------------------
  Déité                            Alignement  Domaines        Symbole
                                               suggérés        
  ------------------------------- ------------ --------------- -------------------
  Zeus, dieu du ciel, chef des         N       Tempête         Un poing rempli
  dieux.                                                       d'éclairs.

  Aphrodite, déesse de l'amour        CG      Lumière         Coquillage
  et de la beauté                                              

  Apollon, dieu de la lumière, de      CG      Connaissance,   Lyre
  la musique et de la guérison.                Vie, Lumière    

  Ares, dieu de la guerre et des       CE      Guerre          Lance
  conflits                                                     

  Artémis, déesse de la chasse et      NG      Vie, Nature     Arc et flèche sur
  de l'accouchement                                           le disque lunaire

  Athéna, déesse de la sagesse et      LG      Connaissance,   Chouette
  de la civilisation                           guerre          

  Déméter, déesse de                   NG      La vie          Tête de jument
  l'agriculture                                               

  Dionysos, dieu de la joie et du      CN      La vie          Thyrsus (bâton muni
  vin                                                          d'une pomme de
                                                               pin)

  Hadès, dieu des enfers               LE      Décès           Bélier noir

  Hécate, déesse de la magie et        CE      Connaissance,   Lune couchée
  de la lune.                                  ruse            

  Héphaïstos, dieu de la forge et      NG      Connaissances   Marteau et enclume
  de l'artisanat                                              

  Héra, déesse du mariage et de        CN      Tromperie       Eventail de plumes
  l'intrigue.                                                 de paon

  Hercule, dieu de la force et de      CG      Tempête, guerre Tête de lion
  l'aventure                                                  

  Hermès, dieu du voyage et du         CG      Tromperie       Caducée (bâton ailé
  commerce                                                     et serpents)

  Hestia, déesse du foyer et de        NG      La vie          Foyer
  la famille                                                   

  Nike, déesse de la victoire          LN      Guerre          Femme ailée

  Pan, dieu de la nature               CN      Nature          Syrinx (flûte de
                                                               Pan)

  Poséidon, dieu de la mer et des      CN      Tempête         Trident
  tremblements de terre                                        

  Tyche, déesse de la bonne            N       Tromperie       Pentagramme rouge
  fortune                                                      
  --------------------------------------------------------------------------------

  : Divinités grecques

Les dieux de l'Olympe se font connaître par le doux clapotis des vagues
sur les rivages et le fracas du tonnerre sur les sommets couverts de
nuages. Les bois épais infestés de sangliers et les collines sèches
couvertes d'oliviers témoignent de leur passage. Tous les aspects de la
nature résonnent de leur présence, et ils se sont également fait une
place dans le cœur de l'homme.



## Le Panthéon nordique

  --------------------------------------------------------------------------------
  Déité                            Alignement  Domaines        Symbole
                                               suggérés        
  ------------------------------- ------------ --------------- -------------------
  Odin, dieu de la connaissance        NG      Connaissance,   Regarder l'œil
  et de la guerre                              guerre          bleu

  Aegir, dieu de la mer et des         NE      Tempête         Vagues océaniques
  tempêtes                                                     rugueuses

  Balder, dieu de la beauté et de      NG      La vie, la      Calice en argent
  la poésie                                    lumière         incrusté de pierres
                                                               précieuses

  Forseti, dieu de la justice et       N       Lumière         Tête d'un homme
  du droit                                                     barbu

  Frey, dieu de la fertilité et        NG      La vie, la      Épée à deux mains
  du soleil.                                   lumière         bleu glacier

  Freya, déesse de la fertilité        NG      La vie          Falcon
  et de l'amour                                               

  Frigga, déesse de la naissance       N       La vie, la      Chat
  et de la fertilité                           lumière         

  Heimdall, dieu de la vigilance       LG      Lumière, guerre Corne musicale
  et de la loyauté                                             frisée

  Hel, déesse des enfers.              NE      Décès           Visage d'une
                                                               femme, pourrissant
                                                               d'un côté

  Hermod, dieu de la chance            CN      Tromperie       Rouleau ailé

  Loki, dieu des voleurs et de la      CE      Tromperie       Flamme
  ruse.                                                        

  Njord, dieu de la mer et du          NG      Nature, Tempête Pièce d'or
  vent                                                         

  Odur, dieu de la lumière et du       CG      Lumière         Disque solaire
  soleil.                                                      

  Sif, déesse de la guerre             CG      Guerre          Epée levée

  Skadi, dieu de la terre et des       N       Nature          Sommet de montagne
  montagnes                                                    

  Surtur, dieu des géants du feu       LE      Guerre          Épée flamboyante
  et de la guerre.                                             

  Thor, dieu des tempêtes et du        CG      Tempête, guerre Marteau
  tonnerre.                                                    

  Thrym, dieu des géants du gel        CE      Guerre          Hache blanche à
  et du froid.                                                 double lame

  Tyr, dieu du courage et de la        LN      Connaissance,   Épée
  stratégie                                    guerre          

  Uller, dieu de la chasse et de       CN      Nature          Arc long
  l'hiver                                                     
  --------------------------------------------------------------------------------

  : Déités nordiques

Là où la terre plonge des collines enneigées dans les fjords glacés en
contrebas, là où les chaloupes s'amarrent à la plage, là où les
glaciers avancent et reculent à chaque automne et chaque printemps,
c'est le pays des Vikings, le foyer du panthéon nordique. C'est un
climat brutal, qui exige une vie brutale. Les guerriers de ce pays ont
dû s'adapter aux conditions difficiles pour survivre, mais ils n'ont
pas été trop déformés par les besoins de leur environnement. Compte tenu
de la nécessité de faire des raids pour trouver de la nourriture et des
richesses, il est surprenant que les mortels se soient aussi bien
comportés qu'ils l'ont fait. Leurs pouvoirs reflètent le besoin
qu'avaient ces guerriers d'un leadership fort et d'une action
décisive. Ainsi, ils voient leurs divinités dans chaque coude d'une
rivière, les entendent dans le fracas du tonnerre et le grondement des
glaciers, et les sentent dans la fumée d'une maison longue en feu.

Le panthéon nordique comprend deux grandes familles, les Aesir
(divinités de la guerre et du destin) et les Vanir (dieux de la
fertilité et de la prospérité). Autrefois ennemies, ces deux familles
sont désormais étroitement alliées contre leurs ennemis communs, les
géants (dont les dieux Surtur et Thrym).


