
# Utilisation des valeurs de caractéristique

Six capacités permettent de décrire rapidement les caractéristiques
physiques et mentales de chaque créature :

-   **Force,** mesure de la puissance physique
-   **Dextérité,** mesure de l'agilité
-   **Constitution,** mesure de l'endurance
-   **Intelligence,** mesure du raisonnement et de la mémoire
-   **Sagesse,** mesure de la perception et de l'intuition
-   **charisme,** mesure de la personnalité

Un personnage est-il musclé et perspicace ? Brillant et charmant ? Agile
et robuste ? Les valeurs de caractéristiques définissent ces qualités -
les atouts et les faiblesses d'une créature.

Les trois principaux jets du jeu - le test de capacité, le jet de
sauvegarde et le jet d'attaque - reposent sur les six scores de
capacité. L'introduction du livre décrit la règle de base de ces jets :
lancer un d20, ajouter un modificateur d'aptitude dérivé de l'un des
six scores d'aptitude, et comparer le total à un nombre cible.


## Valeurs de caractéristiques et modificateurs de capacités

Chaque capacité d'une créature possède un score, un nombre qui définit
l'ampleur de cette capacité. Une valeur de caractéristique n'est pas
seulement une mesure des capacités innées, mais englobe également
l'entraînement et la compétence d'une créature dans les activités
liées à cette capacité.

Un score de 10 ou 11 est la moyenne humaine normale, mais les
aventuriers et de nombreux monstres sont bien au-dessus de la moyenne
dans la plupart des capacités. Un score de 18 est le plus élevé qu'une
personne puisse atteindre en général. Les aventuriers peuvent avoir des
scores allant jusqu'à 20, et les monstres et les êtres divins peuvent
avoir des scores allant jusqu'à 30.

Chaque aptitude possède également un modificateur, dérivé du score et
allant de -5 (pour un score d'aptitude de 1) à +10 (pour un score de
30). Le tableau des scores et des modificateurs de capacité indique les
modificateurs de capacité pour la gamme des scores de capacité
possibles, de 1 à 30.

  ----------------------
   Score   Modificateur
  ------- --------------
     1          -5

    2-3         -4

    4-5         -3

    6-7         -2

    8-9         -1

   10-11        +0

   12-13        +1

   14-15        +2

   16-17        +3

   18-19        +4

   20-21        +5

   22-23        +6

   24-25        +7

   26-27        +8

   28-29        +9

    30         +10
  ----------------------

  : Valeurs de caractéristiques et modificateurs de capacités

Pour déterminer un modificateur d'aptitude sans consulter la table,
soustrayez 10 au score d'aptitude, puis divisez le total par 2
(arrondir à l'inférieur).

Parce que les modificateurs d'aptitude affectent presque tous les jets
d'attaque, les contrôles d'aptitude et les jets de sauvegarde, les
modificateurs d'aptitude apparaissent plus souvent en jeu que les
scores qui leur sont associés.



## Avantage et inconvénient

Parfois, une capacité spéciale ou un sort vous indique que vous avez un
avantage ou un désavantage sur un test de capacité, un jet de sauvegarde
ou un jet d'attaque. Dans ce cas, vous lancez un deuxième d20 lorsque
vous effectuez le jet. Utilisez le plus élevé des deux jets si vous avez
un avantage, et utilisez le plus faible des jets si vous avez un
désavantage. Par exemple, si vous avez un désavantage et que vous
obtenez un 17 et un 5, vous utilisez le 5. Si, au contraire, vous avez
un avantage et que vous obtenez ces chiffres, vous utilisez le 17.

Si plusieurs situations affectent un jet et que chacune d'entre elles
lui confère un avantage ou lui impose un désavantage, vous ne lancez pas
plus d'un d20 supplémentaire. Si deux situations favorables confèrent
un avantage, par exemple, vous ne lancez toujours qu'un seul d20
supplémentaire.

Si les circonstances font qu'un jet est à la fois avantageux et
désavantageux, on considère que vous n'avez ni l'un ni l'autre, et
vous lancez un d20. Ceci est vrai même si plusieurs circonstances
imposent un désavantage et qu'une seule accorde un avantage ou vice
versa. Dans une telle situation, vous n'avez ni avantage ni
désavantage.

Lorsque vous avez un avantage ou un désavantage et que quelque chose
dans le jeu, comme le trait Chanceux du halfelin, vous permet de
relancer le d20, vous ne pouvez relancer qu'un seul des dés. Vous
choisissez lequel. Par exemple, si un halfelin a un avantage ou un
désavantage sur un test de capacité et qu'il obtient un 1 et un 13, il
peut utiliser le trait Chanceux pour relancer le 1.

On obtient généralement un avantage ou un désavantage en utilisant des
capacités spéciales, des actions ou des sorts. L'inspiration peut
également donner un avantage à un personnage. Le MJ peut également
décider que les circonstances influencent un jet dans un sens ou dans
l'autre et accorder un avantage ou imposer un désavantage en
conséquence.



## Bonus de maîtrise supplémentaire

Les personnages ont un bonus de maîtrise déterminé par le niveau. Les
monstres ont également ce bonus, qui est incorporé dans leurs blocs de
statistiques. Le bonus est utilisé dans les règles sur les tests de
caractéristiques, les jets de sauvegarde et les jets d'attaque.

Votre bonus de maîtrise ne peut pas être ajouté plus d'une fois à un
seul jet de dé ou à un autre nombre. Par exemple, si deux règles
différentes stipulent que vous pouvez ajouter votre bonus de maîtrise à
un jet de sauvegarde de Sagesse, vous n'ajoutez néanmoins le bonus
qu'une seule fois lorsque vous effectuez la sauvegarde.

Parfois, votre bonus de maîtrise peut être multiplié ou divisé (doublé
ou divisé par deux, par exemple) avant que vous ne l'appliquiez. Par
exemple, la caractéristique Expertise du roublard double le bonus de
maîtrise pour certains tests de capacité. Si une circonstance suggère
que votre bonus de maîtrise s'applique plus d'une fois au même jet,
vous ne l'ajoutez qu'une fois et ne le multipliez ou le divisez
qu'une seule fois.

De même, si une caractéristique ou un effet vous permet de multiplier
votre bonus de maîtrise lors d'un test d'aptitude qui ne bénéficierait
pas normalement de votre bonus de maîtrise, vous n'ajoutez toujours pas
le bonus au test. Pour ce test, votre bonus de maîtrise est de 0, étant
donné que la multiplication de 0 par n'importe quel nombre donne
toujours 0. Par exemple, si vous ne maîtrisez pas la compétence
Histoire, vous ne tirez aucun avantage d'une caractéristique qui vous
permet de doubler votre bonus de maîtrise lorsque vous effectuez des
tests d'Intelligence (Histoire).

En général, vous ne multipliez pas votre bonus de maîtrise pour les jets
d'attaque ou de sauvegarde. Si une caractéristique ou un effet vous
permet de le faire, ces mêmes règles s'appliquent.



## Jets de caractéristiques

Un jet de caractéristique teste le talent et l'entraînement innés d'un
personnage ou d'un monstre dans le but de surmonter un défi. Le MJ
demande un test d'aptitude lorsqu'un personnage ou un monstre tente
une action (autre qu'une attaque) qui a une chance d'échouer. Lorsque
l'issue est incertaine, ce sont les dés qui déterminent les résultats.

Pour chaque test de capacité, le MJ décide laquelle des six capacités
est pertinente pour la tâche à accomplir et la difficulté de la tâche,
représentée par une classe de difficulté. Plus une tâche est difficile,
plus son DC est élevé. Le tableau des Classes de Difficulté typiques
montre les DCs les plus communs.

  -----------------------
  Difficulté de la    DC
  tâche              
  ------------------ ----
  Très facile         5

  Facile              10

  Moyen               15

  Hard                20

  Très difficile      25

  Presque impossible  30
  -----------------------

  : Classes de difficultés typiques

Pour effectuer un test de capacité, lancez un d20 et ajoutez le
modificateur de capacité approprié. Comme pour les autres jets de d20,
appliquez les bonus et les pénalités, et comparez le total au DC. Si le
total est égal ou supérieur au DC, le test d'aptitude est une réussite
: la créature surmonte le défi qui lui est posé. Dans le cas contraire,
c'est un échec, ce qui signifie que le personnage ou le monstre ne
progresse pas vers l'objectif ou progresse avec un revers déterminé par
le MJ.


### Concours

Parfois, les efforts d'un personnage ou d'un monstre sont directement
opposés à ceux d'un autre. Cela peut se produire lorsque les deux
essaient de faire la même chose et que seul l'un d'entre eux peut
réussir, par exemple lorsqu'ils tentent d'attraper un anneau magique
qui est tombé par terre. Cette situation s'applique également lorsque
l'un d'entre eux essaie d'empêcher l'autre d'accomplir un
objectif - par exemple, lorsqu'un monstre tente de forcer une porte
qu'un aventurier tient fermée. Dans ce genre de situation, l'issue est
déterminée par une forme spéciale de jet de caractéristique, appelée
concours.

Les deux participants à un concours effectuent des tests de capacité
adaptés à leurs efforts. Ils appliquent tous les bonus et pénalités
appropriés, mais au lieu de comparer le total à un DC, ils comparent les
totaux de leurs deux tests. Le participant dont le total des tests est
le plus élevé remporte le concours. Ce personnage ou monstre réussit
l'action ou empêche l'autre de réussir.

Si le concours aboutit à une égalité, la situation reste la même
qu'avant le concours. Ainsi, un des participants peut gagner le
concours par défaut. Si deux personnages sont à égalité dans un concours
pour arracher un anneau du sol, aucun d'entre eux ne l'attrape. Dans
un concours entre un monstre qui essaie d'ouvrir une porte et un
aventurier qui essaie de garder la porte fermée, une égalité signifie
que la porte reste fermée.



### Compétences

Chaque aptitude couvre un large éventail de capacités, y compris les
compétences dans lesquelles un personnage ou un monstre peut être
compétent. Une compétence représente un aspect spécifique d'un score de
capacité, et la maîtrise d'une compétence par un individu démontre
qu'il se concentre sur cet aspect. (Les compétences de départ d'un
personnage sont déterminées à la création du personnage, et les
compétences d'un monstre apparaissent dans le bloc de statistiques du
monstre).

Par exemple, un test de Dextérité peut refléter la tentative d'un
personnage de réaliser une acrobatie, de tenir un objet dans sa main ou
de rester caché. Chacun de ces aspects de la Dextérité a une compétence
associée : Acrobaties, Habileté de la main, et Discrétion,
respectivement. Ainsi, un personnage qui maîtrise la compétence
Discrétion est particulièrement doué pour les contrôles de Dextérité
liés à la furtivité et à la dissimulation.

Les compétences liées à chaque valeur de caractéristique sont indiquées
dans la liste suivante. (Aucune compétence n'est liée à la
Constitution.) Voir la description d'une compétence pour des exemples
d'utilisation d'une compétence associée à une compétence.


#### Force

-   Athlétisme



#### Dextérité

-   Acrobaties
-   Escamotage
-   Discrétion



#### Intelligence

-   Mystère
-   Histoire
-   Investigation
-   Nature
-   Religion



#### Sagesse

-   Dressage d'animaux
-   Intuition
-   Médecine
-   Perception
-   Survie



#### Charisme

-   Supercherie
-   Intimidation
-   Représentation
-   Persuasion

Parfois, le MJ peut demander un test de capacité utilisant une
compétence spécifique - par exemple, \"Faites un test de Sagesse
(Perception)\". Dans d'autres cas, un joueur peut demander au MJ si la
maîtrise d'une compétence particulière s'applique à un test. Dans les
deux cas, la maîtrise d'une compétence signifie qu'une personne peut
ajouter son bonus de maîtrise aux tests de capacité qui impliquent cette
compétence. Si le joueur ne maîtrise pas la compétence, il effectue un
test d'aptitude normal.

Par exemple, si un personnage tente d'escalader une falaise dangereuse,
le MJ peut demander un test de Force (Athlétisme). Si le personnage est
compétent en Athlétisme, le bonus de compétence du personnage est ajouté
au test de Force. Si le personnage ne possède pas cette compétence, il
effectue simplement un test de Force.



#### Variante : Compétences avec différentes aptitudes

Normalement, votre maîtrise d'une compétence ne s'applique qu'à un
type spécifique de test d'aptitude. La maîtrise de l'Athlétisme, par
exemple, s'applique généralement aux tests de Force. Dans certaines
situations, cependant, votre maîtrise peut raisonnablement s'appliquer
à un autre type de test. Dans ce cas, le MJ peut demander un test
utilisant une combinaison inhabituelle de capacité et de compétence, ou
vous pouvez demander à votre MJ si vous pouvez appliquer une compétence
à un autre test. Par exemple, si vous devez nager d'une île au large
vers le continent, votre MJ peut demander un test de Constitution pour
voir si vous avez l'endurance nécessaire pour parcourir cette distance.
Dans ce cas, votre MJ pourrait vous permettre d'appliquer votre
maîtrise de l'Athlétisme et vous demander un test de Constitution
(Athlétisme). Ainsi, si vous êtes compétent en Athlétisme, vous
appliquez votre bonus de compétence au test de Constitution comme vous
le feriez normalement pour un test de Force (Athlétisme). De même,
lorsque votre barbare demi-orc utilise une démonstration de force brute
pour intimider un ennemi, votre MJ peut demander un test de Force
(Intimidation), même si l'Intimidation est normalement associée au
Charisme.




### Chèques passifs

Un test passif est un type particulier de test de capacité qui
n'implique aucun jet de dé. Un tel test peut représenter le résultat
moyen d'une tâche effectuée de manière répétée, comme chercher des
portes secrètes encore et encore, ou peut être utilisé lorsque le MJ
veut déterminer secrètement si les personnages réussissent quelque chose
sans lancer de dé, comme remarquer un monstre caché.

Voici comment déterminer le total d'un personnage pour un test passif :

  -----------------------------------------------------------------------------
   10 + tous les modificateurs qui s'appliquent normalement à la vérification
  -----------------------------------------------------------------------------

Si le personnage a un avantage sur le test, ajoutez 5. Pour un
désavantage, soustrayez 5. Le jeu se réfère au total d'un test passif
comme un **score**.

Par exemple, si un personnage de 1er niveau a une Sagesse de 15 et une
maîtrise de la Perception, il a un score passif de Sagesse (Perception)
de 14.

Les règles de dissimulation de la section \"Dextérité\" ci-dessous
reposent sur des contrôles passifs, tout comme les règles
d'exploration.



### Travailler ensemble

Parfois, deux personnages ou plus s'associent pour tenter une tâche. Le
personnage qui dirige l'effort - ou celui qui a le modificateur
d'aptitude le plus élevé - peut faire un jet de caractéristique avec
avantage, reflétant l'aide fournie par les autres personnages. En
combat, cela nécessite l'action Aider.

Un personnage ne peut apporter son aide que si la tâche est une tâche
qu'il pourrait tenter seul. Par exemple, pour essayer d'ouvrir une
serrure, il faut avoir la maîtrise des outils de voleur, donc un
personnage qui n'a pas cette maîtrise ne peut pas aider un autre
personnage dans cette tâche. De plus, un personnage ne peut aider que si
deux ou plusieurs personnes travaillant ensemble sont réellement
productives. Certaines tâches, comme enfiler une aiguille, ne sont pas
plus faciles avec de l'aide.


#### Contrôles de groupe

Lorsqu'un certain nombre d'individus essaient d'accomplir quelque
chose en groupe, le MJ peut demander un test de capacité de groupe. Dans
une telle situation, les personnages qui sont doués pour une tâche
particulière aident à couvrir ceux qui ne le sont pas. Pour faire un
test de capacité de groupe, tout le monde dans le groupe fait le test de
capacité. Si au moins la moitié du groupe réussit, le groupe entier
réussit. Sinon, le groupe échoue. Les tests de groupe ne sont pas très
fréquents, et ils sont surtout utiles lorsque tous les personnages
réussissent ou échouent en tant que groupe. Par exemple, lorsque les
aventuriers naviguent dans un marais, le MJ peut demander un test de
Sagesse (Survie) de groupe pour voir si les personnages peuvent éviter
les sables mouvants, les gouffres et autres dangers naturels de
l'environnement. Si au moins la moitié du groupe réussit, les
personnages qui ont réussi sont capables de guider leurs compagnons hors
de danger. Sinon, le groupe trébuche sur l'un de ces dangers.





## Utiliser chaque capacité

Chaque tâche qu'un personnage ou un monstre peut tenter dans le jeu est
couverte par l'une des six capacités. Cette section explique plus en
détail la signification de ces capacités et la façon dont elles sont
utilisées dans le jeu.


### Force

La force mesure la puissance corporelle, l'entraînement athlétique et
la mesure dans laquelle vous pouvez exercer une force physique brute.


#### Contrôles de Force

Un test de Force peut modéliser toute tentative de soulever, pousser,
tirer ou briser quelque chose, de forcer votre corps à travers un
espace, ou d'appliquer la force brute à une situation. La compétence
Athlétisme reflète l'aptitude à effectuer certains types de tests de
Force.

***Athlétisme.*** Votre test de Force (Athlétisme) couvre les situations
difficiles que vous rencontrez lorsque vous grimpez, sautez ou nagez.
Les exemples incluent les activités suivantes :

-   Vous tentez d'escalader une falaise abrupte ou glissante, d'éviter
    les dangers en escaladant un mur ou de vous accrocher à une surface
    alors que quelque chose essaie de vous faire tomber.
-   Vous essayez de sauter sur une distance inhabituellement longue ou
    de réaliser une acrobatie en plein saut.
-   Vous luttez pour nager ou rester à flot dans les courants traîtres,
    les vagues de tempête ou les zones d'algues épaisses. Ou bien une
    autre créature essaie de vous pousser ou de vous tirer sous l'eau
    ou d'interférer de toute autre manière avec votre nage.

***Autres tests de Force.*** Le MJ peut également demander un test de
Force lorsque vous essayez d'accomplir des tâches telles que les
suivantes :

-   Forcer une porte bloquée, verrouillée ou barrée.
-   Libérez-vous de vos liens
-   Pousser dans un tunnel trop petit
-   S'accrocher à un chariot tout en étant traîné derrière lui.
-   Renverser une statue
-   Empêcher un rocher de rouler



#### Jets d'attaque et dégâts

Vous ajoutez votre modificateur de Force à votre jet d'attaque et à
votre jet de dégâts lorsque vous attaquez avec une arme de mêlée comme
une masse, une hache de guerre ou une javeline. Vous utilisez les armes
de mêlée pour effectuer des attaques de mêlée au corps à corps, et
certaines d'entre elles peuvent être lancées pour effectuer une attaque
à distance.



#### Levage et portage

Votre score de Force détermine la quantité de poids que vous pouvez
supporter. Les termes suivants définissent ce que vous pouvez soulever
ou porter.

***Capacité de charge.*** Votre capacité de charge correspond à votre
score de Force multiplié par 15. Il s'agit du poids (en livres) que
vous pouvez porter, qui est suffisamment élevé pour que la plupart des
personnages n'aient généralement pas à s'en soucier.

***Pousser, traîner ou soulever.*** Vous pouvez pousser, traîner ou
soulever un poids en livres jusqu'à deux fois votre capacité de charge
(ou 30 fois votre score de Force). Lorsque vous poussez ou traînez un
poids supérieur à votre capacité de charge, votre vitesse est réduite à
5 pieds.

***Taille et Force.*** Les créatures plus grandes peuvent supporter plus
de poids, tandis que les créatures Très petites peuvent en supporter
moins. Pour chaque catégorie de taille supérieure à la taille moyenne,
la capacité de charge de la créature et la quantité qu'elle peut
pousser, traîner ou soulever sont doublées. Pour une créature Très
petite (TP), ces poids sont divisés par deux.



#### Variante : Encumbrance

Les règles de soulèvement et de transport sont intentionnellement
simples. Voici une variante si vous cherchez des règles plus détaillées
pour déterminer comment un personnage est gêné par le poids de son
équipement. Lorsque vous utilisez cette variante, ignorez la colonne
Force de la table d'armure.

Si vous portez un poids supérieur à 5 fois votre score de Force, vous
êtes **encombré**, ce qui signifie que votre vitesse diminue de 10
pieds.

Si vous portez un poids supérieur à 10 fois votre valeur de
caractéristique, jusqu'à concurrence de votre capacité de charge
maximale, vous êtes **lourdement encombré**, ce qui signifie que votre
vitesse diminue de 20 pieds et que vous avez un désavantage sur les
tests de capacité, les jets d'attaque et les jets de sauvegarde qui
utilisent la Force, la Dextérité ou la Constitution.




### Dextérité

La Dextérité mesure l'agilité, les réflexes et l'équilibre.


#### Contrôles de Dextérité

Un test de Dextérité peut modéliser toute tentative de se déplacer de
manière agile, rapide ou silencieuse, ou d'éviter de tomber sur un pied
délicat. Les compétences Acrobaties, Escamotage et Discrétion reflètent
l'aptitude à effectuer certains types de tests de Dextérité.

***Acrobaties.*** Votre test de Dextérité (Acrobaties) couvre votre
tentative de rester sur vos pieds dans une situation délicate, comme
lorsque vous essayez de courir sur une plaque de glace, de tenir en
équilibre sur une corde raide, ou de rester droit sur le pont d'un
bateau qui tangue. Le MJ peut également demander un test de Dextérité
(Acrobaties) pour voir si vous pouvez effectuer des acrobaties, y
compris des plongeons, des roulades, des sauts périlleux et des flips.

***Escamotage.*** Chaque fois que vous tentez un acte de ligue ou de
ruse manuelle, comme planter quelque chose sur quelqu'un d'autre ou
dissimuler un objet sur votre personne, faites un test de Dextérité
(Escamotage). Le MJ peut également demander un test de Dextérité
(Escamotage) pour déterminer si vous pouvez soulever un porte-monnaie
d'une autre personne ou glisser quelque chose de la poche d'une autre
personne.

***Discrétion.*** Effectuez un test de Dextérité (Discrétion) lorsque
vous tentez de vous cacher de vos ennemis, de vous faufiler entre les
gardes, de vous éclipser sans être remarqué ou de vous approcher
furtivement de quelqu'un sans être vu ou entendu.

***Autres tests de Dextérité.*** Le MJ peut demander un test de
Dextérité lorsque vous essayez d'accomplir des tâches telles que les
suivantes :

-   Contrôlez un chariot lourdement chargé dans une descente abrupte.
-   Diriger un char dans un virage serré
-   Crocheter une serrure
-   Désactiver un piège
-   Attachez solidement un prisonnier
-   S'affranchir des liens
-   Jouer d'un instrument à cordes
-   Réaliser un objet petit ou détaillé



#### Jets d'attaque et dégâts

Vous ajoutez votre modificateur de Dextérité à votre jet d'attaque et à
votre jet de dégâts lorsque vous attaquez avec une arme à distance,
comme une fronde ou un arc long. Vous pouvez également ajouter votre
modificateur de Dextérité à votre jet d'attaque et à votre jet de
dégâts lorsque vous attaquez avec une arme de mêlée qui a la propriété
de finesse, comme une dague ou une rapière.



#### Classe d'armure

Selon l'armure que vous portez, vous pouvez ajouter une partie ou la
totalité de votre modificateur de Dextérité à votre classe d'armure.



#### Initiative

Au début de chaque combat, vous lancez un jet d'initiative en faisant
un test de Dextérité. L'initiative détermine l'ordre des tours des
créatures en combat.

> #### Cacher {#hiding}
>
> Le MJ décide quand les circonstances sont appropriées pour se cacher.
> Lorsque vous tentez de vous cacher, effectuez un test de Dextérité
> (Discrétion). Jusqu'à ce que vous soyez découvert ou que vous cessiez
> de vous cacher, le total de ce test est contesté par le test de
> Sagesse (Perception) de toute créature qui recherche activement des
> signes de votre présence.
>
> Vous ne pouvez pas vous cacher d'une créature qui vous voit
> clairement, et vous dévoilez votre position si vous faites du bruit,
> par exemple en criant un avertissement ou en renversant un vase.
>
> Une créature invisible peut toujours essayer de se cacher. Les signes
> de son passage peuvent toujours être remarqués, et elle doit rester
> silencieuse.
>
> En combat, la plupart des créatures restent vigilantes aux signes de
> danger tout autour, donc si vous sortez de votre cachette et vous
> approchez d'une créature, elle vous voit généralement. Cependant,
> dans certaines circonstances, le MJ peut vous autoriser à rester caché
> lorsque vous vous approchez d'une créature distraite, vous permettant
> ainsi d'obtenir un avantage sur un jet d'attaque avant d'être vu.
>
> ***Perception passive.*** Lorsque vous vous cachez, il y a une chance
> que quelqu'un vous remarque même s'il ne cherche pas. Pour
> déterminer si une telle créature vous remarque, le MJ compare votre
> test de Dextérité (Discrétion) avec le score de Sagesse passive
> (Perception) de cette créature, qui est égal à 10 + le modificateur de
> Sagesse de la créature, ainsi que tout autre bonus ou malus. Si la
> créature a un avantage, ajoutez 5. Pour un désavantage, soustrayez 5.
> Par exemple, si un personnage de 1er niveau (avec un bonus de maîtrise
> de +2) a une Sagesse de 15 (avec un modificateur de +2) et une
> compétence en Perception, il a une Sagesse passive (Perception) de 14.
>
> ***Que pouvez-vous voir ?*** L'un des principaux facteurs qui
> déterminent si vous pouvez trouver une créature ou un objet caché est
> votre capacité à voir dans une zone, qui peut être légèrement ou
> fortement obscurcie .




### Constitution

La Constitution mesure la santé, l'endurance et la force vitale.


#### Contrôles de la Constitution

Les tests de Constitution sont rares, et aucune compétence ne
s'applique aux tests de Constitution, parce que l'endurance que cette
capacité représente est largement passive plutôt que d'impliquer un
effort spécifique de la part d'un personnage ou d'un monstre. Un test
de Constitution peut cependant modéliser votre tentative de dépasser les
limites normales.

Le MJ peut demander un test de Constitution lorsque vous essayez
d'accomplir des tâches comme celles-ci :

-   Retenez votre souffle
-   Marche ou travail pendant des heures sans repos
-   Se passer de sommeil
-   Survivre sans nourriture ni eau
-   Dégustez une chope entière de bière en une seule fois.



#### Points de vie

Votre modificateur de Constitution contribue à vos points de vie. En
général, vous ajoutez votre modificateur de Constitution à chaque dé de
réussite que vous obtenez pour vos points de vie.

Si votre modificateur de Constitution change, votre maximum de points de
vie change également, comme si vous aviez le nouveau modificateur depuis
le 1er niveau. Par exemple, si vous augmentez votre score de
Constitution lorsque vous atteignez le 4ème niveau et que votre
modificateur de Constitution passe de +1 à +2, vous ajustez votre
maximum de points de vie comme si le modificateur avait toujours été de
+2. Vous ajoutez donc 3 points de vie pour vos trois premiers niveaux,
puis vous calculez vos points de vie pour le 4e niveau en utilisant
votre nouveau modificateur. Ou si vous êtes au 7ème niveau et qu'un
effet abaisse votre score de Constitution de façon à réduire votre
modificateur de Constitution de 1, votre maximum de points de vie est
réduit de 7.




### Intelligence

L'intelligence mesure l'acuité mentale, la précision de la mémoire et
la capacité de raisonnement.


#### Contrôles d'Intelligence

Un test d'Intelligence entre en jeu lorsque vous devez faire appel à la
logique, à l'éducation, à la mémoire ou au raisonnement déductif. Les
compétences en Arcane, Histoire, Investigation, Nature et Religion
reflètent l'aptitude à effectuer certains types de tests
d'Intelligence.

***Mystères.*** Votre test d'Intelligence (Arcane) mesure votre
capacité à vous souvenir de connaissances sur les sorts, les objets
magiques, les symboles eldritch, les traditions magiques, les plans
d'existence et les habitants de ces plans.

***Histoire.*** Votre test d'Intelligence (Histoire) mesure votre
capacité à vous souvenir d'événements historiques, de personnages
légendaires, de royaumes anciens, de conflits passés, de guerres
récentes et de civilisations disparues.

***Investigation.*** Lorsque vous cherchez des indices et que vous
faites des déductions à partir de ces indices, vous effectuez un test
d'Intelligence (Investigation). Vous pouvez déduire l'emplacement
d'un objet caché, discerner d'après l'apparence d'une blessure le
type d'arme qui l'a infligée, ou déterminer le point le plus faible
d'un tunnel qui pourrait le faire s'effondrer. L'exploration de
parchemins anciens à la recherche d'un fragment de savoir caché peut
également nécessiter un test d'Intelligence (Investigation).

***Nature.*** Votre test d'Intelligence (Nature) mesure votre capacité
à vous souvenir des connaissances sur le terrain, les plantes et les
animaux, le temps et les cycles naturels.

***Religion.*** Votre test d'Intelligence (Religion) permet de mesurer
votre capacité à vous souvenir de connaissances sur les divinités, les
rites et les prières, les hiérarchies religieuses, les symboles sacrés
et les pratiques des cultes secrets.

***Autres tests d'Intelligence.*** Le MJ peut demander un test
d'Intelligence lorsque vous essayez d'accomplir des tâches telles que
les suivantes :

-   Communiquer avec une créature sans utiliser de mots
-   Estimer la valeur d'un objet précieux
-   Préparez un déguisement pour passer pour un garde municipal.
-   Falsifier un document
-   Rappeler les traditions d'un métier ou d'une profession
-   Gagner un jeu de compétence



#### Caractéristique d'Incantation

Les magiciens utilisent l'Intelligence comme caractéristique
d'incantation, ce qui permet de déterminer les DC de jet de sauvegarde
des sorts qu'ils lancent.




### Sagesse

La Sagesse reflète votre degré de sensibilité au monde qui vous entoure
et représente la perspicacité et l'intuition.


#### Tests de Sagesse

Un test de Sagesse peut refléter un effort pour lire le langage
corporel, comprendre les sentiments de quelqu'un, remarquer des choses
sur l'environnement, ou soigner une personne blessée. Les compétences
Dressage, Intuition, Médecine, Perception et Survie reflètent
l'aptitude à effectuer certains types de tests de Sagesse.

***Dressage d'animaux.*** Lorsqu'on se demande si vous pouvez calmer
un animal domestique, empêcher une monture de s'effrayer ou deviner les
intentions d'un animal, le MJ peut demander un test de Sagesse
(Dressage). Vous effectuez également un test de Sagesse (Dressage
d'animaux) pour contrôler votre monture lorsque vous tentez une
manœuvre risquée.

***Intuition.*** Votre test de Sagesse (Intuition) détermine si vous
pouvez déterminer les véritables intentions d'une créature, par exemple
lorsque vous cherchez un mensonge ou que vous prédisez le prochain
mouvement de quelqu'un. Pour ce faire, vous devez glaner des indices
dans le langage corporel, les habitudes d'élocution et les changements
de comportement.

***Médecine.*** Un test de Sagesse (Médecine) vous permet d'essayer de
stabiliser un compagnon mourant ou de diagnostiquer une maladie.

***Perception.*** Votre test de Sagesse (Perception) vous permet de
repérer, d'entendre ou de détecter la présence de quelque chose. Il
mesure votre conscience générale de votre environnement et l'acuité de
vos sens. Par exemple, vous pouvez essayer d'entendre une conversation
à travers une porte fermée, écouter aux portes sous une fenêtre ouverte,
ou entendre des monstres se déplacer furtivement dans la forêt. Vous
pouvez aussi essayer de repérer des choses qui sont obscures ou faciles
à manquer, qu'il s'agisse d'orcs en embuscade sur une route, de
malfrats cachés dans l'ombre d'une ruelle ou de la lumière d'une
bougie sous une porte secrète fermée.

***Survie.*** Le MJ peut vous demander de faire un test de Sagesse
(Survie) pour suivre des pistes, chasser du gibier, guider votre groupe
à travers des terres gelées, identifier des signes indiquant que des
Ours-hibous vivent à proximité, prédire le temps, ou éviter les sables
mouvants et autres dangers naturels.

***Autres tests de Sagesse.*** Le MJ peut demander un test de Sagesse
lorsque vous essayez d'accomplir des tâches telles que les suivantes :

-   Avoir une intuition sur la ligne de conduite à suivre
-   Discerner si une créature apparemment morte ou vivante est
    morte-vivante.



#### Caractéristique d'Incantation

Les clercs, les druides et les rôdeurs utilisent la Sagesse comme
caractéristique d'incantation, ce qui permet de déterminer les jets de
sauvegarde des sorts qu'ils lancent.




### Charisme

Le charisme mesure votre capacité à interagir efficacement avec les
autres. Il comprend des facteurs tels que la confiance et l'éloquence,
et peut représenter une personnalité charmante ou autoritaire.


#### Contrôles du Charisme

Un test de Charisme peut survenir lorsque vous essayez d'influencer ou
de divertir les autres, lorsque vous essayez de faire une impression ou
de dire un mensonge convaincant, ou lorsque vous naviguez dans une
situation sociale délicate. Les compétences en Supercherie,
Intimidation, Représentation et Persuasion reflètent l'aptitude à
effectuer certains types de tests de Charisme.

***Supercherie.*** Votre test de Charisme (Supercherie) détermine si
vous pouvez cacher la vérité de manière convaincante, que ce soit
verbalement ou par vos actions. Cette supercherie peut aller de la
tromperie par l'ambiguïté au mensonge pur et simple. Les situations
typiques sont les suivantes : essayer de parler rapidement à un garde,
escroquer un marchand, gagner de l'argent en jouant, se faire passer
pour un déguisement, apaiser les soupçons de quelqu'un avec de fausses
assurances, ou garder un visage impassible tout en racontant un mensonge
flagrant.

***Intimidation.*** Lorsque vous tentez d'influencer quelqu'un par des
menaces ouvertes, des actions hostiles et la violence physique, le MJ
peut vous demander de faire un test de Charisme (Intimidation). Par
exemple, vous pouvez essayer de soutirer des informations à un
prisonnier, convaincre des malfrats de la rue de renoncer à une
confrontation, ou utiliser le bord d'une bouteille cassée pour
convaincre un vizir narquois de reconsidérer une décision.

***Représentation.*** Votre test de Charisme (Représentation) détermine
votre capacité à enchanter un public avec de la musique, de la danse, du
théâtre, des contes ou toute autre forme de divertissement.

***Persuasion.*** Lorsque vous tentez d'influencer quelqu'un ou un
groupe de personnes avec du tact, des grâces sociales ou une bonne
nature, le MJ peut vous demander de faire un test de Charisme
(Persuasion). Généralement, vous utilisez la Persuasion lorsque vous
agissez de bonne foi, pour favoriser les amitiés, faire des demandes
cordiales, ou faire preuve d'une étiquette correcte. Par exemple, vous
pouvez convaincre un chambellan de laisser votre groupe voir le roi,
négocier la paix entre des tribus en guerre ou inspirer une foule de
citadins.

***Autres tests de Charisme.*** Le MJ peut demander un test de Charisme
lorsque vous essayez d'accomplir des tâches telles que les suivantes :

-   Trouvez la meilleure personne à qui parler pour les nouvelles, les
    rumeurs et les potins.
-   Se fondre dans la foule pour avoir une idée des principaux sujets de
    conversation



#### Caractéristique d'Incantation

Les bardes, paladins, sorciers et sorciers utilisent le Charisme comme
caractéristique d'incantation, ce qui aide à déterminer les jets de
sauvegarde des sorts qu'ils lancent.





## Jets de sauvegarde

Un jet de sauvegarde - également appelé sauvegarde - représente une
tentative de résistance à un sort, un piège, un poison, une maladie ou
une menace similaire. Normalement, vous ne décidez pas de faire un jet
de sauvegarde ; vous y êtes contraint parce que votre personnage ou
votre monstre risque d'être blessé.

Pour effectuer un jet de sauvegarde, lancez un d20 et ajoutez le
modificateur de capacité approprié. Par exemple, vous utilisez votre
modificateur de Dextérité pour un jet de sauvegarde de Dextérité.

Un jet de sauvegarde peut être modifié par un bonus ou une pénalité
situationnelle et peut être affecté par un avantage ou un désavantage,
comme déterminé par le MJ.

Chaque classe donne la maîtrise d'au moins deux jets de sauvegarde. Le
magicien, par exemple, est compétent en jet de sauvegarde
d'Intelligence. Comme pour les compétences, la maîtrise d'un jet de
sauvegarde permet au personnage d'ajouter son bonus de compétence aux
jets de sauvegarde effectués en utilisant une valeur de caractéristique
particulière. Certains monstres ont également des compétences en jet de
sauvegarde.

La classe de difficulté d'un jet de sauvegarde est déterminée par
l'effet qui le provoque. Par exemple, le DC d'un jet de sauvegarde
autorisé par un sort est déterminé par la capacité d'incantation et le
bonus de maîtrise du lanceur de sorts.

Le résultat d'un jet de sauvegarde réussi ou raté est également
détaillé dans l'effet qui permet la sauvegarde. En général, un jet de
sauvegarde réussi signifie qu'une créature ne subit aucun dommage, ou
un dommage réduit, à cause d'un effet.


