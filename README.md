# SRD5.1 CC-BY FR

## Présentation

Voici le SRD 5.1 du système **Donjons & Dragons** en français et sous licence Creative Commons BY. Il s'agit d'une traduction effectuée par Deepl à partir du document fourni par oznogon(https://github.com/oznogon/cc-srd5) et basé sur un glossaire (lui aussi en CC-BY) conçu par mes soins en prenant comme référence celui d' AideDD(https://www.aidedd.org/download.php?file=Glossaire&ext=pdf).

## Remerciements

- oznogon pour le SRD anglais au format markdown qui a servit de base à cette traduction.

- Deepl SE pour son merveilleux outil de traduction éponyme.

- La communauté d'AideDD pour son travail extraordinaire de traduction qui a donné lieu a un lexique sans qui ce projet n'aurait sans doute jamais eu lieu.

- La communauté du forum CasusNo et particulièrement (par ordre d'intervention) :

  - Islayre d'Argolh pour le terme Invocateur.
  
  - Dyvim pour le terme Drakonide.
  
  - TwoSharpBlades pour avoir proposé le premier le terme Origine. Mugen, Qui Revient de Loin, Vorghyrn, Hakooh, Blondin, Dr Hiatus, Erestor, pour les discussions entourant ce choix.
  
  - Altay pour le passage à l'outil d'automatisation des exports Sphynx (en cours de réalisation).
  
- La communauté du forum Black Book et particulièrement :

  - Laurendi pour m'avoir donné l'idée de partir du lexique d'AideDD pour construire le mien (sans cela ce document n'existerait pas, qu'il en soit remercié !).
  
  - Nar Sir pour le soutient appréciable a un moment crucial !
  
  - Cedrole pour avoir interrogé le terme de mège pour guérisseur.
  
  - Dyvim, encore, pour ses nombreuses remarques, toutes pertinentes.
  
  - Derle pour ses propositions concernant le Barde et l'Underdark.
  
  - Asharak pour l'Underdark également.
  
  - Nioux pour avoir appuyé de manière pertinente le terme d'Origine.
  
- Et tous ceux que je n'ai pas cités et qui participent aux discussions !

## TODO :

-  Réviser la traduction.

-  Correction d'erreurs de traduction sur les alignements que j'ai oublié d'ajouter au glossaire.

- [x] Suppresions des balises spécifiques à Pandoc pour compatibilité avec d'autres outils et une possible automatisation avec Sphynx.

- [x] Réviser le chapitrage.

- [x] Diviser le documents en plusieurs sections logiques et inclure un script de compilation ?

- [x] Trouver un terme pour remplacer "race".

- [x] Changer Rhapsode pour Barde.

- [x] Changer Cacodémurge pour Invocateur.

- [x] Changer Mège pour Guérisseur.

- [x] En réflexion : changer Consacré pour béni/bénédiction.

- [x] En réflexion : du Péracosme → des Autres Mondes

- [x] En réflexion : Arnaqueur → revenir à Escroc (Roublard) ? J'ai aussi pensé à Aigrefin, mais j'ai peur que ce ne soit que peu évocateur.

## Documents

*lexique.csv* : le glossaire au format csv.

*srd-ccby-fr.md* : le document de référence au format markdown.

*srd-ccby-fr.html* : le document au format html.

*srd-ccby-fr.odt* : le document au format Libre Office Writer.

*srd-ccby-fr.pdf* : le document au format pdf.

## Explication de quelques choix de traduction

*Dark One* → **L'Oscur** : Mot du moyen-français équivalent à obscur (qui me semblait trop commun pour désigner une entité spécifique).

*Eldritch* → **Traumaturgique** : C'est un mot difficile à traduire. Le terme "occulte" est associé au savoir là ou le terme Eldritch renvoie à l'étrangeté, à l'inquiétant, en lien, dans la culture contemporaine, avec l'horreur lovecraftienne. Pour essayer de préserver cette dimension j'ai construis ce mot à partir de "trauma" et du "ergo" grec qui signifie "travail", ce qui pourrait donner quelque chose comme "œuvrer pour le trauma".

*Elemental* → **Élémental** : la traduction française actuelle "élémentaire" me semble eronnée. Élémentaire désigne ce qui concerne les élements, là où l'élémental désigne spécifiquement les esprits constitués d'élements.

*Féérie* → **Aspiolie** : Aspiole signifie Fée en moyen-français.

*Shadowfell* → **Faillombre** : De même que pour drakéide, je ne sais pas si Gisombre est une formulation protégée aussi ai-je cherché une forme alternative.

*Sorcerer* → **Sorcier** : la traduction actuelle par "ensorceleur" ne me semblait pas très heureuse et source de confusion. Pour *Warlock* a été choisit **Invocateur**.

*Underdark* → **Ténèbres Inférieurs** : Si vous avez une meilleure idée...
