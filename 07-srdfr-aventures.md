
# Aventures


## Temps

Dans les situations où il est important de garder la trace du passage du
temps, le MJ détermine le temps que nécessite une tâche. Le MJ peut
utiliser une échelle de temps différente selon le contexte de la
situation. Dans un donjon, les déplacements des aventuriers se font à
l'échelle des **minutes**. Il leur faut environ une minute pour se
faufiler dans un long couloir, une autre minute pour vérifier l'absence
de pièges sur la porte au bout du couloir, et une bonne dizaine de
minutes pour fouiller la chambre au-delà à la recherche de quelque chose
d'intéressant ou de précieux.

Dans une ville ou une région sauvage, une échelle d'**heures** est
souvent plus appropriée. Les aventuriers désireux d'atteindre la tour
solitaire au cœur de la forêt se dépêchent de traverser ces quinze miles
en un peu moins de quatre heures.

Pour les longs voyages, une échelle de **jours** fonctionne mieux. En
suivant la route d'une nation à l'autre, les aventuriers passent
quatre jours sans histoire avant qu'une embuscade de Gobelins
n'interrompe leur voyage.

En combat et dans d'autres situations rapides, le jeu repose sur les
**rounds**, un laps de temps de 6 secondes.



## Mouvement

Traverser à la nage une rivière tumultueuse, se faufiler dans le couloir
d'un donjon, escalader le flanc d'une montagne dangereuse, toutes
sortes de mouvements jouent un rôle essentiel dans les aventures de jeux
fantastiques.

Le MJ peut résumer le déplacement des aventuriers sans calculer les
distances ou les temps de parcours exacts : \"Vous traversez la forêt et
trouvez l'entrée du donjon tard dans la soirée du troisième jour.\"
Même dans un donjon, en particulier un grand donjon ou un réseau de
grottes, le MJ peut résumer les déplacements entre les rencontres :
\"Après avoir tué le gardien à l'entrée de l'ancienne forteresse
naine, vous consultez votre carte, qui vous conduit à travers des
kilomètres de couloirs résonnants jusqu'à un gouffre enjambé par une
étroite arche de pierre.\"

Mais il est parfois important de savoir combien de temps il faut pour
aller d'un endroit à un autre, que la réponse soit en jours, en heures
ou en minutes. Les règles permettant de déterminer le temps de
déplacement dépendent de deux facteurs : la vitesse et le rythme de
déplacement des créatures qui se déplacent et le terrain sur lequel
elles se déplacent.


### Vitesse

Chaque personnage et monstre a une vitesse, qui est la distance en pieds
que le personnage ou le monstre peut parcourir en 1 round. Ce nombre
suppose de courtes bouffées de mouvement énergique au milieu d'une
situation de danger de mort.

Les règles suivantes déterminent la distance que peut parcourir un
personnage ou un monstre en une minute, une heure ou un jour.


#### Rythme de voyage

Lors d'un voyage, un groupe d'aventuriers peut se déplacer à un rythme
normal, rapide ou lent, comme indiqué sur la table de rythme de voyage.
Cette table indique la distance que le groupe peut parcourir en un
certain temps et si le rythme a un effet. Un rythme rapide rend les
personnages moins perceptifs, tandis qu'un rythme lent permet de se
faufiler et de fouiller une zone plus attentivement.

***Marche forcée.*** La table de rythme de voyage suppose que les
personnages voyagent pendant 8 heures par jour. Ils peuvent aller
au-delà de cette limite, au risque de s'épuiser.

Pour chaque heure de voyage supplémentaire au-delà de 8 heures, les
personnages parcourent la distance indiquée dans la colonne Heure pour
leur rythme, et chaque personnage doit effectuer un jet de sauvegarde de
Constitution à la fin de l'heure. Le jet de sauvegarde est de 10 + 1
pour chaque heure au-delà de 8 heures. En cas d'échec au jet de
sauvegarde, le personnage subit un niveau d'épuisement.

***Montures et véhicules.*** Pour de courtes durées (jusqu'à une
heure), de nombreux animaux se déplacent beaucoup plus vite que les
humanoïdes. Un personnage monté peut chevaucher au galop pendant environ
une heure, couvrant ainsi deux fois la distance habituelle pour un
rythme rapide. Si de nouvelles montures sont disponibles tous les 8 à 10
miles, les personnages peuvent couvrir de plus grandes distances à ce
rythme, mais cela est très rare, sauf dans les zones densément peuplées.

Les personnages dans les chariots, les calèches ou autres véhicules
terrestres choisissent une allure comme d'habitude. Les personnages
dans un navire sont limités à la vitesse du navire, et ils ne subissent
pas de pénalités pour un rythme rapide ou ne gagnent pas d'avantages
pour un rythme lent. Selon le navire et la taille de l'équipage, les
navires peuvent être capables de voyager jusqu'à 24 heures par jour.

Certaines montures spéciales, comme un pégase ou un griffon, ou des
véhicules spéciaux, comme un tapis volant, vous permettent de voyager
plus rapidement.

  ----------------------------------------------------------------------------------
  Pace          Distance\   Voyagé\   par\...\   \
                 Minute      Heure     Journée   Effet
  ------------ ----------- --------- ----------- -----------------------------------
  Rapidement    400 pieds   4 miles   30 miles   -Pénalité de 5 points aux scores
                                                 passifs de Sagesse (Perception).

  Normal        300 pieds   3 miles   24 miles   \-

  Lenteur       200 pieds   2 miles   18 miles   Capable d'utiliser la Discrétion
  ----------------------------------------------------------------------------------

  : Rythme de voyage



#### Terrain difficile

Les vitesses de déplacement indiquées dans la table de vitesse de
déplacement supposent un terrain relativement simple : routes, plaines
ouvertes ou couloirs de donjon dégagés. Mais les aventuriers sont
souvent confrontés à des forêts denses, des marais profonds, des ruines
remplies de décombres, des montagnes escarpées et des terrains
recouverts de glace - tous considérés comme des terrains difficiles.

Vous vous déplacez à la moitié de la vitesse en terrain difficile - se
déplacer de 1 pied en terrain difficile coûte 2 pieds de vitesse - de
sorte que vous pouvez couvrir seulement la moitié de la distance normale
en une minute, une heure ou un jour.




### Types de mouvements spéciaux

Pour se déplacer dans des donjons dangereux ou des zones sauvages, il ne
suffit souvent pas de marcher. Les aventuriers doivent parfois grimper,
ramper, nager ou sauter pour arriver à destination.


#### Grimper, nager et ramper

Lors de l'escalade ou de la natation, chaque pied de mouvement coûte 1
pied supplémentaire (2 pieds supplémentaires en terrain difficile), sauf
si une créature possède une vitesse d'escalade ou de natation. Au choix
du MJ, l'escalade d'une surface verticale glissante ou avec peu de
prises nécessite un test de Force (Athlétisme) réussi. De même, gagner
une certaine distance dans des eaux agitées peut nécessiter la réussite
d'un test de Force (Athlétisme).



#### Sauts

Votre Force détermine la distance à laquelle vous pouvez sauter.

***Saut en longueur.*** Lorsque vous effectuez un saut en longueur, vous
parcourez un nombre de pieds égal à votre score de Force si vous vous
déplacez d'au moins 3 mètres à pied juste avant le saut. Lorsque vous
faites un saut en longueur debout, vous ne pouvez sauter que sur la
moitié de cette distance. Dans les deux cas, chaque pied que vous
franchissez lors du saut vous coûte un pied de mouvement.

Cette règle suppose que la hauteur de votre saut n'a pas d'importance,
comme un saut à travers un ruisseau ou un gouffre. Au choix de votre MJ,
vous devez réussir un test de Force (Athlétisme) DC 10 pour franchir un
obstacle bas (pas plus haut qu'un quart de la distance du saut), comme
une haie ou un muret. Sinon, vous le heurtez.

Lorsque vous atterrissez en terrain difficile, vous devez réussir un
test de Dextérité (Acrobaties) DC 10 pour retomber sur vos pieds. Sinon,
vous atterrissez sur le ventre.

***Saut en hauteur.*** Lorsque vous effectuez un saut en hauteur, vous
sautez en l'air d'un nombre de pieds égal à 3 + votre modificateur de
Force si vous vous déplacez d'au moins 3 mètres à pied juste avant le
saut. Lorsque vous faites un saut en hauteur debout, vous ne pouvez
sauter que sur la moitié de cette distance. Dans tous les cas, chaque
pied que vous franchissez lors du saut vous coûte un pied de
déplacement. Dans certaines circonstances, votre MJ peut vous autoriser
à effectuer un test de Force (Athlétisme) pour sauter plus haut que vous
ne le pouvez normalement.

Vous pouvez étendre vos bras à la moitié de votre hauteur au-dessus de
vous pendant le saut. Ainsi, vous pouvez tendre les bras au-dessus de
vous sur une distance égale à la hauteur du saut plus 1 1/2 fois votre
taille.





## Environnement

Par nature, l'aventure implique de plonger dans des endroits sombres,
dangereux et pleins de mystères à explorer. Les règles de cette section
abritent quelques-uns des moyens les plus importants par lesquels les
aventuriers interagissent avec l'environnement dans de tels endroits.


### Falling

Une chute de grande hauteur est l'un des dangers les plus communs
auxquels est confronté un aventurier. À la fin d'une chute, une
créature subit 1d6 points de dégâts contondants par tranche de 10 pieds
de chute, jusqu'à un maximum de 20d6. La créature atterrit à plat
ventre, à moins qu'elle n'évite de subir les dégâts de la chute.



### Suffocant

Une créature peut retenir sa respiration pendant un nombre de minutes
égal à 1 + son modificateur de Constitution (minimum de 30 secondes).

Lorsqu'une créature manque de souffle ou s'étouffe, elle peut survivre
pendant un nombre de rounds égal à son modificateur de Constitution
(minimum de 1 round). Au début de son prochain tour, elle tombe à 0
point de vie et est en train de mourir, et elle ne peut pas regagner de
points de vie ou être stabilisée avant de pouvoir respirer à nouveau.

Par exemple, une créature avec une Constitution de 14 peut retenir sa
respiration pendant 3 minutes. Si elle commence à suffoquer, elle a 2
rounds pour atteindre l'air avant de tomber à 0 points de vie.



### Vision et lumière

Les tâches les plus fondamentales de l'aventure - remarquer un danger,
trouver des objets cachés, frapper un ennemi au combat et cibler un
sort, pour n'en citer que quelques-unes - dépendent fortement de la
capacité de vision du personnage. Les Ténèbres et autres effets qui
obscurcissent la vision peuvent s'avérer un obstacle important.

Une zone donnée peut être légèrement ou fortement obscurcie. Dans une
zone **faiblement obscurcie**, comme une lumière chétive, un brouillard
parsemé ou un feuillage modéré, les créatures ont un désavantage aux
tests de Sagesse (Perception) qui reposent sur la vue.

Une zone **fortement obscurcie** - comme les ténèbres, un brouillard
opaque ou un feuillage dense - bloque entièrement la vision. Une
créature souffre effectivement de l'état d'aveuglement  lorsqu'elle essaie de voir
quelque chose dans cette zone.

La présence ou l'absence de lumière dans un environnement crée trois
catégories d'éclairement : la lumière vive, la lumière chétive et les
ténèbres.

La**lumière vive** permet à la plupart des créatures de voir
normalement. Même les jours sombres fournissent une lumière vive, tout
comme les torches, les lanternes, les feux et autres sources
d'éclairage dans un rayon spécifique.

La**lumière chétive**, également appelée ombre, crée une zone légèrement
obscurcie. Une zone de lumière chétive est généralement une frontière
entre une source de lumière vive, telle qu'une torche, et l'obscurité
environnante. La lumière douce du crépuscule et de l'aube est également
considérée comme une lumière faible. Une pleine lune particulièrement
brillante peut baigner la terre d'une lumière faible.

Les**Ténèbres** créent une zone fortement obscurcie. Les personnages
sont confrontés aux Ténèbres en extérieur la nuit (même la plupart des
nuits de lune), dans les limites d'un donjon non éclairé ou d'une
voûte souterraine, ou dans une zone d'obscurité magique.


#### Vision aveugle

Une créature dotée de vision aveugle peut percevoir son environnement
sans se fier à la vue, dans un rayon spécifique. Les créatures sans
yeux, comme les oozes, et les créatures dotées d'écholocalisation ou de
sens exacerbés, comme les chauves-souris et les vrais dragons, possèdent
ce sens.



#### Vision dans le noir

De nombreuses créatures des mondes de jeux fantastiques, en particulier
celles qui vivent sous terre, ont une vision dans le noir. Dans une
portée donnée, une créature dotée de la vision dans le noir peut voir
dans l'obscurité comme si l'obscurité était une lumière chétive, de
sorte que les zones d'obscurité ne sont que légèrement obscurcies en ce
qui concerne cette créature. Cependant, la créature ne peut pas
discerner les couleurs dans l'obscurité, seulement les nuances de gris.



#### Vision véritable

Une créature dotée d'une vision véritable peut, jusqu'à une certaine
portée, voir dans les ténèbres normales et magiques, voir les créatures
et les objets invisibles, détecter automatiquement les illusions
visuelles et réussir ses jets de sauvegarde contre elles, et percevoir
la forme originale d'un métamorphe ou d'une créature transformée par
la magie. De plus, la créature peut voir dans le plan éthéré.




### Nourriture et eau

Les personnages qui ne mangent pas ou ne boivent pas subissent les
effets de l'épuisement .
L'épuisement causé par le manque de nourriture ou d'eau ne peut pas
être éliminé tant que le personnage n'a pas mangé et bu toute la
quantité requise.


#### Alimentation

Un personnage a besoin d'une livre de nourriture par jour et peut faire
durer la nourriture plus longtemps en subsistant avec des demi-rations.
Manger une demi-livre de nourriture dans une journée compte comme une
demi-journée sans nourriture.

Un personnage peut rester sans nourriture pendant un nombre de jours
égal à 3 + son modificateur de Constitution (minimum 1). À la fin de
chaque jour au-delà de cette limite, le personnage subit automatiquement
un niveau d'épuisement.

Une journée normale d'alimentation remet à zéro le nombre de jours sans
nourriture.



#### Eau

Un personnage a besoin d'un gallon d'eau par jour, ou de deux gallons
par jour si le temps est chaud. Un personnage qui ne boit que la moitié
de cette quantité d'eau doit réussir un jet de sauvegarde de
Constitution DC 15 ou subir un niveau d'épuisement à la fin de la
journée. Un personnage ayant accès à encore moins d'eau subit
automatiquement un niveau d'épuisement à la fin de la journée.

Si le personnage a déjà un ou plusieurs niveaux d'épuisement, il prend
deux niveaux dans les deux cas.





## Objets

L'interaction d'un personnage avec les objets d'un environnement est
souvent simple à résoudre dans le jeu. Le joueur dit au MJ que son
personnage fait quelque chose, comme déplacer un levier, et le MJ décrit
ce qui se passe, le cas échéant.

Par exemple, un personnage peut décider de tirer un levier qui, à son
tour, peut lever une herse, inonder une pièce ou ouvrir une porte
secrète dans un mur voisin. Mais si le levier est rouillé en position,
le personnage devra peut-être le forcer. Dans ce cas, le MJ peut
demander un test de Force pour voir si le personnage peut mettre le
levier en place. Le MJ fixe le DC de ce test en fonction de la
difficulté de la tâche.


### Briser des objets

Les personnages peuvent également endommager les objets avec leurs armes
et leurs sorts. Les objets sont immunisés contre le poison et les dégâts
psychiques, mais sinon ils peuvent être affectés par des attaques
physiques et magiques tout comme les créatures. Le MJ détermine la
classe d'armure et les points de vie d'un objet, et peut décider que
certains objets ont une résistance ou une immunité à certains types
d'attaques. (Il est difficile de couper une corde avec un gourdin, par
exemple.) Les objets échouent toujours aux jets de sauvegarde de Force
et de Dextérité, et ils sont immunisés contre les effets qui nécessitent
d'autres sauvegardes. Lorsqu'un objet tombe à 0 point de vie, il se
brise.

Un personnage peut également tenter un test de Force pour briser un
objet. Le MJ fixe le DC de ce test.

Lorsque les personnages doivent scier des cordes, briser une vitre ou
fracasser le cercueil d'un vampire, la seule règle absolue est la
suivante : avec suffisamment de temps et les bons outils, les
personnages peuvent détruire n'importe quel objet destructible.

Faites preuve de bon sens pour déterminer le succès d'un personnage à
endommager un objet. Un guerrier peut-il traverser une section d'un mur
de pierre avec une épée ? Non, l'épée risque de se briser avant le mur.

Aux fins de ces règles, un objet est un élément discret et inanimé comme
une fenêtre, une porte, une épée, un livre, une table, une chaise ou une
pierre, et non un bâtiment ou un véhicule composé de nombreux autres
objets.


#### Statistiques pour les objets

Lorsque le temps est un facteur, vous pouvez attribuer une classe
d'armure et des points de vie à un objet destructible. Vous pouvez
également lui conférer des immunités, des résistances et des
vulnérabilités à des types de dégâts spécifiques.

  -------------------------
  Substance             AC
  -------------------- ----
  Tissu, papier, corde  11

  Cristal, verre,       13
  glace                

  Bois, os              15

  Pierre                17

  Fer, acier            19

  Mithral               21

  Adamantine            23
  -------------------------

  : Classe d'armure de l'objet

  --------------------------------------------------------------
  Taille                                  Fragile    Résiliente
  -------------------------------------- ---------- ------------
  Très petite (bouteille, serrure)        2 (1d4)     5 (2d4)

  Petit (poitrine, luth)                  3 (1d6)     10 (3d6)

  Moyen (tonneau, lustre)                 4 (1d8)     18 (4d8)

  Grand (chariot, fenêtre de 10 pieds     5 (1d10)   27 (5d10)
  sur 10 pieds)                                     
  --------------------------------------------------------------

  : Points de vie des objets

***Classe d'armure.*** La classe d'armure d'un objet est une mesure
de la difficulté d'infliger des dégâts à l'objet lorsqu'il est frappé
(parce que l'objet n'a aucune chance d'esquiver). Le tableau Classe
d'armure des objets fournit des valeurs de CA suggérées pour diverses
substances.

***Objets Très grands et Gargantuesques.*** Les armes normales sont peu
utiles contre de nombreux objets Très grands et Gargantuesques, tels
qu'une statue colossale, une colonne de pierre imposante ou un rocher
massif. Cela dit, une torche peut brûler une Très grande tapisserie, et
un sort de *tremblement de terre* peut réduire un colosse
en ruines. Vous pouvez suivre les points de vie d'un objet Très grand
ou Gargantuesque si vous le souhaitez, ou vous pouvez simplement décider
de la durée pendant laquelle l'objet peut résister à l'arme ou à la
force qui agit sur lui. Si vous comptabilisez les points de vie de
l'objet, divisez-le en Grandes ou en petites sections, et comptabilisez
les points de vie de chaque section séparément. Détruire l'une de ces
sections peut ruiner l'objet tout entier. Par exemple, une statue
Gargantuesque d'un humain peut basculer si l'une de ses Grandes jambes
est réduite à 0 point de vie.

***Objets et types de dégâts.*** Les objets sont immunisés contre le
poison et les dégâts psychiques. Vous pouvez décider que certains types
de dégâts sont plus efficaces que d'autres contre un objet ou une
substance particulière. Par exemple, les dégâts contondants sont
efficaces pour briser des objets, mais pas pour couper une corde ou du
cuir. Les objets en papier ou en tissu peuvent être vulnérables aux
dégâts du feu et de la foudre. Un pic peut ébrécher une pierre mais ne
peut pas couper efficacement un arbre. Comme toujours, faites preuve de
discernement.

***Seuil de dégâts.*** Les gros objets tels que les murs des châteaux
ont souvent une résilience supplémentaire représentée par un seuil de
dégâts. Un objet avec un seuil de dégâts est immunisé contre tous les
dégâts, sauf s'il subit une quantité de dégâts d'une seule attaque ou
d'un seul effet égale ou supérieure à son seuil de dégâts, auquel cas
il subit des dégâts normalement. Tout dommage qui n'atteint ou ne
dépasse pas le seuil de dégâts de l'objet est considéré comme
superficiel et ne rapetisse pas les points de vie de l'objet. Points de
vie. Les points de vie d'un objet mesurent la quantité de dégâts qu'il
peut subir avant de perdre son intégrité structurelle. Les objets
résilients ont plus de points de vie que les objets fragiles. Les grands
objets ont également tendance à avoir plus de points de vie que les
petits, à moins que briser une petite partie de l'objet soit aussi
efficace que de le briser en entier. La table des points de vie des
objets fournit des suggestions de points de vie pour les objets fragiles
et résilients qui sont Grands ou plus petits.





## Reposez-vous

Aussi héroïques qu'ils puissent être, les aventuriers ne peuvent pas
passer toutes les heures de la journée dans l'exploration, les
interactions sociales et les combats. Ils ont besoin de temps de repos
pour dormir et manger, soigner leurs blessures, se rafraîchir l'esprit
et l'âme pour l'Incantation, et se consolider pour la suite de
l'aventure. Les aventuriers peuvent prendre de courts repos au milieu
d'une journée d'aventure et un long repos pour terminer la journée.


### Repos court

Un repos court est une période d'immobilisation d'au moins une heure
pendant laquelle le personnage ne fait rien de plus fatiguant que de
manger, boire, lire et soigner ses blessures.

Un personnage peut dépenser un ou plusieurs dés de coups à la fin d'un
repos court, dans la limite du nombre maximal de dés de coups du
personnage, qui est égal au niveau du personnage. Pour chaque dé de
points de vie dépensé de cette manière, le joueur lance le dé et y
ajoute le modificateur de Constitution du personnage. Le personnage
regagne un nombre de points de vie égal au total. Le joueur peut décider
de dépenser un Dé de Vie supplémentaire après chaque jet. Un personnage
regagne des dés de points de vie dépensés à la fin d'un long repos,
comme expliqué ci-dessous.



### Repos long

Un repos long est une période d'immobilisation prolongée, d'au moins 8
heures, pendant laquelle un personnage dort ou exerce une activité
légère : lire, parler, manger ou monter la garde pendant 2 heures
maximum. Si le repos est interrompu par une période d'activité intense
(au moins 1 heure de marche, de combat, de lancement de sorts ou
d'activités d'aventure similaires), les personnages doivent
recommencer le repos pour en tirer un quelconque avantage.

À la fin d'un repos long, un personnage regagne tous les points de vie
perdus. Le personnage regagne également les dés de coups dépensés,
jusqu'à un nombre de dés égal à la moitié du nombre total de dés de
coups du personnage (minimum d'un dé). Par exemple, si un personnage a
huit dés de points de vie, il peut en regagner quatre dépensés à la fin
d'un long repos.

Un personnage ne peut pas bénéficier de plus d'un repos long par
période de 24 heures, et il doit avoir au moins 1 point de vie au début
du repos pour en tirer les bénéfices.




## Entre deux aventures

Entre les voyages dans les donjons et les batailles contre les maux
anciens, les aventuriers ont besoin de temps pour se reposer, récupérer
et se préparer pour leur prochaine aventure.

De nombreux aventuriers utilisent également ce temps pour effectuer
d'autres tâches, comme la fabrication d'armes et d'armures, la
recherche ou la dépense de leur or durement gagné.

Dans certains cas, le passage du temps est quelque chose qui se produit
avec peu de fanfare ou de description. Au début d'une nouvelle
aventure, le MJ peut simplement déclarer qu'un certain temps s'est
écoulé et vous permettre de décrire en termes généraux ce que votre
personnage a fait. A d'autres moments, le MJ peut vouloir garder une
trace du temps qui passe, alors que des événements hors de votre
perception sont en mouvement.


### Dépenses liées au mode de vie

Entre deux aventures, vous choisissez une qualité de vie particulière et
payez le coût du maintien de ce mode de vie.

Le fait de mener un style de vie particulier n'a pas un effet énorme
sur votre personnage, mais votre style de vie peut affecter la façon
dont les autres individus et groupes réagissent à votre égard. Par
exemple, si vous menez un style de vie aristocratique, il vous sera
peut-être plus facile d'influencer les nobles de la ville que si vous
vivez dans la pauvreté.



### Activités en temps d'arrêt

Entre deux aventures, le MJ peut vous demander ce que votre personnage
fait pendant ses temps morts. Les périodes d'indisponibilité peuvent
varier en durée, mais chaque activité d'indisponibilité nécessite un
certain nombre de jours pour être achevée avant d'en tirer un
quelconque avantage, et au moins 8 heures de chaque jour doivent être
bénies à l'activité d'indisponibilité pour que le jour compte. Il
n'est pas nécessaire que les jours soient consécutifs. Si vous avez
plus que le nombre minimum de jours à passer, vous pouvez continuer à
faire la même chose pendant une période plus longue, ou passer à une
nouvelle activité d'arrêt.

Des activités de temps d'arrêt autres que celles présentées ci-dessous
sont possibles. Si vous souhaitez que votre personnage passe son temps
libre à effectuer une activité qui n'est pas couverte ici, discutez-en
avec votre MJ.


#### Artisanat

Vous pouvez fabriquer des objets non magiques, y compris des équipements
d'aventure et des œuvres d'art. Vous devez avoir une bonne maîtrise
des outils liés à l'objet que vous essayez de créer (généralement des
outils d'artisan). Vous pouvez également avoir besoin d'accéder à des
matériaux ou des lieux particuliers pour le créer. Par exemple, une
personne maîtrisant les outils du forgeron a besoin d'une forge pour
fabriquer une épée ou une armure.

Pour chaque jour d'arrêt que vous passez à fabriquer, vous pouvez
fabriquer un ou plusieurs objets dont la valeur marchande totale ne
dépasse pas 5 gp, et vous devez dépenser des matières premières valant
la moitié de la valeur marchande totale. Si l'objet que vous souhaitez
fabriquer a une valeur marchande supérieure à 5 gp, vous progressez
chaque jour par tranches de 5 gp jusqu'à ce que vous atteigniez la
valeur marchande de l'objet. Par exemple, il faut 300 jours pour
fabriquer soi-même une armure en plaques (valeur marchande 1 500 gp).

Plusieurs personnages peuvent combiner leurs efforts pour fabriquer un
seul objet, à condition qu'ils maîtrisent tous les outils nécessaires
et qu'ils travaillent ensemble au même endroit.

Chaque personnage fournit 5 gp d'effort pour chaque jour passé à
participer à la fabrication de l'objet. Par exemple, trois personnages
ayant la maîtrise des outils requis et les installations adéquates
peuvent fabriquer une armure harnois en 100 jours, pour un coût total de
750 gp.

Pendant l'artisanat, vous pouvez maintenir un mode de vie modeste sans
avoir à payer 1 gp par jour, ou un mode de vie confortable à la moitié
du coût normal.



#### Exercer une profession

Vous pouvez travailler entre deux aventures, ce qui vous permet de
maintenir un mode de vie modeste sans avoir à payer 1 gp par jour. Cet
avantage dure aussi longtemps que vous continuez à exercer votre
profession.

Si vous êtes membre d'une organisation susceptible de vous fournir un
emploi rémunéré, comme un temple ou une guilde de voleurs, vous gagnez
suffisamment pour mener une vie confortable.

Si vous avez la maîtrise de la compétence Représentation et que vous
mettez à profit cette compétence pendant vos temps morts, vous gagnez
suffisamment pour mener un style de vie aisé.



#### Récupérer

Vous pouvez utiliser les temps d'arrêt entre les aventures pour
récupérer d'une blessure, d'une maladie ou d'un poison débilitant.

Après trois jours d'arrêt passés à récupérer, vous pouvez effectuer un
jet de sauvegarde de Constitution DC 15. En cas de sauvegarde réussie,
vous pouvez choisir l'un des résultats suivants :

-   Mettez fin à un effet qui vous empêche de regagner des points de
    vie.
-   Pendant les 24 prochaines heures, vous bénéficiez d'un avantage aux
    jets de sauvegarde contre une maladie ou un poison qui vous affecte.



#### Chercheur

Le temps qui s'écoule entre les aventures est une excellente occasion
de faire des recherches et de mieux comprendre les mystères qui se sont
révélés au cours de la campagne. Les recherches peuvent consister à
parcourir des tomes poussiéreux et des parchemins en ruine dans une
bibliothèque ou à payer des boissons aux habitants pour leur arracher
des rumeurs et des commérages.

Lorsque vous commencez vos recherches, le MJ détermine si les
informations sont disponibles, combien de jours d'arrêt seront
nécessaires pour les trouver, et s'il y a des restrictions à vos
recherches (comme la nécessité de chercher un individu, un tome ou un
lieu spécifique). Le MJ peut également vous demander d'effectuer un ou
plusieurs tests de capacité, comme un test d'Intelligence
(Investigation) pour trouver des indices menant aux informations que
vous recherchez, ou un test de Charisme (Persuasion) pour obtenir
l'aide de quelqu'un. Une fois ces étâts remplis, vous apprenez
l'information si elle est disponible.

Pour chaque jour de recherche, vous devez dépenser 1 gp pour couvrir vos
dépenses. Ce coût s'ajoute aux dépenses normales de votre mode de vie.



#### Formation

Vous pouvez passer du temps entre deux aventures à apprendre une
nouvelle langue ou à vous entraîner avec un ensemble d'outils. Votre MJ
peut autoriser des options de formation supplémentaires.

Tout d'abord, vous devez trouver un instructeur prêt à vous enseigner.
Le MJ détermine le temps que cela prend, et si un ou plusieurs tests
d'aptitude sont nécessaires.

La formation dure 250 jours et coûte 1 gp par jour. Après avoir dépensé
la quantité de temps et d'argent requise, vous apprenez la nouvelle
langue ou gagnez la maîtrise du nouvel outil.





## États

Les états modifient les capacités d'une créature de diverses manières
et peuvent résulter d'un sort, d'une caractéristique de classe, de
l'attaque d'un monstre ou d'un autre effet. La plupart des
conditions, comme la cécité, sont des handicaps, mais certaines, comme
l'invisibilité, peuvent être avantageuses.

Un état dure soit jusqu'à ce qu'il soit contré (l'état allongé est
contré en se levant, par exemple), soit pour une durée spécifiée par
l'effet qui a imposé l'état.

Si plusieurs effets imposent le même étât à une créature, chaque
instance de l'étât a sa propre durée, mais les effets de l'étât ne
s'aggravent pas.

Une créature est soumise à un étât ou ne l'est pas. Les définitions
suivantes précisent ce qui arrive à une créature lorsqu'elle est
soumise à un étât.


#### Cécité

-   Une créature aveuglée ne peut pas voir et échoue automatiquement à
    tout test de capacité nécessitant la vue.
-   Les jets d'attaque contre la créature ont un avantage, et les jets
    d'attaque de la créature ont un désavantage.



#### Charmé

-   Une créature charmée ne peut pas attaquer le charmeur ou le cibler
    avec des capacités nuisibles ou des effets magiques.
-   Le charmeur a un avantage sur tout test de caractéristique pour
    interagir socialement avec la créature.



#### Assourdi

-   Une créature assourdie ne peut pas entendre et échoue
    automatiquement à tout test de capacité nécessitant l'audition.



#### Épuisement

Certaines capacités spéciales et certains risques environnementaux,
comme la famine et les effets à long terme des températures glaciales ou
brûlantes, peuvent entraîner un état spécial appelé épuisement.
L'épuisement se mesure en six niveaux. Un effet peut donner à une
créature un ou plusieurs niveaux d'épuisement, comme spécifié dans la
description de l'effet.

  --------------------------------------------------
   Niveau  Effet
  -------- -----------------------------------------
     1     Désavantage sur les contrôles
           d'aptitude.

     2     Vitesse réduite de moitié

     3     Désavantage aux jets d'attaque et aux
           jets de sauvegarde.

     4     Réduction de moitié du nombre maximum de
           points de vie

     5     Vitesse réduite à 0

     6     Décès
  --------------------------------------------------

Si une créature déjà épuisée subit un autre effet qui provoque
l'épuisement, son niveau d'épuisement actuel augmente de la quantité
spécifiée dans la description de l'effet.

Une créature subit l'effet de son niveau d'épuisement actuel ainsi que
de tous les niveaux inférieurs. Par exemple, une créature souffrant
d'épuisement de niveau 2 voit sa vitesse réduite de moitié et a un
désavantage aux tests d'aptitude.

Un effet qui supprime l'épuisement réduit son niveau comme indiqué dans
la description de l'effet, tous les effets d'épuisement prenant fin si
le niveau d'épuisement d'une créature est réduit en dessous de 1.

La fin d'un repos long réduit le niveau d'épuisement d'une créature
de 1, à condition qu'elle ait également ingéré de la nourriture et des
boissons.



#### Effrayé

-   Une créature effrayée a un désavantage sur les tests d'aptitude et
    les jets d'attaque lorsque la source de sa peur est dans sa ligne
    de vue.
-   La créature ne peut pas se rapprocher volontairement de la source de
    sa peur.



#### Agrippé

-   La vitesse d'une créature agrippée devient 0, et elle ne peut
    bénéficier d'aucun bonus à sa vitesse.
-   L'étât prend fin si le grappler est frappé d'incapacité.
-   La condition prend également fin si un effet retire la créature
    agrippée de la portée du lutteur ou de l'effet du grappin, comme
    lorsqu'une créature est projetée au loin par le sort Vague
    *tonnante*.



#### Invalidité

-   Une créature frappée d'incapacité ne peut pas effectuer d'actions
    ou de réactions.



#### Invisibilité

-   Une créature invisible est impossible à voir sans l'aide de la
    magie ou d'un sens particulier. Pour se cacher, la créature est
    fortement obscurcie. L'emplacement de la créature peut être détecté
    par tout bruit qu'elle fait ou toute trace qu'elle laisse.
-   Les jets d'attaque contre la créature ont un désavantage, et les
    jets d'attaque de la créature ont un avantage.



#### Paralysés

-   Une créature paralysée est frappée d'incapacité  et
    ne peut ni bouger ni parler.
-   La créature rate automatiquement les jets de sauvegarde de Force et
    de Dextérité.
-   Les jets d'attaque contre la créature ont un avantage.
-   Toute attaque qui touche la créature est un coup critique si
    l'attaquant se trouve à moins de 1,5 mètre de la créature.



#### Pétrifié

-   Une créature pétrifiée est transformée, ainsi que tout objet non
    magique qu'elle porte ou transporte, en une substance inanimée
    solide (généralement de la pierre). Son poids augmente d'un facteur
    dix, et elle cesse de vieillir.
-   La créature est frappée d'incapacité , ne peut ni
    bouger ni parler, et n'a pas conscience de son environnement.
-   Les jets d'attaque contre la créature ont un avantage.
-   La créature rate automatiquement les jets de sauvegarde de Force et
    de Dextérité.
-   La créature possède une résistance à tous les dégâts.
-   La créature est immunisée contre le poison et les maladies, bien
    qu'un poison ou une maladie déjà présents dans son système soient
    suspendus, mais pas neutralisés.



#### Empoisonné

-   Une créature empoisonnée a un désavantage sur les jets d'attaque et
    les tests de capacité.



#### À terre

-   La seule option de mouvement d'une créature couchée est de ramper,
    à moins qu'elle ne se lève et mette ainsi fin à l'étât.
-   La créature a un désavantage sur les jets d'attaque.
-   Un jet d'attaque contre la créature a un avantage si l'attaquant
    se trouve à moins de 1,5 m de la créature. Sinon, le jet d'attaque
    est désavantagé.



#### Entravé

-   La vitesse d'une créature entravée devient 0, et elle ne peut
    bénéficier d'aucun bonus à sa vitesse.
-   Les jets d'attaque contre la créature ont un avantage, et les jets
    d'attaque de la créature ont un désavantage.
-   La créature a un désavantage aux jets de sauvegarde de Dextérité.



#### Étourdi

-   Une créature étourdie est frappée d'incapacité, ne
    peut pas bouger et ne peut parler que de façon hésitante.
-   La créature rate automatiquement les jets de sauvegarde de Force et
    de Dextérité.
-   Les jets d'attaque contre la créature ont un avantage.



#### inconscient

-   Une créature inconsciente est frappée d'incapacité ,
    ne peut ni bouger ni parler, et n'a pas conscience de son
    environnement.
-   La créature laisse tomber ce qu'elle tient et tombe à plat ventre.
-   La créature rate automatiquement les jets de sauvegarde de Force et
    de Dextérité.
-   Les jets d'attaque contre la créature ont un avantage.
-   Toute attaque qui touche la créature est un coup critique si
    l'attaquant se trouve à moins de 1,5 mètre de la créature.




## Poisons

Étant donné leur nature insidieuse et mortelle, les poisons sont
illégaux dans la plupart des sociétés, mais sont un outil de
prédilection pour les assassins, les Drow et autres créatures
maléfiques.

Les poisons se présentent sous les quatre formes suivantes.

***Contact.*** Le poison de contact peut être étalé sur un objet et
reste puissant jusqu'à ce qu'il soit touché ou lavé. Une créature qui
touche le poison de contact avec la peau exposée en subit les effets.

***Ingéré.*** Une créature doit avaler une dose entière de poison ingéré
pour en subir les effets. La dose peut être délivrée dans de la
nourriture ou un liquide. Vous pouvez décider qu'une dose partielle a
un effet réduit, par exemple en accordant un avantage sur le jet de
sauvegarde ou en n'infligeant que la moitié des dégâts en cas d'échec.

***Inhalés.*** Ces poisons sont des poudres ou des gaz qui font effet
lorsqu'ils sont inhalés. Souffler la poudre ou libérer le gaz soumet à
son effet les créatures situées dans un cube de 1,5 m de côté. Le nuage
qui en résulte se dissipe immédiatement après. Retenir sa respiration
est inefficace contre les poisons inhalés, car ils affectent les
membranes nasales, les canaux lacrymaux et d'autres parties du corps.

***Blessure.*** Le poison de blessure peut être appliqué aux armes,
munitions, composants de pièges et autres objets qui infligent des
dégâts perforants ou tranchants et reste puissant jusqu'à ce qu'il
soit délivré par une blessure ou lavé. Une créature qui subit des dégâts
perforants ou tranchants d'un objet enduit de ce poison est exposée à
ses effets.

  ----------------------------------------------
  Article             Type         Prix par dose
  ------------------- ------------ -------------
  Le sang d'un       Ingéré       150 gp
  Assassinat                       

  Fumées d'othur     Inhalation   500 gp
  brûlé                            

  Mucus de chenille   Contact      200 gp

  Drow poison         Blessure     200 gp

  Essence d'éther    Inhalation   300 gp

  Malice              Inhalation   250 gp

  Larmes de minuit    Ingéré       1 500 gp

  Huile de taggit     Contact      400 gp

  Teinture pâle       Ingéré       250 gp

  Empoisonnement du   Blessure     2 000 gp
  ver violet                       

  Venin de serpent    Blessure     200 gp

  Torpeur             Ingéré       600 gp

  Le sérum de vérité  Ingéré       150 gp

  Wiverne poison      Blessure     1 200 gp
  ----------------------------------------------

  : Poisons


### Echantillon de poisons

Chaque type de poison a ses propres effets débilitants.

***Sang d'Assassinat (ingéré).*** Une créature soumise à ce poison doit
effectuer un jet de sauvegarde de Constitution DC 10. En cas d'échec,
elle subit 6 (1d12) dégâts de poison et est empoisonnée pendant 24
heures. En cas de sauvegarde réussie, la créature subit la moitié des
dégâts et n'est pas empoisonnée.

***Fumées d'Othur brûlées (inhalées).*** Une créature soumise à ce
poison doit réussir un jet de sauvegarde de Constitution DC 13 ou subir
10 (3d6) dégâts de poison, et doit répéter le jet de sauvegarde au début
de chacun de ses tours. À chaque échec successif du jet de sauvegarde,
le personnage subit 3 (1d6) dégâts de poison. Après trois sauvegardes
réussies, le poison prend fin.

***Mucus de chenille (Contact).*** Ce poison doit être récolté sur une
chenille morte ou incapacitée. Une créature soumise à ce poison doit
réussir un jet de sauvegarde de Constitution DC 13 ou être empoisonnée
pendant 1 minute. La créature empoisonnée est paralysée. La créature
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de réussite.

***Poison Drow (blessure).*** Ce poison n'est typiquement fabriqué que
par le Drow, et uniquement dans un endroit éloigné de la lumière du
soleil. Une créature soumise à ce poison doit réussir un jet de
sauvegarde de Constitution DC 13 ou être empoisonnée pendant 1 heure. Si
le jet de sauvegarde échoue par 5 ou plus, la créature est également
inconsciente lorsqu'elle est empoisonnée de cette manière. La créature
se réveille si elle subit des dégâts ou si une autre créature entreprend
une action pour la secouer.

***Essence d'éther (Inhalation).*** Une créature soumise à ce poison
doit réussir un jet de sauvegarde de Constitution DC 15 ou
s'empoisonner pendant 8 heures. La créature empoisonnée est
inconsciente. La créature se réveille si elle subit des dégâts ou si une
autre créature entreprend une action pour la secouer.

***Malice (Inhalé).*** Une créature soumise à ce poison doit réussir un
jet de sauvegarde de Constitution DC 15 ou s'empoisonner pendant 1
heure. La créature empoisonnée est aveuglée.

***Larmes de minuit (ingéré).*** Une créature qui ingère ce poison ne
subit aucun effet jusqu'aux douze coups de minuit. Si le poison n'a
pas été neutralisé avant cela, la créature doit réussir un jet de
sauvegarde de Constitution DC 17, subissant 31 (9d6) dégâts de poison en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Huile de Taggit (Contact).*** Une créature soumise à ce poison doit
réussir un jet de sauvegarde de Constitution DC 13 ou s'empoisonner
pendant 24 heures. La créature empoisonnée est inconsciente. La créature
se réveille si elle subit des dégâts.

***Teinture pâle (ingérée).*** Une créature soumise à ce poison doit
réussir un jet de sauvegarde de Constitution DC 16 ou subir 3 (1d6)
dégâts de poison et s'empoisonner. La créature empoisonnée doit répéter
le jet de sauvegarde toutes les 24 heures, subissant 3 (1d6) dégâts de
poison en cas d'échec. Jusqu'à la fin de ce poison, les dégâts qu'il
inflige ne peuvent être soignés par aucun moyen. Après sept jets de
sauvegarde réussis, l'effet prend fin et la créature peut se soigner
normalement.

***Poison de ver pourpre (blessure).*** Ce poison doit être récolté sur
un ver pourpre mort ou frappé d'incapacité. Une créature soumise à ce
poison doit effectuer un jet de sauvegarde de Constitution DC 19,
subissant 42 (12d6) dégâts de poison en cas d'échec, ou la moitié des
dégâts en cas de réussite.

***Venin de serpent (blessure).*** Ce poison doit être récolté sur un
serpent venimeux géant mort ou frappé d'incapacité. Une créature
soumise à ce poison doit réussir un jet de sauvegarde de Constitution DC
11, subissant 10 (3d6) dégâts de poison en cas d'échec, ou la moitié
des dégâts en cas de réussite.

***Torpeur (ingéré).*** Une créature soumise à ce poison doit réussir un
jet de sauvegarde de Constitution DC 15 ou s'empoisonner pendant 4d6
heures. La créature empoisonnée est frappée d'incapacité.

***Sérum de vérité (ingéré).*** Une créature soumise à ce poison doit
réussir un jet de sauvegarde de Constitution DC 11 ou s'empoisonner
pendant 1 heure. La créature empoisonnée ne peut pas sciemment dire un
mensonge, comme si elle était sous l'effet d'un sort de *zone de
vérité*.

***Poison de wiverne (blessure).*** Ce poison doit être récolté sur une
wyvern morte ou incapacitée. Une créature soumise à ce poison doit
effectuer un jet de sauvegarde de Constitution DC 15, subissant 24 (7d6)
dégâts de poison en cas d'échec, ou la moitié des dégâts en cas de
réussite.




## Maladies

Une peste ravage le royaume, mettant les aventuriers en quête d'un
remède. Une aventurière émerge d'une tombe ancienne, non ouverte depuis
des siècles, et se retrouve bientôt atteinte d'une maladie mortelle. Un
sorcier offense une puissance obscure et contracte une étrange
affliction qui se propage chaque fois qu'il jette des sorts.

Une simple épidémie peut se résumer à une petite ponction sur les
ressources du groupe, qui peut être soignée par un jet de *restauration
partielle*. Une épidémie plus complexe peut
constituer la base d'une ou plusieurs aventures, les personnages
cherchant un remède, arrêtant la propagation de la maladie et gérant les
conséquences.

Une maladie qui fait plus qu'infecter quelques membres du groupe est
avant tout un élément de l'intrigue. Les règles aident à décrire les
effets de la maladie et la façon dont elle peut être soignée, mais les
spécificités du fonctionnement d'une maladie ne sont pas liées à un
ensemble commun de règles. Les maladies peuvent affecter n'importe
quelle créature, et une maladie donnée peut ou non passer d'une origine ou
d'un type de créature à un autre. Un fléau peut n'affecter que les
constructions ou les morts-vivants, ou balayer un quartier de halfelins
mais laisser les autres origines intactes. Ce qui compte, c'est
l'histoire que vous voulez raconter.


### Exemples de maladies

Les maladies présentées ici illustrent les différentes façons dont les
maladies peuvent fonctionner dans le jeu. N'hésitez pas à modifier les
jets de sauvegarde, les temps d'incubation, les symptômes et les autres
caractéristiques de ces maladies pour les adapter à votre campagne.


#### La fièvre du caquetage

Cette maladie cible les humanoïdes, bien que les
gnomes soient étrangement immunisés. Lorsqu'elles
sont en proie à cette maladie, les victimes succombent fréquemment à des
crises de fou rire, ce qui a donné à la maladie son nom commun et son
surnom morbide : \"les cris\".

Les symptômes se manifestent 1d4 heures après l'infection et
comprennent la fièvre et la désorientation. La créature infectée gagne
un niveau d'épuisement qui ne peut être supprimé tant que la maladie
n'est pas guérie.

Tout événement qui cause un grand stress à la créature infectée - y
compris le fait de participer à un combat, de subir des dégâts, d'avoir
peur ou de faire un cauchemar - force la créature à effectuer un jet de
sauvegarde de Constitution DC 13. En cas d'échec, la créature subit 5
(1d10) dégâts psychiques et devient incapable de rire pendant 1 minute.
La créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin au fou rire et à l'état d'incapacité en cas de
réussite.

Toute créature humanoïde qui commence son tour à moins de 3 mètres
d'une créature infectée en proie à un fou rire doit réussir un jet de
sauvegarde de Constitution DC 10 ou être également infectée par la
maladie. Lorsqu'une créature réussit ce jet de sauvegarde, elle est
immunisée contre le fou rire de cette créature infectée particulière
pendant 24 heures.

À la fin de chaque repos long, une créature infectée peut effectuer un
jet de sauvegarde de Constitution DC 13. En cas de sauvegarde réussie,
le DC de cette sauvegarde et de la sauvegarde pour éviter une attaque de
fou rire diminue de 1d6. Lorsque le jet de sauvegarde tombe à 0, la
créature se remet de la maladie. Une créature qui échoue à trois de ces
jets de sauvegarde gagne une forme de folie
indéfinie déterminée au hasard.



#### Peste des égouts

La peste des égouts est un terme générique pour une large catégorie de
maladies qui incubent dans les égouts, les tas d'ordures et les
marécages stagnants, et qui sont parfois transmises par des créatures
qui vivent dans ces zones, comme les rats et les
otyughs.

Lorsqu'une créature humanoïde est mordue par une créature porteuse de
la maladie, ou lorsqu'elle entre en contact avec des immondices ou des
abats contaminés par la maladie, elle doit réussir un jet de sauvegarde
de Constitution DC 11 ou être infectée.

Il faut 1d4 jours pour que les symptômes de la peste des égouts se
manifestent chez une créature infectée. Les symptômes comprennent la
fatigue et les crampes. La créature infectée souffre d'un niveau
d'épuisement, et elle ne récupère que la moitié du nombre normal de
points de vie en dépensant des dés de points de vie, et aucun point de
vie en terminant un repos long.

À la fin de chaque repos long, une créature infectée doit effectuer un
jet de sauvegarde de Constitution DC 11. En cas d'échec, le personnage
gagne un niveau d'épuisement. En cas de sauvegarde réussie, le niveau
d'épuisement du personnage diminue d'un niveau. Si un jet de
sauvegarde réussi réduit le niveau d'épuisement de la créature infectée
en dessous de 1, la créature se remet de la maladie.



#### Rotation de la vue

Cette infection douloureuse provoque des saignements des yeux et finit
par rendre la victime aveugle.

Une bête ou un humanoïde qui boit de l'eau souillée par la pourriture
de la vue doit réussir un jet de sauvegarde de Constitution DC 15 ou
être infecté. Un jour après l'infection, la vision de la créature
commence à se troubler. La créature subit un malus de -1 aux jets
d'attaque et aux tests de capacité qui dépendent de la vue. À la fin de
chaque repos long après l'apparition des symptômes, la pénalité
s'aggrave de 1. Lorsqu'elle atteint -5, la victime est aveuglée
jusqu'à ce que sa vue soit restaurée par une magie telle que
Restauration *partielle*.

La pourriture de la vue peut être soignée à l'aide d'une fleur rare,
l'euphorbe, qui pousse dans certains marais. En une heure, un
personnage ayant la maîtrise d'un kit d'herboristerie peut transformer
la fleur en une dose d'onguent. Appliquée sur les yeux avant un long
repos, une dose de cette pommade empêche la maladie de s'aggraver après
ce repos. Après trois doses, l'onguent guérit entièrement la maladie.





## La folie

Dans une campagne typique, les personnages ne sont pas rendus fous par
les horreurs qu'ils affrontent et le carnage qu'ils infligent jour
après jour, mais parfois le stress d'être un aventurier peut être trop
lourd à supporter. Si votre campagne a un thème d'horreur fort, vous
pouvez utiliser la folie pour renforcer ce thème, en soulignant la
nature extraordinairement horrible des menaces auxquelles les
aventuriers sont confrontés.


### Devenir fou

Divers effets magiques peuvent infliger la folie à un esprit autrement
stable. Certains sorts, comme Contact avec *autre
plan*, peuvent provoquer
la folie, et vous pouvez utiliser les règles de folie ici au lieu des
effets de ces sorts. Les maladies, les poisons et les effets planaires
tels que le vent psychique ou les vents hurlants d'un plan chaotique
peuvent tous infliger la folie. Certains artefacts peuvent également
briser la psyché d'un personnage qui les utilise ou qui y est lié.

Résister à un effet provoquant la folie nécessite généralement un jet de
sauvegarde de Sagesse ou de Charisme.



### Effets de la folie

La folie peut être à court terme, à long terme ou indéfinie. La plupart
des effets relativement banals imposent une folie à court terme, qui ne
dure que quelques minutes. Des effets plus horribles ou des effets
cumulatifs peuvent entraîner une folie à long terme ou indéfinie.

Un personnage affligé de **folie à court terme** est soumis à un effet
de la table de folie à court terme pendant 1d10 minutes.

  ------------------------------------------------------------------------
    d100   Effet (dure 1d10 minutes)
  -------- ---------------------------------------------------------------
   01-20   Le personnage se replie dans son esprit et devient paralysé.
           L'effet prend fin si le personnage subit des dégâts.

   21-30   Le personnage est frappé d'incapacité et passe la durée à
           crier, rire ou pleurer.

   31-40   Le personnage est effrayé et doit utiliser son action et son
           mouvement chaque round pour fuir la source de la peur.

   41-50   Le personnage se met à bafouiller et est incapable de parler
           normalement ou de lancer des incantations.

   51-60   Le personnage doit utiliser son action chaque round pour
           attaquer la créature la plus proche.

   61-70   Le personnage a de vives hallucinations et a un désavantage sur
           les tests de capacité.

   71-75   Le personnage fait tout ce qu'on lui dit de faire et qui
           n'est pas manifestement autodestructeur.

   76-80   Le personnage ressent une envie irrésistible de manger quelque
           chose d'étrange comme de la terre, de la bave ou des abats.

   81-90   Le personnage est étourdi.

   91-100  Le personnage tombe inconscient.
  ------------------------------------------------------------------------

  : La folie du court terme

Un personnage atteint de **folie à long terme** est soumis à un effet de
la table Folie à long terme pendant 1d10 × 10 heures.

  ------------------------------------------------------------------------
    d100   Effet (dure 1d10 × 10 heures)
  -------- ---------------------------------------------------------------
   01-10   Le personnage se sent obligé de répéter sans cesse une activité
           spécifique, comme se laver les mains, toucher des objets, prier
           ou compter des pièces.

   11-20   Le personnage a de vives hallucinations et a un désavantage sur
           les tests de capacité.

   21-30   Le personnage souffre d'une paranoïa extrême. Le personnage a
           un désavantage aux tests de Sagesse et de Charisme.

   31-40   Le personnage considère quelque chose (généralement la source
           de la folie) avec une répulsion intense, comme s'il était
           affecté par l'effet d'*antipathie* du sort
           *Aversion/sympathie*.

   41-45   Le personnage fait l'expérience d'un puissant délire.
           Choisissez une potion. Le personnage s'imagine qu'il est sous
           ses effets.

   46-55   Le personnage s'attache à un \"porte-bonheur\", comme une
           personne ou un objet, et a un désavantage aux jets d'attaque,
           aux tests de capacité et aux jets de sauvegarde lorsqu'il se
           trouve à plus de 10 mètres de lui.

   56-65   Le personnage est aveuglé (25%) ou assourdi (75%).

   66-75   Le personnage subit des tremblements ou des tics
           incontrôlables, qui lui imposent un désavantage aux jets
           d'attaque, aux tests de capacité et aux jets de sauvegarde qui
           impliquent la Force ou la Dextérité.

   76-85   Le personnage souffre d'une amnésie partielle. Le personnage
           sait qui il est et conserve ses traits raciaux et ses capacités
           de classe, mais il ne reconnaît pas les autres personnes et ne
           se souvient pas de ce qui s'est passé avant que la folie ne
           fasse effet.

   86-90   Chaque fois que le personnage subit des dégâts, il doit réussir
           un jet de sauvegarde de Sagesse DC 15 ou être affecté comme
           s'il avait raté un jet de sauvegarde contre le sort de
           *confusion*. L'effet de confusion dure 1 minute.

   91-95   Le personnage perd la capacité de parler.

   96-100  Le personnage tombe inconscient. Aucune bousculade ou dommage
           ne peut réveiller le personnage.
  ------------------------------------------------------------------------

  : La folie à long terme

Un personnage atteint de **folie indéfinie** gagne un nouveau défaut de
caractère de la table Folie indéfinie qui dure jusqu'à sa guérison.

  ------------------------------------------------------------------------
    d100   Défaut (dure jusqu'à ce qu'il soit corrigé)
  -------- ---------------------------------------------------------------
   01-15   \"Etre saoul me permet de rester sain d'esprit.\"

   16-25   \"Je garde tout ce que je trouve.\"

   26-30   \"J'essaie de ressembler davantage à une personne que je
           connais, en adoptant son style vestimentaire, ses manières et
           son nom.\"

   31-35   \"Je dois déformer la vérité, exagérer ou mentir carrément pour
           être intéressant aux yeux des autres.\"

   36-45   \"Atteindre mon objectif est la seule chose qui m'intéresse,
           et j'ignorerai tout le reste pour le poursuivre.\"

   46-50   \"J'ai du mal à me soucier de ce qui se passe autour de moi.\"

   51-55   \"Je n'aime pas la façon dont les gens me jugent tout le
           temps.\"

   56-70   \"Je suis la personne la plus intelligente, la plus sage, la
           plus forte, la plus rapide et la plus belle que je connaisse.\"

   71-80   \"Je suis convaincu que de puissants ennemis me traquent, et
           que leurs agents sont partout où je vais. Je suis sûr qu'ils
           me surveillent en permanence.\"

   81-85   \"Il n'y a qu'une seule personne en qui je peux avoir
           confiance. Et je suis le seul à pouvoir voir cet ami spécial.\"

   86-95   \"Je ne peux rien prendre au sérieux. Plus la situation est
           sérieuse, plus je la trouve drôle.\"

   96-100  \"J'ai découvert que j'aime vraiment tuer des gens.\"
  ------------------------------------------------------------------------

  : La folie indéfinie



### Guérir la folie

Un sort de *calme des émotions* peut supprimer les
effets de la folie, tandis qu'un sort de *restauration
partielle* peut débarrasser un personnage d'une
folie à court ou à long terme. En fonction de la source de la folie, les
sorts Délivrance des *malédictions* ou *Dissipation du
mal* peuvent également s'avérer efficaces. Un
sort de *restauration supérieure* ou une magie
plus puissante est nécessaire pour débarrasser un personnage d'une
folie de durée indéterminée.




## Pièges

Les pièges peuvent être trouvés presque partout. Un faux pas dans une
tombe ancienne peut déclencher une série de lames de faucille qui
transpercent les armures et les os. Les vignes apparemment inoffensives
qui pendent à l'entrée d'une grotte peuvent saisir et étouffer
quiconque les traverse. Un filet caché parmi les arbres peut tomber sur
les voyageurs qui passent en dessous. Dans un jeu fantastique, les
aventuriers imprudents peuvent faire une chute mortelle, être brûlés
vifs ou tomber sous une fusillade de fléchettes empoisonnées.

Un piège peut être de nature mécanique ou magique. Les **pièges
mécaniques** comprennent les fosses, les pièges à flèches, les blocs qui
tombent, les pièces remplies d'eau, les lames tournoyantes et tout ce
qui dépend d'un mécanisme pour fonctionner. Les **pièges magiques**
sont soit des pièges à dispositifs magiques, soit des pièges à sorts.
Les pièges magiques déclenchent les effets des sorts lorsqu'ils sont
activés. Les pièges à sortilèges sont des sorts tels que
*Glyphe* qui
fonctionnent comme des pièges.


### Les pièges en jeu

Lorsque les aventuriers rencontrent un piège, vous devez savoir comment
le piège se déclenche et ce qu'il fait, ainsi que la possibilité pour
les personnages de détecter le piège et de le désactiver ou de
l'éviter.


#### Déclencher un piège

La plupart des pièges sont déclenchés lorsqu'une créature va quelque
part ou touche quelque chose que le créateur du piège a voulu protéger.
Les déclencheurs communs comprennent le fait de marcher sur une plaque
de pression ou une fausse section de plancher, de tirer un fil de
déclenchement, de tourner une poignée de porte et d'utiliser la
mauvaise clé dans une serrure. Les pièges magiques sont souvent réglés
pour se déclencher lorsqu'une créature pénètre dans une zone ou touche
un objet. Certains pièges magiques (comme le sort *Glyphe de
protection* ont des conditions de déclenchement
plus compliquées, notamment un mot de passe qui empêche le piège de
s'activer.



#### Détection et désactivation d'un piège

En général, certains éléments d'un piège sont visibles lors d'une
inspection minutieuse. Les personnages peuvent remarquer une dalle
irrégulière qui cache une plaque de pression, repérer la lueur d'un
fil-piège, remarquer les petits trous dans les murs d'où jailliront des
flammes, ou détecter quelque chose qui indique la présence d'un piège.

La description d'un piège précise les tests et les DC nécessaires pour
le détecter, le désactiver, ou les deux. Un personnage qui cherche
activement un piège peut tenter un test de Sagesse (Perception) contre
le DC du piège. Vous pouvez également comparer le DC de détection du
piège avec le score passif de Sagesse (Perception) de chaque personnage
pour déterminer si un membre du groupe remarque le piège au passage. Si
les aventuriers détectent un piège avant de le déclencher, ils peuvent
être en mesure de le désarmer, soit de façon permanente, soit
suffisamment longtemps pour le contourner. Vous pouvez demander un test
d'Intelligence (Investigation) à un personnage pour déduire ce qui doit
être fait, suivi d'un test de Dextérité en utilisant des outils de
voleur pour effectuer le sabotage nécessaire.

Tout personnage peut tenter un test d'Intelligence (Arcane) pour
détecter ou désarmer un piège magique, en plus de tout autre test
indiqué dans la description du piège. Les DC sont les mêmes, quel que
soit le test utilisé. De plus, *Dissipation de la
magie* a une chance de désactiver la plupart des pièges
magiques. La description d'un piège magique fournit le DC pour le test
de capacité effectué lorsque vous utilisez *Dissipation de la
magie*.

Dans la plupart des cas, la description d'un piège est suffisamment
claire pour que vous puissiez déterminer si les actions d'un personnage
permettent de localiser ou de déjouer le piège. Comme dans de nombreuses
situations, vous ne devez pas laisser le jet de dé prendre le pas sur un
jeu intelligent et une bonne planification. Faites appel à votre sens
commun, en vous appuyant sur la description du piège pour déterminer ce
qui se passe. Aucun piège ne peut anticiper toutes les actions possibles
que les personnages pourraient tenter.

Vous devriez permettre à un personnage de découvrir un piège sans faire
de test de capacité si une action révélerait clairement la présence du
piège. Par exemple, si un personnage soulève un tapis qui dissimule une
plaque de pression, le personnage a trouvé le déclencheur et aucun test
n'est nécessaire.

Déjouer les pièges peut être un peu plus compliqué. Prenons l'exemple
d'un coffre au trésor piégé. Si le coffre est ouvert sans avoir tiré
sur les deux poignées fixées sur les côtés, un mécanisme à l'intérieur
envoie une grêle d'aiguilles empoisonnées vers quiconque se trouve
devant lui. Après avoir inspecté le coffre et effectué quelques
vérifications, les personnages ne sont toujours pas sûrs qu'il soit
piégé. Plutôt que d'ouvrir simplement le coffre, ils placent un
bouclier devant et poussent le coffre à distance avec une barre de fer.
Dans ce cas, le piège se déclenche toujours, mais la grêle d'aiguilles
tire inoffensivement sur le bouclier.

Les pièges sont souvent conçus avec des mécanismes qui leur permettent
d'être désarmés ou contournés. Les monstres intelligents qui placent
des pièges dans ou autour de leurs repaires ont besoin de moyens pour
passer ces pièges sans se blesser. Ces pièges peuvent avoir des leviers
cachés qui désactivent leurs déclencheurs, ou une porte secrète peut
cacher un passage qui contourne le piège.



#### Effets de piège

Les effets des pièges peuvent aller du désagrément à la mort, en
utilisant des éléments tels que des flèches, des pointes, des lames, du
poison, des gaz toxiques, des explosions de feu et des fosses profondes.
Les pièges les plus mortels combinent plusieurs éléments pour tuer,
blesser, contenir ou chasser toute créature assez malheureuse pour les
déclencher. La description d'un piège précise ce qui se passe
lorsqu'il est déclenché.

Le bonus d'attaque d'un piège, les points de sauvegarde pour résister
à ses effets et les dégâts qu'il inflige peuvent varier en fonction de
la gravité du piège. Utilisez le tableau des points de sauvegarde et des
bonus d'attaque des pièges et le tableau de la gravité des dégâts par
niveau pour obtenir des suggestions basées sur trois niveaux de gravité
des pièges.

Un piège destiné à être un **revers** a peu de chances de tuer ou de
blesser sérieusement les personnages des niveaux indiqués, alors qu'un
piège **dangereux** est susceptible de blesser sérieusement (et
potentiellement de tuer) les personnages des niveaux indiqués. Un piège
**mortel** est susceptible de tuer les personnages des niveaux indiqués.

  ------------------------------------
  Piège Danger  Save DC      Bonus
                          d'attaque
  ------------ --------- -------------
  Marge de       10-11      +3 à +5
  recul                  

  Dangerous      12-15      +6 à +8

  Deadly         16-20     +9 à +12
  ------------------------------------

  : Les DC de sauvegarde et les bonus d'attaque des pièges

  ------------------------------------------------
      Niveau de     Marge de   Dangerous   Deadly
      caractère       recul               
  ----------------- --------- ----------- --------
      1er-4ème        1d10       4d10       4d10

     5ème-10ème       2d10       10d10     10d10

     11ème-16ème      2d10       10d10     18d10

     17ème-20ème      4d10       18d10     24d10
  ------------------------------------------------

  : Gravité des dommages par niveau



#### Pièges complexes

Les pièges complexes fonctionnent comme les pièges standard, sauf
qu'une fois activés, ils exécutent une série d'actions à chaque round.
Un piège complexe transforme le processus de traitement d'un piège en
quelque chose qui ressemble plus à une rencontre de combat.

Lorsqu'un piège complexe s'active, il effectue un jet d'initiative.
La description du piège inclut un bonus d'initiative. A son tour, le
piège s'active à nouveau, souvent en effectuant une action. Il peut
effectuer des attaques successives contre des intrus, créer un effet qui
change avec le temps, ou produire un défi dynamique. Sinon, le piège
complexe peut être détecté et désactivé ou contourné de la manière
habituelle.

Par exemple, un piège qui provoque une lente inondation d'une pièce
fonctionne mieux comme un piège complexe. Au tour du piège, le niveau de
l'eau augmente. Après plusieurs tours, la pièce est complètement
inondée.




### Pièges à échantillons

Les pièges magiques et mécaniques présentés ici varient en termes de
dangerosité et sont présentés par ordre alphabétique.


#### Toit qui s'effondre

*Piège mécanique*

Ce piège utilise un fil déclencheur pour faire s'effondrer les supports
qui maintiennent en place une section instable d'un plafond.

Le fil déclencheur se trouve à 10 cm du sol et s'étend entre deux
poutres de soutien. Le DC pour repérer le fil-piège est de 10. Un test
de Dextérité DC 15 réussi à l'aide d'outils de voleurs désactive le
fil-piège sans danger. Un personnage sans outils de voleur peut tenter
ce test avec un désavantage en utilisant n'importe quelle arme
tranchante ou outil tranchant. En cas d'échec, le piège se déclenche.

Toute personne qui inspecte les poutres peut facilement déterminer
qu'elles sont simplement calées en place. En tant qu'action, un
personnage peut débloquer une poutre, provoquant le déclenchement du
piège.

Le plafond au-dessus du fil-piège est en mauvais état, et quiconque peut
le voir peut dire qu'il risque de s'effondrer.

Lorsque le piège est déclenché, le plafond instable s'effondre. Toute
créature se trouvant dans la zone située sous la section instable doit
réussir un jet de sauvegarde de Dextérité DC 15, subissant 22 (4d10)
dégâts de matraquage en cas d'échec, ou la moitié des dégâts en cas de
réussite. Une fois le piège déclenché, le sol de la zone est rempli de
gravats et devient un terrain difficile.



#### Filet tombant

*Piège mécanique*

Ce piège utilise un fil de déclenchement pour libérer un filet suspendu
au plafond.

Le fil de détente est à 10 cm du sol et s'étend entre deux colonnes ou
arbres. Le filet est caché par des toiles d'araignée ou du feuillage.
Le DC pour repérer le fil-piège et le filet est de 10. Un test de
Dextérité réussi (DC 15) à l'aide d'outils de voleurs permet de briser
le fil-piège sans danger. Un personnage sans outils de voleur peut
tenter ce test avec un désavantage en utilisant n'importe quelle arme
tranchante ou outil tranchant. En cas d'échec, le piège se déclenche.

Lorsque le piège est déclenché, le filet est libéré, couvrant une zone
de 3 mètres carrés. Les personnes se trouvant dans la zone sont piégées
sous le filet et entravées, et celles qui échouent à un jet de
sauvegarde de Force DC 10 sont également projetées en position couchée.
Une créature peut utiliser son action pour faire un test de Force DC 10,
se libérant ou libérant une autre créature à sa portée en cas de succès.
Le filet a une CA de 10 et 20 points de vie. Si vous infligez 5 points
de dégâts tranchants au filet (AC 10), vous en détruisez une section de
5 pieds carrés, libérant ainsi toute créature piégée dans cette section.



#### Statue crachant du feu

*Piège magique*

Ce piège est activé lorsqu'un intrus marche sur une plaque de pression
cachée, libérant une goutte de flamme magique d'une statue proche. La
statue peut représenter n'importe quoi, y compris un dragon ou un
magicien lançant un sort.

Le DC est de 15 pour repérer la plaque de pression, ainsi que de légères
marques de brûlure sur le sol et les murs. Un sort ou un autre effet
permettant de détecter la présence de magie, tel que *détecter la
magie*, révèle une aura de magie d'évocation autour de
la statue.

Le piège s'active lorsque plus de 10 kg sont placés sur la plaque de
pression, ce qui fait que la statue libère un cône de feu de 10 m de
haut. Chaque créature dans le feu doit effectuer un jet de sauvegarde de
Dextérité DC 13, subissant 22 (4d10) dégâts de feu en cas d'échec, ou
la moitié des dégâts en cas de réussite.

Coincer une pointe de fer ou un autre objet sous le plateau de pression
empêche le piège de s'activer. Un jet réussi de *Dissipation de la
magie* sur la statue détruit le piège.



#### Fosses

*Piège mécanique*

Quatre pièges à fosse de base sont présentés ici.

***Simple Pit.*** Un piège à fosse simple est un trou creusé dans le
sol. Le trou est recouvert d'un grand tissu ancré sur le bord de la
fosse et camouflé par de la terre et des débris.

Le DC pour repérer la fosse est de 10. Toute personne marchant sur le
tissu tombe au travers et entraîne le tissu dans la fosse, subissant des
dégâts en fonction de la profondeur de la fosse (généralement 3 mètres,
mais certaines fosses sont plus profondes).

***Fosse cachée.*** Cette fosse a un abri construit à partir d'un
matériau identique à celui du sol qui l'entoure.

Un test de Sagesse (Perception) DC 15 réussi permet de discerner une
absence de circulation piétonne sur la section du sol qui constitue le
couvercle de la fosse. Un test d'Intelligence (Investigation) DC 15
réussi est nécessaire pour confirmer que la section de sol piégée est en
fait l'abri d'une fosse.

Lorsqu'une créature marche sur l'abri, il s'ouvre comme une trappe et
l'intrus se jette dans la fosse en dessous. La fosse a généralement une
profondeur de 10 ou 20 pieds, mais elle peut être plus profonde.

Une fois le piège détecté, une pointe de fer ou un objet similaire peut
être coincé entre le couvercle de la fosse et le sol environnant de
manière à empêcher le couvercle de s'ouvrir, ce qui permet de traverser
en toute sécurité. Le couvercle peut également être maintenu fermé par
magie à l'aide du sort *Verrou occ* ulte ou d'un sort
similaire.

***Fosse verrouillée.*** Ce piège est identique à un piège à fosse
cachée, à une exception près : la trappe qui recouvre la fosse est
montée sur ressort. Lorsqu'une créature tombe dans la fosse, le
couvercle se referme pour emprisonner sa victime à l'intérieur.

Il faut réussir un test de Force DC 20 pour faire levier et ouvrir
l'abri. L'abri peut également être ouvert en le fracassant. Un
personnage dans la fosse peut également tenter de désactiver le
mécanisme à ressort de l'intérieur avec un test de Dextérité DC 15 en
utilisant des outils de voleur, à condition que le mécanisme puisse être
atteint et que le personnage puisse voir. Dans certains cas, un
mécanisme (généralement caché derrière une porte secrète à proximité)
permet d'ouvrir la fosse.

***Fosse à pointes.*** Ce piège à fosse est un piège simple, caché ou
verrouillé, avec des pointes de bois ou de fer aiguisées au fond. Une
créature tombant dans la fosse subit 11 (2d10) dégâts perforants dus aux
pointes, en plus des dégâts de chute. Les versions encore plus méchantes
ont du poison étalé sur les pointes. Dans ce cas, toute personne
subissant des dégâts perforants dus aux pointes doit également effectuer
un jet de sauvegarde de Constitution DC 13, subissant 22 (4d10) dégâts
de poison en cas d'échec, ou la moitié des dégâts en cas de réussite.



#### Fléchettes empoisonnées

*Piège mécanique*

Lorsqu'une créature marche sur une plaque de pression cachée, des
fléchettes empoisonnées jaillissent de tubes à ressort ou pressurisés
habilement encastrés dans les murs environnants. Une zone peut
comprendre plusieurs plaques de pression, chacune étant équipée de son
propre jeu de fléchettes.

Les très petits trous dans les murs sont cachés par la poussière et les
toiles d'araignée, ou habilement dissimulés parmi les bas-reliefs, les
peintures murales ou les fresques qui ornent les murs. Le DC pour les
repérer est de 15. Avec un test d'Intelligence (Investigation) DC 15
réussi, un personnage peut déduire la présence du harnois à partir des
variations du mortier et de la pierre utilisés pour le créer, par
rapport au sol environnant. Coincer une pointe de fer ou un autre objet
sous la plaque de pression empêche le piège de s'activer. Remplir les
trous de tissu ou de cire empêche les fléchettes qu'ils contiennent de
se lancer.

Le piège s'active lorsqu'un poids de plus de 10 kg est placé sur la
plaque de pression, libérant quatre fléchettes. Chaque fléchette
effectue une attaque à distance avec un bonus de +8 contre une cible
aléatoire située à moins de 3 mètres de la plaque de pression (la vision
n'est pas prise en compte pour ce jet d'attaque). (Si aucune cible ne
se trouve dans la zone, les fléchettes ne touchent rien.) Une cible
touchée subit 2 (1d4) dégâts perforants et doit réussir un jet de
sauvegarde de Constitution DC 15, subissant 11 (2d10) dégâts de poison
en cas d'échec, ou la moitié des dégâts en cas de réussite.



#### Aiguille empoisonnée

*Piège mécanique*

Une aiguille empoisonnée est cachée dans la serrure d'un coffre à
trésor, ou dans un autre objet qu'une créature pourrait ouvrir. Si
l'on ouvre le coffre sans la bonne clé, l'aiguille sort, délivrant une
dose de poison.

Lorsque le piège est déclenché, l'aiguille sort de la serrure de 5 cm.
Une créature à portée subit 1 dégât perforant et 11 (2d10) dégâts de
poison, et doit réussir un jet de sauvegarde de Constitution DC 15 ou
être empoisonnée pendant 1 heure.

Un test d'Intelligence (Investigation) DC 20 réussi permet au
personnage de déduire la présence du piège à partir des modifications
apportées à la serrure pour accueillir l'aiguille. Un test de Dextérité
DC 15 réussi en utilisant des outils de voleurs désarme le piège, en
retirant l'aiguille de la serrure. Une tentative infructueuse de
crochetage de la serrure déclenche le piège.



#### Sphère roulante

*Piège mécanique*

Lorsqu'une pression de 10 kg ou plus est exercée sur la plaque de
pression de ce piège, une trappe cachée dans le plafond s'ouvre,
libérant une sphère de pierre solide de 3 m de diamètre.

En réussissant un test de Sagesse (Perception) DC 15, un personnage peut
repérer la trappe et la plaque de pression. Une fouille du sol
accompagnée d'un test d'Intelligence (Investigation) DC 15 réussi
révèle des variations dans le mortier et la pierre qui trahissent la
présence de la plaque de pression. Le même test effectué lors de
l'inspection du plafond note des variations dans la maçonnerie qui
révèlent la trappe. Coincer une pointe de fer ou un autre objet sous la
plaque de pression empêche le piège de s'activer.

L'activation de la sphère nécessite un jet d'initiative de la part de
toutes les créatures présentes. La sphère effectue un jet d'initiative
avec un bonus de +8. À son tour, elle se déplace de 60 pieds en ligne
droite. La sphère peut se déplacer à travers les espaces des créatures,
et les créatures peuvent se déplacer à travers son espace, en le
traitant comme un terrain difficile. Chaque fois que la sphère entre
dans l'espace d'une créature ou qu'une créature entre dans son espace
pendant qu'elle roule, cette créature doit réussir un jet de sauvegarde
de Dextérité DC 15 ou subir 55 (10d10) dégâts de contondant et être mise
à terre.

La sphère s'arrête lorsqu'elle heurte un mur ou une barrière
similaire. Elle ne peut pas contourner les coins, mais les constructeurs
de donjons intelligents intègrent des virages en douceur dans les
passages proches, ce qui permet à la sphère de continuer à se déplacer.

Comme une action, une créature dans un rayon de 1,5 mètre de la sphère
peut tenter de la ralentir avec un test de Force DC 20. Si le test est
réussi, la vitesse de la sphère est réduite de 15 pieds. Si la vitesse
de la sphère tombe à 0, elle cesse de se déplacer et n'est plus une
menace.



#### Sphère d'annihilation

*Piège magique*

Des ténèbres magiques et impénétrables remplissent la bouche béante
d'un visage de pierre taillé dans un mur. La bouche fait 2 pieds de
diamètre et est grossièrement circulaire. Aucun son n'en sort, aucune
lumière ne peut en illuminer l'intérieur et toute matière qui y pénètre
est instantanément oblitérée.

Un test d'Intelligence (Mystères) réussi à DC 20 révèle que la bouche
contient une sphère *d'annihilation*
qui ne peut être ni contrôlée ni déplacée. Elle est par ailleurs
identique à une sphère *d'annihilation* normale.

Certaines versions du piège comprennent un enchantement placé sur la
face de la pierre, de sorte que les créatures spécifiées ressentent une
envie irrésistible de s'en approcher et de ramper dans sa bouche. Cet
effet est semblable à l'aspect *sympathie* du sort
*Aversion/attirance*. Une réussite de
*Dissipation de la magie* supprime cet
enchantement.


