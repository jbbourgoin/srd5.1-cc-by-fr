
# Dons

Un exploit représente un talent ou un domaine d'expertise qui donne au
personnage des capacités spéciales. Il incarne l'entraînement,
l'expérience et les capacités au-delà de ce que fournit une classe.

À certains niveaux, votre classe vous offre la caractéristique
Amélioration de caractéristiques. En utilisant la règle des exploits
optionnels, vous pouvez renoncer à cette caractéristique pour prendre un
exploit de votre choix à la place. Vous ne pouvez prendre chaque exploit
qu'une seule fois, sauf si la description de l'exploit dit le
contraire.

Vous devez satisfaire à tous les prérequis spécifiés dans un exploit
pour pouvoir l'utiliser. Si vous perdez le prérequis d'un exploit,
vous ne pouvez pas l'utiliser tant que vous ne l'avez pas retrouvé.
Par exemple, l'exploit du Lutteur requiert une Force de 13 ou plus. Si
votre Force est réduite à moins de 13, peut-être par une malédiction,
vous ne pouvez pas bénéficier de l'exploit du Lutteur jusqu'à ce que
votre Force soit restaurée.


## Lutteur

*Prérequis : Force 13 ou plus*

Vous avez développé les compétences nécessaires pour vous défendre lors
d'un combat rapproché au grappin. Vous gagnez les avantages suivants :

-   Vous avez un avantage sur les jets d'attaque contre une créature
    que vous agrippez.
-   Vous pouvez utiliser votre action pour essayer d'immobiliser une
    créature agrippée par vous. Pour ce faire, faites un autre test de
    grappin. Si vous réussissez, vous et la créature êtes tous deux
    immobilisés jusqu'à la fin du grappin.



## Inspiration

L'inspiration est une règle que le maître de jeu peut utiliser pour
vous récompenser d'avoir joué votre personnage d'une manière qui
correspond à ses traits de personnalité, son idéal, son lien et son
défaut. En utilisant l'inspiration, vous pouvez faire appel à votre
trait de personnalité de compassion pour les opprimés pour vous donner
un avantage dans la négociation avec le prince mendiant. L'inspiration
peut aussi vous permettre de faire appel à votre lien avec la défense de
votre village natal pour contrer l'effet d'un sort qui vous a été
jeté.


### Trouver l'inspiration

Votre MJ peut choisir de vous donner de l'inspiration pour diverses
raisons. En général, les MJ l'accordent lorsque vous jouez les traits
de votre personnalité, que vous cédez aux inconvénients d'un défaut ou
d'un lien, et que vous dépeignez votre personnage de manière
convaincante. Votre MJ vous dira comment vous pouvez gagner de
l'inspiration dans le jeu.

Soit vous avez de l'inspiration, soit vous n'en avez pas. Vous ne
pouvez pas stocker de multiples \"inspirations\" pour les utiliser plus
tard.



### Utiliser l'inspiration

Si vous avez de l'inspiration, vous pouvez la dépenser lorsque vous
faites un jet d'attaque, un jet de sauvegarde ou un test de capacité.
Dépenser votre inspiration vous donne un avantage sur ce jet.

De plus, si vous avez de l'inspiration, vous pouvez récompenser un
autre joueur pour un bon jeu de rôle, une réflexion intelligente ou
simplement pour avoir fait quelque chose de passionnant dans le jeu.
Lorsque le personnage d'un autre joueur fait quelque chose qui
contribue réellement à l'histoire d'une manière amusante et
intéressante, vous pouvez abandonner votre inspiration pour donner de
l'inspiration à ce personnage.


