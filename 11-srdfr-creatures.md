# Monstres & PNJ


Les statistiques d'un monstre, parfois appelées **bloc de
statistiques**, fournissent les informations essentielles dont vous avez
besoin pour gérer le monstre.


## Construction de monstres


### Taille

Un monstre peut être Petit, Petit, Moyen, Grand, Énorme ou
Gargantuesque. Le tableau des catégories de taille indique l'espace
qu'une créature d'une taille particulière contrôle en combat. Voir
\"Mouvement et Position\" pour plus
d'informations sur la taille et l'espace des créatures.

  -----------------------------------------------------------
  Taille          Espace                  Exemples
  --------------- ----------------------- -------------------
  Très petite     2-1/2 par 2-1/2 pieds.  Diablotin, esprit
  (TP)                                    follet

  Petit           5 par 5 pieds           Rat géant, gobelin

  Moyen           5 par 5 pieds           Orc, Loup-garou

  Grand           10 par 10 pieds.        Hippogriffe, ogre

  Très grand      15 par 15 pieds.        Géant du feu,
                                          Sylvanide

  Gargantuesque   20 par 20 pieds ou plus Kraken, Ver pourpre
  -----------------------------------------------------------

  : Catégories de taille

> #### Modifier les créatures {#modifying-creatures}
>
> Vous pouvez être désemparé lorsqu'il s'agit de trouver la créature
> parfaite pour une partie d'une aventure. N'hésitez pas à modifier
> une créature existante pour en faire quelque chose de plus utile pour
> vous, peut-être en empruntant un trait ou deux à un monstre différent
> ou en utilisant une variante ou un modèle, comme ceux de ce livre.
> N'oubliez pas que la modification d'un monstre, y compris lorsque
> vous lui appliquez un gabarit, peut changer sa valeur de défi.



### Type

Le type d'un monstre indique sa nature fondamentale. Certains sorts,
objets magiques, caractéristiques de classe et autres effets du jeu
interagissent de manière spéciale avec les créatures d'un type
particulier. Par exemple, une *flèche de la Flèche* ravageuse inflige
des dégâts supplémentaires non seulement aux dragons, mais aussi aux
autres créatures du type dragon, comme les tortues-dragons et les
wivernes.

Le jeu comprend les types de monstres suivants, qui n'ont pas de règles
propres.

Les**aberrations** sont des êtres totalement étrangers. Beaucoup
d'entre elles possèdent des capacités magiques innées, issues de
l'esprit extraterrestre de la créature plutôt que des forces mystiques
du monde. La quintessence des aberrations est l'Aboleth.

Les**bêtes** sont des créatures non humanoïdes qui font partie
intégrante de l'écologie de la fantasy. Certaines d'entre elles ont
des pouvoirs magiques, mais la plupart sont inintelligentes et ne
possèdent ni société ni langage. Les bêtes comprennent toutes les
variétés d'animaux ordinaires, les
dinosaures et les versions géantes des animaux.

Les**céleste** sont des créatures originaires des plans supérieurs.
Beaucoup d'entre eux sont les serviteurs des divinités, employés comme
messagers ou agents dans le royaume des mortels et à travers les plans.
Les céleste sont bons par nature, aussi l'exceptionnel céleste qui
s'écarte d'un bon alignement est une horrible rareté. Les célestes
comprennent les anges et les
pégases.

Les**constructions** sont faites, elles ne naissent pas. Certaines sont
programmées par leurs créateurs pour suivre un simple ensemble
d'instructions, tandis que d'autres sont dotées d'une sensibilité et
sont capables d'une pensée indépendante. Les golems sont des
constructions emblématiques. De nombreuses créatures originaires de
certains plans extérieurs sont des constructions façonnées à partir de
la matière première du plan par la volonté de créatures plus puissantes.

Les**dragons** sont de grandes créatures reptiliennes d'origine
ancienne et dotées d'une grande puissance. Les vrais dragons, y compris
les bons dragons métalliques et les méchants
dragons chromatiques, sont très intelligents et
possèdent une magie innée. On trouve également dans cette catégorie des
créatures apparentées de loin aux vrais dragons, mais moins puissantes,
moins intelligentes et moins magiques, comme les wivernes et
les pseudodragons.

Les**élémentaires** sont des créatures originaires des plans
élémentaires. Certaines créatures de ce type ne sont guère plus que des
masses animées de leurs éléments respectifs, y compris les créatures
simplement appelées élémentaires. D'autres ont des formes biologiques
imprégnées d'énergie élémentaire. Les origines de génies, y
compris les djinns, forment les
civilisations les plus importantes sur les plans élémentaires. Parmi les
autres créatures élémentaires, on trouve les azers et les
traqueurs invisibles.

Les**fées** sont des créatures magiques étroitement liées aux forces de
la nature. Ils vivent dans les bosquets crépusculaires et les forêts
brumeuses. Dans certains mondes, ils sont étroitement liés au plan des
fées. On en trouve également dans les plans extérieurs, en particulier
dans les plans orientés vers la nature. Les fées comprennent les
dryades.

Les**fiélons** sont des créatures maléfiques originaires des plans
inférieurs. Quelques-uns sont les serviteurs de divinités, mais beaucoup
plus travaillent sous la direction d'archidémons et de princes démons.
Les prêtres et les mages maléfiques convoquent parfois des fiélons dans
le monde matériel pour les mettre à contribution. Si un céleste
maléfique est une rareté, un bon fiélon est presque inconcevable. Les
fiélons comprennent les démons, les
chiens de l'enfer.

Les**géants** dominent les humains et leurs semblables. Ils ont une
forme semblable à celle des humains, bien que certains aient plusieurs
têtes(Ettins ou des malformations. Les six variétés de
véritables géants sont les géants des collines, les
géants de pierre,
les géants du feu
et les géants des tempêtes. En outre, des créatures
telles que les ogres sont des géants.

Les**humanoïdes** sont les principaux peuples d'un monde de jeu
fantastique, à la fois civilisés et sauvages, comprenant des humains et
une énorme variété d'autres espèces. Ils ont une langue et une culture,
peu ou pas de capacités magiques innées (bien que la plupart des
humanoïdes puissent apprendre l'incantation), et une forme bipède. Les
origines humanoïdes les plus communes sont celles qui conviennent le mieux
aux joueurs : humains,
elfes. Presque aussi
nombreuses mais beaucoup plus sauvages et brutales, et presque
uniformément maléfiques, sont les origines de gobelins(gobelins,
hobgobelins,
de gnolls.

Les**monstruosités** sont des monstres au sens strict - des créatures
effrayantes qui ne sont pas ordinaires, pas vraiment naturelles et
presque jamais bénignes. Certaines sont le résultat d'expériences
magiques qui ont mal tourné (comme les ours-hibous,
d'autres sont le produit de terribles malédictions (comme les
minotaures. Ils défient toute catégorisation et, dans un
certain sens, servent de catégorie fourre-tout pour les créatures qui ne
correspondent à aucun autre type.

Les**oozes** sont des créatures gélatineuses qui ont rarement une forme
fixe. Ils sont principalement souterrains, vivant dans des grottes et
des donjons et se nourrissant de déchets, de charognes ou de créatures
assez malchanceuses pour se trouver sur leur chemin. Les poudings
noirs sont
parmi les suintements les plus reconnaissables.

Dans ce contexte, les**plantes** sont des créatures végétales, et non
des plantes ordinaires. La plupart d'entre elles sont ambulatoires, et
certaines sont carnivores. La quintessence des plantes est le Tertre
errant et le Sylvanide. Les créatures
fongiques et le Moisissure
violette, entrent également dans cette catégorie.

Les**morts-vivants** sont des créatures autrefois vivantes, amenées à un
état de mort horrible par la pratique de la magie nécromantique ou par
une malédiction impie. Les morts-vivants comprennent les cadavres
ambulants, comme les vampires,
ainsi que les esprits sans corps, comme les fantômes et les
spectres.


#### Tags

Un monstre peut avoir une ou plusieurs étiquettes ajoutées à son type,
entre parenthèses. Par exemple, un orc a le type humanoïde (orc). Les
étiquettes entre parenthèses fournissent une catégorisation
supplémentaire pour certaines créatures. Les étiquettes n'ont pas de
règles propres, mais quelque chose dans le jeu, comme un objet magique,
peut y faire référence. Par exemple, une lance qui est particulièrement
efficace pour combattre les démons fonctionnera contre tout monstre
ayant l'étiquette démon.



#### Alignement

L'alignement d'un monstre donne un indice sur sa disposition et son
comportement dans une situation de jeu de rôle ou de combat. Par
exemple, un monstre maléfique chaotique peut être difficile à raisonner
et attaquer les personnages à vue, alors qu'un monstre neutre peut être
disposé à négocier. Voir \"Alignement\" pour la
description des différents alignements.

L'alignement spécifié dans le bloc de statistiques d'un monstre est la
valeur par défaut. N'hésitez pas à vous en écarter et à modifier
l'alignement d'un monstre pour l'adapter aux besoins de votre
campagne. Si vous voulez un dragon vert d'alignement bon ou un géant
des tempêtes maléfique, rien ne vous en empêche.

Certaines créatures peuvent avoir **n'importe quel alignement**. En
d'autres termes, vous choisissez l'alignement du monstre.
L'alignement de certains monstres indique une tendance ou une aversion
pour la loi, le chaos, le bien ou le mal. Par exemple, un
berserker peut avoir n'importe quel alignement chaotique
(chaotique bon, chaotique neutre ou chaotique mauvais), comme le veut sa
nature sauvage.

De nombreuses créatures de faible intelligence ne comprennent pas la loi
ou le chaos, le bien ou le mal. Elles ne font pas de choix moraux ou
éthiques, mais agissent plutôt par instinct. Ces créatures sont **sans
alignement**, ce qui signifie qu'elles n'ont pas d'alignement.




### Classe d'armure

Un monstre qui porte une armure ou un bouclier a une Classe d'Armure
(CA) qui prend en compte son armure, son bouclier et sa Dextérité.
Sinon, la CA d'un monstre est basée sur son modificateur de Dextérité
et son armure naturelle, le cas échéant. Si un monstre possède une
armure naturelle, porte une armure ou un bouclier, cela est noté entre
parenthèses après sa valeur de CA.



### Points de vie

Un monstre meurt généralement ou est détruit lorsqu'il tombe à 0 points
de vie. Pour en savoir plus sur les points de vie, voir \"Dégâts et
Guérison\".

Les points de vie d'un monstre sont présentés à la fois sous forme
d'expression de dé et de nombre moyen. Par exemple, un monstre ayant
2d8 points de vie a 9 points de vie en moyenne (2 × 4 1/2).

La taille d'un monstre détermine le dé utilisé pour calculer ses points
de vie, comme indiqué dans la table Détermination des points de vie par
taille.

  ---------------------------------------------
  Taille du        Hit Die    HP moyen par dé
  monstre                   
  --------------- --------- -------------------
  Très petite        d4            2 1/2
  (TP)                      

  Petit              d6            3 1/2

  Moyen              d8            4 1/2

  Grand              d10           5 1/2

  Très grand         d12           6 1/2

  Gargantuesque      d20          10 1/2
  ---------------------------------------------

  : Hit Dice par taille

Le modificateur de Constitution d'un monstre affecte également le
nombre de points de vie qu'il possède. Son modificateur de Constitution
est multiplié par le nombre de dés de points de vie qu'il possède, et
le résultat est ajouté à ses points de vie. Par exemple, si un monstre a
une Constitution de 12 (modificateur de +1) et 2d8 Dés de Vie, il a
2d8 + 2 points de vie (moyenne de 11).



### Vitesse

La vitesse d'un monstre vous indique la distance qu'il peut parcourir
pendant son tour. Pour plus d'informations sur la vitesse, voir
\"Mouvement et position\".

Toutes les créatures ont une vitesse de marche, appelée simplement
vitesse du monstre. Les créatures qui n'ont aucune forme de locomotion
terrestre ont une vitesse de marche de 0 pied.

Certaines créatures possèdent un ou plusieurs des modes de déplacement
supplémentaires suivants.


#### Brouillon

Un monstre qui a une vitesse de creusement peut utiliser cette vitesse
pour se déplacer dans le sable, la terre, la boue ou la glace. Un
monstre ne peut pas creuser à travers de la roche solide, sauf s'il
possède un trait spécial qui lui permet de le faire.



#### Escalade

Un monstre qui a une vitesse d'escalade peut utiliser tout ou partie de
son mouvement pour se déplacer sur des surfaces verticales. Le monstre
n'a pas besoin de dépenser un mouvement supplémentaire pour grimper.



#### Vol

Un monstre qui a une vitesse de vol peut utiliser tout ou partie de son
mouvement pour voler. Certains monstres ont la capacité de planer, ce
qui les rend difficiles à débloquer. Un
tel monstre cesse de planer lorsqu'il meurt.



#### Swim

Un monstre qui a une vitesse de nage n'a pas besoin de dépenser un
mouvement supplémentaire pour nager.




### Valeurs de caractéristiques

Chaque monstre possède six valeurs de caractéristiques (Force,
Dextérité, Constitution, Intelligence, Sagesse et Charisme) et les
modificateurs correspondants. Pour plus d'informations sur les valeurs
de caractéristiques et leur utilisation en jeu, voir \"Utiliser les
valeurs de caractéristiques\".



### Jets de sauvegarde

L'entrée Jets de sauvegarde est réservée aux créatures qui sont
capables de résister à certains types d'effets.

Par exemple, une créature qui n'est pas facilement charmée ou effrayée
peut obtenir un bonus sur ses jets de sauvegarde de Sagesse. La plupart
des créatures n'ont pas de bonus spécial aux jets de sauvegarde, auquel
cas cette section est absente.

Le bonus aux jets de sauvegarde est la somme du modificateur d'aptitude
pertinent du monstre et de son bonus de maîtrise, qui est déterminé par
la valeur de défi du monstre (comme indiqué dans le tableau Bonus de
maîtrise par valeur de défi).

  ----------------------------
     Défi         Bonus de
                  maîtrise
               supplémentaire
  ----------- ----------------
       0             +2

      1/8            +2

      1/4            +2

      1/2            +2

       1             +2

       2             +2

       3             +2

       4             +2

       5             +3

       6             +3

       7             +3

       8             +3

       9             +4

      10             +4

      11             +4

      12             +4

      13             +5

      14             +5

      15             +5

      16             +5

      17             +6

      18             +6

      19             +6

      20             +6

      21             +7

      22             +7

      23             +7

      24             +7

      25             +8

      26             +8

      27             +8

      28             +8

      29             +9

      30             +9
  ----------------------------

  : Bonus de maîtrise en fonction de la valeur du défi.



### Compétences

L'entrée Maîtrise est réservée aux monstres qui sont compétents dans
une ou plusieurs compétences. Par exemple, un monstre très perspicace et
discret peut bénéficier de bonus aux tests de Sagesse (Perception) et de
Dextérité (Discrétion).

Un bonus de maîtrise est la somme du modificateur d'aptitude pertinent
d'un monstre et de son bonus de maîtrise, qui est déterminé par la
valeur de défi du monstre (comme indiqué dans le tableau Bonus de
maîtrise par valeur de défi). D'autres modificateurs peuvent
s'appliquer. Par exemple, un monstre peut avoir un bonus plus important
que prévu (généralement le double de son bonus de maîtrise) pour tenir
compte de son expertise accrue.

> #### Maîtrise des armures, des armes et des outils {#armor-weapon-and-tool-proficiencies}
>
> On suppose qu'une créature est compétente avec son armure, ses armes
> et ses outils. Si vous les échangez, vous décidez si la créature est
> compétente avec son nouvel équipement.
>
> Par exemple, un géant des collines porte généralement une armure de
> cuir et manie une massue. Vous pouvez équiper un géant des collines
> d'une cotte de mailles et d'une hache à la place, et supposer que le
> géant maîtrise les deux, l'un ou l'autre, ou aucun des deux.
>
> Voir les sections \"Armures\" et
> \"Armes\" pour les règles d'utilisation des
> armures ou des armes sans maîtrise.



### Vulnérabilités, résistances et immunités

Certaines créatures présentent une vulnérabilité, une résistance ou une
immunité à certains types de dégâts. Certaines créatures sont même
résistantes ou immunisées aux dégâts des attaques non magiques (une
attaque magique est une attaque délivrée par un sort, un objet magique
ou une autre source magique). En outre, certaines créatures sont
immunisées contre certains états.



### Les sens

L'entrée Sens note le score passif de Sagesse (Perception) d'un
monstre, ainsi que les sens spéciaux qu'il peut avoir. Les sens
spéciaux sont décrits ci-dessous.


#### Vision aveugle

Un monstre doté de la vision aveugle peut percevoir son environnement
sans se fier à la vue, dans un rayon spécifique.

Les créatures sans yeux, comme les torves et les vases gris, ont
généralement ce sens particulier, tout comme les créatures dotées
d'écholocalisation ou de sens aiguisés, comme les chauves-souris et les
vrais dragons.

Si un monstre est naturellement aveugle, il bénéficie d'une parenthèse
à cet effet, indiquant que le rayon de sa vision aveugle définit la
portée maximale de sa perception.



#### Vision dans le noir

Un monstre doté de vision dans le noir peut voir dans le noir dans un
rayon spécifique. Le monstre peut voir dans une lumière faible dans le
rayon comme s'il s'agissait d'une lumière vive, et dans l'obscurité
comme s'il s'agissait d'une lumière faible. Le monstre ne peut pas
discerner les couleurs dans l'obscurité, seulement les nuances de gris.
De nombreuses créatures qui vivent sous terre possèdent ce sens
particulier.



#### Perception des secousses

Un monstre doté de la Perception des secousses peut détecter et
localiser l'origine des vibrations dans un rayon spécifique, à
condition que le monstre et la source des vibrations soient en contact
avec le même sol ou la même substance. La Perception des secousses ne
peut pas être utilisée pour détecter les créatures volantes ou
incorporelles. De nombreuses créatures fouisseuses, comme les ankhegs,
possèdent ce sens spécial.



#### Vision véritable

Un monstre doté d'une vision véritable peut, jusqu'à une certaine
portée, voir dans les ténèbres normales et magiques, voir les créatures
et les objets invisibles, détecter automatiquement les illusions
visuelles et réussir ses jets de sauvegarde contre elles, et percevoir
la forme originale d'un métamorphe ou d'une créature transformée par
la magie. De plus, le monstre peut voir dans le plan éthéré dans la même
portée.




### Langues

Les langues qu'un monstre peut parler sont classées par ordre
alphabétique. Parfois, un monstre peut comprendre une langue mais ne
peut pas la parler, et cela est noté dans son entrée. Un \"-\" indique
qu'une créature ne parle ni ne comprend aucune langue.


#### Télépathie

La Télépathie est une capacité magique qui permet à un monstre de
communiquer mentalement avec une autre créature dans une portée donnée.
La créature contactée n'a pas besoin de partager une langue avec le
monstre pour communiquer de cette façon avec lui, mais elle doit être
capable de comprendre au moins une langue. Une créature sans télépathie
peut recevoir des messages télépathiques et y répondre, mais elle ne
peut ni lancer ni terminer une conversation télépathique.

Un monstre télépathe n'a pas besoin de voir la créature contactée et
peut mettre fin au contact télépathique à tout moment. Le contact est
rompu dès que les deux créatures ne sont plus à portée l'une de
l'autre ou si le monstre télépathe contacte une autre créature à
portée. Un monstre télépathe peut initier ou mettre fin à une
conversation télépathique sans utiliser d'action, mais tant que le
monstre est frappé d'incapacité, il ne peut pas initier de contact
télépathique, et tout contact en cours est interrompu.

Une créature se trouvant dans la zone d'un champ antimagie ou dans tout
autre endroit où la magie ne fonctionne pas ne peut ni envoyer ni
recevoir de messages télépathiques.




### Défi

L'indice de difficulté d'un monstre vous indique la gravité de la
menace qu'il représente. Un groupe de quatre aventuriers correctement
équipés et bien reposés devrait pouvoir vaincre un monstre dont le
niveau de difficulté est égal à son niveau sans subir de décès. Par
exemple, un groupe de quatre personnages de niveau 3 devrait trouver
qu'un monstre dont le niveau de difficulté est de 3 constitue un défi
intéressant, mais pas mortel.

Les monstres qui sont nettement plus faibles que les personnages de 1er
niveau ont un indice de difficulté inférieur à 1.

Les monstres ayant un indice de difficulté de 0 sont insignifiants, sauf
en grand nombre ; ceux qui n'ont pas d'attaque efficace ne valent
aucun point d'expérience, tandis que ceux qui ont des attaques valent
10 XP chacun.

Certains monstres représentent un défi plus grand que celui que peut
relever une équipe de niveau 20. Ces monstres ont un niveau de
difficulté de 21 ou plus et sont spécialement conçus pour tester les
compétences des joueurs.


#### Points d'expérience

Le nombre de points d'expérience (XP) que vaut un monstre est basé sur
sa valeur de défi. Généralement, l'XP est attribué pour avoir vaincu le
monstre, bien que le MJ puisse également attribuer de l'XP pour avoir
neutralisé la menace posée par le monstre d'une autre manière.

Sauf indication contraire, un monstre invoqué par un sort ou une autre
capacité magique vaut l'XP indiquée dans son bloc de statuts.

  ---------------------
     Défi        XP
  ----------- ---------
       0       0 ou 10

      1/8        25

      1/4        50

      1/2        100

       1         200

       2         450

       3         700

       4        1,100

       5        1,800

       6        2,300

       7        2,900

       8        3,900

       9        5,000

      10        5,900

      11        7,200

      12        8,400

      13       10,000

      14       11,500

      15       13,000

      16       15,000

      17       18,000

      18       20,000

      19       22,000

      20       25,000

      21       33,000

      22       41,000

      23       50,000

      24       62,000

      25       75,000

      26       90,000

      27       105,000

      28       120,000

      29       135,000

      30       155,000
  ---------------------

  : Points d'expérience par niveau de défi




### Traits particuliers

Les traits spéciaux (qui apparaissent après la valeur de défi du monstre
mais avant toute action ou réaction) sont des caractéristiques
susceptibles d'être pertinentes lors d'une rencontre de combat et qui
nécessitent quelques explications.


#### Lanceur de sorts inné

Un monstre ayant la capacité innée de lancer des sorts possède la
caractéristique spéciale Lanceur de sorts inné. Sauf indication
contraire, un sort inné de 1er niveau ou plus est toujours lancé à son
plus bas niveau possible et ne peut pas être lancé à un niveau
supérieur. Si un monstre possède un tour de magie où son niveau importe
et qu'aucun niveau n'est indiqué, utilisez la valeur de défi du
monstre.

Un sort inné peut avoir des règles ou des restrictions spéciales. Par
exemple, un mage drow peut lancer le sort de lévitation de façon innée,
mais le sort a une restriction \"auto seulement\", ce qui signifie que
le sort n'affecte que le mage drow.

Les sorts innés d'un monstre ne peuvent pas être échangés avec
d'autres sorts. Si les sorts innés d'un monstre ne nécessitent pas de
jet d'attaque, aucun bonus d'attaque n'est accordé pour eux.



#### Incantation

Un monstre doté du trait spécial Lancer de sorts possède un niveau de
lanceur de sorts et des emplacements de sorts, qu'il utilise pour
lancer ses sorts de 1er niveau et plus . Le niveau de lanceur de sorts est
également utilisé pour tous les cantrips inclus dans le trait.

Le monstre possède une liste de sorts connus ou préparés d'une classe
spécifique. La liste peut également inclure des sorts issus d'une
caractéristique de cette classe, comme la caractéristique Domaine divin
du clerc ou la caractéristique Cercle druidique du druide. Le monstre
est considéré comme un membre de cette classe lorsqu'il est lié à ou
utilise un objet magique qui requiert l'appartenance à la classe ou
l'accès à sa liste de sorts.

Un monstre peut lancer un sort de sa liste à un niveau supérieur s'il
dispose de l'emplacement de sort pour le faire. Par exemple, un Mage
drow possédant le sort Éclair de 3e niveau peut le lancer comme un sort
de 5e niveau en utilisant un de ses emplacements de sorts de 5e niveau.

Vous pouvez modifier les sorts qu'un monstre connaît ou a préparé, en
remplaçant n'importe quel sort de sa liste de sorts par un sort du même
niveau et de la même liste de classe. Si vous faites cela, vous pouvez
faire en sorte que le monstre soit une menace plus ou moins grande que
celle suggérée par sa valeur de défi.



#### Psionique

Un monstre qui lance des sorts en utilisant uniquement la puissance de
son esprit a l'étiquette psionique ajoutée à son trait spécial Lanceur
de sorts ou Lanceur de sorts inné. Cette étiquette ne comporte pas de
règles spéciales en soi, mais d'autres parties du jeu peuvent y faire
référence. Un monstre qui possède cette étiquette n'a généralement pas
besoin de composantes pour lancer ses sorts.




### Actions

Lorsqu'un monstre effectue son action, il peut choisir parmi les
options de la section Actions de son bloc de statistiques ou utiliser
l'une des actions disponibles pour toutes les créatures, comme
l'action Foncer ou Se cacher.


#### Attaques en mêlée et à distance

Les actions les plus communes d'un monstre en combat sont les attaques
de mêlée et les attaques à distance. Il peut s'agir d'attaques de
sorts ou d'armes, où l'\"arme\" peut être un objet manufacturé ou une
arme naturelle, comme une griffe ou une pointe de queue. Pour plus
d'informations sur les différents types d'attaques, voir
\"Attaquer\".

***Créature vs. cible.*** La cible d'une attaque de mêlée ou à distance
est généralement soit une créature, soit une cible, la différence étant
qu'une \"cible\" peut être une créature ou un objet.

***Touche.*** Les dégâts infligés ou les autres effets résultant du fait
que l'attaque a touché la cible sont décrits après la mention
\"touche\". Vous avez la possibilité de prendre les dégâts moyens ou de
lancer le dé ; pour cette raison, les dégâts moyens et l'expression du
dé sont tous deux présentés.

***Manqué.*** Si une attaque a un effet qui se produit en cas de ratage,
cette information est présentée après la mention \"Raté :\".

> #### Règles d'agrippage pour les monstres {#grapple-rules-for-monsters}
>
> De nombreux monstres ont des attaques spéciales qui leur permettent
> d'agripper rapidement une proie. Lorsqu'un monstre est touché par
> une telle attaque, il n'a pas besoin de faire un test d'aptitude
> supplémentaire pour déterminer si l'agrippement réussit, sauf si
> l'attaque dit le contraire.
>
> Une créature agrippée par le monstre peut utiliser son action pour
> tenter de s'échapper. Pour cela, elle doit réussir un test de Force
> (Athlétisme) ou de Dextérité (Acrobaties) contre le DC d'évasion
> indiqué dans le bloc de statistiques du monstre. Si aucun DC de fuite
> n'est indiqué, le DC est de 10 + le modificateur de Force
> (Athlétisme) du monstre.



#### Attaques multiples

Une créature qui peut effectuer des attaques multiples à son tour
dispose de l'action Attaques multiples. Une créature ne peut pas
utiliser Attaques multiples lorsqu'elle effectue une attaque
d'opportunité, qui doit être une seule attaque de mêlée.



#### Munitions

Un monstre porte suffisamment de munitions pour effectuer ses attaques à
distance. On peut supposer qu'un monstre dispose de 2d4 munitions pour
une attaque à l'arme lancée, et de 2d10 munitions pour une arme à
projectile comme un arc ou une arbalète.



#### Réactions

Si un monstre peut faire quelque chose de spécial avec sa réaction,
cette information est contenue ici. Si une créature n'a pas de réaction
spéciale, cette section est absente.




### Usage limité

Certaines capacités spéciales ont des restrictions sur le nombre de fois
qu'elles peuvent être utilisées.

***X/Jour.*** La notation \"X/Jour\" signifie qu'une capacité spéciale
peut être utilisée X fois et que le monstre doit terminer un long repos
pour récupérer les utilisations dépensées. Par exemple, \"1/Jour\"
signifie qu'une capacité spéciale peut être utilisée une fois et que le
monstre doit terminer un long repos pour l'utiliser à nouveau.

***Recharge X-Y.*** La mention \"Recharge X-Y\" signifie qu'un monstre
peut utiliser une capacité spéciale une fois et que cette capacité a
ensuite une chance aléatoire de se recharger lors de chaque round de
combat suivant. Au début de chaque tour du monstre, lancez un d6. Si le
résultat est l'un des nombres indiqués dans la notation de recharge, le
monstre retrouve l'utilisation de la capacité spéciale. La capacité se
recharge également lorsque le monstre termine un repos court ou long.

Par exemple, \"Recharge 5-6\" signifie qu'un monstre peut utiliser la
capacité spéciale une fois. Ensuite, au début du tour du monstre, il
retrouve l'utilisation de cette capacité s'il obtient un 5 ou un 6 sur
un d6.

***Recharge après un repos court ou long.*** Cette notation signifie
qu'un monstre peut utiliser une capacité spéciale une fois et doit
ensuite terminer un repos court ou long pour la réutiliser.



### Équipement

Un bloc de stat fait rarement référence à l'équipement, autre que
l'armure ou les armes utilisées par un monstre. Une créature qui porte
habituellement des vêtements, comme un humanoïde, est supposée être
habillée de manière appropriée.

Vous pouvez équiper les monstres d'un équipement supplémentaire et de
babioles comme vous le souhaitez, et vous décidez de la quantité
d'équipement d'un monstre qui peut être récupérée après que la
créature a été tuée et si cet équipement est encore utilisable. Une
armure abîmée faite pour un monstre est rarement utilisable par
quelqu'un d'autre, par exemple.

Si un monstre lanceur de sorts a besoin de composantes matérielles pour
lancer ses sorts, supposez qu'il possède les composantes matérielles
dont il a besoin pour lancer les sorts dans son bloc de statistiques.




### Créatures légendaires

Une créature légendaire peut faire des choses que les créatures
ordinaires ne peuvent pas faire. Elle peut entreprendre des actions
spéciales en dehors de son tour, et elle peut exercer une influence
magique à des kilomètres à la ronde.

Si une créature prend la forme d'une créature légendaire, par exemple
grâce à un sort, elle ne bénéficie pas des actions légendaires, des
actions de repaire ou des effets régionaux de cette forme.


#### Actions légendaires

Une créature légendaire peut effectuer un certain nombre d'actions
spéciales, appelées actions légendaires, en dehors de son tour. Une
seule option d'action légendaire peut être utilisée à la fois et
uniquement à la fin du tour d'une autre créature. Une créature récupère
ses actions légendaires dépensées au début de son tour. Elle peut
renoncer à les utiliser, et elle ne peut pas les utiliser si elle est
frappée d'incapacité ou incapable d'entreprendre des actions. Si elle
est surprise, elle ne peut les utiliser qu'après son premier tour dans
le combat.



#### Le repaire d'une créature légendaire

Une créature légendaire peut avoir une section décrivant sa tanière et
les effets spéciaux qu'elle peut créer lorsqu'elle s'y trouve, soit
par acte de volonté, soit simplement par sa présence. Une telle section
ne s'applique qu'à une créature légendaire qui passe beaucoup de temps
dans sa tanière.


##### Actions de la tanière

Si une créature légendaire a des actions de tanière, elle peut les
utiliser pour exploiter la magie ambiante dans sa tanière. Au compte
d'initiative 20 (en perdant toutes les égalités d'initiative), elle
peut utiliser une de ses options d'action de tanière. Il ne peut pas le
faire s'il est frappé d'incapacité ou s'il est autrement incapable de
faire des actions. S'il est surpris, il ne peut en utiliser une
qu'après son premier tour dans le combat.



##### Effets régionaux

La simple présence d'une créature légendaire peut avoir des effets
étranges et merveilleux sur son environnement, comme indiqué dans cette
section. Les effets régionaux se terminent brusquement ou se dissipent
avec le temps lorsque la créature légendaire meurt.





## Descriptions des monstres


### Non classé


#### Aboleth

*Grandes aberrations, maléfices légaux.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 135 (18d10 + 36)

**Vitesse** 10 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   9 (-1)    15 (+2)   18 (+4)   15 (+2)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +6, Int +8, Wis +6

**Compétences** Histoire +12, Perception +10

**Sens** Vision dans le noir 120 ft., Perception passive 20

**Langues** Parlé des profondeurs, Télépathie 120 ft.

**Défi** 10 (5 900 XP)

***Amphibie.*** L'Aboleth peut respirer de l'air et de l'eau.

***Nuage de mucus.*** Lorsqu'il est sous l'eau, l'aboleth est entouré
de mucus transformateur. Une créature qui touche l'aboleth ou qui le
frappe avec une attaque de mêlée alors qu'elle se trouve à moins de 1,5
m de lui doit effectuer un jet de sauvegarde de Constitution DC 14. En
cas d'échec, la créature est malade pendant 1d4 heures. La créature
malade ne peut respirer que sous l'eau.

***Sonder la télépathie.*** Si une créature communique par télépathie
avec l'aboleth, ce dernier apprend les plus grands désirs de la
créature si l'aboleth peut la voir.


##### Actions

***Attaques multiples.*** L'Aboleth effectue trois attaques de
tentacules.

***Tentacule.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 12 (2d6 + 5) points
de dégâts contondants. Si la cible est une créature, elle doit réussir
un jet de sauvegarde de Constitution DC 14 ou devenir malade. La maladie
n'a aucun effet pendant 1 minute et peut être éliminée par n'importe
quelle magie qui soigne les maladies. Après 1 minute, la peau de la
créature malade devient translucide et visqueuse, la créature ne peut
pas regagner de points de vie à moins d'être sous l'eau, et la maladie
ne peut être supprimée que par *Guérison* ou un autre sort
guérissant les maladies de 6e niveau ou plus. Lorsque la créature se
trouve à l'extérieur d'une étendue d'eau, elle subit 6 (1d12) dégâts
d'acide toutes les 10 minutes, sauf si de l'humidité est appliquée sur
la peau avant que 10 minutes ne se soient écoulées.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 10 ft. une cible. *Touché :* 15 (3d6 + 5) points de dégâts
contondants.

***Asservir (3/Jour).*** L'aboleth cible une créature qu'il peut voir
dans un rayon de 30 pieds autour de lui. La cible doit réussir un jet de
sauvegarde de Sagesse DC 14 ou être magiquement charmée par l'aboleth
jusqu'à la mort de ce dernier ou jusqu'à ce qu'elle se trouve sur un
plan d'existence différent de celui de la cible. La cible charmée est
sous le contrôle de l'aboleth et ne peut pas faire de réactions, et
l'aboleth et la cible peuvent communiquer entre eux par télépathie sur
n'importe quelle distance.

Chaque fois que la cible charmée subit des dégâts, elle peut répéter le
jet de sauvegarde. En cas de réussite, l'effet prend fin. Une fois
toutes les 24 heures au maximum, la cible peut également répéter le jet
de sauvegarde si elle se trouve à au moins 1 mile de l'aboleth.



##### Actions légendaires

L'aboleth peut effectuer 3 actions légendaires, en choisissant parmi
les options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
L'aboleth récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** L'Aboleth effectue un test de Sagesse (Perception).

***Coup de queue.*** L'Aboleth effectue une attaque de la queue.

***Drain psychique (coûte 2 actions).*** Une créature charmée par
l'aboleth subit 10 (3d6) dégâts psychiques, et l'aboleth regagne un
nombre de points de vie égal aux dégâts subis par la créature.




#### Ankheg

*Grande monstruosité, sans alignement.*

**Classe d'armure** 14 (armure naturelle), 11 en position couchée.

**Points de vie** 39 (6d10 + 6)

**Vitesse** 30 ft., terrier 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   11 (+0)   13 (+1)   1 (-5)    13 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 pieds, sens des secousses 60 pieds,
Perception passive 11

**Langues** -

**Défi** 2 (450 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts
tranchants plus 3 (1d6) dégâts d'acide. Si la cible est une créature
Grand ou plus petite, elle est agrippée (fuite DC 13). Jusqu'à la fin
de ce grappin, l'ankheg ne peut mordre que la créature agrippée et a
l'avantage sur les jets d'attaque pour le faire.

***Jet d'acide (Recharge 6).*** L'Ankheg crache de l'acide sur une
ligne de 30 pieds de long et 5 pieds de large, à condition qu'aucune
créature n'y soit agrippée. Chaque créature de cette ligne doit
effectuer un jet de sauvegarde de Dextérité DC 13, subissant 10 (3d6)
dégâts d'acide en cas d'échec, ou la moitié des dégâts en cas de
réussite.




#### Avatar de la mort

*Mort-vivant de taille moyenne, mal neutre.*

**Classe d'armure** 20

**Points de vie**: la moitié du maximum de points de vie de son
invocateur.

**Vitesse** 60 ft, Vol 60 ft (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   16 (+3)   16 (+3)   16 (+3)   16 (+3)   16 (+3)

  -----------------------------------------------------------

**Immunités aux dommages :** nécrotique, poison.

**Condition Immunités :** charmé, effrayé, paralysé, pétrifié,
empoisonné, inconscient.

**Sens** vision dans le noir 60 ft., vision véritable 60 ft., passif

**Perception** 13

**Langues** toutes les langues connues de son invocateur

**Défi** - (0 XP)

***Mouvement Incorporel.*** L'avatar peut se déplacer à travers
d'autres créatures et objets comme s'il s'agissait d'un terrain
difficile. Il subit 5 (1d10) dégâts de force s'il termine son tour à
l'intérieur d'un objet.

***Immunité à la transformation.*** L'avatar est immunisé contre les
capacités qui transforment les morts-vivants.


##### Actions

***Faucheuse.*** L'avatar balaie de sa faux spectrale une créature
située à moins de 1,5 m de lui, infligeant 7 (1d8 + 3) points de dégâts
tranchants plus 4 (1d8) points de dégâts nécrotiques.

Voir \"*Cartes merveilleuses*\".




#### Azer

*Elémentaire moyen, légalement neutre.*

**Classe d'armure** 17 (armure naturelle, bouclier)

**Points de vie** 39 (6d8 + 12)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   12 (+1)   15 (+2)   12 (+1)   13 (+1)   10 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +4

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** passif Perception 11

**Langues** Ardent

**Défi** 2 (450 XP)

***Corps chauffé.*** Une créature qui touche l'azer ou le frappe avec
une attaque de mêlée alors qu'elle se trouve à moins de 1,5 m de lui
subit 5 (1d10) dégâts de feu.

***Armes chauffées.*** Lorsque l'azer frappe avec une arme de mêlée en
métal, il inflige 3 (1d6) dégâts de feu supplémentaires (inclus dans
l'attaque).

***Illumination.*** L'Azer diffuse une lumière vive dans un rayon de 3
mètres et une lumière chétive sur 3 mètres supplémentaires.


##### Actions

***Marteau de guerre.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points
de dégâts de contondant, ou 8 (1d10 + 3) points de dégâts de contondant
si utilisé à deux mains pour faire une attaque de mêlée, plus 3 (1d6)
dégâts de feu.




#### Basilic

*Monstruosité moyenne, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 52 (8d8 + 16)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   8 (-1)    15 (+2)   2 (-4)    8 (-1)    7 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** -

**Défi** 3 (700 XP)

***Regard pétrifié.*** Si une créature commence son tour à moins de 30
pieds du basilic et que tous deux peuvent se voir, le basilic peut
forcer la créature à effectuer un jet de sauvegarde de Constitution DC
12 si le basilic n'est pas frappé d'incapacité. En cas d'échec, la
créature commence magiquement à se transformer en pierre et est
entravée. Elle doit répéter le jet de sauvegarde à la fin de son
prochain tour. En cas de réussite, l'effet prend fin. En cas d'échec,
la créature est pétrifiée jusqu'à ce qu'elle soit libérée par le sort
Restauration *supérieure* ou une autre magie.

Une créature qui n'est pas surprise peut détourner les yeux pour éviter
le jet de sauvegarde au début de son tour. Si elle le fait, elle ne peut
pas voir le basilic jusqu'au début de son prochain tour, où elle peut à
nouveau détourner les yeux. S'il regarde le basilic entre-temps, il
doit immédiatement effectuer le jet de sauvegarde.

Si le basilic voit son reflet à moins de 30 pieds de lui en lumière
vive, il se prend pour un rival et se cible du regard.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts
perforants plus 7 (2d6) dégâts de poison.




#### Béhir

*Très grande monstruosité, mal neutre.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 168 (16d12 + 64)

**Vitesse** 50 ft., ascension 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   16 (+3)   18 (+4)   7 (-2)    14 (+2)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +6, Discrétion +7

**Immunité aux dégâts** de la foudre

**Sens** Vision dans le noir 90 ft., Perception passive 16

**Langues** draconiques

**Défi** 11 (7 200 XP)


##### Actions

***Attaques multiples.*** Le Béhir effectue deux attaques : une avec sa
morsure et une pour constriction.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 22 (3d10 + 6) points
de dégâts perforants.

***Constriction.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 5 pieds, une créature Grand ou plus petite. *Touché
:* 17 (2d10 + 6) dégâts de contondant plus 17 (2d10 + 6) dégâts de
tranchant. La cible est agrippée (fuite DC 16) si le Béhir n'est pas
déjà en train de constrictionner une créature, et la cible est entravée
jusqu'à la fin de cette agrippe.

***Souffle de foudre (Recharge 5-6).*** Le Béhir exhale une ligne
d'éclairs de 20 pieds de long et 5 pieds de large. Chaque créature
située dans cette ligne doit effectuer un jet de sauvegarde de Dextérité
DC 16, subissant 66 (12d10) dégâts de foudre en cas d'échec, ou la
moitié des dégâts en cas de réussite.

***Avaler.*** Le Béhir effectue une attaque de morsure contre une cible
de taille moyenne ou plus petite qu'il agrippe. Si l'attaque touche,
la cible est également avalée, et le grappin prend fin.

Pendant qu'elle est avalée, la cible est aveuglée et entravée, elle
bénéficie d'un abri total contre les attaques et autres effets
extérieurs au Béhir, et elle subit 21 (6d6) dégâts d'acide au début de
chacun des tours du Béhir. Un Béhir ne peut faire avaler qu'une seule
créature à la fois.

Si le Béhir subit 30 points de dégâts ou plus en un seul tour de la part
de la créature avalée, il doit réussir un jet de sauvegarde de
Constitution DC 14 à la fin de ce tour ou régurgiter la créature, qui
tombe couchée dans un espace situé à 10 pieds du Béhir. Si le Béhir
meurt, une créature avalée n'est plus entravée par lui et peut
s'échapper du cadavre en utilisant 15 pieds de mouvement, en sortant
couché.




#### Gobelours

*Humanoïde moyen (gobelinoïde), maléfique chaotique.*

**Classe d'armure** 16 (armure de peau, bouclier)

**Points de vie** 27 (5d8 + 5)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   13 (+1)   8 (-1)    11 (+0)   9 (-1)

  -----------------------------------------------------------

**Compétences** Discrétion +6, Survie +2

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues :** commun, gobelin.

**Défi** 1 (200 XP)

***Brute.*** Une arme de mêlée inflige un dé supplémentaire de ses
dégâts lorsque le Gobelours la touche (inclus dans l'attaque).

***Attaque surprise.*** Si le Gobelours surprend une créature et la
touche avec une attaque au cours du premier round de combat, la cible
subit 7 (2d6) points de dégâts supplémentaires suite à cette attaque.


##### Actions

***Morgenstern.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d8 + 2) points de
dégâts perforants.

***Javeline.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+4 pour toucher, allonge de 5 pieds ou portée de 30/120 pieds, une
cible. *Touché :* 9 (2d6 + 2) dégâts perforants en mêlée ou 5 (1d6 + 2)
dégâts perforants à distance.




#### Bulette

*Grande monstruosité, sans alignement.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 94 (9d10 + 45)

**Vitesse** 40 ft., terrier 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   11 (+0)   21 (+5)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +6

**Sens** Vision dans le noir 60 pieds, sens des secousses 60 pieds,
Perception passive 16

**Langues** -

**Défi** 5 (1 800 XP)

***Saut debout.*** Le saut en longueur de la Bulette peut atteindre 30
pieds et son saut en hauteur peut atteindre 15 pieds, avec ou sans
départ lancé.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 30 (4d12 + 4) points de
dégâts perforants.

***Bond mortel.*** Si la bulette fait un saut d'au moins 15 pieds dans
le cadre de son mouvement, elle peut alors utiliser cette action pour
atterrir sur ses pieds dans un espace contenant une ou plusieurs autres
créatures. Chacune de ces créatures doit réussir un jet de sauvegarde de
Force ou de Dextérité DC 16 (au choix de la cible) ou être contondante
et subir 14 (3d6 + 4) dégâts de déblocage plus 14 (3d6 + 4) dégâts
tranchants. En cas de sauvegarde réussie, la créature ne subit que la
moitié des dégâts, n'est pas mise au sol et est poussée de 1,5 m hors
de l'espace de la bulette dans un espace inoccupé de son choix. Si
aucun espace inoccupé n'est à portée, la créature tombe à plat ventre
dans l'espace de la bulette.




#### Centaure

*Grande monstruosité, bien neutre.*

**Classe d'armure** 12

**Points de vie** 45 (6d10 + 12)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   14 (+2)   14 (+2)   9 (-1)    13 (+1)   11 (+0)

  -----------------------------------------------------------

**Compétences** Athlétisme +6, Perception +3, Survie +3

**Sens** passif Perception 13

**Langues** Elfe, Sylvestre

**Défi** 2 (450 XP)

***Charge.*** Si le centaure se déplace d'au moins 30 pieds en ligne
droite vers une cible puis la touche avec une attaque de pique au même
tour, la cible subit 10 (3d6) dégâts perforants supplémentaires.


##### Actions

***Attaques multiples.*** Le centaure effectue deux attaques : une avec
sa pique et une avec ses sabots ou deux avec son arc long.

***Pique.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 9 (1d10 + 4) points de dégâts
perforants.

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts contondants.

***Arc long.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.




#### Chimère

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 114 (12d10 + 48)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   11 (+0)   19 (+4)   3 (-4)    14 (+2)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +8

**Sens** Vision dans le noir 60 ft., Perception passive 18

**Langues** comprend le draconique mais ne peut pas parler

**Défi** 6 (2 300 XP)


##### Actions

***Attaques multiples.*** La chimère effectue trois attaques : une avec
sa morsure, une avec ses cornes et une avec ses griffes. Lorsque son
souffle de feu est disponible, elle peut l'utiliser à la place de sa
morsure ou de ses cornes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts perforants.

***Cornes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (1d12 + 4) points de
dégâts contondants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Souffle de feu (Recharge 5-6).*** La tête du dragon exhale du feu
dans un cône de 15 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Dextérité DC 15, subissant 31 (7d8) dégâts de
feu en cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Chuul

*Grande aberration, mal chaotique*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 93 (11d10 + 33)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   16 (+3)   5 (-3)    11 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +4

**Immunités aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** comprend le Parlé des profondeurs mais ne peut pas parler

**Défi** 4 (1 100 XP)

***Amphibie.*** Le chuul peut respirer de l'air et de l'eau.

***Détection de la magie.*** Le chuul détecte à volonté la magie dans un
rayon de 120 pieds autour de lui. Ce trait fonctionne autrement comme le
sort *Détection de la magie* mais n'est pas lui-même
magique.


##### Actions

***Attaques multiples.*** Le chuul effectue deux attaques en tenaille.
Si le chuul est agrippé à une créature, le chuul peut également utiliser
ses tentacules une fois.

***Pince.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 11 (2d6 + 4) dégâts
contondants. La cible est agrippée (fuite DC 14) s'il s'agit d'une
créature Grand ou plus petite et que le chuul n'a pas deux autres
créatures agrippées.

***Tentacules.*** Une créature agrippée par le chuul doit réussir un jet
de sauvegarde de Constitution DC 13 ou être empoisonnée pendant 1
minute. Jusqu'à la fin de ce poison, la cible est paralysée. La cible
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de réussite.




#### Manteleur

*Grandes aberrations, chaotique neutre.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 78 (12d10 + 12)

**Vitesse** 10 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   12 (+1)   13 (+1)   12 (+1)   14 (+2)

  -----------------------------------------------------------

**Compétences** Discrétion +5

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** Parlé des profondeurs, commun des profondeurs

**Défi** 8 (3 900 XP)

***Transfert des dégâts.*** Lorsqu'il est attaché à une créature, le
manteau ne subit que la moitié des dégâts qui lui sont infligés
(arrondis à l'inférieur), et cette créature subit l'autre moitié.

***Fausse apparence.*** Si le manteau reste immobile sans que son
dessous soit exposé, il est impossible de le distinguer d'une cape en
cuir sombre.

***Sensibilité à la lumière.*** Lorsqu'il se trouve dans une lumière
vive, le Manteleur a un désavantage aux jets d'attaque et aux tests de
Sagesse (Perception) qui reposent sur la vue.


##### Actions

***Attaques multiples.*** Le Manteleur effectue deux attaques : une avec
sa morsure et une avec sa queue.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 10 (2d6 + 3) points
de dégâts perforants, et si la cible est Grand ou plus petite, la cape
s'attache à elle. Si le manteau a un avantage sur la cible, le manteau
s'attache à la tête de la cible, et la cible est aveuglée et incapable
de respirer tant que le manteau est attaché. Tant qu'il est attaché, le
manteau ne peut effectuer cette attaque que contre la cible et a
l'avantage sur le jet d'attaque. Le manteau peut se détacher en
dépensant 5 pieds de son mouvement. Une créature, y compris la cible,
peut prendre son action pour détacher la cape en réussissant un test de
Force DC 16.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 10 pieds, une créature. *Touché :* 7 (1d8 + 3) dégâts
tranchants.

***Gémissement.*** Chaque créature dans un rayon de 60 pieds du manteau
qui peut entendre son gémissement et qui n'est pas une aberration doit
réussir un jet de sauvegarde de Sagesse DC 13 ou devenir effrayée
jusqu'à la fin du prochain tour du manteau. Si le jet de sauvegarde
d'une créature est réussi, la créature est immunisée contre le
gémissement du Manteleur pour les 24 heures suivantes Phantasmes (se
recharge après un repos court ou long). Le Manteleur crée magiquement
trois doubles illusoires de lui-même s'il n'est pas sous une lumière
vive. Les doubles se déplacent avec lui et imitent ses actions,
changeant de position de façon à ce qu'il soit impossible de déterminer
quel est le véritable manteau. Si le Manteleur se trouve dans une zone
de lumière vive, les doubles disparaissent.

Chaque fois qu'une créature cible le manteau avec une attaque ou un
sort nuisible alors qu'il reste un double, cette créature effectue un
jet aléatoire pour déterminer si elle cible le manteau ou l'un des
doubles. Une créature n'est pas affectée par cet effet magique si elle
ne peut pas voir ou si elle se fie à des sens autres que la vue.

Un double a la CA du manteau et utilise ses jets de sauvegarde. Si une
attaque touche un double, ou si un double échoue à un jet de sauvegarde
contre un effet qui inflige des dégâts, le double disparaît.




#### Cockatrice

*Petite monstruosité, sans alignement.*

**Classe d'armure** 11

**Points de vie** 27 (6d6 + 6)

**Vitesse** 20 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    12 (+1)   12 (+1)   2 (-4)    13 (+1)   5 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** -

**Défi** 1/2 (100 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 3 (1d4 + 1) dégâts
perforants, et la cible doit réussir un jet de sauvegarde de
Constitution DC 11 pour ne pas être pétrifiée magiquement. En cas
d'échec, la créature commence à se transformer en pierre et est
entravée. Elle doit répéter le jet de sauvegarde à la fin de son
prochain tour. En cas de réussite, l'effet prend fin. En cas d'échec,
la créature est pétrifiée pendant 24 heures.




#### Couatl

*Céleste moyen, bon droit.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 97 (13d8 + 39)

**Vitesse** 30 ft., Vol 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   20 (+5)   17 (+3)   18 (+4)   20 (+5)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +5, Wis +7, Cha +6

**Résistance aux dégâts** radiant

**Immunités aux dommages** psychiques ; contondant, perforant et
tranchant des attaques non magiques.

**Sens** vision véritable 120 ft., Perception passive 15

**Langues** toutes, télépathie 120 ft.

**Défi** 4 (1 100 XP)

***Lanceur de sorts inné.*** La caractéristique d'incantation du Couatl
est le Charisme (sauvegarde contre les sorts DC 14). Il peut lancer de
manière innée les sorts suivants, ne nécessitant que des composantes
verbales :

À volonté : *Détection du mal et du bien*,
*Détection de la magie*, *Détection des
pensées*.

3/jour chacun : *Bénédiction*, *Création de nourriture et
d'eau*,
*Restauration partielle*, *Protection contre le
poison*,
*Bouclier*.

1/jour chacun : *Songe*, Restauration
*supérieure*.

***Armes magiques.*** Les attaques des armes du Couatl sont magiques.

***Bouclier mental.*** Le Couatl est immunisé contre la
*Scrutation* et contre tout effet qui pourrait ressentir ses
émotions, lire ses pensées ou détecter sa position.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 8 (1d6 + 5) dégâts
perforants, et la cible doit réussir un jet de sauvegarde de
Constitution DC 13 ou être empoisonnée pendant 24 heures. Jusqu'à la
fin de ce poison, la cible est inconsciente. Une autre créature peut
utiliser une action pour secouer la cible et la réveiller.

***Constriction.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 3 m, une créature moyenne ou plus petite. *Touché :*
10 (2d6 + 3) points de dégâts contondants, et la cible est agrippée pour
échapper à DC 15). Jusqu'à la fin de cet agrippement, la cible est
entravée, et le couatl ne peut pas constrictionner une autre cible.

***Changement de forme.*** Le couatl se métamorphose magiquement en un
humanoïde ou une bête dont la valeur de défi est égale ou inférieure à
la sienne, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du couatl).

Dans une nouvelle forme, le couatl conserve ses statistiques de jeu et
sa capacité à parler, mais sa CA, ses modes de déplacement, sa Force, sa
Dextérité et ses autres actions sont remplacées par celles de la
nouvelle forme, et il gagne toutes les statistiques et capacités (sauf
les caractéristiques de classe, les actions légendaires et les actions
de repaire) que la nouvelle forme possède mais qui lui manquent.

Si la nouvelle forme a une attaque de morsure, le couatl peut utiliser
sa morsure dans cette forme.




#### Mante ténébreuse

*Petite monstruosité, sans alignement.*

**Classe d'armure** 11

**Points de vie** 22 (5d6 + 5)

**Vitesse** 10 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   12 (+1)   13 (+1)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Discrétion +3

**Sens** vision aveugle 60 ft., Perception passive 10

**Langues** -

**Défi** 1/2 (100 XP)

***Echolocation.*** La Mante ténébreuse ne peut pas utiliser sa vision
aveugle lorsqu'elle est assourdie.

***Fausse apparence.*** Lorsque la Mante ténébreuse reste immobile, il
est impossible de la distinguer d'une formation caverneuse telle
qu'une stalactite ou une stalagmite.


##### Actions

***Écraser.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 6 (1d6 + 3) points
de dégâts contondants, et la Mante ténébreuse s'attache à la cible. Si
la cible est de taille moyenne ou plus petite et que la Mante ténébreuse
a l'avantage au jet d'attaque, elle s'attache en engloutissant la
tête de la cible, qui est également aveuglée et incapable de respirer
tant que la Mante ténébreuse est attachée de cette manière.

Tant qu'elle est attachée à la cible, la Mante ténébreuse ne peut
attaquer aucune autre créature que la cible mais a un avantage sur ses
jets d'attaque. La vitesse de la Mante ténébreuse devient également 0,
elle ne peut bénéficier d'aucun bonus à sa vitesse, et elle se déplace
avec la cible.

Une créature peut détacher la Mante ténébreuse en réussissant un test de
Force DC 13 comme action. À son tour, la mante ténébreuse peut se
détacher de la cible en utilisant 1,5 m de mouvement.

***Aura de Ténèbres (1/Jour).*** Un rayon de 15 pieds de ténèbres
magiques s'étend à partir de la Mante ténébreuse, se déplace avec elle,
et se répand dans les coins. Les ténèbres durent aussi longtemps que la
mante ténébreuse maintient sa concentration, jusqu'à 10 minutes (comme
si elle se concentrait sur un sort). La Vision dans le noir ne peut pas
pénétrer cette obscurité, et aucune lumière naturelle ne peut
l'éclairer. Si une partie des ténèbres chevauche une zone de lumière
créée par un sort de 2e niveau ou moins, le sort créant la lumière est
dissipé.




#### Doppelganger

*Monstrosité moyenne (Métamorphe), neutre.*

**Classe d'armure** 14

**Points de vie** 52 (8d8 + 16)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   18 (+4)   14 (+2)   11 (+0)   12 (+1)   14 (+2)

  -----------------------------------------------------------

**Compétences** Supercherie +6, Intuition +3

**Condition Immunités** charmées

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Roturiers** Communs

**Défi** 3 (700 XP)

***Changement de forme.*** Le Doppelganger peut utiliser son action pour
se métamorphoser en un petit ou moyen humanoïde qu'il a vu, ou
reprendre sa forme véritable. Ses statistiques, autres que sa taille,
sont les mêmes sous chaque forme. L'équipement qu'il porte n'est pas
transformé. Il reprend sa vraie forme s'il meurt.

***Embuscade.*** Au premier round de combat, le Doppelganger a un
avantage sur les jets d'attaque contre toute créature qu'il a
surprise.

***Attaque surprise.*** Si le Doppelganger surprend une créature et la
touche avec une attaque au cours du premier round de combat, la cible
subit 10 (3d6) dégâts supplémentaires de l'attaque.


##### Actions

***Attaques multiples.*** Le Doppelganger effectue deux attaques de
mêlée.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 7 (1d6 + 4) points de dégâts
contondants.

***Lecture des pensées.*** Le Doppelganger lit magiquement les pensées
de surface d'une créature située à moins de 60 pieds de lui.

L'effet peut traverser les barrières, mais 3 pieds de bois ou de terre,
2 pieds de pierre, 2 pouces de métal ou une fine feuille de plomb le
bloquent. Tant que la cible est à portée, le doppelganger peut continuer
à lire ses pensées, tant que sa concentration n'est pas rompue (comme
s'il se concentrait sur un sort). Lorsqu'il lit dans les pensées de la
cible, le Doppelganger a un avantage sur les tests de Sagesse
(Intuition) et de Charisme (Tromperie, Intimidation et Persuasion)
contre la cible.




#### Dragon-tortue

*Dragon Gargantuesque, neutre*

**Classe d'armure** 20 (armure naturelle)

**Points de vie** 341 (22d20 + 110)

**Vitesse** 20 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   25 (+7)   10 (+0)   20 (+5)   10 (+0)   12 (+1)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +11, Wis +7

**Résistance aux dégâts** du feu

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** aquatique, draconique

**Défi** 17 (18 000 XP)

***Amphibie.*** La tortue dragon peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** La tortue-dragon effectue trois attaques : une
avec sa morsure et deux avec ses griffes. Elle peut faire une attaque de
queue à la place de ses deux attaques de griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 26 (3d12 + 7) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 16 (2d8 + 7) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 26 (3d12 + 7) points
de dégâts contondants. Si la cible est une créature, elle doit réussir
un jet de sauvegarde de Force DC 20 ou être poussée jusqu'à 10 pieds de
la tortue-dragon et mise à plat ventre.

***Souffle de vapeur (Recharge 5-6).*** Le dragon-tortue exhale de la
vapeur brûlante dans un cône de 60 pieds. Chaque créature dans cette
zone doit effectuer un jet de sauvegarde de Constitution DC 18,
subissant 52 (15d6) dégâts de feu en cas d'échec, ou la moitié des
dégâts en cas de réussite. Le fait d'être sous l'eau ne confère aucune
résistance contre ces dégâts.




#### Drider

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 123 (13d10 + 52)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   16 (+3)   18 (+4)   13 (+1)   14 (+2)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +5, Discrétion +9

**Sens** Vision dans le noir 120 ft., Perception passive 15

**Langues** Elfe, commun des profondeurs

**Défi** 6 (2 300 XP)

***Ascendance féerique.*** Le drider a un avantage aux jets de
sauvegarde contre le charme, et la magie ne peut pas l'endormir.

***Lanceur de sorts inné.*** La caractéristique d'incantation innée du
drider est la Sagesse (sauvegarde contre les sorts DC 13). Le drider
peut lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel :

À volonté : *lumières dansantes*

1/jour chacun : *Ténèbres*

***Pattes d'araignée.*** Le drider peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, le
drider a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.

***Toile d'araignée.*** Le drider ignore les restrictions de mouvement
causées par les sangles.


##### Actions

***Attaques multiples.*** Le drider effectue trois attaques, soit avec
son épée longue, soit avec son arc long. Il peut remplacer une de ces
attaques par une attaque de morsure.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, portée de 1,5 m, une créature. *Touché :* 2 (1d4) dégâts
perforants plus 9 (2d8) dégâts de poison.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts tranchants, ou 8 (1d10 + 3) points de dégâts tranchants si
utilisée à deux mains.

***Arc long.*** *Attaque avec une arme à distance :* +6 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 7 (1d8 + 3) dégâts
perforants plus 4 (1d8) dégâts de poison.




#### Dryade

*Moyennement fée, neutre*

**Classe d'armure** 11 (16 avec *Peau d'écorce*

**Points de vie** 22 (5d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   12 (+1)   11 (+0)   14 (+2)   15 (+2)   18 (+4)

  -----------------------------------------------------------

**Compétences** Perception +4, Discrétion +5

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** Elfe, Sylvestre

**Défi** 1 (200 XP)

***Lanceur de sorts inné.*** La caractéristique innée d'incantation de
la dryade est le Charisme (sauvegarde contre les sorts DC 14). La dryade
peut lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel :

A volonté : *assistance*

3/jour chacun : *Enchevêtrement*, Baies
*nourricières*

1/jour chacun : Peau *d'éc*orce, *Passage sans
trace*

***Résistance à la magie.*** La dryade a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Parler avec les bêtes et les plantes.*** La Dryade peut communiquer
avec les bêtes et les plantes comme si elles partageaient une langue.

***Foulée d'arbre.*** Une fois à son tour, la Dryade peut utiliser 10
pieds de son mouvement pour entrer magiquement dans un arbre vivant à sa
portée et émerger d'un second arbre vivant à moins de 60 pieds du
premier arbre, apparaissant dans un espace inoccupé à moins de 5 pieds
du second arbre.

Les deux arbres doivent être Grands ou plus grands.


##### Actions

***Gourdin.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher (+6 pour toucher avec le *gourdin magique*,
allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts de contondant, ou
8 (1d8 + 4) dégâts de contondant avec le *gourdin
magique*.

***Charme fée.*** La dryade cible un humanoïde ou une bête qu'elle peut
voir dans un rayon de 30 pieds autour d'elle. Si la cible peut voir la
dryade, elle doit réussir un jet de sauvegarde de Sagesse DC 14 ou être
charmée magiquement. La créature charmée considère la dryade comme une
amie de confiance qu'il faut écouter et protéger. Bien que la cible ne
soit pas sous le contrôle de la dryade, elle accepte les demandes ou les
actions de la dryade de la manière la plus favorable possible.

Chaque fois que la dryade ou ses alliés font quelque chose de contaminé
à la cible, elle peut répéter le jet de sauvegarde, mettant fin à
l'effet sur elle-même en cas de réussite. Sinon, l'effet dure 24
heures ou jusqu'à ce que la dryade meure, se trouve sur un autre plan
d'existence que la cible ou mette fin à l'effet par une action bonus.
Si le jet de sauvegarde de la cible est réussi, la cible est immunisée
contre le charme féerique de la dryade pendant les 24 heures suivantes.

La Dryade ne peut avoir plus d'un humanoïde et jusqu'à trois bêtes
charmées à la fois.




#### Duergar

*Humanoïde moyen (nain), maléfique.*

**Classe d'armure** 16 (cotte de mailles, bouclier)

**Points de vie** 26 (4d8 + 8)

**Vitesse** 25 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   11 (+0)   14 (+2)   11 (+0)   10 (+0)   9 (-1)

  -----------------------------------------------------------

**Résistance aux dégâts** poison

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** nain, commun des profondeurs

**Défi** 1 (200 XP)

***Résiliente du Duergar.*** Le Duergar a un avantage aux jets de
sauvegarde contre le poison, les sorts et les illusions, ainsi que pour
résister à être charmé ou paralysé.

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, le
Duergar a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.


##### Actions

***Agrandissement (se recharge après un repos court ou long).*** Pendant
1 minute, le duergar augmente magiquement de taille, ainsi que tout ce
qu'il porte ou transporte. Lorsqu'il est agrandi, le duergar est
Grand, double ses dés de dégâts sur les attaques d'armes basées sur la
Force (inclus dans les attaques), et effectue des tests de Force et des
jets de sauvegarde de Force avec avantage. Si le Duergar manque de place
pour devenir Grand, il atteint la taille maximale possible dans
l'espace disponible.

***Pic de guerre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) dégâts
perforants, ou 11 (2d8 + 2) dégâts perforants si elle est agrandie.

***Javeline.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+4 pour toucher, allonge de 5 pieds ou portée de 30/120 pieds, une
cible. *Touché :* 5 (1d6 + 2) points de dégâts perforants, ou 9 (2d6 +
2) points de dégâts perforants lorsqu'il est agrandi.

***Invisibilité (se recharge après un repos court ou long).*** Le
duergar devient magiquement invisible jusqu'à ce qu'il attaque, lance
un sort ou utilise son Agrandissement, ou jusqu'à ce que sa
concentration soit rompue, jusqu'à 1 heure (comme s'il se concentrait
sur un sort). Tout équipement que le duergar porte ou transporte est
invisible avec lui.




#### Elfe, Drow

*Humanoïde moyen (elfe), neutre mauvais*

**Classe d'armure** 15 (chemise de mailles)

**Points de vie** 13 (3d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   14 (+2)   10 (+0)   11 (+0)   11 (+0)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +4

**Sens** Vision dans le noir 120 ft., Perception passive 12

**Langues** Elfe, commun des profondeurs

**Défi** 1/4 (50 XP)

***Ascendance féerique.*** Le drow a un avantage aux jets de sauvegarde
contre le charme, et la magie ne peut pas endormir le drow.

***Lanceur de sorts inné.*** La caractéristique d'incantation du Drow
est le Charisme (sauvegarde contre les sorts DC 11). Il peut lancer de
manière innée les sorts suivants, ne nécessitant aucun composant
matériel :

À volonté : *lumières dansantes*

1/jour chacun : *Ténèbres*

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, le
Drow a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.


##### Actions

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Arbalète à main.*** *Attaque avec une arme à distance :* +4 pour
toucher, portée de 30/120 pieds, une cible. *Touché :* 5 (1d6 + 2)
points de dégâts perforants, et la cible doit réussir un jet de
sauvegarde de Constitution DC 13 ou être empoisonnée pendant 1 heure. Si
le jet de sauvegarde échoue par 5 ou plus, la cible est également
inconsciente lorsqu'elle est empoisonnée de cette manière. La cible se
réveille si elle subit des dégâts ou si une autre créature entreprend
une action pour la secouer.




#### Ettercap

*Monstruosité moyenne, mal neutre.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 44 (8d8 + 8)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   15 (+2)   13 (+1)   7 (-2)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4, Survie +3

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues** -

**Défi** 2 (450 XP)

***Pattes d'araignée.*** L'ettercap peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** Lorsqu'il est en contact avec une toile,
l'ettercap connaît l'emplacement exact de toute autre créature en
contact avec la même toile.

***Toile d'araignée.*** L'Ettercap ignore les restrictions de
mouvement causées par les sangles.


##### Actions

***Attaques multiples.*** L'ettercap effectue deux attaques : une avec
sa morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 6 (1d8 + 2) dégâts
perforants plus 4 (1d8) dégâts de poison. La cible doit réussir un jet
de sauvegarde de Constitution DC 11 ou être empoisonnée pendant 1
minute.

La créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) points de
dégâts tranchants.

***Toile (recharge 5-6).*** *Attaque avec une arme à distance :* +4 pour
toucher, portée de 30/60 pieds, une créature Grand ou plus petite.
Touché : La créature est entravée par la toile. Comme action, la
créature entravée peut faire un test de Force DC 11 et s'échapper de la
toile si elle réussit. L'effet prend également fin si la sangle est
détruite. La sangle a une CA de 10, 5 points de vie, une vulnérabilité
aux dégâts de feu, et une immunité aux dégâts de matraquage, de poison
et psychiques.




#### Ettin

*Grand géant, mal chaotique*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 85 (10d10 + 30)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   8 (-1)    17 (+3)   6 (-2)    10 (+0)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** Géant, Orc

**Défi** 4 (1 100 XP)

***Deux têtes.*** L'ettin a un avantage sur les tests de Sagesse
(Perception) et sur les jets de sauvegarde contre l'aveuglement, le
charme, la surdité, la frayeur, l'étourdissement et le Déblocage.

***Éveillé.*** Lorsqu'une des têtes de l'ettin est endormie, son autre
tête est éveillée.


##### Actions

***Attaques multiples.*** L'Ettin effectue deux attaques : une avec sa
hache de guerre et une avec son étoile du matin.

***Hache d'armes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d8 + 5) points de
dégâts tranchants.

***Morgenstern.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d8 + 5) points de
dégâts perforants.




#### Gargouille

*Moyen élémentaire, mal chaotique*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 52 (7d8 + 21)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   11 (+0)   16 (+3)   6 (-2)    11 (+0)   7 (-2)

  -----------------------------------------------------------

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas adamantines.

**Immunité aux dommages** poison

**Immunités d'état :** épuisement, pétrifié, empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** glaiseux

**Défi** 2 (450 XP)

***Fausse apparence.*** Lorsque la gargouille reste immobile, il est
impossible de la distinguer d'une statue inanimée.


##### Actions

***Attaques multiples.*** La gargouille effectue deux attaques : une
avec sa morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts tranchants.




#### Fantôme

*Mort-vivant moyen, n'importe quel alignement.*

**Classe d'armure** 11

**Points de vie** 45 (10d8)

**Vitesse** 0 ft., Vol 40 ft. (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    13 (+1)   10 (+0)   10 (+0)   12 (+1)   17 (+3)

  -----------------------------------------------------------

**Résistance aux dégâts**: acide, feu, foudre, tonnerre ; contondant,
perforant et tranchant des attaques non magiques.

**Immunités aux dommages**: froid, nécrotique, poison.

**Immunités d'état** charmé, épuisé, agrippé, paralysé, pétrifié,
empoisonné, couché, retenu

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** toutes les langues qu'il a connues dans la vie

**Défi** 4 (1 100 XP)

***Vue éthérée.*** Le fantôme peut voir à 60 pieds dans le plan éthéré
lorsqu'il se trouve sur le plan matériel, et vice versa.

***Mouvement Incorporel.*** Le fantôme peut se déplacer à travers
d'autres créatures et objets comme s'il s'agissait d'un terrain
difficile. Il subit 5 (1d10) dégâts de force s'il termine son tour à
l'intérieur d'un objet.


##### Actions

***Withering Touch.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, allonge de 1,5 m, une cible. *Touché :* 17 (4d6 + 3)
dégâts nécrotiques.

***Forme éthérée.*** Le fantôme entre dans le plan éthéré à partir du
plan matériel, ou vice versa. Il est visible sur le plan matériel alors
qu'il se trouve dans la frontière éthérée, et vice versa, mais il ne
peut ni affecter ni être affecté par quoi que ce soit sur l'autre plan.

***Visage horrifiant.*** Chaque créature non-morte située à moins de 60
pieds du fantôme et qui peut le voir doit réussir un jet de sauvegarde
de Sagesse DC 13 ou être effrayée pendant 1 minute. Si la sauvegarde
échoue par 5 ou plus, la cible vieillit également de 1d4 × 10 ans. Une
cible effrayée peut répéter le jet de sauvegarde à la fin de chacun de
ses tours, mettant fin à l'état d'effroi sur elle-même en cas de
réussite. Si le jet de sauvegarde d'une cible est réussi ou si l'effet
prend fin pour elle, la cible est immunisée contre le Visage horrifiant
de ce fantôme pendant les 24 heures suivantes. L'effet de
vieillissement peut être inversé par un sort de Restauration
*supérieure*, mais seulement dans les 24 heures
suivant son apparition.

***Possession (Recharge 6).*** Un humanoïde que le fantôme peut voir à
moins de 1,5 m de lui doit réussir un jet de sauvegarde de Charisme DC
13 ou être possédé par le fantôme ; le fantôme disparaît alors, et la
cible est frappée d'incapacité et perd le contrôle de son corps. Le
fantôme contrôle désormais le corps mais ne prive pas la cible de sa
conscience. Le fantôme ne peut être la cible d'aucune attaque, d'aucun
sort ni d'aucun autre effet, à l'exception de ceux qui transforment
les morts-vivants, et il conserve son alignement, son Intelligence, sa
Sagesse, son Charisme et son immunité au charme et à l'effroi. Il
utilise par ailleurs les statistiques de la cible possédée, mais n'a
pas accès aux connaissances, aux caractéristiques de classe ou aux
compétences de la cible.

La possession dure jusqu'à ce que le corps tombe à 0 points de vie, que
le fantôme y mette fin par une action bonus, ou que le fantôme soit
retourné ou forcé à sortir par un effet comme le sort *Dissipation du
mal et du bien*. Lorsque la possession prend
fin, le fantôme réapparaît dans un espace inoccupé situé à moins de 1,5
m du corps. La cible est immunisée contre la possession de ce fantôme
pendant 24 heures après avoir réussi le jet de sauvegarde ou après la
fin de la possession.




#### Babélien

*Aberration moyenne, neutre*

**Classe d'armure** 9

**Points de vie** 67 (9d8 + 27)

**Vitesse** 10 ft., nager 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   8 (-1)    16 (+3)   3 (-4)    10 (+0)   6 (-2)

  -----------------------------------------------------------

**Établissement Immunités** enclines

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** -

**Défi** 2 (450 XP)

***Terrain aberrant.*** Le sol dans un rayon de 3 mètres autour du
mouton est un terrain difficile comme de la pâte. Chaque créature qui
commence son tour dans cette zone doit réussir un jet de sauvegarde de
Force DC 10 ou voir sa vitesse réduite à 0 jusqu'au début de son
prochain tour.

***Baragouinage.*** Le mouton baragouine de façon incohérente tant
qu'il peut voir n'importe quelle créature et n'est pas frappé
d'incapacité. Chaque créature qui commence son tour à moins de 6 mètres
du babélien et qui peut entendre le baragouin doit réussir un jet de
sauvegarde de Sagesse DC 10. En cas d'échec, la créature ne peut pas
avoir de réactions avant le début de son prochain tour et lance un d8
pour déterminer ce qu'elle fait pendant son tour. Sur un résultat de 1
à 4, la créature ne fait rien. Sur un 5 ou 6, la créature ne prend
aucune action ou action bonus et utilise tout son mouvement pour se
déplacer dans une direction déterminée aléatoirement. Sur un 7 ou un 8,
la créature effectue une attaque de mêlée contre une créature déterminée
aléatoirement à sa portée ou ne fait rien si elle ne peut pas effectuer
une telle attaque.


##### Actions

***Attaques multiples.*** Le Babélien fait une attaque de morsure et,
s'il le peut, utilise son crachat aveuglé.

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 17 (5d6) points de
dégâts perforants. Si la cible est de taille moyenne ou plus petite,
elle doit réussir un jet de sauvegarde de Force DC 10 ou être mise au
sol. Si la cible est tuée par ces dégâts, elle est absorbée par le
mouton.

***Crachats aveuglants (Recharge 5-6).*** Le moucheron crache une boule
chimique sur un point qu'il peut voir dans un rayon de 15 pieds autour
de lui. A l'impact, la boule explose en un éclair de lumière
aveuglante. Chaque créature située à moins de 1,5 m de l'éclair doit
réussir un jet de sauvegarde de Dextérité DC 13 ou être aveuglée
jusqu'à la fin du prochain tour du broyeur.




#### Gnoll

*Humanoïde moyen (Gnoll), maléfique.*

**Classe d'armure** 15 (armure de peau, bouclier)

**Points de vie** 22 (5d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   12 (+1)   11 (+0)   6 (-2)    10 (+0)   7 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Gnoll

**Défi** 1/2 (100 XP)

***Rampage.*** Lorsque le gnoll réduit une créature à 0 points de vie
avec une attaque de mêlée à son tour, il peut bénéficier d'une action
bonus pour se déplacer jusqu'à la moitié de sa vitesse et faire une
attaque de morsure.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 4 (1d4 + 2) dégâts
perforants.

***Lance.*** *Attaque avec une arme à distance ou de mêlée :* +4 pour
toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible. *Touché
:* 5 (1d6 + 2) points de dégâts perforants, ou 6 (1d8 + 2) points de
dégâts perforants si elle est utilisée à deux mains pour une attaque de
mêlée.

***Arc long.*** *Attaque avec une arme à distance :* +3 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts perforants.




#### Gnome, Profondeurs (Svirfneblin)

*Petit humanoïde (gnome), neutre bon*

**Classe d'armure** 15 (chemise de mailles)

**Points de vie** 16 (3d6 + 6)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   14 (+2)   12 (+1)   10 (+0)   9 (-1)

  -----------------------------------------------------------

**Compétences** Investigation +3, Perception +2, Discrétion +4

**Sens** Vision dans le noir 120 ft., Perception passive 12

**Langues** gnome, terrienne, commune des profondeurs

**Défi** 1/2 (100 XP)

***Camouflage dans la pierre.*** Le gnome a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain rocheux.

***Ruse gnome.*** Le gnome a un avantage sur les jets de sauvegarde
d'Intelligence, de Sagesse et de Charisme contre la magie.

***Lanceur de sorts inné.*** La caractéristique innée d'incantation du
gnome est l'Intelligence (sauvegarde contre les sorts DC 11). Il peut
lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel :

A volonté : *Antidétection*

1/jour chacun : *cécité/surdité*,
*Déguisement de soi*


##### Actions

***Pic de guerre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.

***Fléchette empoisonnée.*** *Attaque avec une arme à distance :* +4
pour toucher, portée de 30/120 pieds, une créature. *Touché :* 4 (1d4 +
2) points de dégâts perforants, et la cible doit réussir un jet de
sauvegarde de Constitution DC 12 ou être empoisonnée pendant 1 minute.
La cible peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.




#### Gobelin

*Petit humanoïde (goblinoïde), neutre maléfique*

**Classe d'armure** 15 (armure en cuir, bouclier)

**Points de vie** 7 (2d6)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    14 (+2)   10 (+0)   10 (+0)   8 (-1)    8 (-1)

  -----------------------------------------------------------

**Compétences** Discrétion +6

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues :** commun, gobelin.

**Défi** 1/4 (50 XP)

***Fuite agile.*** Le Gobelin peut effectuer l'action Se désengager ou
Se cacher comme action bonus à chacun de ses tours.


##### Actions

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts tranchants.

***Arc court.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 80/320 pieds, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.




#### Gorgone

*Grande monstruosité, sans alignement.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 114 (12d10 + 48)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   11 (+0)   18 (+4)   2 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +4

**Condition Immunités** pétrifiées

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** -

**Défi** 5 (1 800 XP)

***Charge piétinante.*** Si la gorgone se déplace d'au moins 6 mètres
en ligne droite vers une créature et la touche avec une attaque gore
dans le même tour, cette cible doit réussir un jet de sauvegarde de
Force DC 16 ou être mise à terre. Si la cible est couchée, la gorgone
peut effectuer une attaque avec ses sabots contre elle en tant
qu'action bonus.


##### Actions

***Gore.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 18 (2d12 + 5) points de dégâts
perforants.

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 16 (2d10 + 5) points de
dégâts contondants.

***Souffle pétrifié (Recharge 5-6).*** La gorgone exhale un gaz
pétrifiant dans un cône de 30 pieds. Chaque créature dans cette zone
doit réussir un jet de sauvegarde de Constitution DC 13. En cas
d'échec, la cible commence à se transformer en pierre et est
immobilisée. La cible entravée doit répéter le jet de sauvegarde à la
fin de son prochain tour. En cas de réussite, l'effet prend fin sur la
cible. En cas d'échec, la cible est pétrifiée jusqu'à ce qu'elle soit
libérée par le sort Restauration *supérieure* ou
une autre magie.




#### Grick

*Monstrosité moyenne, neutre.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 27 (6d8)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   14 (+2)   11 (+0)   3 (-4)    14 (+2)   5 (-3)

  -----------------------------------------------------------

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues** -

**Défi** 2 (450 XP)

***Camouflage en pierre.*** Le Grick a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain rocheux.


##### Actions

***Attaques multiples.*** Le Grick effectue une attaque avec ses
tentacules. Si cette attaque touche, le Grick peut effectuer une attaque
au bec contre la même cible.

***Tentacules.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d6 + 2) points de
dégâts tranchants.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de dégâts
perforants.




#### Griffon

*Grande monstruosité, sans alignement.*

**Classe d'armure** 12

**Points de vie** 59 (7d10 + 21)

**Vitesse** 30 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   2 (-4)    13 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +5

**Sens** Vision dans le noir 60 ft., Perception passive 15

**Langues** -

**Défi** 2 (450 XP)

***Vue perçante.*** Le griffon a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.


##### Actions

***Attaques multiples.*** Le griffon effectue deux attaques : une avec
son bec et une avec ses griffes.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 8 (1d8 + 4) points de dégâts
perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.




#### Torve

*Humanoïde de taille moyenne (Torve), mal neutre.*

**Classe d'armure** 11

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   12 (+1)   12 (+1)   9 (-1)    8 (-1)    6 (-2)

  -----------------------------------------------------------

**Compétences** Athlétisme +5, Perception +3, Discrétion +3

**État** Cécité**Immunités** aveuglées

**Sens** vision aveugle 30 ft. ou 10 ft. si assourdi (aveugle au-delà de
ce rayon), Perception passive 13

**Langues** communes des profondeurs

**Défi** 1/4 (50 XP)

***Cécité des sens.*** Le Torve ne peut pas utiliser sa vision aveugle
lorsqu'il est assourdi et incapable de sentir.

***Ouïe et odorat aiguisés.*** Le Torve a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Camouflage dans la pierre.*** Le Torve a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain rocheux.


##### Actions

***Gourdin à os pointus.*** *Attaque avec une arme de corps-à-corps :*
+5 pour toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d4 + 3)
dégâts contondants plus 2 (1d4) dégâts perforants.




#### Harpie

*Monstruosité moyenne, mal chaotique*

**Classe d'armure** 11

**Points de vie** 38 (7d8 + 7)

**Vitesse** 20 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   13 (+1)   12 (+1)   7 (-2)    10 (+0)   13 (+1)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Roturiers** Communs

**Défi** 1 (200 XP)


##### Actions

***Attaques multiples.*** La harpie effectue deux attaques : une avec
ses griffes et une avec son gourdin.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (2d4 + 1) points de
dégâts tranchants.

***Gourdin.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d4 + 1) points de
dégâts contondants.

***Chant de séduction.*** La harpie chante une mélodie magique. Chaque
humanoïde et géant dans un rayon de 300 pieds de la harpie qui peut
entendre la chanson doit réussir un jet de sauvegarde de Sagesse DC 11
ou être charmé jusqu'à la fin de la chanson. La harpie doit effectuer
une action bonus lors de ses tours suivants pour continuer à chanter.
Elle peut s'arrêter de chanter à tout moment. Le chant prend fin si la
harpie est frappée d'incapacité.

Pendant qu'elle est charmée par la harpie, la cible est frappée
d'incapacité et ignore les chants des autres harpies. Si la cible
charmée se trouve à plus de 1,5 mètre de la harpie, elle doit se
déplacer à son tour vers la harpie par le chemin le plus direct, en
essayant de s'approcher à moins de 1,5 mètre. Elle n'évite pas les
attaques d'opportunité, mais avant de se déplacer en terrain dangereux,
comme de la lave ou une fosse, et chaque fois qu'elle subit des dégâts
d'une source autre que la harpie, la cible peut répéter le jet de
sauvegarde. Une cible charmée peut également répéter le jet de
sauvegarde à la fin de chacun de ses tours. Si le jet de sauvegarde est
réussi, l'effet prend fin sur elle.

Une cible qui réussit à se sauver est immunisée contre le chant de cette
harpie pendant les 24 heures suivantes.




#### Chien de chasse infernal

*Le Fiélon moyen, le mal légitime.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 45 (7d8 + 14)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   12 (+1)   14 (+2)   6 (-2)    13 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +5

**Immunité aux dommages causés par** le feu

**Sens** Vision dans le noir 60 ft., Perception passive 15

**Langues** comprend l'infernal mais ne le parle pas.

**Défi** 3 (700 XP)

***Ouïe et odorat aiguisés.*** Le molosse a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Tactiques de meute.*** Le molosse a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du molosse se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) dégâts
perforants plus 7 (2d6) dégâts de feu.

***Souffle de feu (Recharge 5-6).*** Le molosse exhale du feu dans un
cône de 15 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 12, subissant 21 (6d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Hippogriffe

*Grande monstruosité, sans alignement.*

**Classe d'armure** 11

**Points de vie** 19 (3d10 + 3)

**Vitesse** 40 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   13 (+1)   13 (+1)   2 (-4)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +5

**Sens** passif Perception 15

**Langues** -

**Défi** 1 (200 XP)

***Vue perçante.*** L'hippogriffe a un avantage sur les tests de
Sagesse (Perception) qui reposent sur la vue.


##### Actions

***Attaques multiples.*** L'hippogriffe effectue deux attaques : une
avec son bec et une avec ses griffes.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 8 (1d10 + 3) points de dégâts
perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de
dégâts tranchants.




#### Hobgobelin

*Humanoïde moyen (gobelinoïde), maléfique.*

**Classe d'armure** 18 (cotte de mailles, bouclier)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   12 (+1)   12 (+1)   10 (+0)   10 (+0)   9 (-1)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues :** commun, gobelin.

**Défi** 1/2 (100 XP)

***Avantage martial.*** Une fois par tour, le hobgobelin peut infliger 7
(2d6) points de dégâts supplémentaires à une créature qu'il touche avec
une attaque d'arme si cette créature se trouve à moins de 1,5 mètre
d'un allié du hobgobelin qui n'est pas frappé d'incapacité.


##### Actions

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts tranchants, ou 6 (1d10 + 1) points de dégâts tranchants si
utilisée à deux mains.

***Arc long.*** *Attaque avec une arme à distance :* +3 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts perforants.




#### Homoncule

*Très petite (TP) construction, neutre*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 5 (2d4)

**Vitesse** 20 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   4 (-3)    15 (+2)   11 (+0)   10 (+0)   10 (+0)   7 (-2)

  -----------------------------------------------------------

**Immunité aux dommages** poison

**Immunités d'état :** charmé, empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** comprend les langues de son créateur mais ne peut pas parler

**Défi** 0 (10 XP)

***Lien télépathique.*** Si l'homoncule se trouve sur le même plan
d'existence que son maître, il peut transmettre magiquement ce qu'il
ressent à son maître, et les deux peuvent communiquer par télépathie.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 1 dégât perforant,
et la cible doit réussir un jet de sauvegarde de Constitution DC 10 ou
être empoisonnée pendant 1 minute. Si le jet de sauvegarde échoue par 5
ou plus, la cible est à la place empoisonnée pendant 5 (1d10) minutes et
inconsciente tant qu'elle est empoisonnée de cette façon.




#### Hydre

*Très grande monstruosité, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 172 (15d12 + 75)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   12 (+1)   20 (+5)   2 (-4)    10 (+0)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +6

**Sens** Vision dans le noir 60 ft., Perception passive 16

**Langues** -

**Défi** 8 (3 900 XP)

***Retenir sa respiration.*** L'Hydre peut retenir sa respiration
pendant 1 heure.

***Têtes multiples.*** L'hydre a cinq têtes. Tant qu'elle a plus
d'une tête, l'hydre a un avantage sur les jets de sauvegarde contre
l'aveuglement, le charme, la surdité, l'effroi, l'étourdissement et
le déblocage inconscient.

Chaque fois que l'hydre subit 25 points de dégâts ou plus en un seul
tour, une de ses têtes meurt. Si toutes ses têtes meurent, l'hydre
meurt.

À la fin de son tour, elle fait repousser deux têtes pour chacune de ses
têtes mortes depuis son dernier tour, sauf si elle a subi des dégâts de
feu depuis son dernier tour. L'hydre regagne 10 points de vie pour
chaque tête ainsi repoussée.

***Têtes réactives.*** Pour chaque tête que l'hydre possède au-delà
d'une, elle obtient une réaction supplémentaire qui ne peut être
utilisée que pour des attaques d'opportunité.

***Éveillé.*** Pendant que l'Hydre dort, au moins une de ses têtes est
éveillée.


##### Actions

***Attaques multiples.*** L'Hydre effectue autant d'attaques par
morsure qu'elle a de têtes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 10 (1d10 + 5) points
de dégâts perforants.




#### Traqueur invisible

*Moyennement élémentaire, neutre*

**Classe d'armure** 14

**Points de vie** 104 (16d8 + 32)

**Vitesse** 50 pieds, Vol 50 pieds (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   19 (+4)   14 (+2)   10 (+0)   15 (+2)   11 (+0)

  -----------------------------------------------------------

**Compétences** Perception +8, Discrétion +10

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunités aux dommages** poison

**État Immunités** épuisement, agrippé, paralysé, pétrifié, empoisonné,
couché, entravé, inconscient

**Sens** Vision dans le noir 60 ft., Perception passive 18

**Langues :** aérien, comprend le commun mais ne le parle pas.

**Défi** 6 (2 300 XP)

***Invisibilité.*** Le harceleur est invisible.

***Traqueur sans faille.*** Le traqueur se voit attribuer une carrière
par son invocateur. Le traqueur connaît la direction et la distance de
sa proie tant qu'ils se trouvent sur le même plan d'existence. Le
traqueur connaît également l'emplacement de son invocateur.


##### Actions

***Attaques multiples.*** Le rôdeur effectue deux attaques de type slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de dégâts
contondants.




#### Kobold

*Petit humanoïde (Kobold), maléfique.*

**Classe d'armure** 12

**Points de vie** 5 (2d6 - 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    15 (+2)   9 (-1)    8 (-1)    7 (-2)    8 (-1)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 8

**Langues :** commun, draconique.

**Défi** 1/8 (25 XP)

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, le
kobold a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.

***Tactiques de meute.*** Le Kobold a un avantage sur un jet d'attaque
contre une créature si au moins un des alliés du Kobold est à moins de
1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Dague.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de dégâts
perforants.

***Fronde.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 30/120 pieds, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts contondants.




#### Kraken

*Monstruosité Gargantuesque (titan), mal chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 472 (27d20 + 189)

**Vitesse** 20 ft., nager 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
  30 (+10)   11 (+0)   25 (+7)   22 (+6)   18 (+4)   20 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +17, Dex +7, Con +14, Int +13, Wis +11

**Immunités aux dégâts**: foudre ; contondant, perforant et tranchant
des attaques non magiques.

**Immunités d'état** Effrayé, paralysé

**Sens** vision véritable 120 ft., Perception passive 14

Comprend les**langues** Abysses, Céleste, Infernale et Primordiale mais
ne peut pas parler, Télépathie 120 ft.

**Défi** 23 (50 000 XP)

***Amphibie.*** Le Kraken peut respirer de l'air et de l'eau.

***Liberté de mouvement.*** Le Kraken ignore le terrain difficile, et
les effets magiques ne peuvent pas réduire sa vitesse ou le contraindre
à se déplacer. Il peut dépenser 5 pieds de mouvement pour échapper à des
contraintes non-magiques ou être agrippé.

***Monstre de siège.*** Le Kraken inflige des dégâts doubles aux objets
et aux structures.


##### Actions

***Attaques multiples.*** Le kraken effectue trois attaques de
tentacules, qu'il peut remplacer chacune par une utilisation de
Flingueur.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 23 (3d8 + 10) points de
dégâts perforants. Si la cible est une créature Grand ou plus petite
agrippée par le Kraken, cette créature est avalée, et l'agrippement
prend fin. Pendant qu'elle est avalée, la créature est aveuglée et
entravée, elle bénéficie d'un abri total contre les attaques et autres
effets extérieurs au Kraken, et elle subit 42 (12d6) dégâts d'acide au
début de chaque tour du Kraken.

Si le kraken subit 50 points de dégâts ou plus en un seul tour de la
part d'une créature à l'intérieur, il doit réussir un jet de
sauvegarde de Constitution DC 25 à la fin de ce tour ou régurgiter
toutes les créatures avalées, qui tombent à plat ventre dans un espace
situé à 10 pieds du kraken. Si le kraken meurt, une créature avalée
n'est plus entravée par lui et peut s'échapper du cadavre en utilisant
15 pieds de mouvement, en sortant couchée.

***Tentacule.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 30 pieds, une cible. *Touche :* 20 (3d6 + 10) points
de dégâts contondants, et la cible est agrippée (évasion DC 18).
Jusqu'à la fin de cet agrippement, la cible est entravée. Le kraken
possède dix tentacules, dont chacune peut agripper une cible.

***Lancer.*** Un objet Grand ou plus petit tenu ou une créature agrippée
par le Kraken est lancé jusqu'à 60 pieds dans une direction aléatoire
et mis à terre. Si une cible lancée frappe une surface solide, elle
subit 3 (1d6) points de dégâts de contondance par tranche de 10 pieds où
elle a été lancée. Si la cible est lancée sur une autre créature,
celle-ci doit réussir un jet de sauvegarde de Dextérité DC 18 ou subir
les mêmes dégâts et être mise à plat ventre.

***Tempête de foudre.*** Le kraken crée magiquement trois foudres,
chacune pouvant frapper une cible que le kraken peut voir dans un rayon
de 120 pieds autour de lui. La cible doit effectuer un jet de sauvegarde
de Dextérité DC 23, subissant 22 (4d10) dégâts de foudre en cas
d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le kraken peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le kraken regagne les actions légendaires dépensées au début de son
tour.

***Attaque de tentacules ou Fling.*** Le Kraken effectue une attaque de
tentacules ou utilise son Fling.

***Tempête de foudre (coûte 2 actions).*** Le Kraken utilise Tempête de
foudre.

***Nuage d'encre (coûte 3 actions).*** Lorsqu'il est sous l'eau, le
kraken expulse un nuage d'encre dans un rayon de 60 pieds. Le nuage se
répand dans les coins, et cette zone est fortement obscurcie pour les
créatures autres que le kraken. Chaque créature autre que le kraken qui
termine son tour à cet endroit doit réussir un jet de sauvegarde de
Constitution DC 23, subissant 16 (3d10) dégâts de poison en cas
d'échec, ou la moitié des dégâts en cas de réussite. Un fort courant
disperse le nuage, qui disparaît sinon à la fin du prochain tour du
kraken.




#### Lamie

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 97 (13d10 + 26)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   13 (+1)   15 (+2)   14 (+2)   15 (+2)   16 (+3)

  -----------------------------------------------------------

**Compétences** Supercherie +7, Intuition +4, Discrétion +3

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues** Abysses, Communes

**Défi** 4 (1 100 XP)

***Lanceur de sorts inné.*** La caractéristique innée d'incantation de
la Lamie est le Charisme (sauvegarde contre les sorts DC 13). Elle peut
lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel.

À volonté : *Déguisement de soi* (toute forme
humanoïde), *Image majeure*.

3/jour chacun : *Charme-personne*, *Image
miroir*,
*Suggestion*

1/jour : *Quête*


##### Actions

***Attaques multiples.*** La lamie effectue deux attaques : une avec ses
griffes et une avec sa dague ou son Toucher enivrant.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d10 + 3) points de
dégâts tranchants.

***Dague.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 5 (1d4 + 3) points de dégâts
perforants.

***Intoxicating Touch.*** Attaque avec sort en mêlée : +5 pour toucher,
allonge de 1,5 m, une créature. Touché : La cible est magiquement
maudite pendant 1 heure. Jusqu'à la fin de la malédiction, la cible a
un désavantage aux jets de sauvegarde de Sagesse et à tous les tests de
capacité.




#### Liche

*Mort-vivant moyen, n'importe quel alignement maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 135 (18d8 + 54)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   16 (+3)   16 (+3)   20 (+5)   14 (+2)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +10, Int +12, Wis +9

**Compétences** Arcane +18, Histoire +12, Intuition +9, Perception +9

**Résistances aux dégâts**: froid, foudre, nécrotique.

**Immunités aux dommages**: poison ; contondant, perforant et tranchant
des attaques non magiques.

**Immunités d'état :** charmé, épuisement, effrayé, paralysé,
empoisonné.

**Sens** vision véritable 120 ft., Perception passive 19

**Langues** Commun et jusqu'à cinq autres langues

**Défi** 21 (33 000 XP)

***Résistance légendaire (3/Jour).*** Si la liche échoue à un jet de
sauvegarde, elle peut choisir de le réussir à la place.

***Rajeunissement .*** Si elle possède un phylactère, une liche détruite
acquiert un nouveau corps en 1d10 jours, regagnant tous ses points de
vie et redevenant active. Le nouveau corps apparaît dans un rayon de 1,5
mètre autour du phylactère.

***Incantation.*** La liche est un lanceur de sorts de 18e niveau. Sa
caractéristique d'incantation est l'Intelligence (sauvegarde contre
les sorts DC 20, +12 pour toucher avec les attaques de sorts). La liche
dispose des sorts de magicien suivants préparés :

Tours de magie (à volonté) : *Main de mage*,
*Prestidigitation*, *Rayon de
givre*.

1er niveau (4 emplacements) : *Détection de la magie*,
*Projectile magique*, Vague
*tonnante*

2ème niveau (3 emplacements) : *flèche acide*,
*Détection des pensées*,
*Invisibilité*

3e niveau (3 emplacements) : *animation des morts*,
*contre-sort*, *dissipation de la
magie*.

4ème niveau (3 slots) : *Flétrissement*, *Porte
dimensionnelle*

5e niveau (3 emplacements) : Nuage *mortelle*,
*Scrutation*

6ème niveau (1 emplacement) : *Désintégration*, Globe
d'*invulnérabilité*

7ème niveau (1 emplacement) : Doigt *de mort*,
Changement *de plan*

8ème niveau (1 emplacement) : Domination de
*monstre*
étourdissant

9ème niveau (1 case) : *Mot de pouvoir mortel*

***Résistance à la transformation.*** La liche a un avantage aux jets de
sauvegarde contre tout effet qui transforme les morts-vivants.


##### Actions

***Toucher paralysé.*** Attaque avec sort en mêlée : +12 pour toucher,
portée de 1,5 m, une créature. *Touché :* 10 (3d6) dégâts de froid. La
cible doit réussir un jet de sauvegarde de Constitution DC 18 ou être
paralysée pendant 1 minute. La cible peut répéter le jet de sauvegarde à
la fin de chacun de ses tours, mettant fin à l'effet sur elle-même en
cas de réussite.



##### Actions légendaires

La liche peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
La liche récupère les actions légendaires dépensées au début de son
tour.

***Tour de magie.*** La Liche lance un sort.

***Toucher paralysé (coûte 2 actions).*** La liche utilise son toucher
paralysant.

***Regard effrayé (coûte 2 actions).*** La liche fixe son regard sur une
créature qu'elle peut voir dans un rayon de 3 mètres. La cible doit
réussir un jet de sauvegarde de Sagesse DC 18 contre cette magie ou être
effrayée pendant 1 minute. La cible effrayée peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet sur
elle-même en cas de réussite. Si le jet de sauvegarde d'une cible est
réussi ou si l'effet prend fin pour elle, la cible est immunisée contre
le regard de la liche pendant les 24 heures suivantes.

***Perturber la vie (coûte 3 actions).*** Chaque créature vivante se
trouvant à moins de 6 mètres de la liche doit effectuer un jet de
sauvegarde de Constitution DC 18 contre cette magie, subissant 21 (6d6)
dégâts nécrotiques en cas d'échec, ou la moitié des dégâts en cas de
réussite.




#### Homme-lézard

*Humanoïde moyen (Homme-lézard), neutre*

**Classe d'armure** 15 (armure naturelle, bouclier)

**Points de vie** 22 (4d8 + 4)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   13 (+1)   7 (-2)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4, Survie +5

**Sens** passif Perception 13

**Langues** draconiques

**Défi** 1/2 (100 XP)

***Retenir sa respiration.*** L'Homme-lézard peut retenir son souffle
pendant 15 minutes.


##### Actions

***Attaques multiples.*** L'Homme-lézard effectue deux attaques de
mêlée, chacune avec une arme différente.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Gourdin lourd.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts contondants.

***Javeline.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+4 pour toucher, allonge de 5 pieds ou portée de 30/120 pieds, une
cible. *Touché :* 5 (1d6 + 2) points de dégâts perforants.

***Bouclier à pointes.*** *Attaque avec une arme de corps-à-corps :* +4
pour toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points
de dégâts perforants.




#### Magmatique

*Petit élémentaire, neutre chaotique*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 9 (2d6 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    15 (+2)   12 (+1)   8 (-1)    11 (+0)   10 (+0)

  -----------------------------------------------------------

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunité aux dommages causés par** le feu

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Ardent

**Défi** 1/2 (100 XP)

***Eclatement de la mort.*** Quand le Magmatique meurt, il explose dans
une explosion de feu et de magma. Chaque créature dans un rayon de 3
mètres doit effectuer un jet de sauvegarde de Dextérité à DC 11,
subissant 7 (2d6) dégâts de feu en cas d'échec, ou la moitié des dégâts
en cas de réussite. Les objets inflammables qui ne sont pas portés ou
transportés dans cette zone sont enflammés.

***Illumination enflammée.*** Comme action bonus, le Magmatique peut
s'enflammer ou éteindre ses flammes. Lorsqu'il s'enflamme, le
Magmatique diffuse une lumière vive dans un rayon de 3 mètres et une
lumière chétive sur 3 mètres supplémentaires.


##### Actions

***Toucher.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d6) dégâts de feu.
Si la cible est une créature ou un objet inflammable, elle s'enflamme.
Jusqu'à ce qu'une créature prenne une action pour éteindre le feu, la
cible subit 3 (1d6) dégâts de feu à la fin de chacun de ses tours.




#### Manticore

*Grandes monstruosités, maléfices légitimes.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 68 (8d10 + 24)

**Vitesse** 30 ft., Vol 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   16 (+3)   17 (+3)   7 (-2)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Roturiers** Communs

**Défi** 3 (700 XP)

***Repousse des pointes de la queue.*** La Manticore a vingt-quatre
pointes de queue. Les pointes utilisées repoussent lorsque la Manticore
termine un long repos.


##### Actions

***Attaques multiples.*** La manticore effectue trois attaques : une
avec sa morsure et deux avec ses griffes ou trois avec les pointes de sa
queue.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts tranchants.

***Pointe de la queue.*** *Attaque avec une arme à distance :* +5 pour
toucher, portée de 100/200 pieds, une cible. *Touché :* 7 (1d8 + 3)
points de dégâts perforants.




#### Méduse

*Monstruosité moyenne, mal légitime.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 127 (17d8 + 51)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   15 (+2)   16 (+3)   12 (+1)   13 (+1)   15 (+2)

  -----------------------------------------------------------

**Compétences** Supercherie +5, Intuition +4, Perception +4, Furtivité
+5.

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Roturiers** Communs

**Défi** 6 (2 300 XP)

***Regard pétrifié.*** Quand une créature qui peut voir les yeux de la
méduse commence son tour à moins de 30 pieds de la méduse, celle-ci peut
la forcer à effectuer un jet de sauvegarde de Constitution DC 14 si la
méduse n'est pas frappée d'incapacité et peut voir la créature. Si le
jet de sauvegarde échoue par 5 ou plus, la créature est instantanément
pétrifiée. Sinon, la créature qui échoue au jet de sauvegarde commence à
se transformer en pierre et est immobilisée. La créature entravée doit
répéter le jet de sauvegarde à la fin de son prochain tour, devenant
pétrifiée en cas d'échec ou mettant fin à l'effet en cas de réussite.
La pétrification dure jusqu'à ce que la créature soit libérée par le
sort Restauration *supérieure* ou une autre
magie.

À moins d'être surprise, une créature peut détourner les yeux pour
éviter le jet de sauvegarde au début de son tour. Si la créature fait
cela, elle ne peut pas voir la méduse jusqu'au début de son prochain
tour, où elle peut à nouveau détourner les yeux. Si la créature regarde
la Méduse entre-temps, elle doit immédiatement effectuer le jet de
sauvegarde.

Si la méduse se voit reflétée sur une surface polie à moins de 10 mètres
d'elle et dans une zone de lumière vive, la méduse est, en raison de sa
malédiction, affectée par son propre regard.


##### Actions

***Attaques multiples.*** La méduse effectue soit trois attaques de
mêlée - une avec ses poils de serpent et deux avec son épée courte -
soit deux attaques à distance avec son arc long.

***Poils de serpent.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, portée de 1,5 m, une créature. *Touché :* 4 (1d4 + 2)
dégâts perforants plus 14 4d6) dégâts de poison.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) dégâts
perforants.

***Arc long.*** *Attaque avec une arme à distance :* +5 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 6 (1d8 + 2) dégâts
perforants plus 7 (2d6) dégâts de poison.




#### Homme-poisson

*Humanoïde moyen (homme-poisson), neutre.*

**Classe d'armure** 11

**Points de vie** 11 (2d8 + 2)

**Vitesse** 10 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   13 (+1)   12 (+1)   11 (+0)   11 (+0)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** passif Perception 12

**Langues** aquatique, commune

**Défi** 1/8 (25 XP)

***Amphibie.*** L'Homme-poisson peut respirer l'air et l'eau.


##### Actions

***Lance.*** *Attaque avec une arme à distance ou de mêlée :* +2 pour
toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible. *Touché
:* 3 (1d6) points de dégâts perforants, ou 4 (1d8) points de dégâts
perforants si elle est utilisée à deux mains pour une attaque de mêlée.




#### Merrow

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 45 (6d10 + 12)

**Vitesse** 10 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   10 (+0)   15 (+2)   8 (-1)    10 (+0)   9 (-1)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Abysses, Aquatique

**Défi** 2 (450 XP)

***Amphibie.*** Le Merrow peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** Le merrow effectue deux attaques : une avec sa
morsure et une avec ses griffes ou son harpon.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d8 + 4) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d4 + 4) points de
dégâts tranchants.

***Harpon.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+6 pour toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible.
*Touché :* 11 (2d6 + 4) points de dégâts perforants. Si la cible est une
créature Très grande ou plus petite, elle doit réussir un concours de
Force contre le merrow ou être tirée jusqu'à 20 pieds vers le merrow.




#### Mimique

*Monstrosité moyenne (Métamorphe), neutre.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 58 (9d8 + 18)

**Vitesse** 15 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   12 (+1)   15 (+2)   5 (-3)    13 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Discrétion +5

**Immunité aux dommages causés par** l'acide

**Établissement Immunités** enclines

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** -

**Défi** 2 (450 XP)

***Métamorphe.*** La mimique peut utiliser son action pour se
métamorphoser en un objet ou reprendre sa véritable forme amorphe. Ses
statistiques sont les mêmes dans chaque forme. L'équipement qu'il
porte ou qu'il transporte n'est pas transformé. Il revient à sa
véritable forme s'il meurt.

***Adhésif (forme objet uniquement).*** La mimique adhère à tout ce qui
la touche. Une créature Très grande ou plus petite qui adhère à la
mimique est également agrippée par elle (fuite DC 13). Les jets de
caractéristiques effectués pour échapper à ce grappin sont désavantagés.

***Fausse apparence (forme objet uniquement).*** Bien que la mimique
reste immobile, il est impossible de la distinguer d'un objet
ordinaire.

***Lutteur.*** La mimique a un avantage sur les jets d'attaque contre
toute créature agrippée par elle.


##### Actions

***Pseudopode.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) dégâts
contondants. Si la Mimique est sous forme d'objet, la cible est soumise
à son trait Adhésif.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) dégâts
perforants plus 4 (1d8) dégâts d'acide.




#### Minotaure

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 76 (9d10 + 27)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   11 (+0)   16 (+3)   6 (-2)    16 (+3)   9 (-1)

  -----------------------------------------------------------

**Compétences** Perception +7

**Sens** Vision dans le noir 60 ft., Perception passive 17

**Langues** Abysses

**Défi** 3 (700 XP)

***Charge.*** Si le Minotaure se déplace d'au moins 3 mètres en ligne
droite vers une cible et la touche avec une attaque gore dans le même
tour, la cible subit 9 (2d8) dégâts perforants supplémentaires. Si la
cible est une créature, elle doit réussir un jet de sauvegarde de Force
DC 14 ou être poussée jusqu'à 10 pieds de distance et mise à plat
ventre.

***Rappel labyrinthique.*** Le Minotaure peut se souvenir parfaitement
de tous les chemins qu'il a parcourus.

***Téméraire.*** Au début de son tour, le Minotaure peut obtenir un
avantage sur tous les jets d'attaque avec arme de mêlée qu'il effectue
pendant ce tour, mais les jets d'attaque contre lui ont un avantage
jusqu'au début de son prochain tour.


##### Actions

***Hache à deux mains.*** *Attaque avec une arme de corps-à-corps :* +6
pour toucher, allonge de 1,5 m, une cible. *Touché :* 17 (2d12 + 4)
points de dégâts tranchants.

***Gore.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de dégâts
perforants.




#### Destrier noir

*Grand fiélon, mal neutre.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 68 (8d10 + 24)

**Vitesse** 60 ft., Vol 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   10 (+0)   13 (+1)   15 (+2)

  -----------------------------------------------------------

**Immunité aux dommages causés par** le feu

**Sens** passif Perception 11

Comprend les**langues** abyssales, communes et infernales, mais ne sait
pas parler.

**Défi** 3 (700 XP)

***Confère la Résistance au feu.*** Le cauchemar peut conférer une
résistance aux dégâts de feu à toute personne qui le chevauche.

***Illumination.*** Le Destrier noir diffuse une lumière vive dans un
rayon de 3 mètres et une lumière chétive sur 3 mètres supplémentaires.


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) dégâts de
contondant plus 7 (2d6) dégâts de feu.

***Chevauchée éthérée.*** Le Destrier noir et jusqu'à trois créatures
consentantes situées à moins de 1,5 mètre de lui passent magiquement du
plan matériel au plan éthéré, ou vice versa.




#### Ogre

*Grand géant, mal chaotique*

**Classe d'armure** 11 (cacher l'armure)

**Points de vie** 59 (7d10 + 21)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   8 (-1)    16 (+3)   5 (-3)    7 (-2)    7 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 8

**Langues** commune, géante

**Défi** 2 (450 XP)


##### Actions

***Massue.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts contondants.

***Javeline.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+6 pour toucher, allonge de 5 pieds ou portée de 30/120 pieds, une
cible. *Touché :* 11 (2d6 + 4) points de dégâts perforants.




#### Oni

*Grand géant, maléfique.*

**Classe d'armure** 16 (cotte de mailles)

**Points de vie** 110 (13d10 + 39)

**Vitesse** 30 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   11 (+0)   16 (+3)   14 (+2)   12 (+1)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +6, Wis +4, Cha +5

**Compétences** Arcane +5, Supercherie +8, Perception +4

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** commune, géante

**Défi** 7 (2 900 XP)

***Lanceur de sorts inné.*** La caractéristique innée d'incantation de
l'Oni est le Charisme (sauvegarde contre les sorts DC 13). L'oni peut
lancer de manière innée les sorts suivants, qui ne nécessitent aucune
composante matérielle :

A volonté : *Ténèbres*

1/jour chacun : *Charme-personne*,
*Cône*,
*Sommeil*

***Armes magiques.*** Les armes d'attaque de l'Oni sont magiques.

***Régénération.*** L'Oni regagne 10 points de vie au début de son tour
s'il a au moins 1 point de vie.


##### Actions

***Attaques multiples.*** L'Oni effectue deux attaques, soit avec ses
griffes, soit avec sa coutille.

***Griffe (forme Oni uniquement).*** *Attaque avec une arme de
corps-à-corps :* +7 pour toucher, allonge de 1,5 m, une cible. *Touché
:* 8 (1d8 + 4) points de dégâts tranchants.

***Couteau.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) points
de dégâts tranchants, ou 9 1d10 + 4) points de dégâts tranchants en
forme Petit ou Moyen.

***Changement de forme.*** L'Oni se métamorphose magiquement en un
Petit ou Moyen humanoïde, en un Grand géant, ou reprend sa véritable
forme. A part sa taille, ses statistiques sont les mêmes dans chaque
forme. Le seul équipement qui se transforme est sa coutille, qui
rétrécit pour pouvoir être maniée sous forme humanoïde. Si l'oni meurt,
il reprend sa vraie forme, et son glaive reprend sa taille normale.




#### Orc

*Humanoïde moyen (Orc), maléfique.*

**Classe d'armure** 13 (cacher l'armure)

**Points de vie** 15 (2d8 + 6)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   12 (+1)   16 (+3)   7 (-2)    11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Intimidation +2

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues :** commun, orc.

**Défi** 1/2 (100 XP)

***Agressif.*** Comme action bonus, l'orc peut se déplacer jusqu'à sa
vitesse vers une créature hostile qu'il peut voir.


##### Actions

***Hache à deux mains.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d12 + 3)
points de dégâts tranchants.

***Javeline.*** *Attaque avec une arme de corps-à-corps ou à distance :*
+5 pour toucher, allonge de 5 pieds ou portée de 30/120 pieds, une
cible. *Touché :* 6 (1d6 + 3) points de dégâts perforants.




#### Otyugh

*Grandes aberrations, neutre*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 114 (12d10 + 48)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   11 (+0)   19 (+4)   6 (-2)    13 (+1)   6 (-2)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +7

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** Otyugh

**Défi** 5 (1 800 XP)

***Télépathie limitée.*** L'otyugh peut transmettre magiquement des
messages et des images simples à toute créature située à moins de 120
pieds de lui et capable de comprendre une langue. Cette forme de
télépathie ne permet pas à la créature réceptrice de répondre par
télépathie.


##### Actions

***Attaques multiples.*** L'otyugh effectue trois attaques : une avec
sa morsure et deux avec ses tentacules.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d8 + 3) points de
dégâts perforants. Si la cible est une créature, elle doit réussir un
jet de sauvegarde de Constitution DC 15 contre la maladie ou
s'empoisonner jusqu'à ce que la maladie soit guérie. Toutes les 24
heures qui s'écoulent, la cible doit répéter le jet de sauvegarde,
réduisant son maximum de points de vie de 5 (1d10) en cas d'échec. La
maladie est guérie en cas de réussite. La cible meurt si la maladie
réduit son maximum de points de vie à 0. Cette réduction du maximum de
points de vie de la cible dure jusqu'à la guérison de la maladie.

***Tentacule.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 7 (1d8 + 3) dégâts
contondants plus 4 1d8) dégâts perforants. Si la cible est de taille
moyenne ou plus petite, elle est agrippée (fuite DC 13) et entravée
jusqu'à la fin de l'agrippement. L'otyugh possède deux tentacules,
chacun pouvant agripper une cible.

***Tentacle Slam.*** L'otyugh projette les créatures agrippées par lui
les unes contre les autres ou contre une surface solide. Chaque créature
doit réussir un jet de sauvegarde de Constitution DC 14 ou subir 10
(2d6 + 3) dégâts de contondant et être étourdie jusqu'à la fin du
prochain tour de l'otyugh. En cas de sauvegarde réussie, la cible subit
la moitié des dégâts de matraquage et n'est pas étourdie.




#### Ours-hibou

*Grande monstruosité, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 59 (7d10 + 21)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   12 (+1)   17 (+3)   3 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues** -

**Défi** 3 (700 XP)

***Vue et odorat aiguisés.*** L'Ours-hibou a un avantage sur les tests
de Sagesse (Perception) qui reposent sur la vue ou l'odorat.


##### Actions

***Attaques multiples.*** L'Ours-hibou effectue deux attaques : une
avec son bec et une avec ses griffes.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
portée de 1,5 m, une créature. *Touché :* 10 (1d10 + 5) dégâts
perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d8 + 5) points de
dégâts tranchants.




#### Pégase

*Grand céleste, chaotique bon*

**Classe d'armure** 12

**Points de vie** 59 (7d10 + 21)

**Vitesse** 60 ft., Vol 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   10 (+0)   15 (+2)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Wis +4, Cha +3

**Compétences** Perception +6

**Sens** passif Perception 16

Comprend**le** céleste, le commun, l'elfe et le sylvestre, mais ne sait
pas parler.

**Défi** 2 (450 XP)


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts contondants.




#### Pseudodragon

*Très petite (TP) dragon, neutre bon*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 7 (2d4 + 2)

**Vitesse** 15 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    15 (+2)   13 (+1)   10 (+0)   12 (+1)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 13

Comprend**le** commun et le draconique mais ne sait pas parler.

**Défi** 1/4 (50 XP)

***Sens aiguisés.*** Le Pseudodragon a un avantage sur les tests de
Sagesse (Perception) qui reposent sur la vue, l'ouïe ou l'odorat.

***Résistance à la magie.*** Le Pseudodragon a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Télépathie limitée.*** Le Pseudodragon peut communiquer magiquement
des idées, des émotions et des images simples par télépathie avec toute
créature située à moins de 100 pieds de lui et capable de comprendre une
langue.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts perforants.

***Piquer.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 4 (1d4 + 2) dégâts
perforants, et la cible doit réussir un jet de sauvegarde de
Constitution DC 11 ou être empoisonnée pendant 1 heure. Si le jet de
sauvegarde échoue par 5 ou plus, la cible tombe inconsciente pour la
même durée, ou jusqu'à ce qu'elle subisse des dégâts ou qu'une autre
créature utilise une action pour la réveiller.




#### Ver pourpre

*Monstruosité Gargantuesque, sans alignement.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 247 (15d20 + 90)

**Vitesse** 50 pieds, terrier 30 pieds.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   28 (+9)   7 (-2)    22 (+6)   1 (-5)    8 (-1)    4 (-3)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +11, Wis +4

**Sens** vision aveugle 30 pieds, sens des secousses 60 pieds,
Perception passive 9

**Langues** -

**Défi** 15 (13 000 XP)

***Tunnelier.*** Le ver peut creuser à travers la roche solide à la
moitié de sa vitesse de creusement et laisse un tunnel de 10 pieds de
diamètre dans son sillage.


##### Actions

***Attaques multiples.*** Le ver effectue deux attaques : une avec sa
morsure et une avec son dard.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 22 (3d8 + 9) points
de dégâts perforants. Si la cible est une créature Grand ou plus petite,
elle doit réussir un jet de sauvegarde de Dextérité DC 19 ou être avalée
par le ver. Une créature avalée est aveuglée et entravée, elle bénéficie
d'un abri total contre les attaques et autres effets extérieurs au ver,
et elle subit 21 (6d6) dégâts d'acide au début de chaque tour du ver.

Si le ver subit 30 points de dégâts ou plus en un seul tour de la part
d'une créature qui se trouve à l'intérieur, il doit réussir un jet de
sauvegarde de Constitution DC 21 à la fin de ce tour ou régurgiter
toutes les créatures avalées, qui tombent à plat ventre dans un espace
situé à 3 mètres du ver. Si le ver meurt, une créature avalée n'est
plus entravée par lui et peut s'échapper du cadavre en utilisant 20
pieds de mouvement, en sortant couchée.

***Dard de queue.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 19 (3d6 + 9)
dégâts perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 19, subissant 42 (12d6) dégâts de poison en cas
d'échec, ou la moitié des dégâts en cas de réussite.




#### Rakshasa

*Le Fiélon moyen, le mal légitime.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 110 (13d8 + 52)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   17 (+3)   18 (+4)   13 (+1)   16 (+3)   20 (+5)

  -----------------------------------------------------------

**Compétences** Supercherie +10, Intuition +8

**Vulnérabilité aux dégâts** perforants des armes magiques maniées par
de bonnes créatures.

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues :** commun, infernal.

**Défi** 13 (10 000 XP)

***Immunité limitée à la magie.*** Le rakshasa ne peut pas être affecté
ou détecté par des sorts de 6e niveau ou moins, sauf s'il le souhaite.
Il a un avantage aux jets de sauvegarde contre tous les autres sorts et
effets magiques.

***Lanceur de sorts inné.*** La caractéristique d'incantation innée du
rakshasa est le Charisme (sauvegarde contre les sorts DC 18, +10 pour
toucher avec les attaques de sorts). Le rakshasa peut lancer de manière
innée les sorts suivants, qui ne nécessitent aucune composante
matérielle :

À volonté : *Détection des pensées*,
*Déguisement*,
*Illusion mineure*.

3/jour chacun : *Charme-personne*, *détection de la
magie*, *image
majeure*

1/jour chacun : Domination de *personne*,
*Vol*, *Vision
suprême*


##### Actions

***Attaques multiples.*** Le Rakshasa effectue deux attaques de griffes.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d6 + 2) dégâts
tranchants, et la cible est maudite si c'est une créature. La
malédiction magique prend effet à chaque fois que la cible prend un
repos court ou long, remplissant les pensées de la cible d'images et de
rêves horribles. La cible maudite ne tire aucun avantage de la fin d'un
repos court ou long. La malédiction dure jusqu'à ce qu'elle soit levée
par un sort de *suppression de malédiction* ou une
magie similaire.




#### Remorhaz

*Très grande monstruosité, sans alignement.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 195 (17d12 + 85)

**Vitesse** 30 ft., terrier 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   24 (+7)   13 (+1)   21 (+5)   4 (-3)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Immunités aux dégâts**: froid, feu.

**Sens** Vision dans le noir 60 pieds, sens des secousses 60 pieds,
Perception passive 10

**Langues** -

**Défi** 11 (7 200 XP)

***Corps chauffé.*** Une créature qui touche le Remorhaz ou le frappe
avec une attaque de mêlée alors qu'elle se trouve à moins de 1,5 m de
lui subit 10 (3d6) dégâts de feu.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 40 (6d10 + 7) dégâts
perforants plus 10 3d6) dégâts de feu. Si la cible est une créature,
elle est agrippée (fuite DC 17). Jusqu'à la fin de cet agrippement, la
cible est entravée, et le Remorhaz ne peut pas mordre une autre cible.

***Avaler.*** Le Remorhaz effectue une attaque de morsure contre une
créature de taille moyenne ou plus petite qu'il agrippe. Si l'attaque
touche, la créature subit les dégâts de la morsure et est avalée, et
l'agrippement prend fin. Pendant qu'elle est avalée, la créature est
aveuglée et entravée, elle bénéficie d'un abri total contre les
attaques et autres effets extérieurs au Remorhaz, et elle subit 21 (6d6)
dégâts d'acide au début de chaque tour du Remorhaz.

Si le remorhaz subit 30 points de dégâts ou plus en un seul tour de la
part d'une créature qui se trouve à l'intérieur, il doit réussir un
jet de sauvegarde de Constitution DC 15 à la fin de ce tour ou
régurgiter toutes les créatures avalées, qui tombent à plat ventre dans
un espace situé à 10 pieds du remorhaz. Si le remorhaz meurt, une
créature avalée n'est plus entravée par lui et peut s'échapper du
cadavre en utilisant 15 pieds de mouvement, en sortant couchée.




#### Roc

*Monstruosité Gargantuesque, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 248 (16d20 + 80)

**Vitesse** 20 ft., Vol 120 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   28 (+9)   10 (+0)   20 (+5)   3 (-4)    10 (+0)   9 (-1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +9, Wis +4, Cha +3

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 11 (7 200 XP)

***Vue perçante.*** Le roc a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.


##### Actions

***Attaques multiples.*** Le roc effectue deux attaques : une avec son
bec et une avec ses serres.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +13 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 27 (4d8 + 9) points de dégâts
perforants.

***Talons.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 23 (4d6 + 9) points de
dégâts tranchants, et la cible est agrippée (évasion DC 19). Jusqu'à la
fin de cet agrippement, la cible est entravée, et le roc ne peut pas
utiliser ses serres sur une autre cible.




#### Enlaceur

*Grande monstruosité, mal neutre.*

**Classe d'armure** 20 (armure naturelle)

**Points de vie** 93 (11d10 + 33)

**Vitesse** 10 ft., montée 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   8 (-1)    17 (+3)   7 (-2)    16 (+3)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +6, Discrétion +5

**Sens** Vision dans le noir 60 ft., Perception passive 16

**Langues** -

**Défi** 5 (1 800 XP)

***Fausse apparence.*** Bien que l'Enlaceur reste immobile, il est
impossible de le distinguer d'une formation caverneuse normale, telle
qu'une stalagmite.

***Tendrilles de préhension.*** L'Enlaceur peut avoir jusqu'à six
vrilles à la fois. Chaque vrille peut être attaquée (CA 20 ; 10 points
de vie ; immunité au poison et aux dégâts psychiques). Détruire une
vrille n'inflige aucun dégât à l'Enlaceur, qui peut extruder une
vrille de remplacement à son tour suivant. Une vrille peut également
être brisée si une créature entreprend une action et réussit un test de
Force DC 15 contre elle.

***Pattes d'araignée.*** L'Enlaceur peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.


##### Actions

***Attaques multiples.*** L'Enlaceur effectue quatre attaques avec ses
vrilles, utilise Moulinet, et effectue une attaque avec sa morsure.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 22 (4d8 + 4) points de
dégâts perforants.

***Tendril.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 15 m, une créature. Touché : La cible est agrippée
(fuite DC 15). Jusqu'à la fin de l'agrippement, la cible est entravée
et a un désavantage aux tests de Force et aux jets de sauvegarde de
Force, et l'Enlaceur ne peut pas utiliser la même vrille sur une autre
cible.

***Moulinet.*** L'Enlaceur tire chaque créature agrippée par lui
jusqu'à 25 pieds droit vers lui.




#### Oxydeur

*Monstruosité moyenne, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 27 (5d8 + 5)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   12 (+1)   13 (+1)   2 (-4)    13 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** -

**Défi** 1/2 (100 XP)

***Odeur de fer.*** Le monstre Oxydeur peut localiser, à l'odeur,
l'emplacement du métal ferreux dans un rayon de 30 pieds autour de lui.

***Métal rouillé.*** Toute arme non magique en métal qui touche le
monstre Oxydeur se corrode. Après avoir infligé des dégâts, l'arme
subit une pénalité permanente et cumulative de -1 aux jets de dégâts. Si
sa pénalité tombe à -5, l'arme est détruite. Les munitions non magiques
en métal qui touchent le monstre de la rouille sont détruites après
avoir infligé des dégâts.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts perforants.

***Antennes.*** Le monstre Oxydeur corrode un objet en métal ferreux non
magique qu'il peut voir dans un rayon de 1,5 mètre autour de lui. Si
l'objet n'est pas porté ou transporté, le toucher détruit un cube de 1
pied de celui-ci. Si l'objet est porté par une créature, celle-ci peut
effectuer un jet de sauvegarde de Dextérité à DC 11 pour éviter le
contact du monstre de la rouille.

Si l'objet touché est une armure ou un bouclier en métal porté ou
transporté, il subit une pénalité permanente et cumulative de -1 à la CA
qu'il offre. Une armure ramenée à une CA de 10 ou un bouclier ramené à
un bonus de +0 est détruit. Si l'objet touché est une arme en métal
tenue en main, il rouille comme décrit dans le trait Rouille du métal.




#### Sahuagin

*Humanoïde moyen (Sahuagin), maléfique.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 22 (4d8 + 4)

**Vitesse** 30 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   11 (+0)   12 (+1)   12 (+1)   13 (+1)   9 (-1)

  -----------------------------------------------------------

**Compétences** Perception +5

**Sens** Vision dans le noir 120 ft., Perception passive 15

**Langues** Sahuagin

**Défi** 1/2 (100 XP)

***Frénésie sanguinaire.*** Le sahuagin a un avantage aux jets
d'attaque de mêlée contre toute créature qui n'a pas tous ses points
de vie.

***Amphibie limité.*** Le sahuagin peut respirer de l'air et de l'eau,
mais il doit être immergé au moins une fois toutes les 4 heures pour ne
pas suffoquer.

***Télépathie des requins.*** Le Sahuagin peut donner des ordres
magiques à n'importe quel requin se trouvant à moins de 120 pieds de
lui, en utilisant une télépathie limitée.


##### Actions

***Attaques multiples.*** Le sahuagin effectue deux attaques de mêlée :
une avec sa morsure et une avec ses griffes ou sa lance.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d4 + 1) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d4 + 1) dégâts
tranchants.

***Lance.*** *Attaque avec une arme à distance ou de mêlée :* +3 pour
toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible. *Touché
:* 4 (1d6 + 1) points de dégâts perforants, ou 5 (1d8 + 1) points de
dégâts perforants si elle est utilisée à deux mains pour une attaque de
mêlée.




#### Salamandre

*Grand élémentaire, mal neutre*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 90 (12d10 + 24)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   14 (+2)   15 (+2)   11 (+0)   10 (+0)   12 (+1)

  -----------------------------------------------------------

**Vulnérabilité aux dégâts** à froid

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunité aux dommages causés par** le feu

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Ardent

**Défi** 5 (1 800 XP)

***Corps chauffé.*** Une créature qui touche la salamandre ou la frappe
avec une attaque de mêlée alors qu'elle se trouve à moins de 1,5 m
d'elle subit 7 (2d6) dégâts de feu.

***Armes chauffées.*** Toute arme de mêlée en métal brandie par la
Salamandre inflige 3 (1d6) points de dégâts de feu supplémentaires en
cas de succès (inclus dans l'attaque).


##### Actions

***Attaques multiples.*** La Salamandre effectue deux attaques : une
avec sa lance et une avec sa queue.

***Lance.*** *Attaque avec une arme à distance ou de mêlée :* +7 pour
toucher, allonge de 5 pieds ou portée de 20 pieds/60 pieds, une cible.
*Touché :* 11 2d6 + 4) points de dégâts perforants, ou 13 (2d8 + 4)
points de dégâts perforants si elle est utilisée à deux mains pour une
attaque de mêlée, plus 3 (1d6) points de dégâts de feu.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 10 pieds, une cible. *Touche :* 11 (2d6 + 4) dégâts de
contondant plus 7 2d6) dégâts de feu, et la cible est agrippée (évasion
DC 14). Jusqu'à la fin de cet agrippement, la cible est entravée, la
salamandre peut automatiquement la frapper avec sa queue, et la
salamandre ne peut pas effectuer d'attaques de queue contre d'autres
cibles.




#### Satyre

*Fée moyenne, neutre chaotique.*

**Classe d'armure** 14 (armure en cuir)

**Points de vie** 31 (7d8)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   16 (+3)   11 (+0)   12 (+1)   10 (+0)   14 (+2)

  -----------------------------------------------------------

**Compétences** Perception +2, Représentation +6, Discrétion +5

**Sens** passif Perception 12

**Langues :** commun, elfe, sylvestre.

**Défi** 1/2 (100 XP)

***Résistance à la magie.*** Le satyre a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 6 (2d4 + 1) points de dégâts
contondants.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts perforants.

***Arc court.*** *Attaque avec une arme à distance :* +5 pour toucher,
portée de 80/320 pieds, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts perforants.




#### Ombre

*Mort-vivant moyen, maléfique chaotique.*

**Classe d'armure** 12

**Points de vie** 16 (3d8 + 3)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    14 (+2)   13 (+1)   6 (-2)    10 (+0)   8 (-1)

  -----------------------------------------------------------

**Compétences** Discrétion +4 (+6 en lumière chétive ou dans les
Ténèbres)

**Vulnérabilité aux dégâts** radiant

**Résistances aux dégâts**: acide, froid, feu, foudre, tonnerre ;
contondant, perforant et tranchant des attaques non magiques.

**Immunités aux dommages :** nécrotique, poison.

**État Immunités** épuisement, effrayé, agrippé, paralysé, pétrifié,
empoisonné, empoisonné, empoisonné, retenu

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** -

**Défi** 1/2 (100 XP)

***Amorphe.*** L'ombre peut se déplacer dans un espace aussi étroit
qu'un pouce de large sans être comprimée.

***Discrétion de l'Ombre.*** Lorsqu'elle se trouve dans une lumière
chétive ou dans les ténèbres, l'ombre peut effectuer l'action Se
cacher comme action bonus.

***Faiblesse au soleil.*** À la lumière du soleil, l'ombre a un
désavantage aux jets d'attaque, aux tests d'aptitude et aux jets de
sauvegarde.


##### Actions

***Drain de Force.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 9 (2d6 + 2) dégâts
nécrotiques, et le score de Force de la cible est réduit de 1d4. La
cible meurt si cela réduit sa Force à 0. Sinon, la réduction dure
jusqu'à ce que la cible termine un repos court ou long.

Si un humanoïde non maléfique meurt de cette attaque, une nouvelle ombre
se lève de son cadavre 1d4 heures plus tard.




#### Tertre errant

*Grande plante, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 136 (16d10 + 48)

**Vitesse** 20 ft., nager 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   8 (-1)    16 (+3)   5 (-3)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Discrétion +2

**Résistances aux dégâts** froid, feu

**Immunité aux dégâts** de la foudre

**États Immunités** aveuglé, assourdi, épuisement

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 10

**Langues** -

**Défi** 5 (1 800 XP)

***Absorption de la foudre.*** Chaque fois que le tertre errant subit
des dégâts de foudre, il ne subit aucun dégât et regagne un nombre de
points de vie égal aux dégâts de foudre infligés.


##### Actions

***Attaques multiples.*** Le Tertre errant effectue deux attaques de
type slam. Si les deux attaques touchent une cible de taille moyenne ou
plus petite, la cible est agrippée (échappatoire DC 14), et le Tertre
errant utilise son Engloutissement sur elle.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de dégâts
contondants.

***Engloutir.*** Le tertre errant engloutit une créature de taille
moyenne ou plus petite agrippée par lui. La cible engloutie est
aveuglée, entravée et incapable de respirer, et elle doit réussir un jet
de sauvegarde de Constitution DC 14 au début de chaque tour du monticule
ou subir 13 (2d8 + 4) dégâts de contondant. Si le monticule se déplace,
la cible engloutie se déplace avec lui. Le monticule ne peut avoir
qu'une seule créature engloutie à la fois.




#### Bouclier Gardien

*Grande construction, sans alignement.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 142 (15d10 + 60)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   8 (-1)    18 (+4)   7 (-2)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 10

**Immunité aux dommages** poison

**Immunités d'état :** charmé, épuisement, effrayé, paralysé,
empoisonné.

**Injonction** comprend les ordres donnés dans n'importe quelle langue
mais ne peut pas parler.

**Défi** 7 (2 900 XP)

***Lié.*** Le Bouclier Gardien est magiquement lié à une amulette. Tant
que le gardien et son amulette se trouvent sur le même plan
d'existence, le porteur de l'amulette peut appeler télépathiquement le
gardien pour qu'il s'y rende, et le gardien connaît la distance et la
direction de l'amulette. Si le gardien se trouve à moins de 60 pieds du
porteur de l'amulette, la moitié des dégâts subis par le porteur
(arrondis au supérieur) est transférée au gardien.

***Régénération.*** Le Bouclier Gardien regagne 10 points de vie au
début de son tour s'il a au moins 1 point de vie.

***Stockage de sorts.*** Un lanceur de sorts qui porte l'amulette du
Bouclier Gardien peut faire en sorte que le gardien stocke un sort de
4ème niveau ou moins. Pour ce faire, le porteur doit lancer le sort sur
le gardien. Le sort n'a aucun effet mais est stocké dans le gardien.
Sur injonction du porteur ou en cas de situation prédéfinie par le
lanceur de sorts, le gardien lance le sort stocké avec les paramètres
définis par le lanceur d'origine, sans nécessiter de composantes.
Lorsque le sort est lancé ou qu'un nouveau sort est stocké, tout sort
précédemment stocké est perdu.


##### Actions

***Attaques multiples.*** Le gardien effectue deux attaques de poing.

***Poing.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de dégâts
contondants.



##### Réactions

***Bouclier.*** Lorsqu'une créature effectue une attaque contre le
porteur de l'amulette du gardien, ce dernier accorde un bonus de +2 à
la CA du porteur si le gardien se trouve à moins de 1,5 mètre de
celui-ci.




#### Spectre

*Mort-vivant moyen, maléfique chaotique.*

**Classe d'armure** 12

**Points de vie** 22 (5d8)

**Vitesse** 0 ft, Vol 50 ft (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   1 (-5)    14 (+2)   11 (+0)   10 (+0)   10 (+0)   11 (+0)

  -----------------------------------------------------------

**Résistances aux dégâts**: acide, froid, feu, foudre, tonnerre ;
contondant, perforant et tranchant des attaques non magiques.

**Immunités aux dommages :** nécrotique, poison.

**Condition Immunités :** charmé, épuisement, agrippé, paralysé,
pétrifié, empoisonné, couché, empoisonné, inconscient.

**Sens** Vision dans le noir 60 ft., Perception passive 10

Les**langues** comprennent toutes les langues qu'il a connues dans la
vie mais qu'il ne peut pas parler.

**Défi** 1 (200 XP)

***Mouvement Incorporel.*** Le spectre peut se déplacer à travers
d'autres créatures et objets comme s'il s'agissait d'un terrain
difficile. Il subit 5 (1d10) dégâts de force s'il termine son tour à
l'intérieur d'un objet.

***Sensibilité à la lumière du Soleil.*** Lorsqu'il est à la lumière du
soleil, le spectre a un désavantage aux jets d'attaque, ainsi qu'aux
tests de Sagesse (Perception) qui reposent sur la vue.


##### Actions

***Drainage de vie.*** Attaque avec sort en mêlée : +4 aux coups, portée
de 1,5 m, une créature. *Touché :* 10 (3d6) dégâts nécrotiques. La cible
doit réussir un jet de sauvegarde de Constitution DC 10 ou son maximum
de points de vie est réduit d'un montant égal aux dégâts subis. Cette
réduction dure jusqu'à ce que la créature termine un long repos. La
cible meurt si cet effet réduit son maximum de points de vie à 0.




#### Esprit follet

*Très petite (TP) fée, neutre bon*

**Classe d'armure** 15 (armure en cuir)

**Points de vie** 2 (1d4)

**Vitesse** 10 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    18 (+4)   10 (+0)   14 (+2)   13 (+1)   11 (+0)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +8

**Sens** passif Perception 13

**Langues :** commun, elfe, sylvestre.

**Défi** 1/4 (50 XP)


##### Actions

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât tranchant.

***Arc court.*** *Attaque avec une arme à distance :* +6 pour toucher,
portée de 40/160 pieds, une cible. *Touché :* 1 dégât perforant, et la
cible doit réussir un jet de sauvegarde de Constitution DC 10 ou être
empoisonnée pendant 1 minute. Si le résultat de son jet de sauvegarde
est inférieur ou égal à 5, la cible empoisonnée tombe inconsciente pour
la même durée, ou jusqu'à ce qu'elle subisse des dégâts ou qu'une
autre créature fasse une action pour la réveiller.

***Vue du cœur.*** L'Esprit follet touche une créature et connaît
magiquement son état émotionnel actuel.

Si la cible échoue à un jet de sauvegarde de Charisme DC 10, l'Esprit
follet connaît également l'alignement de la créature. Les célestes, les
fiélons et les morts-vivants échouent automatiquement au jet de
sauvegarde.

***Invisibilité.*** L'Esprit follet devient magiquement invisible
jusqu'à ce qu'il attaque ou lance un sort, ou jusqu'à la fin de sa
concentration (comme s'il se concentrait sur un sort). Tout équipement
que l'Esprit follet porte ou transporte est invisible avec lui.




#### Strige

*Très petite (TP), sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 2 (1d4)

**Vitesse** 10 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   4 (-3)    16 (+3)   11 (+0)   2 (-4)    8 (-1)    6 (-2)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** -

**Défi** 1/8 (25 XP)


##### Actions

***Drainage de sang.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, portée de 1,5 m, une créature. *Touché :* 5 (1d4 + 3)
dégâts perforants, et le Strige s'attache à la cible. Tant qu'il est
attaché, le Strige n'attaque pas. Au lieu de cela, au début de chacun
des tours du Strige, la cible perd 5 (1d4 + 3) points de vie à cause de
la perte de sang.

Le Strige peut se détacher en dépensant 5 pieds de son mouvement. Il le
fait après avoir drainé 10 points de vie de sang de la cible ou si la
cible meurt. Une créature, y compris la cible, peut utiliser son action
pour détacher le Strige.




#### Succube/Incube

*Fiélon de taille moyenne (Métamorphe), mal neutre.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 66 (12d8 + 12)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    17 (+3)   13 (+1)   15 (+2)   12 (+1)   20 (+5)

  -----------------------------------------------------------

**Compétences** Supercherie +9, Intuition +5, Perception +5, Persuasion
+9, Furtivité +7.

**Résistances aux dégâts**: froid, feu, foudre, poison ; contondant,
perforant et tranchant des attaques non magiques.

**Sens** Vision dans le noir 60 ft., Perception passive 15

**Langues** Abyssales, Commun, Infernal, Télépathie 60 ft.

**Défi** 4 (1 100 XP)

***Lien Télépathique.*** Le fiélon ignore la restriction de portée de sa
télépathie lorsqu'il communique avec une créature qu'il a charmée. Il
n'est même pas nécessaire que les deux soient sur le même plan
d'existence.

***Métamorphe.*** Le fiélon peut utiliser son action pour se
métamorphoser en un humanoïde de taille petite ou moyenne, ou reprendre
sa forme véritable. Sans ailes, le fiélon perd sa vitesse de vol. En
dehors de sa taille et de sa vitesse, ses statistiques sont les mêmes
dans chaque forme. L'équipement qu'il porte n'est pas transformé. Il
reprend sa vraie forme s'il meurt.


##### Actions

***Griffe (forme fiélon uniquement).*** *Attaque avec une arme de
corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible. *Touché
:* 6 (1d6 + 3) points de dégâts tranchants.

***Charme.*** Un humanoïde que le fiélon peut voir à moins de 30 pieds
de lui doit réussir un jet de sauvegarde de Sagesse DC 15 ou être charmé
magiquement pendant 1 jour. La cible charmée obéit aux ordres verbaux ou
télépathiques du fiélon. Si la cible subit une quelconque contamination
ou reçoit un ordre suicidaire, elle peut répéter le jet de sauvegarde,
mettant fin à l'effet en cas de réussite. Si la cible réussit son jet
de sauvegarde contre l'effet, ou si l'effet sur elle prend fin, la
cible est immunisée contre le charme de ce fiélon pour les 24 heures
suivantes.

Le fiélon ne peut avoir qu'une seule cible charmée à la fois. S'il en
charme une autre, l'effet sur la cible précédente prend fin.

***Baiser Drainant.*** Le fiélon embrasse une créature charmée par lui
ou une créature consentante. La cible doit effectuer un jet de
sauvegarde de Constitution DC 15 contre cette magie, subissant 32
(5d10 + 5) dégâts psychiques en cas d'échec, ou la moitié des dégâts en
cas de réussite. Le maximum de points de vie de la cible est réduit
d'un montant égal aux dégâts subis. Cette réduction dure jusqu'à ce
que la cible termine un long repos. La cible meurt si cet effet réduit
son maximum de points de vie à 0.

***Forme éthérée.*** Le fiélon pénètre magiquement dans le plan éthéré
depuis le plan matériel, ou vice versa.




#### Tarasque

*Monstruosité Gargantuesque (titan), sans alignement.*

**Classe d'armure** 25 (armure naturelle)

**Points de vie** 676 (33d20 + 330)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
  30 (+10)   11 (+0)  30 (+10)   3 (-4)    11 (+0)   11 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +5, Wis +9, Cha +9

**Immunités aux dégâts**: feu, poison ; contondant, perforant et
tranchant des attaques non magiques.

**Immunités d'état :** charmé, effrayé, paralysé, empoisonné.

**Sens** vision aveugle 120 ft., Perception passive 10

**Langues** -

**Défi** 30 (155 000 XP)

***Résistance légendaire (3/Jour).*** Si la tarasque échoue à un jet de
sauvegarde, elle peut choisir de le réussir à la place.

***Résistance à la magie.*** La tarasque a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Carapace réfléchissante.*** Chaque fois que la tarasque est visée par
un sort de *projectile magique*, un sort de ligne ou
un sort qui nécessite un jet d'attaque à distance, lancez un d6. Sur un
résultat de 1 à 5, la tarasque n'est pas affectée. Sur un 6, la
tarasque n'est pas affectée, et l'effet est renvoyé au lanceur de
sorts comme s'il provenait de la tarasque, transformant le lanceur de
sorts en cible.

***Monstre de siège.*** La tarasque inflige des dégâts doubles aux
objets et aux structures.


##### Actions

***Attaques multiples.*** La tarasque peut utiliser sa Présence
effrayante. Il effectue alors cinq attaques : une avec sa morsure, deux
avec ses griffes, une avec ses cornes, et une avec sa queue. Il peut
utiliser son Hirondelle à la place de sa morsure.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +19 pour
toucher, allonge de 10 ft, une cible. *Touché :* 36 (4d12 + 10) points
de dégâts perforants. Si la cible est une créature, elle est agrippée
(fuite DC 20). Jusqu'à la fin de cet agrippement, la cible est
entravée, et la tarasque ne peut pas mordre une autre cible.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +19 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 28 (4d8 + 10) points
de dégâts tranchants.

***Cornes.*** *Attaque avec une arme de corps-à-corps :* +19 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 32 (4d10 + 10)
points de dégâts perforants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +19 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 24 (4d6 + 10) points
de dégâts contondants. Si la cible est une créature, elle doit réussir
un jet de sauvegarde de Force DC 20 ou être mise au sol.

***Présence effrayante.*** Chaque créature du choix de la tarasque se
trouvant à moins de 120 pieds d'elle et consciente de sa présence doit
réussir un jet de sauvegarde de Sagesse DC 17 ou être effrayée pendant 1
minute. Une créature peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, avec un désavantage si la tarasque est en ligne de
vue, mettant fin à l'effet sur elle-même en cas de réussite. Si le jet
de sauvegarde d'une créature est réussi ou si l'effet prend fin pour
elle, la créature est immunisée contre la présence effrayante de la
tarasque pendant les 24 heures suivantes.

***Avaler.*** La Tarasque effectue une attaque de morsure contre une
créature Grand ou plus petite qu'elle est agrippée. Si l'attaque
touche, la cible subit les dégâts de la morsure, elle est avalée et
l'agrippement prend fin. Pendant qu'elle est avalée, la créature est
aveuglée et entravée, elle bénéficie d'un abri total contre les
attaques et autres effets extérieurs à la Tarasque, et elle subit 56
(16d6) dégâts d'acide au début de chaque tour de la Tarasque.

Si la tarasque subit 60 points de dégâts ou plus en un seul tour de la
part d'une créature à l'intérieur, elle doit réussir un jet de
sauvegarde de Constitution DC 20 à la fin de ce tour ou régurgiter
toutes les créatures avalées, qui tombent à plat ventre dans un espace
situé à 10 pieds de la tarasque. Si la tarasque meurt, une créature
avalée n'est plus retenue par elle et peut s'échapper du cadavre en
utilisant 30 pieds de mouvement, en sortant couchée.



##### Actions légendaires

La tarasque peut effectuer 3 actions légendaires, en choisissant parmi
les options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le tarasque regagne les actions légendaires dépensées au début de son
tour.

***Attaque.*** Le tarasque effectue une attaque de griffes ou une
attaque de queue.

***Déplacement.*** La Tarasque se déplace jusqu'à la moitié de sa
vitesse.

***Mordre (coûte 2 actions).*** La Tarasque effectue une attaque de
morsure ou utilise son Avaloir.




#### Sylvanide

*Très grande plante, chaotique bon*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 138 (12d12 + 60)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   8 (-1)    21 (+5)   12 (+1)   16 (+3)   12 (+1)

  -----------------------------------------------------------

**Résistance aux dégâts**: contondant, perforant.

Feu sur les**vulnérabilités aux dégâts**

**Sens** passif Perception 13

**Langues** communes, druidique, elfe, sylvestre.

**Défi** 9 (5 000 XP)

***Fausse apparence.*** Lorsque le Sylvanide reste immobile, il est
impossible de le distinguer d'un arbre normal.

***Monstre de siège.*** Le Sylvanide inflige des dégâts doubles aux
objets et aux structures.


##### Actions

***Attaques multiples.*** Le Sylvanide effectue deux attaques de type
slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +10 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 16 (3d6 + 6) points de dégâts
contondants.

***Rock.*** *Attaque avec une arme à distance :* +10 pour toucher,
portée de 60/180 pieds, une cible. *Touché :* 28 (4d10 + 6) points de
dégâts contondants.

***Animer les arbres (1/Jour).*** Le Sylvanide anime magiquement un ou
deux arbres qu'il peut voir dans un rayon de 60 pieds autour de lui.
Ces arbres ont les mêmes statistiques qu'un Sylvanide, sauf qu'ils ont
des scores d'Intelligence et de Charisme de 1, qu'ils ne peuvent pas
parler et qu'ils ne disposent que de l'option d'action Claque. Un
arbre animé agit comme un allié du Sylvanide. L'arbre reste animé
pendant 1 jour ou jusqu'à ce qu'il meure ; jusqu'à ce que le
Sylvanide meure ou se trouve à plus de 120 pieds de l'arbre ; ou
jusqu'à ce que le Sylvanide fasse une action bonus pour le
retransformer en arbre inanimé. L'arbre prend alors racine si possible.




#### Troll

*Grand géant, mal chaotique*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 84 (8d10 + 40)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   13 (+1)   20 (+5)   7 (-2)    9 (-1)    7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues** Géant

**Défi** 5 (1 800 XP)

***Odorat aiguisé.*** Le troll a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Régénération.*** Le troll regagne 10 points de vie au début de son
tour. Si le troll subit des dégâts d'acide ou de feu, ce trait ne
fonctionne pas au début du prochain tour du troll. Le troll ne meurt que
s'il commence son tour avec 0 point de vie et ne se régénère pas.


##### Actions

***Attaques multiples.*** Le troll effectue trois attaques : une avec sa
morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d6 + 4) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.




#### Licorne

*Grand céleste, bien légitime.*

**Classe d'armure** 12

**Points de vie** 67 (9d10 + 18)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   14 (+2)   15 (+2)   11 (+0)   17 (+3)   16 (+3)

  -----------------------------------------------------------

**Immunité aux dommages** poison

**Immunités d'état :** charmé, paralysé, empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues** céleste, elfe, sylvestre, télépathie 60 ft.

**Défi** 5 (1 800 XP)

***Charge.*** Si la licorne se déplace d'au moins 6 mètres en ligne
droite vers une cible et qu'elle la touche avec une attaque de corne
dans le même tour, la cible subit 9 (2d8) points de dégâts perforants
supplémentaires. Si la cible est une créature, elle doit réussir un jet
de sauvegarde de Force DC 15 ou être mise au sol.

***Lanceur de sorts inné.*** La caractéristique innée d'incantation de
la licorne est le Charisme (sauvegarde contre les sorts DC 14). La
licorne peut lancer de manière innée les sorts suivants, ne nécessitant
aucune composante :

A volonté : *Détection du mal et du bien*,
*Assistance*

1/jour chacun : *apaisement des émotions*,
*dissipation du mal* et du
*bien*

***Résistance à la magie.*** La licorne a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes de la licorne sont
magiques.


##### Actions

***Attaques multiples.*** La licorne effectue deux attaques : une avec
ses sabots et une avec sa corne.

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts contondants.

***Cor.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 8 (1d8 + 4) points de dégâts
perforants.

***Toucher guérisseur (3/Jour).*** La licorne touche une autre créature
avec sa corne. La cible regagne magiquement 11 2d8 + 2) points de vie.
De plus, le toucher supprime toutes les maladies et neutralise tous les
poisons qui affligent la cible.

***Téléportation (1/Jour).*** La licorne se téléporte magiquement, ainsi
que jusqu'à trois créatures volontaires qu'elle peut voir dans un
rayon de 1,5 m, et tout équipement qu'elles portent, vers un lieu que
la licorne connaît bien, jusqu'à 1,5 km de distance.



##### Actions légendaires

La licorne peut effectuer 3 actions légendaires, en choisissant parmi
les options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
La licorne récupère les actions légendaires dépensées au début de son
tour.

***Sabots.*** La licorne fait une attaque avec ses sabots.

***Bouclier chatoyant (coûte 2 actions).*** La licorne crée un champ
magique chatoyant autour d'elle ou d'une autre créature qu'elle peut
voir dans un rayon de 60 pieds autour d'elle. La cible bénéficie d'un
bonus de +2 à la CA jusqu'à la fin du prochain tour de la licorne.

***Guérison de soi (Coûte 3 Actions).*** La licorne regagne magiquement
11 (2d8 + 2) points de vie.




#### Nécrophage

*Mort-vivant de taille moyenne, mal neutre.*

**Classe d'armure** 14 (cuir clouté)

**Points de vie** 45 (6d8 + 18)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   16 (+3)   10 (+0)   13 (+1)   15 (+2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Résistance aux dégâts** nécrotiques ; contondant, perforant et
tranchant des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages** poison

**État Immunités** épuisement, empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 13

Les**langues** qu'il a connues dans la vie

**Défi** 3 (700 XP)

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, le
Nécrophage a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.


##### Actions

***Attaques multiples.*** Le Nécrophage effectue deux attaques d'épée
longue ou deux attaques d'arc long. Il peut utiliser son drain de vie à
la place d'une attaque d'épée longue.

***Drainage de vie.*** *Attaque avec une arme de corps-à-corps :* +4
pour toucher, allonge de 1,5 m, une créature. *Touché :* 5 (1d6 + 2)
dégâts nécrotiques. La cible doit réussir un jet de sauvegarde de
Constitution DC 13 ou son maximum de points de vie est réduit d'un
montant égal aux dégâts subis. Cette réduction dure jusqu'à ce que la
cible termine un long repos. La cible meurt si cet effet réduit son
maximum de points de vie à 0.

Un humanoïde tué par cette attaque se relève 24 heures plus tard sous la
forme d'un zombi sous le contrôle du Nécrophage, à moins que
l'humanoïde ne soit ramené à la vie ou que son corps ne soit détruit.

Le Nécrophage ne peut avoir plus de douze zombies sous son contrôle à la
fois.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) dégâts
tranchants, ou 7 (1d10 + 2) dégâts tranchants si utilisée à deux mains.

***Arc long.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.




#### Feu follet

*Très petite (TP) mort-vivant, maléfique chaotique*

**Classe d'armure** 19

**Points de vie** 22 (9d4)

**Vitesse** 0 ft., Vol 50 ft. (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   1 (-5)    28 (+9)   10 (+0)   13 (+1)   14 (+2)   11 (+0)

  -----------------------------------------------------------

**Immunités aux dommages**: foudre, poison.

**Résistances aux dégâts**: acide, froid, feu, nécrotique, tonnerre ;
contondant, perforant et tranchant des attaques non magiques.

**Condition Immunités** épuisement, agrippé, paralysé, empoisonné,
couché, entravé, inconscient.

**Sens** Vision dans le noir 120 ft., Perception passive 12

Les**langues** qu'il a connues dans la vie

**Défi** 2 (450 XP)

***Consumer la vie.*** En tant qu'action bonus, le feu follet peut
cibler une créature qu'il peut voir dans un rayon de 1,5 m de lui et
qui a 0 point de vie et est encore en vie. La cible doit réussir un jet
de sauvegarde de Constitution DC 10 contre cette magie ou mourir. Si la
cible meurt, le feu follet regagne 10 (3d6) points de vie.

***Ephémère.*** Le feu follet ne peut rien porter ou transporter.

***Mouvement Incorporel.*** Le Feu follet peut se déplacer à travers
d'autres créatures et objets comme s'il s'agissait d'un terrain
difficile. Il subit 5 (1d10) dégâts de force s'il termine son tour à
l'intérieur d'un objet.

***Illumination variable.*** Le feu follet diffuse une lumière vive dans
un rayon de 5 à 20 pieds et une lumière chétive sur un nombre de pieds
supplémentaire égal au rayon choisi.

Le Feu follet peut modifier le rayon comme action bonus.


##### Actions

***Choc.*** Attaque avec sort en mêlée : +4 pour toucher, portée de 1,5
m, une créature. *Touché :* 9 (2d8) dégâts de foudre.

***Invisibilité.*** Le feu follet et sa lumière deviennent magiquement
invisibles jusqu'à ce qu'il attaque ou utilise sa consommation de vie,
ou jusqu'à la fin de sa concentration (comme s'il se concentrait sur
un sort).




#### Âme en peine

*Mort-vivant de taille moyenne, mal neutre.*

**Classe d'armure** 13

**Points de vie** 67 (9d8 + 27)

**Vitesse** 0 ft, Vol 60 ft (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    16 (+3)   16 (+3)   12 (+1)   14 (+2)   15 (+2)

  -----------------------------------------------------------

**Résistances aux dégâts**: acide, froid, feu, foudre, tonnerre ;
contondant, perforant et tranchant des attaques non magiques qui ne sont
pas argentées.

**Immunités aux dommages :** nécrotique, poison.

**Immunités d'état** charmé, épuisement, agrippé, paralysé, pétrifié,
empoisonné, empoisonné, empoisonné, retenu

**Sens** Vision dans le noir 60 ft., Perception passive 12

Les**langues** qu'il a connues dans la vie

**Défi** 5 (1 800 XP)

***Mouvement Incorporel.*** L'Âme en peine peut se déplacer à travers
d'autres créatures et objets comme s'il s'agissait d'un terrain
difficile. Il subit 5 (1d10) dégâts de force s'il termine son tour à
l'intérieur d'un objet.

***Sensibilité à la lumière du Soleil.*** À la lumière du soleil, l'Âme
en peine a un désavantage aux jets d'attaque, ainsi qu'aux tests de
Sagesse (Perception) qui dépendent de la vue.


##### Actions

***Drainage de vie.*** *Attaque avec une arme de corps-à-corps :* +6
pour toucher, portée de 1,5 m, une créature. *Touché :* 21 (4d8 + 3)
dégâts nécrotiques. La cible doit réussir un jet de sauvegarde de
Constitution DC 14 ou son maximum de points de vie est réduit d'un
montant égal aux dégâts subis. Cette réduction dure jusqu'à ce que la
cible termine un long repos. La cible meurt si cet effet réduit son
maximum de points de vie à 0.

***Créer Spectre.*** L'Âme en peine cible un humanoïde dans un rayon de
3 mètres de lui qui est mort depuis 1 minute maximum et qui est mort
violemment. L'esprit de la cible s'élève sous forme de spectre dans
l'espace de son cadavre ou dans l'espace inoccupé le plus proche. Le
spectre est sous le contrôle de l'Âme en peine. L'Âme en peine ne peut
avoir plus de sept spectres sous son contrôle en même temps.




#### Wiverne

*Grand dragon, sans alignement*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 110 (13d10 + 39)

**Vitesse** 20 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   16 (+3)   5 (-3)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** -

**Défi** 6 (2 300 XP)


##### Actions

***Attaques multiples.*** La wiverne effectue deux attaques : une avec
sa morsure et une avec son dard. En vol, elle peut utiliser ses griffes
à la place d'une autre attaque.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 11 (2d6 + 4)
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts tranchants.

***Stinger.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 11 (2d6 + 4)
dégâts perforants. La cible doit effectuer un jet de sauvegarde de
Constitution DC 15, subissant 24 (7d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite.




#### Xorn

*Moyennement élémentaire, neutre*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 73 (7d8 + 42)

**Vitesse** 20 ft., terrier 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   10 (+0)   22 (+6)   11 (+0)   10 (+0)   11 (+0)

  -----------------------------------------------------------

**Compétences** Perception +6, Discrétion +3

**Résistance aux dégâts** perforants et tranchants des attaques non
magiques qui ne sont pas adamantines.

**Sens** Vision dans le noir 60 pieds, sens des secousses 60 pieds,
Perception passive 16

**Langues** glaiseux

**Défi** 5 (1 800 XP)

***Glissement de la terre.*** Le xorn peut creuser dans la terre et la
pierre non magiques et non travaillées. Ce faisant, le xorn ne perturbe
pas la matière qu'il traverse.

***Camouflage dans la pierre.*** La xorn a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain rocheux.

***Sens du trésor.*** Le Xorn peut localiser, à l'odeur, l'emplacement
des métaux et pierres précieuses, comme les pièces de monnaie et les
gemmes, dans un rayon de 60 pieds autour de lui.


##### Actions

***Attaques multiples.*** Le Xorn fait trois attaques de griffes et une
attaque de morsure.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts tranchants.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (3d6 + 3) points de
dégâts perforants.





### Anges


#### Déva

*Céleste moyen, bon droit.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 136 (16d8 + 64)

**Vitesse** 30 ft., Vol 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   18 (+4)   18 (+4)   17 (+3)   20 (+5)   20 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Wis +9, Cha +9

**Compétences** Intuition +9, Perception +9

**Résistance aux dégâts** radiant ; contondant, perforant et tranchant
des attaques non-magiques.

**Condition Immunités** charmé, épuisement, effrayé

**Sens** Vision dans le noir 120 ft., Perception passive 19

**Langues** toutes, télépathie 120 ft.

**Défi** 10 (5 900 XP)

***Armes angéliques.*** Les attaques des armes de la Déva sont magiques.
Lorsque la Déva touche avec une arme, celle-ci inflige 4d8 points de
dégâts radiants supplémentaires (inclus dans l'attaque).

***Lanceur de sorts inné.*** La caractéristique d'incantation du Déva
est le Charisme (sauvegarde contre les sorts DC 17). Le Déva peut lancer
de manière innée les sorts suivants, qui ne nécessitent que des
composantes verbales :

A volonté : *détection du mal et du bien*

1/jour chacun : *Communion*, Rappel à la
*vie*

***Résistance à la magie.*** La Déva a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le Déva effectue deux attaques de mêlée.

***Masse d'armes.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d6 + 4) dégâts
contondants plus 18 4d8) dégâts radiants.

***Toucher guérisseur (3/Jour).*** La Déva touche une autre créature. La
cible regagne magiquement 20 (4d8 + 2) points de vie et est libérée de
toute malédiction, maladie, poison, cécité ou surdité.

***Changement de forme.*** Le Déva se métamorphose magiquement en un
humanoïde ou une bête dont la valeur de défi est égale ou inférieure à
la sienne, ou reprend sa véritable forme. Il revient à sa vraie forme
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du Déva).

Dans une nouvelle forme, le Déva conserve ses statistiques de jeu et sa
capacité à parler, mais sa CA, ses modes de déplacement, sa Force, sa
Dextérité et ses sens spéciaux sont remplacés par ceux de la nouvelle
forme, et il gagne toutes les statistiques et capacités (sauf les
caractéristiques de classe, les actions légendaires et les actions de
repaire) que la nouvelle forme possède mais qui lui manquent.




#### Planétar

*Grand céleste, bien légitime*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 200 (16d10 + 112)

**Vitesse** 40 ft., Vol 120 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   24 (+7)   20 (+5)   24 (+7)   19 (+4)   22 (+6)   25 (+7)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +12, Wis +11, Cha +12

**Compétences** Perception +11

**Résistance aux dégâts** radiant ; contondant, perforant et tranchant
des attaques non-magiques.

**Condition Immunités** charmé, épuisement, effrayé

**Sens** vision véritable 120 ft., Perception passive 21

**Langues** toutes, télépathie 120 ft.

**Défi** 16 (15 000 XP)

***Armes angéliques.*** Les attaques d'armes du Planétar sont magiques.
Lorsque le planétar touche avec une arme, celle-ci inflige 5d8 points de
dégâts radiants supplémentaires (inclus dans l'attaque).

***Conscience divine.*** Le Planétar sait s'il entend un mensonge.

***Lanceur de sorts inné.*** La caractéristique d'incantation du
Planétar est le Charisme (sauvegarde contre les sorts DC 20). Le
Planétar peut lancer de manière innée les sorts suivants, ne nécessitant
aucune composante matérielle :

A volonté : *Détection du mal et du bien*,
*Invisibilité*

3/jour chacun : Barrière *de lames*, *Dissipation du
mal et du bien*, Colonne de
*flamme* à la vie

1/jour chacun : *Communion*, Contrôle du
*climat*

***Résistance à la magie.*** Le Planétar a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le Planétar effectue deux attaques de mêlée.

***Épée à deux mains.*** *Attaque avec une arme de corps-à-corps :* +12
pour toucher, allonge de 1,5 m, une cible. *Touché :* 21 (4d6 + 7)
dégâts tranchants plus 22 (5d8) dégâts radiants.

***Healing Touch (4/Jour).*** Le Planétar touche une autre créature. La
cible regagne magiquement 30 (6d8 + 3) points de vie et est libérée de
toute malédiction, maladie, poison, cécité ou surdité.




#### Solar

*Grand céleste, bien légitime*

**Classe d'armure** 21 (armure naturelle)

**Points de vie** 243 (18d10 + 144)

**Vitesse** 50 ft., Vol 150 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   26 (+8)   22 (+6)   26 (+8)   25 (+7)   25 (+7)  30 (+10)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +14, Wis +14, Cha +17

**Compétences** Perception +14

**Résistance aux dégâts** radiant ; contondant, perforant et tranchant
des attaques non-magiques.

**Immunités aux dommages :** nécrotique, poison.

**Immunités d'état :** charmé, épuisement, effrayé, empoisonné.

**Sens** vision véritable 120 ft., Perception passive 24

**Langues** toutes, télépathie 120 ft.

**Défi** 21 (33 000 XP)

***Armes angéliques.*** Les attaques des armes du Solar sont magiques.
Lorsque le solar touche avec une arme, celle-ci inflige 6d8 points de
dégâts radiants supplémentaires (inclus dans l'attaque).

***Conscience divine.*** Le Solar sait s'il entend un mensonge.

***Lanceur de sorts inné.*** La caractéristique d'incantation du Solar
est le Charisme (sauvegarde contre les sorts DC 25). Il peut lancer de
manière innée les sorts suivants, ne nécessitant aucun composant
matériel :

A volonté : *Détection du mal et du bien*,
*Invisibilité*

3/jour chacun : Barrière *de lames*, *Dissipation du
mal et du bien*

1/jour chacun : *Communion*, Contrôle du
*climat*

***Résistance à la magie.*** Le Solar a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le Solar effectue deux attaques d'épée à deux
mains.

***Épée à deux mains.*** *Attaque avec une arme de corps-à-corps :* +15
pour toucher, allonge de 1,5 m, une cible. *Touché :* 22 (4d6 + 8)
points de dégâts tranchants plus 27 (6d8) points de dégâts radiants.

***Arc long pour tuer.*** *Attaque avec une arme à distance :* +13 pour
toucher, portée de 150/600 pieds, une cible. *Touché :* 15 (2d8 + 6)
dégâts perforants plus 27 (6d8) dégâts radiants. Si la cible est une
créature qui a 100 points de vie ou moins, elle doit réussir un jet de
sauvegarde de Constitution DC 15 ou mourir.

***Épée volante.*** Le solar libère son épée à deux mains pour qu'elle
plane magiquement dans un espace inoccupé à moins de 1,5 mètre de lui.
Si le solar peut voir l'épée, il peut mentalement la commander comme
une action bonus pour voler jusqu'à 50 pieds et soit effectuer une
attaque contre une cible, soit retourner dans les mains du solar. Si
l'épée en vol stationnaire est ciblée par un effet quelconque, on
considère que le solar la tient. L'épée volante tombe si le solar
meurt.

***Healing Touch (4/Jour).*** Le solar touche une autre créature. La
cible regagne magiquement 40 (8d8 + 4) points de vie et est libérée de
toute malédiction, maladie, poison, cécité ou surdité.



##### Actions légendaires

Le Solar peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le solar regagne les actions légendaires dépensées au début de son tour.

***Téléportation.*** Le Solar se téléporte magiquement, ainsi que tout
équipement qu'il porte, jusqu'à 120 pieds dans un espace inoccupé
qu'il peut voir.

***Éclats déchirants (coûte 2 actions).*** Le Solar émet une énergie
magique et divine. Chaque créature de son choix dans un rayon de 3
mètres doit effectuer un jet de sauvegarde de Dextérité DC 23, subissant
14 (4d6) dégâts de feu plus 14 (4d6) dégâts radiants en cas d'échec, ou
la moitié des dégâts en cas de réussite.

***Regard aveuglé (coûte 3 actions).*** Le Solar cible une créature
qu'il peut voir dans un rayon de 30 pieds autour de lui. Si la cible
peut le voir, elle doit réussir un jet de sauvegarde de Constitution DC
15 ou être aveuglée jusqu'à ce qu'une magie telle que le sort de
*restauration partielle* supprime la cécité.





### Objets animés


#### Armure animée

*Construction moyenne, sans alignement.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 33 (6d8 + 6)

**Vitesse** 25 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   11 (+0)   13 (+1)   1 (-5)    3 (-4)    1 (-5)

  -----------------------------------------------------------

**Immunités aux dommages**: poison, psychique.

**Immunités de condition :** aveuglé, charmé, paralysé, épuisé, effrayé,
paralysé, pétrifié, empoisonné.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 6

**Langues** -

**Défi** 1 (200 XP)

***Susceptibilité antimagique.*** L'armure est frappée d'incapacité
lorsqu'elle se trouve dans la zone d'un champ
*antimagie*. Si elle est ciblée par *Dissipation de
la magie*, l'armure doit réussir un jet de sauvegarde
de Constitution contre le DC de sauvegarde contre les sorts du lanceur
ou tomber inconsciente pendant 1 minute.

***Fausse apparence.*** Lorsque l'armure reste immobile, il est
impossible de la distinguer d'une armure normale.


##### Actions

***Attaques multiples.*** L'armure effectue deux attaques de mêlée.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de dégâts
contondants.




#### Épée volante

*Petite construction, sans alignement*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 17 (5d6)

**Vitesse** 0 ft, Vol 50 ft (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   15 (+2)   11 (+0)   1 (-5)    5 (-3)    1 (-5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4

**Immunités aux dommages**: poison, psychique.

**Immunités de condition :** aveuglé, charmé, assourdi, effrayé,
paralysé, pétrifié, empoisonné.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 7

**Langues** -

**Défi** 1/4 (50 XP)

***Susceptibilité antimagique.*** L'épée est frappée d'incapacité
lorsqu'elle se trouve dans la zone d'un champ
*antimagie*. Si elle est ciblée par *Dissipation de
la magie*, l'épée doit réussir un jet de sauvegarde de
Constitution contre le DC de sauvegarde des sorts du lanceur ou tomber
inconsciente pendant 1 minute.

***Fausse apparence.*** Bien que l'épée reste immobile et ne vole pas,
elle est indiscernable d'une épée normale.


##### Actions

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts tranchants.




#### Tapis étouffant

*Grande construction, sans alignement.*

**Classe d'armure** 12

**Points de vie** 33 (6d10)

**Vitesse** 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   14 (+2)   10 (+0)   1 (-5)    3 (-4)    1 (-5)

  -----------------------------------------------------------

**Immunités aux dommages**: poison, psychique.

**Immunités de condition :** aveuglé, charmé, assourdi, effrayé,
paralysé, pétrifié, empoisonné.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 6

**Langues** -

**Défi** 2 (450 XP)

***Susceptibilité antimagique.*** Le tapis est frappé d'incapacité
lorsqu'il se trouve dans la zone d'un champ
*antimagie*. Si elle est ciblée par *Dissipation de
la magie*, la carpette doit réussir un jet de sauvegarde
de Constitution contre le DC de sauvegarde des sorts du lanceur ou
tomber inconsciente pendant 1 minute.

***Transfert des dégâts.*** Lorsqu'il agrippe une créature, le tapis ne
subit que la moitié des dégâts qui lui sont infligés, et la créature
agrippée par le tapis subit l'autre moitié.

***Fausse apparence.*** Lorsque le tapis reste immobile, il est
impossible de le distinguer d'un tapis normal.


##### Actions

***Etouffement.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une créature moyenne ou plus petite. *Touché
:* La créature est agrippée (fuite DC 13). Jusqu'à la fin de ce
grappin, la cible est entravée, aveuglée et risque de suffoquer, et le
tapis ne peut pas étouffer une autre cible. De plus, au début de chacun
de ses tours, la cible subit 10 (2d6 + 3) dégâts de contondant.





### Démons


#### Balor

*Très grand fiélon (démon), mal chaotique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 262 (21d12 + 126)

**Vitesse** 40 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   26 (+8)   15 (+2)   22 (+6)   20 (+5)   16 (+3)   22 (+6)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +14, Con +12, Wis +9, Cha +12.

**Résistances aux dégâts**: froid, foudre ; contondant, perforant et
tranchant des attaques non magiques.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 13

**Langues** Abysses, Télépathie 120 ft.

**Défi** 19 (22 000 XP)

***Gorge de mort.*** Lorsque le balor meurt, il explose et chaque
créature se trouvant à moins de 30 pieds de lui doit effectuer un jet de
sauvegarde de Dextérité DC 20, subissant 70 (20d6) dégâts de feu en cas
d'échec, ou la moitié des dégâts en cas de réussite. L'explosion
enflamme les objets inflammables qui se trouvent dans cette zone et qui
ne sont pas portés ou transportés, et elle détruit les armes du Balor.

***Aura de feu.*** Au début de chaque tour du balor, chaque créature se
trouvant à moins de 1,5 m de lui subit 10 (3d6) dégâts de feu, et les
objets inflammables se trouvant dans l'aura qui ne sont pas portés ou
transportés s'enflamment. Une créature qui touche le balor ou le frappe
avec une attaque de mêlée alors qu'elle se trouve à moins de 1,5 m de
lui subit 10 (3d6) dégâts de feu.

***Résistance à la magie.*** Le balor a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du Balor sont magiques.


##### Actions

***Attaques multiples.*** Le balor effectue deux attaques : une avec son
épée longue et une avec son fouet.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 21 (3d8 + 8) dégâts
tranchants plus 13 (3d8) dégâts de foudre. Si le balor obtient un coup
critique, il lance les dés de dégâts trois fois, au lieu de deux.

***Fouet.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 30 pieds, une cible. *Touché :* 15 (2d6 + 8) dégâts
tranchants plus 10 3d6) dégâts de feu, et la cible doit réussir un jet
de sauvegarde de Force DC 20 ou être tirée jusqu'à 25 pieds vers le
balor.

***Téléportation.*** Le balor se téléporte magiquement, ainsi que tout
équipement qu'il porte, jusqu'à 120 pieds dans un espace inoccupé
qu'il peut voir.




#### Dretch

*Petit fiélon (démon), mal chaotique.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 18 (4d6 + 4)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   11 (+0)   12 (+1)   5 (-3)    8 (-1)    3 (-4)

  -----------------------------------------------------------

**Résistances aux dégâts**: froid, feu et foudre.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** Abysses, Télépathie 60 ft. (ne fonctionne qu'avec les
créatures qui comprennent l'Abyssal).

**Défi** 1/4 (50 XP)


##### Actions

***Attaques multiples.*** La Dretch effectue deux attaques : une avec sa
morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d6) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (2d4) points de
dégâts tranchants.

***Nuage fétide (1/Jour).*** Un rayon de 3 mètres de gaz vert dégoûtant
s'étend à partir de la Dretch. Le gaz se répand dans les coins, et sa
zone est légèrement obscurcie. Il dure 1 minute ou jusqu'à ce qu'un
vent fort le disperse. Toute créature qui commence son tour dans cette
zone doit réussir un jet de sauvegarde de Constitution DC 11 ou être
empoisonnée jusqu'au début de son prochain tour. Lorsqu'elle est
empoisonnée de cette manière, la cible peut effectuer soit une action,
soit une action bonus à son tour, mais pas les deux, et ne peut pas
effectuer de réactions.




#### Glabrezu

*Grand fiélon (démon), mal chaotique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 157 (15d10 + 75)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   15 (+2)   21 (+5)   19 (+4)   17 (+3)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +9, Con +9, Wis +7, Cha +7.

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 13

**Langues** Abysses, Télépathie 120 ft.

**Défi** 9 (5 000 XP)

***Lanceur de sorts inné.*** La caractéristique d'incantation du
Glabrezu est l'Intelligence (sauvegarde contre les sorts DC 16). Le
glabrezu peut lancer de manière innée les sorts suivants, ne nécessitant
aucune composante matérielle :

À volonté : *ténèbres*, *détection de la
magie*.

1/jour chacun : *confusion*, *Mot
de* pouvoir étourdissant

***Résistance à la magie.*** Le Glabrezu a un avantage sur les jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le Glabrezu effectue quatre attaques : deux
avec ses pinces et deux avec ses poings. Il peut aussi effectuer deux
attaques avec ses pinces et lancer un sort.

***Pince.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 16 (2d10 + 5) points de
dégâts contondants. Si la cible est une créature de taille moyenne ou
plus petite, elle est agrippée (fuite DC 15). Le glabrezu possède deux
pinces, chacune ne pouvant agripper qu'une seule cible.

***Poing.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) points de dégâts
contondants.




#### Hezrou

*Grand fiélon (démon), mal chaotique.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 136 (13d10 + 65)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   17 (+3)   20 (+5)   5 (-3)    12 (+1)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +7, Con +8, Wis +4

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** Abysses, Télépathie 120 ft.

**Défi** 8 (3 900 XP)

***Résistance à la magie.*** L'Hezrou a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Stench.*** Toute créature qui commence son tour à moins de 3 mètres
de l'hezrou doit réussir un jet de sauvegarde de Constitution DC 14 ou
être empoisonnée jusqu'au début de son prochain tour. En cas de jet de
sauvegarde réussi, la créature est immunisée contre la puanteur de
l'hezrou pendant 24 heures.


##### Actions

***Attaques multiples.*** L'hezrou effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 15 (2d10 + 4) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.




#### Marilith

*Grand fiélon (démon), mal chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 189 (18d10 + 90)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   20 (+5)   20 (+5)   18 (+4)   16 (+3)   20 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +9, Con +10, Wis +8, Cha +10

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 13

**Langues** Abysses, Télépathie 120 ft.

**Défi** 16 (15 000 XP)

***Résistance à la magie.*** Le Marilith a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du Marilith sont magiques.

***Réactif.*** Le Marilith peut effectuer une réaction à chaque tour de
combat.


##### Actions

***Attaques multiples.*** Le Marilith effectue sept attaques : six avec
ses épées longues et une avec sa queue.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 10 pieds, une créature. *Touché :* 15 (2d10 + 4) points de
dégâts contondants. Si la cible est de taille moyenne ou plus petite,
elle est agrippée (fuite DC 19). Jusqu'à la fin de cet agrippement, la
cible est entravée, le marilith peut automatiquement la frapper avec sa
queue, et le marilith ne peut pas faire d'attaque de queue contre
d'autres cibles.

***Téléportation.*** Le marilith se téléporte magiquement, avec tout
équipement qu'il porte, jusqu'à 120 pieds dans un espace inoccupé
qu'il peut voir.



##### Réactions

***Parade.*** Le marilith ajoute 5 à sa CA contre une attaque de mêlée
qui le toucherait. Pour cela, le marilith doit voir l'attaquant et
manier une arme de mêlée.




#### Nalfeshnie

*Grand fiélon (démon), mal chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 184 (16d10 + 96)

**Vitesse** 20 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   10 (+0)   22 (+6)   19 (+4)   12 (+1)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +11, Int +9, Wis +6, Cha +7

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 11

**Langues** Abysses, Télépathie 120 ft.

**Défi** 13 (10 000 XP)

***Résistance à la magie.*** La Nalfeshnie a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** La Nalfeshnie utilise Horror Nimbus si elle le
peut. Il effectue ensuite trois attaques : une avec sa morsure et deux
avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 32 (5d10 + 5) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (3d6 + 5) points
de dégâts tranchants.

***Horror Nimbus (Recharge 5-6).*** La Nalfeshnie émet magiquement une
lumière scintillante et multicolore. Chaque créature située à moins de
15 pieds de la Nalfeshnie qui peut voir la lumière doit réussir un jet
de sauvegarde de Sagesse DC 15 ou être effrayée pendant 1 minute. Une
créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite. Si le
jet de sauvegarde d'une créature est réussi ou si l'effet prend fin
pour elle, la créature est immunisée contre le Nimbus d'horreur de la
Nalfeshnie pendant les 24 heures suivantes.

***Téléportation.*** La Nalfeshnie se téléporte magiquement, ainsi que
tout équipement qu'elle porte, jusqu'à 120 pieds dans un espace
inoccupé qu'elle peut voir.




#### Quasit

*Très petite (TP) fiélon (démon, métamorphe), mal chaotique.*

**Classe d'armure** 13

**Points de vie** 7 (3d4)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    17 (+3)   10 (+0)   7 (-2)    10 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Discrétion +5

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunités aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** Abysses, Communes

**Défi** 1 (200 XP)

***Changement de forme.*** Le Quasit peut utiliser son action pour se
métamorphoser en une forme de bête ressemblant à une chauve-souris
(vitesse 10 ft. vol 40 ft.), un mille-pattes (40 ft., grimper 40 ft.),
ou un crapaud (40 ft., nager 40 ft.), ou revenir à sa forme véritable.

Ses statistiques sont les mêmes dans chaque forme, à l'exception des
changements de vitesse indiqués. L'équipement qu'il porte n'est pas
transformé. Il revient à sa vraie forme s'il meurt.

***Résistance à la magie.*** Le quasit a un avantage sur les jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Griffes (morsure en forme de bête).*** *Attaque avec une arme de
corps-à-corps :* +4 pour toucher, allonge de 1,5 m, une cible. *Touche
:* 5 (1d4 + 3) dégâts perforants, et la cible doit réussir un jet de
sauvegarde de Constitution DC 10 ou subir 5 (2d4) dégâts de poison et
être empoisonnée pendant 1 minute. La cible peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet sur
elle-même en cas de réussite.

***Effarouchement (1/Jour).*** Une créature au choix du quasit, située à
moins de 6 mètres de lui, doit réussir un jet de sauvegarde de Sagesse
DC 10 ou être effrayée pendant 1 minute. La cible peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, avec un désavantage si le
quasit est en ligne de vue, mettant fin à l'effet sur elle-même en cas
de réussite.

***Invisibilité.*** Le quasit devient magiquement invisible jusqu'à ce
qu'il attaque ou utilise Effrayer, ou jusqu'à ce que sa concentration
se termine (comme s'il se concentrait sur un sort). Tout équipement que
le Quasit porte ou transporte est invisible avec lui.




#### Vrock

*Grand fiélon (démon), mal chaotique.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 104 (11d10 + 44)

**Vitesse** 40 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   18 (+4)   8 (-1)    13 (+1)   8 (-1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Wis +4, Cha +2

**Résistances aux dégâts**: froid, feu, foudre ; contondant, perforant
et tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** Abysses, Télépathie 120 ft.

**Défi** 6 (2 300 XP)

***Résistance à la magie.*** Le Vrock a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le vrock effectue deux attaques : une avec son
bec et une avec ses serres.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de dégâts
perforants.

***Talons.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d10 + 3) points de
dégâts tranchants.

***Spores (Recharge 6).*** Un nuage de spores toxiques de 15 pieds de
rayon s'étend à partir du Vrock. Les spores se répandent dans les
coins. Chaque créature dans cette zone doit réussir un jet de sauvegarde
de Constitution DC 14 ou être empoisonnée. Lorsqu'elle est empoisonnée
de cette façon, la cible subit 5 (1d10) dégâts de poison au début de
chacun de ses tours. Une cible peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Vider une fiole d'eau bénite sur la cible met également
fin à l'effet sur elle.

***Cri étourdissant (1/Jour).*** Le Vrock émet un horrible hurlement.
Chaque créature dans un rayon de 6 mètres de lui qui peut l'entendre et
qui n'est pas un démon doit réussir un jet de sauvegarde de
Constitution DC 14 ou être étourdie jusqu'à la fin du prochain tour du
Vrock.





### Diables


#### Diable barbelé

*Le fiélon moyen (diable), le mal légitime.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 110 (13d8 + 52)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   17 (+3)   18 (+4)   12 (+1)   14 (+2)   14 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +6, Con +7, Wis +5, Cha +5

**Compétences** Supercherie +5, Intuition +5, Perception +8

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 18

**Langues** Infernal, Télépathie 120 ft.

**Défi** 5 (1 800 XP)

***Peau barbelée.*** Au début de chacun de ses tours, le diable barbelé
inflige 5 (1d10) dégâts perforants à toute créature qui l'agrippe.

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le diable effectue trois attaques de mêlée :
une avec sa queue et deux avec ses griffes. Alternativement, il peut
utiliser Hurlement de flammes deux fois.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts perforants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de dégâts
perforants.

***Hurlement de flammes.*** Attaque avec un sort à distance : +5 pour
toucher, portée de 150 pieds, une cible. *Touché :* 10 (3d6) dégâts de
feu. Si la cible est un objet inflammable qui n'est pas porté ou
transporté, il prend également feu.




#### Diable barbu

*Le fiélon moyen (diable), le mal légitime.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 52 (8d8 + 16)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   15 (+2)   15 (+2)   9 (-1)    11 (+0)   11 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +5, Con +4, Wis +2

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** Infernal, Télépathie 120 ft.

**Défi** 3 (700 XP)

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Inébranlable.*** Le diable ne peut pas être effrayé tant qu'il peut
voir une créature alliée à moins de 30 pieds de lui.


##### Actions

***Attaques multiples.*** Le diable effectue deux attaques : une avec sa
barbe et une avec sa coutille.

***Barbe.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une créature. *Touché :* 6 (1d8 + 2) dégâts
perforants, et la cible doit réussir un jet de sauvegarde de
Constitution DC 12 ou être empoisonnée pendant 1 minute. Tant qu'elle
est empoisonnée de cette manière, la cible ne peut pas regagner de
points de vie. La cible peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.

***Couteau.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 8 (1d10 + 3) dégâts
tranchants. Si la cible est une créature autre qu'un mort-vivant ou une
construction, elle doit réussir un jet de sauvegarde de Constitution DC
12 ou perdre 5 (1d10) points de vie au début de chacun de ses tours à
cause d'une blessure infernale. Chaque fois que le diable touche la
cible blessée avec cette attaque, les dégâts infligés par la blessure
augmentent de 5 (1d10). Toute créature peut entreprendre une action pour
panser la blessure en réussissant un test de Sagesse (Médecine) DC 12.
La blessure se referme également si la cible reçoit des soins magiques.




#### Diable osseux

*Grand fiélon (diable), le mal légitime.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 142 (15d10 + 60)

**Vitesse** 40 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   16 (+3)   18 (+4)   13 (+1)   14 (+2)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +5, Wis +6, Cha +7

**Compétences** Supercherie +7, Intuition +6

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 12

**Langues** Infernal, Télépathie 120 ft.

**Défi** 9 (5 000 XP)

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le diable effectue trois attaques : deux avec
ses griffes et une avec son dard.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 8 (1d8 + 4) points
de dégâts tranchants.

***Piquer.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 ft, une cible. *Touché :* 13 (2d8 + 4) dégâts
perforants plus 17 5d6) dégâts de poison, et la cible doit réussir un
jet de sauvegarde de Constitution DC 14 ou être empoisonnée pendant 1
minute. La cible peut répéter le jet de sauvegarde à la fin de chacun de
ses tours, mettant fin à l'effet sur elle-même en cas de réussite.




#### Diable à chaîne

*Le fiélon moyen (diable), le mal légitime.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 85 (10d8 + 40)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   18 (+4)   11 (+0)   12 (+1)   14 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +7, Wis +4, Cha +5

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** Infernal, Télépathie 120 ft.

**Défi** 8 (3 900 XP)

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le diable effectue deux attaques avec ses
chaînes.

***Chaîne.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 11 (2d6 + 4) dégâts
tranchants. La cible est agrippée (fuite DC 14) si le diable n'est pas
déjà en train d'agripper une créature. Jusqu'à la fin de ce grappin,
la cible est entravée et subit 7 (2d6) dégâts perforants au début de
chacun de ses tours.

***Chaînes animées (se recharge après un repos court ou long).***
Jusqu'à quatre chaînes que le diable peut voir dans un rayon de 60
pieds de lui font apparaître comme par magie des barbes de rasoir et
s'animent sous le contrôle du diable, à condition que les chaînes ne
soient pas portées.

Chaque chaîne animée est un objet ayant une CA de 20, 20 points de vie,
une résistance aux dégâts perforants et une immunité aux dégâts
psychiques et de tonnerre. Lorsque le diable utilise Attaques multiples
à son tour, il peut utiliser chaque chaîne animée pour effectuer une
attaque en chaîne supplémentaire. Une chaîne animée peut agripper une de
ses créatures mais ne peut pas effectuer d'attaques pendant qu'elle
est agrippée. Une chaîne animée revient à son état inanimé si elle est
réduite à 0 point de vie ou si le diable est frappé d'incapacité ou
meurt.



##### Réactions

***Masque troublant.*** Lorsqu'une créature que le diable peut voir
commence son tour à moins de 30 pieds du diable, ce dernier peut créer
l'illusion qu'il ressemble à l'un des proches ou des ennemis acharnés
de la créature. Si la créature peut voir le diable, elle doit réussir un
jet de sauvegarde de Sagesse DC 14 ou être effrayée jusqu'à la fin de
son tour.




#### Érinyes

*Le fiélon moyen (diable), le mal légitime.*

**Classe d'armure** 18 (harnois)

**Points de vie** 153 (18d8 + 72)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   16 (+3)   18 (+4)   14 (+2)   14 (+2)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +8, Wis +6, Cha +8

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 12

**Langues** Infernal, Télépathie 120 ft.

**Défi** 12 (8 400 XP)

***Armes infernales.*** Les attaques d'armes de l'Érinye sont magiques
et infligent 13 (3d8) dégâts de poison supplémentaires en cas de succès
(inclus dans les attaques).

***Résistance à la magie.*** L'Érinye a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** L'Érinye effectue trois attaques.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d8 + 4) points de
dégâts tranchants, ou 9 (1d10 + 4) points de dégâts tranchants si
utilisée à deux mains, plus 13 3d8 points de dégâts de poison.

***Arc long.*** *Attaque avec une arme à distance :* +7 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 7 (1d8 + 3) dégâts
perforants plus 13 (3d8) dégâts de poison, et la cible doit réussir un
jet de sauvegarde de Constitution DC 14 ou être empoisonnée. Le poison
dure jusqu'à ce qu'il soit éliminé par le sort *Restauration
partielle* ou une magie similaire.



##### Réactions

***Parade.*** L'Érinye ajoute 4 à sa CA contre une attaque de mêlée qui
le toucherait. Pour cela, l'érinye doit voir l'attaquant et manier une
arme de mêlée.




#### Diable cornu

*Grand fiélon (diable), le mal légitime.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 148 (17d10 + 55)

**Vitesse** 20 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   17 (+3)   21 (+5)   12 (+1)   16 (+3)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +10, Dex +7, Wis +7, Cha +7

**Résistance aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas faites avec des armes en
argent.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 13

**Langues** Infernal, Télépathie 120 ft.

**Défi** 11 (7 200 XP)

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le diable effectue trois attaques de mêlée :
deux avec sa fourche et une avec sa queue. Il peut utiliser Hurlement de
flammes à la place de n'importe quelle attaque de mêlée.

***Fourchette.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts perforants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 10 (1d8 + 6) dégâts
perforants. Si la cible est une créature autre qu'un mort-vivant ou une
construction, elle doit réussir un jet de sauvegarde de Constitution DC
17 ou perdre 10 (3d6) points de vie au début de chacun de ses tours à
cause d'une blessure infernale. Chaque fois que le diable touche la
cible blessée avec cette attaque, les dégâts infligés par la blessure
augmentent de 10 (3d6). Toute créature peut entreprendre une action pour
panser la blessure en réussissant un test de Sagesse (Médecine) DC 12.
La blessure se referme également si la cible reçoit des soins magiques.

***Hurlement de flammes.*** Attaque avec un sort à distance : +7 pour
toucher, portée de 150 pieds, une cible. *Touché :* 14 (4d6) dégâts de
feu. Si la cible est un objet inflammable qui n'est pas porté ou
transporté, il prend également feu.




#### Diable gelé

*Grand fiélon (diable), le mal légitime.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 180 (19d10 + 76)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   14 (+2)   18 (+4)   18 (+4)   15 (+2)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +9, Wis +7, Cha +9

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: froid, feu, poison.

**État Immunités** empoisonnées

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 12

**Langues** Infernal, Télépathie 120 ft.

**Défi** 14 (11,500 XP)

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du diable.

***Résistance à la magie.*** Le diable a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Attaques multiples.*** Le diable effectue trois attaques : une avec
sa morsure, une avec ses griffes, et une avec sa queue.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) dégâts
perforants plus 10 (3d6) dégâts de froid.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d4 + 5) dégâts
tranchants plus 10 3d6) dégâts de froid.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 12 (2d6 + 5) dégâts
contondants plus 10 3d6) dégâts de froid.

***Mur de glace (Recharge 6).*** Le diable forme magiquement un mur de
glace opaque sur une surface solide qu'il peut voir dans un rayon de 60
pieds autour de lui. Le mur fait 1 pied d'épaisseur et jusqu'à 30
pieds de long et 10 pieds de haut, ou c'est un dôme hémisphérique
jusqu'à 20 pieds de diamètre.

Lorsque le mur apparaît, chaque créature se trouvant dans son espace est
poussée hors de celui-ci par le chemin le plus court. La créature
choisit de quel côté du mur elle se retrouve, à moins qu'elle ne soit
frappée d'incapacité. La créature effectue alors un jet de sauvegarde
de Dextérité DC 17, subissant 35 (10d6) dégâts de froid en cas d'échec,
ou la moitié des dégâts en cas de réussite.

Le mur dure 1 minute ou jusqu'à ce que le démon soit frappé
d'incapacité ou meure. Le mur peut être endommagé et percé ; chaque
section de 3 mètres a une CA de 5, 30 points de vie, une vulnérabilité
aux dégâts de feu et une immunité aux dégâts d'acide, de froid, de
nécrotique, de poison et de psychique. Si une section est détruite, elle
laisse derrière elle une nappe d'air glacial dans l'espace que le mur
occupait. Chaque fois qu'une créature finit de se déplacer dans l'air
glacial pendant un tour, volontairement ou non, elle doit effectuer un
jet de sauvegarde de Constitution DC 17, subissant 17 (5d6) dégâts de
froid en cas d'échec, ou la moitié des dégâts en cas de réussite.
L'air glacial se dissipe lorsque le reste du mur disparaît.




#### Diablotin

*Très petite (TP) fiélon (diable, métamorphe), maléfique.*

**Classe d'armure** 13

**Points de vie** 10 (3d4 + 3)

**Vitesse** 20 ft., Vol 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    17 (+3)   13 (+1)   11 (+0)   12 (+1)   14 (+2)

  -----------------------------------------------------------

**Compétences** Supercherie +4, Intuition +3, Persuasion +4, Furtivité
+5.

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 120 ft., Perception passive 11

**Langues** infernal, commun.

**Défi** 1 (200 XP)

***Métamorphe.*** Le diablotin peut utiliser son action pour se
métamorphoser en une forme bestiale ressemblant à un rat (vitesse de 6
mètres), un corbeau (6 mètres, vol de 6 mètres) ou une araignée (6
mètres, ascension de 6 mètres), ou revenir à sa forme véritable. Ses
statistiques sont les mêmes sous chaque forme, à l'exception des
changements de vitesse indiqués. L'équipement qu'il porte n'est pas
transformé. Il reprend sa vraie forme s'il meurt.

***Regard du Diable.*** Les ténèbres magiques n'empêchent pas la
vision dans le noir du Diablotin.

***Résistance à la magie.*** Le diablotin a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Piqûre (morsure en forme de bête).*** *Attaque avec une arme de
corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible. *Touché
:* 5 (1d4 + 3) dégâts perforants, et la cible doit réussir un jet de
sauvegarde de Constitution DC 11, subissant 10 (3d6) dégâts de poison en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Invisibilité.*** Le diablotin devient magiquement invisible jusqu'à
ce qu'il attaque ou que sa concentration se termine (comme s'il se
concentrait sur un sort). Tout équipement que le Diablotin porte ou
transporte est invisible avec lui.




#### Lémure

*Le fiélon moyen (diable), le mal légitime.*

**Classe d'armure** 7

**Points de vie** 13 (3d8)

**Vitesse** 15 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   5 (-3)    11 (+0)   1 (-5)    11 (+0)   3 (-4)

  -----------------------------------------------------------

**Résistance** aux**dégâts à** froid

**Immunités aux dommages**: feu, poison.

**Immunités de condition** charmé, effrayé, empoisonné.

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** comprend l'infernal mais ne peut pas le parler.

**Défi** 0 (10 XP)

***Regard du Diable.*** Les ténèbres magiques n'entravent pas la
vision dans le noir de la Lémure.

***Rajeunissement infernal.*** Une lémure morte en Enfer revient à la
vie avec tous ses points de vie en 1d10 jours, sauf si elle est tuée par
une créature d'alignement bon avec un sort de *Bénédiction*
lancé sur cette créature ou si ses restes sont aspergés d'eau bénite.


##### Actions

***Poing.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts contondants.




#### Fiélon des Puits

*Grand fiélon (diable), le mal légitime.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 300 (24d10 + 168)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   26 (+8)   14 (+2)   24 (+7)   22 (+6)   18 (+4)   24 (+7)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +8, Con +13, Wis +10

**Résistances aux dégâts**: froid ; contondant, perforant et tranchant
des attaques non magiques qui ne sont pas argentées.

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** vision véritable 120 ft., Perception passive 14

**Langues** Infernal, Télépathie 120 ft.

**Défi** 20 (25 000 XP)

***Aura de Peur.*** Toute créature hostile au fiélon des puits qui
commence son tour à moins de 6 mètres du fiélon des puits doit effectuer
un jet de sauvegarde de Sagesse DC 21, sauf si le fiélon des puits est
frappé d'incapacité. En cas d'échec, la créature est effrayée
jusqu'au début de son prochain tour. Si le jet de sauvegarde de la
créature est réussi, la créature est immunisée contre l'aura de Peur du
fiélon des puits pendant les 24 heures suivantes.

***Résistance à la magie.*** Le Fiélon des Puits a un avantage sur les
jets de sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du fiélon sont magiques.

***Lanceur de sorts inné.*** La caractéristique d'incantation du fiélon
est le Charisme (sauvegarde contre les sorts DC 21). Le fiélon des puits
peut lancer de manière innée les sorts suivants, qui ne nécessitent
aucune composante matérielle :

À volonté : *détection de la magie*, *boule de
feu*.

3/jour chacun : *immobilisation de monstre*,
*mur* de feu


##### Actions

***Attaques multiples.*** Le fiélon des puits effectue quatre attaques :
une avec sa morsure, une avec sa griffe, une avec sa masse et une avec
sa queue.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 22 (4d6 + 8) points de
dégâts perforants. La cible doit réussir un jet de sauvegarde de
Constitution DC 21 ou s'empoisonner. Lorsqu'elle est empoisonnée de
cette manière, la cible ne peut pas regagner de points de vie, et elle
subit 21 (6d6) points de dégâts de poison au début de chacun de ses
tours. La cible empoisonnée peut répéter le jet de sauvegarde à la fin
de chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts tranchants.

***Masse d'armes.*** *Attaque avec une arme de corps-à-corps :* +14
pour toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d6 + 8)
dégâts de contondant plus 21 (6d6) dégâts de feu.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 24 (3d10 + 8) points
de dégâts contondants.





### Dinosaures


#### Plésiosaure

*Grandes bêtes, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 68 (8d10 + 24)

**Vitesse** 20 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Sens** passif Perception 13

**Langues** -

**Défi** 2 (450 XP)

***Retenir sa respiration.*** Le plésiosaure peut retenir sa respiration
pendant 1 heure.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 14 (3d6 + 4) points
de dégâts perforants.




#### Tricératops

*Très grande bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 95 (10d12 + 30)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   9 (-1)    17 (+3)   2 (-4)    11 (+0)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 5 (1 800 XP)

***Charge piétinante.*** Si le Tricératops se déplace d'au moins 6
mètres en direction d'une créature et la touche avec une attaque gore
dans le même tour, la cible doit réussir un jet de sauvegarde de Force
DC 13 ou être mise à terre.

Si la cible est à terre, le tricératops peut effectuer une attaque de
piétinement contre elle en tant qu'action bonus.


##### Actions

***Gore.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 24 (4d8 + 6) points de dégâts
perforants.

***Stomp.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
portée de 1,5 m, une créature couchée. *Touché :* 22 (3d10 + 6) points
de dégâts contondants.




#### Tyrannosaure Rex

*Très grande bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 136 (13d12 + 52)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   25 (+7)   10 (+0)   19 (+4)   2 (-4)    12 (+1)   9 (-1)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 8 (3 900 XP)


##### Actions

***Attaques multiples.*** Le tyrannosaure effectue deux attaques : une
avec sa morsure et une avec sa queue. Il ne peut pas faire les deux
attaques contre la même cible.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 33 (4d12 + 7) points
de dégâts perforants. Si la cible est une créature de taille moyenne ou
plus petite, elle est agrippée pour échapper à DC 17). Jusqu'à la fin
de cet agrippement, la cible est entravée, et le tyrannosaure ne peut
pas mordre une autre cible.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 20 (3d8 + 7) points
de dégâts contondants.





### Dragons, Chromatique


#### Dragon noir adulte

*Très grand dragon, maléfice chaotique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 195 (17d12 + 85)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   14 (+2)   21 (+5)   14 (+2)   13 (+1)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +10, Wis +6, Cha +8

**Compétences** Perception +11, Discrétion +7

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 21

**Langues :** commun, draconique.

**Défi** 14 (11,500 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) dégâts
perforants plus 4 1d8) dégâts d'acide.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 16 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle acide (Recharge 5-6).*** Le dragon exhale de l'acide sur une
ligne de 60 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 18,
subissant 54 (12d8) dégâts d'acide en cas d'échec, ou la moitié des
dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 19 ou subir 13 (2d6 + 6) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon noir ancien

*Dragon Gargantuesque, maléfique chaotique.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 367 (21d20 + 147)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   14 (+2)   25 (+7)   16 (+3)   15 (+2)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +9, Con +14, Wis +9, Cha +11

**Compétences** Perception +16, Discrétion +9

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 26

**Langues :** commun, draconique.

**Défi** 21 (33 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (2d10 + 8) dégâts
perforants plus 9 2d8) dégâts d'acide.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d6 + 8) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 19 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle acide (Recharge 5-6).*** Le dragon exhale de l'acide sur une
ligne de 90 pieds de long et de 10 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 22,
subissant 67 (15d8) dégâts d'acide en cas d'échec, ou la moitié des
dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 23 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon noir Wyrmling

*Dragon moyen, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 33 (6d8 + 6)

**Vitesse** 30 ft., Vol 60 ft., Nage 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   13 (+1)   10 (+0)   11 (+0)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +3, Wis +2, Cha +3

**Compétences** Perception +4, Discrétion +4

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 2 (450 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d10 + 2) dégâts
perforants plus 2 (1d4) dégâts d'acide.

***Souffle acide (Recharge 5-6).*** Le dragon exhale de l'acide sur une
ligne de 15 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité à DC 11,
subissant 22 (5d8) dégâts d'acide en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Jeune Dragon noir

*Grand dragon, maléfique chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 127 (15d10 + 45)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   14 (+2)   17 (+3)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +6, Wis +3, Cha +5

**Compétences** Perception +6, Discrétion +5

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 16

**Langues :** commun, draconique.

**Défi** 7 (2 900 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) dégâts
perforants plus 4 (1d8) dégâts d'acide.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Souffle acide (Recharge 5-6).*** Le dragon exhale de l'acide sur une
ligne de 30 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 14,
subissant 49 (11d8) dégâts d'acide en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Dragon bleu adulte

*Très grand dragon, maléfique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 225 (18d12 + 108)

**Vitesse** 40 ft., terrier 30 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   25 (+7)   10 (+0)   23 (+6)   16 (+3)   15 (+2)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +11, Wis +7, Cha +9

**Compétences** Perception +12, Discrétion +5

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 22

**Langues :** commun, draconique.

**Défi** 16 (15 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 18 (2d10 + 7) dégâts
perforants plus 5 1d10) dégâts de foudre.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d6 + 7) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 16 (2d8 + 7) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 17 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle de foudre (recharge 5-6).*** Le dragon exhale des éclairs sur
une ligne de 90 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 19,
subissant 66 (12d10) dégâts de foudre en cas d'échec, ou la moitié des
dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 20 ou subir 14 (2d6 + 7) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon bleu ancien

*Dragon Gargantuesque, maléfique.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 481 (26d20 + 208)

**Vitesse** 40 ft., creuser 40 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   29 (+9)   10 (+0)   27 (+8)   18 (+4)   17 (+3)   21 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +15, Wis +10, Cha +12

**Compétences** Perception +17, Discrétion +7

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 27

**Langues :** commun, draconique.

**Défi** 23 (50 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 20 (2d10 + 9) dégâts
perforants plus 11 2d10) dégâts de foudre.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 16 (2d6 + 9) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 18 (2d8 + 9) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 20 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle de foudre (recharge 5-6).*** Le dragon exhale des éclairs sur
une ligne de 120 pieds de long et de 10 pieds de large. Chaque créature
de cette ligne doit effectuer un jet de sauvegarde de Dextérité à 23 DC,
subissant 88 (16d10) dégâts de foudre en cas d'échec, ou la moitié des
dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 24 ou subir 16 (2d6 + 9) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon bleu Wyrmling

*Dragon moyen, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 52 (8d8 + 16)

**Vitesse** 30 ft., terrier 15 ft., voler 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   10 (+0)   15 (+2)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +4, Wis +2, Cha +4

**Compétences** Perception +4, Discrétion +2

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 3 (700 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d10 + 3) dégâts
perforants plus 3 (1d6) dégâts de foudre.

***Souffle de foudre (recharge 5-6).*** Le dragon exhale des éclairs sur
une ligne de 30 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 12,
subissant 22 (4d10) dégâts de foudre en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Jeune Dragon bleu

*Grand dragon, maléfique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 152 (16d10 + 64)

**Vitesse** 40 ft., terrier 20 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   10 (+0)   19 (+4)   14 (+2)   13 (+1)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +8, Wis +5, Cha +7

**Compétences** Perception +9, Discrétion +4

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 19

**Langues :** commun, draconique.

**Défi** 9 (5 000 XP)


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 16 (2d10 + 5) dégâts
perforants plus 5 (1d10) dégâts de foudre.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) points de
dégâts tranchants.

***Souffle de foudre (recharge 5-6).*** Le dragon exhale des éclairs sur
une ligne de 60 pieds de long et de 5 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité DC 16,
subissant 55 (10d10) dégâts de foudre en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Dragon vert adulte

*Très grand dragon, maléfique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 207 (18d12 + 90)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   12 (+1)   21 (+5)   18 (+4)   15 (+2)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +10, Wis +7, Cha +8

**Compétences** Supercherie +8, Intuition +7, Perception +12, Persuasion
+8, Furtivité +6.

**Immunités aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 22

**Langues :** commun, draconique.

**Défi** 15 (13 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) dégâts
perforants plus 7 2d6) dégâts de poison.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 16 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle empoisonné (Recharge 5-6).*** Le dragon exhale un gaz toxique
dans un cône de 60 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 18, subissant 56 (16d6) dégâts
de poison en cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 19 ou subir 13 (2d6 + 6) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon vert ancien

*Dragon Gargantuesque, maléfique.*

**Classe d'armure** 21 (armure naturelle)

**Points de vie** 385 (22d20 + 154)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   12 (+1)   25 (+7)   20 (+5)   17 (+3)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +8, Con +14, Wis +10, Cha +11

**Compétences** Supercherie +11, Intuition +10, Perception +17,
Persuasion +11, Furtivité +8.

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 27

**Langues :** commun, draconique.

**Défi** 22 (41 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (2d10 + 8) dégâts
perforants plus 10 3d6) dégâts de poison.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 22 (4d6 + 8) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 19 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle empoisonné (Recharge 5-6).*** Le dragon exhale un gaz toxique
dans un cône de 90 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 22, subissant 77 (22d6) dégâts
de poison en cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 23 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon vert Wyrmling

*Dragon moyen, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 38 (7d8 + 7)

**Vitesse** 30 ft., Vol 60 ft., Nage 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   12 (+1)   13 (+1)   14 (+2)   11 (+0)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +3, Wis +2, Cha +3

**Compétences** Perception +4, Discrétion +3

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 2 (450 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d10 + 2) dégâts
perforants plus 3 (1d6) dégâts de poison.

***Souffle empoisonné (Recharge 5-6).*** Le dragon exhale un gaz toxique
dans un cône de 15 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 11, subissant 21 (6d6) dégâts de
poison en cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Jeune Dragon vert

*Grand dragon, maléfique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 136 (16d10 + 48)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   12 (+1)   17 (+3)   16 (+3)   13 (+1)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +6, Wis +4, Cha +5

**Compétences** Supercherie +5, Perception +7, Furtivité +4

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 17

**Langues :** commun, draconique.

**Défi** 8 (3 900 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) dégâts
perforants plus 7 (2d6) dégâts de poison.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Souffle empoisonné (Recharge 5-6).*** Le dragon exhale un gaz toxique
dans un cône de 30 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 14, subissant 42 (12d6) dégâts
de poison en cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Dragon rouge adulte

*Très grand dragon, maléfice chaotique.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 256 (19d12 + 133)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   10 (+0)   25 (+7)   16 (+3)   13 (+1)   21 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +13, Wis +7, Cha +11

**Compétences** Perception +13, Discrétion +6

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 23

**Langues :** commun, draconique.

**Défi** 17 (18 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 19 (2d10 + 8) dégâts
perforants plus 7 2d6) dégâts de feu.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 15 (2d6 + 8) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 19 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle de feu (Recharge 5-6).*** Le dragon expire du feu dans un
cône de 60 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 21, subissant 63 (18d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 22 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon rouge ancien

*Dragon Gargantuesque, maléfique chaotique.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 546 (28d20 + 252)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
  30 (+10)   10 (+0)   29 (+9)   18 (+4)   15 (+2)   23 (+6)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +16, Wis +9, Cha +13

**Compétences** Perception +16, Discrétion +7

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 26

**Langues :** commun, draconique.

**Défi** 24 (62 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 21 (2d10 + 10)
dégâts perforants plus 14 4d6) dégâts de feu.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d6 + 10) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 19 (2d8 + 10) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 21 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle de feu (Recharge 5-6).*** Le dragon expire du feu dans un
cône de 90 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 24, subissant 91 (26d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 25 ou subir 17 (2d6 + 10) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon rouge Wyrmling

*Dragon moyen, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 75 (10d8 + 30)

**Vitesse** 30 ft., escalade 30 ft., vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   17 (+3)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +5, Wis +2, Cha +4

**Compétences** Perception +4, Discrétion +2

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 4 (1 100 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d10 + 4) dégâts
perforants plus 3 (1d6) dégâts de feu.

***Souffle de feu (Recharge 5-6).*** Le dragon exhale du feu dans un
cône de 15 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 13, subissant 24 (7d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Jeune Dragon rouge

*Grand dragon, maléfique chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 178 (17d10 + 85)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   10 (+0)   21 (+5)   14 (+2)   11 (+0)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +9, Wis +4, Cha +8

**Compétences** Perception +8, Discrétion +4

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 18

**Langues :** commun, draconique.

**Défi** 10 (5 900 XP)


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) dégâts
perforants plus 3 1d6) dégâts de feu.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Souffle de feu (Recharge 5-6).*** Le dragon expire du feu dans un
cône de 30 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 17, subissant 56 (16d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Dragon blanc adulte

*Très grand dragon, maléfice chaotique.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 200 (16d12 + 96)

**Vitesse** 40 ft., terrier 30 ft., voler 80 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   10 (+0)   22 (+6)   8 (-1)    12 (+1)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +11, Wis +6, Cha +6

**Compétences** Perception +11, Discrétion +5

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 21

**Langues :** commun, draconique.

**Défi** 13 (10 000 XP)

***Marche sur glace.*** Le dragon peut se déplacer sur des surfaces
glacées et les escalader sans avoir besoin de faire un test d'aptitude.
De plus, les terrains difficiles composés de glace ou de neige ne lui
coûtent pas de moment supplémentaire.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) dégâts
perforants plus 4 1d8) dégâts de froid.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 14 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle froid (recharge 5-6).*** Le dragon exhale un souffle glacé
dans un cône de 60 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 19, subissant 54 12d8) dégâts de
froid en cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 19 ou subir 13 (2d6 + 6) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon blanc ancien

*Dragon Gargantuesque, maléfique chaotique.*

**Classe d'armure** 20 (armure naturelle)

**Points de vie** 333 (18d20 + 144)

**Vitesse** 40 ft., creuser 40 ft., voler 80 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   26 (+8)   10 (+0)   26 (+8)   10 (+0)   13 (+1)   14 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +14, Wis +7, Cha +8

**Compétences** Perception +13, Discrétion +6

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 23

**Langues :** commun, draconique.

**Défi** 20 (25 000 XP)

***Marche sur glace.*** Le dragon peut se déplacer sur des surfaces
glacées et les escalader sans avoir besoin de faire un test d'aptitude.
De plus, les terrains difficiles composés de glace ou de neige ne lui
coûtent pas de moment supplémentaire.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (2d10 + 8) dégâts
perforants plus 9 2d8) dégâts de froid.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d6 + 8) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 16 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Souffle froid (recharge 5-6).*** Le dragon exhale un souffle glacé
dans un cône de 90 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 22, subissant 72 16d8) dégâts de
froid en cas d'échec, ou la moitié des dégâts en cas de réussite.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 22 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon blanc Wyrmling

*Dragon moyen, maléfique.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 32 (5d8 + 10)

**Vitesse** 30 ft., creuser 15 ft., voler 60 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   10 (+0)   14 (+2)   5 (-3)    10 (+0)   11 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +4, Wis +2, Cha +2

**Compétences** Perception +4, Discrétion +2

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 2 (450 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d10 + 2) dégâts
perforants plus 2 (1d4) dégâts de froid.

***Souffle froid (recharge 5-6).*** Le dragon exhale un souffle glacé de
grêle dans un cône de 15 pieds. Chaque créature dans cette zone doit
effectuer un jet de sauvegarde de Constitution DC 12, subissant 22 (5d8)
dégâts de froid en cas d'échec, ou la moitié des dégâts en cas de
réussite.




#### Jeune Dragon blanc

*Grand dragon, maléfique chaotique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 133 (14d10 + 56)

**Vitesse** 40 ft., terrier 20 ft., voler 80 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   10 (+0)   18 (+4)   6 (-2)    11 (+0)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +7, Wis +3, Cha +4

**Compétences** Perception +6, Discrétion +3

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 16

**Langues :** commun, draconique.

**Défi** 6 (2 300 XP)

***Marche sur glace.*** Le dragon peut se déplacer sur des surfaces
glacées et les escalader sans avoir besoin de faire un test d'aptitude.
De plus, les terrains difficiles composés de glace ou de neige ne lui
coûtent pas de moment supplémentaire.


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) dégâts
perforants plus 4 (1d8) dégâts de froid.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Souffle froid (recharge 5-6).*** Le dragon exhale un souffle glacé
dans un cône de 30 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 15, subissant 45 10d8) dégâts de
froid en cas d'échec, ou la moitié des dégâts en cas de réussite.





### Dragons, Métallique


#### Dragon d'airain adulte

*Très grand dragon, chaotique bon*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 172 (15d12 + 75)

**Vitesse** 40 ft., terrier 30 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   10 (+0)   21 (+5)   14 (+2)   13 (+1)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +10, Wis +6, Cha +8

**Compétences** Histoire +7, Perception +11, Persuasion +8, Discrétion
+5

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 21

**Langues :** commun, draconique.

**Défi** 13 (10 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 16 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu sur une ligne de 60 pieds
de long et de 5 pieds de large. Chaque créature de cette ligne doit
effectuer un jet de sauvegarde de Dextérité DC 18, subissant 45 (13d6)
dégâts de feu en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Souffle du Sommeil.*** Le dragon exhale un gaz somnifère dans un cône
de 60 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 18 ou tomber inconsciente pendant 10
minutes. Cet effet prend fin pour une créature si celle-ci subit des
dégâts ou si quelqu'un utilise une action pour la réveiller.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 19 ou subir 13 (2d6 + 6) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon d'airain ancien

*Dragon Gargantuesque, bon chaotique.*

**Classe d'armure** 20 (armure naturelle)

**Points de vie** 297 (17d20 + 119)

**Vitesse** 40 ft., creuser 40 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   10 (+0)   25 (+7)   16 (+3)   15 (+2)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +13, Wis +8, Cha +10

**Compétences** Histoire +9, Perception +14, Persuasion +10, Discrétion
+6

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 24

**Langues :** commun, draconique.

**Défi** 20 (25 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (2d10 + 8) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d6 + 8) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 18 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes : Souffle de feu. Le dragon exhale du feu sur une
ligne de 90 pieds de long et de 10 pieds de large. Chaque créature de
cette ligne doit effectuer un jet de sauvegarde de Dextérité à 21 DC,
subissant 56 16d6) dégâts de feu en cas d'échec, ou la moitié des
dégâts en cas de réussite.

***Souffle du Sommeil.*** Le dragon exhale un gaz somnifère dans un cône
de 90 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 21 ou tomber inconsciente pendant 10
minutes. Cet effet prend fin pour une créature si celle-ci subit des
dégâts ou si quelqu'un utilise une action pour la réveiller.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa vraie forme
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 22 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon d'airain Wyrmling

*Dragon moyen, bon chaotique.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 16 (3d8 + 3)

**Vitesse** 30 ft., creuser 15 ft., voler 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   13 (+1)   10 (+0)   11 (+0)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +3, Wis +2, Cha +3

**Compétences** Perception +4, Discrétion +2

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 1 (200 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d10 + 2) points de
dégâts perforants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu sur une ligne de 6 mètres
de long et de 1,5 mètre de large. Chaque créature de cette ligne doit
effectuer un jet de sauvegarde de Dextérité à DC 11, subissant 14 (4d6)
dégâts de feu en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Souffle du Sommeil.*** Le dragon exhale un gaz somnifère dans un cône
de 15 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 11 ou tomber inconsciente pendant 1
minute. Cet effet prend fin pour une créature si celle-ci subit des
dégâts ou si quelqu'un utilise une action pour la réveiller.




#### Jeune Dragon d'airain

*Grand dragon, bon chaotique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 110 (13d10 + 39)

**Vitesse** 40 ft., terrier 20 ft., voler 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   17 (+3)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +6, Wis +3, Cha +5

**Compétences** Perception +6, Persuasion +5, Discrétion +3

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 16

**Langues :** commun, draconique.

**Défi** 6 (2 300 XP)


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu sur une ligne de 40 pieds
de long et de 5 pieds de large. Chaque créature de cette ligne doit
effectuer un jet de sauvegarde de Dextérité DC 14, subissant 42 (12d6)
dégâts de feu en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Souffle du Sommeil.*** Le dragon exhale un gaz somnifère dans un cône
de 30 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 14 ou tomber inconsciente pendant 5
minutes. Cet effet prend fin pour une créature si celle-ci subit des
dégâts ou si quelqu'un utilise une action pour la réveiller.




#### Dragon de bronze adulte

*Très grand dragon, légitimement bon.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 212 (17d12 + 102)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   25 (+7)   10 (+0)   23 (+6)   16 (+3)   15 (+2)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +11, Wis +7, Cha +9

**Compétences** Intuition +7, Perception +12, Discrétion +5

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 22

**Langues :** commun, draconique.

**Défi** 15 (13 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 18 (2d10 + 7) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (2d6 + 7) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 16 (2d8 + 7) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 17 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de foudre.*** Le dragon exhale des éclairs sur une ligne de
90 pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 19, subissant 66
(12d10) dégâts de foudre en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Souffle de répulsion.*** Le dragon exhale une énergie répulsive dans
un cône de 30 pieds. Chaque créature dans cette zone doit réussir un jet
de sauvegarde de Force DC 19. En cas d'échec, la créature est repoussée
à 60 pieds du dragon.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 20 ou subir 14 (2d6 + 7) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon de bronze ancien

*Dragon Gargantuesque, légitimement bon.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 444 (24d20 + 192)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   29 (+9)   10 (+0)   27 (+8)   18 (+4)   17 (+3)   21 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +15, Wis +10, Cha +12

**Compétences** Intuition +10, Perception +17, Discrétion +7

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 27

**Langues :** commun, draconique.

**Défi** 22 (41 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 20 (2d10 + 9) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 16 (2d6 + 9) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +16 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 18 (2d8 + 9) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 20 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de foudre.*** Le dragon exhale des éclairs sur une ligne de
120 pieds de long et de 10 pieds de large. Chaque créature de cette
ligne doit effectuer un jet de sauvegarde de Dextérité à 23 DC,
subissant 88 (16d10) dégâts de foudre en cas d'échec, ou la moitié des
dégâts en cas de réussite.

***Souffle de répulsion.*** Le dragon exhale une énergie répulsive dans
un cône de 30 pieds. Chaque créature dans cette zone doit réussir un jet
de sauvegarde de Force DC 23. En cas d'échec, la créature est repoussée
à 60 pieds du dragon.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 24 ou subir 16 (2d6 + 9) dégâts de
contondant et être mise au sol. Le dragon peut ensuite voler jusqu'à la
moitié de sa vitesse de vol.




#### Dragon de bronze Wyrmling

*Dragon moyen, bien intentionné.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 32 (5d8 + 10)

**Vitesse** 30 ft., Vol 60 ft., Nage 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   10 (+0)   15 (+2)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +4, Wis +2, Cha +4

**Compétences** Perception +4, Discrétion +2

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 2 (450 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d10 + 3) points de
dégâts perforants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de foudre.*** Le dragon exhale des éclairs sur une ligne de
40 pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 12, subissant 16
(3d10) dégâts de foudre en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Souffle de répulsion.*** Le dragon exhale une énergie répulsive dans
un cône de 30 pieds. Chaque créature dans cette zone doit réussir un jet
de sauvegarde de Force DC 12. En cas d'échec, la créature est repoussée
à 10 mètres du dragon.




#### Jeune dragon de bronze

*Grand dragon, licite et bon.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 142 (15d10 + 60)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   10 (+0)   19 (+4)   14 (+2)   13 (+1)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +7, Wis +4, Cha +6

**Compétences** Intuition +4, Perception +7, Discrétion +3

**Immunité aux dégâts** de la foudre

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 17

**Langues :** commun, draconique.

**Défi** 8 (3 900 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 16 (2d10 + 5) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) points de
dégâts tranchants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de foudre.*** Le dragon exhale des éclairs sur une ligne de
60 pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 15, subissant 55
(10d10) dégâts de foudre en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Souffle de répulsion.*** Le dragon exhale une énergie répulsive dans
un cône de 30 pieds. Chaque créature dans cette zone doit réussir un jet
de sauvegarde de Force à 15 DC. En cas d'échec, la créature est
repoussée à 40 pieds du dragon.




#### Dragon de cuivre adulte

*Très grand dragon, chaotique bon*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 184 (16d12 + 80)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   12 (+1)   21 (+5)   18 (+4)   15 (+2)   17 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +10, Wis +7, Cha +8

**Compétences** Supercherie +8, Perception +12, Furtivité +6

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 22

**Langues :** commun, draconique.

**Défi** 14 (11,500 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +11 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 15 (2d8 + 6) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 16 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Haleine acide.*** Le dragon exhale de l'acide sur une ligne de 60
pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 18, subissant 54
(12d8) dégâts d'acide en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Souffle Lenteur.*** Le dragon expire du gaz dans un cône de 60 pieds.
Chaque créature dans cette zone doit réussir un jet de sauvegarde de
Constitution DC 18. En cas d'échec, la créature ne peut pas utiliser de
réactions, sa vitesse est réduite de moitié et elle ne peut pas
effectuer plus d'une attaque à son tour.

De plus, la créature peut utiliser soit une action soit une action bonus
à son tour, mais pas les deux. Ces effets durent 1 minute. La créature
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de sauvegarde réussie.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 19 ou subir 13 (2d6 + 6) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon de cuivre ancien

*Dragon Gargantuesque, bon chaotique.*

**Classe d'armure** 21 (armure naturelle)

**Points de vie** 350 (20d20 + 140)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   12 (+1)   25 (+7)   20 (+5)   17 (+3)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +8, Con +14, Wis +10, Cha +11

**Compétences** Supercherie +11, Perception +17, Furtivité +8

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 27

**Langues :** commun, draconique.

**Défi** 21 (33 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (2d10 + 8) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d6 + 8) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +15 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 19 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Haleine acide.*** Le dragon exhale de l'acide sur une ligne de 90
pieds de long et de 10 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 22, subissant 63
(14d8) dégâts d'acide en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Souffle Lenteur.*** Le dragon expire du gaz dans un cône de 90 pieds.
Chaque créature dans cette zone doit réussir un jet de sauvegarde de
Constitution DC 22. En cas d'échec, la créature ne peut pas utiliser de
réactions, sa vitesse est réduite de moitié et elle ne peut pas
effectuer plus d'une attaque à son tour.

De plus, la créature peut utiliser soit une action soit une action bonus
à son tour, mais pas les deux. Ces effets durent 1 minute. La créature
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de sauvegarde réussie.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 23 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon de cuivre Wyrmling

*Dragon moyen, bon chaotique.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 22 (4d8 + 4)

**Vitesse** 30 ft., escalade 30 ft., vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   12 (+1)   13 (+1)   14 (+2)   11 (+0)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +3, Wis +2, Cha +3

**Compétences** Perception +4, Discrétion +3

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 1 (200 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d10 + 2) points de
dégâts perforants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Haleine acide.*** Le dragon exhale de l'acide sur une ligne de 20
pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité à DC 11, subissant 18
(4d8) dégâts d'acide en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Souffle Lenteur.*** Le dragon expire du gaz dans un cône de 15 pieds.
Chaque créature dans cette zone doit réussir un jet de sauvegarde de
Constitution DC 11. En cas d'échec, la créature ne peut pas utiliser de
réactions, sa vitesse est réduite de moitié et elle ne peut pas
effectuer plus d'une attaque à son tour.

De plus, la créature peut utiliser soit une action soit une action bonus
à son tour, mais pas les deux. Ces effets durent 1 minute. La créature
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de sauvegarde réussie.




#### Jeune dragon de cuivre

*Grand dragon, bon chaotique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 119 (14d10 + 42)

**Vitesse** 40 ft., escalade 40 ft., vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   12 (+1)   17 (+3)   16 (+3)   13 (+1)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +6, Wis +4, Cha +5

**Compétences** Supercherie +5, Perception +7, Furtivité +4

**Immunité aux dommages causés par** l'acide

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 17

**Langues :** commun, draconique.

**Défi** 7 (2 900 XP)


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 15 (2d10 + 4) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Haleine acide.*** Le dragon exhale de l'acide sur une ligne de 40
pieds de long et de 5 pieds de large. Chaque créature de cette ligne
doit effectuer un jet de sauvegarde de Dextérité DC 14, subissant 40
(9d8) dégâts d'acide en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Souffle Lenteur.*** Le dragon expire du gaz dans un cône de 30 pieds.
Chaque créature dans cette zone doit réussir un jet de sauvegarde de
Constitution DC 14. En cas d'échec, la créature ne peut pas utiliser de
réactions, sa vitesse est réduite de moitié et elle ne peut pas
effectuer plus d'une attaque à son tour.

De plus, la créature peut utiliser soit une action soit une action bonus
à son tour, mais pas les deux. Ces effets durent 1 minute. La créature
peut répéter le jet de sauvegarde à la fin de chacun de ses tours,
mettant fin à l'effet sur elle-même en cas de sauvegarde réussie.




#### Dragon d'or adulte

*Très grand dragon, légitimement bon.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 256 (19d12 + 133)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   14 (+2)   25 (+7)   16 (+3)   15 (+2)   24 (+7)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +8, Con +13, Wis +8, Cha +13

**Compétences** Intuition +8, Perception +14, Persuasion +13, Discrétion
+8.

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 24

**Langues :** commun, draconique.

**Défi** 17 (18 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 19 (2d10 + 8) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 15 (2d6 + 8) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +14 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 21 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu dans un cône de 60 pieds.
Chaque créature dans cette zone doit effectuer un jet de sauvegarde de
Dextérité DC 21, subissant 66 (12d10) dégâts de feu en cas d'échec, ou
la moitié des dégâts en cas de réussite.

***Souffle affaiblissant.*** Le dragon exhale du gaz dans un cône de 60
pieds. Chaque créature dans cette zone doit réussir un jet de sauvegarde
de Force DC 21 ou être désavantagée aux jets d'attaque basés sur la
Force, aux tests de Force et aux jets de sauvegarde de Force pendant 1
minute. Une créature peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 22 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon d'or ancien

*Dragon Gargantuesque, légitimement bon.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 546 (28d20 + 252)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
  30 (+10)   14 (+2)   29 (+9)   18 (+4)   17 (+3)   28 (+9)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +9, Con +16, Wis +10, Cha +16

**Compétences** Intuition +10, Perception +17, Persuasion +16,
Discrétion +9

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 27

**Langues :** commun, draconique.

**Défi** 24 (62 000 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 21 (2d10 + 10)
points de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d6 + 10) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 19 (2d8 + 10) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 24 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu dans un cône de 90 pieds.
Chaque créature dans cette zone doit effectuer un jet de sauvegarde de
Dextérité DC 24, subissant 71 (13d10) dégâts de feu en cas d'échec, ou
la moitié des dégâts en cas de réussite.

***Souffle affaiblissant.*** Le dragon expire du gaz dans un cône de 90
pieds. Chaque créature dans cette zone doit réussir un jet de sauvegarde
de Force DC 24 ou être désavantagée aux jets d'attaque basés sur la
Force, aux tests de Force et aux jets de sauvegarde de Force pendant 1
minute. Une créature peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 25 ou subir 17 (2d6 + 10) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Wyrmling Dragon d'or

*Dragon moyen, bien intentionné.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 60 (8d8 + 24)

**Vitesse** 30 ft., Vol 60 ft., Nage 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   14 (+2)   17 (+3)   14 (+2)   11 (+0)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +5, Wis +2, Cha +5

**Compétences** Perception +4, Discrétion +4

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 3 (700 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d10 + 4) points de
dégâts perforants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu dans un cône de 15 pieds.
Chaque créature dans cette zone doit effectuer un jet de sauvegarde de
Dextérité DC 13, subissant 22 (4d10) dégâts de feu en cas d'échec, ou
la moitié des dégâts en cas de réussite.

***Souffle affaiblissant.*** Le dragon exhale du gaz dans un cône de 15
pieds. Chaque créature dans cette zone doit réussir un jet de sauvegarde
de Force DC 13 ou être désavantagée aux jets d'attaque basés sur la
Force, aux tests de Force et aux jets de sauvegarde de Force pendant 1
minute. Une créature peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.




#### Jeune Dragon d'or

*Grand dragon, licite et bon.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 178 (17d10 + 85)

**Vitesse** 40 ft., Vol 80 ft., Nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   14 (+2)   21 (+5)   16 (+3)   13 (+1)   20 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +9, Wis +5, Cha +9

**Compétences** Intuition +5, Perception +9, Persuasion +9, Discrétion
+6.

**Immunité aux dommages causés par** le feu

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 19

**Langues :** commun, draconique.

**Défi** 10 (5 900 XP)

***Amphibie.*** Le dragon peut respirer de l'air et de l'eau.


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle de feu.*** Le dragon exhale du feu dans un cône de 30 pieds.
Chaque créature dans cette zone doit effectuer un jet de sauvegarde de
Dextérité DC 17, subissant 55 (10d10) dégâts de feu en cas d'échec, ou
la moitié des dégâts en cas de réussite.

***Souffle affaiblissant.*** Le dragon expire du gaz dans un cône de 30
pieds. Chaque créature dans cette zone doit réussir un jet de sauvegarde
de Force DC 17 ou être désavantagée aux jets d'attaque basés sur la
Force, aux tests de Force et aux jets de sauvegarde de Force pendant 1
minute. Une créature peut répéter le jet de sauvegarde à la fin de
chacun de ses tours, mettant fin à l'effet sur elle-même en cas de
réussite.




#### Dragon d'argent adulte

*Très grand dragon, légitimement bon.*

**Classe d'armure** 19 (armure naturelle)

**Points de vie** 243 (18d12 + 126)

**Vitesse** 40 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   10 (+0)   25 (+7)   16 (+3)   13 (+1)   21 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +12, Wis +6, Cha +10

**Compétences** Arcane +8, Histoire +8, Perception +11, Discrétion +5

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 21

**Langues :** commun, draconique.

**Défi** 16 (15 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 19 (2d10 + 8) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 15 (2d6 + 8) points de
dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +13 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 17 (2d8 + 8) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 18 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle froid.*** Le dragon exhale un souffle glacé dans un cône de
60 pieds. Chaque créature dans cette zone doit effectuer un jet de
sauvegarde de Constitution DC 20, subissant 58 (13d8) dégâts de froid en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Souffle paralysé.*** Le dragon exhale un gaz paralysant dans un cône
de 60 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 20 ou être paralysée pendant 1 minute. Une
créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 3 mètres du dragon doit réussir un jet
de sauvegarde de Dextérité DC 21 ou subir 15 (2d6 + 8) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon d'argent ancien

*Dragon Gargantuesque, légitimement bon.*

**Classe d'armure** 22 (armure naturelle)

**Points de vie** 487 (25d20 + 225)

**Vitesse** 40 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
  30 (+10)   10 (+0)   29 (+9)   18 (+4)   15 (+2)   23 (+6)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +7, Con +16, Wis +9, Cha +13

**Compétences** Mystère +11, Histoire +11, Perception +16, Discrétion
+7.

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 60 ft., vision dans le noir 120 ft., Perception
passive 26

**Langues :** commun, draconique.

**Défi** 23 (50 000 XP)

***Résistance légendaire (3/Jour).*** Si le dragon échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.


##### Actions

***Attaques multiples.*** Le dragon peut utiliser sa Présence
effrayante. Il effectue alors trois attaques : une avec sa morsure et
deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 21 (2d10 + 10)
points de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d6 + 10) points
de dégâts tranchants.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +17 pour
toucher, allonge de 6 mètres, une cible. *Touché :* 19 (2d8 + 10) points
de dégâts contondants.

***Présence effrayante.*** Chaque créature du choix du dragon qui se
trouve à moins de 120 pieds de lui et qui est consciente de sa présence
doit réussir un jet de sauvegarde de Sagesse DC 21 ou être effrayée
pendant 1 minute. Une créature peut répéter le jet de sauvegarde à la
fin de chacun de ses tours, mettant fin à l'effet sur elle-même en cas
de réussite. Si le jet de sauvegarde d'une créature est réussi ou si
l'effet prend fin pour elle, la créature est immunisée contre la
présence effrayante du dragon pendant les 24 heures suivantes.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle froid.*** Le dragon exhale un souffle glacé dans un cône de
90 pieds. Chaque créature dans cette zone doit effectuer un jet de
sauvegarde de Constitution DC 24, subissant 67 (15d8) dégâts de froid en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Souffle paralysé.*** Le dragon exhale un gaz paralysant dans un cône
de 90 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 24 ou être paralysée pendant 1 minute. Une
créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.

***Changement de forme.*** Le dragon se métamorphose magiquement en un
humanoïde ou une bête dont le niveau de difficulté n'est pas supérieur
au sien, ou reprend sa véritable forme. Il revient à sa forme réelle
s'il meurt. Tout équipement qu'il porte est absorbé ou porté par la
nouvelle forme (au choix du dragon).

Sous une nouvelle forme, le dragon conserve son alignement, ses points
de vie, ses dés de réussite, sa capacité à parler, ses compétences, sa
Résistance légendaire, ses actions de repaire et ses scores
d'Intelligence, de Sagesse et de Charisme, ainsi que cette action. Ses
statistiques et capacités sont autrement remplacées par celles de la
nouvelle forme, à l'exception des caractéristiques de classe ou des
actions légendaires de cette forme.



##### Actions légendaires

Le dragon peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le dragon récupère les actions légendaires dépensées au début de son
tour.

***Détection.*** Le dragon effectue un test de Sagesse (Perception).

***Attaque de la queue.*** Le dragon effectue une attaque de queue.

***Attaque des ailes (coûte 2 actions).*** Le dragon bat des ailes.
Chaque créature située à moins de 15 pieds du dragon doit réussir un jet
de sauvegarde de Dextérité DC 25 ou subir 17 (2d6 + 10) dégâts de
contondant et être mise à terre. Le dragon peut ensuite voler jusqu'à
la moitié de sa vitesse de vol.




#### Dragon d'argent Wyrmling

*Dragon moyen, bien intentionné.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 45 (6d8 + 18)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   17 (+3)   12 (+1)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +2, Con +5, Wis +2, Cha +4

**Compétences** Perception +4, Discrétion +2

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 14

**Langues** draconiques

**Défi** 2 (450 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d10 + 4) points de
dégâts perforants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle froid.*** Le dragon exhale un souffle glacé dans un cône de
15 pieds. Chaque créature dans cette zone doit effectuer un jet de
sauvegarde de Constitution DC 13, subissant 18 (4d8) dégâts de froid en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Souffle paralysé.*** Le dragon exhale un gaz paralysant dans un cône
de 15 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 13 ou être paralysée pendant 1 minute. Une
créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.




#### Jeune Dragon d'argent

*Grand dragon, licite et bon.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 168 (16d10 + 80)

**Vitesse** 40 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   10 (+0)   21 (+5)   14 (+2)   11 (+0)   19 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +4, Con +9, Wis +4, Cha +8

**Compétences** Mystère +6, Histoire +6, Perception +8, Discrétion +4.

**Immunité aux dommages causés par** le froid

**Sens** vision aveugle 30 ft., vision dans le noir 120 ft., Perception
passive 18

**Langues :** commun, draconique.

**Défi** 9 (5 000 XP)


##### Actions

***Attaques multiples.*** Le dragon effectue trois attaques : une avec
sa morsure et deux avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 17 (2d10 + 6) points
de dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) points de
dégâts tranchants.

***Armes à souffle (recharge 5-6).*** Le dragon utilise l'une des armes
à souffle suivantes.

***Souffle froid.*** Le dragon exhale un souffle glacé dans un cône de
30 pieds. Chaque créature dans cette zone doit effectuer un jet de
sauvegarde de Constitution DC 17, subissant 54 (12d8) dégâts de froid en
cas d'échec, ou la moitié des dégâts en cas de réussite.

***Souffle paralysé.*** Le dragon exhale un gaz paralysant dans un cône
de 30 pieds. Chaque créature dans cette zone doit réussir un jet de
sauvegarde de Constitution DC 17 ou être paralysée pendant 1 minute. Une
créature peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.





### Élémentaires


#### Élémentaire de l'air

*Grand élémentaire, neutre*

**Classe d'armure** 15

**Points de vie** 90 (12d10 + 24)

**Vitesse** 0 ft, Vol 90 ft (vol stationnaire)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   20 (+5)   14 (+2)   6 (-2)    10 (+0)   6 (-2)

  -----------------------------------------------------------

**Résistance aux dégâts**: foudre, tonnerre ; contondant, perforant et
tranchant des attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** épuisement, agrippé, paralysé, pétrifié, empoisonné,
couché, entravé, inconscient

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** aériennes

**Défi** 5 (1 800 XP)

***Forme aérienne.*** L'élémentaire peut entrer dans l'espace d'une
créature hostile et s'y arrêter. Il peut se déplacer dans un espace
aussi étroit qu'un pouce de large sans se serrer.


##### Actions

***Attaques multiples.*** L'élémentaire effectue deux attaques de type
slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 14 (2d8 + 5) points de dégâts
contondants.

***Tourbillon (Recharge 4-6).*** Chaque créature dans l'espace de
l'élémentaire doit effectuer un jet de sauvegarde de Force DC 13. En
cas d'échec, la cible subit 15 (3d8 + 2) points de dégâts de contondant
et est projetée à 20 pieds de l'élémentaire dans une direction
aléatoire et mise à terre. Si la cible lancée heurte un objet, tel
qu'un mur ou le sol, elle subit 3 (1d6) points de dégâts de matraquage
tous les 3 mètres où elle a été projetée. Si la cible est lancée sur une
autre créature, celle-ci doit réussir un jet de sauvegarde de Dextérité
DC 13 ou subir les mêmes dégâts et être mise à plat ventre.

Si le jet de sauvegarde est réussi, la cible subit la moitié des dégâts
de contondant et n'est pas projetée au loin ou mise à terre.




#### Élémentaire de la terre

*Grand élémentaire, neutre*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 126 (12d10 + 60)

**Vitesse** 30 ft., creuser 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   8 (-1)    20 (+5)   5 (-3)    10 (+0)   5 (-3)

  -----------------------------------------------------------

Tonnerre de**vulnérabilités aux dégâts**

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** épuisement, paralysé, pétrifié, empoisonné,
inconscient.

**Sens** Vision dans le noir 60 pieds, sens des secousses 60 pieds,
Perception passive 10

**Langues** glaiseux

**Défi** 5 (1 800 XP)

***Earth Glide.*** L'élémentaire peut creuser dans la terre et la
pierre non magiques et non travaillées. Ce faisant, l'élémentaire ne
perturbe pas la matière qu'il traverse.

***Monstre de siège.*** L'élémentaire inflige des dégâts doubles aux
objets et aux structures.


##### Actions

***Attaques multiples.*** L'élémentaire effectue deux attaques de type
slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 14 (2d8 + 5) points de dégâts
contondants.




#### Élémentaire du feu

*Grand élémentaire, neutre*

**Classe d'armure** 13

**Points de vie** 102 (12d10 + 36)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   17 (+3)   16 (+3)   6 (-2)    10 (+0)   7 (-2)

  -----------------------------------------------------------

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunités aux dommages**: feu, poison.

**État Immunités** épuisement, agrippé, paralysé, pétrifié, empoisonné,
couché, entravé, inconscient

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Ardent

**Défi** 5 (1 800 XP)

***Forme de feu.*** L'élémentaire peut se déplacer dans un espace aussi
étroit que 1 pouce de large sans se serrer. Une créature qui touche
l'élémentaire ou le frappe avec une attaque de mêlée alors qu'elle se
trouve à moins de 1,5 mètre de lui subit 5 (1d10) dégâts de feu. De
plus, l'élémentaire peut entrer dans l'espace d'une créature hostile
et s'y arrêter. La première fois qu'il entre dans l'espace d'une
créature lors d'un tour, celle-ci subit 5 (1d10) dégâts de feu et prend
feu ; jusqu'à ce que quelqu'un prenne une action pour éteindre le feu,
la créature subit 5 (1d10) dégâts de feu au début de chacun de ses
tours.

***Illumination.*** L'élémentaire diffuse une lumière vive dans un
rayon de 30 pieds et une lumière chétive dans un rayon supplémentaire de
30 pieds.

***Susceptibilité à l'eau.*** Pour chaque mètre cinquante (1,5 m) où
l'élémentaire se déplace dans l'eau, ou pour chaque gallon d'eau
éclaboussé sur lui, il subit 1 dégât de froid.


##### Actions

***Attaques multiples.*** L'élémentaire effectue deux attaques au
toucher.

***Toucher.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts de
feu. Si la cible est une créature ou un objet inflammable, elle
s'enflamme. Jusqu'à ce qu'une créature prenne une action pour
éteindre le feu, la cible subit 5 (1d10) dégâts de feu au début de
chacun de ses tours.




#### Élémentaire de l'eau

*Grand élémentaire, neutre*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 114 (12d10 + 48)

**Vitesse** 30 ft., nager 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   14 (+2)   18 (+4)   5 (-3)    10 (+0)   8 (-1)

  -----------------------------------------------------------

**Résistance aux dégâts** acide ; contondant, perforant et tranchant des
attaques non magiques.

**Immunité aux dommages** poison

**État Immunités** épuisement, agrippé, paralysé, pétrifié, empoisonné,
couché, entravé, inconscient

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Aquatique

**Défi** 5 (1 800 XP)

***Forme d'eau.*** L'élémentaire peut entrer dans l'espace d'une
créature hostile et s'y arrêter. Il peut se déplacer dans un espace
aussi étroit qu'un pouce de large sans se serrer.

***Gel.*** Si l'élémentaire subit des dégâts de froid, il gèle
partiellement ; sa vitesse est réduite de 6 mètres jusqu'à la fin de
son prochain tour.


##### Actions

***Attaques multiples.*** L'élémentaire effectue deux attaques de type
slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de dégâts
contondants.

***Déluge (Recharge 4-6).*** Chaque créature dans l'espace de
l'élémentaire doit effectuer un jet de sauvegarde de Force DC 15. En
cas d'échec, la cible subit 13 (2d8 + 4) points de dégâts contondants.
Si elle est Grand ou plus petite, elle est également agrippée (fuite DC
14). Jusqu'à la fin de cet agrippement, la cible est entravée et
incapable de respirer, sauf si elle peut respirer de l'eau. Si le jet
de sauvegarde est réussi, la cible est poussée hors de l'espace de
l'élémentaire.

L'élémentaire peut agripper une créature Grand ou jusqu'à deux
créatures moyennes ou plus petites à la fois. Au début de chaque tour de
l'élémentaire, chaque cible agrippée par lui subit 13 (2d8 + 4) dégâts
de contondant.

Une créature située à moins de 1,5 mètre de l'élémentaire peut en
extraire une créature ou un objet en effectuant une action pour faire un
jet de Force DC 14 et en le réussissant.





### Champignons


#### Criard

*Plante moyenne, sans alignement*

**Classe d'armure** 5

**Points de vie** 13 (3d8)

**Vitesse** 0 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   1 (-5)    1 (-5)    10 (+0)   1 (-5)    3 (-4)    1 (-5)

  -----------------------------------------------------------

**Cécité Immunités** aveuglées, assourdies, effrayées

**Sens** vision aveugle 30 ft. (aveugle au-delà de ce rayon), Perception
passive 6

**Langues** -

**Défi** 0 (10 XP)

***Fausse apparence.*** Alors que le Criard reste immobile, il est
impossible de le distinguer d'un champignon ordinaire.


##### Réactions

***Cri.*** Lorsqu'une lumière vive ou une créature se trouve à moins de
30 pieds du Criard, celui-ci émet un hurlement audible dans un rayon de
300 pieds. Le Criard continue à hurler jusqu'à ce que la perturbation
se mette hors de portée et pendant 1d4 des tours du Criard par la suite.




#### Moisissure violette

*Plante moyenne, sans alignement*

**Classe d'armure** 5

**Points de vie** 18 (4d8)

**Vitesse** 5 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    1 (-5)    10 (+0)   1 (-5)    3 (-4)    1 (-5)

  -----------------------------------------------------------

**Cécité Immunités** aveuglées, assourdies, effrayées

**Sens** vision aveugle 30 ft. (aveugle au-delà de ce rayon), Perception
passive 6

**Langues** -

**Défi** 1/4 (50 XP)

***Fausse apparence.*** Alors que le champignon violet reste immobile,
il est impossible de le distinguer d'un champignon ordinaire.


##### Actions

***Attaques multiples.*** Le champignon effectue 1d4 attaques de Toucher
pourrissant.

***Toucher pourri.*** *Attaque avec une arme de corps-à-corps :* +2 au
toucher, allonge de 10 pieds, une créature. *Touché :* 4 (1d8) dégâts
nécrotiques.





### Génies


#### Djinn

*Grand élémentaire, chaotique bon*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 161 (14d10 + 84)

**Vitesse** 30 ft., Vol 90 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   15 (+2)   22 (+6)   15 (+2)   16 (+3)   20 (+5)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Wis +7, Cha +9

**Immunités aux dommages :** foudre, tonnerre.

**Sens** Vision dans le noir 120 ft., Perception passive 13

**Langues** aériennes

**Défi** 11 (7 200 XP)

***Démission élémentaire.*** Si le djinn meurt, son corps se désintègre
dans une brise chaude, ne laissant derrière lui que l'équipement que le
djinn portait ou transportait.

***Lanceur de sorts inné.*** La caractéristique d'incantation innée du
djinn est le Charisme (sauvegarde contre les sorts DC 17, +9 pour
toucher avec les attaques de sorts). Il peut lancer de manière innée les
sorts suivants, ne nécessitant aucun composant matériel :

À volonté : *Détection du mal et du bien*,
*Détection de la magie*, Vague
*tonnante*.

3/jour chacun : Création de nourriture *et
d'eau*,
*langues* sur le
*vent*

1/jour chacun : *conjuration élémentaire*
(élémentaire air uniquement), *création*, *forme
gazeuse*, *image
majeure*


##### Actions

***Attaques multiples.*** Le Djinn effectue trois attaques au cimeterre.

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) points de
dégâts tranchants plus 3 1d6 points de dégâts de foudre ou de tonnerre
(au choix du Djinn).

***Créer un tourbillon.*** Un cylindre d'air tourbillonnant de 5 pieds
de rayon et de 30 pieds de haut se forme magiquement sur un point que le
djinn peut voir dans un rayon de 120 pieds autour de lui. Le tourbillon
dure aussi longtemps que le djinn maintient sa concentration (comme
s'il se concentrait sur un sort). Toute créature, à l'exception du
djinni, qui entre dans le tourbillon doit réussir un jet de sauvegarde
de Force DC 18 ou être entravée par celui-ci. Le djinn peut déplacer le
tourbillon jusqu'à 60 pieds en tant qu'action, et les créatures
entravées par le tourbillon se déplacent avec lui. Le tourbillon prend
fin si le djinn le perd de vue.

Une créature peut utiliser son action pour libérer une créature entravée
par le tourbillon, y compris elle-même, en réussissant un test de Force
DC 18. Si le test est réussi, la créature n'est plus retenue et se
déplace vers l'espace le plus proche en dehors du tourbillon.




#### Éfrites

*Grand élémentaire, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 200 (16d10 + 112)

**Vitesse** 40 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   12 (+1)   24 (+7)   16 (+3)   15 (+2)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +7, Wis +6, Cha +7

**Immunité aux dommages causés par** le feu

**Sens** Vision dans le noir 120 ft., Perception passive 12

**Langues** Ardent

**Défi** 11 (7 200 XP)

***Mort élémentaire.*** Si l'Éfrit meurt, son corps se désintègre dans
un éclair de feu et une bouffée de fumée, ne laissant derrière lui que
l'équipement que l'Éfrit portait.

***Lanceur de sorts inné.*** La caractéristique d'incantation innée de
l'Éfrit est le Charisme (sauvegarde contre les sorts DC 15, +7 pour
toucher avec les attaques de sorts). Il peut lancer les sorts suivants
de manière innée, sans nécessiter de composants matériels :

A volonté : *détecter la magie*

3/jour : *agrandissement/rapetissement*,
*langues*

1/jour chacun : *conjuration d'élémentaire*
(élémentaire de feu uniquement), *forme gazeuse*,
*invisibilité*,
*changement de plan* de feu


##### Actions

***Attaques multiples.*** L'Éfrit effectue deux attaques au cimeterre
ou utilise deux fois sa flamme hurlante.

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d6 + 6) dégâts
tranchants plus 7 2d6) dégâts de feu.

***Hurlement de flammes.*** Attaque avec un sort à distance : +7 pour
toucher, portée de 120 ft., une cible. *Touché :* 17 (5d6) dégâts de
feu.





### Goules


#### Blême

*Mort-vivant moyen, maléfique chaotique.*

**Classe d'armure** 13

**Points de vie** 36 (8d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   17 (+3)   10 (+0)   11 (+0)   10 (+0)   8 (-1)

  -----------------------------------------------------------

**Résistances aux dégâts** nécrotiques

**Immunité aux dommages** poison

**Immunités d'état :** charme, épuisement, empoisonnement.

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Roturiers** Communs

**Défi** 2 (450 XP)

***Stench.*** Toute créature qui commence son tour à moins de 1,5 m du
blême doit réussir un jet de sauvegarde de Constitution DC 10 ou être
empoisonnée jusqu'au début de son prochain tour. En cas de jet de
sauvegarde réussi, la créature est immunisée contre la puanteur du blême
pendant 24 heures.

***Défiance tournante.*** La goule et toutes les goules situées à moins
de 30 pieds d'elle ont un avantage aux jets de sauvegarde contre les
effets qui transforment les morts-vivants.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 12 (2d8 + 3) points
de dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts
tranchants. Si la cible est une créature autre qu'un mort-vivant, elle
doit réussir un jet de sauvegarde de Constitution DC 10 ou être
paralysée pendant 1 minute. La cible peut répéter le jet de sauvegarde à
la fin de chacun de ses tours, mettant fin à l'effet sur elle-même en
cas de réussite.




#### Goule

*Mort-vivant moyen, maléfique chaotique.*

**Classe d'armure** 12

**Points de vie** 22 (5d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   15 (+2)   10 (+0)   7 (-2)    10 (+0)   6 (-2)

  -----------------------------------------------------------

**Immunité aux dommages** poison

**Immunités d'état :** charme, épuisement, empoisonnement.

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Roturiers** Communs

**Défi** 1 (200 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 9 (2d6 + 2) dégâts
perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) dégâts
tranchants. Si la cible est une créature autre qu'un elfe ou un
mort-vivant, elle doit réussir un jet de sauvegarde de Constitution DC
10 ou être paralysée pendant 1 minute. La cible peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet sur
elle-même en cas de réussite.





### Géants


#### Géant des nuages

*Très grand géant, neutre bon (50%) ou neutre mauvais (50%)*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 200 (16d12 + 96)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   27 (+8)   10 (+0)   22 (+6)   12 (+1)   16 (+3)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +10, Wis +7, Cha +7

**Compétences** Intuition +7, Perception +7

**Sens** passif Perception 17

**Langues** Commun, Géant

**Défi** 9 (5 000 XP)

***Odorat aiguisé.*** Le géant a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Lanceur de sorts inné.*** La caractéristique innée d'incantation du
géant est le Charisme. Il peut lancer les sorts suivants de manière
innée, sans nécessiter de composants matériels :

À volonté : *détection de la magie*, *nuage de
brouillard*.

3/jour chacun : *Feuille*, Pas
*brumeux*

1/jour chacun : contrôle du *climat*, *forme
gazeuse*


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques morgenstern.

***Morgenstern.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 21 (3d8 + 8) points
de dégâts perforants.

***Rock.*** *Attaque avec une arme à distance :* +12 pour toucher,
portée de 60/240 pieds, une cible. *Touché :* 30 (4d10 + 8) points de
dégâts contondants.




#### Géant du feu

*Très grand géant, maléfique.*

**Classe d'armure** 18 (harnois)

**Points de vie** 162 (13d12 + 78)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   25 (+7)   9 (-1)    23 (+6)   10 (+0)   14 (+2)   13 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +3, Con +10, Cha +5

**Compétences** Athlétisme +11, Perception +6

**Immunité aux dommages causés par** le feu

**Sens** passif Perception 16

**Langues** Géant

**Défi** 9 (5 000 XP)


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques d'épée.

***Épée à deux mains.*** *Attaque avec une arme de corps-à-corps :* +11
pour toucher, allonge de 10 pieds, une cible. *Touché :* 28 (6d6 + 7)
points de dégâts tranchants.

***Rock.*** *Attaque avec une arme à distance :* +11 pour toucher,
portée de 60/240 pieds, une cible. *Touché :* 29 (4d10 + 7) points de
dégâts contondants.




#### Géant du givre

*Très grand géant, mal neutre.*

**Classe d'armure** 15 (armure en patchwork)

**Points de vie** 138 (12d12 + 60)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   9 (-1)    21 (+5)   9 (-1)    10 (+0)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +8, Wis +3, Cha +4

**Compétences** Athlétisme +9, Perception +3

**Immunité aux dommages causés par** le froid

**Sens** passif Perception 13

**Langues** Géant

**Défi** 8 (3 900 XP)


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques à la hache.

***Hache à deux mains.*** *Attaque avec une arme de corps-à-corps :* +9
pour toucher, allonge de 10 pieds, une cible. *Touché :* 25 (3d12 + 6)
points de dégâts tranchants.

***Rock.*** *Attaque avec une arme à distance :* +9 pour toucher, portée
de 60/240 pieds, une cible. *Touché :* 28 (4d10 + 6) points de dégâts
contondants.




#### Géant des collines

*Très grand géant, le mal chaotique*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 105 (10d12 + 40)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   8 (-1)    19 (+4)   5 (-3)    9 (-1)    6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** passif Perception 12

**Langues** Géant

**Défi** 5 (1 800 XP)


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques massue.

***Massue.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 18 (3d8 + 5) points
de dégâts contondants.

***Rock.*** *Attaque avec une arme à distance :* +8 pour toucher, portée
de 60/240 pieds, une cible. *Touché :* 21 (3d10 + 5) points de dégâts
contondants.




#### Géant des pierres

*Très grand géant, neutre.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 126 (11d12 + 55)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   15 (+2)   20 (+5)   10 (+0)   12 (+1)   9 (-1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +5, Con +8, Wis +4

**Compétences** Athlétisme +12, Perception +4

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** Géant

**Défi** 7 (2 900 XP)

***Camouflage dans la pierre.*** Le géant a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain rocheux.


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques massue.

***Massue.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 19 (3d8 + 6) points
de dégâts contondants.

***Rock.*** *Attaque avec une arme à distance :* +9 pour toucher, portée
de 60/240 pieds, une cible. *Touché :* 28 (4d10 + 6) points de dégâts
contondants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 17 ou être mise au sol.



##### Réactions

***Attrape-roche.*** Si un rocher ou un objet similaire est lancé sur le
géant, celui-ci peut, en réussissant un jet de sauvegarde de Dextérité
DC 10, attraper le missile et n'en subir aucun dégât de contondant.




#### Géant des tempêtes

*Très grand géant, chaotique bon*

**Classe d'armure** 16 (armure d'écailles)

**Points de vie** 230 (20d12 + 100)

**Vitesse** 50 ft., nager 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   29 (+9)   14 (+2)   20 (+5)   16 (+3)   18 (+4)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +14, Con +10, Wis +9, Cha +9.

**Compétences** Mystère +8, Athlétisme +14, Histoire +8, Perception +9

**Résistance** aux**dégâts à** froid

**Immunités aux dommages :** foudre, tonnerre.

Perception passive des**sens** 19

**Langues** commune, géante

**Défi** 13 (10 000 XP)

***Amphibie.*** Le géant peut respirer de l'air et de l'eau.

***Lanceur de sorts inné.*** La caractéristique innée d'incantation du
géant est le Charisme (sauvegarde contre les sorts DC 17). Il peut
lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel :

À volonté : *détection de la magie*, *chute de
plumes*.

3/jour chacun : *contrôle du climat*, *respiration
aquatique*


##### Actions

***Attaques multiples.*** Le géant effectue deux attaques d'épée.

***Épée à deux mains.*** *Attaque avec une arme de corps-à-corps :* +14
pour toucher, allonge de 10 pieds, une cible. *Touché :* 30 (6d6 + 9)
points de dégâts tranchants.

***Rock.*** *Attaque avec une arme à distance :* +14 pour toucher,
portée de 60/240 pieds, une cible. *Touché :* 35 (4d12 + 9) points de
dégâts contondants.

***Coup de foudre (recharge 5-6).*** Le géant lance un éclair magique
sur un point qu'il peut voir dans un rayon de 500 pieds autour de lui.
Chaque créature située dans un rayon de 3 mètres de ce point doit
effectuer un jet de sauvegarde de Dextérité DC 17, subissant 54 12d8)
dégâts de foudre en cas d'échec, ou la moitié des dégâts en cas de
réussite.





### Golems


#### Golem d'argile

*Grande construction, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 133 (14d10 + 56)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   9 (-1)    18 (+4)   3 (-4)    8 (-1)    1 (-5)

  -----------------------------------------------------------

**Immunités aux dommages**: acide, poison, psychique ; contondant,
perforant et tranchant des attaques non magiques qui ne sont pas
adamantines.

**Condition Immunités** charmé, épuisement, effrayé, paralysé, pétrifié,
empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** comprend les langues de son créateur mais ne peut pas parler

**Défi** 9 (5 000 XP)

***Absorption d'acide.*** Chaque fois que le golem subit des dégâts
d'acide, il ne subit aucun dégât et regagne à la place un nombre de
points de vie égal aux dégâts d'acide infligés.

***Berserk.*** Chaque fois que le golem commence son tour avec 60 points
de vie ou moins, lancez un d6. Sur un 6, le golem devient berserk. À
chacun de ses tours pendant qu'il est en état de berserker, le golem
attaque la créature la plus proche qu'il peut voir. Si aucune créature
n'est assez proche pour se déplacer et attaquer, le golem attaque un
objet, avec une préférence pour un objet plus petit que lui. Une fois
que le golem est devenu fou furieux, il continue à le faire jusqu'à ce
qu'il soit détruit ou qu'il récupère tous ses points de vie.

***Forme immuable.*** Le golem est immunisé contre tout sort ou effet
qui altérerait sa forme.

***Résistance à la magie.*** Le golem a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du golem sont magiques.


##### Actions

***Attaques multiples.*** Le golem effectue deux attaques de type slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 16 (2d10 + 5) points de dégâts
contondants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Constitution DC 15 ou voir son maximum de points de vie
réduit d'un montant égal aux dégâts subis. La cible meurt si cette
attaque réduit son maximum de points de vie à 0. La réduction dure
jusqu'à ce qu'elle soit annulée par le sort Restauration
*supérieure* ou une autre magie.

***Hâte (Recharge 5-6).*** Jusqu'à la fin de son prochain tour, le
golem bénéficie magiquement d'un bonus de +2 à sa CA, a l'avantage sur
les jets de sauvegarde de Dextérité, et peut utiliser son attaque de
claquage comme action bonus.




#### Golem de chair

*Construction moyenne, neutre*

**Classe d'armure** 9

**Points de vie** 93 (11d8 + 44)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   9 (-1)    18 (+4)   6 (-2)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Immunités aux dégâts :** foudre, poison ; contondant, perforant et
tranchant des attaques non magiques qui ne sont pas adamantines.

**Condition Immunités** charmé, épuisement, effrayé, paralysé, pétrifié,
empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Languages** comprend les langues de son créateur mais ne peut pas
parler

**Défi** 5 (1 800 XP)

***Berserk.*** Chaque fois que le golem commence son tour avec 40 points
de vie ou moins, lancez un d6. Sur un 6, le golem devient berserk. À
chacun de ses tours pendant qu'il est en état de berserker, le golem
attaque la créature la plus proche qu'il peut voir. Si aucune créature
n'est assez proche pour se déplacer et attaquer, le golem attaque un
objet, avec une préférence pour un objet plus petit que lui. Une fois
que le golem est devenu fou furieux, il continue à le faire jusqu'à ce
qu'il soit détruit ou qu'il récupère tous ses points de vie.

Le créateur du golem, s'il se trouve à moins de 60 pieds du golem
berserker, peut essayer de le calmer en parlant de manière ferme et
persuasive. Le golem doit être capable d'entendre son créateur, qui
doit prendre une action pour faire un test de Charisme (Persuasion) DC
15. Si le test est réussi, le golem cesse d'être berserker. S'il subit
des dégâts alors qu'il a encore 40 points de vie ou moins, le golem
peut redevenir berserker.

***Aversion pour le feu.*** Si le golem subit des dégâts de feu, il a un
désavantage aux jets d'attaque et aux tests de capacité jusqu'à la fin
de son prochain tour.

***Forme immuable.*** Le golem est immunisé contre tout sort ou effet
qui altérerait sa forme.

***Absorption de la foudre.*** Chaque fois que le golem subit des dégâts
de foudre, il ne subit aucun dégât et regagne à la place un nombre de
points de vie égal aux dégâts de foudre infligés.

***Résistance à la magie.*** Le golem a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du golem sont magiques.


##### Actions

***Attaques multiples.*** Le golem effectue deux attaques de type slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de dégâts
contondants.




#### Golem de fer

*Grande construction, sans alignement.*

**Classe d'armure** 20 (armure naturelle)

**Points de vie** 210 (20d10 + 100)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   24 (+7)   9 (-1)    20 (+5)   3 (-4)    11 (+0)   1 (-5)

  -----------------------------------------------------------

**Immunités aux dégâts :** feu, poison, psychique ; contondant,
perforant et tranchant des attaques non magiques qui ne sont pas
adamantines.

**Condition Immunités** charmé, épuisement, effrayé, paralysé, pétrifié,
empoisonné

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** comprend les langues de son créateur mais ne peut pas parler

**Défi** 16 (15 000 XP)

***Absorption du feu.*** Chaque fois que le golem subit des dégâts de
feu, il ne subit aucun dégât et regagne à la place un nombre de points
de vie égal aux dégâts de feu infligés.

***Forme immuable.*** Le golem est immunisé contre tout sort ou effet
qui altérerait sa forme.

***Résistance à la magie.*** Le golem a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du golem sont magiques.


##### Actions

***Attaques multiples.*** Le golem effectue deux attaques de mêlée.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +13 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 20 (3d8 + 7) points de dégâts
contondants.

***Epée.*** *Attaque avec une arme de corps-à-corps :* +13 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 23 (3d10 + 7) points de
dégâts tranchants.

***Souffle empoisonné (Recharge 6).*** Le golem exhale un gaz empoisonné
dans un cône de 15 pieds. Chaque créature dans cette zone doit effectuer
un jet de sauvegarde de Constitution DC 19, subissant 45 (10d8) dégâts
de poison en cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Golem de pierre

*Grande construction, sans alignement.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 178 (17d10 + 85)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   9 (-1)    20 (+5)   3 (-4)    11 (+0)   1 (-5)

  -----------------------------------------------------------

**Immunités aux dommages**: poison, psychique ; contondant, perforant et
tranchant des attaques non magiques qui ne sont pas adamantines.

**Condition Immunités** charmé, épuisement, effrayé, paralysé, pétrifié,
empoisonné

**Sens** Vision dans le noir 120 ft., Perception passive 10

**Langues** comprend les langues de son créateur mais ne peut pas parler

**Défi** 10 (5 900 XP)

***Forme immuable.*** Le golem est immunisé contre tout sort ou effet
qui altérerait sa forme.

***Résistance à la magie.*** Le golem a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Armes magiques.*** Les attaques des armes du golem sont magiques.


##### Actions

***Attaques multiples.*** Le golem effectue deux attaques de type slam.

***Slam.*** *Attaque avec une arme de corps-à-corps :* +10 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 19 (3d8 + 6) points de dégâts
contondants.

***Lenteur (recharge 5-6).*** Le golem cible une ou plusieurs créatures
qu'il peut voir dans un rayon de 3 mètres autour de lui. Chaque cible
doit effectuer un jet de sauvegarde de Sagesse DC 17 contre cette magie.
En cas d'échec, la cible ne peut pas utiliser de réactions, sa vitesse
est réduite de moitié et elle ne peut pas effectuer plus d'une attaque
à son tour. De plus, la cible peut effectuer soit une action, soit une
action bonus à son tour, mais pas les deux. Ces effets durent 1 minute.
Une cible peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.





### Hags


#### Guenaude verte

*Fée moyenne, maléfique neutre.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 82 (11d8 + 33)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   12 (+1)   16 (+3)   13 (+1)   14 (+2)   14 (+2)

  -----------------------------------------------------------

**Compétences** Mystère +3, Discrétion +4, Perception +4, Furtivité +3

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues :** commun, draconique, sylvestre.

**Défi** 3 (700 XP)

***Amphibie.*** La sorcière peut respirer de l'air et de l'eau.

***Lanceur de sorts inné.*** La caractéristique d'incantation innée de
la sorcière est le Charisme (sauvegarde contre les sorts DC 12). Elle
peut lancer de manière innée les sorts suivants, ne nécessitant aucun
composant matériel :

A volonté : Lumières *dansantes*, *illusion
mineure*.

***Mimétisme.*** La sorcière peut imiter les sons d'animaux et les voix
humanoïdes. Une créature qui entend les sons peut dire qu'il s'agit
d'imitations en réussissant un test de Sagesse (Intuition) DC 14.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts tranchants.

***Apparence illusoire.*** La sorcière se couvre, ainsi que tout ce
qu'elle porte, d'une illusion magique qui la fait ressembler à une
autre créature de sa taille générale et de forme humanoïde. L'illusion
prend fin si la sorcière effectue une action bonus pour y mettre fin ou
si elle meurt.

Les changements provoqués par cet effet ne résistent pas à l'inspection
physique. Par exemple, la hag peut sembler avoir une peau lisse, mais
quelqu'un qui la touche sentira sa chair rugueuse. Sinon, une créature
doit prendre une action pour inspecter visuellement l'illusion et
réussir un test d'Intelligence (Investigation) DC 20 pour discerner que
la sorcière est déguisée.

***Passage invisible.*** La sorcière devient magiquement invisible
jusqu'à ce qu'elle attaque ou lance un sort, ou jusqu'à la fin de sa
concentration (comme si elle se concentrait sur un sort). Pendant
qu'elle est invisible, elle ne laisse aucune trace physique de son
passage, de sorte qu'elle ne peut être suivie que par magie. Tout
équipement qu'elle porte ou transporte est invisible avec elle.




#### Guenaude nocturne

*Fiélon moyen, mal neutre.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 112 (15d8 + 45)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   16 (+3)   14 (+2)   16 (+3)

  -----------------------------------------------------------

**Compétences** Supercherie +7, Intuition +6, Perception +6, Furtivité
+6.

**Résistances aux dégâts :** froid, feu ; contondant, perforant et
tranchant des attaques non magiques non réalisées avec des armes en
argent.

**Condition Immunités** charmées

**Sens** Vision dans le noir 120 ft., Perception passive 16

**Langues** Abysses, Commun, Infernal, Primordial

**Défi** 5 (1 800 XP)

***Lanceur de sorts inné.*** La caractéristique d'incantation innée de
la sorcière est le Charisme (sauvegarde contre les sorts DC 14, +6 pour
toucher avec les attaques de sorts). Elle peut lancer de manière innée
les sorts suivants, ne nécessitant aucun composant matériel :

À volonté : *détection de la magie*, *missile
magique*.

2/jour chacun : Changement *de plan*,
*Rayon affa*

***Résistance à la magie.*** La sorcière a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.


##### Actions

***Griffes (forme Hag uniquement).*** *Attaque avec une arme de
corps-à-corps :* +7 pour toucher, allonge de 1,5 m, une cible. *Touché
:* 13 (2d8 + 4) points de dégâts tranchants.

***Changement de forme.*** La sorcière se métamorphose par magie en un
humanoïde féminin de taille petite ou moyenne, ou reprend sa véritable
forme. Ses statistiques sont les mêmes dans chaque forme. L'équipement
qu'elle porte n'est pas transformé. Elle retourne à sa vraie forme si
elle meurt.

***Forme éthérée.*** La sorcière entre magiquement dans le plan éthéré
depuis le plan matériel, ou vice versa. Pour ce faire, la sorcière doit
avoir une pierre de cœur en sa possession.

***Hantise du Destrier Noir (1/Jour).*** Alors qu'elle se trouve sur le
plan éthéré, la sorcière touche magiquement un humanoïde endormi sur le
plan matériel. Un sort de protection contre *le Bien et le
Mal* lancé sur la cible empêche ce
contact, tout comme un *cercle magique*. Tant que le
contact persiste, la cible a des visions terribles. Si ces visions
durent au moins 1 heure, la cible ne tire aucun bénéfice de son repos,
et son maximum de points de vie est réduit de 5 (1d10). Si cet effet
réduit le maximum de points de vie de la cible à 0, la cible meurt, et
si la cible était maléfique, son âme est piégée dans le sac à âmes de la
sorcière. La réduction du maximum de points de vie de la cible dure
jusqu'à ce qu'elle soit annulée par le sort Restauration
*supérieure* ou une magie similaire.




#### Guenaude marine

*Fée moyenne, maléfique chaotique.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 52 (7d8 + 21)

**Vitesse** 30 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   13 (+1)   16 (+3)   12 (+1)   12 (+1)   13 (+1)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 11

**Langues** Aquatique, Commun, Géant

**Défi** 2 (450 XP)

***Amphibie.*** La sorcière peut respirer de l'air et de l'eau.

***Apparence horrifiante.*** Tout humanoïde qui commence son tour à
moins de 30 pieds de la sorcière et qui peut voir la vraie forme de la
sorcière doit effectuer un jet de sauvegarde de Sagesse DC 11. En cas
d'échec, la créature est effrayée pendant 1 minute. Une créature peut
répéter le jet de sauvegarde à la fin de chacun de ses tours, avec un
désavantage si la hag est dans sa ligne de vue, mettant fin à l'effet
sur elle-même en cas de réussite. Si le jet de sauvegarde d'une
créature est réussi ou si l'effet prend fin pour elle, la créature est
immunisée contre l'apparence horrible de la sorcière pendant les 24
heures suivantes.

À moins que la cible ne soit surprise ou que la révélation de la
véritable forme de la hag soit soudaine, elle peut détourner les yeux et
éviter de faire le jet de sauvegarde initial. Jusqu'au début de son
prochain tour, une créature qui détourne les yeux a un désavantage aux
jets d'attaque contre la sorcière.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de
dégâts tranchants.

***Regard de mort.*** La sorcière cible une créature effrayée qu'elle
peut voir dans un rayon de 30 pieds autour d'elle. Si la cible peut
voir la sorcière, elle doit réussir un jet de sauvegarde de Sagesse DC
11 contre cette magie ou tomber à 0 point de vie.

***Apparence illusoire.*** La sorcière se couvre, ainsi que tout ce
qu'elle porte, d'une illusion magique qui la fait ressembler à une
créature hideuse de sa taille générale et de forme humanoïde. L'effet
prend fin si la sorcière effectue une action bonus pour y mettre fin ou
si elle meurt.

Les changements provoqués par cet effet ne résistent pas à l'inspection
physique. Par exemple, la sorcière peut sembler ne pas avoir de griffes,
mais quelqu'un qui touche sa main peut sentir les griffes. Sinon, une
créature doit prendre une action pour inspecter visuellement l'illusion
et réussir un test d'Intelligence (Investigation) DC 16 pour discerner
que la sorcière est déguisée.





### Modèle de demi-dragon

Une bête, un humanoïde, un géant ou une monstruosité peut devenir un
demi-dragon. Il conserve ses statistiques, sauf les suivantes.

***Défi.*** Pour éviter de recalculer la valeur de défi de la créature,
n'appliquez le gabarit qu'à une créature qui remplit les prérequis
optionnels de la table Soufflarme ci-dessous. Sinon, recalculez la
valeur après avoir appliqué le gabarit.

***Sens.*** Le demi-dragon gagne la vision aveugle avec un rayon de 10
pieds et la vision dans le noir avec un rayon de 60 pieds.

***Résistances .*** Le demi-dragon gagne une résistance à un type de
dégâts en fonction de sa couleur.

  --------------------------------------
  Couleur              Résistance aux
                       dégâts
  -------------------- -----------------
  Noir ou cuivre       Acide

  Bleu ou bronze       La foudre

  Laiton, or ou rouge  Feu

  Vert                 Empoisonné

  Argent ou blanc      Froid
  --------------------------------------

***Langues.*** Le demi-dragon parle le draconique en plus de toutes les
autres langues qu'il connaît.

***Nouvelle action : Arme à Souffle.*** Le demi-dragon dispose de
l'arme à souffle de son demi-dragon. La taille du demi-dragon détermine
le fonctionnement de cette action.

  -----------------------------------------------------------
  Taille            Soufflarme          Prérequis facultatif
  ----------------- ------------------- ---------------------
  Grand ou petit    Comme un wyrmling   Challenge 2 ou plus

  Très grand        Comme un jeune      Challenge 7 ou plus
                    dragon              

  Gargantuesque     En tant que dragon  Challenge 8 ou plus
                    adulte              
  -----------------------------------------------------------


#### Demi-dragon rouge vétéran

*Humanoïde moyen (humain), tout type d'alignement.*

**Classe d'armure** 18 (harnois)

**Points de vie** 65 (10d8 + 20)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   13 (+1)   14 (+2)   10 (+0)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Athlétisme +5, Perception +2

**Résistance aux dégâts** du feu

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 12

**Langues :** commun, draconique.

**Défi** 5 (1 800 XP)


##### Actions

***Attaques multiples.*** Le Vétéran effectue deux attaques à l'épée
longue. S'il a une épée courte dégainée, il peut également effectuer
une attaque d'épée courte.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts tranchants, ou 8 (1d10 + 3) points de dégâts tranchants si
utilisée à deux mains.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts perforants.

***Arbalète lourde.*** *Attaque avec une arme à distance :* +3 pour
toucher, portée de 100/400 pieds, une cible. *Touché :* 6 (1d10 + 1)
points de dégâts perforants.

***Souffle de feu (Recharge 5-6).*** Le Vétéran exhale du feu dans un
cône de 15 pieds. Chaque créature dans cette zone doit effectuer un jet
de sauvegarde de Dextérité DC 15, subissant 24 (7d6) dégâts de feu en
cas d'échec, ou la moitié des dégâts en cas de réussite.





### Lycanthropes


#### Ours-garou

*Humanoïde moyen (humain, Métamorphe), neutre bon.*

**Classe d'armure** 10 sous forme humanoïde, 11 (armure naturelle) sous
forme d'ours et d'hybride.

**Points de vie** 135 (18d8 + 54)

**Vitesse** 30 ft (40 ft, grimper 30 ft en forme d'ours ou d'hybride)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   17 (+3)   11 (+0)   12 (+1)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +7

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas effectuées avec des armes en
argent.

**Sens** passif Perception 17

**Langues** Commun (ne peut pas parler sous forme d'ours)

**Défi** 5 (1 800 XP)

***Changement de forme.*** L'Ours-garou peut utiliser son action pour
se métamorphoser en un hybride Grand ours-humanoïde ou en un Grand ours,
ou revenir à sa véritable forme, qui est humanoïde. Ses statistiques,
autres que sa taille et sa CA, sont les mêmes dans chaque forme.
L'équipement qu'il porte n'est pas transformé. Il revient à sa vraie
forme s'il meurt.

***Odorat aiguisé.*** L'Ours-garou a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'odorat.


##### Actions

***Attaques multiples.*** Sous forme d'ours, l'ours-garou effectue
deux attaques de griffes. En forme humanoïde, il effectue deux attaques
de hache à deux mains. En forme hybride, il peut attaquer comme un ours
ou un humanoïde.

***Morsure (forme Ours ou Hybride uniquement).*** *Attaque avec une arme
de corps-à-corps :* +7 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 15 (2d10 + 4) points de dégâts perforants. Si la cible est un
humanoïde, elle doit réussir un jet de sauvegarde de Constitution DC 14
ou être maudite par la lycanthropie des ours-garous.

***Griffe (forme ours ou hybride uniquement).*** *Attaque avec une arme
de corps-à-corps :* +7 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 13 (2d8 + 4) points de dégâts tranchants.

***Hache à deux mains (forme humanoïde ou hybride uniquement).***
*Attaque avec une arme de corps-à-corps :* +7 pour toucher, allonge de
1,5 m, une cible. *Touché :* 10 (1d12 + 4) points de dégâts tranchants.




#### Sanglier-garou

*Humanoïde moyen (humain, Métamorphe), neutre maléfique.*

**Classe d'armure** 10 sous forme humanoïde, 11 (armure naturelle) sous
forme de sanglier ou d'hybride.

**Points de vie** 78 (12d8 + 24)

**Vitesse** 30 ft. (40 ft. en forme de sanglier)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   10 (+0)   15 (+2)   10 (+0)   11 (+0)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +2

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas effectuées avec des armes en
argent. Sens passif Perception 12

**Langues** Commun (ne peut pas parler sous forme de sanglier)

**Défi** 4 (1 100 XP)

***Métamorphe.*** Le Sanglier-garou peut utiliser son action pour se
métamorphoser en un hybride sanglier-humanoïde ou en un sanglier, ou
revenir à sa véritable forme, qui est humanoïde. Ses statistiques,
autres que sa CA, sont les mêmes dans chaque forme. L'équipement qu'il
porte n'est pas transformé. Il revient à sa vraie forme s'il meurt.

***Charge (forme Sanglier ou Hybride uniquement).*** Si le
Sanglier-garou se déplace d'au moins 15 pieds en ligne droite vers une
cible puis la frappe avec ses défenses au même tour, la cible subit 7
(2d6) dégâts tranchants supplémentaires. Si la cible est une créature,
elle doit réussir un jet de sauvegarde de Force DC 13 ou être mise au
sol.

***Implacable (se recharge après un repos court ou long).*** Si le
Sanglier-garou subit 14 points de dégâts ou moins qui le réduiraient à 0
point de vie, il est réduit à 1 point de vie à la place.


##### Actions

***Attaques multiples (forme humanoïde ou hybride uniquement).*** Le
Sanglier-garou effectue deux attaques, dont une seule peut être
effectuée avec ses défenses.

***Maillet (forme humanoïde ou hybride uniquement).*** *Attaque avec une
arme de corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 10 (2d6 + 3) points de dégâts contondants.

***Défenses (forme Sanglier ou Hybride uniquement).*** *Attaque avec une
arme de corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 10 (2d6 + 3) dégâts tranchants. Si la cible est un humanoïde,
elle doit réussir un jet de sauvegarde de Constitution DC 12 ou être
maudite par la lycanthropie du Sanglier-garou.




#### Rat-garou

*Humanoïde moyen (humain, Métamorphe), maléfique.*

**Classe d'armure** 12

**Points de vie** 33 (6d8 + 6)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   15 (+2)   12 (+1)   11 (+0)   10 (+0)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +4

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas effectuées avec des armes en
argent.

**Sens** Vision dans le noir 60 ft. (forme de rat seulement), Perception
passive 12

**Langues** Commun (ne peut pas parler sous forme de rat)

**Défi** 2 (450 XP)

***Métamorphe.*** Le rat-garou peut utiliser son action pour se
métamorphoser en un hybride rat-humanoïde ou en un rat géant, ou revenir
à sa véritable forme, qui est humanoïde. Ses statistiques, autres que sa
taille, sont les mêmes dans chaque forme. L'équipement qu'il porte
n'est pas transformé. Il revient à sa vraie forme s'il meurt.

***Odorat aiguisé.*** Le rat-garou a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'odorat.


##### Actions

***Attaques multiples (forme humanoïde ou hybride uniquement).*** Le
rat-garou effectue deux attaques, dont une seule peut être une morsure.

***Morsure (forme Rat ou Hybride uniquement).*** *Attaque avec une arme
de corps-à-corps :* +4 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 4 (1d4 + 2) dégâts perforants. Si la cible est un humanoïde,
elle doit réussir un jet de sauvegarde de Constitution DC 11 ou être
maudite par la lycanthropie du rat-garou.

***Épée courte (forme humanoïde ou hybride uniquement).*** *Attaque avec
une arme de corps-à-corps :* +4 pour toucher, allonge de 1,5 m, une
cible. *Touché :* 5 (1d6 + 2) points de dégâts perforants.

***Arbalète de poing (forme humanoïde ou hybride uniquement).***
*Attaque avec une arme à distance :* +4 pour toucher, portée de 30/120
pieds, une cible. *Touché :* 5 (1d6 + 2) points de dégâts perforants.




#### Tigre-garou

*Humanoïde moyen (humain, métamorphe), neutre.*

**Classe d'armure** 12

**Points de vie** 120 (16d8 + 48)

**Vitesse** 30 ft. (40 ft. en forme de tigre)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   16 (+3)   10 (+0)   13 (+1)   11 (+0)

  -----------------------------------------------------------

**Compétences** Perception +5, Discrétion +4

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques qui ne sont pas effectuées avec des armes en
argent.

**Sens** Vision dans le noir 60 ft., Perception passive 15

**Langues** Commun (ne peut pas parler sous forme de tigre)

**Défi** 4 (1 100 XP)

***Métamorphe.*** Le Tigre-garou peut utiliser son action pour se
métamorphoser en un hybride tigre-humanoïde ou en un tigre, ou revenir à
sa véritable forme, qui est humanoïde. Ses statistiques, autres que sa
taille, sont les mêmes dans chaque forme. L'équipement qu'il porte
n'est pas transformé. Il revient à sa vraie forme s'il meurt.

***Ouïe et odorat aiguisés.*** Le Tigre-garou a un avantage sur les
tests de Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Bondir (forme tigre ou hybride uniquement).*** Si le tigre-garou se
déplace d'au moins 15 pieds en ligne droite vers une créature et la
touche avec une attaque de griffes dans le même tour, cette cible doit
réussir un jet de sauvegarde de Force DC 14 ou être mise à terre. Si la
cible est couchée, le tigre-garou peut effectuer une attaque de morsure
contre elle en tant qu'action bonus.


##### Actions

***Attaques multiples (forme humanoïde ou hybride uniquement).*** Sous
forme humanoïde, le tigre-garou effectue deux attaques au cimeterre ou
deux attaques à l'arc long. En forme hybride, il peut attaquer comme un
humanoïde ou effectuer deux attaques de griffes.

***Morsure (forme tigre ou hybride uniquement).*** *Attaque avec une
arme de corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 8 (1d10 + 3) dégâts perforants. Si la cible est un humanoïde,
elle doit réussir un jet de sauvegarde de Constitution DC 13 ou être
maudite par la lycanthropie du tigre-garou.

***Griffe (forme tigre ou hybride uniquement).*** *Attaque avec une arme
de corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 7 (1d8 + 3) points de dégâts tranchants.

***Cimeterre (forme humanoïde ou hybride uniquement).*** *Attaque avec
une arme de corps-à-corps :* +5 pour toucher, allonge de 1,5 m, une
cible. *Touché :* 6 1d6 + 3) dégâts tranchants.

***Arc long (forme humanoïde ou hybride uniquement).*** *Attaque avec
une arme à distance :* +4 pour toucher, portée de 150/600 pieds, une
cible. *Touché :* 6 (1d8 + 2) points de dégâts perforants.




#### Loup-garou

*Humanoïde moyen (humain, métamorphe), maléfique chaotique.*

**Classe d'armure** 11 sous forme humanoïde, 12 (armure naturelle) sous
forme de loup ou d'hybride.

**Points de vie** 58 (9d8 + 18)

**Vitesse** 30 ft. (40 ft. en forme de loup)

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   13 (+1)   14 (+2)   10 (+0)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +4, Discrétion +3

**Immunités aux dégâts**: contondant, perforant et tranchant des
attaques non magiques non réalisées avec des armes en argent.

**Sens** passif Perception 14

**Langues** Commun (ne peut pas parler sous forme de loup)

**Défi** 3 (700 XP)

***Changement de forme.*** Le loup-garou peut utiliser son action pour
se métamorphoser en un hybride loup-humanoïde ou en un loup, ou revenir
à sa véritable forme, qui est humanoïde. Ses statistiques, autres que sa
CA, sont les mêmes dans chaque forme. L'équipement qu'il porte n'est
pas transformé. Il revient à sa vraie forme s'il meurt.

***Ouïe et odorat aiguisés.*** Le loup-garou a un avantage sur les tests
de Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Attaques multiples (forme humanoïde ou hybride uniquement).*** Le
loup-garou effectue deux attaques : une avec sa morsure et une avec ses
griffes ou sa lance.

***Morsure (forme loup ou hybride uniquement).*** *Attaque avec une arme
de corps-à-corps :* +4 pour toucher, allonge de 1,5 m, une cible.
*Touché :* 6 (1d8 + 2) dégâts perforants. Si la cible est un humanoïde,
elle doit réussir un jet de sauvegarde de Constitution DC 12 ou être
maudite par la lycanthropie du loup-garou.

***Griffes (forme hybride uniquement).*** *Attaque avec une arme de
corps-à-corps :* +4 pour toucher, portée de 1,5 m, une créature. *Touché
:* 7 (2d4 + 2) dégâts tranchants.

***Lance (forme humanoïde uniquement).*** *Attaque avec une arme de
corps-à-corps ou à distance :* +4 pour toucher, allonge de 5 pieds ou
portée de 20/60 pieds, une créature. *Touché :* 5 (1d6 + 2) points de
dégâts perforants, ou 6 1d8 + 2) points de dégâts perforants si elle est
utilisée à deux mains pour une attaque de mêlée.





### Méphites


#### Méphite poussiéreux

*Petit élémentaire, mal neutre*

**Classe d'armure** 12

**Points de vie** 17 (5d6)

**Vitesse** 30 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    14 (+2)   10 (+0)   9 (-1)    11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +4

Feu sur les**vulnérabilités aux dégâts**

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues :** aérien et glaiseux.

**Défi** 1/2 (100 XP)

***Eclatement de la mort.*** Lorsque le méphite meurt, il explose dans
un jet de poussière. Chaque créature située à moins de 1,5 m de lui doit
alors réussir un jet de sauvegarde de Constitution DC 10 ou être
aveuglée pendant 1 minute. Une créature aveuglée peut répéter le jet de
sauvegarde à chacun de ses tours, mettant fin à l'effet sur elle-même
en cas de réussite.

***Lanceur de sorts inné (1/Jour).*** Le méphite peut lancer le
*Sommeil* de façon innée, sans nécessiter de composantes
matérielles. Sa caractéristique d'incantation innée est le Charisme.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 4 (1d4 + 2) dégâts
tranchants.

***Souffle aveuglant (Recharge 6).*** Le méphite exhale un cône de
poussière aveuglante de 3 mètres de long. Chaque créature dans cette
zone doit réussir un jet de sauvegarde de Dextérité DC 10 ou être
aveuglée pendant 1 minute. Une créature peut répéter le jet de
sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet sur
elle-même en cas de réussite.




#### Méphite gelé

*Petit élémentaire, mal neutre*

**Classe d'armure** 11

**Points de vie** 21 (6d6)

**Vitesse** 30 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    13 (+1)   10 (+0)   9 (-1)    11 (+0)   12 (+1)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +3

**Vulnérabilités aux dégâts**: contondant, feu.

**Immunités aux dommages :** froid, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues** Aquatique, Auran

**Défi** 1/2 (100 XP)

***Eclatement de la mort.*** Lorsque le méphite meurt, il explose dans
une explosion de glace déchiquetée. Chaque créature dans un rayon de 1,5
m doit effectuer un jet de sauvegarde de Dextérité à DC 10, subissant 4
(1d8) dégâts tranchants en cas d'échec, ou la moitié des dégâts en cas
de réussite.

***Fausse apparence.*** Lorsque le méphite reste immobile, il est
impossible de le distinguer d'un tesson de glace ordinaire.

***Lanceur de sorts inné (1/Jour).*** Le méphite peut lancer de manière
innée *Nappe de brouillard*, ne nécessitant aucun
composant matériel. Sa caractéristique d'incantation innée est le
Charisme.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 3 (1d4 + 1) dégâts
tranchants plus 2 (1d4) dégâts de froid.

***Souffle de givre (Recharge 6).*** Le méphite exhale un cône d'air
froid de 15 pieds. Chaque créature dans cette zone doit réussir un jet
de sauvegarde de Dextérité DC 10, subissant 5 (2d4) dégâts de froid en
cas d'échec, ou la moitié des dégâts en cas de réussite.




#### Méphite magmatique

*Petit élémentaire, mal neutre*

**Classe d'armure** 11

**Points de vie** 22 (5d6 + 5)

**Vitesse** 30 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    12 (+1)   12 (+1)   7 (-2)    10 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Discrétion +3

**Vulnérabilité aux dégâts** à froid

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Ardent, Terrien

**Défi** 1/2 (100 XP)

***Death Burst.*** Lorsque le méphite meurt, il explose dans une
explosion de lave. Chaque créature située à moins de 1,5 m de lui doit
effectuer un jet de sauvegarde de Dextérité à DC 11, subissant 7 (2d6)
dégâts de feu en cas d'échec, ou la moitié des dégâts en cas de
réussite.

***Fausse apparence.*** Bien que le méphite reste immobile, il est
impossible de le distinguer d'un monticule de magma ordinaire.

***Lanceur de sorts inné (1/Jour).*** Le méphite peut lancer de manière
innée Métal *brûlant* (sauvegarde contre les sorts DC
10), ne nécessitant aucun composant matériel. Sa caractéristique
d'incantation innée est le Charisme.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 3 (1d4 + 1) dégâts
tranchants plus 2 (1d4) dégâts de feu.

***Souffle de feu (Recharge 6).*** Le méphite exhale un cône de feu de
15 pieds. Chaque créature dans cette zone doit effectuer un jet de
sauvegarde de Dextérité DC 11, subissant 7 (2d6) dégâts de feu en cas
d'échec, ou la moitié des dégâts en cas de réussite.




#### Méphite vaporeux

*Petit élémentaire, mal neutre*

**Classe d'armure** 10

**Points de vie** 21 (6d6)

**Vitesse** 30 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    11 (+0)   10 (+0)   11 (+0)   10 (+0)   12 (+1)

  -----------------------------------------------------------

**Immunités aux dommages**: feu, poison.

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** Aquatique, Ardent

**Défi** 1/4 (50 XP)

***Death Burst.*** Lorsque le méphite meurt, il explose dans un nuage de
vapeur. Chaque créature située à moins de 1,5 m du méphite doit réussir
un jet de sauvegarde de Dextérité DC 10 ou subir 4 (1d8) dégâts de feu.

***Lanceur de sorts inné (1/Jour).*** Le méphite peut lancer *Flou
de* manière innée, sans nécessiter de composantes matérielles.
Sa caractéristique d'incantation innée est le Charisme.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +2 au toucher,
allonge de 1,5 m, une créature. *Touché :* 2 (1d4) dégâts tranchants
plus 2 (1d4) dégâts de feu.

***Souffle de vapeur (Recharge 6).*** Le méphite exhale un cône de
vapeur brûlante de 3 mètres de long. Chaque créature dans cette zone
doit réussir un jet de sauvegarde de Dextérité DC 10, subissant 4 (1d8)
dégâts de feu en cas d'échec, ou la moitié des dégâts en cas de
réussite.





### Momies


#### Momie

*Mort-vivant moyen, maléfique.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 58 (9d8 + 18)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   8 (-1)    15 (+2)   6 (-2)    10 (+0)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Wis +2

Feu sur les**vulnérabilités aux dégâts**

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunités aux dommages :** nécrotique, poison.

**Immunités d'état :** charmé, épuisement, effrayé, paralysé,
empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 10

Les**langues** qu'il a connues dans la vie

**Défi** 3 (700 XP)


##### Actions

***Attaques multiples.*** La Momie peut utiliser son Effroyable Regard
et effectue une attaque avec son poing pourri.

***Poing pourri.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts de
contondant plus 10 (3d6) dégâts nécrotiques. Si la cible est une
créature, elle doit réussir un jet de sauvegarde de Constitution DC 12
ou être maudite par la pourriture de momie. La cible maudite ne peut pas
regagner de points de vie, et son maximum de points de vie diminue de 10
(3d6) toutes les 24 heures qui s'écoulent. Si la malédiction réduit le
maximum de points de vie de la cible à 0, la cible meurt et son corps se
transforme en poussière. La malédiction dure jusqu'à ce qu'elle soit
levée par le sort *supprimer la malédiction* ou par une
autre magie.

***Dreadful Glare.*** La momie cible une créature qu'elle peut voir
dans un rayon de 60 pieds autour d'elle. Si la cible peut voir la
momie, elle doit réussir un jet de sauvegarde de Sagesse DC 11 contre
cette magie ou être effrayée jusqu'à la fin du prochain tour de la
momie. Si la cible échoue au jet de sauvegarde par 5 ou plus, elle est
également paralysée pour la même durée. Une cible qui réussit le jet de
sauvegarde est immunisée contre le regard effrayant de toutes les momies
(mais pas des seigneurs momies) pendant les 24 heures suivantes.




#### Seigneur momie

*Mort-vivant moyen, maléfique.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 97 (13d8 + 39)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   10 (+0)   17 (+3)   11 (+0)   18 (+4)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +8, Int +5, Wis +9, Cha +8

**Compétences** Histoire +5, Religion +5 Vulnérabilité aux dégâts feu

**Immunités aux dommages :** nécrotique, poison ; contondant, perforant
et tranchant des attaques non magiques.

**Immunités d'état** charmé, épuisement, effrayé, paralysé, empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 14

Les**langues** qu'il a connues dans la vie

**Défi** 15 (13 000 XP)

***Résistance à la magie.*** Le Seigneur momie a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Rajeunissement.*** Un Seigneur momie détruit acquiert un nouveau
corps en 24 heures si son cœur est intact, regagnant tous ses points de
vie et redevenant actif. Le nouveau corps apparaît dans un rayon de 1,5
mètre autour du cœur du seigneur momie.

***Incantation.*** Le seigneur momie est un lanceur de sorts de 10e
niveau. Sa caractéristique d'incantation est Sagesse (sauvegarde contre
les sorts DC 17, +9 pour toucher avec les attaques de sorts). Le
seigneur momie a les sorts de clercs suivants préparés :

Tours de magie (à volonté) : flamme *sacrée*,
*thaumaturgie*

1er niveau (4 emplacements) : *Injonction*, Éclair
*traçant*

2ème niveau (3 emplacements) : Immobilisation de
*personne*, *Arme
spirituelle*

3e niveau (3 emplacements) : *animer les morts*,
*dissipation de la magie*.

4ème niveau (3 emplacements) : *Divination*, *Gardien de
la foi*

5e niveau (2 emplacements) : *Contagion*, *Fléau des
insectes*

6ème niveau (1 emplacement) : *Contamination*


##### Actions

***Attaques multiples.*** La Momie peut utiliser son Effroyable Regard
et effectue une attaque avec son poing pourri.

***Poing pourri.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 14 (3d6 + 4) dégâts de
contondant plus 21 (6d6) dégâts nécrotiques. Si la cible est une
créature, elle doit réussir un jet de sauvegarde de Constitution DC 16
ou être maudite par la pourriture de momie. La cible maudite ne peut pas
regagner de points de vie, et son maximum de points de vie diminue de 10
(3d6) toutes les 24 heures qui s'écoulent. Si la malédiction réduit le
maximum de points de vie de la cible à 0, la cible meurt et son corps se
transforme en poussière. La malédiction dure jusqu'à ce qu'elle soit
levée par le sort supprimer la malédiction ou par une autre magie.

***Dreadful Glare.*** Le Seigneur momie cible une créature qu'il peut
voir dans un rayon de 60 pieds autour de lui. Si la cible peut voir le
seigneur momie, elle doit réussir un jet de sauvegarde de Sagesse DC 16
contre cette magie ou être effrayée jusqu'à la fin du prochain tour de
la momie. Si la cible échoue le jet de sauvegarde par 5 ou plus, elle
est également paralysée pour la même durée. Une cible qui réussit le jet
de sauvegarde est immunisée contre le regard effrayant de toutes les
momies et de tous les seigneurs momies pendant les 24 heures suivantes.



##### Actions légendaires

Le seigneur momie peut effectuer 3 actions légendaires, en choisissant
parmi les options ci-dessous. Une seule option d'action légendaire peut
être utilisée à la fois et uniquement à la fin du tour d'une autre
créature. Le seigneur momie regagne les actions légendaires dépensées au
début de son tour.

***Attaque.*** Le Seigneur momie effectue une attaque avec son poing
pourri ou utilise son Effroyable Regard.

***Poussière aveuglante.*** De la poussière et du sable aveuglants
tourbillonnent comme par magie autour du seigneur momie. Chaque créature
se trouvant à moins de 1,5 m du seigneur momie doit réussir un jet de
sauvegarde de Constitution DC 16 ou être aveuglée jusqu'à la fin du
prochain tour de la créature.

***Parole blasphématoire (coûte 2 actions).*** Le seigneur momie
prononce un mot blasphématoire. Chaque créature non-morte se trouvant à
moins de 3 mètres du seigneur momie et pouvant entendre la parole
magique doit réussir un jet de sauvegarde de Constitution DC 16 ou être
étourdie jusqu'à la fin du prochain tour du seigneur momie.

***Canaliser l'énergie négative (coûte 2 actions).*** Le seigneur momie
libère magiquement de l'énergie négative. Les créatures situées à moins
de 60 pieds du Seigneur momie, y compris celles qui se trouvent derrière
des barrières ou dans des coins, ne peuvent pas regagner de points de
vie avant la fin du prochain tour du Seigneur momie.

***Tourbillon de sable (coûte 2 actions).*** Le Seigneur momie se
transforme magiquement en un tourbillon de sable, se déplace jusqu'à 60
pieds, puis reprend sa forme normale. Lorsqu'il est sous forme de
tourbillon, le Seigneur momie est immunisé contre tous les dégâts, et il
ne peut pas être agrippé, pétrifié, mis au tapis, entravé ou étourdi.
L'équipement porté par le seigneur momie reste en sa possession.





### Nagas


#### Naga gardien

*Grande monstruosité, bien légitime.*

**Classe d'armure** 18 (armure naturelle)

**Points de vie** 127 (15d10 + 45)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   18 (+4)   16 (+3)   16 (+3)   19 (+4)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +8, Con +7, Int +7, Wis +8, Cha +8.

**Immunité aux dommages** poison

**Immunités d'état :** charmé, empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** céleste, commune

**Défi** 10 (5 900 XP)

***Rajeunissement.*** S'il meurt, le naga revient à la vie en 1d6 jours
et regagne tous ses points de vie. Seul un sort de *Souhait*
peut empêcher ce trait de fonctionner.

***Incantation.*** Le naga est un lanceur de sorts de 11e niveau. Sa
caractéristique d'incantation est la Sagesse (sauvegarde contre les
sorts DC 16, +8 pour toucher avec les attaques de sorts), et il n'a
besoin que de composantes verbales pour lancer ses sorts. Il a les sorts
de clercs suivants préparés :

Tours de magie (à volonté) : *réparation*, flamme
*sacrée*

1er niveau (4 emplacements) : *commandement*, *guérison des
blessures* de la foi

2e niveau (3 emplacements) : *apaisement des
émotions*, *immobilisation de la
personne*

3e niveau (3 emplacements) : *conférer une
malédiction*

4ème niveau (3 slots) : *Bannissement*, *Liberté de
mouvement*

5e niveau (2 emplacements) : Colonne *de flamme*,
*Quête*

6ème niveau (1 emplacement) : Vision *suprême*


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 8 (1d8 + 4)
dégâts perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 15, subissant 45 (10d8) dégâts de poison en cas
d'échec, ou la moitié des dégâts en cas de réussite.

***Crache du poison.*** *Attaque avec une arme à distance :* +8 pour
toucher, portée de 15/30 pieds, une créature. Touché : La cible doit
effectuer un jet de sauvegarde de Constitution DC 15, subissant 45
(10d8) dégâts de poison en cas d'échec, ou la moitié des dégâts en cas
de réussite.




#### Naga corrupteur

*Grande monstruosité, mal chaotique.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 75 (10d10 + 20)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   17 (+3)   14 (+2)   16 (+3)   15 (+2)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +5, Wis +5, Cha +6

**Immunité aux dommages** poison

**Immunités d'état :** charmé, empoisonné.

**Sens** Vision dans le noir 60 ft., Perception passive 12

**Langues** Abysses, Communes

**Défi** 8 (3 900 XP)

***Rajeunissement.*** S'il meurt, le naga revient à la vie en 1d6 jours
et regagne tous ses points de vie. Seul un sort de *Souhait*
peut empêcher ce trait de fonctionner.

***Incantation.*** Le naga est un lanceur de sorts de 10e niveau. Sa
caractéristique d'incantation est l'Intelligence (sauvegarde contre
les sorts DC 14, +6 pour toucher avec les attaques de sorts), et il n'a
besoin que de composantes verbales pour lancer ses sorts. Il a les sorts
de magicien suivants préparés :

Tours de magie (à volonté) : *main de mage*, *illusion
mineure*.

1er niveau (4 emplacements) : *Charme-personne*,
*Détection de la magie*

2ème niveau (3 emplacements) : Détection des
*pensées*, Immobilisation *de
personne*

3ème niveau (3 emplacements) : *Éclair*,
*Respiration aquatique*

4ème niveau (3 slots) : *Flétrissement*, *Porte
dimensionnelle*

5ème niveau (2 slots) : Domination *de personne*


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 7 (1d6 + 4)
dégâts perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 13, subissant 31 (7d8) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite.





### Vases


#### Pouding noir

*Grand vase, sans alignement*

**Classe d'armure** 7

**Points de vie** 85 (10d10 + 30)

**Vitesse** 20 ft., montée 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   5 (-3)    16 (+3)   1 (-5)    6 (-2)    1 (-5)

  -----------------------------------------------------------

**Immunités aux dégâts :** acide, froid, foudre, tranchant.

**Condition Immunités :** aveuglé, charmé, assourdi, épuisement,
effrayé, couché sur le sol.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 8

**Langues** -

**Défi** 4 (1 100 XP)

***Amorphe.*** Le pudding peut se déplacer dans un espace aussi étroit
qu'un pouce de large sans se tasser.

***Forme corrosive.*** Une créature qui touche le pudding ou le frappe
avec une attaque de mêlée alors qu'elle se trouve à moins de 1,5 m de
lui subit 4 (1d8) dégâts d'acide. Toute arme non magique en métal ou en
bois qui touche le pudding se corrode. Après avoir infligé des dégâts,
l'arme subit une pénalité permanente et cumulative de -1 aux jets de
dégâts. Si sa pénalité tombe à -5, l'arme est détruite. Les munitions
non magiques en métal ou en bois qui touchent le pudding sont détruites
après avoir infligé des dégâts.

Le pudding peut traverser du bois ou du métal non magique de 2 pouces
d'épaisseur en 1 round.

***Pattes d'araignée.*** Le boudin peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.


##### Actions

***Pseudopode.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) dégâts de
contondant plus 18 (4d8) dégâts d'acide. De plus, les armures non
magiques portées par la cible sont partiellement dissoutes et subissent
une pénalité permanente et cumulative de -1 à la CA qu'elles offrent.
L'armure est détruite si la pénalité réduit sa CA à 10.



##### Réactions

***Se divise.*** Quand un pudding de taille moyenne ou supérieure subit
des dégâts de foudre ou de tranchant, il se divise en deux nouveaux
puddings s'il a au moins 10 points de vie. Chaque nouveau pudding a un
nombre de points de vie égal à la moitié de celui du pudding original,
arrondi à l'inférieur. Les nouveaux boudins sont plus petits d'une
taille que le boudin d'origine.




#### Cube gélatineux

*Grand vase, sans alignement*

**Classe d'armure** 6

**Points de vie** 84 (8d10 + 40)

**Vitesse** 15 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   3 (-4)    20 (+5)   1 (-5)    6 (-2)    1 (-5)

  -----------------------------------------------------------

**Condition Immunités :** aveuglé, charmé, assourdi, épuisement,
effrayé, sujet à des troubles.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 8

**Langues** -

**Défi** 2 (450 XP)

***Vase Cube.*** Le cube occupe tout son espace. Les autres créatures
peuvent entrer dans l'espace, mais une créature qui le fait est soumise
à l'Engloutissement du cube et a un désavantage au jet de sauvegarde.

Les créatures à l'intérieur du cube peuvent être vues mais bénéficient
d'un abri total.

Une créature située à moins de 1,5 mètre du cube peut prendre une action
pour tirer une créature ou un objet hors du cube. Pour ce faire, elle
doit réussir un test de Force DC 12, et la créature qui tente l'action
subit 10 (3d6) dégâts d'acide.

Le cube ne peut contenir qu'une créature Grand ou jusqu'à quatre
créatures moyennes ou plus petites à la fois.

***Transparent.*** Même lorsque le cube est à la vue de tous, il faut
réussir un test de Sagesse (Perception) DC 15 pour repérer un cube qui
n'a ni bougé ni attaqué. Une créature qui tente de pénétrer dans
l'espace du cube alors qu'elle n'est pas consciente de sa présence
est surprise par le cube.


##### Actions

***Pseudopode.*** *Attaque avec une arme de corps-à-corps :* +4 au
toucher, portée de 1,5 m, une créature. *Touché :* 10 (3d6) dégâts
d'acide.

***Engulf.*** Le cube se déplace à la hauteur de sa vitesse. Ce faisant,
il peut entrer dans les espaces des créatures Grandes ou plus petites.
Chaque fois que le cube entre dans l'espace d'une créature, celle-ci
doit effectuer un jet de sauvegarde de Dextérité DC 12.

Si le jet de sauvegarde est réussi, la créature peut choisir d'être
poussée de 5 pieds en arrière ou sur le côté du cube. Une créature qui
choisit de ne pas être poussée subit les conséquences d'un jet de
sauvegarde raté.

En cas de sauvegarde ratée, le cube pénètre dans l'espace de la
créature, qui subit 10 (3d6) dégâts d'acide et est engloutie. La
créature engloutie ne peut pas respirer, est entravée et subit 21 (6d6)
dégâts d'acide au début de chaque tour du cube. Lorsque le cube se
déplace, la créature engloutie se déplace avec lui.

Une créature engloutie peut tenter de s'échapper en effectuant, dans le
cadre d'une action, un test de Force DC 12. En cas de succès, la
créature s'échappe et entre dans un espace de son choix dans un rayon
de 1,5 m du cube.




#### Vase grise

*Vase moyen, sans alignement*

**Classe d'armure** 8

**Points de vie** 22 (3d8 + 9)

**Vitesse** 10 ft., montée 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   6 (-2)    16 (+3)   1 (-5)    6 (-2)    2 (-4)

  -----------------------------------------------------------

**Compétences** Discrétion +2

**Résistances aux dégâts**: acide, froid, feu.

**Condition Immunités :** aveuglé, charmé, assourdi, épuisement,
effrayé, couché sur le sol.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 8

**Langues** -

**Défi** 1/2 (100 XP)

***Amorphe.*** Le vase peut se déplacer dans un espace aussi étroit
qu'un pouce de large sans se comprimer.

***Métal corrodé.*** Toute arme non magique en métal qui touche le vase
se corrode. Après avoir infligé des dégâts, l'arme subit une pénalité
permanente et cumulative de -1 aux jets de dégâts. Si sa pénalité tombe
à -5, l'arme est détruite. Les munitions non magiques en métal qui
touchent le vase sont détruites après avoir infligé des dégâts.

Le vase peut manger du métal non magique de 5 cm d'épaisseur en un
tour.

***Fausse apparence.*** Bien que le vase reste immobile, il est
impossible de le distinguer d'une piscine huileuse ou d'une roche
humide.


##### Actions

***Pseudopode.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touche :* 4 (1d6 + 1) dégâts de
matraquage plus 7 (2d6) dégâts d'acide, et si la cible porte une armure
métallique non magique, son armure est partiellement corrodée et subit
une pénalité permanente et cumulative de -1 à la CA qu'elle offre.
L'armure est détruite si la pénalité réduit sa CA à 10.




#### Gelée ocre

*Grand vase, sans alignement*

**Classe d'armure** 8

**Points de vie** 45 (6d10 + 12)

**Vitesse** 10 ft., montée 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   6 (-2)    14 (+2)   2 (-4)    6 (-2)    1 (-5)

  -----------------------------------------------------------

**Résistance aux dégâts** acide

**Immunités aux dégâts**: foudre, tranchant.

**Condition Immunités :** aveuglé, charmé, assourdi, épuisement,
effrayé, couché sur le sol.

**Sens** vision aveugle 60 ft. (aveugle au-delà de ce rayon), Perception
passive 8

**Langues** -

**Défi** 2 (450 XP)

***Amorphe.*** La gelée peut se déplacer dans un espace aussi étroit
qu'un pouce de large sans se comprimer.

***Pattes d'araignée.*** La gelée peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.


##### Actions

***Pseudopode.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d6 + 2) dégâts de
contondant plus 3 (1d6) dégâts d'acide.



##### Réactions

***Se divise.*** Lorsqu'une gelée de taille moyenne ou supérieure subit
des dégâts de foudre ou de tranchant, elle se divise en deux nouvelles
gelées si elle compte au moins 10 points de vie. Chaque nouvelle gelée a
des points de vie égaux à la moitié de ceux de la gelée d'origine,
arrondis à l'inférieur. Les nouvelles gelées sont d'une taille
inférieure à celle de la gelée d'origine.





### Squelettes


#### Squelette Minotaure

*Grand mort-vivant, maléfique licite*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 67 (9d10 + 18)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   11 (+0)   15 (+2)   6 (-2)    8 (-1)    5 (-3)

  -----------------------------------------------------------

**Vulnérabilités aux dégâts** contondants

**Immunité aux dommages** poison

**État Immunités** épuisement, empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** comprend Abysses mais ne peut pas parler

**Défi** 2 (450 XP)

***Charge.*** Si le squelette se déplace d'au moins 3 mètres en ligne
droite vers une cible et la touche avec une attaque gore dans le même
tour, la cible subit 9 (2d8) dégâts perforants supplémentaires. Si la
cible est une créature, elle doit réussir un jet de sauvegarde de Force
DC 14 ou être poussée jusqu'à 10 pieds de distance et mise à plat
ventre.


##### Actions

***Hache à deux mains.*** *Attaque avec une arme de corps-à-corps :* +6
pour toucher, allonge de 1,5 m, une cible. *Touché :* 17 (2d12 + 4)
points de dégâts tranchants.

***Gore.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de dégâts
perforants.




#### Squelette

*Mort-vivant moyen, maléfique.*

**Classe d'armure** 13 (rebuts d'armure)

**Points de vie** 13 (2d8 + 4)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   14 (+2)   15 (+2)   6 (-2)    8 (-1)    5 (-3)

  -----------------------------------------------------------

**Vulnérabilités aux dégâts** contondants

**Immunité aux dommages** poison

**État Immunités** épuisement, empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 9

Les**langues** comprennent toutes les langues qu'il a connues dans la
vie mais qu'il ne peut pas parler.

**Défi** 1/4 (50 XP)


##### Actions

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Arc court.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 80/320 pieds, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.




#### Squelette de cheval de guerre

*Grand mort-vivant, maléfique licite*

**Classe d'armure** 13 (débris de bardage)

**Points de vie** 22 (3d10 + 6)

**Vitesse** 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   12 (+1)   15 (+2)   2 (-4)    8 (-1)    5 (-3)

  -----------------------------------------------------------

**Vulnérabilités aux dégâts** contondants

**Immunité aux dommages** poison

**État Immunités** épuisement, empoisonné

**Sens** Vision dans le noir 60 ft., Perception passive 9

**Langues** -

**Défi** 1/2 (100 XP)


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts contondants.





### Sphinx


#### Androsphinx

*Grande monstruosité, légalement neutre.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 199 (19d10 + 95)

**Vitesse** 40 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   10 (+0)   20 (+5)   16 (+3)   18 (+4)   23 (+6)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Con +11, Int +9, Wis +10

**Compétences** Mystère +9, Perception +10, Religion +15

**Immunités aux dommages** psychiques ; contondant, perforant et
tranchant des attaques non magiques.

**Condition Immunités** charmé, effrayé

**Sens** vision véritable 120 ft., Perception passive 20

**Langues** communes, Sphinx

**Défi** 17 (18 000 XP)

***Inscrutable.*** Le sphinx est immunisé contre tout effet permettant
de ressentir ses émotions ou de lire ses pensées, ainsi que contre tout
sort de divination qu'il refuse. Les tests de Sagesse (Intuition)
effectués pour déterminer les intentions ou la sincérité du sphinx ont
un désavantage.

***Armes magiques.*** Les attaques des armes du sphinx sont magiques.

***Incantation.*** Le sphinx est un lanceur de sorts de 12e niveau. Sa
caractéristique d'incantation est la Sagesse (sauvegarde contre les
sorts DC 18, +10 pour toucher avec les attaques de sorts). Il n'a
besoin d'aucun composant matériel pour lancer ses sorts. Le sphinx a
les sorts de clercs suivants préparés :

Tours de magie (à volonté) : Flamme *sacrée*,
*Épargner* les mourrants,
*Thaumaturgie*

1er niveau (4 emplacements) : *Injonction*, *Détection du
mal et du bien*, *Détection de la
magie*

2ème niveau (3 emplacements) : Restauration
*partielle*

3e niveau (3 emplacements) : *Dissipation de la magie*,
*Langues*

4ème niveau (3 slots) : *Bannissement*, *Liberté de
mouvement*

5e niveau (2 emplacements) : Colonne *de flam*me,
Restauration *supérieure*

6ème niveau (1 emplacement) : *Festin des héros*


##### Actions

***Attaques multiples.*** Le sphinx effectue deux attaques de griffes.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +12 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 17 (2d10 + 6) points de
dégâts tranchants.

***Rugissement (3/Jour).*** Le sphinx émet un rugissement magique.
Chaque fois qu'il rugit avant de terminer un repos long, le rugissement
est plus fort et l'effet est différent, comme détaillé ci-dessous.
Chaque créature située à moins de 500 pieds du sphinx et capable
d'entendre le rugissement doit effectuer un jet de sauvegarde.

***Premier rugissement.*** Chaque créature qui échoue à un jet de
sauvegarde de Sagesse DC 18 est effrayée pendant 1 minute. Une créature
effrayée peut répéter le jet de sauvegarde à la fin de chacun de ses
tours, mettant fin à l'effet sur elle-même en cas de réussite.

***Second Rugissement.*** Chaque créature qui échoue à un jet de
sauvegarde de Sagesse DC 18 est assourdie et effrayée pendant 1 minute.
Une créature effrayée est paralysée et peut répéter le jet de sauvegarde
à la fin de chacun de ses tours, mettant fin à l'effet sur elle-même en
cas de réussite.

***Troisième rugissement.*** Chaque créature effectue un jet de
sauvegarde de Constitution DC 18. Si le jet de sauvegarde échoue, la
créature subit 44 (8d10) dégâts de tonnerre et est mise au sol. Si le
jet de sauvegarde est réussi, la créature subit moitié moins de dégâts
et n'est pas mise au sol.



##### Actions légendaires

Le sphinx peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le sphinx regagne les actions légendaires dépensées au début de son
tour.

***Attaque de griffes.*** Le sphinx effectue une attaque de griffes.

***Téléportation (coûte 2 actions).*** Le sphinx se téléporte
magiquement, ainsi que tout équipement qu'il porte, jusqu'à 120 pieds
dans un espace inoccupé qu'il peut voir.

***Lancer un sort (coûte 3 actions).*** Le sphinx lance un sort depuis
sa liste de sorts préparés, en utilisant un emplacement de sort comme
d'habitude.




#### Gynosphinx

*Grandes monstruosités, légalement neutres.*

**Classe d'armure** 17 (armure naturelle)

**Points de vie** 136 (16d10 + 48)

**Vitesse** 40 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   18 (+4)   18 (+4)   18 (+4)

  -----------------------------------------------------------

**Compétences** Arcane +12, Histoire +12, Perception +8, Religion +8.

**Résistance aux dégâts**: contondant, perforant et tranchant des
attaques non magiques.

**Immunités aux dommages** psychiques

**Condition Immunités** charmé, effrayé

**Sens** vision véritable 120 ft., Perception passive 18

**Langues** communes, Sphinx

**Défi** 11 (7 200 XP)

***Inscrutable.*** Le sphinx est immunisé contre tout effet permettant
de ressentir ses émotions ou de lire ses pensées, ainsi que contre tout
sort de divination qu'il refuse. Les tests de Sagesse (Intuition)
effectués pour déterminer les intentions ou la sincérité du sphinx ont
un désavantage.

***Armes magiques.*** Les attaques des armes du sphinx sont magiques.

***Incantation.*** Le sphinx est un lanceur de sorts de 9e niveau. Sa
caractéristique d'incantation est l'Intelligence (sauvegarde contre
les sorts DC 16, +8 pour toucher avec les attaques de sorts). Il n'a
besoin d'aucun composant matériel pour lancer ses sorts. Le sphinx a
les sorts de magicien suivants préparés :

Tours de magie (à volonté) : *main de mage*, *illusion
mineure*.

1er niveau (4 emplacements) : *Détection de la magie*,
*Identification*

2ème niveau (3 emplacements) : *Ténèbres*, *Localiser
objet*

3e niveau (3 emplacements) : *Dissipation de la magie*,
Délivrance des *malédictions*

4ème niveau (3 emplacements) : *Bannissement*,
*Invisibilité supérieure*

5e niveau (1 emplacement) : *Légendes* traditionnelles


##### Actions

***Attaques multiples.*** Le sphinx effectue deux attaques de griffes.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts tranchants.



##### Actions légendaires

Le sphinx peut effectuer 3 actions légendaires, en choisissant parmi les
options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le sphinx regagne les actions légendaires dépensées au début de son
tour.

***Attaque de griffes.*** Le sphinx effectue une attaque de griffes.

***Téléportation (coûte 2 actions).*** Le sphinx se téléporte
magiquement, ainsi que tout équipement qu'il porte, jusqu'à 120 pieds
dans un espace inoccupé qu'il peut voir.

***Lancer un sort (coûte 3 actions).*** Le sphinx lance un sort depuis
sa liste de sorts préparés, en utilisant un emplacement de sort comme
d'habitude.





### Vampires


#### Vampire

*Mort-vivant moyen (Métamorphe), maléfique.*

**Classe d'armure** 16 (armure naturelle)

**Points de vie** 144 (17d8 + 68)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   18 (+4)   18 (+4)   17 (+3)   15 (+2)   18 (+4)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +9, Wis +7, Cha +9

**Compétences** Perception +7, Discrétion +9

**Résistance aux dégâts** nécrotiques ; contondant, perforant et
tranchant des attaques non magiques.

**Sens** Vision dans le noir 120 ft., Perception passive 17

Les**langues** qu'il a connues dans la vie

**Défi** 13 (10 000 XP)

***Changement de forme.*** Si le vampire n'est pas à la lumière du
soleil ou dans l'eau courante, il peut utiliser son action pour se
métamorphoser en une Très petite (TP) ou un nuage de brume moyen, ou
reprendre sa véritable forme.

Lorsqu'il est sous forme de chauve-souris, le vampire ne peut pas
parler, sa vitesse de marche est de 1,5 mètre et sa vitesse de vol est
de 10 mètres. Ses statistiques, autres que sa taille et sa vitesse, sont
inchangées. Tout ce qu'il porte se transforme avec lui, mais rien de ce
qu'il transporte ne se transforme. Il revient à sa vraie forme s'il
meurt.

Lorsqu'il est sous forme de brume, le vampire ne peut entreprendre
aucune action, parler ou manipuler des objets. Il est en apesanteur, a
une vitesse de vol de 20 pieds, peut faire du surplace, et peut entrer
dans l'espace d'une créature hostile et s'y arrêter. De plus, si
l'air peut traverser un espace, la brume peut le faire sans se presser,
et elle ne peut pas traverser l'eau. Elle a un avantage aux jets de
sauvegarde de Force, de Dextérité et de Constitution, et elle est
immunisée contre tous les dégâts non magiques, à l'exception de ceux
qu'elle subit à la lumière du soleil.

***Résistance légendaire (3/Jour).*** Si le vampire échoue à un jet de
sauvegarde, il peut choisir de le réussir à la place.

***Échapée brumeuse.*** Lorsqu'il tombe à 0 point de vie hors de son
lieu de repos, le vampire se transforme en un nuage de brume (comme dans
le trait Métamorphe) au lieu de tomber inconscient, à condition qu'il
ne soit pas au soleil ou dans l'eau courante. S'il ne peut pas se
transformer, il est détruit.

Tant qu'il n'a pas de points de vie sous forme de brume, il ne peut
pas reprendre sa forme de vampire, et il doit atteindre son lieu de
repos dans les 2 heures ou être détruit. Une fois dans son lieu de
repos, il reprend sa forme de vampire. Il est alors paralysé jusqu'à ce
qu'il récupère au moins 1 point de vie. Après avoir passé 1 heure dans
son lieu de repos avec 0 point de vie, il regagne 1 point de vie.

***Régénération.*** Le vampire regagne 20 points de vie au début de son
tour s'il a au moins 1 point de vie et n'est pas au soleil ou dans
l'eau courante. Si le vampire subit des dégâts radiants ou des dégâts
d'eau bénite, ce trait ne fonctionne pas au début du prochain tour du
vampire.

***Pattes d'araignée.*** Le vampire peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Les faiblesses du Vampire.*** Le vampire présente les défauts
suivants :

**Interdiction.** Le vampire ne peut pas entrer dans une résidence sans
l'invitation d'un des occupants.

**Contamination par l'eau courante.** Le vampire subit 20 dégâts
d'acide s'il termine son tour dans l'eau courante.

**Pieu dans le cœur.** Si une arme perforante en bois est enfoncée dans
le cœur du vampire alors que celui-ci est frappé d'incapacité dans son
lieu de repos, le vampire est paralysé jusqu'à ce que le pieu soit
retiré.

**Hypersensibilité à la lumière du soleil.** Le vampire subit 20 dégâts
radiant lorsqu'il commence son tour à la lumière du soleil. Lorsqu'il
est à la lumière du soleil, il a un désavantage aux jets d'attaque et
aux tests de capacité.


##### Actions

***Attaques multiples (forme Vampire uniquement).*** Le vampire effectue
deux attaques, dont une seule peut être une attaque de morsure.

***Coup à mains nues (forme Vampire uniquement).*** *Attaque avec une
arme de corps-à-corps :* +9 pour toucher, allonge de 1,5 m, une
créature. *Touché :* 8 (1d8 + 4) points de dégâts contondants. Au lieu
d'infliger des dégâts, le vampire peut agripper la cible (échappatoire
DC 18).

***Morsure (forme Chauve-souris ou Vampire uniquement).*** *Attaque avec
une arme de corps-à-corps :* +9 pour toucher, portée de 1,5 m, une
créature consentante, ou une créature agrippée par le vampire, frappée
d'incapacité ou entravée. *Touché :* 7 (1d6 + 4) dégâts perforants plus
10 3d6) dégâts nécrotiques. Le maximum de points de vie de la cible est
réduit d'un montant égal aux dégâts nécrotiques subis, et le vampire
regagne un nombre de points de vie égal à ce montant. La réduction dure
jusqu'à ce que la cible termine un long repos. La cible meurt si cet
effet réduit son maximum de points de vie à 0. Un humanoïde tué de cette
façon puis enterré dans le sol se relève la nuit suivante sous forme de
rapetissement de vampire sous le contrôle du vampire.

***Charme.*** Le vampire cible un humanoïde qu'il peut voir dans un
rayon de 30 pieds autour de lui. Si la cible peut voir le vampire, elle
doit réussir un jet de sauvegarde de Sagesse DC 17 contre cette magie ou
être charmée par le vampire. La cible charmée considère le vampire comme
un ami de confiance qui doit être écouté et protégé. Bien que la cible
ne soit pas sous le contrôle du vampire, elle accepte les demandes ou
les actions du vampire de la manière la plus favorable qui soit, et elle
est une cible consentante pour l'attaque par morsure du vampire.

Chaque fois que le vampire ou ses compagnons font quelque chose de
contaminé à la cible, il peut répéter le jet de sauvegarde, mettant fin
à l'effet sur lui-même en cas de réussite. Sinon, l'effet dure 24
heures ou jusqu'à ce que le vampire soit détruit, qu'il se trouve sur
un autre plan d'existence que la cible ou qu'il effectue une action
bonus pour mettre fin à l'effet.

***Enfants de la nuit (1/Jour).*** Le vampire appelle magiquement 2d4
nuées de chauves-souris ou de rats, à condition que le soleil ne soit
pas levé. À l'extérieur, le vampire peut appeler 3d6 loups à la place.
Les créatures appelées arrivent en 1d4 rounds, agissant comme des alliés
du vampire et obéissant à ses ordres vocaux. Les bêtes restent pendant 1
heure, jusqu'à la mort du vampire, ou jusqu'à ce que le vampire les
congédie en action bonus.



##### Actions légendaires

Le vampire peut effectuer 3 actions légendaires, en choisissant parmi
les options ci-dessous. Une seule option d'action légendaire peut être
utilisée à la fois et uniquement à la fin du tour d'une autre créature.
Le vampire récupère les actions légendaires dépensées au début de son
tour.

***Déplacement.*** Le vampire se déplace à la hauteur de sa vitesse sans
provoquer d'attaque d'opportunité.

***Coup sans arme.*** Le vampire porte un coup sans arme.

***Morsure (Coûte 2 Actions).*** Le vampire effectue une attaque de
morsure.




#### Vampirion Spawn

*Mort-vivant de taille moyenne, mal neutre.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 82 (11d8 + 33)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   16 (+3)   16 (+3)   11 (+0)   10 (+0)   12 (+1)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Wis +3

**Compétences** Perception +3, Discrétion +6

**Résistance aux dégâts** nécrotiques ; contondant, perforant et
tranchant des attaques non magiques.

**Sens** Vision dans le noir 60 ft., Perception passive 13

Les**langues** qu'il a connues dans la vie

**Défi** 5 (1 800 XP)

***Régénération.*** Le vampire regagne 10 points de vie au début de son
tour s'il a au moins 1 point de vie et n'est pas au soleil ou dans
l'eau courante. Si le vampire subit des dégâts radiants ou des dégâts
d'eau bénite, ce trait ne fonctionne pas au début du prochain tour du
vampire.

***Pattes d'araignée.*** Le vampire peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Les faiblesses du Vampire.*** Le vampire présente les défauts
suivants :

**Interdiction.** Le vampire ne peut pas entrer dans une résidence sans
l'invitation d'un des occupants.

**Contamination par l'eau courante.** Le vampire subit 20 dégâts
d'acide lorsqu'il termine son tour dans l'eau courante.

**Pieu dans le cœur.** Le vampire est détruit si une arme perforante en
bois est enfoncée dans son cœur alors qu'il est frappé d'incapacité
dans son lieu de repos.

**Hypersensibilité à la lumière du soleil.** Le vampire subit 20 dégâts
radiant lorsqu'il commence son tour à la lumière du soleil. Lorsqu'il
est à la lumière du soleil, il a un désavantage aux jets d'attaque et
aux tests de capacité.


##### Actions

***Attaques multiples.*** Le vampire effectue deux attaques, dont une
seule peut être une attaque de morsure.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 8 (2d4 + 3) dégâts
tranchants. Au lieu d'infliger des dégâts, le vampire peut agripper la
cible en s'échappant DC 13).

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, portée de 1,5 m, une créature consentante, ou une créature
agrippée par le vampire, frappée d'incapacité ou entravée. *Touché :* 6
(1d6 + 3) dégâts perforants plus 7 (2d6) dégâts nécrotiques. Le maximum
de points de vie de la cible est réduit d'un montant égal aux dégâts
nécrotiques subis, et le vampire regagne un nombre de points de vie égal
à ce montant. La réduction dure jusqu'à ce que la cible termine un long
repos. La cible meurt si cet effet réduit son maximum de points de vie à
0.





### Zombies


#### Zombi ogre

*Grand mort-vivant, mal neutre*

**Classe d'armure** 8

**Points de vie** 85 (9d10 + 36)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   6 (-2)    18 (+4)   3 (-4)    6 (-2)    5 (-3)

  -----------------------------------------------------------

**Jets de sauvegarde** Wis +0

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 8

**Langues** comprend le commun et le géant, mais ne peut pas parler.

**Défi** 2 (450 XP)

***Force d'âme de mort-vivant.*** Si les dégâts réduisent le zombi à 0
point de vie, il doit effectuer un jet de sauvegarde de Constitution
avec un DC de 5 + les dégâts subis, sauf si les dégâts sont radiants ou
proviennent d'un coup critique. En cas de réussite, le zombi est ramené
à 1 point de vie.


##### Actions

***Morgenstern.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts contondants.




#### Zombi

*Mort-vivant de taille moyenne, mal neutre.*

**Classe d'armure** 8

**Points de vie** 22 (3d8 + 9)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   6 (-2)    16 (+3)   3 (-4)    6 (-2)    5 (-3)

  -----------------------------------------------------------

**Jets de sauvegarde** Wis +0

**Immunité aux dommages** poison

**État Immunités** empoisonnées

**Sens** Vision dans le noir 60 ft., Perception passive 8

Les**langues** comprennent les langues qu'il a connues dans la vie mais
qu'il ne peut pas parler.

**Défi** 1/4 (50 XP)

***Force d'âme de mort-vivant.*** Si les dégâts réduisent le zombi à 0
point de vie, il doit effectuer un jet de sauvegarde de Constitution
avec un DC de 5 + les dégâts subis, sauf si les dégâts sont radiants ou
proviennent d'un coup critique. En cas de réussite, le zombi est ramené
à 1 point de vie.


##### Actions

***Slam.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) points de dégâts
contondants.







### Créatures diverses

Cette section contient les statistiques de divers animaux, vermines et
autres créatures. Les blocs de statistiques sont organisés par ordre
alphabétique du nom de la créature.



#### Grand singe

*Bête moyenne, sans alignement.*

**Classe d'armure** 12

**Points de vie** 19 (3d8 + 6)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   14 (+2)   14 (+2)   6 (-2)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Athlétisme +5, Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 1/2 (100 XP)


##### Actions

***Attaques multiples.*** Le Grand singe effectue deux attaques de
poing.

***Poing.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de dégâts
contondants.

***Rock.*** *Attaque avec une arme à distance :* +5 pour toucher, portée
de 25/50 pieds, une cible. *Touché :* 6 (1d6 + 3) points de dégâts
contondants.




#### Arbuste éveillé

*Petite plante, sans alignement*

**Classe d'armure** 9

**Points de vie** 10 (3d6)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    8 (-1)    11 (+0)   10 (+0)   10 (+0)   6 (-2)

  -----------------------------------------------------------

Feu sur les**vulnérabilités aux dégâts**

**Résistance aux dégâts** perforant

**Sens** passif Perception 10

**Langues** une langue connue par son créateur

**Défi** 0 (10 XP)

***Fausse apparence.*** Lorsque l'arbuste reste immobile, il est
impossible de le distinguer d'un arbuste normal.


##### Actions

***Râteau.*** *Attaque avec une arme de corps-à-corps :* +1 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 (1d4 - 1) dégâts
tranchants.

Un **arbuste éveillé** est un arbuste ordinaire auquel le sort
d'*éveil* ou une magie similaire confère sensibilité et
mobilité.




#### Arbre éveillé

*Très grande plante, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 59 (7d12 + 14)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   6 (-2)    15 (+2)   10 (+0)   10 (+0)   7 (-2)

  -----------------------------------------------------------

Feu sur les**vulnérabilités aux dégâts**

**Résistance aux dégâts**: contondant, perforant.

**Sens** passif Perception 10

**Langues** une langue connue par son créateur

**Défi** 2 (450 XP)

***Fausse apparence.*** Bien que l'arbre reste immobile, il est
impossible de le distinguer d'un arbre normal.


##### Actions

***Slam.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 14 (3d6 + 4) points de dégâts
contondants.

Un **arbre éveillé** est un arbre ordinaire auquel le sort
d'*éveil* ou une magie similaire confère sensibilité et
mobilité.




#### Bec de hache

*Grandes bêtes, sans alignement.*

**Classe d'armure** 11

**Points de vie** 19 (3d10 + 3)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   12 (+1)   12 (+1)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Bec.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) points de dégâts
tranchants.

Le **bec de hache** est un grand oiseau incapable de voler, doté de
solides pattes et d'un bec lourd et cunéiforme. Il a un mauvais
caractère et a tendance à attaquer toute créature inconnue qui
s'approche de trop près.




#### Babouin

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 3 (1d6)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    14 (+2)   11 (+0)   4 (-3)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 11

**Langues** -

**Défi** 0 (10 XP)

***Tactiques de meute.*** Le babouin a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du babouin se trouve à
moins de 1,5 m de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +1 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 (1d4 - 1) dégâts
perforants.




#### Blaireau

*Très petite (TP), sans alignement.*

**Classe d'armure** 10

**Points de vie** 3 (1d4 + 1)

**Vitesse** 20 ft., creuser 5 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   4 (-3)    11 (+0)   12 (+1)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 11

**Langues** -

**Défi** 0 (10 XP)

***Odorat aiguisé.*** Le blaireau a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.




#### Chauve-souris

*Très petite (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 1 (1d4 - 1)

**Vitesse** 5 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    15 (+2)   8 (-1)    2 (-4)    12 (+1)   4 (-3)

  -----------------------------------------------------------

**Sens** vision aveugle 60 ft., Perception passive 11

**Langues** -

**Défi** 0 (10 XP)

***Echolocation.*** La chauve-souris ne peut pas utiliser sa vision
aveugle lorsqu'elle est assourdie.

***Ouïe fine.*** La Chauve-souris a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'audition.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +0 au toucher,
portée de 1,5 m, une créature. *Touché :* 1 dégât perforant.




#### Ours noir

*Bête moyenne, sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 19 (3d8 + 6)

**Vitesse** 40 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   14 (+2)   2 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 1/2 (100 XP)

***Odorat aiguisé.*** L'ours a un avantage sur les tests de Sagesse
(Perception) qui font appel à l'odorat.


##### Actions

***Attaques multiples.*** L'ours effectue deux attaques : une avec sa
morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) dégâts
tranchants.




#### Chien clignotant

*Fée moyenne, bon droit.*

**Classe d'armure** 13

**Points de vie** 22 (4d8 + 4)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   17 (+3)   12 (+1)   10 (+0)   13 (+1)   11 (+0)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +5

**Sens** passif Perception 13

**Langues** Chien clignotant, comprend le sylvestre mais ne peut pas le
parler

**Défi** 1/4 (50 XP)

***Ouïe et odorat aiguisés.*** Le chien a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) points de
dégâts perforants.

***Téléportation (Recharge 4-6).*** Le chien se téléporte magiquement,
ainsi que tout équipement qu'il porte, jusqu'à 12 mètres dans un
espace inoccupé qu'il peut voir. Avant ou après la téléportation, le
chien peut effectuer une attaque par morsure.

Le **Chien clignotant** doit son nom à sa capacité de clignoter et de
disparaître, un talent qu'il utilise pour faciliter ses attaques et
éviter les contaminations.




#### Faucon de sang

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 7 (2d6)

**Vitesse** 10 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    14 (+2)   10 (+0)   3 (-4)    14 (+2)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 1/8 (25 XP)

***Vue perçante.*** Le faucon a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.

***Tactiques de meute.*** Le faucon a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du faucon se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Bec.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de dégâts
perforants.

Tirant son nom de ses plumes cramoisies et de sa nature agressive, le
faucon sanguin attaque sans crainte presque tous les animaux, les
poignardant de son bec en forme de poignard. Les faucons sanguinaires se
rassemblent en grand nombre, attaquant en meute pour abattre leurs
proies.




#### Sanglier

*Bête moyenne, sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   11 (+0)   12 (+1)   2 (-4)    9 (-1)    5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 9

**Langues** -

**Défi** 1/4 (50 XP)

***Charge.*** Si le sanglier se déplace d'au moins 6 mètres en ligne
droite vers une cible et la touche avec une attaque de défense dans le
même tour, la cible subit 3 (1d6) dégâts tranchants supplémentaires. Si
la cible est une créature, elle doit réussir un jet de sauvegarde de
Force DC 11 ou être mise au sol.

***Implacable (se recharge après un repos court ou long).*** Si le
sanglier subit 7 points de dégâts ou moins qui le réduiraient à 0 point
de vie, il est réduit à 1 point de vie à la place.


##### Actions

***Tusk.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) dégâts tranchants.




#### Ours brun

*Grandes bêtes, sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 34 (4d10 + 12)

**Vitesse** 40 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   16 (+3)   2 (-4)    13 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 1 (200 XP)

***Odorat aiguisé.*** L'ours a un avantage sur les tests de Sagesse
(Perception) qui font appel à l'odorat.


##### Actions

***Attaques multiples.*** L'ours effectue deux attaques : une avec sa
morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d8 + 4) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts tranchants.




#### Chameau

*Grande bête, sans alignement.*

**Classe d'armure** 9

**Points de vie** 15 (2d10 + 4)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   8 (-1)    14 (+2)   2 (-4)    8 (-1)    5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 9

**Langues** -

**Défi** 1/8 (25 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts
contondants.




#### Chat

*Très petite (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 2 (1d4)

**Vitesse** 40 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    15 (+2)   10 (+0)   3 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Odorat aiguisé.*** Le Chat a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.


##### Actions

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +0 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât tranchant.




#### Serpent constricteur

*Grandes bêtes, sans alignement.*

**Classe d'armure** 12

**Points de vie** 13 (2d10 + 2)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   12 (+1)   1 (-5)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 10 ft., Perception passive 10

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 5 (1d6 + 2) dégâts
perforants.

***Constriction.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 6 (1d8 + 2) dégâts
contondants, et la cible est agrippée (évasion DC 14). Jusqu'à la fin
de cet agrippement, la créature est entravée, et le serpent ne peut pas
constricter une autre cible.




#### Crabe

*Très petite (TP), sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 2 (1d4)

**Vitesse** 20 ft., nager 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    11 (+0)   10 (+0)   1 (-5)    8 (-1)    2 (-4)

  -----------------------------------------------------------

**Compétences** Discrétion +2

**Sens** vision aveugle 30 ft., Perception passive 9

**Langues** -

**Défi** 0 (10 XP)

***Amphibie.*** Le crabe peut respirer l'air et l'eau.


##### Actions

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +0 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât contondant.




#### Crocodile

*Grande bête, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 19 (3d10 + 3)

**Vitesse** 20 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   13 (+1)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Discrétion +2

**Sens** passif Perception 10

**Langues** -

**Défi** 1/2 (100 XP)

***Retenir sa respiration.*** Le crocodile peut retenir sa respiration
pendant 15 minutes.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 7 (1d10 + 2) dégâts
perforants, et la cible est agrippée (évasion DC 12). Jusqu'à la fin de
cet agrippement, la cible est entravée, et le crocodile ne peut pas
mordre une autre cible.




#### Chien du trépas

*Monstruosité moyenne, mal neutre.*

**Classe d'armure** 12

**Points de vie** 39 (6d8 + 12)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   14 (+2)   14 (+2)   3 (-4)    13 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +5, Discrétion +4

**Sens** Vision dans le noir 120 ft., Perception passive 15

**Langues** -

**Défi** 1 (200 XP)

***Bicéphale.*** Le chien a un avantage sur les tests de Sagesse
(Perception) et sur les jets de sauvegarde contre l'aveuglement, le
charme, la surdité, la frayeur, l'étourdissement ou le Déblocage.


##### Actions

***Attaques multiples.*** Le chien effectue deux attaques par morsure.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Constitution DC 12 contre la maladie ou s'empoisonner
jusqu'à ce que la maladie soit guérie. Toutes les 24 heures qui
s'écoulent, la créature doit répéter le jet de sauvegarde, réduisant
son maximum de points de vie de 5 (1d10) en cas d'échec. Cette
réduction dure jusqu'à la guérison de la maladie. La créature meurt si
la maladie réduit son maximum de points de vie à 0.

Le **Chien du trépas** est un affreux molosse à deux têtes qui erre dans
les plaines et les déserts. La haine brûle dans le cœur du chien de la
mort, et son goût pour la chair humanoïde le pousse à attaquer les
voyageurs et les explorateurs. La salive du Chien du trépas est porteuse
d'une maladie infectieuse qui fait pourrir lentement la chair de la
victime jusqu'à l'os.




#### Cerf

*Bête moyenne, sans alignement.*

**Classe d'armure** 13

**Points de vie** 4 (1d8)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   16 (+3)   11 (+0)   2 (-4)    14 (+2)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 12

**Langues** -

**Défi** 0 (10 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts
perforants.




#### Loup sanguinaire

*Grande bête, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 37 (5d10 + 10)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   15 (+2)   3 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Sens** passif Perception 13

**Langues** -

**Défi** 1 (200 XP)

***Ouïe et odorat aiguisés.*** Le loup a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Tactiques de meute.*** Le loup a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du loup se trouve à moins
de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 13 ou être mise au sol.




#### Cheval de trait

*Grande bête, sans alignement.*

**Classe d'armure** 10

**Points de vie** 19 (3d10 + 3)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   10 (+0)   12 (+1)   2 (-4)    11 (+0)   7 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d4 + 4) points de
dégâts contondants.




#### Aigle

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 3 (1d6)

**Vitesse** 10 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    15 (+2)   10 (+0)   2 (-4)    14 (+2)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 0 (10 XP)

***Vue perçante.*** L'aigle a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.


##### Actions

***Talons.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts tranchants.




#### Éléphant

*Très grande bête, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 76 (8d12 + 24)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   22 (+6)   9 (-1)    17 (+3)   3 (-4)    11 (+0)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 4 (1 100 XP)

***Charge de piétinement.*** Si l'éléphant se déplace d'au moins 6
mètres en ligne droite vers une créature et la frappe avec une attaque
gore dans le même tour, cette cible doit réussir un jet de sauvegarde de
Force DC 12 ou être mise au sol. Si la cible est couchée, l'éléphant
peut effectuer une attaque de piétinement contre elle en tant qu'action
bonus.


##### Actions

***Gore.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 19 (3d8 + 6) points de dégâts
perforants.

***Stomp.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
portée de 1,5 m, une créature couchée. *Touché :* 22 (3d10 + 6) points
de dégâts contondants.




#### Élan

*Grande bête, sans alignement.*

**Classe d'armure** 10

**Points de vie** 13 (2d10 + 2)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   10 (+0)   12 (+1)   2 (-4)    10 (+0)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/4 (50 XP)

***Charge.*** Si l'Élan se déplace d'au moins 6 mètres en ligne droite
vers une cible et la touche avec une attaque bélier dans le même tour,
la cible subit 7 (2d6) dégâts supplémentaires. Si la cible est une
créature, elle doit réussir un jet de sauvegarde de Force DC 13 ou être
mise au sol.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de dégâts
contondants.

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, portée de 1,5 m, une créature couchée. *Touché :* 8 (2d4 + 3)
dégâts contondants.




#### Serpent volant

*Très petite (TP), sans alignement.*

**Classe d'armure** 14

**Points de vie** 5 (2d4)

**Vitesse** 30 ft., Vol 60 ft., Nage 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   4 (-3)    18 (+4)   11 (+0)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Sens** vision aveugle 10 ft., Perception passive 11

**Langues** -

**Défi** 1/8 (25 XP)

***Vol.*** Le serpent ne provoque pas d'attaque d'opportunité
lorsqu'il vole hors de portée d'un ennemi.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant plus
7 (3d4) dégâts de poison.

Un **serpent volant** est un serpent ailé aux couleurs vives que l'on
trouve dans les jungles reculées. Les tribus et les cultistes
domestiquent parfois les serpents volants pour en faire des messagers
qui livrent des parchemins enroulés dans leurs bobines.




#### Grenouille

*Très petite (TP), sans alignement.*

**Classe d'armure** 11

**Points de vie** 1 (1d4 - 1)

**Vitesse** 20 ft., nager 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   1 (-5)    13 (+1)   8 (-1)    1 (-5)    8 (-1)    3 (-4)

  -----------------------------------------------------------

**Compétences** Perception +1, Discrétion +3

**Sens** Vision dans le noir 30 ft., Perception passive 11

**Langues** -

**Défi** 0 (0 XP)

***Amphibie.*** La grenouille peut respirer de l'air et de l'eau.

***Saut debout.*** Le saut en longueur de la grenouille peut atteindre 3
mètres et son saut en hauteur peut atteindre 1,5 mètre, avec ou sans
départ lancé.

Une **grenouille** n'a pas d'attaque efficace. Elle se nourrit de
petits insectes et vit généralement près de l'eau, dans les arbres ou
sous terre. Les statistiques de la grenouille peuvent également être
utilisées pour représenter un crapaud.



#### Grand singe géant

*Très grande bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 157 (15d12 + 60)

**Vitesse** 40 ft., ascension 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   14 (+2)   18 (+4)   7 (-2)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Athlétisme +9, Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 7 (2 900 XP)


##### Actions

***Attaques multiples.*** Le Grand singe effectue deux attaques de
poing.

***Poing.*** *Attaque avec une arme de corps-à-corps :* +9 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 22 (3d10 + 6) points de
dégâts contondants.

***Rock.*** *Attaque avec une arme à distance :* +9 pour toucher, portée
de 50/100 pieds, une cible. *Touché :* 30 (7d6 + 6) points de dégâts
contondants.




#### Blaireau géant

*Bête moyenne, sans alignement.*

**Classe d'armure** 10

**Points de vie** 13 (2d8 + 4)

**Vitesse** 30 ft., terrier 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   10 (+0)   15 (+2)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 11

**Langues** -

**Défi** 1/4 (50 XP)

***Odorat aiguisé.*** Le blaireau a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.


##### Actions

***Attaques multiples.*** Le Blaireau effectue deux attaques : une avec
sa morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (2d4 + 1) points de
dégâts tranchants.




#### Chauve-souris géante

*Grande bête, sans alignement.*

**Classe d'armure** 13

**Points de vie** 22 (4d10)

**Vitesse** 10 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   16 (+3)   11 (+0)   2 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** vision aveugle 60 ft., Perception passive 11

**Langues** -

**Défi** 1/4 (50 XP)

***Echolocation.*** La chauve-souris ne peut pas utiliser sa vision
aveugle lorsqu'elle est assourdie.

***Ouïe fine.*** La Chauve-souris a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'audition.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, portée de 1,5 m, une créature. *Touché :* 5 (1d6 + 2) dégâts
perforants.




#### Sanglier géant

*Grandes bêtes, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 42 (5d10 + 15)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   10 (+0)   16 (+3)   2 (-4)    7 (-2)    5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 8

**Langues** -

**Défi** 2 (450 XP)

***Charge.*** Si le sanglier se déplace d'au moins 6 mètres en ligne
droite vers une cible et la touche avec une attaque de défense dans le
même tour, la cible subit 7 (2d6) dégâts tranchants supplémentaires. Si
la cible est une créature, elle doit réussir un jet de sauvegarde de
Force DC 13 ou être mise à plat ventre.

***Implacable (se recharge après un repos court ou long).*** Si le
sanglier subit 10 points de dégâts ou moins qui le réduiraient à 0 point
de vie, il est réduit à 1 point de vie à la place.


##### Actions

***Tusk.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de dégâts
tranchants.




#### Mille-pattes géant

*Petite bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 4 (1d6 + 1)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    14 (+2)   12 (+1)   1 (-5)    7 (-2)    3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 30 ft., Perception passive 8

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 4 (1d4 + 2) dégâts
perforants, et la cible doit réussir un jet de sauvegarde de
Constitution DC 11 ou subir 10 (3d6) dégâts de poison. Si les dégâts de
poison réduisent la cible à 0 point de vie, elle est stable mais
empoisonnée pendant 1 heure, même après avoir regagné des points de vie,
et est paralysée lorsqu'elle est empoisonnée de cette façon.




#### Serpent constricteur géant

*Très grande bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 60 (8d12 + 8)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   14 (+2)   12 (+1)   1 (-5)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** vision aveugle 10 ft., Perception passive 12

**Langues** -

**Défi** 2 (450 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 10 pieds, une créature. *Touché :* 11 (2d6 + 4)
dégâts perforants.

***Constriction.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, portée de 1,5 m, une créature. *Touché :* 13 (2d8 + 4) points
de dégâts contondants, et la cible est agrippée (évasion DC 16).
Jusqu'à la fin de cet agrippement, la créature est entravée, et le
serpent ne peut pas constricter une autre cible.




#### Crabe géant

*Bête moyenne, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 13 (3d8)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   15 (+2)   11 (+0)   1 (-5)    9 (-1)    3 (-4)

  -----------------------------------------------------------

**Compétences** Discrétion +4

**Sens** vision aveugle 30 ft., Perception passive 9

**Langues** -

**Défi** 1/8 (25 XP)

***Amphibie.*** Le crabe peut respirer l'air et l'eau.


##### Actions

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) dégâts
contondants, et la cible est agrippée (évasion DC 11). Le crabe possède
deux pinces, chacune ne pouvant agripper qu'une seule cible.




#### Crocodile géant

*Très grande bête, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 85 (9d12 + 27)

**Vitesse** 30 ft., nager 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   9 (-1)    17 (+3)   2 (-4)    10 (+0)   7 (-2)

  -----------------------------------------------------------

**Compétences** Discrétion +5

**Sens** passif Perception 10

**Langues** -

**Défi** 5 (1 800 XP)

***Retenir sa respiration.*** Le crocodile peut retenir sa respiration
pendant 30 minutes.


##### Actions

***Attaques multiples.*** Le crocodile effectue deux attaques : une avec
sa morsure et une avec sa queue.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +8 pour
toucher, allonge de 1,5 m, une cible. *Touche :* 21 (3d10 + 5) points de
dégâts perforants, et la cible est agrippée (évasion DC 16). Jusqu'à la
fin de cet agrippement, la cible est entravée, et le crocodile ne peut
pas mordre une autre cible.

***Queue.*** *Attaque avec une arme de corps-à-corps :* +8 pour toucher,
allonge de 10 pieds, une cible non agrippée par le crocodile. *Touché :*
14 (2d8 + 5) points de dégâts contondants. Si la cible est une créature,
elle doit réussir un jet de sauvegarde de Force DC 16 ou être mise à
terre.




#### Aigle géant

*Grandes bêtes, neutre bon*

**Classe d'armure** 13

**Points de vie** 26 (4d10 + 4)

**Vitesse** 10 ft., Vol 80 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   17 (+3)   13 (+1)   8 (-1)    14 (+2)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** Aigle géant, comprend le commun et l'aérien mais ne peut
pas les parler.

**Défi** 1 (200 XP)

***Vue perçante.*** L'aigle a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.


##### Actions

***Attaques multiples.*** L'aigle effectue deux attaques : une avec son
bec et une avec ses serres.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de dégâts
perforants.

***Talons.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de
dégâts tranchants.

Un **aigle géant** est une créature noble qui parle sa propre langue et
comprend les discours en langue commune. Un couple d'aigles géants
accouplés a généralement jusqu'à quatre œufs ou jeunes dans son nid
(traitez les jeunes comme des aigles normaux).




#### Élan géant

*Très grande bête, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 42 (5d12 + 10)

**Vitesse** 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   16 (+3)   14 (+2)   7 (-2)    14 (+2)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** Élan géant, comprend le commun, l'elfe et le sylvestre mais
ne peut pas les parler.

**Défi** 2 (450 XP)

***Charge.*** Si l'Élan se déplace d'au moins 6 mètres en ligne droite
vers une cible et la touche avec une attaque bélier dans le même tour,
la cible subit 7 (2d6) points de dégâts supplémentaires. Si la cible est
une créature, elle doit réussir un jet de sauvegarde de Force DC 14 ou
être mise au sol.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +6 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 11 (2d6 + 4) points de dégâts
contondants.

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, portée de 1,5 m, une créature couchée. *Touché :* 22 (4d8 + 4)
dégâts contondants.

L'Élan géant majestueux est rare au point que son apparition est
souvent considérée comme le présage d'un événement important, comme la
naissance d'un roi. Les légendes parlent de dieux qui prennent la forme
d'élans géants lorsqu'ils visitent le plan matériel. De nombreuses
cultures pensent donc que chasser ces créatures revient à s'attirer la
colère divine.




#### Scarabée de feu géant

*Petite bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 4 (1d6 + 1)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    10 (+0)   12 (+1)   1 (-5)    7 (-2)    3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 30 ft., Perception passive 8

**Langues** -

**Défi** 0 (10 XP)

***Illumination.*** Le scarabée diffuse une lumière vive dans un rayon
de 10 pieds et une lumière chétive sur 10 pieds supplémentaires.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +1 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 2 (1d6 - 1) dégâts
tranchants.

Le **scarabée géant** du feu est une créature nocturne qui doit son nom
à une paire de glandes incandescentes qui émettent de la lumière. Les
mineurs et les aventuriers apprécient ces créatures, car les glandes
d'un scarabée de feu géant continuent à émettre de la lumière pendant
1d6 jours après la mort du scarabée. Les scarabées de feu géants se
trouvent le plus souvent sous terre et dans les forêts sombres.




#### Vol géant

*Grande bête, sans alignement.*

**Classe d'armure** 11

**Points de vie** 19 (3d10 + 3)

**Vitesse** 30 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   13 (+1)   13 (+1)   2 (-4)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** -

**Défi** - (0 XP)

Voir \"*Figurine merveilleuse*\".



#### Grenouille géante

*Bête moyenne, sans alignement.*

**Classe d'armure** 11

**Points de vie** 18 (4d8)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   13 (+1)   11 (+0)   2 (-4)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +3

**Sens** Vision dans le noir 30 ft., Perception passive 12

**Langues** -

**Défi** 1/4 (50 XP)

***Amphibie.*** La grenouille peut respirer de l'air et de l'eau.

***Saut debout.*** Le saut en longueur de la grenouille peut atteindre 6
mètres et son saut en hauteur peut atteindre 3 mètres, avec ou sans
départ lancé.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touche :* 4 (1d6 + 1) dégâts
perforants, et la cible est agrippée (évasion DC 11). Jusqu'à la fin de
cet agrippement, la cible est entravée, et la grenouille ne peut pas
mordre une autre cible.

***Avaler.*** La grenouille effectue une attaque de morsure contre une
cible de Petit ou plus petite taille qu'elle agrippe. Si l'attaque
touche, la cible est avalée, et le grappin prend fin. La cible avalée
est aveuglée et entravée, elle bénéficie d'un abri total contre les
attaques et autres effets extérieurs à la grenouille, et elle subit 5
(2d4) dégâts d'acide au début de chacun des tours de la grenouille. La
grenouille ne peut avoir qu'une seule cible avalée à la fois.

Si la grenouille meurt, la créature avalée n'est plus entravée par elle
et peut s'échapper du cadavre en utilisant 5 pieds de mouvement,
sortant ainsi en position couchée.




#### Chèvre géante

*Grande bête, sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 19 (3d10 + 3)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   11 (+0)   12 (+1)   3 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 11

**Langues** -

**Défi** 1/2 (100 XP)

***Charge.*** Si la chèvre se déplace d'au moins 6 mètres en ligne
droite vers une cible et la touche avec une attaque bélier au même tour,
la cible subit 5 (2d4) dégâts de matraquage supplémentaires. Si la cible
est une créature, elle doit réussir un jet de sauvegarde de Force DC 13
ou être mise à plat ventre.

***Pieds sûrs.*** La chèvre a un avantage aux jets de sauvegarde de
Force et de Dextérité effectués contre les effets qui la feraient tomber
à terre.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +5 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 8 (2d4 + 3) points de dégâts
contondants.




#### Hyène géante

*Grandes bêtes, sans alignement.*

**Classe d'armure** 12

**Points de vie** 45 (6d10 + 12)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   14 (+2)   14 (+2)   2 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 1 (200 XP)

***Rampage.*** Lorsque la hyène réduit une créature à 0 point de vie
avec une attaque de mêlée à son tour, elle peut bénéficier d'une action
bonus pour se déplacer jusqu'à la moitié de sa vitesse et effectuer une
attaque de morsure.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) points de
dégâts perforants.




#### Lézard géant

*Grande bête, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 19 (3d10 + 3)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   12 (+1)   13 (+1)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 10

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.

Un **lézard géant** peut être monté ou utilisé comme animal de trait.
Les Homme-lézards les gardent aussi comme animaux de compagnie, et les
lézards géants souterrains sont utilisés comme montures et animaux de
bât par les Drow, les Duergar et d'autres.




#### Pieuvre géante

*Grandes bêtes, sans alignement.*

**Classe d'armure** 11

**Points de vie** 52 (8d10 + 8)

**Vitesse** 10 ft., nager 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   13 (+1)   13 (+1)   4 (-3)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +4, Discrétion +5

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** -

**Défi** 1 (200 XP)

***Retenir sa respiration.*** Hors de l'eau, la pieuvre peut retenir sa
respiration pendant 1 heure.

***Camouflage sous-marin.*** La pieuvre a un avantage sur les tests de
Dextérité (Discrétion) effectués sous l'eau.

***Respiration aquatique.*** La pieuvre ne peut respirer que sous
l'eau.


##### Actions

***Tentacules.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 15 pieds, une cible. *Touché :* 10 (2d6 + 3) dégâts
contondants. Si la cible est une créature, elle est agrippée (fuite DC
16). Jusqu'à la fin de cet agrippement, la cible est entravée, et la
pieuvre ne peut pas utiliser ses tentacules sur une autre cible.

***Nuage d'encre (se recharge après un repos court ou long).*** Un
nuage d'encre de 20 pieds de rayon s'étend tout autour de la pieuvre
si elle est sous l'eau. La zone est fortement obscurcie pendant 1
minute, bien qu'un courant important puisse disperser l'encre. Après
avoir libéré l'encre, la pieuvre peut utiliser l'action bonus Foncer.




#### Chouette géante

*Grandes bêtes, neutres.*

**Classe d'armure** 12

**Points de vie** 19 (3d10 + 3)

**Vitesse** 5 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   15 (+2)   12 (+1)   8 (-1)    13 (+1)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +5, Discrétion +4

**Sens** Vision dans le noir 120 ft., Perception passive 15

**Langues** Chouette géante, comprend le commun, l'elfe et le sylvestre
mais ne peut pas les parler.

**Défi** 1/4 (50 XP)

***Vol.*** La chouette ne provoque pas d'attaque d'opportunité
lorsqu'elle vole hors de portée d'un ennemi.

***Ouïe et vue perçantes.*** La Chouette a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou la vue.


##### Actions

***Talons.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (2d6 + 1) points de
dégâts tranchants.

Les**chouettes géantes** se lient souvent d'amitié avec des fées et
d'autres créatures sylvestres et sont les gardiennes de leurs royaumes
forestiers.




#### Serpent venimeux géant

*Bête moyenne, sans alignement.*

**Classe d'armure** 14

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   18 (+4)   13 (+1)   2 (-4)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** vision aveugle 10 ft., Perception passive 12

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 10 pieds, une cible. *Touché :* 6 (1d4 + 4) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 11, subissant 10 (3d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite.




#### Rat géant

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 7 (2d6)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    15 (+2)   11 (+0)   2 (-4)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** -

**Défi** 1/8 (25 XP)

***Odorat aiguisé.*** Le rat a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Tactiques de meute.*** Le rat a un avantage sur un jet d'attaque
contre une créature si au moins un des alliés du rat se trouve à moins
de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts perforants.



##### Variante : Rats géants malades.

Certains rats géants sont porteurs de maladies infâmes qu'ils propagent
par leurs morsures. Un rat géant malade a un indice de difficulté de 1/8
(25 XP) et l'action suivante au lieu de son attaque de morsure normale.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Constitution DC 10 ou contracter une maladie. Jusqu'à ce
que la maladie soit guérie, la cible ne peut pas regagner de points de
vie, sauf par des moyens magiques, et le maximum de points de vie de la
cible diminue de 3 (1d6) toutes les 24 heures. Si le maximum de points
de vie de la cible tombe à 0 suite à cette maladie, la cible meurt.




#### Scorpion géant

*Grandes bêtes, sans alignement.*

**Classe d'armure** 15 (armure naturelle)

**Points de vie** 52 (7d10 + 14)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   13 (+1)   15 (+2)   1 (-5)    9 (-1)    3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 60 ft., Perception passive 9

**Langues** -

**Défi** 3 (700 XP)


##### Actions

***Attaques multiples.*** Le scorpion effectue trois attaques : deux
avec ses griffes et une avec son dard.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) dégâts
contondants, et la cible est agrippée (évasion DC 12). Le scorpion
possède deux griffes, chacune ne pouvant agripper qu'une seule cible.

***Piquer.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 7 (1d10 + 2) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 12, subissant 22 (4d10) dégâts de poison en cas
d'échec, ou la moitié des dégâts en cas de réussite.




#### Hippocampe géant

*Grande bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 16 (3d10)

**Vitesse** 0 ft., nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   15 (+2)   11 (+0)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 11

**Langues** -

**Défi** 1/2 (100 XP)

***Charge.*** Si l'hippocampe se déplace d'au moins 6 mètres en ligne
droite vers une cible et la touche avec une attaque bélier dans le même
tour, la cible subit 7 (2d6) points de dégâts de matraquage
supplémentaires. S'il s'agit d'une créature, elle doit réussir un jet
de sauvegarde de Force DC 11 ou être mise au sol.

***Respiration aquatique.*** L'Hippocampe ne peut respirer que sous
l'eau.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) points de dégâts
contondants.

Comme leurs cousins plus petits, les **Hippocampe géants** sont des
poissons timides et colorés au corps allongé et à la queue enroulée. Les
elfes aquatiques les dressent comme montures.




#### Requin géant

*Très grande bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 126 (11d12 + 55)

**Vitesse** 0 ft., nage 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   23 (+6)   11 (+0)   21 (+5)   1 (-5)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** vision aveugle 60 ft., Perception passive 13

**Langues** -

**Défi** 5 (1 800 XP)

***Frénésie sanguinaire.*** Le requin a l'avantage aux jets d'attaque
de mêlée contre toute créature qui n'a pas tous ses points de vie.

***Respiration aquatique.*** Le requin ne peut respirer que sous l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +9 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 22 (3d10 + 6) points de
dégâts perforants.

Un **requin géant** mesure 10 mètres de long et se trouve normalement
dans les océans profonds. Totalement intrépide, il s'attaque à tout ce
qui croise son chemin, y compris les baleines et les navires.




#### Araignée géante

*Grande bête, sans alignement.*

**Classe d'armure** 14 (armure naturelle)

**Points de vie** 26 (4d10 + 4)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   16 (+3)   12 (+1)   2 (-4)    11 (+0)   4 (-3)

  -----------------------------------------------------------

**Compétences** Discrétion +7

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 10

**Langues** -

**Défi** 1 (200 XP)

***Pattes d'araignée.*** L'araignée peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** Lorsqu'elle est en contact avec une toile,
l'araignée connaît l'emplacement exact de toute autre créature en
contact avec la même toile.

***Toile d'araignée.*** L'araignée ignore les restrictions de
mouvement causées par les toiles.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 7 (1d8 + 3) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 11, subissant 9 (2d8) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite. Si les dégâts de poison
réduisent la cible à 0 point de vie, elle est stable mais empoisonnée
pendant 1 heure, même après avoir regagné des points de vie, et est
paralysée lorsqu'elle est empoisonnée de cette façon.

***Toile (recharge 5-6).*** *Attaque avec une arme à distance :* +5 pour
toucher, portée de 30/60 pieds, une créature. Touché : La cible est
entravée par une toile. En tant qu'action, la cible entravée peut faire
un test de Force DC 12, faisant éclater la toile en cas de succès. La
sangle peut également être attaquée et détruite (CA 10 ; pv 5 ;
vulnérabilité aux dégâts du feu ; immunité aux coups de matraque, au
poison et aux dégâts psychiques).

Pour piéger sa proie, l'**araignée géante** tisse des toiles complexes
ou fait sortir de son abdomen des fils de toile collants. Les araignées
géantes se trouvent le plus souvent sous terre, faisant leur repaire sur
les plafonds ou dans des crevasses sombres remplies de toiles. Ces
repaires sont souvent festonnés de cocons de toiles contenant
d'anciennes victimes.




#### Crapaud géant

*Grande bête, sans alignement.*

**Classe d'armure** 11

**Points de vie** 39 (6d10 + 6)

**Vitesse** 20 ft., nager 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   13 (+1)   13 (+1)   2 (-4)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 10

**Langues** -

**Défi** 1 (200 XP)

***Amphibie.*** Le crapaud peut respirer de l'air et de l'eau.

***Saut debout.*** Le saut en longueur du crapaud peut atteindre 6
mètres et son saut en hauteur peut atteindre 3 mètres, avec ou sans
départ lancé.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touche :* 7 (1d10 + 2) dégâts
perforants plus 5 (1d10) dégâts de poison, et la cible est agrippée
(évasion DC 13). Jusqu'à la fin de cet agrippement, la cible est
entravée, et le crapaud ne peut pas mordre une autre cible.

***Avaler.*** Le crapaud effectue une attaque de morsure contre une
cible de taille moyenne ou plus petite qu'il agrippe. Si l'attaque
touche, la cible est avalée, et le grappin prend fin. La cible avalée
est aveuglée et entravée, elle bénéficie d'un abri total contre les
attaques et autres effets extérieurs au crapaud, et elle subit 10 (3d6)
dégâts d'acide au début de chaque tour du crapaud. Le crapaud ne peut
faire avaler qu'une seule cible à la fois.

Si le crapaud meurt, la créature avalée n'est plus entravée par lui et
peut s'échapper du cadavre en utilisant 5 pieds de mouvement, sortant
ainsi couchée.




#### Vautour géant

*Grandes bêtes, mal neutre.*

**Classe d'armure** 10

**Points de vie** 22 (3d10 + 6)

**Vitesse** 10 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   15 (+2)   6 (-2)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

Les**Roturiers** comprennent le commun mais ne peuvent pas parler.

**Défi** 1 (200 XP)

***Vue et odorat aiguisés.*** Le Vautour a un avantage sur les tests de
Sagesse (Perception) qui reposent sur la vue ou l'odorat.

***Tactiques de meute.*** Le Vautour a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du Vautour se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Attaques multiples.*** Le Vautour effectue deux attaques : une avec
son bec et une avec ses serres.

***Bec.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) points de dégâts
perforants.

***Talons.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (2d6 + 2) points de
dégâts tranchants.

Le **Vautour géant** est doté d'une intelligence avancée et d'un
penchant malveillant. Contrairement à ses semblables plus petits, il
attaquera une créature blessée pour hâter sa fin. Les vautours géants
sont connus pour hanter une créature assoiffée et affamée pendant des
jours pour profiter de sa souffrance.




#### Guêpe géante

*Bête moyenne, sans alignement.*

**Classe d'armure** 12

**Points de vie** 13 (3d8)

**Vitesse** 10 ft., Vol 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   14 (+2)   10 (+0)   1 (-5)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/2 (100 XP)


##### Actions

***Piquer.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 5 (1d6 + 2) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 11, subissant 10 (3d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite. Si les dégâts de poison
réduisent la cible à 0 point de vie, elle est stable mais empoisonnée
pendant 1 heure, même après avoir regagné des points de vie, et est
paralysée lorsqu'elle est empoisonnée de cette façon.




#### Belette géante

*Bête moyenne, sans alignement.*

**Classe d'armure** 13

**Points de vie** 9 (2d8)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   16 (+3)   10 (+0)   4 (-3)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +5

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues** -

**Défi** 1/8 (25 XP)

***Ouïe et odorat aiguisés.*** La belette a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d4 + 3) points de
dégâts perforants.




#### Araignée-loup géante

*Bête moyenne, sans alignement.*

**Classe d'armure** 13

**Points de vie** 11 (2d8 + 2)

**Vitesse** 40 ft., ascension 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   16 (+3)   13 (+1)   3 (-4)    12 (+1)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +7

**Sens** vision aveugle 10 ft., vision dans le noir 60 ft., Perception
passive 13

**Langues** -

**Défi** 1/4 (50 XP)

***Pattes d'araignée.*** L'araignée peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** Lorsqu'elle est en contact avec une toile,
l'araignée connaît l'emplacement exact de toute autre créature en
contact avec la même toile.

***Toile d'araignée.*** L'araignée ignore les restrictions de
mouvement causées par les toiles.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 4 (1d6 + 1) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 11, subissant 7 (2d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite. Si les dégâts de poison
réduisent la cible à 0 point de vie, elle est stable mais empoisonnée
pendant 1 heure, même après avoir regagné des points de vie, et est
paralysée lorsqu'elle est empoisonnée de cette façon.

Plus petite qu'une araignée géante, l'**araignée-loup géante** chasse
ses proies sur un terrain ouvert ou se cache dans un terrier ou une
crevasse, ou encore dans une cavité cachée sous des débris.




#### Chèvre

*Bête moyenne, sans alignement.*

**Classe d'armure** 10

**Points de vie** 4 (1d8)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   10 (+0)   11 (+0)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 0 (10 XP)

***Charge.*** Si la chèvre se déplace d'au moins 6 mètres en ligne
droite vers une cible puis la touche avec une attaque bélier au même
tour, la cible subit 2 (1d4) dégâts de matraquage supplémentaires. Si la
cible est une créature, elle doit réussir un jet de sauvegarde de Force
DC 10 ou être mise au sol.

***Pieds sûrs.*** La chèvre a un avantage aux jets de sauvegarde de
Force et de Dextérité effectués contre les effets qui la feraient tomber
à terre.


##### Actions

***Ram.*** *Attaque avec une arme de corps-à-corps :* +3 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 3 (1d4 + 1) points de dégâts
contondants.




#### Faucon

*Très petite (TP), sans alignement.*

**Classe d'armure** 13

**Points de vie** 1 (1d4 - 1)

**Vitesse** 10 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    16 (+3)   8 (-1)    2 (-4)    14 (+2)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** passif Perception 14

**Langues** -

**Défi** 0 (10 XP)

***Vue perçante.*** Le faucon a un avantage sur les tests de Sagesse
(Perception) qui reposent sur la vue.


##### Actions

***Talons.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât tranchant.




#### Requin-chasseur

*Grande bête, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 45 (6d10 + 12)

**Vitesse** 0 ft., nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   13 (+1)   15 (+2)   1 (-5)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** vision aveugle 30 ft., Perception passive 12

**Langues** -

**Défi** 2 (450 XP)

***Frénésie sanguinaire.*** Le requin a l'avantage aux jets d'attaque
de mêlée contre toute créature qui n'a pas tous ses points de vie.

***Respiration aquatique.*** Le requin ne peut respirer que sous l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 13 (2d8 + 4) points de
dégâts perforants.

Plus petit qu'un requin géant mais plus grand et plus féroce qu'un
requin de récif, le **requin-chasseur** hante les eaux profondes. Il
chasse généralement seul, mais plusieurs requins chasseurs peuvent se
nourrir dans la même zone. Un requin-chasseur adulte mesure de 15 à 20
pieds de long.




#### Hyène

*Bête moyenne, sans alignement.*

**Classe d'armure** 11

**Points de vie** 5 (1d8 + 1)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   13 (+1)   12 (+1)   2 (-4)    12 (+1)   5 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Tactiques de meute.*** La hyène a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés de la hyène se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d6) points de
dégâts perforants.




#### Chacal

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 3 (1d6)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    15 (+2)   11 (+0)   3 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Ouïe et odorat aiguisés.*** Le Chacal a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Tactiques de meute.*** Le Chacal a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du Chacal se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +1 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 (1d4 - 1) dégâts
perforants.




#### Épaulard

*Très grande bête, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 90 (12d12 + 12)

**Vitesse** 0 ft., nager 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   19 (+4)   10 (+0)   13 (+1)   3 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** vision aveugle 120 ft., Perception passive 13

**Langues** -

**Défi** 3 (700 XP)

***Echolocation.*** La baleine ne peut pas utiliser sa vision aveugle
lorsqu'elle est assourdie.

***Retenir sa respiration.*** La baleine peut retenir sa respiration
pendant 30 minutes.

***Ouïe fine.*** La baleine a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'audition.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 21 (5d6 + 4) points de
dégâts perforants.




#### Lion

*Grande bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 26 (4d10 + 4)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   13 (+1)   3 (-4)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +6

**Sens** passif Perception 13

**Langues** -

**Défi** 1 (200 XP)

***Odorat aiguisé.*** Le lion a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Tactiques de meute.*** Le lion a un avantage sur un jet d'attaque
contre une créature si au moins un des alliés du lion se trouve à moins
de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.

***Bondir.*** Si le lion se déplace d'au moins 6 mètres en ligne droite
vers une créature et la touche avec une attaque de griffes dans le même
tour, cette cible doit réussir un jet de sauvegarde de Force DC 13 ou
être mise à terre. Si la cible est couchée, le lion peut effectuer une
attaque de morsure contre elle en tant qu'action bonus.

***Saut en courant.*** Avec un départ en courant de 3 mètres, le lion
peut faire un saut en longueur de 7 mètres.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts tranchants.




#### Lézard

*Très petite (TP), sans alignement.*

**Classe d'armure** 10

**Points de vie** 2 (1d4)

**Vitesse** 20 ft., montée 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    11 (+0)   10 (+0)   1 (-5)    8 (-1)    3 (-4)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 9

**Langues** -

**Défi** 0 (10 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +0 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.




#### Mammouth

*Très grande bête, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 126 (11d12 + 55)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   24 (+7)   9 (-1)    21 (+5)   3 (-4)    11 (+0)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 6 (2 300 XP)

***Charge piétinante.*** Si le Mammouth se déplace d'au moins 6 mètres
en direction d'une créature et la touche avec une attaque gore dans le
même tour, la cible doit réussir un jet de sauvegarde de Force DC 18 ou
être mise à plat ventre.

Si la cible est à terre, le Mammouth peut effectuer une attaque de
piétinement contre elle en tant qu'action bonus.


##### Actions

***Gore.*** *Attaque avec une arme de corps-à-corps :* +10 pour toucher,
allonge de 10 pieds, une cible. *Touché :* 25 (4d8 + 7) points de dégâts
perforants.

***Stomp.*** *Attaque avec une arme de corps-à-corps :* +10 pour
toucher, portée de 1,5 m, une créature couchée. *Touché :* 29 (4d10 + 7)
points de dégâts contondants.

Un **mammouth** est une créature éléphantesque à la fourrure épaisse et
aux longues défenses. Plus gros et plus féroces que les éléphants
normaux, les mammouths vivent sous des climats très variés, du
subarctique au subtropical.




#### Molosse

*Bête moyenne, sans alignement.*

**Classe d'armure** 12

**Points de vie** 5 (1d8 + 1)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   14 (+2)   12 (+1)   3 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 1/8 (25 XP)

***Ouïe et odorat aiguisés.*** Le molosse a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 11 ou être mise au sol.

Les molosses sont d'impressionnants chiens de chasse prisés par les
humanoïdes pour leur loyauté et leurs sens aiguisés. Les Molosses
peuvent être dressés comme chiens de garde, de chasse et de guerre. Les
halflings et autres petits humanoïdes les utilisent comme monture.




#### Mule

*Bête moyenne, sans alignement.*

**Classe d'armure** 10

**Points de vie** 11 (2d8 + 2)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   10 (+0)   13 (+1)   2 (-4)    10 (+0)   5 (-3)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/8 (25 XP)

***Bête de somme.*** La mule est considérée comme un Grand animal pour
la détermination de sa capacité de charge.

***Pieds sûrs.*** La mule a un avantage aux jets de sauvegarde de Force
et de Dextérité effectués contre les effets qui la feraient tomber à
terre.


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts contondants.




#### Pieuvre

*Petite bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 3 (1d6)

**Vitesse** 5 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   4 (-3)    15 (+2)   11 (+0)   3 (-4)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +2, Discrétion +4

**Sens** Vision dans le noir 30 ft., Perception passive 12

**Langues** -

**Défi** 0 (10 XP)

***Retenir sa respiration.*** Hors de l'eau, la pieuvre peut retenir sa
respiration pendant 30 minutes.

***Camouflage sous-marin.*** La pieuvre a un avantage sur les tests de
Dextérité (Discrétion) effectués sous l'eau.

***Respiration aquatique.*** La pieuvre ne peut respirer que sous
l'eau.


##### Actions

***Tentacules.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touche :* 1 dégât contondant, et
la cible est agrippée (évasion DC 10). Jusqu'à la fin de cet
agrippement, la pieuvre ne peut pas utiliser ses tentacules sur une
autre cible.

***Nuage d'encre (se recharge après un repos court ou long).*** Un
nuage d'encre d'un rayon de 1,5 m s'étend tout autour de la pieuvre
si elle est sous l'eau. La zone est fortement obscurcie pendant 1
minute, bien qu'un courant important puisse disperser l'encre. Après
avoir libéré l'encre, la pieuvre peut utiliser l'action Foncer comme
action bonus.




#### Chouette

*Très petite (TP), sans alignement.*

**Classe d'armure** 11

**Points de vie** 1 (1d4 - 1)

**Vitesse** 5 ft., Vol 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    13 (+1)   8 (-1)    2 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +3

**Sens** Vision dans le noir 120 ft., Perception passive 13

**Langues** -

**Défi** 0 (10 XP)

***Vol.*** La chouette ne provoque pas d'attaque d'opportunité
lorsqu'elle vole hors de portée d'un ennemi.

***Ouïe et vue perçantes.*** La Chouette a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou la vue.


##### Actions

***Talons.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât tranchant.




#### Panthère

*Bête moyenne, sans alignement.*

**Classe d'armure** 12

**Points de vie** 13 (3d8)

**Vitesse** 50 ft., ascension 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   15 (+2)   10 (+0)   3 (-4)    14 (+2)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +4, Discrétion +6

**Sens** passif Perception 14

**Langues** -

**Défi** 1/4 (50 XP)

***Odorat aiguisé.*** La panthère a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Bondir.*** Si la panthère se déplace d'au moins 6 mètres en ligne
droite vers une créature et la touche avec une attaque de griffes au
même tour, cette cible doit réussir un jet de sauvegarde de Force DC 12
ou être mise à terre. Si la cible est couchée, la panthère peut
effectuer une attaque de morsure contre elle en tant qu'action bonus.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d4 + 2) points de
dégâts tranchants.




#### Araignée de phase

*Grande monstruosité, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 32 (5d10 + 5)

**Vitesse** 30 ft., escalade 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   15 (+2)   12 (+1)   6 (-2)    10 (+0)   6 (-2)

  -----------------------------------------------------------

**Compétences** Discrétion +6

**Sens** Vision dans le noir 60 ft., Perception passive 10

**Langues** -

**Défi** 3 (700 XP)

***Jaunt éthérée.*** Comme action bonus, l'araignée peut passer
magiquement du plan matériel au plan éthéré, ou vice versa.

***Pattes d'araignée.*** L'araignée peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** L'araignée ignore les restrictions de
mouvement causées par les toiles.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 7 (1d10 + 2) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 11, subissant 18 (4d8) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite. Si les dégâts de poison
réduisent la cible à 0 point de vie, elle est stable mais empoisonnée
pendant 1 heure, même après avoir regagné des points de vie, et est
paralysée lorsqu'elle est empoisonnée de cette façon.

Une **araignée de phase** possède la capacité magique d'entrer et de
sortir du plan éthéré. Elle semble surgir de nulle part et disparaît
rapidement après avoir attaqué. Son mouvement sur le plan éthéré avant
de revenir sur le plan matériel donne l'impression qu'elle peut se
téléporter.




#### Serpent venimeux

*Très petite (TP), sans alignement.*

**Classe d'armure** 13

**Points de vie** 2 (1d4)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    16 (+3)   11 (+0)   1 (-5)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 10 ft., Perception passive 10

**Langues** -

**Défi** 1/8 (25 XP)


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant, et
la cible doit effectuer un jet de sauvegarde de Constitution DC 10,
subissant 5 (2d4) dégâts de poison en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Ours polaire

*Grandes bêtes, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 42 (5d10 + 15)

**Vitesse** 40 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   20 (+5)   10 (+0)   16 (+3)   2 (-4)    13 (+1)   7 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 2 (450 XP)

***Odorat aiguisé.*** L'ours a un avantage sur les tests de Sagesse
(Perception) qui font appel à l'odorat.


##### Actions

***Attaques multiples.*** L'ours effectue deux attaques : une avec sa
morsure et une avec ses griffes.

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d8 + 5) points de
dégâts perforants.

***Griffes.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) points de
dégâts tranchants.




#### Poney

*Bête moyenne, sans alignement.*

**Classe d'armure** 10

**Points de vie** 11 (2d8 + 2)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   10 (+0)   13 (+1)   2 (-4)    11 (+0)   7 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/8 (25 XP)


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) dégâts
contondants.




#### Piranha

*Très petite (TP), sans alignement.*

**Classe d'armure** 13

**Points de vie** 1 (1d4 - 1)

**Vitesse** 0 ft., nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    16 (+3)   9 (-1)    1 (-5)    7 (-2)    2 (-4)

  -----------------------------------------------------------

**Sens** Vision dans le noir 60 ft., Perception passive 8

**Langues** -

**Défi** 0 (10 XP)

***Frénésie sanguinaire.*** Le Piranha a l'avantage sur les jets
d'attaque de mêlée contre toute créature qui n'a pas tous ses points
de vie.

***Respiration aquatique.*** Le Piranha ne peut respirer que sous
l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.

Le **Piranha** est un poisson carnivore aux dents acérées. Les Piranhas
peuvent s'adapter à n'importe quel environnement aquatique, y compris
les lacs souterrains froids. Ils se rassemblent souvent en essaims ;
pour des statistiques, voir \"Nuéede piranhas\".




#### Rat

*Très petite (TP), sans alignement.*

**Classe d'armure** 10

**Points de vie** 1 (1d4 - 1)

**Vitesse** 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    11 (+0)   9 (-1)    2 (-4)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Sens** Vision dans le noir 30 ft., Perception passive 10

**Langues** -

**Défi** 0 (10 XP)

***Odorat aiguisé.*** Le rat a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +0 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.




#### Corbeau

*Très petite (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 1 (1d4 - 1)

**Vitesse** 10 ft., Vol 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    14 (+2)   8 (-1)    2 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Mimétisme.*** Le corbeau peut imiter des sons simples qu'il a
entendus, comme le murmure d'une personne, les pleurs d'un bébé ou le
gazouillis d'un animal. Une créature qui entend les sons peut dire
qu'il s'agit d'imitations en réussissant un test de Sagesse
(Intuition) DC 10.


##### Actions

***Bec.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.




#### Requin de récifs

*Bête moyenne, sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 22 (4d8 + 4)

**Vitesse** 0 ft., nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   14 (+2)   13 (+1)   13 (+1)   1 (-5)    10 (+0)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** vision aveugle 30 ft., Perception passive 12

**Langues** -

**Défi** 1/2 (100 XP)

***Tactiques de meute.*** Le requin a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du requin se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.

***Respiration aquatique.*** Le requin ne peut respirer que sous l'eau.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.

Plus petits que les requins géants et les requins chasseurs, les
**requins de récifs** habitent les eaux peu profondes et les récifs
coralliens, se rassemblant en petites bandes pour chasser. Un spécimen
adulte mesure entre 2 et 3 mètres de long.




#### Rhinocéros

*Grandes bêtes, sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 45 (6d10 + 12)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   21 (+5)   8 (-1)    15 (+2)   2 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 11

**Langues** -

**Défi** 2 (450 XP)

***Charge.*** Si le Rhinocéros se déplace d'au moins 6 mètres en ligne
droite vers une cible et la touche avec une attaque gore dans le même
tour, la cible subit 9 (2d8) dégâts de matraquage supplémentaires. Si la
cible est une créature, elle doit réussir un jet de sauvegarde de Force
DC 15 ou être mise au sol.


##### Actions

***Gore.*** *Attaque avec une arme de corps-à-corps :* +7 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 14 (2d8 + 5) points de dégâts
contondants.




#### Cheval de selle

*Grande bête, sans alignement.*

**Classe d'armure** 10

**Points de vie** 13 (2d10 + 2)

**Vitesse** 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   10 (+0)   12 (+1)   2 (-4)    11 (+0)   7 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 1/4 (50 XP)


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (2d4 + 3) points de
dégâts contondants.




#### Tigre à dents de sabe

*Grande bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 52 (7d10 + 14)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   14 (+2)   15 (+2)   3 (-4)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +6

**Sens** passif Perception 13

**Langues** -

**Défi** 2 (450 XP)

***Odorat aiguisé.*** Le tigre a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Bondir.*** Si le tigre se déplace d'au moins 6 mètres en ligne
droite vers une créature et la touche avec une attaque de griffes dans
le même tour, cette cible doit réussir un jet de sauvegarde de Force DC
14 ou être mise à terre. Si la cible est couchée, le tigre peut
effectuer une attaque de morsure contre elle en tant qu'action bonus.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (1d10 + 5) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 12 (2d6 + 5) points de
dégâts tranchants.




#### Scorpion

*Très petite (TP), sans alignement.*

**Classe d'armure** 11 (armure naturelle)

**Points de vie** 1 (1d4 - 1)

**Vitesse** 10 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    11 (+0)   8 (-1)    1 (-5)    8 (-1)    2 (-4)

  -----------------------------------------------------------

**Sens** vision aveugle 10 ft., Perception passive 9

**Langues** -

**Défi** 0 (10 XP)


##### Actions

***Piquer.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 1 dégât perforant,
et la cible doit effectuer un jet de sauvegarde de Constitution DC 9,
subissant 4 (1d8) dégâts de poison en cas d'échec, ou la moitié des
dégâts en cas de réussite.




#### Hippocampe

*Très petite (TP), sans alignement.*

**Classe d'armure** 11

**Points de vie** 1 (1d4 - 1)

**Vitesse** 0 ft., nager 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   1 (-5)    12 (+1)   8 (-1)    1 (-5)    10 (+0)   2 (-4)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** -

**Défi** 0 (0 XP)

***Respiration aquatique.*** L'Hippocampe ne peut respirer que sous
l'eau.



#### Araignée

*Très petite (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 1 (1d4 - 1)

**Vitesse** 20 ft., montée 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   2 (-4)    14 (+2)   8 (-1)    1 (-5)    10 (+0)   2 (-4)

  -----------------------------------------------------------

**Compétences** Discrétion +4

**Sens** Vision dans le noir 30 ft., Perception passive 10

**Langues** -

**Défi** 0 (10 XP)

***Pattes d'araignée.*** L'araignée peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** Lorsqu'elle est en contact avec une toile,
l'araignée connaît l'emplacement exact de toute autre créature en
contact avec la même toile.

***Toile d'araignée.*** L'araignée ignore les restrictions de
mouvement causées par les toiles.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 1 dégât perforant,
et la cible doit réussir un jet de sauvegarde de Constitution DC 9 ou
subir 2 (1d4) dégâts de poison.




#### Nuée de chauves-souris

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 22 (5d8)

**Vitesse** 0 ft., Vol 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   5 (-3)    15 (+2)   10 (+0)   2 (-4)    12 (+1)   4 (-3)

  -----------------------------------------------------------

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** vision aveugle 60 ft., Perception passive 11

**Langues** -

**Défi** 1/4 (50 XP)

***Echolocation.*** L'essaim ne peut pas utiliser sa vision aveugle
lorsqu'il est assourdi.

***Ouïe fine.*** L'essaim a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'audition.

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice-versa, et l'essaim peut se déplacer dans n'importe quelle
ouverture assez grande pour une Très petite (TP) chauve-souris.
L'essaim ne peut pas regagner des points de vie ou en gagner
temporairement.


##### Actions

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 0 ft, une créature dans l'espace de l'essaim.
*Touché :* 5 (2d4) dégâts perforants, ou 2 (1d4) dégâts perforants si
l'essaim a la moitié de ses points de vie ou moins.




#### Nuée d'insectes

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 12 (armure naturelle)

**Points de vie** 22 (5d8)

**Vitesse** 20 ft., montée 20 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    13 (+1)   10 (+0)   1 (-5)    7 (-2)    1 (-5)

  -----------------------------------------------------------

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** vision aveugle 10 ft., Perception passive 8

**Langues** -

**Défi** 1/2 (100 XP)

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice versa, et l'essaim peut se déplacer à travers n'importe quelle
ouverture assez grande pour un Très petite (TP) insecte. L'essaim ne
peut pas regagner des points de vie ou en gagner temporairement.


##### Actions

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 0 ft, une cible dans l'espace de l'essaim. *Touché
:* 10 (4d4) dégâts perforants, ou 5 (2d4) dégâts perforants si l'essaim
a la moitié de ses points de vie ou moins.




#### Variante : Nuée d'insectes

Différentes sortes d'insectes peuvent se rassembler en essaims, et
chaque essaim possède les caractéristiques particulières décrites
ci-dessous.


##### Essaim de coléoptères

Un essaim de scarabées gagne une vitesse de creusement de 5 pieds.



##### Essaim de mille-pattes

Une créature réduite à 0 point de vie par un essaim de mille-pattes est
stable mais empoisonnée pendant 1 heure, même après avoir regagné des
points de vie, et paralysée lorsqu'elle est empoisonnée de cette façon.



##### Essaim d'araignées

Un essaim d'araignées possède les caractéristiques supplémentaires
suivantes.

***Pattes d'araignée.*** L'essaim peut grimper sur des surfaces
difficiles, y compris la tête en bas sur les plafonds, sans avoir besoin
de faire un test d'aptitude.

***Toile d'araignée.*** Lorsqu'il est en contact avec une toile,
l'essaim connaît l'emplacement exact de toute autre créature en
contact avec la même toile.

***Toile d'araignée.*** L'essaim ignore les restrictions de mouvement
causées par les toiles.



##### Essaim de guêpes

Un essaim de guêpes a une vitesse de marche de 5 pieds, une vitesse de
vol de 30 pieds, et aucune vitesse d'escalade.




#### Nuée de serpents venimeux

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 14

**Points de vie** 36 (8d8)

**Vitesse** 30 ft., nager 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   8 (-1)    18 (+4)   11 (+0)   1 (-5)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** vision aveugle 10 ft., Perception passive 10

**Langues** -

**Défi** 2 (450 XP)

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice versa, et l'essaim peut se déplacer à travers n'importe quelle
ouverture assez grande pour un Très petite serpent. L'essaim ne peut
pas regagner des points de vie ou en gagner temporairement.


##### Actions

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 0 ft, une créature dans l'espace de l'essaim.
*Touché :* 7 (2d6) dégâts perforants, ou 3 (1d6) dégâts perforants si
l'essaim a la moitié de ses points de vie ou moins. La cible doit
effectuer un jet de sauvegarde de Constitution DC 10, subissant 14 (4d6)
dégâts de poison en cas d'échec, ou la moitié des dégâts en cas de
réussite.




#### Nuée de piranhas

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 13

**Points de vie** 28 (8d8 - 8)

**Vitesse** 0 ft., nage 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   16 (+3)   9 (-1)    1 (-5)    7 (-2)    2 (-4)

  -----------------------------------------------------------

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** Vision dans le noir 60 ft., Perception passive 8

**Langues** -

**Défi** 1 (200 XP)

***Frénésie sanguinaire.*** L'essaim a l'avantage aux jets d'attaque
de mêlée contre toute créature qui n'a pas tous ses points de vie.

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice versa, et l'essaim peut se déplacer à travers n'importe quelle
ouverture assez grande pour un Très petite (TP) Piranha. L'essaim ne
peut pas regagner des points de vie ou en gagner temporairement.

***Respiration aquatique.*** L'essaim ne peut respirer que sous l'eau.


##### Actions

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 0 ft, une créature dans l'espace de l'essaim.
*Touché :* 14 (4d6) dégâts perforants, ou 7 (2d6) dégâts perforants si
l'essaim a la moitié de ses points de vie ou moins.




#### Nuée de rats

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 10

**Points de vie** 24 (7d8 - 7)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   9 (-1)    11 (+0)   9 (-1)    2 (-4)    10 (+0)   3 (-4)

  -----------------------------------------------------------

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** Vision dans le noir 30 ft., Perception passive 10

**Langues** -

**Défi** 1/4 (50 XP)

***Odorat aiguisé.*** L'essaim a un avantage sur les tests de Sagesse
(Perception) qui font appel à l'odorat.

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice versa, et l'essaim peut se déplacer dans n'importe quelle
ouverture assez grande pour un Très petite rat. L'essaim ne peut pas
regagner des points de vie ou en gagner temporairement.


##### Actions

***Morsures.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 0 ft, une cible dans l'espace de l'essaim. *Touché
:* 7 (2d6) dégâts perforants, ou 3 (1d6) dégâts perforants si l'essaim
a la moitié de ses points de vie ou moins.




#### Nuée de corbeaux

*Essaim moyen de Très petites (TP), sans alignement.*

**Classe d'armure** 12

**Points de vie** 24 (7d8 - 7)

**Vitesse** 10 ft., Vol 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   6 (-2)    14 (+2)   8 (-1)    3 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +5

**Résistances aux dégâts**: contondant, perforant, tranchant.

**Condition Immunités** charmé, effrayé, agrippé, paralysé, pétrifié,
plaqué, entravé, étourdi

**Sens** passif Perception 15

**Langues** -

**Défi** 1/4 (50 XP)

***Essaim.*** L'essaim peut occuper l'espace d'une autre créature et
vice-versa, et l'essaim peut se déplacer par n'importe quelle
ouverture assez grande pour un Très petite corbeau. L'essaim ne peut
pas regagner des points de vie ou en gagner temporairement.


##### Actions

***Becs.*** *Attaque avec une arme de corps-à-corps :* +4 pour toucher,
allonge de 1,5 m, une cible dans l'espace de l'essaim. *Touché :* 7
(2d6) dégâts perforants, ou 3 (1d6) dégâts perforants si l'essaim a la
moitié de ses points de vie ou moins.




#### Tigre

*Grande bête, sans alignement.*

**Classe d'armure** 12

**Points de vie** 37 (5d10 + 10)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   17 (+3)   15 (+2)   14 (+2)   3 (-4)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +6

**Sens** Vision dans le noir 60 ft., Perception passive 13

**Langues** -

**Défi** 1 (200 XP)

***Odorat aiguisé.*** Le tigre a un avantage sur les tests de Sagesse
(Perception) qui reposent sur l'odorat.

***Bondir.*** Si le tigre se déplace d'au moins 6 mètres en ligne
droite vers une créature et la touche avec une attaque de griffes dans
le même tour, cette cible doit réussir un jet de sauvegarde de Force DC
13 ou être mise à terre. Si la cible est couchée, le tigre peut
effectuer une attaque de morsure contre elle en tant qu'action bonus.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 8 (1d10 + 3) points de
dégâts perforants.

***Griffe.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts tranchants.




#### Vautour

*Bête moyenne, sans alignement.*

**Classe d'armure** 10

**Points de vie** 5 (1d8 + 1)

**Vitesse** 10 ft., Vol 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   7 (-2)    10 (+0)   13 (+1)   2 (-4)    12 (+1)   4 (-3)

  -----------------------------------------------------------

**Compétences** Perception +3

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Vue et odorat aiguisés.*** Le Vautour a un avantage sur les tests de
Sagesse (Perception) qui reposent sur la vue ou l'odorat.

***Tactiques de meute.*** Le Vautour a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du Vautour se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Bec.*** *Attaque avec une arme de corps-à-corps :* +2 pour toucher,
allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts perforants.




#### Cheval de guerre

*Grande bête, sans alignement.*

**Classe d'armure** 11

**Points de vie** 19 (3d10 + 3)

**Vitesse** 60 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   12 (+1)   13 (+1)   2 (-4)    12 (+1)   7 (-2)

  -----------------------------------------------------------

**Sens** passif Perception 11

**Langues** -

**Défi** 1/2 (100 XP)

***Charge de piétinement.*** Si le cheval se déplace d'au moins 6
mètres en ligne droite vers une créature et la touche avec une attaque
de sabots dans le même tour, cette cible doit réussir un jet de
sauvegarde de Force DC 14 ou être mise à terre. Si la cible est couchée,
le cheval peut effectuer une autre attaque avec ses sabots contre elle
comme action bonus.


##### Actions

***Sabots.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) points de
dégâts contondants.




#### Belette

*Très petite (TP), sans alignement.*

**Classe d'armure** 13

**Points de vie** 1 (1d4 - 1)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   3 (-4)    16 (+3)   8 (-1)    2 (-4)    12 (+1)   3 (-4)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +5

**Sens** passif Perception 13

**Langues** -

**Défi** 0 (10 XP)

***Ouïe et odorat aiguisés.*** La belette a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 1 dégât perforant.




#### Loup arctique

*Grande monstruosité, mal neutre.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 75 (10d10 + 20)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   13 (+1)   14 (+2)   7 (-2)    12 (+1)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +5, Discrétion +3

**Immunité aux dommages causés par** le froid

**Sens** passif Perception 15

**Langues** Commun, Géant, Loup arctique

**Défi** 3 (700 XP)

***Ouïe et odorat aiguisés.*** Le loup a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Tactiques de meute.*** Le loup a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du loup se trouve à moins
de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.

***Camouflage dans la neige.*** Le loup a un avantage sur les tests de
Dextérité (Discrétion) effectués pour se cacher en terrain enneigé.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 11 (2d6 + 4) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 14 ou être mise au sol.

***Souffle froid (recharge 5-6).*** Le loup exhale un souffle de vent
glacial dans un cône de 15 pieds. Chaque créature dans cette zone doit
effectuer un jet de sauvegarde de Dextérité DC 12, subissant 18 (4d8)
dégâts de froid en cas d'échec, ou la moitié des dégâts en cas de
réussite.

Ce **loup** arctique d'**hiver** est aussi grand qu'un loup sinistre,
mais sa fourrure est blanche comme neige et ses yeux sont bleu pâle. Les
géants du givre utilisent ces créatures maléfiques comme gardes et
compagnons de chasse, utilisant le souffle mortel des loups contre leurs
ennemis. Les loups arctiques communiquent entre eux par des grognements
et des aboiements, mais ils parlent suffisamment bien le commun et le
géant pour suivre des conversations simples.




#### Loup

*Bête moyenne, sans alignement.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 40 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   12 (+1)   15 (+2)   12 (+1)   3 (-4)    12 (+1)   6 (-2)

  -----------------------------------------------------------

**Compétences** Perception +3, Discrétion +4

**Sens** passif Perception 13

**Langues** -

**Défi** 1/4 (50 XP)

***Ouïe et odorat aiguisés.*** Le loup a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.

***Tactiques de meute.*** Le loup a l'avantage sur les jets d'attaque
contre une créature si au moins un des alliés du loup se trouve à moins
de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (2d4 + 2) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 11 ou être mise au sol.




#### Worg

*Grande monstruosité, mal neutre.*

**Classe d'armure** 13 (armure naturelle)

**Points de vie** 26 (4d10 + 4)

**Vitesse** 50 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   13 (+1)   13 (+1)   7 (-2)    11 (+0)   8 (-1)

  -----------------------------------------------------------

**Compétences** Perception +4

**Sens** Vision dans le noir 60 ft., Perception passive 14

**Langues** Gobelin, Worg

**Défi** 1/2 (100 XP)

***Ouïe et odorat aiguisés.*** Le Worg a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou l'odorat.


##### Actions

***Mordre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3) dégâts
perforants. Si la cible est une créature, elle doit réussir un jet de
sauvegarde de Force DC 13 ou être mise au sol.

Un **worg** est un prédateur maléfique qui prend plaisir à chasser et à
dévorer les créatures plus faibles que lui. Rusés et malveillants, les
worgs errent dans les régions sauvages reculées ou sont élevés par des
gobelins et des hobgobelins. Ces créatures utilisent les worgs comme
montures, mais un worg se retournera contre son cavalier s'il se sent
maltraité ou mal nourri. Les worgs parlent leur propre langue et le
Gobelin, et quelques-uns apprennent aussi à parler le Commun.


## Personnages non-joueurs

Cette section contient les statistiques de divers personnages humanoïdes
non joueurs (PNJ) que les aventuriers peuvent rencontrer au cours d'une
campagne, y compris de modestes roturiers et de puissants archimages.

Ces blocs de statistiques peuvent être utilisés pour représenter des PNJ
humains et non humains.


### Personnalisation des PNJ

Il existe de nombreuses façons simples de personnaliser les PNJ de cette
section pour votre campagne personnelle.

***Traits raciaux.*** Vous pouvez ajouter des traits raciaux à un PNJ.
Par exemple, un druide halfelin peut avoir une vitesse de 25 pieds et le
trait Chanceux. L'ajout de traits raciaux à un PNJ ne modifie pas sa
valeur de défi. Pour en savoir plus sur les traits raciaux, voir
\"Origines\".

***Échange de sorts.*** Une façon de personnaliser un lanceur de sorts
PNJ est de remplacer un ou plusieurs de ses sorts. Vous pouvez remplacer
n'importe quel sort de la liste de sorts du PNJ par un autre sort de
même niveau de la même liste de sorts. Le fait d'échanger des sorts de
cette manière ne modifie pas le niveau de défi du PNJ.

***Échange d'armures et d'armes.*** Vous pouvez améliorer ou diminuer
l'armure d'un PNJ, ou ajouter ou changer d'arme. Les ajustements de
la classe d'armure et des dégâts peuvent modifier le niveau de défi
d'un PNJ.

***Objets magiques.*** Plus un PNJ est puissant, plus il est probable
qu'il ait un ou plusieurs objets magiques en sa possession. Un
Archimage, par exemple, peut avoir un bâton ou une baguette magique,
ainsi qu'une ou plusieurs potions et parchemins. Donner à un PNJ un
objet magique qui inflige de puissants dégâts peut modifier sa valeur de
défi.



### Descriptions des personnages non-joueurs


#### Acolyte

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 10

**Points de vie** 9 (2d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   10 (+0)   10 (+0)   10 (+0)   14 (+2)   11 (+0)

  -----------------------------------------------------------

**Compétences** Médecine +4, Religion +2

**Sens** passif Perception 12

Roturier toute**langue** (généralement commune)

**Défi** 1/4 (50 XP)

***Incantation.*** L'acolyte est un lanceur de sorts de 1er niveau. Sa
caractéristique d'incantation est la Sagesse (sauvegarde contre les
sorts DC 12, +4 pour toucher avec les attaques de sorts). L'acolyte a
les sorts de clercs suivants préparés :

Tours de magie (à volonté) : *lumière*, *flamme
sacrée*

1er niveau (3 emplacements) : *Bénédiction*,
*Soins*


##### Actions

***Gourdin.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts
contondants.

Les**acolytes** sont des membres subalternes du clergé, généralement
sous les ordres d'un prêtre. Ils remplissent diverses fonctions dans un
temple et reçoivent des pouvoirs d'incantation mineurs de la part de
leurs divinités.




#### Archimage

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 12 (15 avec une *armure de mage*

**Points de vie** 99 (18d8 + 18)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   14 (+2)   12 (+1)   20 (+5)   15 (+2)   16 (+3)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +9, Wis +6

**Compétences** Arcane +13, Histoire +13 Résistance aux dégâts des sorts
; matraquage, perforation et tranchage non magiques (à partir de la
*peau de pierre*.

**Sens** passif Perception 12

**Langues** six langues au choix

**Défi** 12 (8 400 XP)

***Résistance à la magie.*** L'Archimage a un avantage aux jets de
sauvegarde contre les sorts et autres effets magiques.

***Incantation.*** L'Archimage est un lanceur de sorts de 18ème niveau.
Sa caractéristique d'incantation est l'Intelligence (sauvegarde contre
les sorts DC 17, +9 pour toucher avec les attaques de sorts).
L'archimage peut lancer *Déguisement* et
*Invisibilité* à volonté et a les sorts de magicien
suivants préparés :

Tours de magie (à volonté) : *éclair de* feu,
*lumière*,
*prestidigitation*, *poigne de
choc*.

1er niveau (4 emplacements) : *Détection de la magie*,
*Identification*,\*
*Projectile magique*

2ème niveau (3 emplacements) : *Détection des
pensées*, Pas
*brumeux*

3e niveau (3 emplacements) : *Contresort*,
*Vol*.

4ème niveau (3 emplacements) : *Bannissement*, *Bouclier
de feu*

5ème niveau (3 emplacements) : *Cône de* froid,
*Scrutation* de force

6ème niveau (1 emplacement) : Globe
d'*invulnérabilité*

7ème niveau (1 emplacement) : *Téléportation*

8ème niveau (1 emplacement) : *Esprit impénétrable*

9ème niveau (1 slot) : *Arrêt du temps*

\* *L'Archimage lance ces sorts sur lui-même avant le combat.*


##### Actions

***Dague.*** *Attaque avec une arme de corps-à-corps ou à distance :* +6
pour toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible.
*Touché :* 4 (1d4 + 2) points de dégâts perforants.

Les**Archimages** sont des lanceurs de sorts puissants - et généralement
assez âgés - qui se consacrent à l'étude des arts des arcanes. Les
bienveillants conseillent les rois et les reines, tandis que les
malveillants règnent en tyrans et recherchent le lichdom. Ceux qui ne
sont ni bons ni mauvais s'enferment dans des tours isolées pour
pratiquer leur magie sans interruption. Un archimage a généralement un
ou plusieurs apprentis mages, et sa demeure est dotée de nombreuses
protections magiques et de gardiens pour décourager les intrus.




#### Assassinat

*Humanoïde moyen (n'importe quelle origine), tout alignement non bon*

**Classe d'armure** 15 (cuir clouté)

**Points de vie** 78 (12d8 + 24)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   16 (+3)   14 (+2)   13 (+1)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Dex +6, Int +4

**Compétences** Acrobaties +6, Discrétion +3, Perception +3, Furtivité
+9

**Résistance aux dégâts** poison

**Sens** passif Perception 13

**Langues** Jargon des voleurs plus deux langues au choix

**Défi** 8 (3 900 XP)

***Assassinat .*** Lors de son premier tour, l'assassin a un avantage
sur les jets d'attaque contre toute créature qui n'a pas fait de tour.
Tout coup porté par l'assassin contre une créature surprise est un coup
critique.

***Dérobade.*** Si l'assassin est soumis à un effet qui lui permet de
faire un jet de sauvegarde de Dextérité pour ne subir que la moitié des
dégâts, l'assassin ne subit à la place aucun dégât s'il réussit son
jet de sauvegarde, et seulement la moitié des dégâts s'il échoue.

***Attaque sournoise.*** Une fois par tour, l'assassin inflige 14 (4d6)
points de dégâts supplémentaires lorsqu'il touche une cible avec une
attaque d'arme et qu'il a l'avantage au jet d'attaque, ou lorsque la
cible se trouve à moins de 1,5 m d'un allié de l'assassin qui n'est
pas frappé d'incapacité et que l'assassin n'a pas de désavantage au
jet d'attaque.


##### Actions

***Attaques multiples.*** L'Assassinat effectue deux attaques à l'épée
courte.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +6 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) dégâts
perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 15, subissant 24 (7d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite.

***Arbalète légère.*** *Attaque avec une arme à distance :* +6 pour
toucher, portée de 80/320 pieds, une cible. *Touché :* 7 (1d8 + 3)
dégâts perforants, et la cible doit effectuer un jet de sauvegarde de
Constitution DC 15, subissant 24 (7d6) dégâts de poison en cas d'échec,
ou la moitié des dégâts en cas de réussite.

Formés à l'utilisation du poison, les **Assassins** sont des tueurs
sans remords qui travaillent pour les nobles, les maîtres de guilde, les
souverains et tous ceux qui peuvent se les payer.




#### Bandit

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement
non-juridique.*

**Classe d'armure** 12 (armure en cuir)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   12 (+1)   12 (+1)   10 (+0)   10 (+0)   10 (+0)

  -----------------------------------------------------------

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 1/8 (25 XP)


##### Actions

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 4 (1d6 + 1) points de
dégâts tranchants.

***Arbalète légère.*** *Attaque avec une arme à distance :* +3 pour
toucher, portée de 80 ft./320 ft., une cible. *Touché :* 5 (1d8 + 1)
points de dégâts perforants.

Les**bandits** errent en bandes et sont parfois dirigés par des
malfrats, des vétérans ou des lanceurs de sorts. Tous les bandits ne
sont pas mauvais. L'oppression, la sécheresse, la maladie ou la famine
peuvent souvent conduire des gens honnêtes à une vie de banditisme.

Les**pirates** sont des bandits de la haute mer. Il peut s'agir de
flibustiers ne s'intéressant qu'au trésor et au meurtre, ou de
corsaires autorisés par la Couronne à attaquer et à piller les navires
d'une nation ennemie.




#### Capitaine bandit

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement
non-juridique.*

**Classe d'armure** 15 (cuir clouté)

**Points de vie** 65 (10d8 + 20)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   16 (+3)   14 (+2)   14 (+2)   11 (+0)   14 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +4, Dex +5, Wis +2

**Compétences** Athlétisme +4, Supercherie +4

**Sens** passif Perception 10

**Langues** deux langues au choix

**Défi** 2 (450 XP)


##### Actions

***Attaques multiples.*** Le capitaine effectue trois attaques de mêlée
: deux avec son cimeterre et une avec sa dague. Ou le capitaine effectue
deux attaques à distance avec ses dagues.

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts tranchants.

***Dague.*** *Attaque avec une arme de corps-à-corps ou à distance :* +5
pour toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible.
*Touché :* 5 (1d4 + 3) points de dégâts perforants.



##### Réactions

***Parade.*** Le capitaine ajoute 2 à sa CA contre une attaque de mêlée
qui le toucherait. Pour cela, le capitaine doit voir l'attaquant et
manier une arme de mêlée.

Il faut une forte personnalité, une ruse impitoyable et une langue
d'argent pour tenir en respect une bande de bandits. Le **capitaine
bandit** possède ces qualités à la pelle.

En plus de gérer un équipage de mécontents égoïstes, le **capitaine
pirate** est une variante du capitaine bandit, avec un navire à protéger
et à commander. Pour garder l'équipage dans le droit chemin, le
capitaine doit régulièrement distribuer des récompenses et des
punitions.

Plus qu'un trésor, un capitaine bandit ou un capitaine pirate désire
l'infamie. Un prisonnier qui fait appel à la vanité ou à l'ego du
capitaine a plus de chances d'être traité équitablement qu'un
prisonnier qui ne connaît pas ou prétend ne pas connaître la réputation
colorée du capitaine.




#### Berserker

*Humanoïde moyen (toute origine), tout alignement chaotique*

**Classe d'armure** 13 (cacher l'armure)

**Points de vie** 67 (9d8 + 27)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   12 (+1)   17 (+3)   9 (-1)    11 (+0)   9 (-1)

  -----------------------------------------------------------

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 2 (450 XP)

***Téméraire.*** Au début de son tour, le berserker peut obtenir un
avantage sur tous les jets d'attaque avec une arme de mêlée pendant ce
tour, mais les jets d'attaque contre lui ont un avantage jusqu'au
début de son prochain tour.


##### Actions

***Hache à deux mains.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, allonge de 1,5 m, une cible. *Touché :* 9 (1d12 + 3)
points de dégâts tranchants.

Originaires de terres non civilisées, ces **berserkers** imprévisibles
se réunissent en groupes de guerre et cherchent le conflit partout où
ils peuvent le trouver.




#### Roturier

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 10

**Points de vie** 4 (1d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   10 (+0)   10 (+0)   10 (+0)   10 (+0)   10 (+0)

  -----------------------------------------------------------

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 0 (10 XP)


##### Actions

***Gourdin.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 2 (1d4) dégâts
contondants.

Les**roturiers** sont des paysans, des serfs, des esclaves, des
domestiques, des pèlerins, des marchands, des artisans et des ermites.




#### Fanatique du culte

*Humanoïde moyen (n'importe quelle origine), tout alignement non bon*

**Classe d'armure** 13 (armure en cuir)

**Points de vie** 33 (6d8 + 6)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   14 (+2)   12 (+1)   10 (+0)   13 (+1)   14 (+2)

  -----------------------------------------------------------

**Compétences** Supercherie +4, Persuasion +4, Religion +2

**Sens** passif Perception 11

Roturier toute**langue** (généralement commune)

**Défi** 2 (450 XP)

***Dévotion ténébreuse.*** Le fanatique a un avantage sur les jets de
sauvegarde contre le fait d'être charmé ou effrayé.

***Incantation.*** Le fanatique est un lanceur de sorts de 4e niveau. Sa
caractéristique d'incantation est Sagesse (sauvegarde contre les sorts
DC 11, +3 pour toucher avec les attaques de sorts). Le fanatique a les
sorts de clercs suivants préparés :

Tours de magie (à volonté) : *lumière*, *flamme
sacrée*

1er niveau (4 emplacements) : *commandement*, *infliger des
blessures*

2ème niveau (3 emplacements) : Immobilisation de
*personne*


##### Actions

***Attaques multiples.*** Le fanatique effectue deux attaques de mêlée.

***Dague.*** *Attaque avec une arme de corps-à-corps ou à distance :* +4
au toucher, allonge de 5 pieds ou portée de 20/60 pieds, une créature.
*Touché :* 4 (1d4 + 2) points de dégâts perforants.

Les**fanatiques** font souvent partie de la direction d'une secte,
utilisant leur charisme et leur dogme pour influencer et s'attaquer aux
personnes de faible volonté. La plupart sont intéressés par le pouvoir
personnel avant tout.




#### Cultiste

*Humanoïde moyen (n'importe quelle origine), tout alignement non bon*

**Classe d'armure** 12 (armure en cuir)

**Points de vie** 9 (2d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   12 (+1)   10 (+0)   10 (+0)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Supercherie +2, Religion +2

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 1/8 (25 XP)

***Dévotion ténébreuse.*** Le cultiste a un avantage aux jets de
sauvegarde contre le charme ou l'effroi.


##### Actions

***Cimeterre.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 4 (1d6 + 1) dégâts
tranchants.

**Les cultistes** prêtent allégeance à des puissances obscures telles
que les princes élémentaires, les seigneurs démoniaques ou les
archidémons. La plupart dissimulent leur loyauté pour éviter d'être
ostracisés, emprisonnés ou exécutés pour leurs croyances. Contrairement
aux acolytes du mal, les cultistes montrent souvent des signes de folie
dans leurs croyances et leurs pratiques.




#### Druide

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 11 (16 avec *Peau d'écorce*

**Points de vie** 27 (5d8 + 5)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   12 (+1)   13 (+1)   12 (+1)   15 (+2)   11 (+0)

  -----------------------------------------------------------

**Compétences** Médecine +4, Nature +3, Perception +4

**Sens** passif Perception 14

**Langues** Druidique et deux langues au choix.

**Défi** 2 (450 XP)

***Incantation.*** Le druide est un lanceur de sorts de 4e niveau. Sa
caractéristique d'incantation est Sagesse (sauvegarde contre les sorts
DC 12, +4 pour toucher avec les attaques de sorts). Il a les sorts de
druide suivants préparés :

Tours de magie (à volonté) : *assistance*, *produire des
flam*

1er niveau (4 emplacements) : *Enchevêtrement*, Grande
*foulée*, *Parler avec les
animaux*

2ème niveau (3 emplacements) : *Messager animal*,
*Peau d'écorce*


##### Actions

***Bâton.*** *Attaque avec une arme de corps-à-corps :* +2 pour toucher
(+4 pour toucher avec le *gourdin magique*, portée de
1,5 m, une cible. *Touché :* 3 (1d6) points de dégâts de contondance, 4
(1d8) points de dégâts de contondance si l'arme est maniée à deux
mains, ou 6 (1d8 + 2) points de dégâts de contondance avec le *gourdin
magique*.

Les**druides** vivent dans les forêts et autres lieux sauvages isolés,
où ils protègent la nature des monstres et de l'empiètement de la
civilisation. Certains sont des chamans tribaux qui guérissent les
malades, prient les esprits des animaux et fournissent une assistance
spirituelle.




#### Gladiateur

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 16 (cuir clouté, Bouclier)

**Points de vie** 112 (15d8 + 45)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   18 (+4)   15 (+2)   16 (+3)   10 (+0)   12 (+1)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Str +7, Dex +5, Con +6

**Compétences** Athlétisme +10, Intimidation +5

**Sens** passif Perception 11

**Langues** une seule langue (généralement le commun)

**Défi** 5 (1 800 XP)

***Brave.*** Le gladiateur a un avantage sur les jets de sauvegarde
contre l'effroi.

***Brute.*** Une arme de mêlée inflige un dé supplémentaire de ses
dégâts lorsque le gladiateur la touche (inclus dans l'attaque).


##### Actions

***Attaques multiples.*** Le gladiateur effectue trois attaques de mêlée
ou deux attaques à distance.

***Lance.*** *Attaque avec une arme de corps-à-corps ou à distance :* +7
pour toucher, allonge de 5 pieds et portée de 20/60 pieds, une cible.
*Touché :* 11 (2d6 + 4) points de dégâts perforants, ou 13 (2d8 + 4)
points de dégâts perforants si elle est utilisée à deux mains pour une
attaque de mêlée.

***Bouclier Bash.*** *Attaque avec une arme de corps-à-corps :* +7 pour
toucher, portée de 1,5 m, une créature. *Touché :* 9 (2d4 + 4) points de
dégâts contondants. Si la cible est une créature de taille moyenne ou
plus petite, elle doit réussir un jet de sauvegarde de Force DC 15 ou
être mise à plat ventre.



##### Réactions

***Parade.*** Le gladiateur ajoute 3 à sa CA contre une attaque de mêlée
qui le toucherait. Pour cela, le gladiateur doit voir l'attaquant et
manier une arme de mêlée.

Les**gladiateurs** se battent pour le divertissement de foules
déchaînées. Certains gladiateurs sont des combattants de fosse brutaux
qui considèrent chaque combat comme une question de vie ou de mort,
tandis que d'autres sont des duellistes professionnels qui perçoivent
des honoraires énormes mais se battent rarement à mort.




#### Garde

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 16 (chemise à mailles, bouclier)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   12 (+1)   12 (+1)   10 (+0)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Perception +2

**Sens** passif Perception 12

Roturier toute**langue** (généralement commune)

**Défi** 1/8 (25 XP)


##### Actions

***Lance.*** *Attaque avec une arme à distance ou de mêlée :* +3 pour
toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible. *Touché
:* 4 (1d6 + 1) points de dégâts perforants, ou 5 (1d8 + 1) points de
dégâts perforants si elle est utilisée à deux mains pour une attaque de
mêlée.

Les**gardes** comprennent les membres de la garde d'une ville, les
sentinelles d'une citadelle ou d'une ville fortifiée, et les gardes du
corps des marchands et des nobles.




#### Chevalier

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 18 (harnois)

**Points de vie** 52 (8d8 + 16)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   11 (+0)   14 (+2)   11 (+0)   11 (+0)   15 (+2)

  -----------------------------------------------------------

**Jets de sauvegarde** Con +4, Wis +2

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 3 (700 XP)

***Brave.*** Le Chevalier a un avantage sur les jets de sauvegarde
contre la peur.


##### Actions

***Attaques multiples.*** Le Chevalier effectue deux attaques de mêlée.

***Épée à deux mains.*** *Attaque avec une arme de corps-à-corps :* +5
pour toucher, allonge de 1,5 m, une cible. *Touché :* 10 (2d6 + 3)
points de dégâts tranchants.

***Arbalète lourde.*** *Attaque avec une arme à distance :* +2 pour
toucher, portée de 100/400 pieds, une cible. *Touché :* 5 (1d10) points
de dégâts perforants.

***Leadership (se recharge après un repos court ou long).*** Pendant 1
minute, le chevalier peut prononcer une injonction ou un avertissement
spécial chaque fois qu'une créature non hostile qu'il peut voir dans
un rayon de 30 pieds de lui fait un jet d'attaque ou un jet de
sauvegarde. La créature peut ajouter un d4 à son jet, à condition
qu'elle puisse entendre et comprendre le chevalier. Une créature ne
peut bénéficier que d'un seul dé de leadership à la fois. Cet effet
prend fin si le chevalier est frappé d'incapacité.



##### Réactions

***Parade.*** Le chevalier ajoute 2 à sa CA contre une attaque de mêlée
qui le toucherait. Pour cela, le chevalier doit voir l'attaquant et
manier une arme de mêlée.

Les**chevaliers** sont des guerriers qui s'engagent à servir les
souverains, les ordres religieux et les nobles causes. L'alignement
d'un chevalier détermine la mesure dans laquelle son engagement est
honoré. Qu'il entreprenne une quête ou patrouille dans un royaume, un
chevalier voyage souvent avec un entourage comprenant des écuyers et des
mercenaires qui sont des roturiers.




#### Mage

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 12 (15 avec une *armure de mage*

**Points de vie** 40 (9d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   9 (-1)    14 (+2)   11 (+0)   17 (+3)   12 (+1)   11 (+0)

  -----------------------------------------------------------

**Jets de sauvegarde** Int +6, Wis +4

**Compétences** Arcane +6, Histoire +6

**Sens** passif Perception 11

**Langues** quatre langues au choix

**Défi** 6 (2 300 XP)

***Incantation.*** Le mage est un lanceur de sorts de 9e niveau. Sa
caractéristique d'incantation est l'Intelligence (sauvegarde contre
les sorts DC 14, +6 pour toucher avec les attaques de sorts). Le mage
dispose des sorts de magicien suivants préparés :

Tours de magie (à volonté) : *éclair de* feu,
*lumière*,
*prestidigitation*.

1er niveau (4 emplacements) : *Détection de la magie*,
Armure *de mage*,
*Bouclier*

2ème niveau (3 emplacements) : Pas *brumeux*,
*Suggestion*

3ème niveau (3 emplacements) : *Contresort*, *Boule de
feu*

4ème niveau (3 emplacements) : *Invisibilité
supérieure*êle

5ème niveau (1 emplacement) : *Cône de froid*


##### Actions

***Dague.*** *Attaque avec une arme de corps-à-corps ou à distance :* +5
pour toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible.
*Touché :* 4 (1d4 + 2) points de dégâts perforants.

Les**Mages** passent leur vie à étudier et à pratiquer la magie. Les
mages d'alignement bon offrent des conseils aux nobles et aux autres
personnes de pouvoir, tandis que les mages maléfiques vivent dans des
sites isolés pour réaliser des expériences innommables sans
interférence.




#### Noble

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 15 (cuirasse)

**Points de vie** 9 (2d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   12 (+1)   11 (+0)   12 (+1)   14 (+2)   16 (+3)

  -----------------------------------------------------------

**Compétences** Supercherie +5, Intuition +4, Persuasion +5

**Sens** passif Perception 12

**Langues** deux langues au choix

**Défi** 1/8 (25 XP)


##### Actions

***Rapière.*** *Attaque avec une arme de corps-à-corps :* +3 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d8 + 1) points de
dégâts perforants.



##### Réactions

***Parade.*** Le noble ajoute 2 à sa CA contre une attaque de mêlée qui
le toucherait. Pour cela, le noble doit voir l'attaquant et manier une
arme de mêlée.

Les**nobles** exercent une grande autorité et une grande influence en
tant que membres de la classe supérieure, possédant des richesses et des
relations qui peuvent les rendre aussi puissants que les monarques et
les généraux. Un noble voyage souvent en compagnie de gardes, ainsi que
de serviteurs qui sont des roturiers.

Les statistiques du noble peuvent également être utilisées pour
représenter les courtisans qui ne sont pas de naissance noble.




#### Prêtre

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 13 (chemise de mailles)

**Points de vie** 27 (5d8 + 5)

**Vitesse** 25 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   10 (+0)   12 (+1)   13 (+1)   16 (+3)   13 (+1)

  -----------------------------------------------------------

**Compétences** Médecine +7, Persuasion +3, Religion +4

**Sens** passif Perception 13

**Langues** deux langues au choix

**Défi** 2 (450 XP)

***Éminence divine.*** Comme action bonus, le prêtre peut dépenser un
emplacement de sort pour que ses attaques à l'arme de mêlée infligent
magiquement 10 (3d6) points de dégâts radiants supplémentaires à une
cible en cas de succès. Cet avantage dure jusqu'à la fin du tour. Si le
prêtre dépense un emplacement de sort de 2e niveau ou plus, les dégâts
supplémentaires augmentent de 1d6 pour chaque niveau supérieur au 1er.

***Incantation.*** Le prêtre est un lanceur de sorts de 5e niveau. Sa
caractéristique d'incantation est Sagesse (sauvegarde contre les sorts
DC 13, +5 pour toucher avec les attaques de sorts). Le prêtre a les
sorts de clercs suivants préparés :

Tours de magie (à volonté) : *lumière*, *flamme
sacrée*

1er niveau (4 emplacements) : *Soins*, *Éclair
traçant*

2ème niveau (3 emplacements) : Restauration
*partielle*, *Arme
spirituelle*

3ème niveau (2 emplacements) : *Dissipation de la
magie*


##### Actions

***Masse d'armes.*** *Attaque avec une arme de corps-à-corps :* +2 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 3 (1d6) points de
dégâts contondants.

Les**Prêtres** transmettent les enseignements de leurs dieux au commun
des mortels. Ils sont les chefs spirituels des temples et des
sanctuaires et occupent souvent des postes d'influence dans leurs
communautés. Les prêtres maléfiques peuvent travailler ouvertement sous
les ordres d'un tyran, ou être les dirigeants de sectes religieuses
cachées dans l'ombre de la bonne société, supervisant des rites
dépravés.

Un prêtre a généralement un ou plusieurs acolytes pour l'aider dans les
cérémonies religieuses et autres tâches sacrées.




#### Éclaireur

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 13 (armure en cuir)

**Points de vie** 16 (3d8 + 3)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   11 (+0)   14 (+2)   12 (+1)   11 (+0)   13 (+1)   11 (+0)

  -----------------------------------------------------------

**Compétences** Nature +4, Perception +5, Discrétion +6, Survie +5

**Sens** passif Perception 15

Roturier toute**langue** (généralement commune)

**Défi** 1/2 (100 XP)

***Ouïe et vue perçantes.*** L'Éclaireur a un avantage sur les tests de
Sagesse (Perception) qui reposent sur l'ouïe ou la vue.


##### Actions

***Attaques multiples.*** L'Éclaireur effectue deux attaques de mêlée
ou deux attaques à distance.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Arc long.*** *Attaque avec une arme à distance :* +4 pour toucher,
portée de 150/600 pieds, une cible. *Touché :* 6 (1d8 + 2) points de
dégâts perforants.

Les**Éclaireurs** sont des chasseurs et des traqueurs doués qui offrent
leurs services contre rémunération. La plupart d'entre eux chassent le
gibier, mais quelques-uns travaillent comme chasseurs de primes, servent
de guides ou effectuent des reconnaissances militaires.




#### Espion

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 12

**Points de vie** 27 (6d8)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   10 (+0)   15 (+2)   10 (+0)   12 (+1)   14 (+2)   16 (+3)

  -----------------------------------------------------------

**Compétences** Supercherie +5, Intuition +4, Investigation +5,
Perception +6, Persuasion +5, Escamotage +4, Furtivité +4.

**Sens** passif Perception 16

**Langues** deux langues au choix

**Défi** 1 (200 XP)

***Geste sournois.*** À chacun de ses tours, l'espion peut utiliser une
action bonus pour effectuer l'action Foncer, Se désengager ou Se
cacher.

***Attaque sournoise (1/Tour).*** L'espion inflige 7 (2d6) dégâts
supplémentaires lorsqu'il touche une cible avec une attaque d'arme et
qu'il a l'avantage au jet d'attaque, ou lorsque la cible se trouve à
moins de 1,5 m d'un allié de l'espion qui n'est pas frappé
d'incapacité et que l'espion n'a pas de désavantage au jet
d'attaque.


##### Actions

***Attaques multiples.*** L'espion effectue deux attaques de mêlée.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 5 (1d6 + 2) points de
dégâts perforants.

***Arbalète à main.*** *Attaque avec une arme à distance :* +4 pour
toucher, portée de 30/120 pieds, une cible. *Touché :* 5 (1d6 + 2)
points de dégâts perforants.

Les souverains, les nobles, les marchands, les maîtres de guilde et
d'autres personnes fortunées utilisent des **espions** pour prendre le
dessus dans un monde où la politique est impitoyable. Un espion est
formé pour recueillir secrètement des informations. Les espions loyaux
préfèrent mourir plutôt que de divulguer des informations qui pourraient
les compromettre, eux ou leurs employeurs.




#### Malfrat

*Humanoïde moyen (n'importe quelle origine), tout alignement non bon*

**Classe d'armure** 11 (armure en cuir)

**Points de vie** 32 (5d8 + 10)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   15 (+2)   11 (+0)   14 (+2)   10 (+0)   10 (+0)   11 (+0)

  -----------------------------------------------------------

**Compétences** Intimidation +2

**Sens** passif Perception 10

Roturier toute**langue** (généralement commune)

**Défi** 1/2 (100 XP)

***Tactiques de meute.*** Le Malfrat a l'avantage sur un jet d'attaque
contre une créature si au moins un des alliés du Malfrat se trouve à
moins de 1,5 mètre de la créature et que l'allié n'est pas frappé
d'incapacité.


##### Actions

***Attaques multiples.*** Le Malfrat effectue deux attaques de mêlée.

***Masse d'armes.*** *Attaque avec une arme de corps-à-corps :* +4 pour
toucher, allonge de 1,5 m, une créature. *Touché :* 5 (1d6 + 2) dégâts
contondants.

***Arbalète lourde.*** *Attaque avec une arme à distance :* +2 pour
toucher, portée de 100/400 pieds, une cible. *Touché :* 5 (1d10) points
de dégâts perforants.

Les**Malfrat** sont des exécuteurs impitoyables, doués pour
l'intimidation et la violence. Ils travaillent pour l'argent et ont
peu de scrupules.




#### Guerrier tribal

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 12 (cacher l'armure)

**Points de vie** 11 (2d8 + 2)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   13 (+1)   11 (+0)   12 (+1)   8 (-1)    11 (+0)   8 (-1)

  -----------------------------------------------------------

**Sens** passif Perception 10

**Langues** une seule langue

**Défi** 1/8 (25 XP)

***Tactiques de meute.*** Le guerrier a l'avantage sur un jet
d'attaque contre une créature si au moins un de ses alliés se trouve à
moins de 1,5 mètre de la créature et que cet allié n'est pas frappé
d'incapacité.


##### Actions

***Lance.*** Attaque avec *une arme à distance ou de mêlée*: +3 pour
toucher, allonge de 5 pieds ou portée de 20/60 pieds, une cible. *Touché
:* 4 (1d6 + 1) points de dégâts perforants, ou 5 (1d8 + 1) points de
dégâts perforants si elle est utilisée à deux mains pour une attaque de
mêlée.

Les**guerriers tribaux** vivent en dehors de la civilisation, subsistant
le plus souvent de la pêche et de la chasse. Chaque tribu agit selon les
souhaits de son chef, qui est le plus grand ou le plus ancien guerrier
de la tribu ou un membre de la tribu béni par les dieux.




#### Vétéran

*Humanoïde moyen (n'importe quelle origine), n'importe quel alignement.*

**Classe d'armure** 17 (clibanion)

**Points de vie** 58 (9d8 + 18)

**Vitesse** 30 ft.

  -----------------------------------------------------------
     FOR       DEX       CON       INT       SAG       CHA
  --------- --------- --------- --------- --------- ---------
   16 (+3)   13 (+1)   14 (+2)   10 (+0)   11 (+0)   10 (+0)

  -----------------------------------------------------------

**Compétences** Athlétisme +5, Perception +2

**Sens** passif Perception 12

Roturier toute**langue** (généralement commune)

**Défi** 3 (700 XP)


##### Actions

***Attaques multiples.*** Le Vétéran effectue deux attaques à l'épée
longue. S'il a une épée courte dégainée, il peut également effectuer
une attaque d'épée courte.

***Épée longue.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 7 (1d8 + 3) points de
dégâts tranchants, ou 8 (1d10 + 3) points de dégâts tranchants si
utilisée à deux mains.

***Epée courte.*** *Attaque avec une arme de corps-à-corps :* +5 pour
toucher, allonge de 1,5 m, une cible. *Touché :* 6 (1d6 + 3) points de
dégâts perforants.

***Arbalète lourde.*** *Attaque avec une arme à distance :* +3 pour
toucher, portée de 100/400 pieds, une cible. *Touché :* 6 (1d10 + 1)
points de dégâts perforants.

Les**vétérans** sont des combattants professionnels qui prennent les
armes pour être payés ou pour protéger quelque chose en quoi ils croient
ou qu'ils apprécient. Ils comptent parmi eux des soldats retraités
après un long service et des guerriers qui n'ont jamais servi
qu'eux-mêmes.


