
# Combat


## L'ordre de combat

Un combat typique est un affrontement entre deux camps, un déluge de
coups d'armes, de feintes, de parades, de jeux de jambes et
d'incantations. Le jeu organise le chaos du combat en un cycle de
rounds et de tours. Un round représente environ 6 secondes dans le monde
du jeu. Au cours d'un round, chaque participant à une bataille prend un
tour. L'ordre des tours est déterminé au début d'une rencontre de
combat, lorsque tout le monde fait un jet d'initiative. Une fois que
tout le monde a eu son tour, le combat se poursuit au tour suivant si
aucun camp n'a vaincu l'autre.

> #### Le combat étape par étape {#combat-step-by-step}
>
> 1.  **Déterminer la surprise.** Le MJ détermine si toute personne
>     impliquée dans la rencontre de combat est surprise.
>
> 2.  **Établir les positions.** Le MJ décide de l'emplacement de tous
>     les personnages et monstres. En fonction de l'ordre de marche des
>     aventuriers ou de leur position dans la pièce ou dans un autre
>     lieu, le MJ détermine où se trouvent les adversaires, à quelle
>     distance et dans quelle direction.
>
> 3.  **Lancer l'initiative.** Toutes les personnes impliquées dans la
>     rencontre de combat font un jet d'initiative, déterminant
>     l'ordre des tours des combattants.
>
> 4.  **Chacun son tour.** Chaque participant à la bataille prend son
>     tour dans l'ordre d'initiative.
>
> 5.  **Commencez le tour suivant.** Lorsque toutes les personnes
>     impliquées dans le combat ont eu leur tour, le round se termine.
>     Répétez l'étape 4 jusqu'à ce que le combat s'arrête.


### Surprise

Une bande d'aventuriers s'approche furtivement d'un camp de bandits,
surgissant des arbres pour les attaquer. Un cube gélatineux glisse dans
un passage du donjon, sans être remarqué par les aventuriers jusqu'à ce
qu'il engloutisse l'un d'entre eux. Dans ces situations, un côté de
la bataille gagne la surprise sur l'autre.

Le MJ détermine qui peut être surpris. Si aucun des camps n'essaie
d'être furtif, ils se remarquent automatiquement. Sinon, le MJ compare
les tests de Dextérité (Discrétion) de quiconque se cache avec le score
de Sagesse passive (Perception) de chaque créature du camp adverse. Tout
personnage ou monstre qui ne remarque pas une menace est surpris au
début de la rencontre.

Si vous êtes surpris, vous ne pouvez pas bouger ou faire une action lors
de votre premier tour de combat, et vous ne pouvez pas faire de réaction
avant la fin de ce tour. Un membre d'un groupe peut être surpris même
si les autres membres ne le sont pas.



### Initiative

L'initiative détermine l'ordre des tours pendant le combat. Au début
du combat, chaque participant effectue un test de Dextérité pour
déterminer sa place dans l'ordre d'initiative. Le MJ effectue un seul
jet pour un groupe entier de créatures identiques, de sorte que chaque
membre du groupe agit en même temps.

Le MJ classe les combattants dans l'ordre, de celui qui a le plus haut
total de tests de Dextérité à celui qui a le plus bas. C'est dans cet
ordre (appelé ordre d'initiative) qu'ils agissent à chaque tour.
L'ordre d'initiative reste le même d'un round à l'autre.

En cas d'égalité, le MJ décide de l'ordre entre les créatures
contrôlées par le MJ qui sont à égalité, et les joueurs décident de
l'ordre entre leurs personnages à égalité. Le MJ peut décider de
l'ordre si l'égalité est entre un monstre et un personnage joueur. En
option, le MJ peut demander aux personnages et monstres liés de lancer
un d20 pour déterminer l'ordre, le résultat le plus élevé étant le
premier.



### Votre tour

À votre tour, vous pouvez **vous déplacer** d'une distance égale à
votre vitesse et **effectuer une action**. Vous décidez de vous déplacer
ou d'effectuer votre action en premier. Votre vitesse - parfois appelée
vitesse de marche - est notée sur votre feuille de personnage.

Les actions les plus communes que vous pouvez entreprendre sont décrites
dans la section \"Actions en combat\". De
nombreuses caractéristiques de classe et autres capacités offrent des
options supplémentaires pour votre action.

La section \"Mouvement et position\"
donne les règles de votre mouvement.

Vous pouvez renoncer à vous déplacer, à entreprendre une action ou à
faire quoi que ce soit pendant votre tour. Si vous n'arrivez pas à
décider quoi faire à votre tour, envisagez de faire une action Esquive
ou Se tenir prêt, comme décrit dans \"Actions en
combat\".


#### Actions bonus

Diverses caractéristiques de classe, sorts et autres capacités vous
permettent d'effectuer une action supplémentaire à votre tour, appelée
action bonus. La capacité Geste sournois, par exemple, permet à un
roublard d'effectuer une action bonus. Vous ne pouvez effectuer une
action bonus que si une capacité spéciale, un sort ou une autre
caractéristique du jeu indique que vous pouvez faire quelque chose en
tant qu'action bonus. Sinon, vous n'avez pas d'action bonus à faire.
Vous ne pouvez prendre qu'une seule action bonus à votre tour, vous
devez donc choisir quelle action bonus utiliser lorsque vous en avez
plusieurs de disponibles. Vous choisissez quand prendre une action bonus
pendant votre tour, sauf si le moment de l'action bonus est spécifié,
et tout ce qui vous prive de votre capacité à prendre des actions vous
empêche également de prendre une action bonus.



#### Autre activité à votre tour

Votre tour peut inclure une variété de fioritures qui ne nécessitent ni
votre action ni votre déplacement.

Vous pouvez communiquer comme vous le pouvez, par des énoncés et des
gestes brefs, au fur et à mesure que vous prenez votre tour.

Vous pouvez également interagir avec un objet ou une caractéristique de
l'environnement gratuitement, pendant votre déplacement ou votre
action. Par exemple, vous pouvez ouvrir une porte pendant votre
déplacement vers un ennemi, ou dégainer votre arme dans le cadre de la
même action que vous utilisez pour attaquer.

Si vous voulez interagir avec un deuxième objet, vous devez utiliser
votre action. Certains objets magiques et autres objets spéciaux
nécessitent toujours une action pour être utilisés, comme indiqué dans
leur description.

Le MJ peut vous demander d'utiliser une action pour l'une de ces
activités lorsqu'elle nécessite un soin particulier ou lorsqu'elle
présente un obstacle inhabituel. Par exemple, le MJ peut raisonnablement
s'attendre à ce que vous utilisiez une action pour ouvrir une porte
bloquée ou tourner une manivelle pour abaisser un pont-levis.




### Réactions

Certaines capacités spéciales, certains sorts et certaines situations
vous permettent d'effectuer une action spéciale appelée réaction. Une
réaction est une réponse instantanée à un déclencheur quelconque, qui
peut se produire à votre tour ou à celui d'un autre joueur. L'attaque
d'opportunité est le type de réaction le plus
commun.

Lorsque vous faites une réaction, vous ne pouvez pas en faire une autre
avant le début de votre prochain tour. Si la réaction interrompt le tour
d'une autre créature, cette dernière peut continuer son tour juste
après la réaction.




## Mouvement et position

En combat, les personnages et les monstres sont en mouvement constant,
utilisant souvent le déplacement et la position pour prendre le dessus.

A votre tour, vous pouvez vous déplacer sur une distance égale à votre
vitesse. Vous pouvez utiliser autant ou aussi peu de votre vitesse que
vous le souhaitez à votre tour, en suivant les règles ici.

Votre mouvement peut inclure le saut, l'escalade et la natation. Ces
différents modes de déplacement peuvent être combinés avec la marche, ou
bien ils peuvent constituer l'intégralité de votre déplacement. Quelle
que soit la façon dont vous vous déplacez, vous déduisez la distance de
chaque partie de votre mouvement de votre vitesse jusqu'à ce qu'elle
soit épuisée ou que vous ayez fini de vous déplacer.


### La rupture de votre déménagement

Vous pouvez fractionner votre mouvement à votre tour, en utilisant une
partie de votre vitesse avant et après votre action. Par exemple, si
vous avez une vitesse de 30 pieds, vous pouvez vous déplacer de 10
pieds, faire votre action, puis vous déplacer de 20 pieds.


#### Se déplacer entre les attaques

Si vous entreprenez une action qui comprend plus d'une attaque d'arme,
vous pouvez fractionner encore plus votre mouvement en vous déplaçant
entre ces attaques. Par exemple, un guerrier qui peut effectuer deux
attaques avec la capacité Attaque supplémentaire et qui a une vitesse de
25 pieds peut se déplacer de 10 pieds, effectuer une attaque, se
déplacer de 15 pieds, puis attaquer à nouveau.



#### Utilisation de différentes vitesses

Si vous avez plus d'une vitesse, comme votre vitesse de marche et une
vitesse de vol, vous pouvez passer d'une vitesse à l'autre pendant
votre déplacement. À chaque fois que vous changez de vitesse, soustrayez
la distance que vous avez déjà parcourue de la nouvelle vitesse. Le
résultat détermine la distance que vous pouvez parcourir. Si le résultat
est inférieur ou égal à 0, vous ne pouvez pas utiliser la nouvelle
vitesse pendant le déplacement en cours.

Par exemple, si vous avez une vitesse de 30 et une vitesse de vol de 60
parce qu'un magicien vous a lancé le sort *Vol*, vous pouvez
voler sur 20 pieds, puis marcher sur 10 pieds, et enfin sauter dans les
airs pour voler sur 30 pieds de plus.




### Terrain difficile

Les combats se déroulent rarement dans des pièces nues ou sur des
plaines sans relief. Les cavernes jonchées de rochers, les forêts
couvertes de ronces, les escaliers traîtres - le cadre d'un combat
typique contient un terrain difficile.

Chaque pied de mouvement en terrain difficile coûte 1 pied
supplémentaire. Cette règle est vraie même si plusieurs choses dans un
espace comptent comme terrain difficile.

Les meubles bas, les gravats, les broussailles, les escaliers abrupts,
la neige et les tourbières peu profondes sont des exemples de terrain
difficile. L'espace d'une autre créature, qu'elle soit hostile ou
non, compte également comme un terrain difficile.



### Être à terre

Les combattants se retrouvent souvent allongés sur le sol, soit parce
qu'ils sont déboulés, soit parce qu'ils se jettent à terre. Dans le
jeu, ils sont étourdis .

Vous pouvez **vous mettre à plat ventre** sans utiliser votre vitesse.
**Se mettre debout** demande plus d'efforts ; cela coûte une quantité
de mouvement égale à la moitié de votre vitesse. Par exemple, si votre
vitesse est de 30 pieds, vous devez dépenser 15 pieds de mouvement pour
vous mettre debout. Vous ne pouvez pas vous mettre debout si vous
n'avez plus assez de mouvement ou si votre vitesse est nulle.

Pour vous déplacer lorsque vous êtes à terre, vous devez **ramper** ou
utiliser une magie telle que la téléportation. Chaque pied de mouvement
en rampant coûte 1 pied supplémentaire. Ramper de 1 pied en terrain
difficile coûte donc 3 pieds de mouvement.

> #### Interagir avec les objets qui vous entourent {#interacting-with-objects-around-you}
>
> Voici quelques exemples du genre de choses que vous pouvez faire en
> tandem avec votre mouvement et votre action :
>
> -   dégainer ou rengainer une épée
> -   ouvrir ou fermer une porte
> -   retirer une potion de votre sac à dos
> -   ramasser une hache abandonnée
> -   prendre une babiole sur une table
> -   retirer une bague de votre doigt
> -   mettez de la nourriture dans votre bouche
> -   planter une bannière dans le sol
> -   sortir quelques pièces de votre pochette de ceinture
> -   boire toute l'ale dans un flacon
> -   actionner un levier ou un interrupteur
> -   tirer une torche d'une applique
> -   prendre un livre sur une étagère que vous pouvez atteindre
> -   éteindre une petite flamme
> -   mettre un masque
> -   tirez le capuchon de votre cape vers le haut et sur votre tête.
> -   mettez votre oreille sur une porte
> -   donner un coup de pied à une petite pierre
> -   tourner une clé dans une serrure
> -   taper le sol avec une perche de 3 mètres
> -   remettre un objet à un autre personnage



### Se déplacer autour d'autres créatures

Vous pouvez vous déplacer dans l'espace d'une créature non hostile. En
revanche, vous ne pouvez vous déplacer dans l'espace d'une créature
hostile que si celle-ci est au moins deux tailles plus grande ou plus
petite que vous. N'oubliez pas que l'espace d'une autre créature est
un terrain difficile pour vous.

Qu'une créature soit amie ou ennemie, vous ne pouvez pas volontairement
terminer votre mouvement dans son espace.

Si vous quittez la portée d'une créature hostile pendant votre
déplacement, vous provoquez une attaque
d'opportunité.



### Mouvement Volant

Les créatures volantes bénéficient des nombreux avantages de la
mobilité, mais elles doivent aussi faire face au danger de la chute. Si
une créature volante est mise au tapis, que sa vitesse est réduite à 0
ou qu'elle est privée de la possibilité de se déplacer, elle tombe, à
moins qu'elle ne soit capable de planer ou qu'elle soit maintenue en
l'air par la magie, comme par le sort *Vol*.



### Taille de la créature

Chaque créature occupe une quantité différente d'espace. Le tableau des
catégories de taille indique la quantité d'espace qu'une créature
d'une taille donnée contrôle en combat. Les objets utilisent parfois
les mêmes catégories de taille.

  --------------------------------------
  Taille          Espace
  --------------- ----------------------
  Très petite     2-1/2 par 2-1/2 pieds.
  (TP)            

  Petit           5 par 5 pieds

  Moyen           5 par 5 pieds

  Grand           10 par 10 pieds.

  Très grand      15 par 15 pieds.

  Gargantuesque   20 par 20 pieds ou
                  plus
  --------------------------------------

  : Catégories de taille


#### Espace

L'espace d'une créature est la zone en pieds qu'elle contrôle
effectivement en combat, et non l'expression de ses dimensions
physiques. Une créature moyenne typique ne fait pas 1,5 mètre de large,
par exemple, mais elle contrôle un espace de cette largeur. Si un
hobgobelin de taille moyenne se tient dans une porte de 1,5 m de large,
les autres créatures ne peuvent pas passer à moins que le hobgobelin ne
les laisse faire.

L'espace d'une créature reflète également la zone dont elle a besoin
pour combattre efficacement. Pour cette raison, il y a une limite au
nombre de créatures qui peuvent entourer une autre créature en combat.
En supposant des combattants de taille moyenne, huit créatures peuvent
tenir dans un rayon de 1,5 m autour d'une autre.

Comme les créatures plus grandes prennent plus de place, elles sont
moins nombreuses à pouvoir entourer une créature. Si cinq créatures
Grandes s'entassent autour d'une créature Moyenne ou plus petite, il y
a peu de place pour les autres. En revanche, jusqu'à vingt créatures
moyennes peuvent entourer une créature Gargantuesque.



#### S'adapter à un espace plus restreint

Une créature peut se faufiler dans un espace qui est suffisamment grand
pour une créature d'une taille inférieure à la sienne. Ainsi, une
créature Grand peut se faufiler dans un passage de seulement 1,5 m de
large. Lorsqu'elle se faufile dans un espace, une créature doit
dépenser 1 pied supplémentaire pour chaque pied qu'elle y déplace, et
elle est désavantagée aux jets d'attaque et aux jets de sauvegarde de
Dextérité. Les jets d'attaque contre la créature ont un avantage tant
qu'elle se trouve dans l'espace réduit.





## Actions en combat

Lorsque vous effectuez votre action à votre tour, vous pouvez prendre
une des actions présentées ici, une action que vous avez obtenue grâce à
votre classe ou à une caractéristique spéciale, ou une action que vous
improvisez. De nombreux monstres ont leurs propres options d'action
dans leurs blocs de statuts.

Lorsque vous décrivez une action qui n'est pas détaillée ailleurs dans
les règles, le MJ vous indique si cette action est possible et quel type
de jet vous devez effectuer, le cas échéant, pour déterminer la réussite
ou l'échec.


### Attaque

L'action la plus commune en combat est l'action Attaquer, qu'il
s'agisse de brandir une épée, de tirer une flèche d'un arc ou de se
battre avec les poings.

Avec cette action, vous effectuez une attaque de mêlée ou à distance.
Consultez la section \"Attaquer\" pour
connaître les règles qui régissent les attaques.

Certaines capacités, comme la caractéristique Attaque supplémentaire du
guerrier, vous permettent d'effectuer plus d'une attaque avec cette
action.



### Lancer un sort

Les lanceurs de sorts tels que les magiciens et les clercs, ainsi que de
nombreux monstres, ont accès à des incantations et peuvent les utiliser
à bon escient en combat. Chaque sort a un temps d'incantation, qui
précise si le lanceur doit utiliser une action, une réaction, des
minutes ou même des heures pour lancer le sort. Lancer un sort n'est
donc pas nécessairement une action. La plupart des sorts ont un temps
d'incantation d'une action, de sorte que le lanceur de sorts utilise
souvent son action en combat pour lancer un tel sort.



### Dash

Lorsque vous effectuez l'action Foncer, vous gagnez un mouvement
supplémentaire pour le tour en cours. L'augmentation est égale à votre
vitesse, après application de tout modificateur. Avec une vitesse de 30
pieds, par exemple, vous pouvez vous déplacer jusqu'à 60 pieds pendant
votre tour si vous vous précipitez.

Toute augmentation ou diminution de votre vitesse modifie ce mouvement
supplémentaire de la même quantité. Si votre vitesse de 30 pieds est
réduite à 15 pieds, par exemple, vous pouvez vous déplacer de 30 pieds
au cours de ce tour en vous élançant.



### Désengager

Si vous effectuez l'action Se désangager, votre mouvement ne provoque
pas d'attaques d'opportunité pour le reste du tour.



### Dodge

Lorsque vous effectuez l'action Esquive, vous vous concentrez
entièrement sur l'évitement des attaques. Jusqu'au début de votre
prochain tour, tout jet d'attaque effectué contre vous est désavantagé
si vous pouvez voir l'attaquant, et vous effectuez des jets de
sauvegarde de Dextérité avec avantage. Vous perdez cet avantage si vous
êtes frappé d'incapacité  ou si
votre vitesse tombe à 0.



### Aide

Vous pouvez prêter votre aide à une autre créature dans
l'accomplissement d'une tâche. Lorsque vous effectuez l'action Aider,
la créature que vous aidez gagne un avantage sur le prochain test de
capacité qu'elle effectue pour accomplir la tâche pour laquelle vous
l'aidez, à condition qu'elle effectue ce test avant le début de votre
prochain tour.

Vous pouvez également aider une créature amie à attaquer une créature
située à moins de 1,5 m de vous. Vous feintez, distrayez la cible ou
faites équipe d'une manière ou d'une autre pour rendre l'attaque de
votre allié plus efficace. Si votre allié attaque la cible avant votre
prochain tour, le premier jet d'attaque est effectué avec avantage.



### Cacher {#cacher}

Lorsque vous effectuez l'action Se cacher, vous effectuez un test de
Dextérité (Discrétion) pour tenter de vous cacher, en suivant les règles
de dissimulation. Si vous réussissez, vous gagnez certains avantages,
comme décrit dans la section \"Attaquants et cibles
invisibles\".



### Prêt

Parfois, vous voulez prendre de l'avance sur un ennemi ou attendre une
circonstance particulière avant d'agir. Pour cela, vous pouvez faire
l'action Se tenir prêt à votre tour, ce qui vous permet d'agir en
utilisant votre réaction avant le début de votre prochain tour.

D'abord, vous décidez quelle circonstance perceptible va déclencher
votre réaction. Ensuite, vous choisissez l'action que vous allez
entreprendre en réponse à ce déclencheur, ou vous choisissez de vous
déplacer à votre vitesse en réponse à ce déclencheur. Par exemple : \"Si
le cultiste marche sur la trappe, je tire le levier qui l'ouvre\" et
\"Si le gobelin marche à côté de moi, je m'éloigne\".

Lorsque le déclencheur se produit, vous pouvez soit prendre votre
réaction juste après la fin du déclencheur, soit ignorer le déclencheur.
N'oubliez pas que vous ne pouvez effectuer qu'une seule réaction par
round.

Lorsque vous préparez un sort, vous le lancez comme d'habitude mais
vous retenez son énergie, que vous libérez avec votre réaction lorsque
le déclencheur se produit. Pour être préparé, un sort doit avoir un
temps d'incantation de 1 action, et retenir la magie du sort demande de
la concentration. Si votre concentration est rompue, le sort se dissipe
sans faire d'effet. Par exemple, si vous vous concentrez sur le sort
*toile* et que vous préparez *missile
magique* se termine, et si
vous subissez des dégâts avant de libérer *missile
magique* avec votre réaction, votre concentration
risque d'être rompue.



### Recherche

Lorsque vous effectuez l'action Chercher, vous consacrez votre
attention à trouver quelque chose. Selon la nature de votre recherche,
le MJ peut vous demander de faire un test de Sagesse (Perception) ou un
test d'Intelligence (Investigation).



### Utiliser un objet

Vous interagissez normalement avec un objet tout en faisant autre chose,
comme lorsque vous dégainez une épée dans le cadre d'une attaque.
Lorsqu'un objet nécessite votre action pour être utilisé, vous
effectuez l'action Utiliser un objet. Cette action est également utile
lorsque vous souhaitez interagir avec plus d'un objet pendant votre
tour.




## Faire une attaque

Que vous frappiez avec une arme de mêlée, que vous tiriez une arme à
distance ou que vous fassiez un jet d'attaque dans le cadre d'un sort,
une attaque a une structure simple.

1.  **Choisissez une cible.** Choisissez une cible dans la portée de
    votre attaque : une créature, un objet ou un lieu.
2.  **Déterminez les modificateurs.** Le MJ détermine si la cible est à
    couvert et si vous avez un avantage ou un désavantage contre elle.
    De plus, les sorts, les capacités spéciales et d'autres effets
    peuvent appliquer des pénalités ou des bonus à votre jet d'attaque.
3.  **Résolvez l'attaque.** Vous effectuez le jet d'attaque. En cas de
    succès, vous effectuez un jet de dégâts, sauf si l'attaque en
    question a des règles qui spécifient le contraire. Certaines
    attaques provoquent des effets spéciaux en plus ou à la place des
    dégâts.

Si la question se pose de savoir si ce que vous faites compte comme une
attaque, la règle est simple : si vous faites un jet d'attaque, vous
faites une attaque.


### Jets d'attaque

Lorsque vous effectuez une attaque, votre jet d'attaque détermine si
l'attaque touche ou rate. Pour effectuer un jet d'attaque, lancez un
d20 et ajoutez les modificateurs appropriés. Si le total du jet et des
modificateurs est égal ou supérieur à la classe d'armure (CA) de la
cible, l'attaque réussit. La CA d'un personnage est déterminée lors de
la création du personnage, alors que la CA d'un monstre se trouve dans
son bloc de statistiques.


#### Modificateurs du jet

Lorsqu'un personnage effectue un jet d'attaque, les deux modificateurs
les plus communs sont un modificateur de capacité et le bonus de
maîtrise du personnage. Lorsqu'un monstre effectue un jet d'attaque,
il utilise le modificateur indiqué dans son bloc de statistiques.

***Modificateur d'aptitude.*** Le modificateur de capacité utilisé pour
une attaque avec une arme de corps-à-corps est la Force, et le
modificateur de capacité utilisé pour une attaque avec une arme à
distance est la Dextérité. Les armes qui ont la propriété finesse ou
lancée enfreignent cette règle.

Certains sorts nécessitent également un jet d'attaque. Le modificateur
d'aptitude utilisé pour une attaque de sort dépend de la
caractéristique d'incantation du lanceur de sorts.

***Bonus de maîtrise.*** Vous ajoutez votre bonus de maîtrise à votre
jet d'attaque lorsque vous attaquez avec une arme que vous maîtrisez,
ainsi que lorsque vous attaquez avec un sort.



#### Rouler 1 ou 20

Il arrive que le destin bénisse ou maudisse un combattant, faisant que
le novice touche et que le vétéran rate.

Si le jet de d20 pour une attaque est un 20, l'attaque touche sans
tenir compte des modificateurs ou de la CA de la cible. C'est ce qu'on
appelle un coup critique.

Si le jet de d20 pour une attaque est un 1, l'attaque rate sans tenir
compte des modificateurs ou de la CA de la cible.




### Attaquants et cibles invisibles

Les combattants tentent souvent d'échapper à la vigilance de leurs
ennemis en se cachant, en lançant le sort
d'*invisibilité* ou en se tapissant dans les ténèbres.

Lorsque vous attaquez une cible que vous ne pouvez pas voir, vous avez
un désavantage sur le jet d'attaque. Ceci est vrai que vous deviniez
l'emplacement de la cible ou que vous visiez une créature que vous
pouvez entendre mais pas voir. Si la cible ne se trouve pas à l'endroit
que vous avez ciblé, vous ratez automatiquement votre coup, mais le MJ
se contente généralement de dire que l'attaque a raté, sans préciser si
vous avez deviné correctement l'emplacement de la cible.

Lorsqu'une créature ne peut pas vous voir, vous avez un avantage sur
les jets d'attaque contre elle. Si vous êtes caché - à la fois
invisible et inaudible - lorsque vous effectuez une attaque, vous donnez
votre emplacement lorsque l'attaque touche ou rate.



### Attaques à distance

Lorsque vous effectuez une attaque à distance, vous tirez avec un arc ou
une arbalète, lancez une hachette ou envoyez des projectiles pour
frapper un ennemi à distance. Un monstre peut tirer des épines de sa
queue. De nombreux sorts impliquent également une attaque à distance.


#### Portée

Vous ne pouvez effectuer des attaques à distance que contre des cibles
situées dans une portée donnée. Si une attaque à distance, comme celle
effectuée avec un sort, a une seule portée, vous ne pouvez pas attaquer
une cible au-delà de cette portée.

Certaines attaques à distance, comme celles effectuées avec un arc long
ou un arc court, ont deux portées. Le plus petit chiffre est la portée
normale, et le plus grand chiffre est la portée longue. Votre jet
d'attaque est désavantagé lorsque votre cible est au-delà de la portée
normale, et vous ne pouvez pas attaquer une cible au-delà de la longue
portée.




### Attaques à distance en combat rapproché

Viser une attaque à distance est plus difficile lorsqu'un ennemi est à
côté de vous. Lorsque vous effectuez une attaque à distance avec une
arme, un sort ou tout autre moyen, vous avez un désavantage au jet
d'attaque si vous vous trouvez à moins de 1,5 m d'une créature hostile
qui peut vous voir et qui n'est pas frappée d'incapacité.



### Attaques de mêlée

Utilisée en combat au corps à corps, une attaque de mêlée vous permet
d'attaquer un ennemi à votre portée. Une attaque de mêlée utilise
généralement une arme de poing telle qu'une épée, un marteau de guerre
ou une hache. Un monstre typique effectue une attaque de mêlée
lorsqu'il frappe avec ses griffes, ses cornes, ses dents, ses
tentacules ou une autre partie de son corps. Quelques sorts impliquent
également une attaque de mêlée.

La plupart des créatures ont une **portée** de 1,5 m et peuvent donc
attaquer des cibles situées à 1,5 m d'elles lorsqu'elles effectuent
une attaque de mêlée. Certaines créatures (généralement celles de taille
supérieure à Medium) ont des attaques de mêlée d'une portée supérieure
à 5 pieds, comme indiqué dans leur description.

Au lieu d'utiliser une arme pour effectuer une attaque de mêlée, vous
pouvez utiliser une **frappe sans arme :** un coup de poing, un coup de
pied, un coup de tête ou un coup de force similaire (qui ne comptent pas
comme des armes). En cas de succès, une attaque à mains nues inflige des
dégâts contondants égaux à 1 + votre modificateur de Force. Vous êtes
compétent en matière de frappes à mains nues.


#### Attaques d'opportunité

Dans un combat, tout le monde est constamment à l'affût d'une occasion
de frapper un ennemi qui s'enfuit ou qui passe par là. Une telle frappe
s'appelle une attaque d'opportunité.

Vous pouvez effectuer une attaque d'opportunité lorsqu'une créature
hostile que vous pouvez voir se déplace hors de votre portée. Pour
effectuer cette attaque d'opportunité, vous utilisez votre réaction
pour effectuer une attaque de mêlée contre la créature qui vous
provoque. L'attaque a lieu juste avant que la créature ne quitte votre
portée.

Vous pouvez éviter de provoquer une attaque d'opportunité en effectuant
l'action Se désengager. Vous ne provoquez pas non plus d'attaque
d'opportunité lorsque vous vous téléportez ou lorsque quelqu'un ou
quelque chose vous déplace sans utiliser votre mouvement, action ou
réaction. Par exemple, vous ne provoquez pas d'attaque d'opportunité
si une explosion vous projette hors de portée d'un ennemi ou si la
gravité vous fait tomber devant un ennemi.



#### Combat à deux armes

Lorsque vous effectuez l'action Attaquer et attaquez avec une arme de
mêlée légère que vous tenez dans une main, vous pouvez utiliser une
action bonus pour attaquer avec une autre arme de mêlée légère que vous
tenez dans l'autre main. Vous n'ajoutez pas votre modificateur de
capacité aux dégâts de l'attaque bonus, sauf si ce modificateur est
négatif.

Si l'une ou l'autre des armes possède la propriété lancée, vous pouvez
lancer l'arme, au lieu d'effectuer une attaque de mêlée avec elle.



#### Agrippés

Lorsque vous voulez saisir une créature ou lutter avec elle, vous pouvez
utiliser l'action d'attaque pour effectuer une attaque de mêlée
spéciale, un grappin. Si vous pouvez effectuer plusieurs attaques avec
l'action Attaquer, cette attaque remplace l'une d'entre elles.

La cible de votre grappin ne doit pas avoir plus d'une taille de plus
que vous et doit être à votre portée. En utilisant au moins une main
libre, vous essayez de saisir la cible en effectuant un test
d'agrippement au lieu d'un jet d'attaque : un test de Force
(Athlétisme) contesté par un test de Force (Athlétisme) ou de Dextérité
(Acrobaties) de la cible (la cible choisit la capacité à utiliser). Si
vous réussissez, vous soumettez la cible à la condition agrippée . L'état spécifie les éléments
qui y mettent fin, et vous pouvez libérer la cible quand vous le
souhaitez (aucune action requise).

***Echapper à un grappin.*** Une créature agrippée peut utiliser son
action pour s'échapper. Pour ce faire, elle doit réussir un test de
Force (Athlétisme) ou de Dextérité (Acrobaties) contesté par votre test
de Force (Athlétisme).

***Déplacer une créature agrippée.*** Lorsque vous vous déplacez, vous
pouvez traîner ou transporter la créature agrippée avec vous, mais votre
vitesse est réduite de moitié, sauf si la créature est plus petite que
vous de deux tailles ou plus.

> #### Concours de combat {#contests-in-combat}
>
> La bataille consiste souvent à opposer vos prouesses à celles de votre
> adversaire. Un tel défi est représenté par un concours. Cette section
> comprend les concours les plus communs qui nécessitent une action en
> combat : agripper et bousculer une créature. Le MJ peut utiliser ces
> concours comme modèles pour en improviser d'autres.



#### Bousculer une créature

En utilisant l'action Attaquer, vous pouvez faire une attaque de mêlée
spéciale pour pousser une créature, soit pour la mettre au sol, soit
pour la repousser loin de vous. Si vous pouvez effectuer plusieurs
attaques avec l'action Attaquer, cette attaque remplace l'une d'entre
elles.

La cible ne doit pas avoir plus d'une taille de plus que vous et doit
être à votre portée. Au lieu de faire un jet d'attaque, vous effectuez
un test de Force (Athlétisme) contesté par le test de Force (Athlétisme)
ou de Dextérité (Acrobaties) de la cible (la cible choisit la capacité à
utiliser). Si vous remportez le concours, vous placez la cible à plat
ventre ou la poussez à 1,5 m de vous.





## Abri

Les murs, les arbres, les créatures et autres obstacles peuvent fournir
un abri pendant le combat, rendant une cible plus difficile à atteindre.
Une cible ne peut bénéficier d'un abri que si une attaque ou un autre
effet provient du côté opposé de l'abri.

Il existe trois degrés d'abri. Si une cible se trouve derrière
plusieurs sources d'abri, seul le degré d'abri le plus protecteur
s'applique ; les degrés ne sont pas additionnés. Par exemple, si une
cible se trouve derrière une créature qui lui donne un demi-abri et un
tronc d'arbre qui lui donne trois-quarts d'abri, elle a trois-quarts
d'abri.

Une cible à **demi-abri** bénéficie d'un bonus de +2 à la CA et aux
jets de sauvegarde de Dextérité. Une cible est à demi-abri si un
obstacle bloque au moins la moitié de son corps. L'obstacle peut être
un muret, un grand meuble, un tronc d'arbre étroit ou une créature, que
cette dernière soit un ennemi ou un ami.

Une cible **couverte aux trois quarts** bénéficie d'un bonus de +5 à la
CA et aux jets de sauvegarde de Dextérité. Une cible est à couvert aux
trois quarts si elle est couverte aux trois quarts par un obstacle.
L'obstacle peut être une herse, une fente pour les flèches ou un tronc
d'arbre épais.

Une cible à **couvert total** ne peut pas être visée directement par une
attaque ou un sort, bien que certains sorts puissent atteindre une telle
cible en l'incluant dans une zone d'effet. Une cible bénéficie d'un
abri total si elle est complètement dissimulée par un obstacle.



## Dégâts et Guérison

Les blessures et le risque de mort sont les compagnons constants de ceux
qui explorent les univers de jeux fantastiques. Un coup d'épée, une
flèche bien placée ou un jet de flamme d'un sort de *boule de
feu* peuvent endommager, voire tuer, les créatures les plus
robustes.


### Points de vie

Les points de vie représentent une combinaison d'endurance physique et
mentale, de volonté de vivre et de chance. Les créatures ayant plus de
points de vie sont plus difficiles à tuer. Celles qui ont moins de
points de vie sont plus fragiles.

Les points de vie actuels d'une créature (généralement appelés points
de vie) peuvent être n'importe quel nombre entre le maximum de points
de vie de la créature et 0. Ce nombre change fréquemment lorsque la
créature subit des dégâts ou reçoit des soins.

Chaque fois qu'une créature subit des dégâts, ceux-ci sont soustraits
de ses points de vie. La perte de points de vie n'a aucun effet sur les
capacités d'une créature jusqu'à ce qu'elle tombe à 0 points de vie.



### Roulements de dommages

Chaque arme, sort et capacité de monstre nuisible précise les dégâts
qu'il inflige. Vous lancez le ou les dés de dégâts, ajoutez les
modificateurs éventuels, et appliquez les dégâts à votre cible. Les
armes magiques, les capacités spéciales et d'autres facteurs peuvent
accorder un bonus aux dégâts. Avec une pénalité, il est possible
d'infliger 0 dégât, mais jamais de dégât négatif.

Lorsque vous attaquez avec une **arme,** vous ajoutez votre modificateur
de capacité - le même modificateur que celui utilisé pour le jet
d'attaque - aux dégâts. Un **sort** vous indique quels dés lancer pour
les dégâts et si vous devez ajouter des modificateurs.

Si un sort ou un autre effet inflige des dégâts à **plusieurs cibles**
en même temps, lancez le dé une seule fois pour chacune d'entre elles.
Par exemple, lorsqu'un magicien lance une *boule de feu*
ou qu'un clerc lance Colonne *de flamme*, les dégâts
du sort sont lancés une fois pour toutes les créatures prises dans
l'explosion.


#### Coups critiques

Lorsque vous obtenez un succès critique, vous pouvez lancer des dés
supplémentaires pour les dégâts de l'attaque contre la cible. Lancez
deux fois tous les dés de dégâts de l'attaque et additionnez-les.
Ajoutez ensuite tout modificateur pertinent comme d'habitude. Pour
accélérer le jeu, vous pouvez lancer tous les dés de dégâts en une seule
fois.

Par exemple, si vous obtenez un coup critique avec une dague, lancez 2d4
pour les dégâts, au lieu de 1d4, puis ajoutez votre modificateur
d'aptitude pertinent. Si l'attaque implique d'autres dés de dégâts,
comme ceux de la caractéristique Attaque sournoise du roublard, vous
lancez ces dés deux fois également.



#### Types de dommages

Différentes attaques, sorts dommageables et autres effets néfastes
infligent différents types de dégâts. Les types de dégâts n'ont pas de
règles propres, mais d'autres règles, comme la résistance aux dégâts,
reposent sur les types.

Les types de dégâts suivent, avec des exemples pour aider un MJ à
attribuer un type de dégâts à un nouvel effet.

***Acide.*** Le jet corrosif du souffle d'un dragon noir et les enzymes
dissolvantes sécrétées par un pouding noir infligent des dégâts acides.

***Contondant.*** Les attaques par force contondante - marteaux, chutes,
constrictions, etc. - infligent des dégâts de contondant.

***Froid.*** Le froid infernal qui irradie de la lance d'un diable de
glace et le souffle glacial d'un dragon blanc infligent des dégâts de
froid.

***Feu.*** Les dragons rouges crachent du feu, et de nombreux sorts
conjurent des flammes pour infliger des dégâts de feu.

***La Force.*** La force est une énergie magique pure concentrée sous
une forme dommageable. La plupart des effets qui infligent des dégâts de
force sont des sorts, notamment *Projectile magique*
et *Arme spirituelle*.

***Foudre.*** Un sort de *foudre* et le souffle d'un
dragon bleu infligent des dégâts de foudre.

***Nécrotique.*** Les dégâts nécrotiques, infligés par certains
morts-vivants et un sort tel que Contact *glacial*,
flétrissent la matière et même l'âme.

***Perforant .*** Les attaques perforantes et empalantes, y compris les
lances et les morsures de monstres, infligent des dégâts perforants.

***Empoisonné.*** Les piqûres venimeuses et le gaz toxique de l'haleine
d'un dragon vert infligent des dégâts de poison.

***Psychique.*** Les capacités mentales, telles que l'explosion
psionique, infligent des dégâts psychiques.

***Radiant.*** Les dégâts radiants, infligés par le sort Colonne de
*flamme* d'un clerc ou par l'arme d'un ange, brûlent
la chair comme le feu et surchargent l'esprit de puissance.

***Tranchant.*** Les épées, les haches et les griffes des monstres
infligent des dégâts tranchants.

***Tonnerre.*** Une explosion de sons, comme l'effet du sort *Vague
tonnante*, inflige des dégâts de tonnerre.




### Résistance aux dégâts et vulnérabilité

Certaines créatures et objets sont excessivement difficiles ou
exceptionnellement faciles à blesser avec certains types de dégâts.

Si une créature ou un objet possède une **résistance** à un type de
dégâts, les dégâts de ce type sont réduits de moitié. Si une créature ou
un objet est **vulnérable à** un type de dommage, les dommages de ce
type sont doublés.

La résistance puis la vulnérabilité sont appliquées après tous les
autres modificateurs de dégâts. Par exemple, une créature possède une
résistance aux dégâts de matraquage et est touchée par une attaque qui
lui inflige 25 points de dégâts de matraquage. La créature se trouve
également dans une aura magique qui réduit tous les dégâts de 5. Les 25
dégâts sont d'abord réduits de 5, puis divisés par deux, de sorte que
la créature subit 10 dégâts.

Les instances multiples de résistance ou de vulnérabilité qui affectent
le même type de dégâts ne comptent que pour une seule instance. Par
exemple, si une créature possède une résistance aux dégâts de feu ainsi
qu'une résistance à tous les dégâts non magiques, les dégâts d'un feu
non magique sont réduits de moitié contre la créature, et non réduits de
trois quarts.



### Guérison

A moins qu'ils n'entraînent la mort, les dommages ne sont pas
permanents. Même la mort est réversible grâce à une magie puissante. Le
repos peut restaurer les points de vie d'une créature, et des méthodes
magiques telles qu'un sort de *soins* ou une *potion de
guérison* peuvent supprimer les dégâts en un instant.

Lorsqu'une créature reçoit des soins, quels qu'ils soient, les points
de vie récupérés sont ajoutés à ses points de vie actuels. Les points de
vie d'une créature ne peuvent pas dépasser son maximum de points de
vie, donc tout point de vie récupéré au-delà de ce nombre est perdu. Par
exemple, un druide accorde à un rôdeur 8 points de vie de guérison. Si
le rôdeur a 14 points de vie actuels et que son maximum de points de vie
est de 20, le rôdeur récupère 6 points de vie auprès du druide, et non
8.

Une créature morte ne peut pas regagner de points de vie tant qu'une
magie telle que le sort Retour *à la vie* ne l'a pas
ramenée à la vie.



### Chute à 0 points de vie

Lorsque vous tombez à 0 point de vie, vous mourrez ou tombez
inconscient, comme expliqué dans les sections suivantes.


#### Mort instantanée

Les dégâts massifs peuvent vous tuer instantanément. Lorsque les dégâts
vous réduisent à 0 point de vie et qu'il en reste, vous mourez si les
dégâts restants sont égaux ou supérieurs à votre maximum de points de
vie.

Par exemple, un clerc avec un maximum de 12 points de vie a actuellement
6 points de vie. S'il subit 18 points de dégâts lors d'une attaque, il
est réduit à 0 point de vie, mais il lui reste 12 points de dégâts.
Comme les dégâts restants sont égaux à son maximum de points de vie, le
clerc meurt.



#### Tomber inconscient

Si les dégâts vous ramènent à 0 points de vie et ne parviennent pas à
vous tuer, vous tombez inconscient. Cette inconscience prend fin si
vous regagnez des points de vie.



#### Jets de sauvegarde contre la mort

Chaque fois que vous commencez votre tour avec 0 points de vie, vous
devez effectuer un jet de sauvegarde spécial, appelé jet de sauvegarde
contre la mort, pour déterminer si vous vous rapprochez de la mort ou si
vous vous accrochez à la vie. Contrairement aux autres jets de
sauvegarde, celui-ci n'est pas lié à un score de capacité. Vous êtes
maintenant entre les mains du destin, aidé uniquement par des sorts et
des caractéristiques qui améliorent vos chances de réussir un jet de
sauvegarde.

Lancez un d20. Si le résultat est de 10 ou plus, vous réussissez. Sinon,
vous échouez. Un succès ou un échec n'a aucun effet en soi. À votre
troisième réussite, vous devenez stable (voir ci-dessous). Lors de votre
troisième échec, vous mourrez. Les succès et les échecs n'ont pas
besoin d'être consécutifs ; gardez la trace des deux jusqu'à ce que
vous obteniez trois cartes identiques. Le nombre des deux est remis à
zéro lorsque vous regagnez des points de vie ou devenez stable.

***Jet de 1 ou 20.*** Lorsque vous effectuez un jet de sauvegarde contre
la mort et obtenez un 1 sur le d20, cela compte comme deux échecs. Si
vous obtenez un 20 sur le d20, vous regagnez 1 point de vie.

***Dégâts à 0 points de vie.*** Si vous subissez des dégâts alors que
vous avez 0 points de vie, vous subissez un échec au jet de sauvegarde
contre la mort. Si les dégâts sont dus à un coup critique, vous subissez
deux échecs à la place. Si les dégâts sont égaux ou supérieurs à votre
maximum de points de vie, vous subissez une mort instantanée.



#### Stabiliser une créature

La meilleure façon de sauver une créature avec 0 points de vie est de la
guérir. Si la guérison n'est pas possible, la créature peut au moins
être stabilisée afin de ne pas être tuée par un jet de sauvegarde contre
la mort raté.

Vous pouvez utiliser votre action pour administrer les premiers soins à
une créature inconsciente et tenter de la stabiliser, ce qui nécessite
un test de Sagesse (Médecine) DC 10 réussi.

Une créature **stable** ne fait pas de jet de sauvegarde contre la mort,
même si elle a 0 points de vie, mais elle reste inconsciente. La
créature cesse d'être stable, et doit recommencer à faire des jets de
sauvegarde contre la mort, si elle subit des dégâts. Une créature stable
qui n'est pas guérie regagne 1 point de vie après 1d4 heures.



#### Les monstres et la mort

La plupart des MJ font mourir un monstre à l'instant où il tombe à 0
points de vie, plutôt que de le faire tomber inconscient et faire des
jets de sauvegarde contre la mort.

Les méchants puissants et les personnages spéciaux non joueurs sont des
exceptions communes ; le MJ peut les faire tomber inconscients et suivre
les mêmes règles que les personnages joueurs.




### Déblocage d'une créature

Parfois, un attaquant souhaite neutraliser un ennemi, plutôt que de lui
porter un coup mortel. Lorsqu'un attaquant réduit une créature à 0
points de vie avec une attaque de mêlée, il peut la débloquer.
L'attaquant peut faire ce choix au moment où les dégâts sont infligés.
La créature tombe inconsciente et est stable.



### Points de vie temporaires

Certains sorts et capacités spéciales confèrent des points de vie
temporaires à une créature. Les points de vie temporaires ne sont pas
des points de vie réels ; ils constituent un tampon contre les dégâts,
une réserve de points de vie qui vous protège des blessures.

Lorsque vous avez des points de vie temporaires et que vous subissez des
dégâts, les points de vie temporaires sont perdus en premier, et les
dégâts restants sont reportés sur vos points de vie normaux. Par
exemple, si vous avez 5 points de vie temporaires et que vous subissez 7
dégâts, vous perdez les points de vie temporaires et subissez ensuite 2
dégâts.

Comme les points de vie temporaires sont distincts de vos points de vie
réels, ils peuvent dépasser votre maximum de points de vie. Un
personnage peut donc avoir un maximum de points de vie et recevoir des
points de vie temporaires.

La Guérison ne peut pas restaurer les points de vie temporaires, et ils
ne peuvent pas être additionnés. Si vous avez des points de vie
temporaires et que vous en recevez d'autres, vous décidez de garder
ceux que vous avez ou de gagner les nouveaux. Par exemple, si un sort
vous accorde 12 points de vie temporaires alors que vous en avez déjà
10, vous pouvez avoir 12 ou 10, mais pas 22.

Si vous avez 0 points de vie, recevoir des points de vie temporaires ne
vous rend pas votre conscience et ne vous stabilise pas. Ils peuvent
toujours absorber les dégâts qui vous sont infligés pendant que vous
êtes dans cet état, mais seule une véritable guérison peut vous sauver.

À moins qu'une caractéristique qui vous accorde des points de vie
temporaires n'ait une durée, ils durent jusqu'à ce qu'ils soient
épuisés ou que vous terminiez un repos long.




## Combattant monté

Un chevalier qui se lance dans la bataille sur un cheval de guerre, un
magicien qui lance des sorts depuis le dos d'un griffon ou un clerc qui
s'envole dans le ciel sur un pégase profitent tous des avantages de la
vitesse et de la mobilité qu'offre une monture.

Une créature volontaire d'au moins une taille de plus que vous et dotée
d'une anatomie appropriée peut servir de monture, selon les règles
suivantes.


### Montage et démontage

Une fois pendant votre déplacement, vous pouvez monter une créature qui
se trouve à moins de 1,5 m de vous ou descendre de cheval. Cela vous
coûte une quantité de mouvement égale à la moitié de votre vitesse. Par
exemple, si votre vitesse est de 30 pieds, vous devez dépenser 15 pieds
de mouvement pour monter un cheval. Par conséquent, vous ne pouvez pas
le monter si vous n'avez plus 15 pieds de mouvement ou si votre vitesse
est de 0.

Si un effet déplace votre monture contre sa volonté alors que vous êtes
dessus, vous devez réussir un jet de sauvegarde de Dextérité DC 10 ou
tomber de la monture, atterrissant à plat ventre dans un espace situé à
moins de 1,5 m de celle-ci. Si vous êtes débloqué alors que vous êtes à
cheval, vous devez effectuer le même jet de sauvegarde.

Si votre monture est mise au sol, vous pouvez utiliser votre réaction
pour en descendre au moment où elle tombe et atterrir sur vos pieds.
Sinon, vous êtes mis à pied et vous tombez à plat ventre dans un espace
situé à moins de 1,5 m de celle-ci.



### Contrôle d'un support

Lorsque vous êtes monté, vous avez deux options. Vous pouvez soit
contrôler la monture, soit lui permettre d'agir indépendamment. Les
créatures intelligentes, comme les dragons, agissent indépendamment.

Vous ne pouvez contrôler une monture que si elle a été entraînée à
accepter un cavalier. Les chevaux domestiques, les ânes et les créatures
similaires sont supposés avoir reçu un tel entraînement. L'initiative
d'une monture contrôlée change pour correspondre à la vôtre lorsque
vous la montez. Elle se déplace selon vos instructions et n'a que trois
possibilités d'action : Sprint, Désengagement, et Esquive. Une monture
contrôlée peut se déplacer et agir même pendant le tour où vous la
montez.

Une monture indépendante conserve sa place dans l'ordre d'initiative.
Le fait de porter un cavalier n'impose aucune restriction sur les
actions que la monture peut entreprendre, et elle se déplace et agit
comme elle le souhaite. Elle peut fuir le combat, se précipiter pour
attaquer et dévorer un ennemi gravement blessé, ou agir autrement contre
vos souhaits.

Dans les deux cas, si la monture provoque une attaque d'opportunité
alors que vous êtes dessus, l'attaquant peut vous cibler ou cibler la
monture.




## Combat sous-marin

Lorsque les aventuriers poursuivent les Sahuagin jusqu'à leurs maisons
sous-marines, combattent les requins dans une ancienne épave ou se
retrouvent dans une salle de donjon inondée, ils doivent se battre dans
un environnement difficile. Sous l'eau, les règles suivantes
s'appliquent.

Lors d'une **attaque avec une arme de mêlée**, une créature qui n'a
pas de vitesse de nage (naturelle ou accordée par la magie) a un
désavantage au jet d'attaque, sauf si l'arme est une dague, un
javelot, une épée courte, une lance ou un trident.

Une attaque avec une **arme à distance** rate automatiquement une cible
située au-delà de la portée normale de l'arme. Même contre une cible à
portée normale, le jet d'attaque a un désavantage, sauf si l'arme est
une arbalète, un filet ou une arme lancée comme un javelot (y compris
une lance, un trident ou une fléchette).

Les créatures et les objets qui sont entièrement immergés dans l'eau
ont une résistance aux dégâts du feu.


