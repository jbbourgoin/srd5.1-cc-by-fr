
# Classes


## Barbare


### Capacités de la classe

  ---------------------------------------------------------------------------
   Niveau      Bonus de     Capacités                    Rages     Dégâts de
               maîtrise                                              Rage
            supplémentaire                                        
  -------- ---------------- -------------------------- ---------- -----------
    1er           +2        Rage, Défense sans armure      2          +2

    2ème          +2        Attaque téméraire, Sens du     2          +2
                            danger                                

    3ème          +2        Voie primitive                 3          +2

    4ème          +2        Amélioration de la valeur      3          +2
                            de caractéristiques                   

    5ème          +3        Attaque supplémentaire,        3          +2
                            Mouvement rapide                      

    6ème          +3        Caractéristique de la voie     4          +2

    7ème          +3        Instinct sauvage               4          +2

    8ème          +3        Amélioration de la valeur      4          +2
                            de caractéristiques                   

    9ème          +4        Critique sauvage (1 dé)        4          +3

   10ème          +4        Caractéristique de la voie     4          +3

   11ème          +4        Rage implacable                4          +3

   12ème          +4        Amélioration de la valeur      5          +3
                            de caractéristiques                   

   13ème          +5        Critique sauvage (2 dés)       5          +3

   14ème          +5        Caractéristique de la voie     5          +3

   15ème          +5        Rage persistante               5          +3

   16ème          +5        Amélioration de la valeur      5          +4
                            de caractéristiques                   

   17ème          +6        Critique sauvage (3 dés)       6          +4

   18ème          +6        Puissance indomptable          6          +4

   19ème          +6        Amélioration de la valeur      6          +4
                            de caractéristiques                   

   20ème          +6        Champion primitif           Illimité      +4
  ---------------------------------------------------------------------------

  : Le barbare

En tant que barbare, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d12 par niveau de barbare.

**Points de vie au 1er niveau :** 12 + votre modificateur de
Constitution

**Points de vie à des niveaux plus élevés :** 1d12 (ou 7) + votre
modificateur de Constitution par niveau de barbare après le 1er



#### Maîtrises

**Blindage :** armure légère, armure moyenne, boucliers.

**Armes :** Armes courantes, armes martiales.

**Outils :** Aucun

**Jets de sauvegarde :** Force, Constitution

**Compétences :** Choisissez-en deux parmi Dressage, Athlétisme,
Intimidation, Nature, Perception et Survie.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une hache à deux mains ou*(b*) toute arme de mêlée martiale.
-   *(a)* deux hachettes ou*(b*) toute arme courante.
-   Un sac d'explorateur et quatre javelots.




### Rage

Au combat, vous vous battez avec une férocité primitive. À votre tour,
vous pouvez entrer en rage comme action bonus.

Pendant votre rage, vous bénéficiez des avantages suivants si vous ne
portez pas d'armure lourde :

-   Vous avez un avantage sur les tests de Force et les jets de
    sauvegarde de Force.
-   Lorsque vous effectuez une attaque avec une arme de corps-à-corps en
    utilisant la Force, vous bénéficiez d'un bonus au jet de dégâts qui
    augmente à mesure que vous gagnez des niveaux en tant que barbare,
    comme indiqué dans la colonne Dégâts de Rage de la table des
    barbares.
-   Vous avez une résistance aux dégâts de contondant, de perforant et
    de tranchant.

Si vous êtes capable de lancer des sorts, vous ne pouvez pas les lancer
ou vous concentrer sur eux pendant le Rage.

Votre Rage dure 1 minute. Elle se termine prématurément si vous êtes
débloqué ou si votre tour se termine et que vous n'avez pas attaqué une
créature hostile depuis votre dernier tour ou subi de dégâts depuis.
Vous pouvez également mettre fin à votre rage à votre tour en tant
qu'action bonus.

Une fois que vous avez fait rage le nombre de fois indiqué pour votre
niveau de barbare dans la colonne Rages de la table des barbares, vous
devez terminer un repos long avant de pouvoir faire rage à nouveau.



### Défense sans armure

Lorsque vous ne portez pas d'armure, votre classe d'armure est égale à
10 + votre modificateur de Dextérité + votre modificateur de
Constitution. Vous pouvez utiliser un bouclier et bénéficier de cet
avantage.



### Attaque téméraire

À partir du 2e niveau, vous pouvez mettre de côté tout souci de défense
pour attaquer avec un désespoir féroce. Lorsque vous effectuez votre
première attaque à votre tour, vous pouvez décider d'attaquer de
manière téméraire. Cela vous donne un avantage sur les jets d'attaque
d'armes de mêlée utilisant la Force pendant ce tour, mais les jets
d'attaque contre vous ont un avantage jusqu'à votre prochain tour.



### Sens du danger

Au 2e niveau, vous obtenez un sens aigu de l'irrégularité des choses à
proximité, ce qui vous donne un avantage lorsque vous esquivez un
danger.

Vous avez un avantage aux jets de sauvegarde de Dextérité contre les
effets que vous pouvez voir, comme les pièges et les sorts. Pour
bénéficier de cet avantage, vous ne devez pas être aveuglé, assourdi ou
frappé d'incapacité.



### Voie primitive

Au 3e niveau, vous choisissez une voie  qui façonne la nature de votre
rage. Votre choix vous confère des capacités au 3e niveau, puis aux 6e,
10e et 14e niveaux.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Attaque supplémentaire

À partir du 5e niveau, vous pouvez attaquer deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.



### Mouvement rapide

À partir du 5e niveau, votre vitesse augmente de 3 mètres lorsque vous
ne portez pas d'armure lourde.



### Instinct sauvage

Au 7ème niveau, votre instinct est si aiguisé que vous avez un avantage
sur les jets d'initiative.

De plus, si vous êtes surpris au début du combat et que vous n'êtes pas
frappé d'incapacité, vous pouvez agir normalement à votre premier tour,
mais seulement si vous entrez en rage avant de faire quoi que ce soit
d'autre à ce tour.



### Critique sauvage

À partir du 9e niveau, vous pouvez lancer un dé de dégâts d'arme
supplémentaire pour déterminer les dégâts supplémentaires infligés par
un coup critique avec une attaque de mêlée.

Cela passe à deux dés supplémentaires au 13ème niveau et à trois dés
supplémentaires au 17ème niveau.



### Rage implacable

À partir du 11e niveau, votre rage peut vous permettre de continuer à
vous battre malgré des blessures graves. Si vous tombez à 0 point de vie
pendant que vous êtes en rage et que vous ne mourez pas tout de suite,
vous pouvez effectuer un jet de sauvegarde de Constitution DC 10. Si
vous réussissez, vous tombez à 1 point de vie à la place.

Chaque fois que vous utilisez cette fonction après la première, le DC
augmente de 5. Lorsque vous terminez un repos court ou long, le DC se
réinitialise à 10.



### Rage persistante

À partir du 15e niveau, votre rage est si féroce qu'elle ne se termine
prématurément que si vous tombez inconscient ou si vous choisissez d'y
mettre fin.



### Puissance indomptable

À partir du 18e niveau, si votre total pour un test de Force est
inférieur à votre score de Force, vous pouvez utiliser ce score à la
place du total.



### Champion primitif

Au 20e niveau, vous incarnez la puissance des espaces sauvages. Vos
scores de Force et de Constitution augmentent de 4. Votre maximum pour
ces scores est maintenant de 24.





### Voies du Barbare ###



#### Voie du berserker ####

Pour certains barbares, la rage est un moyen d'arriver à ses fins,
c'est-à-dire la violence. La Voie du berserker est un chemin de fureur
sans limites, baigné de sang. Lorsque vous entrez dans la rage du
Berserker, vous vibrez dans le chaos de la bataille, sans vous soucier
de votre santé ou de votre bien-être.



##### Frénésie #####

À partir du moment où vous choisissez cette voie au 3e niveau, vous
pouvez entrer en frénésie lorsque vous êtes en rage. Dans ce cas,
pendant la durée de votre rage, vous pouvez effectuer une seule attaque
à l'arme de corps-à-corps en tant qu'action bonus à chacun de vos
tours suivant celui-ci. Lorsque votre rage prend fin, vous subissez un
niveau d'étouffement .




##### Rage aveugle #####

À partir du 6e niveau, vous ne pouvez plus être charmé ou effrayé
pendant votre rage. Si vous êtes charmé ou effrayé lorsque vous entrez
en rage, l'effet est suspendu pour la durée de la rage.




##### Présence intimidante #####

À partir du 10e niveau, vous pouvez utiliser votre action pour effrayer
quelqu'un avec votre présence menaçante. Pour ce faire, choisissez une
créature que vous pouvez voir dans un rayon de 30 pieds autour de vous.
Si la créature peut vous voir ou vous entendre, elle doit réussir un jet
de sauvegarde de Sagesse (DC égal à 8 + votre bonus de maîtrise + votre
modificateur de Charisme) ou être effrayée par vous jusqu'à la fin de
votre prochain tour. Lors des tours suivants, vous pouvez utiliser votre
action pour prolonger la durée de cet effet sur la créature effrayée
jusqu'à la fin de votre prochain tour. Cet effet prend fin si la
créature termine son tour hors de sa ligne de vue ou à plus de 60 pieds
de vous.

Si la créature réussit son jet de sauvegarde, vous ne pouvez plus
utiliser cette caractéristique sur cette créature pendant 24 heures.




##### Représailles #####

À partir du 14e niveau, lorsque vous subissez des dégâts d'une créature
située à moins de 1,5 mètre de vous, vous pouvez utiliser votre réaction
pour effectuer une attaque avec une arme de corps-à-corps contre cette
créature.





## Barde


### Capacités de la classe

  -------------------------------------------------------------------------------
   Niveau      Maîtrise     Capacités                          Tours de   Sorts
            supplémentaire                                       magie    connus
                                                                connus   
  -------- ---------------- ---------------------------------- --------- --------
    1er           +2        Lanceur de sorts, Inspiration du       2        4
                            barde (d6)                                

    2ème          +2        Touche-à-tout, Chant du Repos (d6)     2        5

    3ème          +2        Collège des bardes, Expertise       2        6

    4ème          +2        Amélioration de la valeur de           3        7
                            caractéristiques                             

    5ème          +3        Inspiration du barde (d8),          3        8
                            Source d'inspiration                        

    6ème          +3        Contre-charme, caractéristique du      3        9
                            Collège des bardes                        

    7ème          +3        \-                                     3        10

    8ème          +3        Amélioration de la valeur de           3        11
                            caractéristiques                             

    9ème          +4        Chant du Repos (d8)                    3        12

   10ème          +4        Inspiration du barde (d10),         4        14
                            Expertise, Secrets magiques                  

   11ème          +4        \-                                     4        15

   12ème          +4        Amélioration de la valeur de           4        15
                            caractéristiques                             

   13ème          +5        Chant du Repos (d10)                   4        16

   14ème          +5        Secrets magiques, reportage du         4        18
                            Collège des bardes                        

   15ème          +5        Inspiration du barde (d12)          4        19

   16ème          +5        Amélioration de la valeur de           4        19
                            caractéristiques                             

   17ème          +6        Chant du Repos (d12)                   4        20

   18ème          +6        Secrets magiques                       4        22

   19ème          +6        Amélioration de la valeur de           4        22
                            caractéristiques                             

   20ème          +6        Inspiration suprême                    4        22
  -------------------------------------------------------------------------------

  : Le Barde

En tant que Barde, vous gagnez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau de barde.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau de barde après le 1er degré



#### Maîtrises

**Armure :** Armure légère.

**Armes :** Armes simples, arbalètes, épées longues, rapières, épées
courtes.

**Outils :** Trois instruments de musique de votre choix

**Jets de sauvegarde :** Dextérité, Charisme

**Compétences :** Choisissez-en trois



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une rapière,*(b*) une épée longue, ou*(c*) toute arme
    courante.
-   *(a)* la valise d'un diplomate ou*(b*) la valise d'un artiste.
-   *(a)* un luth ou*(b*) tout autre instrument de musique
-   Une armure en cuir et une dague.




### Incantation

Vous avez appris à démêler et à remodeler le tissu de la réalité en
harmonie avec vos souhaits et votre musique. Vos sorts font partie de
votre vaste répertoire, une magie que vous pouvez adapter à différentes
situations.


#### Tours de magie

Vous connaissez deux tours de magie de votre choix dans la liste des
sorts de barde. Vous apprenez d'autres tours de magie de barde de votre
choix à des niveaux supérieurs, comme indiqué dans la colonne Tours de
magie connus de la table des bardes.



#### Emplacements de sorts

  -----------------------------------------------------------------------------------
  Niveau de   Niveau                                                          
   Barde    des                                                            
              sorts                                                           
  ---------- -------- ------- ------- ------- ------- ------- ------- ------- -------
               1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

     1er        2       \-      \-      \-      \-      \-      \-      \-      \-

     2ème       3       \-      \-      \-      \-      \-      \-      \-      \-

     3ème       4        2      \-      \-      \-      \-      \-      \-      \-

     4ème       4        3      \-      \-      \-      \-      \-      \-      \-

     5ème       4        3       2      \-      \-      \-      \-      \-      \-

     6ème       4        3       3      \-      \-      \-      \-      \-      \-

     7ème       4        3       3       1      \-      \-      \-      \-      \-

     8ème       4        3       3       2      \-      \-      \-      \-      \-

     9ème       4        3       3       3       1      \-      \-      \-      \-

    10ème       4        3       3       3       2      \-      \-      \-      \-

    11ème       4        3       3       3       2       1      \-      \-      \-

    12ème       4        3       3       3       2       1      \-      \-      \-

    13ème       4        3       3       3       2       1       1      \-      \-

    14ème       4        3       3       3       2       1       1      \-      \-

    15ème       4        3       3       3       2       1       1       1      \-

    16ème       4        3       3       3       2       1       1       1      \-

    17ème       4        3       3       3       2       1       1       1       1

    18ème       4        3       3       3       3       1       1       1       1

    19ème       4        3       3       3       3       2       1       1       1

    20ème       4        3       3       3       3       2       2       1       1
  -----------------------------------------------------------------------------------

  : Emplacements de sorts pour les bardes par niveau

Le tableau Emplacements de sorts par niveau pour les bardes indique le
nombre d'emplacements de sorts dont vous disposez pour lancer vos sorts
de 1er niveau et plus. Pour lancer l'un de ces sorts, vous devez
dépenser un emplacement du niveau du sort ou d'un niveau supérieur.
Vous récupérez tous les emplacements de sorts dépensés lorsque vous
terminez un long repos.

Par exemple, si vous connaissez le sort de 1er niveau *soigner les
blessures* et que vous disposez d'un emplacement de 1er
niveau et d'un emplacement de 2ème niveau, vous pouvez lancer le sort
*soigner les blessures* en utilisant l'un ou l'autre
de ces emplacements.



#### Sorts connus de 1er niveau et plus

Vous connaissez quatre sorts de 1er niveau de votre choix dans la liste
des sorts pour bardes.

La colonne Sorts connus de la table de Barde indique quand vous
apprenez d'autres sorts de barde de votre choix. Chacun de ces sorts
doit être d'un niveau pour lequel vous avez des emplacements de sorts,
comme indiqué sur la table. Par exemple, lorsque vous atteignez le 3e
niveau dans cette classe, vous pouvez apprendre un nouveau sort de 1er
ou 2e niveau.

De plus, lorsque vous gagnez un niveau dans cette classe, vous pouvez
choisir un des sorts de barde que vous connaissez et le remplacer par un
autre sort de la liste des sorts de barde, qui doit également être d'un
niveau pour lequel vous avez des emplacements de sorts.



#### Caractéristique d'Incantation

Le Charisme est votre caractéristique d'incantation pour vos sorts de
Barde. Votre magie vient du cœur et de l'âme que vous mettez dans la
représentation de votre musique ou de votre oraison. Vous utilisez votre
Charisme chaque fois qu'un sort fait référence à votre caractéristique
d'incantation. De plus, vous utilisez votre modificateur de Charisme
pour déterminer le jet de sauvegarde d'un sort de barde que vous lancez
et pour effectuer un jet d'attaque avec un tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Charisme.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Charisme.
  -----------------------------------------------------------------------



#### Lancer de rituel

Vous pouvez lancer n'importe quel sort de barde que vous connaissez
comme un rituel si ce sort a l'étiquette rituel.



#### Lanceur de sorts Focus

Vous pouvez utiliser un instrument de musique comme foyer d'incantation pour
vos sorts de barde.




### Inspiration du barde

Vous pouvez inspirer les autres par des paroles ou de la musique
émouvantes. Pour ce faire, vous utilisez une action bonus à votre tour
pour choisir une créature autre que vous-même dans un rayon de 60 pieds
de vous et qui peut vous entendre. Cette créature gagne un dé
d'Inspiration du barde, un d6.

Une fois dans les 10 minutes suivantes, la créature peut lancer le dé et
ajouter le nombre obtenu à un test de capacité, un jet d'attaque ou un
jet de sauvegarde qu'elle effectue. La créature peut attendre d'avoir
lancé le d20 pour décider d'utiliser le dé d'Inspiration du barde,
mais elle doit décider avant que le MJ ne dise si le jet réussit ou
échoue. Une fois le dé d'Inspiration du barde lancé, il est perdu.
Une créature ne peut avoir qu'un seul dé d'inspiration du barde à
la fois.

Vous pouvez utiliser cette caractéristique un nombre de fois égal à
votre modificateur de Charisme (au minimum une fois). Vous récupérez
toutes les utilisations dépensées lorsque vous terminez un repos long.

Votre dé d'Inspiration du barde change lorsque vous atteignez
certains niveaux dans cette classe. Le dé devient un d8 au 5e niveau, un
d10 au 10e niveau et un d12 au 15e niveau.



### Touche-à-tout

A partir du 2ème niveau, vous pouvez ajouter la moitié de votre bonus de
maîtrise, arrondi à l'inférieur, à tout test d'aptitude que vous
effectuez et qui n'inclut pas déjà votre bonus de maîtrise.



### Chant du Repos

À partir du 2e niveau, vous pouvez utiliser une musique apaisante ou une
oraison pour aider à revitaliser vos alliés blessés pendant un court
repos. Si vous ou toute créature amie qui peut entendre votre
représentation regagnez des points de vie à la fin du repos court en
dépensant un ou plusieurs dés de points de vie, chacune de ces créatures
regagne 1d6 points de vie supplémentaires.

Les points de vie supplémentaires augmentent lorsque vous atteignez
certains niveaux dans cette classe : à 1d8 au 9ème niveau, à 1d10 au
13ème niveau, et à 1d12 au 17ème niveau.



### Collège des bardes

Au 3e niveau, vous vous plongez dans les techniques avancées d'un
collège de bardes de votre choix. Votre choix vous
confère des capacités au 3e niveau, puis au 6e et au 14e niveau.



### Expertise

Au 3e niveau, choisissez deux de vos compétences. Votre bonus de
maîtrise est doublé pour tout test d'aptitude que vous effectuez et qui
utilise l'une ou l'autre des compétences choisies.

Au 10ème niveau, vous pouvez choisir deux autres compétences pour
bénéficier de cet avantage.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Source d'inspiration

À partir du 5e niveau, vous regagnez toutes vos utilisations de
l'Inspiration du barde lorsque vous terminez un repos court ou long.



### Contre-charme

Au 6e niveau, vous gagnez la capacité d'utiliser des notes de musique
ou des mots de pouvoir pour perturber les effets d'influence sur
l'esprit. En tant qu'action, vous pouvez commencer une représentation
qui dure jusqu'à la fin de votre prochain tour. Pendant ce temps, vous
et toutes les créatures amies situées à moins de 10 mètres de vous avez
un avantage aux jets de sauvegarde contre l'effroi ou le charme. Une
créature doit être capable de vous entendre pour bénéficier de cet
avantage. La Représentation se termine prématurément si vous êtes frappé
d'incapacité ou réduit au silence ou si vous y mettez fin
volontairement (aucune action requise).



### Secrets magiques

Au 10ème niveau, vous avez pillé les connaissances magiques d'un large
éventail de disciplines. Choisissez deux sorts dans n'importe quelle
classe, y compris celle-ci. Le sort que vous choisissez doit être d'un
niveau que vous pouvez lancer, comme indiqué sur la table de Barde,
ou un cantrip.

Les sorts choisis comptent comme des sorts de barde pour vous et sont
inclus dans le nombre de la colonne Sorts connus de la table de
Barde.

Vous apprenez deux sorts supplémentaires de n'importe quelle classe au
14ème niveau et à nouveau au 18ème niveau.



### Inspiration suprême

Au 20e niveau, lorsque vous effectuez un jet d'initiative et que vous
n'avez plus d'utilisations d'Inspiration du barde, vous regagnez
une utilisation.





### Collèges des Bardes ###



#### Collège des Traditions ####

Les bardes du Collège des Traditions en savent un peu plus sur la
plupart des choses, recueillant des bribes de connaissances à partir de
sources aussi diverses que des tomes savants et des contes paysans.
Qu'ils chantent des ballades populaires dans les tavernes ou des
compositions élaborées dans les cours royales, ces bardes utilisent
leurs dons pour envoûter le public. Lorsque les applaudissements se
taisent, les membres de l'auditoire peuvent être amenés à remettre en
question tout ce qu'ils croyaient être vrai, de leur foi dans le
sacerdoce du temple local à leur loyauté envers le roi.

La loyauté de ces bardes réside dans la recherche de la beauté et de la
vérité, et non dans la fidélité à un monarque ou dans le respect des
principes d'une divinité. Un noble qui garde un tel barde comme héraut
ou conseiller sait que le barde préfère être honnête plutôt que
politique.

Les membres du collège se réunissent dans des bibliothèques et parfois
dans de véritables collèges, avec salles de classe et dortoirs, pour
partager leurs connaissances. Ils se réunissent également à l'occasion
de festivals ou d'affaires d'État, où ils peuvent dénoncer la
corruption, démêler les mensonges et se moquer des figures d'autorité
qui se croient supérieures.



##### Maîtrise supplémentaire #####

Lorsque vous rejoignez le Collège des Traditions au 3e niveau, vous
gagnez la maîtrise de trois compétences de votre choix.




##### Proposants cuisants #####

Au 3e niveau également, vous apprenez à utiliser votre esprit pour
distraire, confondre et saper la confiance et la compétence des autres.
Lorsqu'une créature que vous pouvez voir à moins de 60 pieds de vous
fait un jet d'attaque, un test de capacité ou un jet de dégâts, vous
pouvez utiliser votre réaction pour dépenser une de vos utilisations
d'Inspiration du barde, en lançant un dé d'Inspiration du barde
et en soustrayant le nombre obtenu du jet de la créature. Vous pouvez
choisir d'utiliser cette caractéristique après que la créature a
effectué son jet, mais avant que le MJ ne détermine si le jet d'attaque
ou le test de capacité réussit ou échoue, ou avant que la créature
n'inflige ses dégâts. La créature est immunisée si elle ne peut pas
vous entendre ou si elle est immunisée contre le charme.




##### Secrets magiques supplémentaires #####

Au 6e niveau, vous apprenez deux sorts de votre choix dans n'importe
quelle classe. Le sort que vous choisissez doit être d'un niveau que
vous pouvez lancer, comme indiqué sur la table de Barde, ou un
cantrip. Les sorts choisis comptent comme des sorts de barde pour vous
mais ne sont pas comptabilisés dans le nombre de sorts de barde que vous
connaissez.




##### Compétence hors-pair #####

A partir du 14ème niveau, lorsque vous effectuez un test d'aptitude,
vous pouvez dépenser une utilisation d'Inspiration du barde. Lancez
un dé d'inspiration bardique et ajoutez le nombre obtenu à votre test
de capacité. Vous pouvez choisir de le faire après avoir lancé le dé
pour le test d'aptitude, mais avant que le MJ ne vous dise si vous avez
réussi ou échoué.





## Clerc


### Capacités de la classe

  -----------------------------------------------------------------------------
   Niveau      Bonus de     Capacités                                  Tours de
               maîtrise                                                 magie
            supplémentaire                                              connus
  -------- ---------------- ------------------------------------------ --------
    1er           +2        Lanceur de sorts, Domaine divin               3

    2ème          +2        Canalisation divine (1/rest),                 3
                            caractéristique Domaine divin              

    3ème          +2        \-                                            3

    4ème          +2        Amélioration de la valeur de                  4
                            caractéristiques                           

    5ème          +3        Destruction des morts-vivants (CR 1/2)        4

    6ème          +3        Canalisation divine (2/rest),                 4
                            caractéristique Domaine divin              

    7ème          +3        \-                                            4

    8ème          +3        Amélioration de la valeur de                  4
                            caractéristiques, Destruction des          
                            morts-vivants (CR 1), caractéristique du   
                            Domaine divin.                             

    9ème          +4        \-                                            4

   10ème          +4        Intervention divine                           4

   11ème          +4        Destruction des morts-vivants (CR 2)          5

   12ème          +4        Amélioration de la valeur de                  5
                            caractéristique                            

   13ème          +5        \-                                            5

   14ème          +5        Destruction des morts-vivants (CR 3)          5

   15ème          +5        \-                                            5

   16ème          +5        Amélioration de la valeur de                  5
                            caractéristiques                           

   17ème          +6        Destruction des morts-vivants (CR 4),         5
                            caractéristique du Domaine divin           

   18ème          +6        Canalisation divine (3/rest)                  5

   19ème          +6        Amélioration de la valeur de                  5
                            caractéristiques                           

   20ème          +6        Amélioration de l'Intervention divine        5
  -----------------------------------------------------------------------------

  : Le clerc

En tant que clerc, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau de clerc.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau de clerc après le 1er



#### Maîtrises

**Blindage :** armure légère, armure moyenne, boucliers.

**Armes :** Armes courantes

**Outils :** Aucun

**Jets de sauvegarde :** Sagesse, Charisme

**Compétences :** Choisissez-en deux parmi l'Histoire, l'Intuition, la
Médecine, la Persuasion et la Religion.




### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une masse ou*(b*) un marteau de guerre (si vous en avez la
    maîtrise).
-   *(a)* cotte de mailles,*(b*) armure de cuir, ou*(c*) cotte de
    mailles (si vous en avez la maîtrise).
-   *(a)* une arbalète légère et 20 carreaux ou*(b*) toute arme
    courante.
-   *(a)* le sac d'un prêtre ou*(b*) le sac d'un explorateur.
-   Un bouclier et un symbole sacré



### Incantation

En tant que conduit de la puissance divine, vous pouvez lancer des sorts
de clercs.


#### Tours de magie

Au 1er niveau, vous connaissez trois cantrips de votre choix dans la
liste des sorts de clercs. Vous apprenez d'autres tours de magie de
clercs de votre choix à des niveaux supérieurs, comme indiqué dans la
colonne Tours de magie connus de la table des clercs.



#### Préparer et lancer des sorts

  ---------------------------------------------------------------------------------
   Niveau   Niveau                                                          
     de      des                                                            
   clercs   sorts                                                           
  -------- -------- ------- ------- ------- ------- ------- ------- ------- -------
             1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

    1er       2       \-      \-      \-      \-      \-      \-      \-      \-

    2ème      3       \-      \-      \-      \-      \-      \-      \-      \-

    3ème      4        2      \-      \-      \-      \-      \-      \-      \-

    4ème      4        3      \-      \-      \-      \-      \-      \-      \-

    5ème      4        3       2      \-      \-      \-      \-      \-      \-

    6ème      4        3       3      \-      \-      \-      \-      \-      \-

    7ème      4        3       3       1      \-      \-      \-      \-      \-

    8ème      4        3       3       2      \-      \-      \-      \-      \-

    9ème      4        3       3       3       1      \-      \-      \-      \-

   10ème      4        3       3       3       2      \-      \-      \-      \-

   11ème      4        3       3       3       2       1      \-      \-      \-

   12ème      4        3       3       3       2       1      \-      \-      \-

   13ème      4        3       3       3       2       1       1      \-      \-

   14ème      4        3       3       3       2       1       1      \-      \-

   15ème      4        3       3       3       2       1       1       1      \-

   16ème      4        3       3       3       2       1       1       1      \-

   17ème      4        3       3       3       2       1       1       1       1

   18ème      4        3       3       3       3       1       1       1       1

   19ème      4        3       3       3       3       2       1       1       1

   20ème      4        3       3       3       3       2       2       1       1
  ---------------------------------------------------------------------------------

  : Emplacements de sorts de clercs par niveau

Le tableau Emplacements de sorts par niveau pour les clercs indique le
nombre d'emplacements de sorts dont vous disposez pour lancer vos sorts
de 1er niveau et plus. Pour lancer l'un de ces sorts, vous devez
dépenser un emplacement du niveau du sort ou d'un niveau supérieur.
Vous récupérez tous les emplacements de sorts dépensés lorsque vous
terminez un long repos.

Vous préparez la liste des sorts de clercs qui sont disponibles pour
vous, en choisissant dans la liste des sorts de clercs. Vous choisissez
alors un nombre de sorts de clercs égal à votre modificateur de
Sagesse + votre niveau de clerc (au moins un sort). Les sorts doivent
être d'un niveau pour lequel vous disposez d'emplacements de sorts.

Par exemple, si vous êtes un clerc de 3e niveau, vous avez quatre
emplacements de sorts de 1er niveau et deux de 2e niveau. Avec une
Sagesse de 16, votre liste de sorts préparés peut inclure six sorts de
1er ou 2ème niveau, dans n'importe quelle combinaison. Si vous préparez
le sort de 1er niveau *soigner les soins*, vous pouvez
le lancer en utilisant un emplacement de 1er niveau ou de 2ème niveau.
Lancer le sort ne le retire pas de votre liste de sorts préparés.

Vous pouvez modifier votre liste de sorts préparés lorsque vous terminez
un repos long. La préparation d'une nouvelle liste de sorts de clercs
nécessite du temps passé en prière et en méditation : au moins 1 minute
par niveau de sort pour chaque sort de votre liste.



#### Caractéristique d'Incantation

La Sagesse est votre caractéristique d'incantation pour vos sorts de
clerc. La puissance de vos sorts provient de votre dévotion à votre
divinité. Vous utilisez votre Sagesse lorsqu'un sort de clerc fait
référence à votre caractéristique d'incantation. De plus, vous utilisez
votre modificateur de Sagesse pour déterminer le jet de sauvegarde d'un
sort de clerc que vous lancez et pour effectuer un jet d'attaque avec
un tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Sagesse.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Sagesse.
  -----------------------------------------------------------------------



#### Lancer de rituel

Vous pouvez lancer un sort de clerc comme un rituel si ce sort a
l'étiquette rituel et que vous avez le sort préparé.



#### Lanceur de sorts Focus

Vous pouvez utiliser un symbole sacré  comme foyer d'incantation pour
vos sorts de clerc.




### Domaine divin

Choisissez un domaine  lié à
votre divinité. Chaque domaine fournit des exemples de divinités qui lui
sont associées. Votre choix vous octroie des sorts de domaine et
d'autres caractéristiques lorsque vous le choisissez au 1er niveau. Il
vous confère également des moyens supplémentaires d'utiliser
Canalisation divine lorsque vous obtenez cette caractéristique au 2e
niveau, et des avantages supplémentaires aux 6e, 8e et 17e niveaux.


#### Sorts de domaine

Chaque domaine possède une liste de sorts - ses sorts de domaine - que
vous gagnez aux niveaux de clercs indiqués dans la description du
domaine. Une fois que vous obtenez un sort de domaine, vous l'avez
toujours préparé, et il ne compte pas dans le nombre de sorts que vous
pouvez préparer chaque jour.

Si vous disposez d'un sort de domaine qui n'apparaît pas dans la liste
des sorts de clercs, ce sort est néanmoins un sort de clercs pour vous.




### Canalisation divine

Au 2e niveau, vous gagnez la capacité de canaliser l'énergie divine
directement de votre divinité, en utilisant cette énergie pour alimenter
des effets magiques. Vous commencez avec deux effets de ce type :
Renversement des morts-vivants et un effet déterminé par votre domaine.
Certains domaines vous accordent des effets supplémentaires au fur et à
mesure que vous avancez en niveau, comme indiqué dans la description du
domaine.

Lorsque vous utilisez votre Canalisation divine, vous choisissez
l'effet à créer. Vous devez ensuite terminer un repos court ou long
pour utiliser à nouveau votre Canalisation divine.

Certains effets de Canalisation divine nécessitent des jets de
sauvegarde. Lorsque vous utilisez un tel effet de cette classe, le jet
de sauvegarde est égal à votre jet de sauvegarde des sorts de clerc.

À partir du 6e niveau, vous pouvez utiliser votre Canalisation divine
deux fois entre deux repos, et à partir du 18e niveau, vous pouvez
l'utiliser trois fois entre deux repos. Lorsque vous terminez un repos
court ou long, vous récupérez vos utilisations dépensées.


#### Canalisation divine : renvoi des morts-vivants

En tant qu'action, vous présentez votre symbole sacré et prononcez une
prière censurant les morts-vivants. Chaque mort-vivant qui peut vous
voir ou vous entendre à moins de 30 pieds de vous doit effectuer un jet
de sauvegarde de Sagesse. Si la créature échoue à son jet de sauvegarde,
elle est retournée pendant 1 minute ou jusqu'à ce qu'elle subisse des
dégâts.

Une créature transformée doit passer ses tours à essayer de s'éloigner
le plus possible de vous, et elle ne peut pas se déplacer volontairement
vers un espace situé à moins de 30 pieds de vous. Elle ne peut pas non
plus avoir de réactions. Pour son action, elle ne peut utiliser que
l'action Foncer ou essayer d'échapper à un effet qui l'empêche de se
déplacer. S'il n'y a nulle part où se déplacer, la créature peut
utiliser l'action Esquive.




### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Destruction des morts-vivants

À partir du 5e niveau, lorsqu'un mort-vivant rate son jet de sauvegarde
contre votre caractéristique de renvoi des morts-vivants, la créature
est instantanément détruite si son indice de difficulté est inférieur ou
égal à un certain seuil, comme indiqué dans la table Destruction des
morts-vivants.

  --------------------------------------------
    Niveau de    Détruit les morts-vivants de
      clercs     CR\...
  -------------- -----------------------------
       5ème      1/2 ou moins

       8ème      1 ou moins

      11ème      2 ou moins

      14ème      3 ou moins

      17ème      4 ou moins
  --------------------------------------------

  : Destruction des morts-vivants



### Intervention divine

À partir du 10e niveau, vous pouvez demander à votre divinité
d'intervenir en votre faveur lorsque votre besoin est grand.

Implorer l'aide de votre divinité vous demande d'utiliser votre
action. Décrivez l'aide que vous recherchez, et lancez des dés de
percentile. Si vous obtenez un nombre inférieur ou égal à votre niveau
de clerc, votre divinité intervient.

Le MJ choisit la nature de l'intervention ; l'effet de n'importe quel
sort de clerc ou de domaine de clerc serait approprié.

Si votre divinité intervient, vous ne pouvez plus utiliser cette
capacité pendant 7 jours. Sinon, vous pouvez l'utiliser à nouveau après
un long repos.

Au 20e niveau, votre appel à l'intervention réussit automatiquement,
sans jet de dé.





### Domaines du Clerc ###



#### Domaine de la Vie ####

Le Domaine de la Vie se concentre sur l'énergie positive vibrante -
l'une des forces fondamentales de l'univers - qui entretient toute
vie. Les dieux de la vie 
favorisent la vitalité et la santé en guérissant les malades et les
blessés, en prenant soin de ceux qui sont dans le besoin et en chassant
les forces de la mort et de la non-mort. Presque toutes les divinités
non maléfiques peuvent revendiquer une influence sur ce domaine, en
particulier les divinités agricoles (comme Isis, le Daghdha et Déméter),
les dieux du soleil (comme Belenus, Frey et Re-Horakhty), les dieux de
la guérison ou de l'endurance (comme Apollo, Lugh et Diancecht), et les
dieux du foyer et de la communauté (comme Hestia, Hathor et Frigga).

  ----------------------------------------------------------------
    Niveau de    Sorts
      clercs     
  -------------- -------------------------------------------------
       1er       *Bénédiction*, *soins des
                 plaies*.

       3ème      Restauration*partielle*,
                 *Arme spirituelle*

       5ème      Lueur*d'espoir*, Retour *à
                 la vie*

       7ème      Protection contre la*mort*,
                 Gardien de *la foi*

       9ème      *soins de groupe*, *rappel
                 à la vie*
  ----------------------------------------------------------------

  : Sorts du Domaine de la Vie



##### Maîtrise supplémentaire #####

Lorsque vous choisissez ce domaine au 1er niveau, vous gagnez la
maîtrise des armures lourdes.




##### Disciple de la Vie #####

De plus, à partir du 1er niveau, vos sorts de guérison sont plus
efficaces. Chaque fois que vous utilisez un sort de 1er niveau ou plus
pour rendre des points de vie à une créature, celle-ci regagne un nombre
de points de vie supplémentaires égal à 2 + le niveau du sort.




##### Canalisation divine : Maintient de la vie #####

A partir du 2ème niveau, vous pouvez utiliser votre Canalisation divine
pour guérir les personnes gravement blessées.

En action, vous présentez votre symbole sacré et évoquez une énergie de
guérison qui peut restaurer un nombre de points de vie égal à cinq fois
votre niveau de clerc.

Choisissez toutes les créatures situées dans un rayon de 30 pieds autour
de vous, et répartissez ces points de vie entre elles. Cette
caractéristique peut ramener une créature à la moitié de son maximum de
points de vie. Vous ne pouvez pas utiliser cette caractéristique sur un
mort-vivant ou une construction.




##### Guérisseur béni #####

À partir du 6e niveau, les sorts de guérison que vous lancez sur les
autres vous guérissent également. Lorsque vous lancez un sort de 1er
niveau ou plus qui redonne des points de vie à une créature autre que
vous, vous regagnez un nombre de points de vie égal à 2 + le niveau du
sort.




##### Frappe divine #####

Au 8e niveau, vous gagnez la capacité d'infuser vos coups d'arme avec
de l'énergie divine. Une fois à chacun de vos tours, lorsque vous
touchez une créature avec une attaque d'arme, vous pouvez faire en
sorte que l'attaque inflige 1d8 points de dégâts radiants
supplémentaires à la cible. Lorsque vous atteignez le 14e niveau, les
dégâts supplémentaires passent à 2d8.




##### Guérison suprême #####

À partir du 17e niveau, lorsque vous devriez normalement lancer un ou
plusieurs dés pour restaurer des points de vie avec un sort, vous
utilisez à la place le plus grand nombre possible pour chaque dé. Par
exemple, au lieu de restituer 2d6 points de vie à une créature, vous en
restituez 12.





## Druide


### Capacités de la classe

  ----------------------------------------------------------------
   Niveau      Bonus de     Capacités                    Tours de
               maîtrise                                    magie
            supplémentaire                                connus
  -------- ---------------- ---------------------------- ---------
    1er           +2        Druidique, Lanceur de sorts      2

    2ème          +2        Forme sauvage, Cercle            2
                            druidique                    

    3ème          +2        \-                               2

    4ème          +2        Amélioration de la Forme         3
                            sauvage, Amélioration de la  
                            valeur caractéristique       

    5ème          +3        \-                               3

    6ème          +3        Fonctionnement du Cercle         3
                            druidique                    

    7ème          +3        \-                               3

    8ème          +3        Amélioration de la Forme         3
                            sauvage, Amélioration de la  
                            valeur caractéristique       

    9ème          +4        \-                               3

   10ème          +4        Fonctionnement du Cercle         4
                            druidique                    

   11ème          +4        \-                               4

   12ème          +4        Amélioration de la valeur de     4
                            caractéristiques             

   13ème          +5        \-                               4

   14ème          +5        Fonctionnement du Cercle         4
                            druidique                    

   15ème          +5        \-                               4

   16ème          +5        Amélioration de la valeur de     4
                            caractéristiques             

   17ème          +6        \-                               4

   18ème          +6        Jeunesse éternelle,              4
                            Sortilèges bestiaux          

   19ème          +6        Amélioration de la valeur de     4
                            caractéristiques             

   20ème          +6        Archidruide                      4
  ----------------------------------------------------------------

  : Le Druide

En tant que druide, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau de Druide.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau de Druide après le 1er niveau.



#### Maîtrises

**Armure :** Armure légère, armure moyenne, boucliers (les druides ne
portent pas d'armure et n'utilisent pas de boucliers en métal).

**Armes :** Gourdins, dagues, fléchettes, javelots, masses, bâtons,
cimeterres, faucilles, frondes et lances.

**Outils :** Kit d'herboristerie

**Jets de sauvegarde :** Intelligence, Sagesse

**Compétences :** Choisissez deux compétences parmi les suivantes :
Mystère, Dressage d'animaux, Intuition, Médecine, Nature, Perception,
Religion et Survie.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* un bouclier en bois ou*(b*) toute arme courante.
-   (*a)* d'un cimeterre ou*b*) de toute arme de mêlée simple.
-   Une armure en cuir, un sac d'explorateur, et une concentration
    druidique.




### Druidique

Vous connaissez le druidique, la langue secrète des druides. Vous pouvez
parler cette langue et l'utiliser pour laisser des messages cachés.
Vous et les autres personnes qui connaissent cette langue repèrent
automatiquement un tel message. Les autres repèrent la présence du
message en réussissant un test de Sagesse (Perception) DC 15, mais ne
peuvent pas le déchiffrer sans magie.



### Incantation

En puisant dans l'essence divine de la nature elle-même, vous pouvez
lancer des sorts pour façonner cette essence à votre guise.


#### Tours de magie

Au 1er niveau, vous connaissez deux cantrips de votre choix dans la
liste des sorts de druide. Vous apprenez d'autres tours de magie
druidiques de votre choix à des niveaux supérieurs, comme indiqué dans
la colonne Cantrips Known de la table Druide.



#### Préparer et lancer des sorts

  ---------------------------------------------------------------------------------
   Niveau   Niveau                                                          
   Druide    des                                                            
            sorts                                                           
  -------- -------- ------- ------- ------- ------- ------- ------- ------- -------
             1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

    1er       2       \-      \-      \-      \-      \-      \-      \-      \-

    2ème      3       \-      \-      \-      \-      \-      \-      \-      \-

    3ème      4        2      \-      \-      \-      \-      \-      \-      \-

    4ème      4        3      \-      \-      \-      \-      \-      \-      \-

    5ème      4        3       2      \-      \-      \-      \-      \-      \-

    6ème      4        3       3      \-      \-      \-      \-      \-      \-

    7ème      4        3       3       1      \-      \-      \-      \-      \-

    8ème      4        3       3       2      \-      \-      \-      \-      \-

    9ème      4        3       3       3       1      \-      \-      \-      \-

   10ème      4        3       3       3       2      \-      \-      \-      \-

   11ème      4        3       3       3       2       1      \-      \-      \-

   12ème      4        3       3       3       2       1      \-      \-      \-

   13ème      4        3       3       3       2       1       1      \-      \-

   14ème      4        3       3       3       2       1       1      \-      \-

   15ème      4        3       3       3       2       1       1       1      \-

   16ème      4        3       3       3       2       1       1       1      \-

   17ème      4        3       3       3       2       1       1       1       1

   18ème      4        3       3       3       3       1       1       1       1

   19ème      4        3       3       3       3       2       1       1       1

   20ème      4        3       3       3       3       2       2       1       1
  ---------------------------------------------------------------------------------

  : Emplacements de sorts pour les druides par niveau

Le tableau Emplacements de sorts par niveau pour les druides indique le
nombre d'emplacements de sorts dont vous disposez pour lancer vos sorts
de 1er niveau et plus. Pour lancer l'un de ces sorts de druide, vous
devez dépenser un emplacement du niveau du sort ou d'un niveau
supérieur. Vous récupérez tous les emplacements de sorts dépensés
lorsque vous terminez un long repos.

Vous préparez la liste des sorts de druide que vous pouvez lancer, en
choisissant dans la liste des sorts de druide. Vous choisissez alors un
nombre de sorts de druide égal à votre modificateur de Sagesse + votre
niveau de druide (au moins un sort). Les sorts doivent être d'un niveau
pour lequel vous disposez d'emplacements de sorts.

Par exemple, si vous êtes un druide de 3e niveau, vous avez quatre
emplacements de sorts de 1er niveau et deux de 2e niveau. Avec une
Sagesse de 16, votre liste de sorts préparés peut inclure six sorts de
1er ou 2ème niveau, dans n'importe quelle combinaison. Si vous préparez
le sort de 1er niveau *soigner les soins*, vous pouvez
le lancer en utilisant un emplacement de 1er niveau ou de 2ème niveau.
Lancer le sort ne le retire pas de votre liste de sorts préparés.

Vous pouvez également modifier votre liste de sorts préparés lorsque
vous terminez un repos long. La préparation d'une nouvelle liste de
sorts druides nécessite du temps passé en prière et en méditation : au
moins 1 minute par niveau de sort pour chaque sort de votre liste.



#### Caractéristique d'Incantation

La Sagesse est votre caractéristique d'incantation pour vos sorts de
druide, puisque votre magie s'appuie sur votre dévotion et votre accord
avec la nature. Vous utilisez votre Sagesse lorsqu'un sort fait
référence à votre caractéristique d'incantation. De plus, vous utilisez
votre modificateur de Sagesse pour déterminer le jet de sauvegarde d'un
sort druide que vous lancez et pour effectuer un jet d'attaque avec un
tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Sagesse.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Sagesse.
  -----------------------------------------------------------------------



#### Lancer de rituel

Vous pouvez lancer un sort de druide en tant que rituel si ce sort a
l'étiquette rituel et si vous avez préparé le sort.



#### Lanceur de sorts Focus

Vous pouvez utiliser un foyer druidique comme foyer d'incantation pour
vos sorts de druide.




### Forme sauvage

À partir du 2e niveau, vous pouvez utiliser votre action pour prendre
magiquement la forme d'une bête que vous avez déjà vue. Vous pouvez
utiliser cette caractéristique deux fois. Vous récupérez les
utilisations dépensées lorsque vous terminez un repos court ou long.

Votre niveau de druide détermine les bêtes en lesquelles vous pouvez
vous transformer, comme l'indique la table Formes des bêtes. Au 2e
niveau, par exemple, vous pouvez vous transformer en n'importe quelle
bête dont le niveau de difficulté est de 1/4 ou moins et qui n'a pas de
vitesse de vol ou de natation.

  ------------------------------------------------------------
   Niveau   Max. CR  Limites                      Exemple
  -------- --------- ---------------------------- ------------
    2ème      1/4    Pas de vitesse de vol ou de  Loup
                     natation                     

    4ème      1/2    Pas de vitesse de vol        Crocodile

    8ème       1     \-                           Aigle géant
  ------------------------------------------------------------

  : Formes de la bête

Vous pouvez rester sous une forme de bête pendant un nombre d'heures
égal à la moitié de votre niveau de druide (arrondi à l'inférieur).
Vous reprenez ensuite votre forme normale, sauf si vous dépensez une
autre utilisation de cette caractéristique. Vous pouvez revenir à votre
forme normale plus tôt en utilisant une action bonus à votre tour. Vous
revenez automatiquement à votre forme normale si vous tombez
inconscient, si vous êtes à zéro point de vie ou si vous mourez.

Pendant votre transformation, les règles suivantes s'appliquent :

-   Vos statistiques de jeu sont remplacées par celles de la bête, mais
    vous conservez votre alignement, votre personnalité et vos scores
    d'Intelligence, de Sagesse et de Charisme. Vous conservez également
    toutes vos compétences et jets de sauvegarde, en plus de gagner
    celles de la créature. Si la créature a la même maîtrise que vous et
    que le bonus de son bloc de statistiques est supérieur au vôtre,
    utilisez le bonus de la créature au lieu du vôtre. Si la créature
    possède des actions légendaires ou des actions de repaire, vous ne
    pouvez pas les utiliser.
-   Lorsque vous vous transformez, vous assumez les points de vie et les
    dés de réussite de la bête. Lorsque vous reprenez votre forme
    normale, vous retrouvez le nombre de points de vie que vous aviez
    avant votre transformation. Cependant, si vous vous retrouvez à 0
    point de vie, tous les dégâts excédentaires sont reportés dans votre
    forme normale. Par exemple, si vous subissez 10 points de dégâts
    sous forme animale et qu'il ne vous reste qu'un point de vie, vous
    vous retransformez et subissez 9 points de dégâts. Tant que
    l'excédent de dégâts ne réduit pas votre forme normale à 0 point de
    vie, vous n'êtes pas débloqué.
-   Vous ne pouvez pas lancer de sorts, et votre capacité à parler ou à
    effectuer toute action nécessitant des mains est limitée aux
    capacités de votre forme bestiale. Cependant, la transformation ne
    vous empêche pas de vous concentrer sur un sort que vous avez déjà
    lancé, ni d'entreprendre des actions qui font partie d'un sort,
    comme Appel *de la foudre*, que vous avez déjà
    lancé.
-   Vous conservez les avantages de toutes les caractéristiques de votre
    classe, origine ou autre source et pouvez les utiliser si votre
    nouvelle forme en est physiquement capable. Cependant, vous ne
    pouvez utiliser aucun de vos sens spéciaux, comme la vision dans le
    noir, à moins que votre nouvelle forme ne possède également ce sens.
-   Vous choisissez si votre équipement tombe sur le sol dans votre
    espace, s'intègre à votre nouvelle forme ou est porté par elle.
    L'équipement porté fonctionne normalement, mais le MJ décide s'il
    est pratique pour la nouvelle forme de porter un équipement, en
    fonction de la forme et de la taille de la créature. Votre
    équipement ne change pas de taille ou de forme pour s'adapter à la
    nouvelle forme, et tout équipement que la nouvelle forme ne peut pas
    porter doit soit tomber au sol, soit fusionner avec elle.
    L'équipement qui fusionne avec la forme n'a aucun effet jusqu'à
    ce que vous quittiez la forme.



### Cercle druidique

Au 2e niveau, vous choisissez de vous identifier à un cercle de druides
. Votre choix
vous octroie des capacités au 2ème niveau, puis au 6ème, 10ème et 14ème
niveau.



### Jeunesse éternelle

À partir du 18e niveau, la magie primitive que vous maniez vous fait
vieillir plus lentement. Pour chaque tranche de 10 ans qui passe, votre
corps ne vieillit que d'un an.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Sortilèges bestiaux

À partir du 18e niveau, vous pouvez lancer bon nombre de vos sorts de
druide dans n'importe quelle forme que vous adoptez en utilisant Forme
sauvage. Vous pouvez exécuter les composantes somatiques et verbales
d'un sort de druide lorsque vous êtes dans une forme de bête, mais vous
ne pouvez pas fournir de composantes matérielles.



### Archidruide

Au 20e niveau, vous pouvez utiliser votre Forme sauvage un nombre
illimité de fois.

De plus, vous pouvez ignorer les composantes verbales et somatiques de
vos sorts de druide, ainsi que toutes les composantes matérielles qui
n'ont pas de coût et qui ne sont pas consommées par un sort. Vous
bénéficiez de cet avantage à la fois sous votre forme normale et sous
votre forme sauvage de Forme sauvage.

> #### Plantes et bois sacrés {#sacred-plants-and-wood}
>
> Un druide considère certaines plantes comme sacrées, notamment
> l'aulne, le frêne, le bouleau, le sureau, le noisetier, le houx, le
> genévrier, le gui, le chêne, le sorbier, le saule et l'if. Les
> druides utilisent souvent ces plantes dans le cadre d'une
> incantation, en y incorporant des tronçons de chêne ou d'if ou des
> brins de gui.
>
> De même, un druide utilise ces bois pour fabriquer d'autres objets,
> comme des armes et des boucliers. L'if est associé à la mort et à la
> renaissance, c'est pourquoi les manches de cimeterres ou de faucilles
> peuvent en être façonnés. Le frêne est associé à la vie et le chêne à
> la force. Ces bois font d'excellents fers ou armes entières, comme
> des gourdins ou des bâtons, ainsi que des boucliers. L'aulne est
> associé à l'air et peut être utilisé pour les armes lancées, comme
> les fléchettes ou les javelots.
>
> Les Druides des régions qui ne possèdent pas les plantes décrites ici
> ont choisi d'autres plantes pour des usages similaires. Par exemple,
> un druide d'une région désertique pourrait apprécier le yucca et les
> plantes de cactus.

> #### Les Druides et les Dieux {#druids-and-the-gods}
>
> Certains druides vénèrent les forces de la nature elles-mêmes, mais la
> plupart des druides sont dévoués à l'une des nombreuses divinités de
> la nature vénérées dans le multivers (les listes de dieux dans la
> section \"Panthéons\" incluent de nombreuses
> divinités de ce type). Le culte de ces divinités est souvent considéré
> comme une tradition plus ancienne que les croyances des clercs et des
> peuples urbanisés.





### Cercles druidiques ###



#### Cercle de la Terre ####

Le Cercle de la Terre est composé de mystiques et de sages qui
sauvegardent les connaissances et les rites anciens grâce à une vaste
tradition orale. Ces druides se réunissent dans des cercles sacrés
d'arbres ou de pierres dressées pour murmurer des secrets primitifs en
druidique. Les membres les plus sages du cercle président en tant que
prêtres en chef des communautés qui adhèrent à l'ancienne foi et
servent de conseillers aux dirigeants de ces peuples. En tant que membre
de ce cercle, votre magie est influencée par la terre où vous avez été
initié aux rites mystérieux du cercle.



##### Tour de magie en prime #####

Lorsque vous choisissez ce cercle au 2ème niveau, vous apprenez un tour
de magie druide supplémentaire de votre choix.




##### Régénération naturelle #####

À partir du 2e niveau, vous pouvez récupérer une partie de votre énergie
magique en vous asseyant en méditation et en communion avec la nature.
Pendant un repos court, vous choisissez des emplacements de sorts
dépensés pour récupérer. Les emplacements de sorts peuvent avoir un
niveau combiné égal ou inférieur à la moitié de votre niveau de druide
(arrondi au supérieur), et aucun des emplacements ne peut être de 6e
niveau ou plus. Vous ne pouvez pas utiliser cette caractéristique à
nouveau avant d'avoir terminé un long repos.

Par exemple, lorsque vous êtes un druide de 4e niveau, vous pouvez
récupérer jusqu'à deux niveaux d'emplacements de sorts. Vous pouvez
récupérer soit un emplacement de 2ème niveau, soit deux emplacements de
1er niveau.




##### Sortilèges de cercle #####

Votre lien mystique avec la terre vous infuse la capacité de lancer
certains sorts. Aux 3e, 5e, 7e et 9e niveaux, vous avez accès à des
sorts de cercle liés à la terre où vous êtes devenu druide. Choisissez
cette terre - arctique, côte, désert, forêt, prairie, montagne ou
marécage - et consultez la liste de sorts associée.

Une fois que vous avez accès à un sort de cercle, vous l'avez toujours
préparé, et il ne compte pas dans le nombre de sorts que vous pouvez
préparer chaque jour. Si vous avez accès à un sort qui ne figure pas sur
la liste des sorts de druide, ce sort est néanmoins un sort de druide
pour vous.

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Immobilisation depersonne*, *croissance d'épines*

      5ème      *Tempête de neige*

      7ème      *Liberté demouvement*, *tempête de verglas*

      9ème      *Communion avec la nature*, *Cône de froid*
  ----------------------------------------------------------------------

  : Arctique

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      Image *miroir*, *Pas
                brumeux*

      5ème      *Respiration aquatique*, Marche sur
                l'*eau*

      7ème      *contrôle de l'eau*, *liberté de
                mouvement*

      9ème      Invocation d'*élémentaire*,
                *Scrutation*
  ----------------------------------------------------------------------

  : Côte

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Flou*

      5ème      *Création* de nourriture*et
                d'eau*, Protection contre
                l'*énergie*

      7ème      *Flétrissement*, *terrain
                halluciné*

      9ème      *Fléau d'insectes*, *Mur de
                pierre*
  ----------------------------------------------------------------------

  : Désert

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Peau d'*écorce, *araignée
                de*montagne

      5ème      *Appel à la foudre*, *croissance
                végétale*

      7ème      *divination*, *liberté de
                mouvement*

      9ème      *Communion avec la nature*,
                *Foulée d'arbre*
  ----------------------------------------------------------------------

  : Forêt

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Invisibilité*, *passage sans
                trace*

      5ème      Lumière*du jour*

      7ème      *divination*, *liberté de
                mouvement*

      9ème      *Songe*
  ----------------------------------------------------------------------

  : Prairie

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Pattes*
                d'*araignée*, *Croissance
                d'épines*

      5ème      *foudre*, *fusion dans la
                pierre*

      7ème      *Façonnage de la pierre*, *Peau de
                pierre*

      9ème      *Passe-muraille*, *mur de
                pierre*
  ----------------------------------------------------------------------

  : Montagne

  ----------------------------------------------------------------------
  Niveau Druide Sortilèges de cercle
  ------------- --------------------------------------------------------
      3ème      *Marche* sur l'eau, Nuage
                *nauséabond*

      5ème      Liberté de*mouvement*,
                *localiser une créature*

      7ème      *Fléau d'insectes*,
                *Scrutation*

      9ème      *flèche acide*
  ----------------------------------------------------------------------

  : Marais




##### Foulée terrestre #####

À partir du 6e niveau, se déplacer à travers un terrain difficile non
magique ne vous coûte aucun mouvement supplémentaire. Vous pouvez
également traverser des plantes non magiques sans être ralenti par elles
et sans subir de dégâts si elles ont des épines, des épines ou un danger
similaire.

De plus, vous avez un avantage aux jets de sauvegarde contre les plantes
créées ou manipulées magiquement pour entraver le mouvement, comme
celles créées par le sort *Enchevêtrement*.




##### Protégé par la Nature #####

Lorsque vous atteignez le 10e niveau, vous ne pouvez plus être charmé ou
effrayé par les élémentaires ou les fées, et vous êtes immunisé contre
le poison et les maladies.




##### Sanctuaire de la Nature #####

Lorsque vous atteignez le 14e niveau, les créatures du monde naturel
ressentent votre lien avec la nature et hésitent à vous attaquer.
Lorsqu'une créature animale ou végétale vous attaque, elle doit
effectuer un jet de sauvegarde de Sagesse contre le DC de sauvegarde de
votre sort druide. En cas d'échec, la créature doit choisir une autre
cible, ou l'attaque est automatiquement manquée. En cas de sauvegarde
réussie, la créature est immunisée contre cet effet pendant 24 heures.

La créature est consciente de cet effet avant d'effectuer son attaque
contre vous.





## Guerrier


### Capacités de la classe

  --------------------------------------------------------------------------
   Niveau      Bonus de     Capacités
               maîtrise     
            supplémentaire  
  -------- ---------------- ------------------------------------------------
    1er           +2        Style de combat, Second souffle

    2ème          +2        Élan (une seule utilisation)

    3ème          +2        Archétype martial

    4ème          +2        Amélioration de la valeur de caractéristiques

    5ème          +3        Attaque supplémentaire

    6ème          +3        Amélioration de la valeur de caractéristiques

    7ème          +3        Caractéristique de l'Archétype martial

    8ème          +3        Amélioration de la valeur de caractéristiques

    9ème          +4        Indomptable (un usage)

   10ème          +4        Caractéristique de l'Archétype martial

   11ème          +4        Attaque supplémentaire (2)

   12ème          +4        Amélioration de la valeur de caractéristiques

   13ème          +5        Indomptable (deux utilisations)

   14ème          +5        Amélioration de la valeur de caractéristiques

   15ème          +5        Caractéristique de l'Archétype martial

   16ème          +5        Amélioration de la valeur de caractéristiques

   17ème          +6        Élan (deux utilisations), Indomptable (trois
                            utilisations)

   18ème          +6        Caractéristique de l'Archétype martial

   19ème          +6        Amélioration de la valeur de caractéristiques

   20ème          +6        Attaque supplémentaire (3)
  --------------------------------------------------------------------------

  : Le guerrier

En tant que guerrier, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d10 par niveau de guerrier.

**Points de vie au 1er niveau :** 10 + votre modificateur de
Constitution

**Points de vie à des niveaux supérieurs :** 1d10 (ou 6) + votre
modificateur de Constitution par niveau de guerrier après le 1er



#### Maîtrises

**Armures :** Toutes les armures, boucliers.

**Armes :** Armes courantes, armes martiales.

**Outils :** Aucun

**Jets de sauvegarde :** Force, Constitution

**Compétences :** Choisissez deux compétences parmi les suivantes :
Acrobaties, Dressage d'animaux, Athlétisme, Histoire, Intuition,
Intimidation, Perception et Survie.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* cotte de mailles ou*(b)* armure de cuir, arc long, et 20
    flèches
-   *(a)* une arme de guerre et un bouclier ou*(b*) deux armes de
    guerre.
-   *(a)* une arbalète légère et 20 carreaux ou*(b)* deux hachettes
-   *(a)* un sac de donjon ou*(b*) un sac d'explorateur.




### Style de combat

Vous adoptez un style de combat particulier comme spécialité. Choisissez
l'une des options suivantes. Vous ne pouvez pas choisir un Style de
combat plus d'une fois, même si vous pouvez le choisir à nouveau plus
tard.


#### Archerie

Vous bénéficiez d'un bonus de +2 aux jets d'attaque effectués avec des
armes à distance.



#### Défense

Lorsque vous portez une armure, vous bénéficiez d'un bonus de +1 à la
CA.



#### Duel

Lorsque vous brandissez une arme de mêlée dans une main et aucune autre
arme, vous gagnez un bonus de +2 aux jets de dégâts avec cette arme.



#### Arme de grande taille

Lorsque vous obtenez un 1 ou un 2 sur un dé de dégâts pour une attaque
effectuée avec une arme de corps-à-corps que vous maniez à deux mains,
vous pouvez relancer le dé et devez utiliser le nouveau jet, même si le
nouveau jet est un 1 ou un 2. L'arme doit avoir la propriété à deux
mains ou polyvalente pour que vous puissiez bénéficier de cet avantage.



#### Protection

Lorsqu'une créature que vous pouvez voir attaque une cible autre que
vous qui se trouve à moins de 1,5 mètre de vous, vous pouvez utiliser
votre réaction pour imposer un désavantage au jet d'attaque. Vous devez
brandir un bouclier.



#### Combat à deux armes

Lors d'un combat à deux armes, vous pouvez ajouter votre modificateur
de capacité aux dégâts de la deuxième attaque.




### Second souffle

Vous disposez d'une réserve d'endurance limitée dans laquelle vous
pouvez puiser pour vous protéger des contaminations. À votre tour, vous
pouvez utiliser une action bonus pour regagner un nombre de points de
vie égal à 1d10 + votre niveau de guerrier. Une fois que vous avez
utilisé cette capacité, vous devez terminer un repos court ou long avant
de pouvoir l'utiliser à nouveau.



### Élan d'action

À partir du 2e niveau, vous pouvez vous pousser au-delà de vos limites
normales pendant un instant. À votre tour, vous pouvez effectuer une
action supplémentaire en plus de votre action normale et une éventuelle
action bonus. Une fois que vous avez utilisé cette caractéristique, vous
devez terminer un repos court ou long avant de pouvoir l'utiliser à
nouveau. À partir du 17e niveau, vous pouvez l'utiliser deux fois avant
un repos, mais seulement une fois au cours du même tour.



### Archétype martial

Au 3e niveau, vous choisissez un archétype  que vous vous efforcez
d'imiter dans vos styles et techniques de combat. L'archétype que vous
choisissez vous octroie des capacités au 3e niveau, puis au 7e, 10e, 15e
et 18e niveau.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 6e, 8e, 12e, 14e,
16e et 19e niveau, vous pouvez augmenter un score de capacité de votre
choix de 2, ou vous pouvez augmenter deux scores de capacité de votre
choix de 1. Comme d'habitude, vous ne pouvez pas augmenter un score de
capacité au-dessus de 20 en utilisant cette caractéristique.



### Attaque supplémentaire

À partir du 5e niveau, vous pouvez attaquer deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.

Le nombre d'attaques passe à trois lorsque vous atteignez le 11e niveau
dans cette classe et à quatre lorsque vous atteignez le 20e niveau dans
cette classe.



### Indomptable

À partir du 9e niveau, vous pouvez relancer un jet de sauvegarde que
vous avez raté. Si vous le faites, vous devez utiliser le nouveau jet,
et vous ne pouvez pas utiliser cette caractéristique à nouveau avant
d'avoir terminé un long repos.

Vous pouvez utiliser cette fonction deux fois entre des repos longs à
partir du 13e niveau et trois fois entre des repos longs à partir du 17e
niveau.





### Archétypes martiaux ###

Différents guerriers choisissent différentes approches pour
perfectionner leurs prouesses au combat. L'archétype martial que vous
choisissez d'imiter reflète votre approche.



#### Champion ####

L'archétype du Champion se concentre sur le développement d'une
puissance physique brute aiguisée à la perfection. Ceux qui s'inspirent
de cet archétype associent un entraînement rigoureux à l'excellence
physique pour porter des coups dévastateurs.



##### Critique améliorée #####

À partir du moment où vous choisissez cet archétype au 3e niveau, vos
attaques d'armes obtiennent un coup critique sur un jet de 19 ou 20.




##### Athlète accomplie #####

À partir du 7e niveau, vous pouvez ajouter la moitié de votre bonus de
maîtrise (arrondi au supérieur) à tout test de Force, de Dextérité ou de
Constitution que vous effectuez et qui n'utilise pas déjà votre bonus
de maîtrise.

De plus, lorsque vous effectuez un saut en longueur, la distance que
vous pouvez parcourir augmente d'un nombre de pieds égal à votre
modificateur de Force.




##### Style de combat supplémentaire #####

Au 10e niveau, vous pouvez choisir une deuxième option parmi les
caractéristiques de classe du Style de combat.




##### Critique supérieur #####

À partir du 15e niveau, vos attaques d'armes obtiennent un coup
critique sur un jet de 18-20.




##### Survivant #####

Au 18e niveau, vous atteignez le summum de la résilience au combat. Au
début de chacun de vos tours, vous regagnez un nombre de points de vie
égal à 5 + votre modificateur de Constitution s'il ne vous reste pas
plus de la moitié de vos points de vie. Vous ne bénéficiez pas de cet
avantage si vous avez 0 points de vie.





## Moine


### Capacités de la classe

  ----------------------------------------------------------------------------------------
   Niveau      Bonus de        Arts     Points   Déplacement  Capacités
               maîtrise      martiaux     Ki     sans armure  
            supplémentaire                                    
  -------- ---------------- ---------- -------- ------------- ----------------------------
    1er           +2           1d4        \-         \-       Défense sans armure, Arts
                                                              martiaux

    2ème          +2           1d4        2        +10 ft.    Ki, Déplacement sans armure

    3ème          +2           1d4        3        +10 ft.    Tradition monastique, Parade
                                                              de projectiles

    4ème          +2           1d4        4        +10 ft.    Amélioration de la valeur de
                                                              caractéristiques, Chute
                                                              ralentie

    5ème          +3           1d6        5        +10 ft.    Attaque supplémentaire,
                                                              Frappe étourdissante

    6ème          +3           1d6        6        +15 ft.    Frappes mues par le Ki,
                                                              caractéristique de la
                                                              Tradition monastique

    7ème          +3           1d6        7        +15 ft.    Dérobade, Tranquilité de
                                                              l'esprit

    8ème          +3           1d6        8        +15 ft.    Amélioration de la valeur de
                                                              caractéristiques

    9ème          +4           1d6        9        +15 ft.    Amélioration du Déplacement
                                                              sans armure

   10ème          +4           1d6        10       +20 ft.    Pureté du corps

   11ème          +4           1d8        11       +20 ft.    Dossier sur la tradition
                                                              monastique

   12ème          +4           1d8        12       +20 ft.    Amélioration de la valeur de
                                                              caractéristiques

   13ème          +5           1d8        13       +20 ft.    Langue du soleil et de la
                                                              lune

   14ème          +5           1d8        14       +25 ft.    Âme de diamant

   15ème          +5           1d8        15       +25 ft.    Jeunesse éternelle

   16ème          +5           1d8        16       +25 ft.    Amélioration de la valeur de
                                                              caractéristiques

   17ème          +6           1d10       17       +25 ft.    Dossier sur la tradition
                                                              monastique

   18ème          +6           1d10       18       +30 ft.    Vidage de l'âme

   19ème          +6           1d10       19       +30 ft.    Amélioration de la valeur de
                                                              caractéristiques

   20ème          +6           1d10       20       +30 ft.    Autoperfectionnement
  ----------------------------------------------------------------------------------------

  : Le moine

En tant que moine, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau de moine.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux plus élevés :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau de moine après le 1er



#### Maîtrises

**Armure :** Aucune

**Armes :** Armes courantes, épées courtes.

**Outils :** Choisissez un type d'outils d'artisan ou un instrument de
musique.

**Jets de sauvegarde :** Force, Dextérité

**Compétences :** Choisissez deux compétences parmi les suivantes :
Acrobaties, Athlétisme, Histoire, Intuition, Religion et Discrétion.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une épée courte ou*(b*) toute arme courante.
-   *(a)* un sac de donjon ou*(b*) un sac d'explorateur.
-   10 fléchettes




### Défense sans armure

À partir du 1er niveau, si vous ne portez pas d'armure et ne brandissez
pas de bouclier, votre CA est égale à 10 + votre modificateur de
Dextérité + votre modificateur de Sagesse.



### Arts martiaux

Au 1er niveau, votre pratique des arts martiaux vous confère la maîtrise
des styles de combat utilisant des frappes sans arme et des armes de
moine, c'est-à-dire des épées courtes et toute arme de mêlée simple ne
possédant pas la propriété à deux mains ou lourde.

Vous bénéficiez des avantages suivants lorsque vous n'êtes pas armé ou
que vous ne brandissez que des armes de moine et que vous ne portez pas
d'armure ou ne brandissez pas de bouclier :

-   Vous pouvez utiliser la Dextérité au lieu de la Force pour les jets
    d'attaque et de dégâts de vos frappes sans arme et de vos armes de
    moine.
-   Vous pouvez lancer un d4 à la place des dégâts normaux de votre
    frappe sans arme ou de votre arme de moine. Ce dé change au fur et à
    mesure que vous gagnez des niveaux de moine, comme indiqué dans la
    colonne Arts martiaux de la table des moines.
-   Lorsque vous utilisez l'action Attaquer avec une attaque à mains
    nues ou une arme de moine à votre tour, vous pouvez effectuer une
    attaque à mains nues comme action bonus. Par exemple, si vous
    effectuez l'action Attaquer et que vous attaquez avec un bâton,
    vous pouvez également effectuer une frappe à mains nues en tant
    qu'action bonus, en supposant que vous n'ayez pas déjà effectué
    une action bonus ce tour-ci.

Certains monastères utilisent des formes spécialisées des armes des
moines. Par exemple, vous pouvez utiliser un gourdin constitué de deux
morceaux de bois reliés par une courte chaîne (appelé nunchaku) ou une
serpe avec une lame plus courte et plus droite (appelée kama). Quel que
soit le nom que vous donnez à une arme de moine, vous pouvez utiliser
les statistiques de jeu fournies pour cette arme.



### Ki

À partir du 2e niveau, votre formation vous permet d'exploiter
l'énergie mystique du Ki. Votre accès à cette énergie est représenté
par un certain nombre de points de Ki. Votre niveau de moine détermine
le nombre de points dont vous disposez, comme indiqué dans la colonne
Points de Ki de la table des moines.

Vous pouvez dépenser ces points pour alimenter diverses capacités de Ki.
Vous commencez à connaître trois de ces capacités : Rafale de coups,
Défense patiente, et Pas aériens. Vous apprenez d'autres
caractéristiques de Ki à mesure que vous gagnez des niveaux dans cette
classe.

Lorsque vous dépensez un point de Ki, il est indisponible jusqu'à la
fin d'un repos court ou long, à la fin duquel vous réinjectez tout le
Ki que vous avez dépensé. Vous devez passer au moins 30 minutes de ce
repos à méditer pour regagner vos points de Ki.

Pour certaines de vos capacités de Ki, votre cible doit effectuer un jet
de sauvegarde pour résister aux effets de la caractéristique. Le jet de
sauvegarde est calculé comme suit :

  -------------------------------------------------------------------------------------
   **Sauvegarde Ki DC** = 8 + votre bonus de maîtrise + votre modificateur de Sagesse.
  -------------------------------------------------------------------------------------


#### Rafale de coups

Immédiatement après avoir effectué l'action Attaquer à votre tour, vous
pouvez dépenser 1 point de Ki pour effectuer deux frappes à mains nues
comme action bonus.



#### Défense patiente

Vous pouvez dépenser 1 point de Ki pour effectuer l'action Esquive
comme action bonus à votre tour.



#### Pas aériens

Vous pouvez dépenser 1 point de Ki pour effectuer l'action Se
désengager ou Foncer comme action bonus à votre tour, et votre distance
de saut est doublée pour le tour.




### Déplacement sans armure

À partir du 2e niveau, votre vitesse augmente de 3 mètres lorsque vous
ne portez pas d'armure ou ne brandissez pas de bouclier. Ce bonus
augmente lorsque vous atteignez certains niveaux de moine, comme indiqué
dans la table des moines.

Au 9ème niveau, vous gagnez la capacité de vous déplacer sur des
surfaces verticales et à travers des liquides à votre tour sans tomber
pendant le mouvement.



### Tradition monastique

Lorsque vous allongez le 3e niveau, vous vous engagez dans une tradition
monastique. Votre tradition vous
accorde des capacités au 3e niveau, puis au 6e, 11e et 17e niveau.



### Parade de projectiles

À partir du 3e niveau, vous pouvez utiliser votre réaction pour dévier
ou attraper le missile lorsque vous êtes touché par une attaque avec une
arme à distance. Dans ce cas, les dégâts subis par l'attaque sont
réduits de 1d10 + votre modificateur de Dextérité + votre niveau de
moine.

Si vous réduisez les dégâts à 0, vous pouvez attraper le missile s'il
est suffisamment petit pour que vous puissiez le tenir dans une main et
que vous avez au moins une main libre. Si vous attrapez un missile de
cette façon, vous pouvez dépenser 1 point de ki pour effectuer une
attaque à distance avec l'arme ou la munition que vous venez
d'attraper, dans le cadre de la même réaction. Vous effectuez cette
attaque avec maîtrise, indépendamment de vos compétences en matière
d'armes, et le missile compte comme une arme de moine pour l'attaque,
qui a une portée normale de 20 pieds et une portée longue de 60 pieds.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Chute ralentie

À partir du 4e niveau, vous pouvez utiliser votre réaction lorsque vous
tombez pour réduire les dégâts de chute que vous subissez d'un montant
égal à cinq fois votre niveau de moine.



### Attaque supplémentaire

À partir du 5e niveau, vous pouvez attaquer deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.



### Jeunesse éternelle

Au 15ème niveau, votre Ki vous soutient de sorte que vous ne souffrez
pas de la fragilité de la vieillesse, et vous ne pouvez pas être vieilli
magiquement. Cependant, vous pouvez toujours mourir de vieillesse. De
plus, vous n'avez plus besoin de nourriture ni d'eau.



### Frappe étourdissante

À partir du 5e niveau, vous pouvez interférer avec le flux de ki dans le
corps d'un adversaire. Lorsque vous touchez une autre créature avec une
attaque à l'arme de mêlée, vous pouvez dépenser 1 point de ki pour
tenter une frappe étourdissante. La cible doit réussir un jet de
sauvegarde de Constitution ou être étourdie jusqu'à la fin de votre
prochain tour.



### Frappes mues par le Ki

À partir du 6e niveau, vos coups à mains nues sont considérés comme
magiques pour surmonter la résistance et l'immunité aux attaques et
dégâts non magiques.



### Dérobade

Au 7e niveau, votre agilité instinctive vous permet d'esquiver certains
effets de zone, comme le souffle de foudre d'un dragon bleu ou un sort
de *boule de feu*. Lorsque vous êtes soumis à un effet qui
vous permet de faire un jet de sauvegarde de Dextérité pour ne subir que
la moitié des dégâts, vous ne subissez à la place aucun dégât si vous
réussissez le jet de sauvegarde, et seulement la moitié des dégâts si
vous échouez.



### Tranquillité d'esprit

À partir du 7e niveau, vous pouvez utiliser votre action pour mettre fin
à un effet sur vous-même qui vous cause du charme ou de l'effroi.



### Pureté du corps

Au 10e niveau, votre maîtrise du ki qui vous traverse vous immunise
contre les maladies et le poison.



### Langue du soleil et de la lune

À partir du 13e niveau, vous apprenez à toucher le Ki des autres esprits
afin de comprendre toutes les langues parlées. De plus, toute créature
capable de comprendre une langue peut comprendre ce que vous dites.



### Âme de diamant

À partir du 14e niveau, votre maîtrise du Ki vous confère la maîtrise de
tous les jets de sauvegarde.

De plus, chaque fois que vous faites un jet de sauvegarde et que vous
échouez, vous pouvez dépenser 1 point de Ki pour le relancer et prendre
le second résultat.



### Vidage de l'âme

À partir du 18e niveau, vous pouvez utiliser votre action pour dépenser
4 points de ki pour devenir invisible pendant 1 minute. Pendant ce
temps, vous bénéficiez également d'une résistance à tous les dégâts,
sauf ceux de la force.

De plus, vous pouvez dépenser 8 points de Ki pour lancer le sort de
*projection astrale*, sans avoir besoin de
composantes matérielles. Lorsque vous le faites, vous ne pouvez pas
emmener d'autres créatures avec vous.



### Autoperfectionnement

Au 20e niveau, lorsque vous effectuez un jet d'initiative et qu'il ne
vous reste plus de points de Ki, vous regagnez 4 points de Ki.





### Traditions monastiques ###

Plusieurs traditions de poursuite monastique sont communes aux
monastères dispersés dans le multivers.

La plupart des monastères pratiquent exclusivement une tradition, mais
quelques-uns honorent les nombreuses traditions et instruisent chaque
moine en fonction de ses aptitudes et de ses intérêts. Toutes les
traditions reposent sur les mêmes techniques de base, qui divergent à
mesure que l'étudiant devient plus habile. Ainsi, un moine ne doit
choisir une tradition que lorsqu'il atteint le 3e niveau.



#### Voie de la paume ouverte ####

Les moines de la Voie de la paume ouverte sont les maîtres ultimes du
combat en arts martiaux, qu'ils soient armés ou non. Ils apprennent des
techniques pour pousser et faire trébucher leurs adversaires, manipulent
le Ki pour guérir les dommages causés à leur corps, et pratiquent une
méditation avancée qui peut les protéger de la contamination.



##### Technique de la paume ouverte #####

À partir du moment où vous choisissez cette tradition au 3e niveau, vous
pouvez manipuler le ki de votre ennemi lorsque vous maîtrisez le vôtre.
Chaque fois que vous touchez une créature avec l'une des attaques
accordées par votre rafale de coups, vous pouvez imposer à cette cible
l'un des effets suivants :

-   Il doit réussir un jet de sauvegarde de Dextérité ou être mis à
    terre.
-   Il doit effectuer un jet de sauvegarde de Force. S'il échoue, vous
    pouvez le pousser jusqu'à 15 pieds de vous.
-   Il ne peut pas prendre de réactions jusqu'à la fin de votre
    prochain tour.




##### Plénitude physique #####

Au 6e niveau, vous gagnez la capacité de vous guérir. En tant
qu'action, vous pouvez regagner un nombre de points de vie égal à trois
fois votre niveau de moine. Vous devez terminer un long repos avant de
pouvoir utiliser à nouveau cette caractéristique.




##### Tranquillité #####

À partir du 11e niveau, vous pouvez entrer dans une méditation spéciale
qui vous entoure d'une aura de paix. À la fin d'un repos long, vous
obtenez l'effet d'un sort de *sanctuaire* qui dure
jusqu'au début de votre prochain repos long (le sort peut se terminer
plus tôt que d'habitude). Le jet de sauvegarde pour ce sort est égal à
8 + votre modificateur de Sagesse + votre bonus de maîtrise.




##### Paume frémissante #####

Au 17e niveau, vous gagnez la capacité d'installer des vibrations
mortelles dans le corps de quelqu'un. Lorsque vous touchez une créature
avec une frappe sans arme, vous pouvez dépenser 3 points de Ki pour
déclencher ces vibrations imperceptibles, qui durent pendant un nombre
de jours égal à votre niveau de moine. Les vibrations sont inoffensives,
sauf si vous utilisez votre action pour y mettre fin. Pour ce faire,
vous et la cible devez vous trouver sur le même plan d'existence.
Lorsque vous utilisez cette action, la créature doit effectuer un jet de
sauvegarde de Constitution. Si elle échoue, elle est réduite à 0 point
de vie. Si elle réussit, elle subit 10d10 points de dégâts nécrotiques.

Vous ne pouvez avoir qu'une seule créature sous l'effet de cette
caractéristique à la fois. Vous pouvez choisir de mettre fin aux
vibrations de manière inoffensive sans utiliser d'action.





## Paladin


### Capacités de la classe

  -------------------------------------------------------------------
   Niveau      Bonus de     Capacités
               maîtrise     
            supplémentaire  
  -------- ---------------- -----------------------------------------
    1er           +2        Sens divin, Imposition des mains

    2ème          +2        Style de combat, incantation, sort divin.

    3ème          +2        Santé divine, Serment sacré

    4ème          +2        Amélioration de la valeur de
                            caractéristiques

    5ème          +3        Attaque supplémentaire

    6ème          +3        Aura de protection

    7ème          +3        Fonctionnement du Serment sacré

    8ème          +3        Amélioration de la valeur de
                            caractéristiques

    9ème          +4        \-

   10ème          +4        Aura de courage

   11ème          +4        Punition divine améliorée

   12ème          +4        Amélioration de la valeur de
                            caractéristiques

   13ème          +5        \-

   14ème          +5        Contact purifiant

   15ème          +5        Fonctionnement du Serment sacré

   16ème          +5        Amélioration de la valeur de
                            caractéristiques

   17ème          +6        \-

   18ème          +6        Améliorations de l'aura

   19ème          +6        Amélioration de la valeur de
                            caractéristiques

   20ème          +6        Fonctionnement du Serment sacré
  -------------------------------------------------------------------

  : Le Paladin

En tant que paladin, vous gagnez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d10 par niveau de paladin.

**Points de vie au 1er niveau :** 10 + votre modificateur de
Constitution

**Points de vie à des niveaux supérieurs :** 1d10 (ou 6) + votre
modificateur de Constitution par niveau de paladin après le 1er



#### Maîtrises

**Armures :** Toutes les armures, boucliers.

**Armes :** Armes courantes, armes martiales.

**Outils :** Aucun

**Jets de sauvegarde :** Sagesse, Charisme

**Compétences :** Choisissez-en deux parmi Athlétisme, Intuition,
Intimidation, Médecine, Persuasion et Religion.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une arme de guerre et un bouclier ou*(b*) deux armes de
    guerre.
-   *(a)* cinq javelots ou*(b*) toute arme de mêlée simple.
-   *(a)* le sac d'un prêtre ou*(b*) le sac d'un explorateur.
-   Cotte de mailles et symbole sacré




### Sens divin

La présence d'un mal puissant est perceptible à vos sens comme une
odeur nocive, et un bien puissant résonne comme une musique céleste à
vos oreilles. Comme une action, vous pouvez ouvrir votre conscience pour
détecter de telles forces. Jusqu'à la fin de votre prochain tour, vous
connaissez l'emplacement de tout céleste, fiélon ou mort-vivant situé à
moins de 60 pieds de vous et qui n'est pas à couvert total. Vous
connaissez le type (céleste, démon ou mort-vivant) de tout être dont
vous sentez la présence, mais pas son identité (le vampire Comte
Dracula, par exemple). Dans le même rayon, vous détectez également la
présence de tout lieu ou objet qui a été béni ou profané, comme avec
le sort de *sanctification*.

Vous pouvez utiliser cette caractéristique un nombre de fois égal à 1 +
votre modificateur de Charisme. Lorsque vous terminez un repos long,
vous récupérez toutes les utilisations dépensées.



### Imposition des mains

Votre toucher béni peut guérir les blessures. Vous disposez d'une
réserve de pouvoir de guérison qui se reconstitue lorsque vous prenez un
repos long. Avec cette réserve, vous pouvez restaurer un nombre total de
points de vie égal à votre niveau de paladin × 5.

En tant qu'action, vous pouvez toucher une créature et puiser de
l'énergie dans le pool pour restaurer un certain nombre de points de
vie à cette créature, jusqu'au montant maximum restant dans votre pool.

Vous pouvez également dépenser 5 points de vie de votre réserve de soins
pour guérir la cible d'une maladie ou neutraliser un poison qui
l'affecte. Vous pouvez guérir plusieurs maladies et neutraliser
plusieurs poisons en une seule utilisation d'Imposition des mains, en
dépensant des points de vie séparément pour chacun d'eux.

Cette caractéristique n'a aucun effet sur les morts-vivants et les
constructions.



### Style de combat

Au 2e niveau, vous adoptez un style de combat comme spécialité.
Choisissez l'une des options suivantes. Vous ne pouvez pas prendre une
option de Style de combat plus d'une fois, même si vous pouvez choisir
à nouveau plus tard.


#### Défense

Lorsque vous portez une armure, vous bénéficiez d'un bonus de +1 à la
CA.



#### Duel

Lorsque vous brandissez une arme de mêlée dans une main et aucune autre
arme, vous gagnez un bonus de +2 aux jets de dégâts avec cette arme.



#### Arme de grande taille

Lorsque vous obtenez un 1 ou un 2 sur un dé de dégâts pour une attaque
effectuée avec une arme de mêlée que vous maniez à deux mains, vous
pouvez relancer le dé et devez utiliser le nouveau résultat. L'arme
doit avoir la propriété à deux mains ou polyvalente pour que vous
puissiez bénéficier de cet avantage.



#### Protection

Lorsqu'une créature que vous pouvez voir attaque une cible autre que
vous qui se trouve à moins de 1,5 mètre de vous, vous pouvez utiliser
votre réaction pour imposer un désavantage au jet d'attaque. Vous devez
brandir un bouclier.




### Incantation

Au 2ème niveau, vous avez appris à puiser dans la magie divine par la
méditation et la prière pour lancer des sorts comme le fait un clerc.


#### Préparer et lancer des sorts

  --------------------------------------------------------------------------
  Niveau paladin Niveau des                                      
                    sorts                                        
  -------------- ----------- ----------- ----------- ----------- -----------
                     1er        2ème        3ème        4ème        5ème

       1er           \-          \-          \-          \-          \-

       2ème           2          \-          \-          \-          \-

       3ème           3          \-          \-          \-          \-

       4ème           3          \-          \-          \-          \-

       5ème           4           2          \-          \-          \-

       6ème           4           2          \-          \-          \-

       7ème           4           3          \-          \-          \-

       8ème           4           3          \-          \-          \-

       9ème           4           3           2          \-          \-

      10ème           4           3           2          \-          \-

      11ème           4           3           3          \-          \-

      12ème           4           3           3          \-          \-

      13ème           4           3           3           1          \-

      14ème           4           3           3           1          \-

      15ème           4           3           3           2          \-

      16ème           4           3           3           2          \-

      17ème           4           3           3           3           1

      18ème           4           3           3           3           1

      19ème           4           3           3           3           2

      20ème           4           3           3           3           2
  --------------------------------------------------------------------------

  : Emplacements de sorts pour paladins par niveau

Le tableau Emplacements de sorts pour paladins par niveau indique le
nombre d'emplacements de sorts dont vous disposez pour lancer vos
sorts. Pour lancer un de vos sorts de paladin de 1er niveau ou plus,
vous devez dépenser un emplacement du niveau du sort ou plus. Vous
récupérez tous les emplacements de sorts dépensés lorsque vous terminez
un long repos.

Vous préparez la liste des sorts de paladin que vous pouvez lancer, en
choisissant dans la liste des sorts de paladin. Vous choisissez alors un
nombre de sorts de paladin égal à votre modificateur de Charisme + la
moitié de votre niveau de paladin, arrondi à l'inférieur (minimum d'un
sort). Les sorts doivent être d'un niveau pour lequel vous disposez
d'emplacements de sorts.

Par exemple, si vous êtes un paladin de 5e niveau, vous avez quatre
emplacements de sorts de 1er niveau et deux de 2e niveau.

Avec un Charisme de 14, votre liste de sorts préparés peut inclure
quatre sorts de 1er ou 2ème niveau, dans n'importe quelle combinaison.
Si vous préparez le sort de 1er niveau *soigner les
soins*, vous pouvez le lancer en utilisant un emplacement
de 1er niveau ou de 2ème niveau. Lancer le sort ne le retire pas de
votre liste de sorts préparés.

Vous pouvez modifier votre liste de sorts préparés lorsque vous terminez
un repos long. La préparation d'une nouvelle liste de sorts de paladin
nécessite du temps passé en prière et en méditation : au moins 1 minute
par niveau de sort pour chaque sort de votre liste.



#### Caractéristique d'Incantation

Le Charisme est votre caractéristique d'incantation pour vos sorts de
paladin, puisque leur puissance découle de la force de vos convictions.
Vous utilisez votre Charisme lorsqu'un sort fait référence à votre
caractéristique d'incantation. De plus, vous utilisez votre
modificateur de Charisme pour déterminer le jet de sauvegarde d'un sort
de paladin que vous lancez et pour effectuer un jet d'attaque avec un
tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Charisme.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Charisme.
  -----------------------------------------------------------------------




### Lanceur de sorts Focus

Vous pouvez utiliser un symbole sacré comme foyer d'incantation pour
vos sorts de paladin.



### Punition divine

À partir du 2e niveau, lorsque vous touchez une créature avec une
attaque avec une arme de mêlée, vous pouvez dépenser un emplacement de
sort pour infliger des dégâts radiants à la cible, en plus des dégâts de
l'arme. Les dégâts supplémentaires sont de 2d8 pour un emplacement de
sort de 1er niveau, plus 1d8 pour chaque niveau de sort supérieur au
1er, jusqu'à un maximum de 5d8. Les dégâts augmentent de 1d8 si la
cible est un mort-vivant ou un fiélon.



### Santé divine

Au 3e niveau, la magie divine qui coule en vous vous immunise contre les
maladies.



### Serment sacré

Lorsque vous atteignez le 3e niveau, vous prêtez le serment  qui vous lie comme paladin
pour toujours. Jusqu'à ce moment, vous étiez dans une phase
préparatoire, engagé dans la voie mais pas encore assermenté. Vous
choisissez maintenant un serment sacré.

Votre choix vous confère des capacités au 3e niveau, puis au 7e, 15e et
20e niveau. Ces capacités comprennent les sorts de serment et la
caractéristique Canalisation divine.


#### Sorts de serment

Chaque serment a une liste de sorts associés. Vous avez accès à ces
sorts aux niveaux spécifiés dans la description du serment. Une fois que
vous avez accès à un sort de serment, vous l'avez toujours préparé. Les
sorts de serment ne comptent pas dans le nombre de sorts que vous pouvez
préparer chaque jour.

Si vous obtenez un sort de serment qui ne figure pas dans la liste des
sorts de paladin, ce sort est néanmoins un sort de paladin pour vous.



#### Canalisation divine

Votre serment vous permet de canaliser l'énergie divine pour alimenter
les effets magiques. Chaque option de Canalisation divine fournie par
votre serment explique comment l'utiliser.

Lorsque vous utilisez votre Canalisation divine, vous choisissez
l'option à utiliser. Vous devez ensuite terminer un repos court ou long
pour utiliser à nouveau votre Canalisation divine.

Certains effets de Canalisation divine nécessitent des jets de
sauvegarde. Lorsque vous utilisez un tel effet de cette classe, le jet
de sauvegarde est égal à votre jet de sauvegarde des sorts de paladin.




### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Attaque supplémentaire

À partir du 5e niveau, vous pouvez attaquer deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.



### Aura de protection

À partir du 6e niveau, chaque fois que vous ou une créature amie située
à 3 mètres de vous devez effectuer un jet de sauvegarde, la créature
gagne un bonus au jet de sauvegarde égal à votre modificateur de
Charisme (avec un bonus minimum de +1). Vous devez être conscient pour
accorder ce bonus.

Au 18ème niveau, la portée de cette aura passe à 30 pieds.



### Aura de courage

À partir du 10e niveau, vous et les créatures amies situées à 3 mètres
de vous ne pouvez pas être effrayés tant que vous êtes conscient.

Au 18ème niveau, la portée de cette aura passe à 30 pieds.



### Punition divine améliorée

Au 11ème niveau, vous êtes tellement imprégné de la puissance de la
justice que toutes vos frappes à l'arme de mêlée sont porteuses de
puissance divine. Chaque fois que vous touchez une créature avec une
arme de mêlée, celle-ci subit 1d8 points de dégâts radiants
supplémentaires. Si vous utilisez également votre Punition divine avec
une attaque, vous ajoutez ces dégâts aux dégâts supplémentaires de votre
Punition divine.



### Contact purifiant

À partir du 14e niveau, vous pouvez utiliser votre action pour mettre
fin à un sort sur vous-même ou sur une créature consentante que vous
touchez.

Vous pouvez utiliser cette caractéristique un nombre de fois égal à
votre modificateur de Charisme (au minimum une fois). Vous regagnez les
utilisations dépensées lorsque vous terminez un long repos.





### Serments sacrés du Paladin ###

Pour devenir paladin, il faut prononcer des serments qui engagent le
paladin dans la cause de la droiture, une voie active de lutte contre la
méchanceté. Le serment final, prononcé lorsqu'il atteint le 3e niveau,
est l'aboutissement de tout l'entraînement du paladin. Certains
personnages de cette classe ne se considèrent pas comme de véritables
paladins tant qu'ils n'ont pas atteint le 3e niveau et prêté ce
serment. Pour d'autres, la prestation de serment est une formalité, un
cachet officiel sur ce qui a toujours été vrai dans le cœur du paladin.

> #### Rompre son serment {#breaking-your-oath}
>
> Un paladin s'efforce de respecter les normes de conduite les plus
> élevées, mais même le paladin le plus vertueux est faillible. Parfois,
> le droit chemin s'avère trop exigeant, parfois une situation exige le
> moindre mal, et parfois la chaleur de l'émotion amène un paladin à
> transgresser son serment.
>
> Un paladin qui a rompu un vœu demande généralement l'absolution à un
> clerc qui partage sa foi ou à un autre paladin du même ordre. Le
> paladin peut passer une nuit entière en prière en signe de pénitence,
> ou entreprendre un jeûne ou un acte similaire d'abnégation. Après un
> rite de confession et de pardon, le paladin prend un nouveau départ.
>
> Si un paladin viole délibérément son serment et ne montre aucun signe
> de repentance, les conséquences peuvent être plus graves. À la
> discrétion du MJ, un paladin impénitent peut être contraint
> d'abandonner cette classe pour en adopter une autre.



#### Serment de Dévotion ####

Le Serment de Dévotion lie un paladin aux idéaux les plus élevés de
justice, de vertu et d'ordre. Parfois appelés cavaliers, chevaliers
blancs ou guerriers saints, ces paladins correspondent à l'idéal du
chevalier en armure brillante, agissant avec honneur dans la poursuite
de la justice et du bien commun. Ils s'imposent les normes de conduite
les plus élevées et certains, pour le meilleur ou pour le pire, imposent
les mêmes normes au reste du monde. Beaucoup de ceux qui prêtent ce
serment sont dévoués aux dieux de la loi et du bien et utilisent les
principes de leurs dieux comme mesure de leur dévotion. Ils considèrent
les anges - les parfaits serviteurs du bien - comme leur idéal, et
incorporent des images d'ailes angéliques dans leurs casques ou leurs
armoiries.



##### Les principes de la dévotion #####

Bien que les termes exacts et les règles du Serment de Dévotion varient,
les paladins de ce serment partagent ces principes.

***L'honnêteté.*** Ne mentez pas et ne trichez pas. Que votre parole
soit votre promesse.

***Le courage.*** Ne jamais avoir peur d'agir, même si la prudence est
de mise.

***Compassion.*** Aidez les autres, protégez les faibles et punissez
ceux qui les menacent. Soyez indulgent envers vos ennemis, mais faites
preuve de sagesse.

***L'honneur.*** Traitez les autres avec équité, et faites en sorte que
vos actes honorables soient un exemple pour eux. Faites autant de bien
que possible tout en causant le moins de contamination possible.

***Le devoir.*** Soyez responsable de vos actes et de leurs
conséquences, protégez ceux qui vous sont confiés et obéissez à ceux qui
ont une juste autorité sur vous.




##### Sorts de serment #####

Vous gagnez des sorts de serment aux niveaux paladins indiqués.

  -------------------------------------------------------------------
  Niveau paladin  Sorts
  --------------- ---------------------------------------------------
       3ème       Protection contre le Bien et
                  le*Mal*,
                  *Sanctuaire*

       5ème       Restauration*partielle*,
                  *Zone de vérité*

       9ème       Lueur d'*espoir*, *Dissipation
                  de la magie*

       13ème      Liberté de*mouvement*,
                  *Gardien de la foi*

       17ème      *Communion*, Colonne *de
                  flamme*
  -------------------------------------------------------------------

  : Sorts du Serment de Dévotion




##### Canalisation divine #####

Lorsque vous prêtez ce serment au 3e niveau, vous gagnez les deux
options de Canalisation divine suivantes.

***Arme sacrée.*** Comme action, vous pouvez imprégner une arme que vous
tenez d'énergie positive, en utilisant votre Canalisation divine.
Pendant 1 minute, vous ajoutez votre modificateur de Charisme aux jets
d'attaque effectués avec cette arme (avec un bonus minimum de +1).
L'arme émet également une lumière vive dans un rayon de 20 pieds et une
lumière chétive 20 pieds au-delà. Si l'arme n'est pas déjà magique,
elle le devient pour la durée du sort.

Vous pouvez mettre fin à cet effet à votre tour dans le cadre de toute
autre action. Si vous ne tenez ou ne portez plus cette arme, ou si vous
tombez inconscient, cet effet prend fin.

***Renvoi des sacrilèges.*** En tant qu'action, vous présentez votre
symbole sacré et prononcez une prière censurant les fiélons et les
morts-vivants, en utilisant votre Canalisation divine. Chaque fiélon ou
mort-vivant qui peut vous voir ou vous entendre à moins de 30 pieds de
vous doit effectuer un jet de sauvegarde de Sagesse. Si la créature
échoue à son jet de sauvegarde, elle est retournée pendant 1 minute ou
jusqu'à ce qu'elle subisse des dégâts.

Une créature transformée doit passer ses tours à essayer de s'éloigner
le plus possible de vous, et elle ne peut pas se déplacer volontairement
vers un espace situé à moins de 30 pieds de vous. Elle ne peut pas non
plus avoir de réactions. Pour son action, elle ne peut utiliser que
l'action Foncer ou essayer d'échapper à un effet qui l'empêche de se
déplacer. S'il n'y a nulle part où se déplacer, la créature peut
utiliser l'action Esquive.




##### Aura de dévotion #####

À partir du 7e niveau, vous et les créatures amies situées à 3 mètres de
vous ne pouvez pas être charmés tant que vous êtes conscient.

Au 18ème niveau, la portée de cette aura passe à 30 pieds.




##### Pureté de l'esprit #####

À partir du 15e niveau, vous êtes toujours sous les effets d'un sort de
Protection contre le Bien *et le Mal*.




##### Nimbe sacré #####

Au 20e niveau, comme une action, vous pouvez émaner une aura de lumière
solaire. Pendant 1 minute, une lumière vive brille de vous dans un rayon
de 30 pieds, et une lumière chétive brille à 30 pieds au-delà.

Chaque fois qu'une créature ennemie commence son tour dans la lumière
vive, elle subit 10 dégâts radiants.

De plus, pendant toute la durée de l'opération, vous avez un avantage
aux jets de sauvegarde contre les sorts lancés par des fiélons ou des
morts-vivants.

Une fois que vous avez utilisé cette fonction, vous ne pouvez plus
l'utiliser jusqu'à ce que vous ayez terminé un repos long.





## Rôdeur


### Capacités de la classe

  -------------------------------------------------------------------------------
   Niveau      Bonus de     Capacités                                     Sorts
               maîtrise                                                   connus
            supplémentaire                                               
  -------- ---------------- -------------------------------------------- --------
    1er           +2        Ennemi juré, Explorateur atavique               \-

    2ème          +2        Style de combat, Incantation                    2

    3ème          +2        Archétype du rôdeur, Vigilance primitive        3

    4ème          +2        Amélioration de la valeur de                    3
                            caractéristiques                             

    5ème          +3        Attaque supplémentaire                          4

    6ème          +3        Ennemi juré et Explorateur atavique             4
                            améliorations                                

    7ème          +3        Capacité de l'Archétype du rôdeur              5

    8ème          +3        Amélioration de la valeur de                    5
                            caractéristiques, Foulée terrestre           

    9ème          +4        \-                                              6

   10ème          +4        Amélioration Explorateur atavique,              6
                            Camouflage en plein air                      

   11ème          +4        Capacité de l'Archétype du rôdeur              7

   12ème          +4        Amélioration de la valeur de                    7
                            caractéristiques                             

   13ème          +5        \-                                              8

   14ème          +5        Amélioration Ennemi juré, Disparition           8

   15ème          +5        Capacité de l'Archétype du rôdeur              9

   16ème          +5        Amélioration de la valeur de                    9
                            caractéristiques                             

   17ème          +6        \-                                              10

   18ème          +6        Sens sauvages                                   10

   19ème          +6        Amélioration de la valeur de caractéristique    11

   20ème          +6        Tueur d'ennemis                                11
  -------------------------------------------------------------------------------

  : Le rôdeur

En tant que rôdeur, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d10 par niveau de rôdeur.

**Points de vie au 1er niveau :** 10 + votre modificateur de
Constitution

**Points de vie à des niveaux supérieurs :** 1d10 (ou 6) + votre
modificateur de Constitution par niveau de rôdeur après le 1er



#### Maîtrises

**Blindage :** armure légère, armure moyenne, boucliers.

**Armes :** Armes courantes, armes martiales.

**Outils :** Aucun

**Jets de sauvegarde :** Force, Dextérité

**Compétences :** Choisissez-en trois parmi Dressage, Athlétisme,
Intuition, Investigation, Nature, Perception, Discrétion et Survie.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* cotte de mailles ou*(b*) armure de cuir
-   *(a)* deux épées courtes ou*(b*) deux armes de mêlée simples.
-   *(a)* un sac de donjon ou*(b*) un sac d'explorateur.
-   Un arc long et un carquois de 20 flèches




### Ennemi juré

À partir du 1er niveau, vous avez une expérience significative dans
l'étude, la traque, la chasse et même la conversation avec un certain
type d'ennemi.

Choisissez un type d'ennemi juré : aberrations, bêtes, céleste,
constructions, dragons, élémentaires, fey, fiélons, géants,
monstruosités, oozes, plantes, ou morts-vivants. Vous pouvez également
choisir deux origines d'humanoïdes (comme les gnolls et les orcs) comme
ennemis privilégiés.

Vous avez un avantage sur les tests de Sagesse (Survie) pour suivre vos
Ennemis jurés, ainsi que sur les tests d'Intelligence pour obtenir des
informations sur eux.

Lorsque vous obtenez cette caractéristique, vous apprenez également une
langue de votre choix parlée par vos ennemis préférés, s'ils en parlent
une.

Vous choisissez un Ennemi juré supplémentaire, ainsi qu'une langue
associée, aux 6e et 14e niveaux. À mesure que vous gagnez des niveaux,
vos choix doivent refléter les types de monstres que vous avez
rencontrés au cours de vos aventures.



### Explorateur atavique

Vous êtes particulièrement familier avec un type d'environnement
naturel et êtes capable de voyager et de survivre dans de telles
régions. Choisissez un type de terrain favorisé : arctique, côte,
désert, forêt, prairie, montagne ou marécage. Lorsque vous effectuez un
test d'Intelligence ou de Sagesse lié à votre terrain favori, votre
bonus de maîtrise est doublé si vous utilisez une compétence que vous
maîtrisez.

Lorsque vous voyagez pendant une heure ou plus dans votre terrain
favori, vous gagnez les avantages suivants :

-   Les terrains difficiles ne ralentissent pas les déplacements de
    votre groupe.
-   Votre groupe ne peut pas se perdre, sauf par des moyens magiques.
-   Même si vous vous livrez à une autre activité lors de vos
    déplacements (comme la recherche de nourriture, la navigation ou le
    pistage), vous restez vigilant face au danger.
-   Si vous voyagez seul, vous pouvez vous déplacer furtivement à un
    rythme normal.
-   Quand on fourrage, on trouve deux fois plus de nourriture que
    d'habitude.
-   En traquant d'autres créatures, vous apprenez également leur nombre
    exact, leur taille et depuis combien de temps elles sont passées
    dans la région.

Vous choisissez des types de terrain favorisés supplémentaires au 6e et
au 10e niveau.



### Style de combat

Au 2e niveau, vous adoptez un style de combat particulier comme
spécialité. Choisissez l'une des options suivantes. Vous ne pouvez pas
prendre une option de Style de combat plus d'une fois, même si vous
pouvez choisir à nouveau plus tard.


#### Archerie

Vous bénéficiez d'un bonus de +2 aux jets d'attaque effectués avec des
armes à distance.



#### Défense

Lorsque vous portez une armure, vous bénéficiez d'un bonus de +1 à la
CA.



#### Duel

Lorsque vous brandissez une arme de mêlée dans une main et aucune autre
arme, vous gagnez un bonus de +2 aux jets de dégâts avec cette arme.



#### Combat à deux armes

Lors d'un combat à deux armes, vous pouvez ajouter votre modificateur
de capacité aux dégâts de la deuxième attaque.




### Incantation

Lorsque vous atteignez le 2e niveau, vous avez appris à utiliser
l'essence magique de la nature pour lancer des sorts, comme le fait un
druide. Voir \"Incantation\" et la liste des
sorts.


#### Emplacements de sorts

  ----------------------------------------------------------
   Niveau   Niveau                                 
   rôdeur  des sorts                               
  -------- --------- --------- --------- --------- ---------
              1er      2ème      3ème      4ème      5ème

    1er       \-        \-        \-        \-        \-

    2ème       2        \-        \-        \-        \-

    3ème       3        \-        \-        \-        \-

    4ème       3        \-        \-        \-        \-

    5ème       4         2        \-        \-        \-

    6ème       4         2        \-        \-        \-

    7ème       4         3        \-        \-        \-

    8ème       4         3        \-        \-        \-

    9ème       4         3         2        \-        \-

   10ème       4         3         2        \-        \-

   11ème       4         3         3        \-        \-

   12ème       4         3         3        \-        \-

   13ème       4         3         3         1        \-

   14ème       4         3         3         1        \-

   15ème       4         3         3         2        \-

   16ème       4         3         3         2        \-

   17ème       4         3         3         3         1

   18ème       4         3         3         3         1

   19ème       4         3         3         3         2

   20ème       4         3         3         3         2
  ----------------------------------------------------------

  : Emplacements de sorts de rôdeurs par niveau

Le tableau des emplacements de sorts des rôdeurs par niveau indique le
nombre d'emplacements de sorts dont vous disposez pour lancer vos sorts
de 1er niveau et plus. Pour lancer l'un de ces sorts, vous devez
dépenser un emplacement du niveau du sort ou d'un niveau supérieur.
Vous récupérez tous les emplacements de sorts dépensés lorsque vous
terminez un long repos.

Par exemple, si vous connaissez le sort *Parler avec les
animaux* de 1er niveau et que vous disposez d'un
emplacement de 1er niveau et d'un emplacement de 2ème niveau, vous
pouvez lancer *Parler avec les animaux* en
utilisant l'un ou l'autre de ces emplacements.



#### Sorts connus de 1er niveau et plus

Vous connaissez deux sorts de 1er niveau de votre choix dans la liste de
sorts des rôdeurs.

La colonne Sorts connus de la table du rôdeur indique quand vous
apprenez d'autres sorts de rôdeur de votre choix. Chacun de ces sorts
doit être d'un niveau pour lequel vous disposez d'emplacements de
sorts. Par exemple, lorsque vous atteignez le 5e niveau dans cette
classe, vous pouvez apprendre un nouveau sort de 1er ou 2e niveau.

De plus, lorsque vous gagnez un niveau dans cette classe, vous pouvez
choisir un des sorts de rôdeur que vous connaissez et le remplacer par
un autre sort de la liste des sorts de rôdeur, qui doit également être
d'un niveau pour lequel vous avez des emplacements de sorts.



#### Caractéristique d'Incantation

La Sagesse est votre caractéristique d'incantation pour vos sorts de
rôdeur, puisque votre magie s'appuie sur votre lien avec la nature.
Vous utilisez votre Sagesse lorsqu'un sort fait référence à votre
caractéristique d'incantation. De plus, vous utilisez votre
modificateur de Sagesse pour déterminer le jet de sauvegarde d'un sort
de rôdeur que vous lancez et pour effectuer un jet d'attaque avec un
tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Sagesse.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Sagesse.
  -----------------------------------------------------------------------




### Archétype du rôdeur

Au 3e niveau, vous choisissez un archétype que vous vous efforcez d'imiter.
Votre choix vous confère des capacités au 3e niveau, puis au 7e, 11e et
15e niveau.



### Vigilance primitive

À partir du 3e niveau, vous pouvez utiliser votre action et dépenser un
emplacement de sort de rôdeur pour concentrer votre conscience sur la
région qui vous entoure. Pendant 1 minute par niveau de l'emplacement
de sort que vous dépensez, vous pouvez sentir si les types de créatures
suivants sont présents dans un rayon de 1 mile autour de vous (ou
jusqu'à 6 miles si vous êtes dans votre terrain de prédilection) :
aberrations, céleste, dragons, élémentaires, fées, fiélons et
morts-vivants. Cette caractéristique ne révèle pas l'emplacement ni le
nombre des créatures.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Attaque supplémentaire

À partir du 5e niveau, vous pouvez attaquer deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.



### Foulée terrestre

À partir du 8e niveau, se déplacer à travers un terrain difficile non
magique ne vous coûte aucun mouvement supplémentaire. Vous pouvez
également traverser des plantes non magiques sans être ralenti par elles
et sans subir de dégâts si elles ont des épines, des épines ou un danger
similaire.

De plus, vous avez un avantage aux jets de sauvegarde contre les plantes
créées ou manipulées magiquement pour entraver le mouvement, comme
celles créées par le sort *Enchevêtrement*.



### Camouflage en plein air

À partir du 10e niveau, vous pouvez passer 1 minute à créer un
camouflage pour vous-même. Vous devez avoir accès à de la boue fraîche,
de la terre, des plantes, de la suie et d'autres matériaux d'origine
naturelle avec lesquels vous pouvez créer votre camouflage.

Une fois que vous êtes ainsi camouflé, vous pouvez essayer de vous
cacher en vous plaquant contre une surface solide, comme un arbre ou un
mur, qui est au moins aussi haute et large que vous. Vous bénéficiez
d'un bonus de +10 aux tests de Dextérité (Discrétion) tant que vous
restez là sans vous déplacer ni effectuer d'action. Une fois que vous
vous déplacez ou entreprenez une action ou une réaction, vous devez vous
camoufler à nouveau pour bénéficier de cet avantage.



### Disparition

À partir du 14e niveau, vous pouvez utiliser l'action Se cacher comme
action bonus à votre tour. De plus, vous ne pouvez pas être suivi par
des moyens non-magiques, sauf si vous choisissez de laisser une trace.



### Sens sauvages

Au 18e niveau, vous gagnez des sens préternaturels qui vous aident à
combattre les créatures que vous ne pouvez pas voir. Lorsque vous
attaquez une créature que vous ne pouvez pas voir, votre incapacité à la
voir n'impose pas de désavantage sur vos jets d'attaque contre elle.
Vous êtes également conscient de l'emplacement de toute créature
invisible située à moins de 30 pieds de vous, à condition que la
créature ne soit pas cachée et que vous ne soyez pas aveuglé ou
assourdi.



### Tueur d'ennemis

Au 20e niveau, vous devenez un Chasseur hors pair de vos ennemis. Une
fois à chacun de vos tours, vous pouvez ajouter votre modificateur de
Sagesse au jet d'attaque ou au jet de dégâts d'une attaque que vous
effectuez contre l'un de vos ennemis privilégiés. Vous pouvez choisir
d'utiliser cette caractéristique avant ou après le jet, mais avant que
les effets du jet ne soient appliqués.





### Archétypes du rôdeur ###

Le Chasseur est une expression classique de l'idéal du rôdeur.



#### Chasseur ####

Emprunter l'archétype du Chasseur signifie accepter votre place en tant
que rempart entre la civilisation et les terreurs de la nature sauvage.
En suivant la voie du Chasseur, vous apprenez des techniques
spécialisées pour combattre les menaces auxquelles vous êtes confronté,
des ogres déchaînés et des hordes d'orcs aux géants imposants et aux
dragons terrifiants.



##### Proie de chasseur #####

Au 3e niveau, vous gagnez une des capacités suivantes de votre choix.

***Tueur de colosses.*** Votre ténacité peut user les ennemis les plus
puissants. Lorsque vous touchez une créature avec une attaque d'arme,
celle-ci subit 1d8 points de dégâts supplémentaires si elle est en
dessous de son maximum de points de vie. Vous ne pouvez infliger ces
dégâts supplémentaires qu'une fois par tour.

***Tueur de géants .*** Lorsqu'une créature Grand ou plus grande située
à 1,5 mètre de vous vous touche ou vous rate avec une attaque, vous
pouvez utiliser votre réaction pour attaquer cette créature
immédiatement après son attaque, à condition que vous puissiez voir la
créature.

***Briseur de horde.*** Une fois à chacun de vos tours, lorsque vous
effectuez une attaque avec une arme, vous pouvez effectuer une autre
attaque avec la même arme contre une créature différente qui se trouve à
moins de 1,5 m de la cible initiale et à portée de votre arme.




##### Tactiques défensives #####

Au 7ème niveau, vous gagnez une des capacités suivantes de votre choix.

***Echapper à la Horde.*** Les attaques d'opportunité contre vous sont
faites avec un désavantage.

***Défense Attaques multiples.*** Lorsqu'une créature vous touche avec
une attaque, vous gagnez un bonus de +4 à la CA contre toutes les
attaques suivantes de cette créature pour le reste du tour.

***Volonté d'acier.*** Vous avez un avantage sur les jets de sauvegarde
contre l'effroi.




##### Attaques multiples #####

Au 11ème niveau, vous gagnez une des capacités suivantes de votre choix.

***Volée.*** Vous pouvez utiliser votre action pour effectuer une
attaque à distance contre un nombre quelconque de créatures situées à 3
mètres d'un point que vous pouvez voir dans la portée de votre arme.
Vous devez avoir des munitions pour chaque cible, comme d'habitude, et
vous faites un jet d'attaque séparé pour chaque cible.

***Attaque tourbillon.*** Vous pouvez utiliser votre action pour
effectuer une attaque de mêlée contre un nombre quelconque de créatures
situées à moins de 1,5 m de vous, avec un jet d'attaque distinct pour
chaque cible.




##### Défense du chasseur suprême #####

Au 15ème niveau, vous gagnez l'une des caractéristiques suivantes de
votre choix.

***Dérobade.*** Lorsque vous subissez un effet, comme le souffle ardent
d'un dragon rouge ou un sort de foudre, qui vous permet de faire un jet
de sauvegarde de Dextérité pour ne subir que la moitié des dégâts, vous
ne subissez aucun dégât si vous réussissez le jet de sauvegarde, et
seulement la moitié des dégâts si vous échouez.

***Tenir tête à la marée.*** Lorsqu'une créature hostile vous rate avec
une attaque de mêlée, vous pouvez utiliser votre réaction pour forcer
cette créature à répéter la même attaque contre une autre créature
(autre qu'elle-même) de votre choix.

***Esquive instinctive.*** Lorsqu'un attaquant que vous pouvez voir
vous touche avec une attaque, vous pouvez utiliser votre réaction pour
réduire de moitié les dégâts de l'attaque contre vous.





## Roublard


### Capacités de la classe

  ----------------------------------------------------------------------------
   Niveau      Bonus de       Attaque   Capacités
               maîtrise      sournoise  
            supplémentaire              
  -------- ---------------- ----------- --------------------------------------
    1er           +2            1d6     Expertise, Attaque sournoise, Jargon
                                        des voleurs

    2ème          +2            1d6     Geste sournois

    3ème          +2            2d6     Archétype de roublard

    4ème          +2            2d6     Amélioration de la valeur de
                                        caractéristiques

    5ème          +3            3d6     Esquive instinctive

    6ème          +3            3d6     Expertise

    7ème          +3            4d6     Dérobade

    8ème          +3            4d6     Amélioration de la valeur de
                                        caractéristiques

    9ème          +4            5d6     Capacité de l'Archétype du roublard

   10ème          +4            5d6     Amélioration de la valeur de
                                        caractéristiques

   11ème          +4            6d6     Savoir-faire fiable

   12ème          +4            6d6     Amélioration de la valeur de
                                        caractéristiques

   13ème          +5            7d6     Capacité de l'Archétype du roublard

   14ème          +5            7d6     Sens aveugle

   15ème          +5            8d6     Esprit fuyant

   16ème          +5            8d6     Amélioration de la valeur de
                                        caractéristiques

   17ème          +6            9d6     Capacité de l'Archétype du roublard

   18ème          +6            9d6     Insaisissable

   19ème          +6           10d6     Amélioration de la valeur de
                                        caractéristiques

   20ème          +6           10d6     Coup de chance
  ----------------------------------------------------------------------------

  : Le Roublard

En tant que roublard, vous avez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau de roublard.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau de roublard après le 1er



#### Maîtrises

**Armure :** Armure légère.

**Armes :** Armes simples, arbalètes, épées longues, rapières, épées
courtes.

**Outils :** Outils des voleurs.

**Jets de sauvegarde :** Dextérité, Intelligence

**Compétences :** Choisissez-en quatre parmi les suivantes : Acrobaties,
Athlétisme, Discrétion, Investigation, Perception, Représentation,
Persuasion, Habileté manuelle et Furtivité.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une rapière ou*(b*) une épée courte.
-   *(a)* un arc court et un carquois de 20 flèches ou*(b*) une épée
    courte.
-   (*a)* un sac de cambrioleur,*b*) un sac de donjon, ou*c*) un sac
    d'explorateur.
-   *(a*) Une armure en cuir, deux dagues et des outils de voleur.




### Expertise

Au 1er niveau, choisissez deux de vos compétences, ou une de vos
compétences et votre maîtrise des outils de voleur. Votre bonus de
maîtrise est doublé pour tous les tests d'aptitude que vous effectuez
et qui utilisent l'une ou l'autre des compétences choisies.

Au 6ème niveau, vous pouvez choisir deux autres de vos maîtrises (en
compétences ou avec des outils de voleur) pour obtenir cet avantage.



### Attaque sournoise

À partir du 1er niveau, vous savez comment frapper subtilement et
exploiter la distraction d'un ennemi. Une fois par tour, vous pouvez
infliger 1d6 points de dégâts supplémentaires à une créature que vous
touchez avec une attaque si vous avez l'avantage au jet d'attaque.
L'attaque doit utiliser une finesse ou une arme à distance.

Vous n'avez pas besoin de l'avantage au jet d'attaque si un autre
ennemi de la cible se trouve à moins de 1,5 m de celle-ci, si cet ennemi
n'est pas frappé d'incapacité et si vous n'avez pas de désavantage au
jet d'attaque.

Le montant des dégâts supplémentaires augmente à mesure que vous gagnez
des niveaux dans cette classe, comme l'indique la colonne Attaque
sournoise de la table des Roublards.



### Jargon des voleurs

Pendant votre formation de roublard, vous avez appris le jargon des
voleurs, un mélange secret de dialecte, de jargon et de code qui vous
permet de dissimuler des messages dans une conversation apparemment
normale. Seule une autre créature connaissant le jargon des voleurs peut
comprendre de tels messages. Il faut quatre fois plus de temps pour
transmettre un tel message que pour exprimer la même idée en clair.

De plus, vous comprenez un ensemble de signes et de symboles secrets
utilisés pour transmettre des messages courts et simples, comme par
exemple si une zone est dangereuse ou le territoire d'une guilde de
voleurs, si le butin est à proximité, ou si les habitants d'une zone
sont des cibles faciles ou constitueront un refuge pour les voleurs en
fuite.



### Geste sournois

À partir du 2e niveau, votre vivacité d'esprit et votre agilité vous
permettent de vous déplacer et d'agir rapidement. Vous pouvez effectuer
une action bonus à chacun de vos tours en combat. Cette action ne peut
être utilisée que pour prendre l'action Foncer, Se désengager ou Se
cacher.



### Archétype de roublard

Au 3e niveau, vous choisissez un archétype que vous imitez dans
l'exercice de vos capacités de roublard. Votre choix d'archétype vous
octroie des caractéristiques au 3e niveau, puis à nouveau aux 9e, 13e et
17e niveaux.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 10e, 12e, 16e
et 19e niveau, vous pouvez augmenter un score de capacité de votre choix
de 2, ou vous pouvez augmenter deux scores de capacité de votre choix
de 1. Comme d'habitude, vous ne pouvez pas augmenter un score de
capacité au-dessus de 20 en utilisant cette caractéristique.



### Esquive instinctive

À partir du 5e niveau, lorsqu'un attaquant que vous pouvez voir vous
touche avec une attaque, vous pouvez utiliser votre réaction pour
réduire de moitié les dégâts de l'attaque contre vous.



### Dérobade

À partir du 7e niveau, vous pouvez esquiver avec agilité certains effets
de zone, comme le souffle ardent d'un dragon rouge ou un sort de
*tempête de glace*. Lorsque vous êtes soumis à un effet
qui vous permet de faire un jet de sauvegarde de Dextérité pour ne subir
que la moitié des dégâts, vous ne subissez à la place aucun dégât si
vous réussissez le jet de sauvegarde, et seulement la moitié des dégâts
si vous échouez.



### Savoir-faire fiable

Au 11e niveau, vous avez affiné vos compétences jusqu'à ce qu'elles
approchent la perfection. Chaque fois que vous effectuez un test
d'aptitude qui vous permet d'ajouter votre bonus de caractéristique,
vous pouvez considérer un résultat de 9 ou moins au d20 comme un 10.



### Sens aveugle

A partir du 14ème niveau, si vous êtes capable d'entendre, vous savez
où se trouve toute créature cachée ou invisible à moins de 3 mètres de
vous.



### Esprit fuyant

Au 15ème niveau, vous avez acquis une plus grande force mentale. Vous
gagnez la maîtrise des jets de sauvegarde de Sagesse.



### Insaisissable

À partir du 18e niveau, vous êtes si évasif que les attaquants ont
rarement le dessus sur vous. Aucun jet d'attaque n'a d'avantage
contre vous tant que vous n'êtes pas frappé d'incapacité.



### Coup de chance

Au 20ème niveau, vous avez un don étrange pour réussir quand vous en
avez besoin. Si votre attaque manque une cible à portée, vous pouvez
transformer ce manque en réussite. De même, si vous échouez à un test de
capacité, vous pouvez considérer le résultat du d20 comme un 20.

Une fois que vous avez utilisé cette fonction, vous ne pouvez plus
l'utiliser jusqu'à ce que vous ayez terminé un repos court ou long.





### Archétypes de roublards ###

Les roublards ont de nombreuses capacités en commun, notamment
l'importance qu'ils accordent au perfectionnement de leurs
compétences, leur approche précise et mortelle du combat et leurs
réflexes de plus en plus rapides. Mais les différents roublards dirigent
ces talents dans des directions différentes, incarnées par les
archétypes de roublard. Votre choix d'archétype est le reflet de vos
centres d'intérêt, pas nécessairement une indication de la profession
que vous avez choisie, mais une description de vos techniques préférées.



#### Voleur ####

Vous perfectionnez vos compétences dans l'art du larcin. Les
cambrioleurs, les bandits, les coupeurs de route et autres criminels
suivent généralement cet archétype, mais aussi les roublards qui
préfèrent se considérer comme des chercheurs de trésors, des
explorateurs, des fouilleurs et des investigateurs professionnels. En
plus d'améliorer votre agilité et votre discrétion, vous apprenez des
compétences utiles pour fouiller des ruines anciennes, lire des langues
inconnues et utiliser des objets magiques que vous ne pourriez
normalement pas employer.



##### Gestes précis #####

À partir du 3e niveau, vous pouvez utiliser l'action bonus accordée par
votre Geste sournois pour effectuer un test de Dextérité (habileté
manuelle), utiliser vos outils de voleur pour désarmer un piège ou
ouvrir une serrure, ou effectuer l'action Utiliser un objet.




##### Grimpeur-né #####

Lorsque vous choisissez cet archétype au 3e niveau, vous gagnez la
capacité de grimper plus rapidement que la normale ; grimper ne vous
coûte plus de mouvement supplémentaire.

De plus, lorsque vous effectuez un saut en courant, la distance que vous
parcourez augmente d'un nombre de pieds égal à votre modificateur de
Dextérité.




##### Furtivité suprême #####

À partir du 9e niveau, vous avez un avantage sur un test de Dextérité
(Discrétion) si vous ne vous déplacez pas plus de la moitié de votre
vitesse dans le même tour.




##### Utilisation d'objets magiques #####

Au 13e niveau, vous avez suffisamment appris le fonctionnement de la
magie pour pouvoir improviser l'utilisation d'objets même s'ils ne
vous sont pas destinés. Vous ignorez toutes les exigences de classe, de
origine et de niveau concernant l'utilisation des objets magiques.




##### Réflexes de voleur #####

Lorsque vous atteignez le 17e niveau, vous êtes devenu adepte des
embuscades et de la fuite rapide du danger. Vous pouvez faire deux tours
pendant le premier round de n'importe quel combat. Vous prenez votre
premier tour à votre initiative normale et votre second tour à votre
initiative moins 10. Vous ne pouvez pas utiliser cette caractéristique
lorsque vous êtes surpris.





## Sorcier


### Capacités de la classe

  ----------------------------------------------------------------------------------------
   Niveau      Bonus de       Points de   Capacités                      Tours de  Sorts
               maîtrise      sorcellerie                                  magie    connus
            supplémentaire                                                connus  
  -------- ---------------- ------------- ------------------------------ -------- --------
    1er           +2             \-       Lanceur de sorts, Origine         4        2
                                          magique                                 

    2ème          +2              2       Source de magie                   4        3

    3ème          +2              3       Métamagie                         4        4

    4ème          +2              4       Amélioration de la valeur de      5        5
                                          caractéristiques                        

    5ème          +3              5       \-                                5        6

    6ème          +3              6       Capacité Origine magique          5        7

    7ème          +3              7       \-                                5        8

    8ème          +3              8       Amélioration de la valeur de      5        9
                                          caractéristiques                        

    9ème          +4              9       \-                                5        10

   10ème          +4             10       Métamagie                         6        11

   11ème          +4             11       \-                                6        12

   12ème          +4             12       Amélioration de la valeur de      6        12
                                          caractéristiques                        

   13ème          +5             13       \-                                6        13

   14ème          +5             14       Capacité Origine magique          6        13

   15ème          +5             15       \-                                6        14

   16ème          +5             16       Amélioration de la valeur de      6        14
                                          caractéristiques                        

   17ème          +6             17       Métamagie                         6        15

   18ème          +6             18       Capacité Origine magique          6        15

   19ème          +6             19       Amélioration de la valeur de      6        15
                                          caractéristiques                        

   20ème          +6             20       Régénération magique              6        15
  ----------------------------------------------------------------------------------------

  : Le sorcier

En tant que sorcier, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d6 par niveau de sorcier.

**Points de vie au 1er niveau :** 6 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d6 (ou 4) + votre
modificateur de Constitution par niveau de sorcier après le 1er



#### Maîtrises

**Armure :** Aucune

**Armes :** Dagues, fléchettes, frondes, bâtons, arbalètes légères.

**Outils :** Aucun

**Jets de sauvegarde :** Constitution, Charisme

**Compétences :** Choisissez deux compétences parmi les suivantes :
Arcane, Supercherie, Intuition, Intimidation, Persuasion et Religion.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une arbalète légère et 20 carreaux ou*(b*) toute arme
    courante.
-   *(a)* une pochette de composants ou*(b)* un arcane.
-   *(a)* un sac de donjon ou*(b*) un sac d'explorateur.
-   Deux dagues




### Incantation

Un événement dans votre passé, ou dans la vie d'un parent ou d'un
ancêtre, a laissé une marque indélébile sur vous, vous insufflant de la
magie des arcanes. Cette source de magie, quelle que soit son origine,
alimente vos sorts.


#### Tours de magie

Au 1er niveau, vous connaissez quatre tours de magie de votre choix dans
la liste des sorts de sorcier. Vous apprenez des tours de magie
supplémentaires de votre choix à des niveaux supérieurs, comme indiqué
dans la colonne Tours de magie connus de la table Sorcier.



#### Emplacements de sorts

  ----------------------------------------------------------------------------------
  Niveau de  Niveau                                                          
   sorcier    des                                                            
             sorts                                                           
  --------- -------- ------- ------- ------- ------- ------- ------- ------- -------
              1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

     1er       2       \-      \-      \-      \-      \-      \-      \-      \-

    2ème       3       \-      \-      \-      \-      \-      \-      \-      \-

    3ème       4        2      \-      \-      \-      \-      \-      \-      \-

    4ème       4        3      \-      \-      \-      \-      \-      \-      \-

    5ème       4        3       2      \-      \-      \-      \-      \-      \-

    6ème       4        3       3      \-      \-      \-      \-      \-      \-

    7ème       4        3       3       1      \-      \-      \-      \-      \-

    8ème       4        3       3       2      \-      \-      \-      \-      \-

    9ème       4        3       3       3       1      \-      \-      \-      \-

    10ème      4        3       3       3       2      \-      \-      \-      \-

    11ème      4        3       3       3       2       1      \-      \-      \-

    12ème      4        3       3       3       2       1      \-      \-      \-

    13ème      4        3       3       3       2       1       1      \-      \-

    14ème      4        3       3       3       2       1       1      \-      \-

    15ème      4        3       3       3       2       1       1       1      \-

    16ème      4        3       3       3       2       1       1       1      \-

    17ème      4        3       3       3       2       1       1       1       1

    18ème      4        3       3       3       3       1       1       1       1

    19ème      4        3       3       3       3       2       1       1       1

    20ème      4        3       3       3       3       2       2       1       1
  ----------------------------------------------------------------------------------

  : Emplacements de sorts de sorcier par niveau

Le tableau Emplacements de sorts de sorcier par niveau indique le nombre
d'emplacements de sorts dont vous disposez pour lancer vos sorts de 1er
niveau et plus. Pour lancer l'un de ces sorts de sorcier, vous devez
dépenser un emplacement du niveau du sort ou plus. Vous récupérez tous
les emplacements de sorts dépensés lorsque vous terminez un long repos.

Par exemple, si vous connaissez le sort *mains
brûlantes* de 1er niveau et que vous disposez d'un
emplacement de 1er niveau et d'un emplacement de 2ème niveau, vous
pouvez lancer le sort *mains brûlantes* en utilisant
l'un ou l'autre de ces emplacements.



#### Sorts connus de 1er niveau et plus

Vous connaissez deux sorts de 1er niveau de votre choix dans la liste
des sorts de sorcier.

La colonne Sorts connus de la table des sorciers indique quand vous
apprenez d'autres sorts de sorcier de votre choix. Chacun de ces sorts
doit être d'un niveau pour lequel vous avez des emplacements de sorts.
Par exemple, lorsque vous atteignez le 3e niveau dans cette classe, vous
pouvez apprendre un nouveau sort de 1er ou 2e niveau.

De plus, lorsque vous gagnez un niveau dans cette classe, vous pouvez
choisir un des sorts de sorcier que vous connaissez et le remplacer par
un autre sort de la liste des sorts de sorcier, qui doit également être
d'un niveau pour lequel vous avez des emplacements de sorts.



#### Caractéristique d'Incantation

Le Charisme est votre caractéristique d'incantation pour vos sorts de
sorcier, puisque la puissance de votre magie repose sur votre capacité à
projeter votre volonté dans le monde. Vous utilisez votre Charisme
chaque fois qu'un sort fait référence à votre caractéristique
d'incantation. De plus, vous utilisez votre modificateur de Charisme
pour déterminer le jet de sauvegarde d'un sort de sorcier que vous
lancez et pour effectuer un jet d'attaque avec un sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Charisme.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Charisme.
  -----------------------------------------------------------------------



#### Focalisation des lanceurs de sorts

Vous pouvez utiliser un arcane comme foyer d'incantation pour vos sorts
de sorcier.




### Origine magique

Choisissez une origine magique , qui décrit la source de votre
pouvoir magique inné.

Votre choix vous octroie des capacités lorsque vous le choisissez au 1er
niveau et à nouveau au 6ème, 14ème et 18ème niveau.



### Source de magie

Au 2ème niveau, vous puisez dans une profonde source de magie en vous.
Cette source est représentée par des points de sorcellerie, qui vous
permettent de créer une variété d'effets magiques.


#### Points de sorcellerie

Vous avez 2 points de sorcellerie, et vous en gagnez d'autres au fur et
à mesure que vous atteignez des niveaux plus élevés, comme indiqué dans
la colonne Points de sorcellerie de la table Sorcier. Vous ne pouvez
jamais avoir plus de points de sorcellerie que ce qui est indiqué sur la
table correspondant à votre niveau. Vous récupérez tous les points de
sorcellerie dépensés lorsque vous terminez un long repos.



#### Moulage flexible

Vous pouvez utiliser vos points de sorcellerie pour obtenir des
emplacements de sorts supplémentaires, ou sacrifier des emplacements de
sorts pour obtenir des points de sorcellerie supplémentaires. Vous
apprenez d'autres façons d'utiliser vos points de sorcellerie lorsque
vous atteignez des niveaux plus élevés.

***Créer des emplacements de sorts.*** Vous pouvez transformer des
points de sorcellerie non dépensés en un emplacement de sort en tant
qu'action bonus à votre tour. La table de création d'emplacements de
sorts indique le coût de création d'un emplacement de sort d'un niveau
donné. Vous ne pouvez pas créer d'emplacements de sorts d'un niveau
supérieur à 5.

Tout emplacement de sort que vous créez avec cette caractéristique
disparaît lorsque vous terminez un repos long.

  -----------------------------
    Niveau des   Coût en points
   emplacements  de sorcellerie
     de sorts    
  -------------- --------------
       1er             2

       2ème            3

       3ème            5

       4ème            6

       5ème            7
  -----------------------------

  : Création d'emplacements de sorts

***Conversion d'un emplacement de sort en points de sorcellerie.***
Comme action bonus à votre tour, vous pouvez dépenser un emplacement de
sort et gagner un nombre de points de sorcellerie égal au niveau de
l'emplacement.




### Métamagie

Au 3e niveau, vous gagnez la capacité de tordre vos sorts pour les
adapter à vos besoins. Vous gagnez deux des options de Métamagie
suivantes de votre choix. Vous en gagnez une autre au 10ème et 17ème
niveau.

Vous ne pouvez utiliser qu'une seule option de Métamagie sur un sort
lorsque vous le lancez, sauf indication contraire.


#### Sort méticuleux

Lorsque vous lancez un sort qui oblige d'autres créatures à effectuer
un jet de sauvegarde, vous pouvez protéger certaines de ces créatures de
la pleine force du sort. Pour ce faire, vous dépensez 1 point de
sorcellerie et choisissez un nombre de ces créatures égal à votre
modificateur de Charisme (au moins une créature). Une créature choisie
réussit automatiquement son jet de sauvegarde contre le sort.



#### Sort ample

Lorsque vous lancez un sort dont la portée est de 1,5 m ou plus, vous
pouvez dépenser 1 point de sorcellerie pour doubler la portée du sort.

Lorsque vous lancez un sort qui a une portée de toucher, vous pouvez
dépenser 1 point de sorcellerie pour que la portée du sort soit de 30
pieds.



#### Sort renforcé

Lorsque vous effectuez un jet de dégâts pour un sort, vous pouvez
dépenser 1 point de sorcellerie pour relancer un nombre de dés de dégâts
égal à votre modificateur de Charisme (minimum de un). Vous devez
utiliser les nouveaux jets.

Vous pouvez utiliser le sort renforcé même si vous avez déjà utilisé une
autre option de Métamagie pendant le lancement du sort.



#### Sort étendu

Lorsque vous lancez un sort dont la durée est de 1 minute ou plus, vous
pouvez dépenser 1 point de sorcellerie pour doubler sa durée, jusqu'à
une durée maximale de 24 heures.



#### Sort intensifié

Lorsque vous lancez un sort qui force une créature à faire un jet de
sauvegarde pour résister à ses effets, vous pouvez dépenser 3 points de
sorcellerie pour donner à une cible du sort un désavantage sur son
premier jet de sauvegarde effectué contre le sort.



#### Sort accéléré

Lorsque vous lancez un sort dont le temps d'incantation est de 1
action, vous pouvez dépenser 2 points de sorcellerie pour changer le
temps d'incantation en 1 action bonus pour cette incantation.



#### Sort subtil

Lorsque vous lancez un sort, vous pouvez dépenser 1 point de sorcellerie
pour le lancer sans aucune composante somatique ou verbale.



#### Sort jumeau

Lorsque vous lancez un sort qui ne cible qu'une seule créature et n'a
pas de portée de soi, vous pouvez dépenser un nombre de points de
sorcellerie égal au niveau du sort pour cibler une deuxième créature à
portée avec le même sort (1 point de sorcellerie si le sort est un
cantrip).

Pour être éligible, un sort doit être incapable de cibler plus d'une
créature au niveau actuel du sort. Par exemple, Projectile
*magique* ne sont
pas éligibles, mais *Rayon de givre* et *Doigt de
mort* le sont.




### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Régénération magique

Au 20e niveau, vous regagnez 4 points de sorcellerie dépensés chaque
fois que vous terminez un repos court.





### Origines magiques du Sorcier ###

Différents sorciers revendiquent différentes origines pour leur magie
innée.



#### Lignée draconique ####

Votre magie innée provient de la magie draconique qui s'est mélangée à
votre sang ou à celui de vos ancêtres. Le plus souvent, les sorciers
ayant cette origine remontent à un puissant sorcier des temps anciens
qui a fait un pacte avec un dragon ou qui a peut-être même revendiqué un
parent dragon. Certaines de ces lignées sont bien établies dans le
monde, mais la plupart sont obscures. Tout sorcier peut être le premier
d'une nouvelle lignée, à la suite d'un pacte ou d'une autre
circonstance exceptionnelle.



##### Ancêtre draconique #####

Au 1er niveau, vous choisissez un type de dragon comme ancêtre. Le type
de dégâts associé à chaque dragon est utilisé par les caractéristiques
que vous gagnez par la suite.

  ---------------------
  Dragon   Type de
           dommage
  -------- ------------
  Noir     Acide

  Bleu     La foudre

  Laiton   Feu

  Bronze   La foudre

  Cuivre   Acide

  Or       Feu

  Vert     Empoisonné

  Rouge    Feu

  Argent   Froid

  Blanc    Froid
  ---------------------

  : Ascendance draconique

Vous pouvez parler, lire et écrire le draconique. De plus, lorsque vous
effectuez un test de Charisme en interaction avec des dragons, votre
bonus de maîtrise est doublé s'il s'applique au test.




##### Résistance draconique #####

Lorsque la magie circule dans votre corps, elle fait apparaître les
traits physiques de vos ancêtres dragons. Au 1er niveau, votre maximum
de points de vie augmente de 1 et augmente à nouveau de 1 à chaque fois
que vous gagnez un niveau dans cette classe.

De plus, certaines parties de votre peau sont recouvertes d'une fine
couche d'écailles semblables à celles d'un dragon. Lorsque vous ne
portez pas d'armure, votre CA est égale à 13 + votre modificateur de
Dextérité.




##### Affinité élémentaire #####

À partir du 6e niveau, lorsque vous lancez un sort qui inflige des
dégâts du type associé à votre ascendance draconique, vous pouvez
ajouter votre modificateur de Charisme à un jet de dégâts de ce sort.
Dans le même temps, vous pouvez dépenser 1 point de sorcellerie pour
acquérir une résistance à ce type de dégâts pendant 1 heure.




##### Ailes de dragon #####

Au 14e niveau, vous gagnez la capacité de faire jaillir une paire
d'ailes de dragon de votre dos, gagnant une vitesse de vol égale à
votre vitesse actuelle. Vous pouvez créer ces ailes en tant qu'action
bonus à votre tour. Elles durent jusqu'à ce que vous les retiriez en
tant qu'action bonus à votre tour.

Vous ne pouvez pas manifester vos ailes lorsque vous portez une armure,
sauf si celle-ci est conçue pour les accueillir, et les vêtements qui ne
sont pas conçus pour accueillir vos ailes risquent d'être détruits
lorsque vous les manifestez.




##### Présence draconique #####

À partir du 18e niveau, vous pouvez canaliser la présence redoutable de
votre ancêtre dragon, ce qui a pour effet d'impressionner ou
d'effrayer ceux qui vous entourent. En tant qu'action, vous pouvez
dépenser 5 points de sorcellerie pour puiser dans ce pouvoir et dégager
une aura de crainte ou de peur (au choix) sur une distance de 60 pieds.
Pendant 1 minute ou jusqu'à ce que vous perdiez votre concentration
(comme si vous lanciez un sort de concentration), chaque créature
hostile qui commence son tour dans cette aura doit réussir un jet de
sauvegarde de Sagesse ou être charmée (si vous avez choisi la crainte)
ou effrayée (si vous avez choisi la peur) jusqu'à ce que l'aura prenne
fin. Une créature qui réussit ce jet de sauvegarde est immunisée contre
votre aura pendant 24 heures.





## Invocateur


### Capacités de la classe

  ------------------------------------------------------------------------------------
   Niveau      Bonus de     Capacités                  Tours de  Sorts    Invocations
               maîtrise                                 magie    connus     connues
            supplémentaire                              connus           
  -------- ---------------- -------------------------- -------- -------- -------------
    1er           +2        Protecteur venu d'AIlleurs,     2        2          \-
                            Magie de pacte                               

    2ème          +2        Manifestations                2        3           2
                            traumaturgiques                              

    3ème          +2        Faveur de pacte               2        4           2

    4ème          +2        Amélioration de la valeur     3        5           2
                            de caractéristiques                          

    5ème          +3        \-                            3        6           3

    6ème          +3        Fonction Protecteur        3        7           3
                            venu d'Ailleurs                                  

    7ème          +3        \-                            3        8           4

    8ème          +3        Amélioration de la valeur     3        9           4
                            de caractéristiques                          

    9ème          +4        \-                            3        10          5

   10ème          +4        Fonction Protecteur         4        10          5
                            venu d'Ailleurs                                  

   11ème          +4        Arcanum mystique (6e          4        11          5
                            niveau)                                      

   12ème          +4        Amélioration de la valeur     4        11          6
                            de caractéristiques                          

   13ème          +5        Arcanum mystique (7e          4        12          6
                            niveau)                                      

   14ème          +5        Fonction Protecteur         4        12          6
                            venu d'Ailleurs                                  

   15ème          +5        Arcanum mystique (8e          4        13          7
                            niveau)                                      

   16ème          +5        Amélioration de la valeur     4        13          7
                            de caractéristiques                          

   17ème          +6        Arcanum mystique (9e          4        14          7
                            niveau)                                      

   18ème          +6        \-                            4        14          8

   19ème          +6        Amélioration de la valeur     4        15          8
                            de caractéristiques                          

   20ème          +6        Maître du Traumaturge         4        15          8
  ------------------------------------------------------------------------------------

  : L'Invocateur

En tant que sorcier, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d8 par niveau d'Invocateur.

**Points de vie au 1er niveau :** 8 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d8 (ou 5) + votre
modificateur de Constitution par niveau d'Invocateur après le 1er



#### Maîtrises

**Armure :** Armure légère.

**Armes :** Armes courantes

**Outils :** Aucun

**Jets de sauvegarde :** Sagesse, Charisme

**Compétences :** Choisissez deux compétences parmi les suivantes :
Arcane, Mystère, Histoire, Intimidation, Investigation, Nature et
Religion.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* une arbalète légère et 20 carreaux ou*(b*) toute arme
    courante.
-   *(a)* une pochette de composants ou*(b)* un arcane.
-   *(a)* un sac d'érudit ou*(b*) un sac de donjon.
-   Une armure en cuir, une arme courante et deux dagues.




### Protecteur venu d'Ailleurs

Au 1er niveau, vous avez conclu un marché avec un être venu d'Ailleurs
de votre choix, comme le Fiélon. Votre choix vous confère des capacités
au 1er niveau, puis au 6ème, 10ème et 14ème niveau.



### Magie de pacte

Vos recherches sur les arcanes et la magie que vous a conférée votre
protecteur vous ont permis d'acquérir une grande facilité
d'utilisation des sorts.


#### Tours de magie

Vous connaissez deux sorts de sorcier de votre choix dans la liste des
sorts de sorcier. Vous apprenez d'autres
tours de magie de sorcier de votre choix à des niveaux supérieurs, comme
indiqué dans la colonne Tours de magie connus de la table des sorciers.



#### Emplacements de sorts

  --------------------------------------------
     Niveau de     Emplacements  Niveau de la
    invocateur      de sorts        fente
  --------------- -------------- -------------
        1er             1             1er

       2ème             2             1er

       3ème             2            2ème

       4ème             2            2ème

       5ème             2            3ème

       6ème             2            3ème

       7ème             2            4ème

       8ème             2            4ème

       9ème             2            5ème

       10ème            2            5ème

       11ème            3            5ème

       12ème            3            5ème

       13ème            3            5ème

       14ème            3            5ème

       15ème            3            5ème

       16ème            3            5ème

       17ème            4            5ème

       18ème            4            5ème

       19ème            4            5ème

       20ème            4            5ème
  --------------------------------------------

  : Emplacements de sorts d'Invocateur par niveau

Le tableau des emplacements de sorts du invocateur par niveau indique
le nombre d'emplacements de sorts dont vous disposez. Le tableau
indique également le niveau de ces emplacements ; tous vos emplacements
de sorts sont de même niveau. Pour lancer un de vos sorts de sorcier de
1er niveau ou plus, vous devez dépenser un emplacement de sort. Vous
récupérez tous les emplacements de sorts dépensés lorsque vous terminez
un repos court ou long.

Par exemple, lorsque vous êtes au 5e niveau, vous disposez de deux
emplacements de sorts de 3e niveau. Pour lancer le sort *Vague tonnante
de* 1er niveau, vous devez dépenser un de ces
emplacements, et vous le lancez comme un sort de 3ème niveau.



#### Sorts connus de 1er niveau et plus

Au 1er niveau, vous connaissez deux sorts de 1er niveau de votre choix
dans la liste des sorts de sorcier.

La colonne Sorts connus de la table d'Invocateur indique quand vous
apprenez d'autres sorts de sorcier de votre choix, de 1er niveau ou
plus. Le sort que vous choisissez ne doit pas être d'un niveau
supérieur à ce qui est indiqué dans la colonne Slot Level de la table
Warlock pour votre niveau. Lorsque vous atteignez le 6e niveau, par
exemple, vous apprenez un nouveau sort de sorcier, qui peut être de 1er,
2e ou 3e niveau.

De plus, lorsque vous gagnez un niveau dans cette classe, vous pouvez
choisir un des sorts d'Invocateur que vous connaissez et le remplacer
par un autre sort de la liste des sorts d'Invocateur, qui doit
également être d'un niveau pour lequel vous avez des emplacements de
sorts.



#### Caractéristique d'Incantation

Le Charisme est votre caractéristique d'incantation pour vos sorts de
sorcier, donc vous utilisez votre Charisme chaque fois qu'un sort fait
référence à votre caractéristique d'incantation. De plus, vous utilisez
votre modificateur de Charisme pour déterminer le jet de sauvegarde
d'un sort de sorcier que vous lancez et pour effectuer un jet
d'attaque avec un tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                      votre modificateur de Charisme.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                         modificateur de Charisme.
  -----------------------------------------------------------------------



#### Lanceur de sorts Focus

Vous pouvez utiliser un foyer arcanique comme foyer d'incantation pour
vos sorts d'Invocateur.




### Manifestations traumaturgiques

Dans votre étude des traditions occultes, vous avez déterré des
manifestations traumaturgiques , des fragments de
connaissances interdites qui vous confèrent une capacité magique
permanente.

Au 2e niveau, vous gagnez deux invocations traumaturgiques eldritch de
votre choix. Lorsque vous gagnez certains niveaux de sorcier, vous
gagnez des invocations supplémentaires de votre choix, comme indiqué
dans la colonne Invocations connues de la table des sorciers.

De plus, lorsque vous gagnez un niveau dans cette classe, vous pouvez
choisir une des invocations que vous connaissez et la remplacer par une
autre invocation que vous pourriez apprendre à ce niveau.



### Faveur de pacte

Au 3e niveau, votre Patron de l'Autre Monde vous offre un cadeau pour vos
loyaux services. Vous gagnez une des capacités suivantes de votre choix.


#### Pacte de la chaîne

Vous apprenez le sort *trouver un familier* et pouvez
le lancer comme un rituel. Le sort ne compte pas dans votre nombre de
sorts connus.

Lorsque vous lancez le sort, vous pouvez choisir l'une des formes
normales de votre familier ou l'une des formes spéciales suivantes :
diablotin, pseudodragon, quasit ou esprit follet.

De plus, lorsque vous effectuez l'action Attaquer, vous pouvez renoncer
à l'une de vos propres attaques pour permettre à votre familier
d'effectuer une attaque de son côté avec sa réaction.



#### Pacte de la lame

Vous pouvez utiliser votre action pour créer une arme de pacte dans
votre main vide. Vous pouvez choisir la forme que prend cette arme de
mêlée à chaque fois que vous la créez. Vous en avez la maîtrise tant que
vous la maniez. Cette arme est considérée comme magique lorsqu'il
s'agit de surmonter la résistance et l'immunité aux attaques et aux
dégâts non magiques.

Votre arme de pacte disparaît si elle se trouve à plus de 1,5 mètre de
vous pendant 1 minute ou plus. Elle disparaît également si vous utilisez
à nouveau cette caractéristique, si vous renvoyez l'arme (aucune action
requise), ou si vous mourez.

Vous pouvez transformer une arme magique en votre arme de pacte en
effectuant un rituel spécial pendant que vous tenez l'arme. Vous
effectuez le rituel en 1 heure, ce qui peut être fait pendant un court
repos. Vous pouvez ensuite renvoyer l'arme, en la déplaçant dans un
espace extradimensionnel, et elle apparaît chaque fois que vous créez
votre arme de pacte par la suite. Vous ne pouvez pas affecter un
artefact ou une arme sensible de cette manière. L'arme cesse d'être
votre arme de pacte si vous mourez, si vous effectuez le rituel d'une
heure sur une autre arme, ou si vous utilisez un rituel d'une heure
pour rompre votre lien avec elle. L'arme apparaît à vos pieds si elle
se trouve dans l'espace extradimensionnel au moment de la rupture du
lien.



#### Pacte du grimoire

Votre mécène vous offre un grimoire appelé Livre des ombres. Lorsque
vous obtenez cette caractéristique, choisissez trois enchantements dans
la liste des sorts de n'importe quelle classe (les trois ne doivent pas
nécessairement provenir de la même liste). Tant que le livre est sur
vous, vous pouvez lancer ces sorts à volonté. Ils ne sont pas
comptabilisés dans votre nombre de sorts connus. S'ils n'apparaissent
pas sur la liste des sorts d'Invocateur, ils sont néanmoins des sorts
d'Invocateur pour vous.

Si vous perdez votre Livre des Ombres, vous pouvez effectuer une
cérémonie d'une heure pour recevoir un remplacement de la part de votre
mécène. Cette cérémonie peut être effectuée pendant un repos court ou
long, et elle détruit le livre précédent. Le livre se transforme en
cendres lorsque vous mourez.




### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Arcanum mystique

Au 11e niveau, votre patron vous confère un secret magique appelé
arcanum. Choisissez un sort de 6e niveau de la liste des sorts de
sorcier comme arcane.

Vous pouvez lancer votre sort d'arcane une fois sans dépenser un
emplacement de sort. Vous devez terminer un long repos avant de pouvoir
le faire à nouveau.

Aux niveaux supérieurs, vous gagnez plus de sorts d'Invocateur de
votre choix qui peuvent être lancés de cette façon : un sort de 7e
niveau au 13e niveau, un sort de 8e niveau au 15e niveau et un sort de
9e niveau au 17e niveau. Vous récupérez toutes les utilisations de votre
Arcanum mystique lorsque vous terminez un repos long.



### Maître du Traumaturge

Au 20e niveau, vous pouvez puiser dans votre réserve intérieure de
puissance mystique tout en implorant votre patron pour récupérer des
emplacements de sorts épuisés. Vous pouvez passer 1 minute à implorer
l'aide de votre patron pour récupérer tous vos emplacements de sorts
dépensés grâce à votre caractéristique Magie de pacte. Une fois que vous
avez regagné des emplacements de sorts avec cette caractéristique, vous
devez vous reposer longuement avant de pouvoir le faire à nouveau.




### Manifestations traumaturgiques

Si une invocation eldritch a des prérequis, vous devez les remplir pour
l'apprendre. Vous pouvez apprendre l'invocation en même temps que vous
remplissez ses prérequis. Un prérequis de niveau fait référence à votre
niveau dans cette classe.


#### Déflagration Douloureuse

*Prérequis : tour de magie de la décharge
*traumaturgique*.*

Lorsque vous lancez *Décharge traum*aturgique, ajoutez votre
modificateur de Charisme aux dégâts qu'elle inflige en cas de succès.



#### Armure spectrale

Vous pouvez lancer une *armure de mage* sur vous-même à
volonté, sans dépenser un emplacement de sort ou des composants
matériels.



#### Pas ascensionnel

*Prérequis : 9ème niveau*

Vous pouvez lancer la *lévitation* sur vous-même à volonté,
sans dépenser un emplacement de sort ou des composants matériels.



#### Langage des bêtes

Vous pouvez lancer Parler *avec les animaux* à
volonté, sans dépenser un emplacement de sort.



#### Emprise séduisante

Vous gagnez la maîtrise des compétences Supercherie et Persuasion.



#### Murmures envoûtants

*Prérequis : 7ème niveau*

Vous pouvez lancer une *compulsion* une fois en utilisant
un emplacement de sort de sorcier. Vous ne pouvez pas le faire à nouveau
avant d'avoir terminé un long repos.



#### Livre des Mystères Anciens

*Prérequis : La capacité Pacte du grimoire.*

Vous pouvez désormais inscrire des rituels magiques dans votre Livre des
Ombres. Choisissez deux sorts de 1er niveau avec l'étiquette rituel
dans la liste des sorts de n'importe quelle classe (les deux ne doivent
pas nécessairement être dans la même liste). Les sorts apparaissent dans
le livre et ne sont pas comptabilisés dans le nombre de sorts que vous
connaissez. Avec votre Livre des Ombres en main, vous pouvez lancer les
sorts choisis comme des rituels. Vous ne pouvez lancer les sorts qu'en
tant que rituels, à moins que vous ne les ayez appris par un autre
moyen. Vous pouvez également lancer un sort d'Invocateur que vous
connaissez sous forme de rituel s'il porte l'étiquette rituel.

Au cours de vos aventures, vous pouvez ajouter d'autres sorts rituels à
votre Livre des Ombres. Lorsque vous trouvez un tel sort, vous pouvez
l'ajouter au livre si le niveau du sort est égal ou inférieur à la
moitié de votre niveau d'Invocateur (arrondi au supérieur) et si vous
avez le temps de transcrire le sort. Pour chaque niveau du sort, le
processus de transcription prend 2 heures et coûte 50 gp pour les encres
rares nécessaires à l'inscription.



#### Chaînes du Deodand

*Prérequis : 15ème niveau, caractéristique Pacte de la chaîne.*

Vous pouvez lancer Immobilisation *de monstre* à
volonté - en ciblant un céleste, un fiélon ou un élémentaire - sans
dépenser un emplacement de sort ou des composantes matérielles. Vous
devez terminer un repos long avant de pouvoir utiliser à nouveau cette
invocation sur la même créature.



#### Regard du Diable

Vous pouvez voir normalement dans les Ténèbres, qu'elles soient
magiques ou non, jusqu'à une distance de 120 pieds.



#### Vocable Effroyable

*Prérequis : 7ème niveau*

Vous pouvez jeter de la *confusion* une fois en utilisant
un emplacement de sort de sorcier. Vous ne pouvez pas le faire à nouveau
avant d'avoir terminé un long repos.



#### Vision Traumaturgique

Vous pouvez lancer *Détection de la magie* à volonté,
sans dépenser un emplacement de sort.



#### Lance Traumaturgique

*Prérequis : tour de magie de la décharge
*traumaturgique*.*

Lorsque vous lancez *Décharge traumaturgique*, sa portée est de 300
pieds.



#### Regard du Gardien des Runes

Vous pouvez lire tous les écrits.



#### Vigueur fiélonne

Vous pouvez lancer un *simulacre de vie* sur vous-même à
volonté comme un sort de 1er niveau, sans dépenser un emplacement de
sort ou des composantes matérielles.



#### Regard de deux esprits

Vous pouvez utiliser votre action pour toucher un humanoïde consentant
et percevoir par ses sens jusqu'à la fin de votre prochain tour. Tant
que la créature se trouve sur le même plan d'existence que vous, vous
pouvez utiliser votre action aux tours suivants pour maintenir cette
connexion, prolongeant la durée jusqu'à la fin de votre prochain tour.
Lorsque vous percevez à travers les sens de l'autre créature, vous
bénéficiez de tous les sens spéciaux possédés par cette créature, et
vous êtes aveuglé et assourdi à votre propre environnement.



#### Buveuse de vie

*Prérequis : 12ème niveau, caractéristique du Pacte de la lame.*

Lorsque vous touchez une créature avec votre arme de pacte, celle-ci
subit des dégâts nécrotiques supplémentaires égaux à votre modificateur
de Charisme (minimum 1).



#### Masque des Multiples Faciès

Vous pouvez *vous déguiser* à volonté, sans dépenser
un emplacement de sort.



#### Maître d'une Myriade de Formes

*Prérequis : 15ème niveau*

Vous pouvez lancer *Modification d'apparence* à volonté,
sans dépenser un emplacement de sort.



#### Grouillots du chaos

*Prérequis : 9ème niveau*

Vous pouvez lancer Invocation *élémentaire* une
fois en utilisant un emplacement de sort de sorcier. Vous ne pouvez pas
le faire à nouveau avant d'avoir terminé un long repos.



#### Cibler l'esprit

*Prérequis : 5ème niveau*

Vous pouvez lancer *Lenteur* une fois en utilisant un
emplacement de sort de sorcier. Vous ne pouvez pas le faire à nouveau
avant d'avoir terminé un long repos.



#### Visions vaporeuses

Vous pouvez lancer Image *silencieuse* à volonté, sans
dépenser un emplacement de sort ou des composants matériels.



#### Uni avec les ombres

*Prérequis : 5ème niveau*

Lorsque vous vous trouvez dans une zone de lumière chétive ou de
ténèbres, vous pouvez utiliser votre action pour devenir invisible
jusqu'à ce que vous vous déplaciez ou que vous fassiez une action ou
une réaction.



#### Saut de l'Autre Monde

*Prérequis : 9ème niveau*

Vous pouvez lancer *Saut* sur vous-même à volonté, sans
dépenser un emplacement de sort ou des composants matériels.



#### Déflagration répulsive

*Prérequis : tour de magie de la décharge
*traumaturgique*.*

Lorsque vous touchez une créature avec *Décharge traum*aturgique, vous
pouvez la repousser jusqu'à 3 mètres de vous en ligne droite.



#### Sculpteur de chair

*Prérequis : 7ème niveau*

Vous pouvez lancer le sort *Métamorphose* une fois en
utilisant un emplacement de sort de sorcier. Vous ne pouvez pas le faire
à nouveau avant d'avoir terminé un long repos.



#### Sombre présage

*Prérequis : 5ème niveau*

Vous pouvez lancer un sort *de malédiction* une fois en
utilisant un emplacement de sort de sorcier. Vous ne pouvez pas le faire
à nouveau avant d'avoir terminé un long repos.



#### Voleur aux cinq destinées

Vous pouvez lancer *Fléau* une fois en utilisant un emplacement
de sort de sorcier. Vous ne pouvez pas le faire à nouveau avant d'avoir
terminé un long repos.



#### Lame insatiable

*Prérequis : 5ème niveau, caractéristique Pacte de la lame.*

Vous pouvez attaquer avec votre arme de pacte deux fois, au lieu d'une,
lorsque vous effectuez l'action Attaquer à votre tour.



#### Visions de royaumes lointains

*Prérequis : 15ème niveau*

Vous pouvez lancer l'*Oeil occulte* à volonté, sans
dépenser un emplacement de sort.



#### Parole du maître des Chaînes

*Prérequis : Caractéristique du Pacte de la chaîne.*

Vous pouvez communiquer télépathiquement avec votre familier et
percevoir à travers les sens de votre familier tant que vous êtes sur le
même plan d'existence. De plus, lorsque vous percevez à travers les
sens de votre familier, vous pouvez également parler à travers votre
familier avec votre propre voix, même si votre familier est normalement
incapable de parler.



#### Murmures de la Tombe

*Prérequis : 9ème niveau*

Vous pouvez lancer Parler *avec les morts* à
volonté, sans dépenser un emplacement de sort.



#### Vision de sorcier

*Prérequis : 15ème niveau*

Vous pouvez voir la vraie forme de n'importe quel métamorphe ou
créature dissimulée par la magie d'illusion ou de transmutation tant
que la créature est à moins de 30 pieds de vous et dans la ligne de vue.





### Patrons des Autres Mondes ###

Les êtres qui servent de parrains aux sorciers sont de puissants
habitants d'autres plans d'existence. Ce ne sont pas des dieux, mais
leur pouvoir est presque équivalent à celui des dieux. Divers mécènes
donnent à leurs sorciers l'accès à différents pouvoirs et invocations,
et attendent des faveurs importantes en retour.

Certains mécènes collectionnent les sorciers, dispensant leur savoir
mystique relativement librement ou se vantant de leur capacité à lier
les mortels à leur volonté. D'autres mécènes n'accordent leur pouvoir
qu'à contrecœur et peuvent conclure un pacte avec un seul sorcier. Les
sorciers qui servent le même patron peuvent se considérer comme des
alliés, des frères et sœurs ou des rivaux.



#### Le Fiélon ####

Vous avez fait un pacte avec un fiélon des plans inférieurs de
l'existence, un être dont les objectifs sont mauvais, même si vous vous
battez contre ces objectifs. Ces êtres désirent la corruption ou la
destruction de toute chose, y compris vous. Les fiélons suffisamment
puissants pour conclure un pacte sont les seigneurs démoniaques, les
archidémons, les fiélons des fosses et les balors qui sont
particulièrement puissants.



##### Liste élargie de sorts #####

Le Fiélon vous permet de choisir parmi une liste étendue de sorts
lorsque vous apprenez un sort de sorcier. Les sorts suivants sont
ajoutés à la liste des sorts de sorcier pour
vous.

  ----------------------------------------------------------
   Niveau des   Sorts
      sorts     
  ------------- --------------------------------------------
       1er      *Mains brûlantes*,
                *Injonction*

      2ème      *cécité/surdité*,
                *rayon ardent*

      3ème      *Boule de feu*, Nuage
                *nauséabond*

      4ème      *Bouclier de feu*, *Mur de
                feu*

      5ème      Colonne*de flamme*,
                *Sanctification*
  ----------------------------------------------------------

  : Sorts étendus du Fiélon




##### Bénédiction de l'Oscur #####

À partir du 1er niveau, lorsque vous réduisez une créature hostile à 0
point de vie, vous gagnez des points de vie temporaires égaux à votre
modificateur de Charisme + votre niveau d'Invocateur (minimum de 1).




##### La chance des Ténèbres #####

À partir du 6e niveau, vous pouvez faire appel à votre protecteur pour
modifier le destin en votre faveur. Lorsque vous effectuez un test de
capacité ou un jet de sauvegarde, vous pouvez utiliser cette
caractéristique pour ajouter un d10 à votre jet. Vous pouvez le faire
après avoir vu le jet initial mais avant que les effets du jet ne se
produisent.

Une fois que vous avez utilisé cette fonction, vous ne pouvez plus
l'utiliser jusqu'à ce que vous ayez terminé un repos court ou long.




##### Résistance fiélonne #####

À partir du 10e niveau, vous pouvez choisir un type de dégâts lorsque
vous terminez un repos court ou long. Vous gagnez une résistance à ce
type de dégâts jusqu'à ce que vous en choisissiez un autre avec cette
caractéristique. Les dégâts infligés par les armes magiques ou les armes
en argent ignorent cette résistance.




##### Jeter dans les enfers #####

À partir du 14e niveau, lorsque vous touchez une créature avec une
attaque, vous pouvez utiliser cette capacité pour transporter
instantanément la cible à travers les plans inférieurs. La créature
disparaît et se précipite dans un paysage de cauchemar.

À la fin de votre prochain tour, la cible retourne dans l'espace
qu'elle occupait précédemment, ou dans l'espace inoccupé le plus
proche. Si la cible n'est pas un fiélon, elle subit 10d10 points de
dégâts psychiques alors qu'elle se remet de son horrible expérience.

Une fois que vous avez utilisé cette fonction, vous ne pouvez plus
l'utiliser jusqu'à ce que vous ayez terminé un repos long.

> #### Votre Faveur de pacte {#your-pact-boon}
>
> Chaque option de Faveur du pacte produit une créature spéciale ou un
> objet qui reflète la nature de votre mécène.
>
> ***Pacte de la chaîne.*** Votre familier est plus rusé qu'un familier
> typique. Sa forme par défaut peut être le reflet de votre patron, avec
> des diablotins liés au Fiélon.
>
> ***Pacte de la lame.*** Si vous servez le Fiélon, votre arme peut être
> une hache en métal noir ornée de flammes décoratives.
>
> ***Pacte du grimoire.*** Votre Livre des Ombres pourrait être un tome
> lourd relié en peau de démon cloutée de fer, contenant des sorts de
> conjuration et une richesse de connaissances interdites sur les
> régions sinistres du cosmos, un cadeau du Fiélon.





## Magicien


### Capacités de la classe

  ------------------------------------------------------------------
   Niveau      Bonus de     Capacités                      Tours de
               maîtrise                                      magie
            supplémentaire                                  connus
  -------- ---------------- ------------------------------ ---------
    1er           +2        Lanceur de sorts, Régénération     3
                            occulte                        

    2ème          +2        Tradition occulte                  3

    3ème          +2        \-                                 3

    4ème          +2        Amélioration de la valeur de       4
                            caractéristiques               

    5ème          +3        \-                                 4

    6ème          +3        Fonctionnement de la Tradition     4
                            occulte                        

    7ème          +3        \-                                 4

    8ème          +3        Amélioration de la valeur de       4
                            caractéristiques               

    9ème          +4        \-                                 4

   10ème          +4        Fonctionnement de la Tradition     4
                            occulte                        

   11ème          +4        \-                                 5

   12ème          +4        Amélioration de la valeur de       5
                            caractéristiques               

   13ème          +5        \-                                 5

   14ème          +5        Fonctionnement de la Tradition     5
                            occulte                        

   15ème          +5        \-                                 5

   16ème          +5        Amélioration de la valeur de       5
                            caractéristiques               

   17ème          +6        \-                                 5

   18ème          +6        Maîtrise des sorts                 5

   19ème          +6        Amélioration de la valeur de       5
                            caractéristiques               

   20ème          +6        Sorts de prédilection              5
  ------------------------------------------------------------------

  : Le magicien

En tant que magicien, vous obtenez les capacités de classe suivantes.


#### Points de vie

**Dés de combat :** 1d6 par niveau de magicien.

**Points de vie au 1er niveau :** 6 + votre modificateur de Constitution

**Points de vie à des niveaux supérieurs :** 1d6 (ou 4) + votre
modificateur de Constitution par niveau de magicien après le 1er



#### Maîtrises

**Armure :** Aucune

**Armes :** Dagues, fléchettes, frondes, bâtons, arbalètes légères.

**Outils :** Aucun

**Jets de sauvegarde :** Intelligence, Sagesse

**Compétences :** Choisissez deux compétences parmi les suivantes :
Arcane, Histoire, Intuition, Investigation, Médecine et Religion.



#### Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement
accordé par votre historique :

-   *(a)* un bâton ou*(b*) une dague.
-   *(a)* une pochette de composants ou*(b)* un arcane.
-   *(a)* un sac d'écolier ou*(b*) un sac d'explorateur.
-   Un livre de sorts




### Incantation

En tant qu'étudiant en magie des arcanes, vous disposez d'un livre de
sorts contenant des sorts qui montrent les premières lueurs de votre
véritable pouvoir.


#### Tours de magie

Au 1er niveau, vous connaissez trois sorts de magie de votre choix dans
la liste des sorts de magicien. Vous apprenez des tours de magie
supplémentaires de votre choix à des niveaux supérieurs, comme indiqué
dans la colonne Tours de magie connus de la table des magiciens.



#### Livre de sorts

Au 1er niveau, vous disposez d'un livre de sorts contenant six sorts de
magicien de 1er niveau de votre choix. Votre livre de sorts est le
référentiel des sorts de magicien que vous connaissez, à l'exception de
vos cantrips, qui sont fixés dans votre esprit.



#### Préparer et lancer des sorts

  ---------------------------------------------------------------------------------
   Niveau   Niveau                                                          
   de Mag    des                                                            
  Mag Mag   sorts                                                           
    Mag                                                                     
  -------- -------- ------- ------- ------- ------- ------- ------- ------- -------
             1er     2ème    3ème    4ème    5ème    6ème    7ème    8ème    9ème

    1er       2       \-      \-      \-      \-      \-      \-      \-      \-

    2ème      3       \-      \-      \-      \-      \-      \-      \-      \-

    3ème      4        2      \-      \-      \-      \-      \-      \-      \-

    4ème      4        3      \-      \-      \-      \-      \-      \-      \-

    5ème      4        3       2      \-      \-      \-      \-      \-      \-

    6ème      4        3       3      \-      \-      \-      \-      \-      \-

    7ème      4        3       3       1      \-      \-      \-      \-      \-

    8ème      4        3       3       2      \-      \-      \-      \-      \-

    9ème      4        3       3       3       1      \-      \-      \-      \-

   10ème      4        3       3       3       2      \-      \-      \-      \-

   11ème      4        3       3       3       2       1      \-      \-      \-

   12ème      4        3       3       3       2       1      \-      \-      \-

   13ème      4        3       3       3       2       1       1      \-      \-

   14ème      4        3       3       3       2       1       1      \-      \-

   15ème      4        3       3       3       2       1       1       1      \-

   16ème      4        3       3       3       2       1       1       1      \-

   17ème      4        3       3       3       2       1       1       1       1

   18ème      4        3       3       3       3       1       1       1       1

   19ème      4        3       3       3       3       2       1       1       1

   20ème      4        3       3       3       3       2       2       1       1
  ---------------------------------------------------------------------------------

  : Emplacements de sorts par niveau pour les magiciens

Le tableau des emplacements de sorts par niveau indique le nombre
d'emplacements de sorts dont vous disposez pour lancer vos sorts de 1er
niveau et plus. Pour lancer l'un de ces sorts, vous devez dépenser un
emplacement du niveau du sort ou d'un niveau supérieur. Vous récupérez
tous les emplacements de sorts dépensés lorsque vous terminez un long
repos.

Vous préparez la liste des sorts de magicien que vous pouvez lancer.
Pour ce faire, choisissez un nombre de sorts de magicien dans votre
livre de sorts égal à votre modificateur d'Intelligence + votre niveau
de magicien (minimum d'un sort). Les sorts doivent être d'un niveau
pour lequel vous disposez d'emplacements de sorts.

Par exemple, si vous êtes un magicien de 3e niveau, vous avez quatre
emplacements de sorts de 1er niveau et deux de 2e niveau. Avec une
Intelligence de 16, votre liste de sorts préparés peut inclure six sorts
de 1er ou 2ème niveau, dans n'importe quelle combinaison, choisis dans
votre livre de sorts. Si vous préparez le sort *missile
magique* de 1er niveau, vous pouvez le lancer en
utilisant un emplacement de 1er niveau ou de 2e niveau. Lancer le sort
ne le retire pas de votre liste de sorts préparés.

Vous pouvez modifier votre liste de sorts préparés lorsque vous terminez
un repos long. Préparer une nouvelle liste de sorts de magicien
nécessite du temps passé à étudier votre livre de sorts et à mémoriser
les incantations et les gestes que vous devez faire pour lancer le sort
: au moins 1 minute par niveau de sort pour chaque sort de votre liste.



#### Caractéristique d'Incantation

L'Intelligence est votre caractéristique d'incantation pour vos sorts
de magicien, puisque vous apprenez vos sorts en les étudiant et en les
mémorisant. Vous utilisez votre Intelligence lorsqu'un sort fait
référence à votre caractéristique d'incantation. De plus, vous utilisez
votre modificateur d'Intelligence pour déterminer le jet de sauvegarde
d'un sort de magicien que vous lancez et pour effectuer un jet
d'attaque avec un tel sort.

  -----------------------------------------------------------------------
    **Sauvegarde contre les sorts DC** = 8 + votre bonus de maîtrise +
                    votre modificateur d'Intelligence.

  **Modificateur d'attaque par sort** = votre bonus de maîtrise + votre
                       modificateur d'Intelligence.
  -----------------------------------------------------------------------



#### Lancer de rituel

Vous pouvez lancer un sort de magicien comme un rituel si ce sort a
l'étiquette rituel et si vous avez le sort dans votre livre de sorts.
Il n'est pas nécessaire que le sort soit préparé.



#### Focalisation des lanceurs de sorts

Vous pouvez utiliser un foyer arcanique comme foyer d'incantation pour
vos sorts de magicien.



#### Apprendre les sorts de 1er niveau et plus

Chaque fois que vous gagnez un niveau de magicien, vous pouvez ajouter
gratuitement deux sorts de magicien de votre choix à votre livre de
sorts. Chacun de ces sorts doit être d'un niveau pour lequel vous
disposez d'emplacements de sorts, comme indiqué sur la table des
magiciens.

Au cours de tes aventures, tu trouveras peut-être d'autres sorts que tu
pourras ajouter à ton livre de sorts.




### Régénération occulte

Vous avez appris à récupérer une partie de votre énergie magique en
étudiant votre livre de sorts. Une fois par jour, lorsque vous terminez
un court repos, vous pouvez choisir des emplacements de sorts dépensés
pour récupérer. Les emplacements de sorts peuvent avoir un niveau
combiné égal ou inférieur à la moitié de votre niveau de magicien
(arrondi au supérieur), et aucun des emplacements ne peut être de 6e
niveau ou plus.

Par exemple, si vous êtes un magicien de 4e niveau, vous pouvez
récupérer jusqu'à deux niveaux d'emplacements de sorts. Vous pouvez
récupérer soit un emplacement de sort de 2ème niveau, soit deux
emplacements de sort de 1er niveau.



### Tradition occulte

Lorsque vous allongez le 2ème niveau, vous choisissez une tradition
occulte, façonnant votre pratique de la magie à travers une école
spécifique .

Votre choix vous confère des capacités au 2ème niveau, puis au 6ème,
10ème et 14ème niveau.



### Amélioration de la valeur de caractéristiques

Lorsque vous atteignez le 4e niveau, et de nouveau au 8e, 12e, 16e et
19e niveau, vous pouvez augmenter un score de capacité de votre choix de
2, ou vous pouvez augmenter deux scores de capacité de votre choix de 1.
Comme d'habitude, vous ne pouvez pas augmenter un score de capacité
au-dessus de 20 en utilisant cette caractéristique.



### Maîtrise des sorts

Au 18ème niveau, vous avez atteint une telle maîtrise de certains sorts
que vous pouvez les lancer à volonté. Choisissez un sort de magicien de
1er niveau et un sort de magicien de 2e niveau qui se trouvent dans
votre livre de sorts. Vous pouvez lancer ces sorts à leur plus bas
niveau sans dépenser un emplacement de sort lorsque vous les avez
préparés. Si vous voulez lancer l'un ou l'autre de ces sorts à un
niveau supérieur, vous devez dépenser un emplacement de sort comme
d'habitude.

En passant 8 heures à l'étude, vous pouvez échanger l'un ou les deux
sorts que vous avez choisis contre d'autres sorts de même niveau.



### Sorts de prédilection

Lorsque vous atteignez le 20e niveau, vous obtenez la maîtrise de deux
puissants sorts et pouvez les lancer sans trop d'efforts. Choisissez
deux sorts de magicien de 3e niveau dans votre livre de sorts comme vos
sorts de signature. Vous avez toujours ces sorts préparés, ils ne
comptent pas dans le nombre de sorts que vous avez préparés, et vous
pouvez les lancer chacun une fois au 3e niveau sans dépenser un
emplacement de sort. Lorsque vous le faites, vous ne pouvez plus le
faire jusqu'à ce que vous ayez terminé un repos court ou long.

Si vous voulez lancer l'un ou l'autre de ces sorts à un niveau
supérieur, vous devez dépenser un emplacement de sort comme d'habitude.





### Traditions magiques du Magicien ###

L'étude de la magie est ancienne, remontant aux premières
découvertes de la magie par les mortels. Elle est fermement établie dans
les univers de jeux fantastiques, avec diverses traditions dédiées à son
étude complexe.

Les traditions occultes les plus communes dans le multivers tournent
autour des écoles de magie. Les magiciens, à travers les âges, ont
répertorié des milliers de sorts, les regroupant en huit catégories
appelées écoles. Dans certains endroits, ces traditions sont
littéralement des écoles. Dans d'autres établissements, les écoles
ressemblent davantage à des départements universitaires, avec des
facultés rivales qui se disputent les étudiants et les financements.
Même les magiciens qui forment des apprentis dans la solitude de leur
propre tour utilisent la division de la magie en écoles comme un moyen
d'apprentissage, puisque les sorts de chaque école exigent la maîtrise
de techniques différentes.



#### École de l'Évocation ####

Vous concentrez votre étude sur la magie qui crée de puissants effets
élémentaires tels que le froid glacial, les flammes brûlantes, le
tonnerre roulant, les éclairs crépitants et l'acide brûlant. Certains
évokers trouvent un emploi dans les forces militaires, servant
d'artillerie pour faire exploser les armées ennemies à distance.
D'autres utilisent leur pouvoir spectaculaire pour protéger les
faibles, tandis que d'autres encore cherchent à s'enrichir en tant que
bandits, aventuriers ou tyrans en herbe.



##### Maître évocateur #####

À partir du moment où vous choisissez cette école au 2e niveau, l'or et
le temps que vous devez dépenser pour copier un sort d'évocation dans
votre livre de sorts sont réduits de moitié.




##### Sculpter des sorts #####

À partir du 2e niveau, vous pouvez créer des poches de sécurité relative
dans les effets de vos sorts d'évocation. Lorsque vous lancez un sort
d'évocation qui affecte d'autres créatures que vous pouvez voir, vous
pouvez choisir un nombre d'entre elles égal à 1 + le niveau du sort.
Les créatures choisies réussissent automatiquement leurs jets de
sauvegarde contre le sort, et elles ne subissent aucun dégât si elles
subissent normalement la moitié des dégâts en cas de sauvegarde réussie.




##### Tour de magie puissant #####

À partir du 6e niveau, vos cantrips dommageables affectent même les
créatures qui évitent le choc de l'effet. Lorsqu'une créature réussit
un jet de sauvegarde contre votre tour de magie, elle subit la moitié
des dégâts du tour de magie (le cas échéant), mais ne subit aucun effet
supplémentaire du tour de magie.




##### Évocation améliorée #####

À partir du 10e niveau, vous pouvez ajouter votre modificateur
d'Intelligence à un jet de dégâts de tout sort d'évocation de magicien
que vous lancez.




##### Surcanalisation #####

À partir du 14e niveau, vous pouvez augmenter la puissance de vos sorts
plus simples. Lorsque vous lancez un sort de magicien du 1er au 5e
niveau qui inflige des dégâts, vous pouvez infliger le maximum de dégâts
avec ce sort.

La première fois que vous le faites, vous ne subissez aucun effet
indésirable. Si vous utilisez à nouveau cette caractéristique avant de
terminer un repos long, vous subissez 2d12 dégâts nécrotiques pour
chaque niveau du sort, immédiatement après l'avoir lancé. Chaque fois
que vous utilisez à nouveau cette caractéristique avant de terminer un
repos long, les dégâts nécrotiques par niveau de sort augmentent de
1d12. Ces dégâts ignorent la résistance et l'immunité.

> #### Votre livre de sorts {#your-spellbook}
>
> Les sorts que vous ajoutez à votre livre de sorts au fur et à mesure
> que vous gagnez des niveaux reflètent les recherches arcaniques que
> vous menez par vous-même, ainsi que les percées intellectuelles que
> vous avez faites sur la nature du multivers. Vous pouvez trouver
> d'autres sorts au cours de vos aventures. Vous pourriez découvrir un
> sort enregistré sur un parchemin dans le coffre d'un magicien
> maléfique, par exemple, ou dans un tome poussiéreux d'une
> bibliothèque ancienne.
>
> ***Copier un sort dans le livre.*** Lorsque vous trouvez un sort de
> magicien de 1er niveau ou plus, vous pouvez l'ajouter à votre livre
> de sorts s'il est d'un niveau que vous pouvez préparer et si vous
> avez le temps de le déchiffrer et de le copier.
>
> Copier ce sort dans votre livre de sorts implique de reproduire la
> forme de base du sort, puis de déchiffrer le système unique de
> notation utilisé par le magicien qui l'a écrit. Vous devez pratiquer
> le sort jusqu'à ce que vous compreniez les sons ou les gestes requis,
> puis le transcrire dans votre livre de sorts en utilisant votre propre
> notation.
>
> Pour chaque niveau du sort, le processus prend 2 heures et coûte 50
> gp. Ce coût représente les composants matériels que vous dépensez
> pendant que vous expérimentez le sort pour le maîtriser, ainsi que les
> encres fines dont vous avez besoin pour l'enregistrer. Une fois que
> vous avez dépensé ce temps et cet argent, vous pouvez préparer le sort
> comme vos autres sorts.
>
> ***Remplacer le livre.*** Vous pouvez copier un sort de votre propre
> livre de sorts dans un autre livre, par exemple si vous voulez faire
> une copie de sauvegarde de votre livre de sorts. Cela revient à copier
> un nouveau sort dans votre livre de sorts, mais plus rapidement et
> plus facilement, puisque vous comprenez votre propre notation et savez
> déjà comment lancer le sort. Vous n'avez besoin de dépenser qu'une
> heure et 10 gp pour chaque niveau du sort copié.
>
> Si vous perdez votre livre de sorts, vous pouvez utiliser la même
> procédure pour transcrire les sorts que vous avez préparés dans un
> nouveau livre de sorts. Pour remplir le reste de votre livre de sorts,
> vous devrez trouver de nouveaux sorts, comme d'habitude. C'est pour
> cette raison que de nombreux magiciens conservent des carnets de sorts
> de secours dans un endroit sûr.
>
> ***L'apparence du livre.*** Votre livre de sorts est une compilation
> unique de sorts, avec ses propres fioritures décoratives et ses notes
> en marge. Il peut s'agir d'un volume en cuir simple et fonctionnel
> que vous avez reçu en cadeau de votre maître, d'un tome finement
> relié et doré que vous avez trouvé dans une bibliothèque ancienne, ou
> même d'une collection de notes rassemblées après avoir perdu votre
> précédent livre de sorts dans une mésaventure.

> #### Les écoles de magie {#the-schools-of-magic}
>
> Les académies de magie regroupent les sorts en huit catégories
> appelées écoles de magie. Les érudits, en particulier les magiciens,
> appliquent ces catégories à tous les sorts, estimant que toute magie
> fonctionne essentiellement de la même manière, qu'elle soit le fruit
> d'une étude rigoureuse ou qu'elle soit conférée par une divinité.
>
> Les écoles de magie aident à décrire les sorts ; elles n'ont pas de
> règles propres, bien que certaines règles fassent référence aux
> écoles.
>
> Les sorts d'**abjuration** sont de nature protectrice, bien que
> certains d'entre eux aient des utilisations agressives. Ils créent
> des barrières magiques, annulent les effets néfastes, nuisent aux
> intrus ou bannissent les créatures vers d'autres plans d'existence.
>
> Les sorts de**conjuration** permettent de transporter des objets et
> des créatures d'un endroit à un autre. Certains sorts convoquent des
> créatures ou des objets aux côtés du lanceur, tandis que d'autres
> permettent au lanceur de se téléporter dans un autre lieu. Certaines
> conjurations créent des objets ou des effets à partir de rien.
>
> Les sorts de**divination** révèlent des informations, qu'il s'agisse
> de secrets oubliés depuis longtemps, d'aperçus de l'avenir, de
> l'emplacement de choses cachées, de la vérité derrière des illusions
> ou de visions de personnes ou de lieux éloignés.
>
> Les sorts d'**enchantement** affectent l'esprit des autres,
> influençant ou contrôlant leur comportement. Ces sorts peuvent faire
> en sorte que des ennemis voient le lanceur de sorts comme un ami,
> forcer des créatures à suivre une ligne de conduite, ou même contrôler
> une autre créature comme une marionnette.
>
> Les sorts d'**évocation** manipulent l'énergie magique pour produire
> un effet désiré. Certains appellent des explosions de feu ou des
> éclairs. D'autres canalisent l'énergie positive pour guérir les
> blessures.
>
> Les sorts d'**illusion** trompent les sens ou l'esprit des autres.
> Ils amènent les gens à voir des choses qui ne sont pas là, à manquer
> des choses qui sont là, à entendre des bruits fantômes ou à se
> souvenir de choses qui ne sont jamais arrivées. Certaines illusions
> créent des images fantômes que toute créature peut voir, mais les
> illusions les plus insidieuses plantent une image directement dans
> l'esprit d'une créature.
>
> Les sorts de**nécromancie** manipulent les énergies de vie et de mort.
> Ces sorts peuvent accorder une réserve supplémentaire de force vitale,
> drainer l'énergie vitale d'une autre créature, créer des
> morts-vivants ou même ramener les morts à la vie. Créer des
> morts-vivants à l'aide de sorts de nécromancie tels que *Animer les
> morts* n'est pas une bonne action, et seuls les
> lanceurs de sorts maléfiques utilisent fréquemment de tels sorts.
>
> Les sorts de**transmutation** modifient les propriétés d'une
> créature, d'un objet ou d'un environnement. Ils peuvent transformer
> un ennemi en une créature inoffensive, renforcer la force d'un allié,
> faire bouger un objet à la demande du lanceur de sorts ou améliorer
> les capacités de guérison innées d'une créature pour qu'elle se
> remette rapidement d'une blessure.


