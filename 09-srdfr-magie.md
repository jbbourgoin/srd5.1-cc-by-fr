
# Incantation

La magie est omniprésente dans les univers de jeux fantastiques et
apparaît souvent sous la forme d'un sort.

Cette section présente les règles pour lancer des sorts. Les différentes
classes de personnages ont des manières distinctes d'apprendre et de
préparer leurs sorts, et les monstres utilisent les sorts de manière
unique. Quelle que soit sa source, un sort suit les règles énoncées ici.


## Qu'est-ce qu'un sort ?

Un sort est un effet magique discret, un façonnage unique des énergies
magiques qui imprègnent le multivers en une expression spécifique et
limitée. En lançant un sort, un personnage arrache soigneusement les
brins invisibles de magie brute qui imprègnent le monde, les fixe en
place selon un modèle particulier, les fait vibrer d'une manière
spécifique, puis les relâche pour déclencher l'effet désiré - dans la
plupart des cas, tout cela en l'espace de quelques secondes.

Les sorts peuvent être des outils polyvalents, des armes ou des
protections. Ils peuvent infliger des dégâts ou les annuler, imposer ou
supprimer des conditions ,
drainer l'énergie vitale et rendre la vie aux morts.

Des milliers de sorts ont été créés au cours de l'histoire du
multivers, et beaucoup d'entre eux sont oubliés depuis longtemps.
Certains sont peut-être encore consignés dans des livres de sorts en
ruine, cachés dans d'anciennes ruines ou piégés dans l'esprit de dieux
morts. Ou bien, ils pourraient un jour être réinventés par un personnage
qui a accumulé suffisamment de pouvoir et de sagesse pour le faire.


### Niveau des sorts

Chaque sort a un niveau allant de 0 à 9. Le niveau d'un sort est un
indicateur général de sa puissance, avec le modeste (mais toujours
impressionnant) *projectile magique* au 1er niveau et
le *Souhait* qui secoue la terre au 9ème niveau. Les tours de
magie - des sorts simples mais puissants que les personnages peuvent
lancer presque par cœur - sont de niveau 0. Plus le niveau d'un sort
est élevé, plus le lanceur de sorts doit être de haut niveau pour
l'utiliser.

Le niveau des sorts et le niveau du personnage ne correspondent pas
directement. En général, un personnage doit être au moins de 17ème
niveau, et non de 9ème niveau, pour lancer un sort de 9ème niveau.



### Sorts connus et préparés

Avant qu'un lanceur de sorts puisse utiliser un sort, il doit avoir le
sort fermement fixé à l'esprit, ou doit avoir accès au sort dans un
objet magique. Les membres de quelques classes, dont les bardes et les
sorciers, ont une liste limitée de sorts qu'ils connaissent et qui sont
toujours fixés dans leur esprit.

Il en va de même pour de nombreux monstres utilisant la magie. D'autres
lanceurs de sorts, comme les clercs et les magiciens, suivent un
processus de préparation des sorts. Ce processus varie selon les
classes, comme le détaillent leurs descriptions.

Dans tous les cas, le nombre de sorts qu'un lanceur peut avoir en tête
à un moment donné dépend du niveau du personnage.



### Emplacements de sorts

Quel que soit le nombre de sorts que le lanceur connaît ou prépare, il
ne peut en lancer qu'un nombre limité avant de se reposer. Manipuler le
tissu de la magie et canaliser son énergie, même pour un simple sort,
est physiquement et mentalement éprouvant, et les sorts de niveau
supérieur le sont encore plus. Ainsi, la description de chaque classe
d'incantation (à l'exception de celle du invocateur) comprend un
tableau indiquant le nombre d'emplacements de chaque niveau de sort
qu'un personnage peut utiliser à chaque niveau de personnage. Par
exemple, la magicienne de 3e niveau Morgane dispose de quatre
emplacements de sorts de 1er niveau et de deux emplacements de 2e
niveau.

Lorsqu'un personnage lance un sort, il dépense un emplacement du niveau
de ce sort ou d'un niveau supérieur, ce qui a pour effet de \"remplir\"
un emplacement avec le sort. Vous pouvez considérer un emplacement de
sort comme une rainure d'une certaine taille - petite pour un
emplacement de 1er niveau, plus grande pour un sort de niveau supérieur.
Un sort de 1er niveau peut être placé dans un emplacement de n'importe
quelle taille, mais un sort de 9e niveau ne peut être placé que dans un
emplacement de 9e niveau. Ainsi, lorsque Morgane lance un *missile
magique*, un sort de 1er niveau, elle dépense un de ses
quatre emplacements de 1er niveau et il lui en reste trois. Après un
long repos, tous les emplacements de sorts dépensés sont restaurés.

Certains personnages et monstres ont des capacités spéciales qui leur
permettent de lancer des sorts sans utiliser d'emplacements de sorts.
Par exemple, un moine qui suit la Voie des quatre éléments, un sorcier
qui choisit certaines manifestations traumaturgiques et un fiélon de
l'enfer peuvent tous lancer des sorts de cette manière.


#### Lancer un sort à un niveau supérieur

Lorsqu'un lanceur de sorts lance un sort en utilisant un emplacement
qui est d'un niveau supérieur à celui du sort, le sort prend le niveau
supérieur pour cette incantation. Par exemple, si Morgane lance
*Projectile magique* en utilisant l'un de ses
emplacements de 2e niveau, ce *projectile magique* est
de 2e niveau. En fait, le sort s'étend pour remplir l'emplacement dans
lequel il est lancé.

Certains sorts, comme *Projectile magique* et
*Soins*, ont des effets plus puissants lorsqu'ils sont
lancés à un niveau plus élevé, comme indiqué dans la description du
sort.

> #### Moulage en armure {#casting-in-armor}
>
> En raison de la concentration mentale et des gestes précis nécessaires
> à l'incantation, vous devez maîtriser l'armure que vous portez pour
> lancer un sort. Sinon, vous êtes trop distrait et physiquement gêné
> par votre armure pour lancer un sort.




### Tours de magie

Un tour de magie est un sort qui peut être lancé à volonté, sans
utiliser d'emplacement de sort et sans être préparé à l'avance. La
pratique répétée a fixé le sort dans l'esprit du lanceur et lui a
insufflé la magie nécessaire pour produire l'effet encore et encore. Le
niveau de magie d'un tour de magie est de 0.



### Rituels

Certains sorts ont une étiquette spéciale : rituel. Un tel sort peut
être lancé en suivant les règles normales de l'incantation, ou le sort
peut être lancé comme un rituel. La version rituelle d'un sort prend 10
minutes de plus que la version normale. Elle n'utilise pas non plus
d'emplacement de sort, ce qui signifie que la version rituelle d'un
sort ne peut pas être lancée à un niveau supérieur.

Pour lancer un sort sous forme de rituel, un lanceur de sorts doit
posséder une caractéristique qui lui en donne la capacité. Le clerc et
le druide, par exemple, possèdent une telle caractéristique. Le lanceur
de sorts doit également avoir préparé le sort ou l'avoir inscrit sur sa
liste de sorts connus, à moins que la caractéristique de rituel du
personnage ne spécifie le contraire, comme c'est le cas pour le
magicien.

> #### Les écoles de magie {#the-schools-of-magic-1}
>
> Les académies de magie regroupent les sorts en huit catégories
> appelées écoles de magie. Les érudits, en particulier les magiciens,
> appliquent ces catégories à tous les sorts, estimant que toute magie
> fonctionne essentiellement de la même manière, qu'elle soit le fruit
> d'une étude rigoureuse ou qu'elle soit conférée par une divinité.
>
> Les écoles de magie aident à décrire les sorts ; elles n'ont pas de
> règles propres, bien que certaines règles fassent référence aux
> écoles.
>
> Les sorts d'**abjuration** sont de nature protectrice, bien que
> certains d'entre eux aient des utilisations agressives. Ils créent
> des barrières magiques, annulent les effets néfastes, nuisent aux
> intrus ou bannissent les créatures vers d'autres plans d'existence.
>
> Les sorts de**conjuration** permettent de transporter des objets et
> des créatures d'un endroit à un autre. Certains sorts convoquent des
> créatures ou des objets aux côtés du lanceur, tandis que d'autres
> permettent au lanceur de se téléporter dans un autre lieu. Certaines
> conjurations créent des objets ou des effets à partir de rien.
>
> Les sorts de**divination** révèlent des informations, qu'il s'agisse
> de secrets oubliés depuis longtemps, d'aperçus de l'avenir, de
> l'emplacement de choses cachées, de la vérité derrière des illusions
> ou de visions de personnes ou de lieux éloignés.
>
> Les sorts d'**enchantement** affectent l'esprit des autres,
> influençant ou contrôlant leur comportement. Ces sorts peuvent faire
> en sorte que des ennemis voient le lanceur de sorts comme un ami,
> forcer des créatures à suivre une ligne de conduite, ou même contrôler
> une autre créature comme une marionnette.
>
> Les sorts d'**évocation** manipulent l'énergie magique pour produire
> un effet désiré. Certains appellent des explosions de feu ou des
> éclairs. D'autres canalisent l'énergie positive pour guérir les
> blessures.
>
> Les sorts d'**illusion** trompent les sens ou l'esprit des autres.
> Ils amènent les gens à voir des choses qui ne sont pas là, à manquer
> des choses qui sont là, à entendre des bruits fantômes ou à se
> souvenir de choses qui ne sont jamais arrivées. Certaines illusions
> créent des images fantômes que toute créature peut voir, mais les
> illusions les plus insidieuses plantent une image directement dans
> l'esprit d'une créature.
>
> Les sorts de**nécromancie** manipulent les énergies de vie et de mort.
> Ces sorts peuvent accorder une réserve supplémentaire de force vitale,
> drainer l'énergie vitale d'une autre créature, créer des
> morts-vivants ou même ramener les morts à la vie. Créer des
> morts-vivants à l'aide de sorts de nécromancie tels que *Animer les
> morts* n'est pas une bonne action, et seuls les
> lanceurs de sorts maléfiques utilisent fréquemment de tels sorts.
>
> Les sorts de**transmutation** modifient les propriétés d'une
> créature, d'un objet ou d'un environnement. Ils peuvent transformer
> un ennemi en une créature inoffensive, renforcer la force d'un allié,
> faire bouger un objet à la demande du lanceur de sorts ou améliorer
> les capacités de guérison innées d'une créature pour qu'elle se
> remette rapidement d'une blessure.




## Lancer un sort

Lorsqu'un personnage lance un sort, les mêmes règles de base sont
suivies, indépendamment de la classe du personnage ou des effets du
sort.

Chaque description de sort commence par un bloc d'informations
comprenant le nom du sort, son niveau, son école de magie, son temps
d'incantation, sa portée, ses composantes et sa durée. Le reste de la
description d'un sort décrit ses effets.


### Temps de coulée

La plupart des sorts nécessitent une seule action pour être lancés, mais
certains nécessitent une action bonus, une réaction ou beaucoup plus de
temps pour être lancés.


#### Action bonus

Un sort lancé avec une action bonus est particulièrement rapide. Vous
devez utiliser une action bonus à votre tour pour lancer le sort, à
condition de ne pas avoir déjà effectué une action bonus ce tour-ci.
Vous ne pouvez pas lancer un autre sort pendant le même tour, à
l'exception d'un cantrip dont le temps d'incantation est de 1 action.



#### Réactions

Certains sorts peuvent être lancés comme des réactions. Ces sorts sont
lancés en une fraction de seconde, en réponse à un événement quelconque.
Si un sort peut être lancé en tant que réaction, la description du sort
vous indique exactement quand vous pouvez le faire.



#### Temps de coulée plus longs

Certains sorts (y compris les sorts lancés en tant que rituels)
nécessitent plus de temps pour être lancés : des minutes, voire des
heures. Lorsque vous lancez un sort dont le temps d'incantation est
supérieur à une action ou une réaction unique, vous devez dépenser votre
action à chaque tour pour lancer le sort, et vous devez maintenir votre
concentration pendant que vous le faites . Si votre concentration est rompue,
le sort échoue, mais vous ne dépensez pas d'emplacement de sort. Si
vous voulez essayer de lancer le sort à nouveau, vous devez recommencer.




### Portée

La cible d'un sort doit se trouver dans la portée du sort. Pour un sort
comme *Projectile magique*, la cible est une créature.
Pour un sort comme Boule *de feu*, la cible est le point
dans l'espace où la boule de feu fait irruption.

La plupart des sorts ont une portée exprimée en pieds. Certains sorts ne
peuvent cibler qu'une créature (y compris vous) que vous touchez.
D'autres sorts, comme le sort *Bouclier*, n'affectent que
vous. Ces sorts ont une portée de soi.

Les sorts qui créent des cônes ou des lignes d'effet qui partent de
vous ont également une portée de soi, ce qui indique que le point
d'origine de l'effet du sort doit être vous .

Une fois qu'un sort est lancé, ses effets ne sont pas limités par sa
portée, sauf si la description du sort indique le contraire.



### Composantes

Les composantes d'un sort sont les conditions physiques que vous devez
remplir pour le lancer. La description de chaque sort indique s'il
nécessite des composantes verbales (V), somatiques (S) ou matérielles
(M). Si vous ne pouvez pas fournir une ou plusieurs composantes d'un
sort, vous ne pouvez pas le lancer.


#### Verbal (V)

La plupart des sorts nécessitent la récitation de mots mystiques. Les
mots eux-mêmes ne sont pas la source du pouvoir du sort ; c'est plutôt
la combinaison particulière de sons, avec une hauteur et une résonance
spécifiques, qui met en mouvement les fils de la magie. Ainsi, un
personnage bâillonné ou se trouvant dans une zone de silence, telle que
celle créée par le sort de *silence*, ne peut pas lancer un
sort à composante verbale.



#### Somatique (S)

Les gestes d'incantation peuvent inclure une gesticulation énergique ou
un ensemble complexe de gestes. Si un sort nécessite une composante
somatique, le lanceur doit avoir l'usage libre d'au moins une main
pour exécuter ces gestes.



#### Matériau (M)

Lancer certains sorts nécessite des objets particuliers, spécifiés entre
parenthèses dans l'entrée de la composante. Un personnage peut utiliser
une **pochette de composants** ou un **foyer d'incantation** à la place des composants
spécifiés pour un sort. Mais si un coût est indiqué pour une composante,
un personnage doit avoir cette composante spécifique avant de pouvoir
lancer le sort.

Si un sort indique qu'un composant matériel est consommé par le sort,
le lanceur doit fournir ce composant à chaque lancement du sort.

Un lanceur de sorts doit avoir une main libre pour accéder aux
composantes matérielles d'un sort - ou pour tenir un foyer
d'incantation - mais il peut s'agir de la même main que celle qu'il
utilise pour exécuter les composantes somatiques.




### Durée

La durée d'un sort est le temps pendant lequel le sort persiste. Une
durée peut être exprimée en rounds, minutes, heures ou même années.
Certains sorts précisent que leurs effets durent jusqu'à ce qu'ils
soient dissipés ou détruits.


#### Instantané

De nombreux sorts sont instantanés. Le sort blesse, guérit, crée ou
modifie une créature ou un objet d'une manière qui ne peut être
dissipée, car sa magie n'existe que pendant un instant.



#### Concentration

Certains sorts exigent que vous restiez concentré pour que leur magie
reste active. Si vous perdez votre concentration, un tel sort prend fin.

Si un sort doit être maintenu par la concentration, ce fait apparaît
dans son entrée Durée, et le sort précise combien de temps vous pouvez
vous concentrer sur lui. Vous pouvez mettre fin à la concentration à
tout moment (aucune action requise).

L'activité normale, comme le déplacement et l'attaque, n'interfère
pas avec la concentration. Les facteurs suivants peuvent nuire à la
concentration :

-   **Lancer un autre sort qui nécessite de la concentration.** Vous
    perdez votre concentration sur un sort si vous lancez un autre sort
    qui nécessite de la concentration. Vous ne pouvez pas vous
    concentrer sur deux sorts à la fois.
-   **Prise de dégâts.** Chaque fois que vous subissez des dégâts alors
    que vous êtes concentré sur un sort, vous devez effectuer un jet de
    sauvegarde de Constitution pour maintenir votre concentration. Le
    jet de sauvegarde est égal à 10 ou à la moitié des dégâts que vous
    subissez, le chiffre le plus élevé étant retenu. Si vous subissez
    des dégâts de plusieurs sources, comme une flèche et un souffle de
    dragon, vous effectuez un jet de sauvegarde distinct pour chaque
    source de dégâts.
-   **Être frappé d'incapacité ou tué.** Vous perdez votre
    concentration sur un sort si vous êtes frappé d'incapacité ou si
    vous mourez.

Le MJ peut également décider que certains phénomènes environnementaux,
comme une vague qui s'écrase sur vous alors que vous êtes sur un navire
en pleine tempête, vous obligent à réussir un jet de sauvegarde de
Constitution DC 10 pour rester concentré sur un sort.




### Cibles

Un sort typique exige que vous choisissiez une ou plusieurs cibles qui
seront affectées par la magie du sort. La description d'un sort vous
indique si le sort cible des créatures, des objets ou un point
d'origine pour une zone d'effet .

À moins qu'un sort n'ait un effet perceptible, une créature peut ne
pas savoir qu'elle a été ciblée par un sort. Un effet tel qu'un éclair
crépitant est évident, mais un effet plus subtil, tel qu'une tentative
de lire dans les pensées d'une créature, passe généralement inaperçu, à
moins qu'un sort ne dise le contraire.


#### Un chemin clair vers la cible

Pour cibler quelque chose, vous devez avoir un chemin clair vers lui,
donc il ne peut pas être derrière un abri total.

Si vous placez une zone d'effet en un point que vous ne pouvez pas voir
et qu'un obstacle, tel qu'un mur, se trouve entre vous et ce point, le
point d'origine se trouve du côté le plus proche de cet obstacle.



#### Se cibler

Si un sort cible une créature de votre choix, vous pouvez vous choisir,
sauf si la créature doit être hostile ou spécifiquement une créature
autre que vous. Si vous vous trouvez dans la zone d'effet d'un sort
que vous avez lancé, vous pouvez vous cibler.




### Zones d'effet

Les sorts tels que Mains *brûlantes* et
*Cône* couvrent une zone,
ce qui leur permet d'affecter plusieurs créatures à la fois.

La description d'un sort précise sa zone d'effet, qui a généralement
l'une des cinq formes suivantes : cône, cube, cylindre, ligne ou
sphère. Chaque zone d'effet a un **point d'origine**, un endroit d'où
l'énergie du sort jaillit. Les règles de chaque forme précisent comment
positionner son point d'origine. Généralement, un point d'origine est
un point dans l'espace, mais certains sorts ont une zone dont
l'origine est une créature ou un objet.

L'effet d'un sort s'étend en ligne droite à partir du point
d'origine. Si aucune ligne droite non bloquée ne s'étend du point
d'origine à un endroit situé dans la zone d'effet, cet endroit n'est
pas inclus dans la zone du sort. Pour bloquer l'une de ces lignes
imaginaires, un obstacle doit fournir un abri total.


#### Cône

Un cône s'étend dans une direction que vous choisissez à partir de son
point d'origine. La largeur d'un cône en un point donné de sa longueur
est égale à la distance de ce point par rapport au point d'origine. La
zone d'effet d'un cône précise sa longueur maximale.

Le point d'origine d'un cône n'est pas inclus dans la zone d'effet
du cône, sauf si vous en décidez autrement.



#### Cube

Vous sélectionnez le point d'origine d'un cube, qui se trouve
n'importe où sur une face de l'effet cubique. La taille du cube est
exprimée par la longueur de chaque côté.

Le point d'origine d'un cube n'est pas inclus dans la zone d'effet
du cube, sauf si vous en décidez autrement.



#### Cylindre

Le point d'origine d'un cylindre est le centre d'un cercle d'un
rayon particulier, comme indiqué dans la description du sort. Le cercle
doit être soit sur le sol, soit à la hauteur de l'effet du sort.
L'énergie contenue dans un cylindre s'étend en ligne droite du point
d'origine au périmètre du cercle, formant ainsi la base du cylindre.
L'effet du sort se propage ensuite vers le haut depuis la base ou vers
le bas depuis le sommet, sur une distance égale à la hauteur du
cylindre.

Le point d'origine d'un cylindre est inclus dans la zone d'effet du
cylindre.



#### Ligne

Une ligne s'étend de son point d'origine en ligne droite jusqu'à sa
longueur et couvre une surface définie par sa largeur.

Le point d'origine d'une ligne n'est pas inclus dans la zone d'effet
de la ligne, sauf si vous en décidez autrement.



#### Sphère

Vous sélectionnez le point d'origine d'une sphère, et la sphère
s'étend vers l'extérieur à partir de ce point. La taille de la sphère
est exprimée par un rayon en pieds qui s'étend à partir du point.

Le point d'origine d'une sphère est inclus dans la zone d'effet de la
sphère.




### Jets de sauvegarde

De nombreux sorts précisent que la cible peut effectuer un jet de
sauvegarde pour éviter tout ou partie des effets du sort. Le sort
précise la capacité que la cible utilise pour le jet de sauvegarde et ce
qui se passe en cas de réussite ou d'échec.

Le DC pour résister à l'un de vos sorts est égal à 8 + votre
modificateur d'aptitude de lanceur de sorts + votre bonus de maîtrise +
tout modificateur spécial.



### Jets d'attaque

Pour certains sorts, le lanceur doit effectuer un jet d'attaque pour
déterminer si l'effet du sort touche la cible visée. Votre bonus
d'attaque avec une attaque par sort est égal à votre modificateur
d'aptitude aux sorts + votre bonus de maîtrise.

La plupart des sorts qui nécessitent des jets d'attaque impliquent des
attaques à distance. N'oubliez pas que vous avez un désavantage sur un
jet d'attaque à distance si vous vous trouvez à moins de 1,5 mètre
d'une créature hostile qui peut vous voir et qui n'est pas frappée
d'incapacité.



### Combinaison d'effets magiques

Les effets de différents sorts s'additionnent tandis que les durées de
ces sorts se chevauchent. En revanche, les effets d'un même sort lancé
plusieurs fois ne se combinent pas. Au lieu de cela, l'effet le plus
puissant - comme le bonus le plus élevé - de ces lancers s'applique
pendant que leurs durées se chevauchent.

Par exemple, si deux clercs lancent *Bénédiction* sur la même
cible, le personnage ne bénéficie du sort qu'une seule fois ; il ne
peut pas lancer deux dés bonus.





# Listes de sorts



## Sorts de Barde


### Tours de magie (0 Niveau)

-   *Lumières dansantes*
-   *Lumière*
-   *Main de mage*
-   *Réparation*
-   *Message*
-   *Illusion mineure*
-   *Prestidigitation*
-   *Coup au but*
-   *Moquerie cruelle*



### 1er niveau

-   *Amitié avec les animaux*
-   *Fléau*
-   *Charme-personne*
-   *Compréhension des langues*
-   *Soins des plaies*
-   *Détection de la magie*
-   *Déguisement de soi*
-   *Lueurs féeriques*
-   *Feuille morte*
-   *Mot de guérison*
-   *Héroïsme*
-   *Rire hideux*
-   *Identification*
-   *Texte illusoire*
-   *Grande foulée*
-   *Image silencieuse*
-   *Sommeil*
-   *Parler avec les animaux*
-   *Vague tonnante*
-   *Serviteur invisible*



### 2ème niveau

-   *Messager animal*
-   *Cécité/surdité*
-   *Apaisement des émotions*
-   *Détection des pensées*
-   *Amélioration de caractéristique*
-   *Captiver*
-   *Métal brûlant*
-   *Immobilisation de personne*
-   *Invisibilité*
-   *Déblocage*
-   *Restauration partielle*
-   *Localiser les animaux ou les plantes*
-   *Localiser un objet*
-   *Bouche magique*
-   *Voir l'Invisibilité*
-   *Fracassement*
-   *Silence*
-   *Suggestion*
-   *Zone de vérité*



### 3ème niveau

-   *Invoquer la malédiction*
-   *Clairvoyance*
-   *Dissipation de la magie*
-   *Peur*
-   *Glyphe de protection*
-   *Motif hypnotique*
-   *Image majeure*
-   *Antidétection*
-   *Croissance végétale*
-   *Envoi de*
-   *Par Par Parler avec les morts Par parler avec parler avec les
    morts*
-   *Parler avec les plantes*
-   *Nuage nauséabond*
-   *Très petite (TP)*
-   *Don des langues*



### 4ème niveau

-   *Compulsion*
-   *Confusion*
-   *Porte dimensionnelle*
-   *Liberté de mouvement*
-   *Invisibilité supérieure*
-   *Terrain halluciné*
-   *Localiser une créature*
-   *Métamorphose*



### 5ème niveau

-   *Animation d'objets*
-   *Éveil*
-   *Domination de personne*
-   *Songe*
-   *Quête*
-   *Restauration supérieure*
-   *Immobilisation de monstre*
-   *Légendes traditionnelles*
-   *Soins de groupe*
-   *Double illusoire*
-   *Modification de mémoire*
-   *Contrat*
-   *Rappel à la vie*
-   *Scrutation*
-   *Apparence trompeuse*
-   *Cercle de téléportation*



### 6ème niveau

-   *Mauvais oeil*
-   *Sens de l'orientation*
-   *Protections et sceaux*
-   *Danse irrésistible*
-   *Suggestion de groupe*
-   *Illusion programmée*
-   *Vision suprême*



### 7ème niveau

-   *Épée Arcane*
-   *Forme éthérée*
-   *Cage de force*
-   *Magnifique manoir*
-   *Mirage Occulte*
-   *Projection d'image*
-   *Régénération*
-   *Résurrection*
-   *Symbole*
-   *Téléportation*



### 8ème niveau

-   *Domination de monstre*
-   *Esprit faible*
-   *Bagou*
-   *Esprit impénétrable*
-   *Mot de pouvoir étourdissant*



### 9ème niveau

-   *Prémonition*
-   *Mot de pouvoir mortel*
-   *Métamorphose véritable*




## Sorts de clercs


### Tours de magie (0 Niveau)

-   *Assistance*
-   *Lumière*
-   *Réparation*
-   *Résistance*
-   *Flamme sacrée*
-   *Épargner les mourrants*
-   *Thaumaturgie*



### 1er niveau

-   *Fléau*
-   *Bénédiction*
-   *Injonction*
-   *Création ou anéantissement de l'eau*
-   *Soins des plaies*
-   *Détection du mal et du bien*
-   *Détection de la magie*
-   *Détection du poison et des maladies*
-   *Éclair traçant*
-   *Mot de guérison*
-   *Blessures infligées*
-   *Protection contre le Bien et le
    Mal*
-   *Purification de nourriture et d'eau*
-   *Sanctuaire*
-   *Bouclier de la foi*



### 2ème niveau

-   *Aide*
-   *Augure*
-   *Cécité/surdité*
-   *Apaisement des émotions*
-   *Flamme continuelle*
-   *Amélioration de caractéristique*
-   *Sens des pièges*
-   *Préservation des morts*
-   *Immobilisation de personne*
-   *Restauration partielle*
-   *Localiser un objet*
-   *Prière de guérison*
-   *Protection contre le poison*
-   *Silence*
-   *Arme spirituelle*
-   *Lien de protection*
-   *Zone de vérité*



### 3ème niveau

-   *Mort animée*
-   *Lueur d'espoir*
-   *Invoquer la malédiction*
-   *Clairvoyance*
-   *Création de nourriture et d'eau*
-   *Lumière du jour*
-   *Dissipation de la magie*
-   *Glyphe de protection*
-   *Cercle magique*
-   *Mot de guérison Guérison de groupe*
-   *Fusion dans la pierre*
-   *Protection contre une énergie*
-   *Délivrance des malédictions*
-   *Retour à la vie*
-   *Envoi de*
-   *Par Par Parler avec les morts Par parler avec parler avec les
    morts*
-   *Esprits gardiens*
-   *Don des langues*
-   *Marche sur l'eau*



### 4ème niveau

-   *Bannissement*
-   *Contrôle de l'eau*
-   *Protection contre la mort*
-   *Divination*
-   *Liberté de mouvement*
-   *Gardien de la foi*
-   *Localiser une créature*
-   *Façonnage de la pierre*



### 5ème niveau

-   *Communion*
-   *Contagion*
-   *Dissipation du mal et du bien*
-   *Colonne de flamme*
-   *Quête*
-   *Restauration supérieure*
-   *Sanctification*
-   *Fléau d'insectes*
-   *Légendes traditionnelles*
-   *Soins de groupe*
-   *Contrat*
-   *Rappel à la vie*
-   *Scrutation*



### 6ème niveau

-   *Barrière de lames*
-   *Création de mort-vivant*
-   *Sens de l'orientation*
-   *Interdiction*
-   *Contamination*
-   *Guérison*
-   *Festin des héros*
-   *Allié planaire*
-   *Vision suprême*
-   *Mot de retour*



### 7ème niveau

-   *Invocation de céleste*
-   *Parole divine*
-   *Forme éthérée*
-   *Tempête de feu*
-   *Changement de plan*
-   *Régénération*
-   *Résurrection*
-   *Symbole*



### 8ème niveau

-   *Champ antimagie*
-   *Contrôle du climat*
-   *Tremblement de terre*
-   *Aura sacrée*



### 9ème niveau

-   *Projection astrale*
-   *Portail*
-   *Guérison de groupe*
-   *Résurrection incontestable*




## Sorts Druides


### Tours de magie (0 Niveau)

-   *Druidisme*
-   *Assistance*
-   *Réparation*
-   *Bouffée de poison*
-   *Produire des flammes*
-   *Résistance*
-   *Gourdin magique*



### 1er niveau

-   *Amitié avec les animaux*
-   *Charme-personne*
-   *Création ou anéantissement de l'eau*
-   *Soins des plaies*
-   *Détection de la magie*
-   *Détection du poison et des maladies*
-   *Enchevêtrement*
-   *Lueurs féeriques*
-   *Nappe de brouillard*
-   *Baies nourricières*
-   *Mot de guérison*
-   *Saut*
-   *Grande foulée*
-   *Purification de nourriture et d'eau*
-   *Parler avec les animaux*
-   *Vague tonnante*



### 2ème niveau

-   *Messager animal*
-   *Peau d'écorce*
-   *Vision dans le noir*
-   *Amélioration de caractéristique*
-   *Sens des pièges*
-   *Lame de feu*
-   *Sphère de feu*
-   *Bourrasque de vent*
-   *Métal brûlant*
-   *Immobilisation de personne*
-   *Restauration partielle*
-   *Localiser les animaux ou les plantes*
-   *Localiser un objet*
-   *Rayon de lune*
-   *Passage sans trace*
-   *Protection contre le poison*
-   *Croissance d'épines*



### 3ème niveau

-   *Appel de la foudre*
-   *Invocation d'animaux*
-   *Lumière du jour*
-   *Dissipation de la magie*
-   *Fusion dans la pierre*
-   *Croissance végétale*
-   *Protection contre une énergie*
-   *Tempête de neige*
-   *Parler avec les plantes*
-   *Respiration aquatique*
-   *Marche sur l'eau*
-   *Mur de vent*



### 4ème niveau

-   *Flétrissement*
-   *Confusion*
-   *Invocation d'élémentaux mineurs*
-   *Invocation d'êtres sylvestres*
-   *Contrôle de l'eau*
-   *Domination de bête*
-   *Liberté de mouvement*
-   *Insecte géant*
-   *Terrain halluciné*
-   *Tempête de grêle*
-   *Localiser une créature*
-   *Métamorphose*
-   *Façonnage de la pierre*
-   *Peau de pierre*
-   *Mur de feu*



### 5ème niveau

-   *Coquille antivie*
-   *Éveil*
-   *Communion avec la nature*
-   *Invocation d'élémentaire*
-   *Contagion*
-   *Quête*
-   *Restauration supérieure*
-   *Fléau d'insectes*
-   *Soins de groupe*
-   *Contrat*
-   *Réincarnation*
-   *Scrutation*
-   *Foulée d'arbre*
-   *Mur de pierre*



### 6ème niveau

-   *Invocation de fée*
-   *Sens de l'orientation*
-   *Guérison*
-   *Festin des héros*
-   *Façonnage de la Terre*
-   *Rayon de soleil*
-   *Transport végétal*
-   *Mur d'épines*
-   *Marche sur le vent*



### 7ème niveau

-   *Tempête de feu*
-   *Mirage Occulte*
-   *Changement de plan*
-   *Régénération*
-   *Inversion de la gravité*



### 8ème niveau

-   *Formes animales*
-   *Aversion/attirance*
-   *Contrôle du climat*
-   *Tremblement de terre*
-   *Esprit faible*
-   *Éclat du soleil*



### 9ème niveau

-   *Prémonition*
-   *Changement de forme*
-   *Tempête vengeresse*
-   *Résurrection incontestable*




## Sorts de paladin


### 1er niveau

-   *Bénédiction*
-   *Injonction*
-   *Soins des plaies*
-   *Détection du mal et du bien*
-   *Détection de la magie*
-   *Détection du poison et des maladies*
-   *Faveur divine*
-   *Héroïsme*
-   *Protection contre le Bien et le
    Mal*
-   *Purification de nourriture et d'eau*
-   *Bouclier de la foi*



### 2ème niveau

-   *Aide*
-   *Marque de punition*
-   *Appel de destrier*
-   *Restauration partielle*
-   *Localiser un objet*
-   *Arme magique*
-   *Protection contre le poison*
-   *Zone de vérité*



### 3ème niveau

-   *Création de nourriture et d'eau*
-   *Lumière du jour*
-   *Dissipation de la magie*
-   *Cercle magique*
-   *Délivrance des malédictions*
-   *Retour à la vie*



### 4ème niveau

-   *Bannissement*
-   *Protection contre la mort*
-   *Localiser une créature*



### 5ème niveau

-   *Dissipation du mal et du bien*
-   *Quête*
-   *Rappel à la vie*




## Sorts du rôdeur


### 1er niveau

-   *Alarme*
-   *Amitié avec les animaux*
-   *Soins des plaies*
-   *Détection de la magie*
-   *Détection du poison et des maladies*
-   *Nappe de brouillard*
-   *Baies nourricières*
-   *Marque du chasseur*
-   *Saut*
-   *Grande foulée*
-   *Parler avec les animaux*



### 2ème niveau

-   *Messager animal*
-   *Peau d'écorce*
-   *Vision dans le noir*
-   *Sens des pièges*
-   *Restauration partielle*
-   *Localiser les animaux ou les plantes*
-   *Localiser un objet*
-   *Passage sans trace*
-   *Protection contre le poison*
-   *Silence*
-   *Croissance d'épines*



### 3ème niveau

-   *Invocation d'animaux*
-   *Lumière du jour*
-   *Antidétection*
-   *Croissance végétale*
-   *Protection contre une énergie*
-   *Parler avec les plantes*
-   *Respiration aquatique*
-   *Marche sur l'eau*
-   *Mur de vent*



### 4ème niveau

-   *Invocation d'êtres sylvestres*
-   *Liberté de mouvement*
-   *Localiser une créature*
-   *Peau de pierre*



### 5ème niveau

-   *Communion avec la nature*
-   *Foulée d'arbre*




## Sorts de sorcier


### Tours de magie (0 Niveau)

-   *Aspersion d'acide*
-   *Contact glacial*
-   *Lumières dansantes*
-   *Trait de feu*
-   *Lumière*
-   *Main de mage*
-   *Réparation*
-   *Message*
-   *Illusion mineure*
-   *Bouffée de poison*
-   *Prestidigitation*
-   *Rayon de givre*
-   *Poigne électrique*
-   *Coup au but*



### 1er niveau

-   *Mains brûlantes*
-   *Charme-personne*
-   *Couleurs dansantes*
-   *Compréhension des langues*
-   *Détection de la magie*
-   *Déguisement de soi*
-   *Repli expéditif*
-   *Simulacre de vie*
-   *Feuille morte*
-   *Nappe de brouillard*
-   *Saut*
-   *Armure de mage*
-   *Projectile magique*
-   *Bouclier*
-   *Image silencieuse*
-   *Sommeil*
-   *Vague tonnante*



### 2ème niveau

-   *Modification d'apparence*
-   *Cécité/surdité*
-   *Flou*
-   *Ténèbres*
-   *Vision dans le noir*
-   *Détection des pensées*
-   *Amélioration de caractéristique*
-   *Agrandissement/rapetissement*
-   *Bourrasque de vent*
-   *Immobilisation de personne*
-   *Invisibilité*
-   *Déblocage*
-   *Lévitation*
-   *Image miroir*
-   *Pas brumeux*
-   *Rayon ardent*
-   *Voir l'Invisibilité*
-   *Fracassement*
-   *Pattes d'araignée*
-   *Suggestion*
-   *Toile d'araignée*



### 3ème niveau

-   *Clignotement*
-   *Clairvoyance*
-   *Contresort*
-   *Lumière du jour*
-   *Dissipation de la magie*
-   *Décharge traumaturgique*
-   *Peur*
-   *Boule de feu*
-   *Vol*
-   *Forme gazeuse*
-   *Hâte*
-   *Motif hypnotique*
-   *Éclair*
-   *Image majeure*
-   *Protection contre une énergie*
-   *Tempête de neige*
-   *Lenteur*
-   *Nuage nauséabond*
-   *Don des langues*
-   *Respiration aquatique*
-   *Marche sur l'eau*



### 4ème niveau

-   *Bannissement*
-   *Flétrissement*
-   *Confusion*
-   *Porte dimensionnelle*
-   *Domination de bête*
-   *Invisibilité supérieure*
-   *Tempête de grêle*
-   *Métamorphose*
-   *Peau de pierre*
-   *Mur de feu*



### 5ème niveau

-   *Animation d'objets*
-   *Nuage mortelle*
-   *Cône de froid*
-   *Création*
-   *Domination de personne*
-   *Immobilisation de monstre*
-   *Fléau d'insectes*
-   *Apparence trompeuse*
-   *Télékinésie*
-   *Cercle de téléportation*
-   *Mur de pierre*



### 6ème niveau

-   *Chaîne d'éclairs*
-   *Cercle de mort*
-   *Désintégration*
-   *Mauvais oeil*
-   *Globe d'invulnérabilité*
-   *Suggestion de groupe*
-   *Façonnage de la Terre*
-   *Rayon de soleil*
-   *Vision suprême*



### 7ème niveau

-   *Boule de feu à retardement*
-   *Forme éthérée*
-   *Doigt de mort*
-   *Tempête de feu*
-   *Changement de plan*
-   *Rayons prismatiques*
-   *Inversion de la gravité*
-   *Téléportation*



### 8ème niveau

-   *Domination de monstre*
-   *Tremblement de terre*
-   *Nuage incendiaire*
-   *Mot de pouvoir étourdissant*
-   *Éclat du soleil*



### 9ème niveau

-   *Portail*
-   *Nuée de météores*
-   *Mot de pouvoir mortel*
-   *Arrêt du temps*
-   *Souhait*




## Sorts d'Invocateur


### Tours de magie (0 Niveau)

-   *Contact glacial*
-   *Main de mage*
-   *Illusion mineure*
-   *Prestidigitation*
-   *Coup au but*



### 1er niveau

-   *Charme-personne*
-   *Compréhension des langues*
-   *Repli expéditif*
-   *Représailles infernales*
-   *Texte illusoire*
-   *Protection contre le Bien et le
    Mal*
-   *Serviteur invisible*



### 2ème niveau

-   *Ténèbres*
-   *Captiver*
-   *Immobilisation de personne*
-   *Invisibilité*
-   *Image miroir*
-   *Pas brumeux*
-   *Rayon affaiblissant*
-   *Fracassement*
-   *Pattes d'araignée*
-   *Suggestion*



### 3ème niveau

-   *Contresort*
-   *Dissipation de la magie*
-   *Peur*
-   *Vol*
-   *Forme gazeuse*
-   *Motif hypnotique*
-   *Cercle magique*
-   *Image majeure*
-   *Délivrance des malédictions*
-   *Don des langues*
-   *Toucher du vampire*



### 4ème niveau

-   *Bannissement*
-   *Flétrissement*
-   *Porte dimensionnelle*
-   *Terrain halluciné*



### 5ème niveau

-   *Contact avec un autre plan*
-   *Songe*
-   *Immobilisation de monstre*
-   *Scrutation*



### 6ème niveau

-   *Cercle de mort*
-   *Invocation de fée*
-   *Création de mort-vivant*
-   *Mauvais oeil*
-   *La Pétrification*
-   *Suggestion de groupe*
-   *Vision suprême*



### 7ème niveau

-   *Forme éthérée*
-   *Doigt de mort*
-   *Cage de force*
-   *Changement de plan*



### 8ème niveau

-   *Demi-plans*
-   *Domination de monstre*
-   *Esprit faible*
-   *Bagou*
-   *Mot de pouvoir étourdissant*



### 9ème niveau

-   *Projection astrale*
-   *Prémonition*
-   *Emprisonnement*
-   *Mot de pouvoir mortel*
-   *Métamorphose véritable*




## Sorts de magicien


### Tours de magie (0 Niveau)

-   *Aspersion d'acide*
-   *Contact glacial*
-   *Lumières dansantes*
-   *Trait de feu*
-   *Lumière*
-   *Main de mage*
-   *Réparation*
-   *Message*
-   *Illusion mineure*
-   *Bouffée de poison*
-   *Prestidigitation*
-   *Rayon de givre*
-   *Poigne électrique*
-   *Coup au but*



### 1er niveau

-   *Alarme*
-   *Mains brûlantes*
-   *Charme-personne*
-   *Couleurs dansantes*
-   *Compréhension des langues*
-   *Détection de la magie*
-   *Déguisement de soi*
-   *Repli expéditif*
-   *Simulacre de vie*
-   *Feuille morte*
-   *Appel de familier*
-   *Disque flottant*
-   *Nappe de brouillard*
-   *Graisse*
-   *Rire hideux*
-   *Identification*
-   *Texte illusoire*
-   *Saut*
-   *Grande foulée*
-   *Armure de mage*
-   *Projectile magique*
-   *Protection contre le Bien et le
    Mal*
-   *Bouclier*
-   *Image silencieuse*
-   *Sommeil*
-   *Vague tonnante*
-   *Serviteur invisible*



### 2ème niveau

-   *Flèche acide*
-   *Modification d'apparence*
-   *Verrou occulte*
-   *Aura magique de l'arcaniste*
-   *Cécité/surdité*
-   *Flou*
-   *Flamme continuelle*
-   *Ténèbres*
-   *Vision dans le noir*
-   *Détection des pensées*
-   *Agrandissement/rapetissement*
-   *Sphère de feu*
-   *Préservation des morts*
-   *Bourrasque de vent*
-   *Immobilisation de personne*
-   *Invisibilité*
-   *Déblocage*
-   *Lévitation*
-   *Localiser un objet*
-   *Bouche magique*
-   *Arme magique*
-   *Image miroir*
-   *Pas brumeux*
-   *Rayon affaiblissant*
-   *Corde enchantée*
-   *Rayon ardent*
-   *Voir l'Invisibilité*
-   *Fracassement*
-   *Pattes d'araignée*
-   *Suggestion*
-   *Toile d'araignée*



### 3ème niveau

-   *Mort animée*
-   *Invoquer la malédiction*
-   *Clignotement*
-   *Clairvoyance*
-   *Contresort*
-   *Dissipation de la magie*
-   *Peur*
-   *Boule de feu*
-   *Vol*
-   *Forme gazeuse*
-   *Glyphe de protection*
-   *Hâte*
-   *Motif hypnotique*
-   *Éclair*
-   *Cercle magique*
-   *Image majeure*
-   *Antidétection*
-   *Monture fantôme*
-   *Protection contre une énergie*
-   *Délivrance des malédictions*
-   *Envoi de*
-   *Tempête de neige*
-   *Lenteur*
-   *Nuage nauséabond*
-   *Très petite (TP)*
-   *Don des langues*
-   *Toucher du vampire*
-   *Respiration aquatique*



### 4ème niveau

-   *Oeil occulte*
-   *Bannissement*
-   *Tentacules noirs*
-   *Flétrissement*
-   *Confusion*
-   *Invocation d'élémentaux mineurs*
-   *Contrôle de l'eau*
-   *Porte dimensionnelle*
-   *Fabrication*
-   *Chien fidèle*
-   *Bouclier de feu*
-   *Invisibilité supérieure*
-   *Terrain halluciné*
-   *Tempête de grêle*
-   *Localiser une créature*
-   *Assassin imaginaire*
-   *Métamorphose*
-   *Sanctuaire privé*
-   *Sphère Résiliente*
-   *Coffre secret*
-   *Façonnage de la pierre*
-   *Peau de pierre*
-   *Mur de feu*



### 5ème niveau

-   *Animation d'objets*
-   *Main d'arcane*
-   *Nuage mortelle*
-   *Cône de froid*
-   *Invocation d'élémentaire*
-   *Contact avec un autre plan*
-   *Création*
-   *Domination de personne*
-   *Songe*
-   *Quête*
-   *Immobilisation de monstre*
-   *Légendes traditionnelles*
-   *Double illusoire*
-   *Modification de mémoire*
-   *Passe-muraille*
-   *Contrat*
-   *Scrutation*
-   *Apparence trompeuse*
-   *Télékinésie*
-   *Lien Télépathique*
-   *Cercle de téléportation*
-   *Mur de force*
-   *Mur de pierre*



### 6ème niveau

-   *Chaîne d'éclairs*
-   *Cercle de mort*
-   *Prévoyance*
-   *Création de mort-vivant*
-   *Désintégration*
-   *Mauvais oeil*
-   *La Pétrification*
-   *Sphère de congélation*
-   *Globe d'invulnérabilité*
-   *Protections et sceaux*
-   *Invocations instantanées*
-   *Danse irrésistible*
-   *Urne magique*
-   *Suggestion de groupe*
-   *Façonnage de la Terre*
-   *Illusion programmée*
-   *Rayon de soleil*
-   *Vision suprême*
-   *Mur de glace*



### 7ème niveau

-   *Épée Arcane*
-   *Boule de feu à retardement*
-   *Forme éthérée*
-   *Doigt de mort*
-   *Cage de force*
-   *Magnifique manoir*
-   *Mirage Occulte*
-   *Changement de plan*
-   *Rayons prismatiques*
-   *Projection d'image*
-   *Inversion de la gravité*
-   *Dissimulation*
-   *Simulacre*
-   *Symbole*
-   *Téléportation*



### 8ème niveau

-   *Champ antimagie*
-   *Aversion/attirance*
-   *Clone*
-   *Contrôle du climat*
-   *Demi-plans*
-   *Domination de monstre*
-   *Esprit faible*
-   *Nuage incendiaire*
-   *Dédale*
-   *Esprit impénétrable*
-   *Mot de pouvoir étourdissant*
-   *Éclat du soleil*



### 9ème niveau

-   *Projection astrale*
-   *Prémonition*
-   *Portail*
-   *Emprisonnement*
-   *Nuée de météores*
-   *Mot de pouvoir mortel*
-   *Mur prismatique*
-   *Changement de forme*
-   *Arrêt du temps*
-   *Métamorphose véritable*
-   *Étrangeté*
-   *Souhait*




## Descriptions des sorts


#### Flèche acide

*Evocation de 2ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S, M (une minuscule cloche et un morceau de fil
d'argent fin).

**Durée :** 8 heures

Une flèche verte scintillante se dirige vers une cible à portée et
éclate en un jet d'acide. Effectuez une attaque de sort à distance
contre la cible. En cas de succès, la cible subit 4d4 points de dégâts
d'acide immédiatement et 2d4 points de dégâts d'acide à la fin de son
prochain tour. En cas d'échec, la flèche éclabousse la cible d'acide
pour la moitié des dégâts initiaux et aucun dégât à la fin de son
prochain tour.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, les dégâts (initiaux et
ultérieurs) augmentent de 1d4 pour chaque niveau d'emplacement
supérieur au 2e.



#### Aspersion d'acide

*Tour de magie de conjuration*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous lancez une bulle d'acide. Choisissez une créature à portée, ou
choisissez deux créatures à portée qui sont à moins de 1,5 m l'une de
l'autre. La cible doit réussir un jet de sauvegarde de Dextérité ou
subir 1d6 points de dégâts d'acide.

Les dégâts de ce sort augmentent de 1d6 lorsque vous atteignez le 5e
niveau (2d6), le 11e niveau (3d6) et le 17e niveau (4d6).



#### Aide

*ab ab abjuration de 2ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une minuscule bande de tissu blanc)

**Durée :** 8 heures

Ce sort renforce la résistance et la détermination de vos alliés.
Choisissez jusqu'à trois créatures à portée. Le maximum de points de
vie et les points de vie actuels de chaque cible augmentent de 5 pour la
durée du sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, les points de vie de la
cible augmentent de 5 supplémentaires pour chaque niveau d'emplacement
supérieur au 2e.



#### Alarme

*abjuration de 1er niveau (rituel)*

**Classes :** Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composants :** V, S, M (feuille de rhubarbe en poudre et estomac de
vipère)

**Durée :** Instantané

Vous déclenchez une alarme contre les intrusions non désirées.
Choisissez une porte, une fenêtre ou une zone à portée qui n'est pas
plus grande qu'un cube de 6 mètres de côté. Jusqu'à la fin du sort,
une alarme vous avertit dès qu'une créature Très petite (TP) ou plus
grande touche ou entre dans la zone surveillée. Lorsque vous lancez le
sort, vous pouvez désigner les créatures qui ne déclencheront pas
l'alarme. Vous pouvez également choisir si l'alarme est mentale ou
audible.

Une alarme mentale vous avertit par un ping dans votre esprit si vous
vous trouvez à moins d'un kilomètre de la zone surveillée. Ce ping vous
réveille si vous dormez.

Une alarme sonore produit le son d'une cloche à main pendant 10
secondes dans un rayon de 60 pieds.



#### Modification d'apparence

*transmutation de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous prenez une forme différente. Lorsque vous lancez le sort,
choisissez l'une des options suivantes, dont les effets durent pendant
toute la durée du sort. Pendant que le sort dure, vous pouvez mettre fin
à une option en tant qu'action pour bénéficier des avantages d'une
autre option.

***Adaptation aquatique.*** Vous adaptez votre corps à un environnement
aquatique, en faisant pousser des branchies et des toiles entre vos
doigts. Vous pouvez respirer sous l'eau et gagner une vitesse de nage
égale à votre vitesse de marche.

***Changer d'apparence.*** Vous transformez votre apparence. Vous
décidez de votre apparence, notamment de votre taille, de votre poids,
des traits de votre visage, du son de votre voix, de la longueur de vos
cheveux, de leur coloration et de vos caractéristiques distinctives, le
cas échéant. Vous pouvez vous faire passer pour un membre d'une autre
origine, mais aucune de vos statistiques ne change. Vous ne pouvez pas non
plus vous faire passer pour une créature d'une taille différente de la
vôtre, et votre forme de base reste la même ; si vous êtes bipède, vous
ne pouvez pas utiliser ce sort pour devenir quadrupède, par exemple. À
tout moment pendant la durée du sort, vous pouvez utiliser votre action
pour changer à nouveau votre apparence de cette manière.

***Armes naturelles.*** Vous faites pousser des griffes, des crocs, des
épines, des cornes, ou une autre arme naturelle de votre choix. Vos
frappes à mains nues infligent 1d6 points de dégâts contondants,
perforants ou tranchants, selon l'arme naturelle choisie, et vous avez
une bonne maîtrise de vos frappes à mains nues. Enfin, l'arme naturelle
est magique et vous bénéficiez d'un bonus de +1 aux jets d'attaque et
de dégâts que vous effectuez en l'utilisant.



#### Amitié avec les animaux

*Enchantement de 1er niveau*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un morceau de nourriture)

**Durée :** 24 heures

Ce sort vous permet de convaincre une bête que vous ne lui voulez aucun
mal. Choisissez une bête que vous pouvez voir à portée. Elle doit vous
voir et vous entendre. Si l'Intelligence de la bête est supérieure ou
égale à 4, le sort échoue. Sinon, la bête doit réussir un jet de
sauvegarde de Sagesse ou être charmée par vous pour la durée du sort. Si
vous ou l'un de vos compagnons blessez la cible, le sort prend fin.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, vous pouvez affecter une bête
supplémentaire à chaque niveau supérieur au 1er.



#### Messager animal

*Enchantement de 2ème niveau (rituel)*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un morceau de nourriture)

**Durée :** 24 heures

Grâce à ce sort, vous utilisez un animal pour délivrer un message.
Choisissez une Très petite (TP) bête que vous pouvez voir à portée,
comme un écureuil, un geai bleu ou une chauve-souris. Vous spécifiez un
lieu, que vous devez avoir visité, et un destinataire qui correspond à
une description générale, comme \"un homme ou une femme portant
l'uniforme du garde municipal\" ou \"un nain roux portant un chapeau
pointu\". Vous prononcez également un message d'un maximum de
vingt-cinq mots. La bête cible se déplace pendant la durée du sort vers
l'endroit spécifié, couvrant environ 50 miles par 24 heures pour un
messager volant, ou 25 miles pour les autres animaux.

Lorsque le messager arrive, il transmet votre message à la créature que
vous avez décrite, en reproduisant le son de votre voix. Le messager ne
parle qu'à une créature correspondant à la description que vous avez
donnée. Si le messager n'atteint pas sa destination avant la fin du
sort, le message est perdu et la bête retourne à l'endroit où vous avez
lancé ce sort.

***Aux niveaux supérieurs.*** Si vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, la durée du sort augmente de
48 heures pour chaque niveau d'emplacement supérieur au 2e.



#### Formes animales

*Transmutation de 8ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 24 heures

Votre magie transforme les autres en bêtes. Choisissez un nombre
quelconque de créatures consentantes que vous pouvez voir à portée. Vous
transformez chaque cible sous la forme d'une bête de taille Grand ou
plus petite, avec un indice de difficulté de 4 ou moins. Lors des tours
suivants, vous pouvez utiliser votre action pour transformer les
créatures affectées en de nouvelles formes.

La transformation dure pendant la durée pour chaque cible, ou jusqu'à
ce que la cible tombe à 0 points de vie ou meure. Vous pouvez choisir
une forme différente pour chaque cible. Les statistiques de jeu d'une
cible sont remplacées par les statistiques de la bête choisie, bien que
la cible conserve son alignement et ses scores d'Intelligence, de
Sagesse et de Charisme. La cible assume les points de vie de sa nouvelle
forme, et lorsqu'elle revient à sa forme normale, elle retrouve le
nombre de points de vie qu'elle avait avant sa transformation. Si elle
revient à sa forme normale parce qu'elle est tombée à 0 point de vie,
tout dommage excédentaire est reporté sur sa forme normale. Tant que les
dégâts excédentaires ne ramènent pas la forme normale de la créature à 0
point de vie, elle n'est pas frappée d'inconscience. La créature est
limitée dans les actions qu'elle peut effectuer par la nature de sa
nouvelle forme, et elle ne peut ni parler ni lancer de sorts.

L'équipement de la cible se fond dans sa nouvelle forme. La cible ne
peut pas activer, manier ou bénéficier d'un quelconque de ses
équipements.



#### Mort animée

*Nécromancie de 3e niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 minute

**Portée :** 10 pieds

**Composantes :** V, S, M (une goutte de sang, un morceau de chair, et
une pincée de poussière d'os)

**Durée :** Instantané

Ce sort crée un serviteur mort-vivant. Choisissez un tas d'os ou le
cadavre d'un humanoïde de taille moyenne ou petite à portée. Votre sort
imprègne la cible d'un immonde mimétisme de vie, l'élevant au rang de
créature mort-vivante. La cible devient un squelette si vous avez choisi
des os ou un zombi si vous avez choisi un cadavre (le MJ dispose des
statistiques de jeu de la créature).

À chacun de vos tours, vous pouvez utiliser une action bonus pour
commander mentalement toute créature que vous avez créée avec ce sort si
elle se trouve à moins de 60 pieds de vous (si vous contrôlez plusieurs
créatures, vous pouvez en commander une ou toutes en même temps, en
donnant le même ordre à chacune). Vous décidez de l'action de la
créature et de l'endroit où elle se déplacera lors de son prochain
tour, ou vous pouvez donner un ordre général, comme celui de garder une
chambre ou un couloir particulier. Si vous ne donnez aucun ordre, la
créature ne se défend que contre les créatures hostiles. Une fois
l'ordre donné, la créature continue à le suivre jusqu'à ce que sa
tâche soit terminée.

La créature est sous votre contrôle pendant 24 heures, après quoi elle
cesse d'obéir aux ordres que vous lui avez donnés. Pour conserver le
contrôle de la créature pendant 24 heures supplémentaires, vous devez
lancer à nouveau ce sort sur la créature avant la fin de la période de
24 heures en cours. Cette utilisation du sort réaffirme votre contrôle
sur un maximum de quatre créatures que vous avez animées avec ce sort,
plutôt que d'en animer une nouvelle.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 4e niveau ou plus, vous animez ou réaffirmez le
contrôle sur deux créatures mort-vivantes supplémentaires pour chaque
niveau d'emplacement au-dessus du 3e. Chacune de ces créatures doit
provenir d'un cadavre ou d'un tas d'os différent.



#### Animation d'objets

*transmutation de 5ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Les objets prennent vie à votre injonction. Choisissez jusqu'à dix
objets non magiques à portée de main, qui ne sont ni portés, ni
transportés. Les cibles moyennes comptent pour deux objets, les cibles
Grandes comptent pour quatre objets, les cibles Très Grandes comptent
pour huit objets. Vous ne pouvez pas animer un objet plus grand que Très
grand. Chaque cible s'anime et devient une créature sous votre contrôle
jusqu'à la fin du sort ou jusqu'à ce qu'elle soit réduite à 0 points
de vie.

Comme action bonus, vous pouvez commander mentalement toute créature que
vous avez créée avec ce sort si elle se trouve à moins de 500 pieds de
vous (si vous contrôlez plusieurs créatures, vous pouvez en commander
une ou toutes en même temps, en donnant le même ordre à chacune). Vous
décidez de l'action de la créature et de l'endroit où elle se
déplacera lors de son prochain tour, ou vous pouvez donner un ordre
général, comme celui de garder une chambre ou un couloir particulier. Si
vous ne donnez aucun ordre, la créature ne se défend que contre les
créatures hostiles. Une fois l'ordre donné, la créature continue à le
suivre jusqu'à ce que sa tâche soit terminée.

  ---------------------------------------------------------
  Taille    HP   AC  Attaque                     Str   Dex
  -------- ---- ---- -------------------------- ----- -----
  Très      20   18  +8 pour toucher, 1d4 + 4     4    18
  petite             dommages                         
  (TP)                                                

  Petit     25   16  +6 pour toucher, 1d8 + 2     6    14
                     dommages                         

  Moyen     40   13  +5 pour toucher, 2d6 + 1    10    12
                     dommages                         

  Grand     50   10  +6 pour toucher, 2d10 + 2   14    10
                     dommages                         

  Très      80   10  +8 pour toucher, 2d12 + 4   18     6
  grand              dommages                         
  ---------------------------------------------------------

  : Statistiques des objets animés

Un objet animé est une construction dont la CA, les points de vie, les
attaques, la Force et la Dextérité sont déterminés par sa taille. Sa
Constitution est de 10, son Intelligence et sa Sagesse de 3, et son
Charisme de 1. Sa vitesse est de 30 pieds ; si l'objet n'a pas de
jambes ou d'autres appendices qu'il peut utiliser pour se déplacer, il
a une vitesse de vol de 30 pieds et peut faire du surplace. Si l'objet
est solidement attaché à une surface ou à un objet plus grand, comme une
chaîne boulonnée à un mur, sa vitesse est de 0. Il a une vision aveugle
avec un rayon de 30 pieds et est aveugle au-delà de cette distance.
Lorsque l'objet animé tombe à 0 points de vie, il reprend sa forme
d'origine, et les dégâts restants sont reportés sur sa forme
d'origine.

Si vous donnez l'ordre à un objet d'attaquer, il peut effectuer une
seule attaque de mêlée contre une créature située à moins de 1,5 m de
lui. Il effectue une attaque de type slam avec un bonus d'attaque et
des dégâts contondants déterminés par sa taille. Le MJ peut décider
qu'un objet spécifique inflige des dégâts tranchants ou perforants en
fonction de sa forme.

***À des niveaux supérieurs.*** Si vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, vous pouvez animer deux objets
supplémentaires pour chaque niveau d'emplacement supérieur au 5e.



#### Coquille antivie

*ab ab abjuration de niveau 5*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Soi-même (rayon de 10 pieds)

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Une barrière scintillante s'étend à partir de vous dans un rayon de 3
mètres et se déplace avec vous, restant centrée sur vous et protégeant
les créatures autres que les morts-vivants et les constructions. La
barrière dure toute la durée du sort.

La barrière empêche une créature affectée de passer ou d'atteindre à
travers. Une créature affectée peut lancer des sorts ou effectuer des
attaques avec des armes à distance ou à portée à travers la barrière.

Si vous vous déplacez de manière à ce qu'une créature affectée soit
forcée de passer à travers la barrière, le sort prend fin.



#### Champ antimagie

*abjuration de 8ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Soi-même (sphère de 10 pieds de rayon)

**Composantes :** V, S, M (une pincée de fer en poudre ou de limaille de
fer)

**Durée :** Concentration, jusqu'à 1 heure

Une sphère invisible d'antimagie de 3 mètres de rayon vous entoure.
Cette zone est séparée de l'énergie magique qui imprègne le multivers.
À l'intérieur de la sphère, les sorts ne peuvent pas être lancés, les
créatures invoquées disparaissent, et même les objets magiques
deviennent banals. Jusqu'à la fin du sort, la sphère se déplace avec
vous, centrée sur vous.

Les sorts et autres effets magiques, à l'exception de ceux créés par un
artefact ou une divinité, sont supprimés dans la sphère et ne peuvent
pas y faire saillie. Un emplacement dépensé pour lancer un sort supprimé
est consommé. Lorsqu'un effet est supprimé, il ne fonctionne pas, mais
le temps qu'il passe à être supprimé compte dans sa durée.

***Effets ciblés.*** Les sorts et autres effets magiques, comme
*Projectile magique* et
*Charme-personne*, qui ciblent une créature ou un objet
dans la sphère n'ont aucun effet sur cette cible.

***Zones de magie.*** La zone d'un autre sort ou effet magique, comme
une *boule de feu*, ne peut pas s'étendre dans la sphère.
Si la sphère chevauche une zone de magie, la partie de la zone qui est
couverte par la sphère est supprimée. Par exemple, les flammes créées
par un *mur de feu* sont supprimées dans la sphère,
créant un vide dans le mur si le chevauchement est assez grand.

***Sorts.*** Tout sort actif ou autre effet magique sur une créature ou
un objet dans la sphère est supprimé tant que la créature ou l'objet
s'y trouve.

***Objets magiques.*** Les propriétés et les pouvoirs des objets
magiques sont supprimés dans la sphère. Par exemple, une *épée longue
+1* dans la sphère fonctionne comme une épée longue non-magique.

Les propriétés et les pouvoirs d'une arme magique sont supprimés si
elle est utilisée contre une cible dans la sphère ou brandie par un
attaquant dans la sphère. Si une arme magique ou une munition magique
quitte complètement la sphère (par exemple, si vous tirez une flèche
magique ou lancez une lance magique sur une cible en dehors de la
sphère), la magie de l'objet cesse d'être supprimée dès sa sortie.

***Voyages magiques.*** La téléportation et les voyages planaires ne
fonctionnent pas dans la sphère, que la sphère soit la destination ou le
point de départ de ces voyages magiques. Un portail vers un autre lieu,
monde ou plan d'existence, ainsi qu'une ouverture vers un espace
extradimensionnel comme celui créé par le sort *Corde*
enchantée, se ferme temporairement lorsqu'il se trouve dans la sphère.

***Créatures et objets.*** Une créature ou un objet invoqué ou créé par
la magie disparaît temporairement de la sphère. Une telle créature
réapparaît instantanément dès que l'espace qu'elle occupait n'est
plus dans la sphère.

***Dissipation de la magie.*** Les sorts et les effets magiques tels que
*Dissipation de la magie* n'ont aucun effet sur la
sphère. De même, les sphères créées par différents sorts de champ
*antimagie* ne s'annulent pas les unes les autres.



#### Aversion/attirance

*Enchantement de 8ème niveau*

**Classes :** Druide

**Durée du moulage :** 1 heure

**Portée :** 60 pieds

**Composantes :** V, S, M (soit un morceau d'alun trempé dans du
vinaigre pour l'effet d'antipathie, soit une goutte de miel pour
l'effet de sympathie).

**Durée :** 10 jours

Ce sort attire ou repousse les créatures de votre choix. Vous ciblez
quelque chose à portée, soit un objet ou une créature Très grand ou plus
petit, soit une zone dont la taille ne dépasse pas un cube de 200 pieds.
Précisez ensuite un type de créature intelligente, comme les dragons
rouges, les gobelins ou les vampires. Vous investissez la cible d'une
aura qui attire ou repousse les créatures spécifiées pour la durée du
sort. Choisissez l'antipathie ou la sympathie comme effet de l'aura.

***Antipathie.*** L'enchantement fait en sorte que les créatures du
genre que vous avez désigné ressentent un besoin intense de quitter la
zone et d'éviter la cible. Lorsqu'une telle créature peut voir la
cible ou se trouve à moins de 60 pieds d'elle, elle doit réussir un jet
de sauvegarde de Sagesse ou devenir effrayée. La créature reste effrayée
tant qu'elle peut voir la cible ou qu'elle se trouve à moins de 60
pieds d'elle. Tant qu'elle est effrayée par la cible, la créature doit
utiliser son mouvement pour se déplacer vers l'endroit sûr le plus
proche d'où elle ne peut pas voir la cible. Si la créature se déplace à
plus de 60 pieds de la cible et ne peut pas la voir, elle n'est plus
effrayée, mais elle redevient effrayée si elle retrouve la vue de la
cible ou se déplace à moins de 60 pieds d'elle.

***Sympathie.*** L'enchantement fait que les créatures spécifiées
ressentent une envie intense de s'approcher de la cible lorsqu'elles
se trouvent à moins de 60 pieds d'elle ou qu'elles sont capables de la
voir. Lorsqu'une telle créature peut voir la cible ou s'en approche à
moins de 60 pieds, elle doit réussir un jet de sauvegarde de Sagesse ou
utiliser son mouvement à chacun de ses tours pour entrer dans la zone ou
se déplacer à portée de la cible. Lorsque la créature l'a fait, elle ne
peut pas s'éloigner volontairement de la cible.

Si la cible endommage ou blesse une créature affectée, celle-ci peut
effectuer un jet de sauvegarde de Sagesse pour mettre fin à l'effet,
comme décrit ci-dessous.

***Fin de l'effet.*** Si une créature affectée termine son tour alors
qu'elle n'est pas à moins de 60 pieds de la cible ou capable de la
voir, elle effectue un jet de sauvegarde de Sagesse. En cas de
sauvegarde réussie, la créature n'est plus affectée par la cible et
reconnaît le sentiment de répugnance ou d'attraction comme magique. De
plus, une créature affectée par le sort a droit à un autre jet de
sauvegarde de Sagesse toutes les 24 heures tant que le sort persiste.

Une créature qui réussit à se protéger contre cet effet est immunisée
contre lui pendant 1 minute, après quoi elle peut être affectée à
nouveau.



#### Oeil occulte

*Divination de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un peu de fourrure de chauve-souris)

**Durée :** Concentration, jusqu'à 1 heure

Vous créez un œil invisible et magique dans la portée qui plane dans
l'air pour la durée.

Vous recevez mentalement des informations visuelles de l'œil, qui a une
vision normale et une vision dans le noir jusqu'à 30 pieds. L'œil peut
regarder dans toutes les directions.

Comme une action, vous pouvez déplacer l'œil jusqu'à 30 pieds dans
n'importe quelle direction. Il n'y a pas de limite à la distance à
laquelle l'œil peut se déplacer, mais il ne peut pas entrer dans un
autre plan d'existence. Une barrière solide bloque le mouvement de
l'œil, mais l'œil peut passer à travers une ouverture d'un diamètre
aussi petit qu'un pouce.



#### Main d'arcane

*Evocation de 5ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (une coquille d'œuf et un gant en peau de
serpent)

**Durée :** Concentration, jusqu'à 1 minute

Vous créez une Grande main de force chatoyante et translucide dans un
espace inoccupé que vous pouvez voir à portée. La main dure le temps du
sort, et elle se déplace à votre commande, imitant les mouvements de
votre propre main.

La main est un objet qui a une CA de 20 et un nombre de points de vie
égal à votre maximum de points de vie. Si elle tombe à 0 points de vie,
le sort prend fin. Elle a une Force de 26 (+8) et une Dextérité de 10
(+0). La main ne remplit pas son espace.

Lorsque vous lancez le sort et comme action bonus à vos tours suivants,
vous pouvez déplacer la main jusqu'à 60 pieds et ensuite provoquer
l'un des effets suivants avec elle.

***Poing serré.*** La main frappe une créature ou un objet dans un rayon
de 1,5 m autour d'elle. Effectuez une attaque de sort en mêlée pour la
main en utilisant vos statistiques de jeu. En cas de succès, la cible
subit 4d8 points de dégâts de force.

***Main de force.*** La main tente de pousser une créature se trouvant à
moins de 1,5 mètre d'elle dans la direction de votre choix. Faites un
test avec la Force de la main contestée par le test de Force
(Athlétisme) de la cible. Si la cible est de taille moyenne ou plus
petite, vous avez un avantage sur ce test. Si vous réussissez, la main
pousse la cible jusqu'à 5 pieds plus un nombre de pieds égal à cinq
fois votre modificateur de caractéristique d'incantation. La main se
déplace avec la cible pour rester à moins de 1,5 mètre d'elle.

***Main agrippante.*** La main tente d'agripper une créature Très
grande ou plus petite dans un rayon de 1,5 m autour d'elle. Vous
utilisez le score de Force de la main pour résoudre l'agrippement. Si
la cible est de taille moyenne ou plus petite, vous avez un avantage sur
ce test. Pendant que la main agrippe la cible, vous pouvez utiliser une
action bonus pour l'écraser. La cible subit alors des dégâts de
contondant égaux à 2d6 + votre modificateur de capacité d'incantation.

***Main interposée.*** La main s'interpose entre vous et une créature
de votre choix jusqu'à ce que vous lui donniez une autre Injonction. La
main se déplace pour rester entre vous et la cible, vous offrant un
demi-abri contre la cible. La cible ne peut pas se déplacer dans
l'espace de la main si son score de Force est inférieur ou égal au
score de Force de la main. Si son score de Force est supérieur au score
de Force de la main, la cible peut se déplacer vers vous à travers
l'espace de la main, mais cet espace est un terrain difficile pour la
cible.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, les dégâts de l'option
poing serré augmentent de 2d8 et ceux de la main agrippante de 2d6 pour
chaque niveau d'emplacement supérieur au 5e.



#### Verrou occulte

*l'ab juration de 2ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (poussière d'or valant au moins 25 gp, que le
sort consomme)

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous touchez une porte, une fenêtre, un portail, un coffre ou toute
autre entrée fermée, et elle devient verrouillée pour la durée du sort.
Vous et les créatures que vous désignez lorsque vous lancez ce sort
pouvez ouvrir l'objet normalement. Vous pouvez également définir un mot
de passe qui, lorsqu'il est prononcé à moins de 1,5 mètre de l'objet,
supprime ce sort pendant 1 minute. Sinon, l'objet est infranchissable
jusqu'à ce qu'il soit brisé ou que le sort soit dissipé ou supprimé.
Le lancement de *Déblocage* sur l'objet supprime *Verrou
occulte* pendant 10 minutes.

Lorsqu'il est affecté par ce sort, l'objet est plus difficile à briser
ou à forcer ; le DC pour le briser ou crocheter les serrures augmente de
10.



#### Épée Arcane

*7ème niveau d'évocation*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (une épée miniature en platine avec une
poignée et un pommeau en cuivre et en zinc, d'une valeur de 250 gp).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez un plan de force en forme d'épée qui plane à portée. Il dure
le temps nécessaire.

Lorsque l'épée apparaît, vous effectuez une attaque de sort en mêlée
contre une cible de votre choix située à moins de 1,5 mètre de l'épée.
En cas de succès, la cible subit 3d10 points de dégâts de force.
Jusqu'à la fin du sort, vous pouvez utiliser une action bonus à chacun
de vos tours pour déplacer l'épée jusqu'à 6 mètres vers un endroit que
vous pouvez voir et répéter cette attaque contre la même cible ou une
autre.



#### Aura magique de l'arcaniste

*illusion de 2ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un petit carré de soie)

**Durée :** 24 heures

Vous placez une illusion sur une créature ou un objet que vous touchez
afin que les sorts de divination révèlent de fausses informations à son
sujet. La cible peut être une créature consentante ou un objet qui
n'est pas porté par une autre créature.

Lorsque vous lancez le sort, choisissez l'un ou les deux effets
suivants. L'effet dure pendant toute la durée du sort. Si vous lancez
ce sort sur la même créature ou le même objet tous les jours pendant 30
jours, en y plaçant le même effet à chaque fois, l'illusion dure
jusqu'à ce qu'elle soit dissipée.

***Fausse Aura.*** Vous modifiez la façon dont la cible apparaît aux
sorts et aux effets magiques, tels que *détecter la
magie*, qui détectent les auras magiques. Vous pouvez
faire en sorte qu'un objet non magique paraisse magique, qu'un objet
magique paraisse non magique, ou changer l'aura magique de l'objet
pour qu'il semble appartenir à une école de magie spécifique que vous
choisissez. Lorsque vous utilisez cet effet sur un objet, vous pouvez
rendre la fausse magie apparente à toute créature qui manipule l'objet.

***Masque.*** Vous modifiez la façon dont la cible apparaît aux sorts et
effets magiques qui détectent les types de créatures, comme le Sens
divin d'un paladin ou le déclenchement d'un sort de
*symbole*. Vous choisissez un type de créature et les autres
sorts et effets magiques traitent la cible comme si elle était une
créature de ce type ou de cet alignement.



#### Projection astrale

*Nécromancie de 9ème niveau*

**Classes :** Clerc,
Magicien

**Durée du moulage :** 1 heure

**Portée :** 10 pieds

**Composantes :** V, S, M (pour chaque créature que vous affectez avec
ce sort, vous devez fournir une jacinthe d'une valeur d'au moins 1 000
gp et une barre d'argent sculptée d'une valeur d'au moins 100 gp, le
tout consommé par le sort).

**Durée :** Spécial

Vous et jusqu'à huit créatures consentantes à portée projetez vos corps
astraux dans le plan astral (le sort échoue et l'incantation est perdue
si vous êtes déjà sur ce plan). Le corps matériel que vous laissez
derrière vous est inconscient et en état d'animation suspendue ; il
n'a pas besoin de nourriture ni d'air et ne vieillit pas.

Votre corps astral ressemble en tous points à votre forme mortelle,
reproduisant vos statistiques de jeu et vos possessions. La principale
différence est l'ajout d'une corde argentée qui part d'entre vos
omoplates et traîne derrière vous, devenant invisible au bout d'un
mètre. Cette corde est votre lien avec votre corps matériel. Tant que la
corde reste intacte, vous pouvez retrouver votre chemin. Si le cordon
est coupé - ce qui ne peut se produire que lorsqu'un effet le prévoit
spécifiquement - votre âme et votre corps se séparent, vous tuant
instantanément.

Votre forme astrale peut voyager librement dans le plan astral et peut y
traverser des portails menant à tout autre plan. Si vous entrez dans un
nouveau plan ou retournez sur le plan où vous étiez au moment de lancer
ce sort, votre corps et vos biens sont transportés le long de la corde
d'argent, ce qui vous permet de réintégrer votre corps lorsque vous
entrez dans le nouveau plan. Votre forme astrale est une incarnation
distincte. Les dommages ou autres effets qui s'y appliquent n'ont
aucun effet sur votre corps physique, et ne persistent pas lorsque vous
y retournez.

Le sort prend fin pour vous et vos compagnons lorsque vous utilisez
votre action pour l'annuler. Lorsque le sort prend fin, la créature
affectée retrouve son corps physique, et elle s'éveille.

Le sort peut également prendre fin prématurément pour vous ou l'un de
vos compagnons. Un sort de *dissipation de la magie*
réussi contre un corps astral ou physique met fin au sort pour cette
créature. Si le corps original d'une créature ou sa forme astrale tombe
à 0 points de vie, le sort prend fin pour cette créature. Si le sort
prend fin et que le cordon d'argent est intact, le cordon ramène la
forme astrale de la créature dans son corps, mettant fin à son état
d'animation suspendue.

Si vous êtes ramené dans votre corps prématurément, vos compagnons
restent dans leur forme astrale et doivent trouver leur propre chemin
pour retourner dans leur corps, généralement en tombant à 0 points de
vie.



#### Augure

*Divination de 2ème niveau (rituel)*

**Classes :** Clerc

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V, S, M (bâtons, os ou jetons similaires spécialement
marqués d'une valeur d'au moins 25 gp)

**Durée :** Instantané

En lançant des bâtons incrustés de pierres précieuses, en faisant rouler
des os de dragon, en disposant des cartes ornées ou en utilisant un
autre outil de divination, vous recevez un présage d'une entité d'un
autre monde concernant les résultats d'une action spécifique que vous
prévoyez d'entreprendre dans les 30 prochaines minutes. Le MJ choisit
parmi les présages possibles suivants :

-   *Weal,* pour de bons résultats
-   *Malheur,* pour les mauvais résultats
-   Le*bonheur et le malheur,* pour les bons et les mauvais résultats.
-   *Rien,* pour des résultats qui ne sont pas spécialement bons ou
    mauvais.

Le sort ne prend pas en compte les circonstances qui pourraient changer
le résultat, comme le lancement de sorts supplémentaires ou la perte ou
le gain d'un compagnon.

Si vous lancez le sort deux fois ou plus avant la fin de votre prochain
repos long, il y a 25 % de chances cumulatives pour chaque lancement
après le premier d'obtenir une lecture aléatoire. Le MJ effectue ce jet
en secret.



#### Éveil

*transmutation de niveau 5*

**Classes :** Barde

**Temps de coulée :** 8 heures

**Portée :** Toucher

**Composantes :** V, S, M (une agate d'au moins 1 000 gp, que le sort
consomme).

**Durée :** Instantané

Après avoir passé le temps d'incantation à tracer des voies magiques
dans une pierre précieuse, vous touchez une bête ou une plante Très
grande ou plus petite. La cible doit avoir soit un score d'Intelligence
nul, soit une Intelligence de 3 ou moins. La cible gagne une
Intelligence de 10. La cible gagne également la capacité de parler une
langue que vous connaissez. Si la cible est une plante, elle gagne la
capacité de bouger ses membres, ses racines, ses lianes, ses plantes
grimpantes, etc. et elle acquiert des sens similaires à ceux d'un
humain. Votre MJ choisit des statistiques appropriées pour la plante
éveillée, comme celles de l'arbuste éveillé ou de l'arbre éveillé.

La bête ou la plante éveillée est charmée par vous pendant 30 jours ou
jusqu'à ce que vous ou vos compagnons lui fassiez du mal. Lorsque
l'état de charme prend fin, la créature éveillée choisit de rester
amicale avec vous, en fonction de la façon dont vous l'avez traitée
lorsqu'elle était charmée.



#### Fléau

*Enchantement de 1er niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une goutte de sang)

**Durée :** Concentration, jusqu'à 1 minute

Jusqu'à trois créatures de votre choix que vous pouvez voir à portée
doivent effectuer des jets de sauvegarde de Charisme. Chaque fois
qu'une cible qui échoue à ce jet de sauvegarde effectue un jet
d'attaque ou un jet de sauvegarde avant la fin du sort, elle doit
lancer un d4 et soustraire le nombre obtenu du jet d'attaque ou du jet
de sauvegarde.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er.



#### Bannissement

*4th-level abjuration*

**Classes :** Clerc,
Sorcier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un objet répugnant pour la cible)

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez d'envoyer une créature que vous pouvez voir à portée de
main dans un autre plan d'existence. La cible doit réussir un jet de
sauvegarde de Charisme ou être bannie.

Si la cible est native du plan d'existence sur lequel vous vous
trouvez, vous la bannissez vers un Demi-plan inoffensif. Pendant son
séjour, la cible est frappée d'incapacité.

La cible y reste jusqu'à la fin du sort, moment où elle réapparaît dans
l'espace qu'elle a quitté ou dans l'espace inoccupé le plus proche si
cet espace est occupé.

Si la cible est originaire d'un autre plan d'existence que celui où
vous vous trouvez, elle est bannie avec un faible bruit sec, et retourne
sur son plan d'origine. Si le sort prend fin avant qu'une minute ne se
soit écoulée, la cible réapparaît dans l'espace qu'elle a quitté ou
dans l'espace inoccupé le plus proche si cet espace est occupé. Sinon,
la cible ne revient pas.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 5e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
4e.



#### Peau d'écorce

*transmutation de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une poignée d'écorce de chêne)

**Durée :** Concentration, jusqu'à 1 heure

Vous touchez une créature consentante. Jusqu'à la fin du sort, la peau
de la cible a un aspect rugueux, semblable à de l'écorce, et la CA de
la cible ne peut être inférieure à 16, quel que soit le type d'armure
qu'elle porte.



#### Lueur d'espoir

*3rd-level abjuration*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Ce sort confère espoir et vitalité. Choisissez un nombre quelconque de
créatures à portée. Pendant la durée du sort, chaque cible a l'avantage
aux jets de sauvegarde contre la Sagesse et aux jets de sauvegarde
contre la mort, et récupère le maximum de points de vie possible grâce
aux soins.



#### Invoquer la malédiction

*Nécromancie de 3e niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous touchez une créature, et cette dernière doit réussir un jet de
sauvegarde de Sagesse ou devenir maudite pour la durée du sort. Lorsque
vous lancez ce sort, choisissez la nature de la malédiction parmi les
options suivantes :

-   Choisissez une valeur caractéristique. Tant qu'elle est maudite, la
    cible a un désavantage sur les tests d'aptitude et les jets de
    sauvegarde effectués avec ce score d'aptitude.
-   Tant qu'elle est maudite, la cible a un désavantage sur les jets
    d'attaque contre vous.
-   Tant qu'elle est maudite, la cible doit effectuer un jet de
    sauvegarde de Sagesse au début de chacun de ses tours. Si elle
    échoue, elle gaspille son action de ce tour à ne rien faire.
-   Tant que la cible est maudite, vos attaques et sorts lui infligent
    1d8 points de dégâts nécrotiques supplémentaires.

Un sort de *délivrance des malédictions* met fin à cet
effet. Au choix du MJ, vous pouvez choisir un autre effet de
malédiction, mais il ne doit pas être plus puissant que ceux décrits
ci-dessus. Le MJ a le dernier mot sur l'effet d'une telle malédiction.

***Aux niveaux supérieurs.*** Si vous lancez ce sort en utilisant un
emplacement de sort de 4e niveau ou plus, la durée est la concentration,
jusqu'à 10 minutes. Si vous utilisez un emplacement de sort de 5e
niveau ou plus, la durée est de 8 heures. Si vous utilisez un
emplacement de sort de 7e niveau ou plus, la durée est de 24 heures. Si
vous utilisez un emplacement de sort de 9e niveau, le sort dure jusqu'à
ce qu'il soit dissipé. L'utilisation d'un emplacement de sort de 5e
niveau ou plus confère une durée qui ne nécessite pas de concentration.



#### Tentacules noirs

*conjuration de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S, M (un morceau de tentacule d'une pieuvre géante
ou d'un calmar géant).

**Durée :** Concentration, jusqu'à 1 minute

Des tentacules d'ébène se tortillant remplissent un carré de 6 mètres
de côté sur le sol que vous pouvez voir à portée. Pour la durée, ces
tentacules transforment le sol de la zone en terrain difficile.

Lorsqu'une créature pénètre dans la zone affectée pour la première fois
lors d'un tour ou commence son tour à cet endroit, elle doit réussir un
jet de sauvegarde de Dextérité ou subir 3d6 points de dégâts de
matraquage et être entravée par les tentacules jusqu'à la fin du sort.
Une créature qui commence son tour dans la zone et qui est déjà entravée
par les tentacules subit 3d6 points de dégâts de matraquage.

Une créature entravée par les tentacules peut utiliser son action pour
faire un test de Force ou de Dextérité (au choix) contre le DC de
sauvegarde de votre sort. En cas de succès, elle se libère.



#### Barrière de lames

*Evocation de 6ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez un mur vertical de lames tourbillonnantes et tranchantes
comme des rasoirs, faites d'énergie magique. Le mur apparaît à portée
et dure toute la durée du sort. Vous pouvez créer un mur droit jusqu'à
100 pieds de long, 20 pieds de haut, et 5 pieds d'épaisseur, ou un mur
annelé jusqu'à 60 pieds de diamètre, 20 pieds de haut, et 5 pieds
d'épaisseur. Le mur offre un abri de trois quarts aux créatures qui se
trouvent derrière lui, et son espace est un terrain difficile.

Lorsqu'une créature entre dans la zone du mur pour la première fois
lors d'un tour ou commence son tour à cet endroit, elle doit effectuer
un jet de sauvegarde de Dextérité. En cas d'échec, la créature subit
6d10 points de dégâts tranchants. En cas de sauvegarde réussie, la
créature subit la moitié des dégâts.



#### Bénédiction

*Enchantement de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une aspersion d'eau bénite)

**Durée :** Concentration, jusqu'à 1 minute

Vous bénissez jusqu'à trois créatures de votre choix à portée. Chaque
fois qu'une cible effectue un jet d'attaque ou un jet de sauvegarde
avant la fin du sort, elle peut lancer un d4 et ajouter le nombre obtenu
au jet d'attaque ou au jet de sauvegarde.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er.



#### Flétrissement

*Nécromancie de 4ème niveau*

**Classes :** Druide,
Invocateur

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Instantané

L'énergie nécromantique se déverse sur une créature de votre choix que
vous pouvez voir à portée, drainant son humidité et sa vitalité. La
cible doit effectuer un jet de sauvegarde de Constitution. Elle subit
8d8 points de dégâts nécrotiques en cas d'échec, ou la moitié en cas de
réussite. Ce sort n'a aucun effet sur les morts-vivants ou les
constructions.

Si vous ciblez une créature végétale ou une plante magique, celle-ci
effectue le jet de sauvegarde avec un désavantage, et le sort lui
inflige le maximum de dégâts.

Si vous ciblez une plante non magique qui n'est pas une créature, comme
un arbre ou un arbuste, elle ne fait pas de jet de sauvegarde ; elle se
fane et meurt tout simplement.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 5e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 4e.



#### Cécité/surdité

*Nécromancie de 2ème niveau*

**Classes :** Barde,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V

**Durée :** 1 minute

Vous pouvez aveugler ou assourdir un ennemi. Choisissez une créature que
vous pouvez voir à portée pour effectuer un jet de sauvegarde de
Constitution. En cas d'échec, la cible est soit aveuglée, soit
assourdie (au choix) pour la durée du jet. À la fin de chacun de ses
tours, la cible peut effectuer un jet de sauvegarde de Constitution. En
cas de réussite, le sort prend fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
2e.



#### Clignotement

*3rd-level transmutation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** 1 minute

Lancez un d20 à la fin de chacun de vos tours pendant la durée du sort.
Sur un jet de 11 ou plus, vous disparaissez de votre plan d'existence
actuel et apparaissez dans le plan éthéré (le sort échoue et
l'incantation est gaspillée si vous étiez déjà sur ce plan). Au début
de votre prochain tour, et à la fin du sort si vous êtes sur le plan
éthéré, vous retournez dans un espace inoccupé de votre choix que vous
pouvez voir dans un rayon de 3 mètres de l'espace d'où vous avez
disparu. Si aucun espace inoccupé n'est disponible dans ce rayon, vous
apparaissez dans l'espace inoccupé le plus proche (choisi au hasard si
plusieurs espaces sont également proches). Vous pouvez annuler ce sort
en tant qu'action.

Lorsque vous êtes sur le plan éthéré, vous pouvez voir et entendre le
plan d'où vous venez, qui est exprimé en nuances de gris, et vous ne
pouvez rien voir à plus de 60 pieds de distance. Vous ne pouvez affecter
et être affecté par d'autres créatures que sur le plan éthéré. Les
créatures qui n'y sont pas ne peuvent pas vous percevoir ou interagir
avec vous, sauf si elles en ont la capacité.



#### Flou

*illusion de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V

**Durée :** Concentration, jusqu'à 1 minute

Votre corps devient flou, changeant et vacillant pour tous ceux qui
peuvent vous voir. Pendant toute la durée de l'effet, toute créature a
un désavantage aux jets d'attaque contre vous. Un attaquant est
immunisé contre cet effet s'il ne dépend pas de la vue, comme dans le
cas de la vision aveugle, ou s'il peut voir à travers les illusions,
comme dans le cas de la vision véritable.



#### Marque de punition

*Evocation de 2ème niveau*

**Classes :** Paladin

**Temps d'incantation :** 1 action bonus

**Portée :** Self

**Composantes :** V

**Durée :** Concentration, jusqu'à 1 minute

La prochaine fois que vous frappez une créature avec une attaque d'arme
avant la fin de ce sort, l'arme brille d'un éclat astral au moment où
vous frappez. L'attaque inflige 2d6 points de dégâts radiants
supplémentaires à la cible, qui devient visible si elle est invisible,
et la cible diffuse une lumière faible dans un rayon de 1,5 m et ne peut
pas devenir invisible avant la fin du sort.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts supplémentaires
augmentent de 1d6 pour chaque niveau d'emplacement supérieur au 2e.



#### Mains brûlantes

*Evocation de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (cône de 15 pieds)

**Composantes :** V, S

**Durée :** Instantané

Lorsque vous tenez vos mains avec les pouces qui se touchent et les
doigts écartés, une fine feuille de flammes jaillit du bout de vos
doigts tendus. Chaque créature dans un cône de 15 pieds doit effectuer
un jet de sauvegarde de Dextérité. Une créature subit 3d6 points de
dégâts de feu en cas d'échec, ou la moitié en cas de réussite.

Le feu enflamme tous les objets inflammables qui se trouvent dans la
zone et qui ne sont pas portés ou transportés.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les dégâts augmentent de 1d6
pour chaque niveau d'emplacement supérieur au 1er.



#### Appel de la foudre

*conjuration de 3ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Un nuage d'orage apparaît sous la forme d'un cylindre de 10 pieds de
haut et de 60 pieds de rayon, centré sur un point que vous pouvez voir à
100 pieds directement au-dessus de vous. Le sort échoue si vous ne
pouvez pas voir un point dans l'air où le nuage d'orage pourrait
apparaître (par exemple, si vous êtes dans une pièce qui ne peut pas
accueillir le nuage).

Lorsque vous lancez le sort, choisissez un point que vous pouvez voir à
portée. Un éclair descend du nuage jusqu'à ce point. Chaque créature
située à moins de 1,5 m de ce point doit effectuer un jet de sauvegarde
de Dextérité. La créature subit 3d10 points de dégâts de foudre en cas
d'échec, ou la moitié en cas de réussite. À chacun de vos tours
jusqu'à la fin du sort, vous pouvez utiliser votre action pour faire
tomber la foudre de cette façon à nouveau, en visant le même point ou un
autre.

Si vous êtes à l'extérieur dans des conditions orageuses lorsque vous
lancez ce sort, celui-ci vous permet de contrôler la tempête existante
au lieu d'en créer une nouvelle. Dans de telles conditions, les dégâts
du sort augmentent de 1d10.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts augmentent de
1d10 pour chaque niveau d'emplacement supérieur au 3e.



#### Apaisement des émotions

*Enchantement de 2ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez de supprimer les émotions fortes dans un groupe de
personnes. Chaque humanoïde dans une sphère de 20 pieds de rayon centrée
sur un point que vous choisissez dans la portée doit effectuer un jet de
sauvegarde de Charisme ; une créature peut choisir d'échouer ce jet de
sauvegarde si elle le souhaite. Si une créature échoue à son jet de
sauvegarde, choisissez l'un des deux effets suivants.

Vous pouvez supprimer tout effet provoquant le charme ou l'effroi
d'une cible. Lorsque ce sort prend fin, tout effet supprimé reprend, à
condition que sa durée n'ait pas expiré entre-temps.

Vous pouvez également rendre une cible indifférente aux créatures de
votre choix envers lesquelles elle est hostile. Cette indifférence prend
fin si la cible est attaquée ou blessée par un sort ou si elle est
témoin qu'un de ses amis est blessé. Lorsque le sort prend fin, la
créature redevient hostile, sauf si le MJ en décide autrement.



#### Chaîne d'éclairs

*Evocation de 6ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (un morceau de fourrure, un morceau d'ambre,
de verre ou une tige de cristal, et trois épingles en argent).

**Durée :** Instantané

Vous créez un éclair qui se dirige vers une cible de votre choix que
vous pouvez voir à portée. Trois éclairs partent ensuite de cette cible
et atteignent jusqu'à trois autres cibles, chacune d'entre elles
devant se trouver dans un rayon de 30 pieds de la première cible. Une
cible peut être une créature ou un objet et ne peut être visée que par
un seul des éclairs.

La cible doit effectuer un jet de sauvegarde de Dextérité. La cible
subit 10d8 points de dégâts de foudre en cas d'échec, ou la moitié en
cas de réussite.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau ou plus, un boulon supplémentaire
saute de la première cible à une autre cible pour chaque niveau
d'emplacement supérieur au 6e.



#### Charme-personne

*Enchantement de 1er niveau*

**Classes :** Barde,
Sorcier,
Magicien\...

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** 1 heure

Vous tentez de charmer un humanoïde que vous pouvez voir à portée. Il
doit effectuer un jet de sauvegarde de Sagesse, et le fait avec avantage
si vous ou vos compagnons le combattez. Si elle échoue au jet de
sauvegarde, elle est charmée par vous jusqu'à la fin du sort ou
jusqu'à ce que vous ou vos compagnons lui fassiez du mal. La créature
charmée vous considère comme une connaissance amicale. Lorsque le sort
prend fin, la créature sait qu'elle a été charmée par vous.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er. Les créatures doivent se trouver à moins de 10 mètres l'une de
l'autre lorsque vous les ciblez.



#### Contact glacial

*Tour de magie de nécromancie.*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** 1 round

Vous créez une main fantomatique et squelettique dans l'espace d'une
créature à portée. Effectuez une attaque de sort à distance contre la
créature pour l'assaillir avec le froid de la tombe. En cas de succès,
la cible subit 1d8 points de dégâts nécrotiques et ne peut pas regagner
de points de vie avant le début de votre prochain tour. Jusque-là, la
main s'accroche à la cible.

Si vous touchez une cible morte-vivante, elle a également un désavantage
aux jets d'attaque contre vous jusqu'à la fin de votre prochain tour.

Les dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le 5e
niveau (2d8), le 11e niveau (3d8) et le 17e niveau (4d8).



#### Cercle de mort

*Nécromancie de 6ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (la poudre d'une perle noire écrasée valant
au moins 500 gp)

**Durée :** Instantané

Une sphère d'énergie négative se propage dans un rayon de 60 pieds à
partir d'un point situé à portée. Chaque créature dans cette zone doit
effectuer un jet de sauvegarde de Constitution. La cible subit 8d6
points de dégâts nécrotiques en cas d'échec, ou la moitié en cas de
réussite.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 7e niveau ou plus, les dégâts augmentent de 2d6
pour chaque niveau d'emplacement supérieur au 6e.



#### Clairvoyance

*Divination de 3ème niveau*

**Classes :** Barde,
Sorcier

**Temps de coulée :** 10 minutes

**Portée :** 1 mile

**Composantes :** V, S, M (un centre d'intérêt d'une valeur d'au
moins 100 gp, une corne en bijou pour l'ouïe ou un œil en verre pour la
vue).

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez un capteur invisible à portée dans un endroit qui vous est
familier (un lieu que vous avez déjà visité ou vu) ou dans un endroit
évident qui ne vous est pas familier (comme derrière une porte, au coin
d'une rue ou dans un bosquet d'arbres). Le capteur reste en place
pendant toute la durée de l'opération, et il ne peut pas être attaqué
ou subir d'autres interactions.

Lorsque vous lancez le sort, vous choisissez la vue ou l'ouïe. Vous
pouvez utiliser le sens choisi à travers le capteur comme si vous étiez
dans son espace. Comme action, vous pouvez passer de la vue à l'ouïe.

Une créature qui peut voir le capteur (comme une créature bénéficiant de
*voir l'invisibilité* voit
un orbe lumineux et intangible de la taille de votre poing.



#### Clone

*Nécromancie de 8ème niveau*

**Classes :** Magicien

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (un diamant d'une valeur d'au moins 1 000 gp
et au moins 1 pouce cube de chair de la créature à cloner, que le sort
consomme, ainsi qu'un récipient d'une valeur d'au moins 2 000 gp,
doté d'un couvercle scellable et suffisamment grand pour contenir une
créature de taille moyenne, tel qu'une énorme urne, un cercueil, un
kyste rempli de boue dans le sol ou un récipient en cristal rempli
d'eau salée).

**Durée :** Instantané

Ce sort permet de faire pousser un double inerte d'une créature vivante
pour la protéger de la mort. Ce clone se forme à l'intérieur d'un
récipient scellé et atteint sa taille et sa maturité au bout de 120
jours ; vous pouvez également choisir que le clone soit une version plus
jeune de la même créature. Il reste inerte et perdure indéfiniment, tant
que son récipient n'est pas perturbé.

À tout moment après la maturation du clone, si la créature originale
meurt, son âme est transférée au clone, à condition que l'âme soit
libre et désireuse de revenir.

Le clone est physiquement identique à l'original et possède la même
personnalité, les mêmes souvenirs et capacités, mais aucun des
équipements de l'original. Les restes physiques de la créature
originale, s'ils existent encore, deviennent inertes et ne peuvent plus
être ramenés à la vie par la suite, puisque l'âme de la créature est
ailleurs.



#### Nuage mortelle

*conjuration de 5e niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez une sphère de 20 pieds de rayon de brouillard jaune-vert
empoisonné centré sur un point de votre choix à portée. Le brouillard se
répand dans les coins. Il dure toute la durée du sort ou jusqu'à ce
qu'un vent fort le disperse, mettant fin au sort. Sa zone est fortement
obscurcie.

Lorsqu'une créature entre dans la zone du sort pour la première fois
lors d'un tour ou commence son tour à cet endroit, cette créature doit
effectuer un jet de sauvegarde de Constitution. La créature subit 5d8
points de dégâts de poison en cas d'échec, ou la moitié en cas de
réussite. Les créatures sont affectées même si elles retiennent leur
souffle ou n'ont pas besoin de respirer.

Le brouillard s'éloigne de 10 pieds de vous au début de chacun de vos
tours, en roulant à la surface du sol. Les vapeurs, plus lourdes que
l'air, descendent jusqu'au niveau le plus bas du terrain, voire se
déversent dans les ouvertures.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 5e.



#### Couleurs dansantes

*illusion de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (cône de 15 pieds)

**Composantes :** V, S, M (une pincée de poudre ou de sable coloré en
rouge, jaune et bleu).

**Durée :** 1 round

Un ensemble éblouissant de lumières colorées et clignotantes jaillit de
votre main. Lancez 6d10 ; le total correspond au nombre de points de vie
des créatures que ce sort peut affecter. Les créatures situées dans un
cône de 15 pieds partant de vous sont affectées dans l'ordre croissant
de leurs points de vie actuels (en ignorant les créatures inconscientes
et celles qui ne peuvent pas voir).

En commençant par la créature qui a le moins de points de vie actuels,
chaque créature affectée par ce sort est aveuglée jusqu'à la fin du
sort. Soustrayez les points de vie de chaque créature du total avant de
passer à la créature ayant le nombre de points de vie le plus bas
suivant. Les points de vie d'une créature doivent être égaux ou
inférieurs au total restant pour que cette créature soit affectée.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, lancez 2d10 supplémentaires
pour chaque niveau d'emplacement supérieur au 1er.



#### Injonction

*Enchantement de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** 1 round

Vous énoncez un ordre d'un mot à une créature que vous pouvez voir à
portée. La cible doit réussir un jet de sauvegarde de Sagesse ou suivre
l'ordre à son prochain tour. Le sort n'a aucun effet si la cible est
morte-vivante, si elle ne comprend pas votre langue ou si votre ordre
lui est directement nuisible.

Voici quelques injonctions typiques et leurs effets. Vous pouvez donner
une injonction autre que celle décrite ici. Si vous le faites, le MJ
détermine le comportement de la cible. Si la cible ne peut pas suivre
votre commandement, le sort prend fin.

***Approche.*** La cible se déplace vers vous par le chemin le plus
court et le plus direct, terminant son tour si elle se trouve à moins de
1,5 m de vous.

***Lâcher.*** La cible laisse tomber ce qu'elle tient et termine son
tour.

***Fuir.*** La cible passe son tour à s'éloigner de vous par le moyen
le plus rapide disponible.

***Ramper.*** La cible tombe à terre puis termine son tour.

***Halte.*** La cible ne bouge pas et ne fait aucune action. Une
créature volante reste en l'air, à condition qu'elle soit capable de
le faire. Si elle doit se déplacer pour rester en l'air, elle vole sur
la distance minimale nécessaire pour rester en l'air.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez affecter une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er. Les créatures doivent se trouver à moins de 10 mètres l'une de
l'autre lorsque vous les ciblez.



#### Communion

*Divination de 5ème niveau (rituel)*

**Classes :** Clerc

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V, S, M (encens et une fiole d'eau bénite ou impie)

**Durée :** 1 minute

Vous contactez votre divinité ou un mandataire divin et posez jusqu'à
trois questions auxquelles vous pouvez répondre par oui ou par non. Vous
devez poser vos questions avant que le sort ne prenne fin. Vous recevez
une réponse correcte pour chaque question.

Les êtres divins ne sont pas nécessairement omniscients, donc vous
pouvez recevoir \"pas clair\" comme réponse si une question concerne des
informations qui sont au-delà de la connaissance de la divinité. Dans le
cas où une réponse en un mot pourrait être trompeuse ou contraire aux
intérêts de la divinité, le MJ peut proposer une courte phrase à la
place.

Si vous lancez le sort deux fois ou plus avant la fin de votre prochain
repos long, vous avez 25 % de chances cumulatives de ne pas obtenir de
réponse pour chaque lancer après le premier. Le MJ effectue ce jet en
secret.



#### Communion avec la nature

*Divination de 5ème niveau (rituel)*

**Classes :** Druide

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V, S

**Durée :** Instantané

Vous faites brièvement corps avec la nature et gagnez des connaissances
sur le territoire environnant. En plein air, le sort vous permet de
connaître les terres situées dans un rayon de 3 miles autour de vous.
Dans les grottes et autres environnements souterrains naturels, le rayon
est limité à 300 pieds. Le sort ne fonctionne pas là où la nature a été
remplacée par des constructions, comme dans les donjons et les villes.

Vous acquérez instantanément des connaissances sur un maximum de trois
faits de votre choix concernant l'un des sujets suivants, en rapport
avec la région :

-   le terrain et les plans d'eau
-   plantes, minéraux, animaux ou peuples prévalents
-   puissants célestes, fées, fiélons, élémentaires ou morts-vivants.
-   l'influence d'autres plans d'existence
-   bâtiments

Par exemple, vous pouvez déterminer l'emplacement des morts-vivants
puissants dans la région, l'emplacement des principales sources d'eau
potable et l'emplacement des villes voisines.



#### Compréhension des langues

*Divination de 1er niveau (rituel)*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (une pincée de suie et de sel)

**Durée :** 1 heure

Pendant cette période, vous comprenez le sens littéral de toute langue
parlée que vous entendez. Vous comprenez également toute langue écrite
que vous voyez, mais vous devez toucher la surface sur laquelle les mots
sont écrits. Il faut environ 1 minute pour lire une page de texte.

Ce sort ne permet pas de décoder les messages secrets contenus dans un
texte ou un glyphe, tel qu'un sigil arcanique, qui ne fait pas partie
d'une langue écrite.



#### Compulsion

*Enchantement de 4ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Les créatures de votre choix que vous pouvez voir à portée et qui
peuvent vous entendre doivent effectuer un jet de sauvegarde de Sagesse.
Une cible réussit automatiquement ce jet de sauvegarde si elle ne peut
pas être charmée. En cas d'échec, la cible est affectée par ce sort.
Jusqu'à ce que le sort prenne fin, vous pouvez utiliser une action
bonus à chacun de vos tours pour désigner une direction qui est
horizontale pour vous. Chaque cible affectée doit utiliser autant de son
mouvement que possible pour se déplacer dans cette direction à son
prochain tour. Elle peut effectuer son action avant de se déplacer.
Après s'être déplacée de cette manière, elle peut effectuer une autre
sauvegarde de Sagesse pour tenter de mettre fin à l'effet.

Une cible n'est pas obligée de se déplacer dans un danger mortel
évident, comme un feu ou une fosse, mais elle provoquera des attaques
d'opportunité pour se déplacer dans la direction désignée.



#### Cône de froid

*Evocation de 5ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (cône de 60 pieds)

**Composantes :** V, S, M (un petit cristal ou un cône de verre)

**Durée :** Instantané

Un souffle d'air froid s'échappe de vos mains. Chaque créature dans un
cône de 60 pieds doit effectuer un jet de sauvegarde de Constitution. En
cas d'échec, la créature subit 8d8 points de dégâts dus au froid, ou la
moitié en cas de réussite.

Une créature tuée par ce sort devient une statue gelée jusqu'à ce
qu'elle dégèle.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 5e.



#### Confusion

*Enchantement de 4ème niveau*

**Classes :** Barde,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S, M (trois coquilles de noix)

**Durée :** Concentration, jusqu'à 1 minute

Ce sort agresse et tord l'esprit des créatures, engendrant des
illusions et provoquant des actions incontrôlées. Chaque créature dans
une sphère de 3 mètres de rayon centrée sur un point que vous choisissez
dans la portée doit réussir un jet de sauvegarde de Sagesse quand vous
lancez ce sort ou être affectée par lui.

Une cible affectée ne peut pas avoir de réactions et doit lancer un d10
au début de chacun de ses tours pour déterminer son comportement pour ce
tour.

  ------------------------------------------------------------------------
   d10   Comportement
  ------ -----------------------------------------------------------------
    1    La créature utilise tout son mouvement pour se déplacer dans une
         direction aléatoire. Pour déterminer la direction, lancez un d8
         et attribuez une direction à chaque face du dé. La créature ne
         fait pas d'action pendant ce tour.

   2-6   La créature ne se déplace pas et ne fait pas d'action pendant ce
         tour.

   7-8   La créature utilise son action pour effectuer une attaque de
         mêlée contre une créature déterminée aléatoirement à sa portée.
         S'il n'y a pas de créature à sa portée, la créature ne fait
         rien à ce tour.

   9-10  La créature peut agir et se déplacer normalement.
  ------------------------------------------------------------------------

À la fin de chacun de ses tours, une cible affectée peut effectuer un
jet de sauvegarde de Sagesse. Si elle réussit, cet effet prend fin pour
cette cible.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 5e niveau ou plus, le rayon de la sphère augmente
de 1,5 m pour chaque niveau d'emplacement supérieur au 4e.



#### Invocation d'animaux

*3rd-level conjuration*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous invoquez des esprits féeriques qui prennent la forme de bêtes et
apparaissent dans les espaces inoccupés que vous pouvez voir à portée.
Choisissez l'une des options suivantes pour ce qui apparaît :

-   Une bête de niveau de défi 2 ou inférieur.
-   Deux bêtes de niveau de défi 1 ou inférieur.
-   Quatre bêtes de défi de valeur 1/2 ou inférieure.
-   Huit bêtes d'un niveau de difficulté inférieur ou égal à 1/4.

Chaque bête est également considérée comme fée, et elle disparaît
lorsqu'elle tombe à 0 points de vie ou lorsque le sort prend fin.

Les créatures invoquées sont amicales envers vous et vos compagnons.
Faites un jet d'initiative pour les créatures invoquées en tant que
groupe, qui a ses propres tours. Elles obéissent à tous les ordres
verbaux que vous leur donnez (aucune action requise de votre part). Si
vous ne leur donnez aucun ordre, elles se défendent contre les créatures
hostiles, mais ne font aucune autre action.

Le MJ dispose des statistiques des créatures.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
certains emplacements de sorts de niveau supérieur, vous choisissez
l'une des options d'invocation ci-dessus, et davantage de créatures
apparaissent : deux fois plus avec un emplacement de 5e niveau, trois
fois plus avec un emplacement de 7e niveau et quatre fois plus avec un
emplacement de 9e niveau.



#### Invocation de céleste

*conjuration de 7ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 minute

**Portée :** 90 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous invoquez une céleste de niveau de défi 4 ou moins, qui apparaît
dans un espace inoccupé que vous pouvez voir à portée. Le céleste
disparaît lorsqu'il tombe à 0 points de vie ou lorsque le sort prend
fin.

Le céleste est amical envers vous et vos compagnons pour la durée.
Faites un jet d'initiative pour le céleste, qui a ses propres tours. Il
obéit à toutes les injonctions verbales que vous lui donnez (aucune
action n'est requise de votre part), pour autant qu'elles ne violent
pas son alignement. Si vous ne lui donnez aucun ordre, la gardienne se
défend contre les créatures hostiles, mais ne fait rien d'autre.

Le MJ a les statistiques du céleste.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 9e niveau, vous invoquez une céleste de niveau
de défi 5 ou moins.



#### Invocation d'élémentaire

*conjuration de 5e niveau*

**Classes :** Druide

**Temps d'incantation :** 1 minute

**Portée :** 90 pieds

**Composantes :** V, S, M (brûler de l'encens pour l'air, de l'argile
molle pour la terre, du soufre et du phosphore pour le feu, ou de l'eau
et du sable pour l'eau).

**Durée :** Concentration, jusqu'à 1 heure

Vous appelez un serviteur élémentaire. Choisissez une zone d'air, de
terre, de feu ou d'eau qui remplit un cube de 3 mètres de côté à
portée. Un élémentaire de niveau de défi 5 ou moins approprié à la zone
que vous avez choisie apparaît dans un espace inoccupé dans un rayon de
3 mètres autour d'elle. Par exemple, un élémentaire de feu émerge d'un
feu de joie, et un élémentaire de terre s'élève du sol.

L'élémentaire disparaît lorsqu'il tombe à 0 points de vie ou lorsque
le sort prend fin. L'élémentaire est amical envers vous et vos
compagnons pour la durée du sort. Faites un jet d'initiative pour
l'élémentaire, qui a ses propres tours. Il obéit à toutes les
injonctions verbales que vous lui adressez (aucune action n'est requise
de votre part). Si vous ne donnez aucun ordre à l'élémentaire, il se
défend contre les créatures hostiles mais ne fait rien d'autre.

Si votre concentration est rompue, l'élémentaire ne disparaît pas. Au
contraire, vous perdez le contrôle de l'élémentaire, il devient hostile
envers vous et vos compagnons, et il peut attaquer. Un élémentaire
incontrôlé ne peut pas être renvoyé par vous, et il disparaît 1 heure
après que vous l'ayez invoqué.

Le MJ dispose des statistiques de l'élémentaire.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, la valeur de défi augmente
de 1 pour chaque niveau d'emplacement supérieur au 5e.



#### Invocation de fée

*Conjuration de 6ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 minute

**Portée :** 90 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous invoquez une créature fée de niveau de défi 6 ou inférieur, ou un
esprit fée qui prend la forme d'une bête de niveau de défi 6 ou
inférieur. Elle apparaît dans un espace inoccupé que vous pouvez voir à
portée. La créature féerique disparaît lorsqu'elle tombe à 0 points de
vie ou lorsque le sort prend fin.

La créature fée est amicale envers vous et vos compagnons pour la durée.
Faites un jet d'initiative pour la créature, qui dispose de ses propres
tours. Elle obéit à tous les ordres verbaux que vous lui donnez (aucune
action requise de votre part), tant qu'ils ne violent pas son
alignement. Si vous ne donnez aucun ordre à la créature fée, elle se
défend contre les créatures hostiles mais ne fait rien d'autre.

Si votre concentration est rompue, la créature féerique ne disparaît
pas. Au contraire, vous perdez le contrôle de la créature féerique, elle
devient hostile envers vous et vos compagnons, et elle peut attaquer.
Une créature féerique incontrôlée ne peut pas être renvoyée par vous, et
elle disparaît 1 heure après que vous l'ayez invoquée.

Le MJ dispose des statistiques de la créature fée.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau ou plus, la valeur de défi augmente
de 1 pour chaque niveau d'emplacement supérieur au 6e.



#### Invocation d'élémentaires mineurs

*conjuration de 4ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 minute

**Portée :** 90 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous invoquez des élémentaires qui apparaissent dans des espaces
inoccupés que vous pouvez voir à portée. Vous choisissez l'une des
options suivantes pour ce qui apparaît :

-   Un élémentaire de niveau de difficulté 2 ou inférieur.
-   Deux élémentaires de niveau de difficulté 1 ou inférieur.
-   Quatre élémentaires de niveau de difficulté 1/2 ou inférieur.
-   Huit élémentaires de niveau de défi 1/4 ou inférieur.

Un élémentaire invoqué par ce sort disparaît lorsqu'il tombe à 0 points
de vie ou lorsque le sort prend fin.

Les créatures invoquées sont amicales envers vous et vos compagnons.
Faites un jet d'initiative pour les créatures invoquées en tant que
groupe, qui a ses propres tours. Elles obéissent à tous les ordres
verbaux que vous leur donnez (aucune action requise de votre part). Si
vous ne leur donnez aucun ordre, elles se défendent contre les créatures
hostiles, mais ne font aucune autre action.

Le MJ dispose des statistiques des créatures.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
certains emplacements de sorts de niveau supérieur, vous choisissez
l'une des options d'invocation ci-dessus, et davantage de créatures
apparaissent : deux fois plus avec un emplacement de 6e niveau et trois
fois plus avec un emplacement de 8e niveau.



#### Invocation d'êtres sylvestres

*conjuration de 4ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (une baie de houx par créature invoquée)

**Durée :** Concentration, jusqu'à 1 heure

Vous invoquez des créatures féeriques qui apparaissent dans des espaces
inoccupés que vous pouvez voir à portée. Choisissez l'une des options
suivantes pour ce qui apparaît :

-   Une créature fée de niveau de défi 2 ou inférieur.
-   Deux créatures fées de niveau de défi 1 ou inférieur.
-   Quatre créatures fées de niveau de défi 1/2 ou inférieur.
-   Huit créatures fées de niveau de défi 1/4 ou inférieur.

Une créature invoquée disparaît lorsqu'elle tombe à 0 points de vie ou
lorsque le sort prend fin.

Les créatures invoquées sont amicales envers vous et vos compagnons.
Faites un jet d'initiative pour les créatures invoquées en tant que
groupe, qui ont leurs propres tours. Elles obéissent à tout ordre verbal
que vous leur donnez (aucune action de votre part n'est requise). Si
vous ne leur donnez aucun ordre, elles se défendent contre les créatures
hostiles, mais ne font aucune autre action.

Le MJ dispose des statistiques des créatures.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
certains emplacements de sorts de niveau supérieur, vous choisissez
l'une des options d'invocation ci-dessus, et davantage de créatures
apparaissent : deux fois plus avec un emplacement de 6e niveau et trois
fois plus avec un emplacement de 8e niveau.



#### Contact avec un autre plan

*Divination de 5ème niveau (rituel)*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V

**Durée :** 1 minute

Vous contactez mentalement un demi-dieu, l'esprit d'un sage décédé
depuis longtemps, ou une autre entité mystérieuse d'un autre plan.
Contacter cette intelligence extraplanaire peut mettre votre esprit à
rude épreuve, voire le briser.

Lorsque vous lancez ce sort, effectuez un jet de sauvegarde
d'Intelligence DC 15. En cas d'échec, vous subissez 6d6 dégâts
psychiques et êtes fou jusqu'à la fin d'un repos long. Pendant que
vous êtes fou, vous ne pouvez pas entreprendre d'actions, ne pouvez pas
comprendre ce que disent les autres créatures, ne pouvez pas lire et ne
parlez qu'en charabia. Un sort de *restauration
supérieure* lancé sur vous met fin à cet effet.

En cas de sauvegarde réussie, vous pouvez poser jusqu'à cinq questions
à l'entité. Vous devez poser vos questions avant que le sort ne prenne
fin. Le MJ répond à chaque question par un mot, comme \" oui \", \" non
\", \" peut-être \", \" jamais \", \" non pertinent \" ou \" pas clair
\" (si l'entité ne connaît pas la réponse à la question). Si une
réponse en un mot peut induire en erreur, le MJ peut proposer une courte
phrase comme réponse.



#### Contagion

*Nécromancie de 5e niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** 7 jours

Votre toucher inflige une maladie. Effectuez une attaque de sort en
mêlée contre une créature à votre portée. En cas de succès, vous
affligez la créature d'une maladie de votre choix parmi celles décrites
ci-dessous.

À la fin de chacun de ses tours, la cible doit effectuer un jet de
sauvegarde de Constitution. Après avoir échoué à trois de ces jets de
sauvegarde, les effets de la maladie durent toute la durée du sort, et
la créature cesse de faire ces jets de sauvegarde. Après avoir réussi
trois de ces jets de sauvegarde, la créature se remet de la maladie, et
le sort prend fin.

Puisque ce sort induit une maladie naturelle chez sa cible, tous les
effets qui suppriment une maladie ou en améliorent les effets
s'appliquent à elle.

***Maladie aveuglante.*** La douleur s'empare de l'esprit de la
créature, et ses yeux deviennent d'un blanc laiteux. La créature a un
désavantage aux tests de Sagesse et aux jets de sauvegarde de Sagesse et
est aveuglée.

***Filth Fever.*** Une fièvre furieuse envahit le corps de la créature.
La créature a un désavantage sur les tests de Force, les jets de
sauvegarde de Force et les jets d'attaque qui utilisent la Force.

***Pourriture de la chair.*** La chair de la créature se décompose. La
créature a un désavantage aux tests de Charisme et une vulnérabilité à
tous les dégâts.

***Feu de l'esprit.*** L'esprit de la créature devient fiévreux. La
créature a un désavantage aux tests d'Intelligence et aux jets de
sauvegarde d'Intelligence, et la créature se comporte comme si elle
était sous les effets du sort *confusion* pendant le
combat.

***Crise d'épilepsie.*** La créature est prise de tremblements. La
créature a un désavantage sur les tests de Dextérité, les jets de
sauvegarde de Dextérité et les jets d'attaque qui utilisent la
Dextérité.

***Slimy Doom.*** La créature se met à saigner de manière incontrôlable.
La créature a un désavantage aux tests de Constitution et aux jets de
sauvegarde de Constitution. De plus, chaque fois que la créature subit
des dégâts, elle est étourdie jusqu'à la fin de son prochain tour.



#### Prévoyance

*Evocation de 6ème niveau*

**Classes :** Magicien

**Temps de coulée :** 10 minutes

**Portée :** Self

**Composantes :** V, S, M (une statuette de vous-même sculptée dans de
l'ivoire et décorée de pierres précieuses d'une valeur d'au moins 1
500 gp).

**Durée :** 10 jours

Choisissez un sort de 5e niveau ou moins que vous pouvez lancer, dont le
temps d'incantation est de 1 action, et qui peut vous cibler. Vous
lancez ce sort - appelé sort contingent - dans le cadre de la
*Prévoyance*, en dépensant des emplacements de sort pour
les deux, mais le sort contingent ne prend pas effet. Au lieu de cela,
il prend effet lorsqu'une certaine circonstance se produit. Vous
décrivez cette circonstance lorsque vous lancez les deux sorts. Par
exemple, un sort de *contingence* avec *Respiration
aquatique* peut stipuler que la *respiration
aquatique* prend effet lorsque vous êtes englouti
dans l'eau ou un liquide similaire.

Le sort de Prévoyance prend effet immédiatement après la première
rencontre de la circonstance, que vous le vouliez ou non, puis la
*Prévoyance* prend fin.

Le sort de contingence ne prend effet que sur vous, même s'il peut
normalement cibler d'autres personnes. Vous ne pouvez utiliser qu'un
seul sort de *prévoyance* à la fois. Si vous lancez à
nouveau ce sort, l'effet d'un autre sort de
*contingence* sur vous prend fin. De plus, la
*Prévoyance* prend fin sur vous si son composant
matériel ne se trouve jamais sur votre personne.



#### Flamme continuelle

*Evocation de 2ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (poussière de rubis d'une valeur de 50 gp,
que le sort consomme)

**Durée :** Jusqu'à ce qu'il soit dissipé

Une flamme, d'une luminosité équivalente à celle d'une torche, jaillit
d'un objet que vous touchez. L'effet ressemble à une flamme ordinaire,
mais elle ne crée aucune chaleur et n'utilise pas d'oxygène. Une
flamme *continuelle* peut être couverte ou cachée
mais pas étouffée ou éteinte.



#### Contrôle de l'eau

*4th-level transmutation*

**Classes :** Clerc,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 300 pieds

**Composantes :** V, S, M (une goutte d'eau et une pincée de poussière)

**Durée :** Concentration, jusqu'à 10 minutes

Jusqu'à ce que le sort prenne fin, vous contrôlez toute eau libre à
l'intérieur d'une zone que vous choisissez qui est un cube jusqu'à
100 pieds de côté. Vous pouvez choisir l'un des effets suivants lorsque
vous lancez ce sort. Comme action à votre tour, vous pouvez répéter le
même effet ou en choisir un autre.

***Inondation.*** Vous faites monter le niveau de toutes les eaux
stagnantes de la zone jusqu'à 6 mètres. Si la zone comprend un rivage,
l'eau de l'inondation se déverse sur la terre ferme.

Si vous choisissez une zone située dans une grande étendue d'eau, vous
créez une vague de 6 mètres de haut qui se déplace d'un côté à l'autre
de la zone avant de s'écraser. Tout véhicule Très grand ou plus petit
se trouvant sur la trajectoire de la vague est emporté avec elle de
l'autre côté. Tout véhicule Très grand ou plus petit frappé par la
vague a 25 % de chances de chavirer.

Le niveau de l'eau reste élevé jusqu'à ce que le sort prenne fin ou
que vous choisissiez un autre effet. Si cet effet a produit une vague,
celle-ci se répète au début de votre prochain tour tant que l'effet
d'inondation dure.

***Une partie de l'eau.*** Vous faites en sorte que l'eau de la zone
se sépare et crée une tranchée. La tranchée s'étend à travers la zone
du sort, et l'eau séparée forme un mur de chaque côté. La tranchée
reste en place jusqu'à ce que le sort prenne fin ou que vous
choisissiez un autre effet. L'eau se remplit ensuite lentement dans la
tranchée au cours du prochain round jusqu'à ce que le niveau d'eau
normal soit rétabli.

***Rediriger le flux.*** Vous faites en sorte que l'eau qui coule dans
la zone se déplace dans la direction que vous choisissez, même si l'eau
doit passer par-dessus des obstacles, escalader des murs ou prendre
d'autres directions improbables. L'eau dans la zone se déplace comme
vous l'orientez, mais dès qu'elle sort de la zone du sort, elle
reprend son cours en fonction des conditions du terrain. L'eau continue
à se déplacer dans la direction que vous avez choisie jusqu'à ce que le
sort prenne fin ou que vous choisissiez un autre effet.

***Tourbillon.*** Cet effet nécessite une étendue d'eau d'au moins 50
pieds carrés et 25 pieds de profondeur. Vous provoquez la formation
d'un tourbillon au centre de la zone. Le tourbillon forme un vortex de
5 pieds de large à la base, jusqu'à 50 pieds de large au sommet et 25
pieds de haut. Toute créature ou objet dans l'eau et dans un rayon de
25 pieds autour du tourbillon est tiré à 10 pieds vers lui. Une créature
peut s'éloigner du vortex à la nage en effectuant un test de Force
(Athlétisme) contre le DC de sauvegarde de votre sort.

Lorsqu'une créature entre dans le vortex pour la première fois lors
d'un tour ou qu'elle y commence son tour, elle doit effectuer un jet
de sauvegarde de Force. En cas d'échec, la créature subit 2d8 points de
dégâts contondants et est prise dans le vortex jusqu'à la fin du sort.
En cas de sauvegarde réussie, la créature subit la moitié des dégâts et
n'est pas prise dans le tourbillon. Une créature prise dans le vortex
peut utiliser son action pour essayer de s'en éloigner à la nage comme
décrit ci-dessus, mais elle a un désavantage sur le test de Force
(Athlétisme) pour le faire.

La première fois qu'un objet entre dans le vortex à chaque tour, il
subit 2d8 points de dégâts de contondance ; ces dégâts se répètent
chaque round où il reste dans le vortex.



#### Contrôle du climat

*Transmutation de 8ème niveau*

**Classes :** Clerc,
Magicien

**Temps de coulée :** 10 minutes

**Portée :** Moi-même (rayon de 8 km)

**Composantes :** V, S, M (encens brûlant et morceaux de terre et de
bois mélangés à l'eau)

**Durée :** Concentration, jusqu'à 8 heures

Vous prenez le contrôle de la météo dans un rayon de 8 km autour de vous
pour la durée du sort. Vous devez être à l'extérieur pour lancer ce
sort. Si vous vous déplacez dans un endroit où vous n'avez pas de
chemin clair vers le ciel, le sort prend fin prématurément.

Lorsque vous lancez ce sort, vous modifiez les conditions
météorologiques actuelles, qui sont déterminées par le MJ en fonction du
climat et de la saison. Vous pouvez modifier les précipitations, la
température et le vent. Il faut 1d4 × 10 minutes pour que les nouvelles
conditions prennent effet. Une fois qu'elles le font, vous pouvez à
nouveau modifier les étâts. Lorsque le sort prend fin, le temps revient
progressivement à la normale.

Lorsque vous changez les étâts, trouvez un étât actuel sur les tableaux
suivants et changez son étape par une, vers le haut ou vers le bas.
Lorsque vous modifiez le vent, vous pouvez changer sa direction.

  -------------------------------------------------
   Scène  État
  ------- -----------------------------------------
     1    Clair

     2    Nuages de lumière

     3    Ciel couvert ou brouillard au sol

     4    Pluie, grêle ou neige

     5    Pluie torrentielle, grêle battante ou
          blizzard.
  -------------------------------------------------

  : Précipitations

  -----------------------
   Scène  État
  ------- ---------------
     1    Chaleur
          insupportable

     2    Hot

     3    Chaud

     4    Cool

     5    Froid

     6    Le froid
          arctique
  -----------------------

  : Température

  ---------------------
   Scène  État
  ------- -------------
     1    Calme

     2    Vent modéré

     3    Vent fort

     4    Gale

     5    Tempête
  ---------------------

  : Vent



#### Contresort

*3rd-level abjuration*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 réaction, que vous prenez lorsque vous
voyez une créature à moins de 60 pieds de vous lancer un sort.

**Portée :** 60 pieds

**Composantes :** S

**Durée :** Instantané

Vous tentez d'interrompre une créature en train de lancer un sort. Si
la créature lance un sort de 3e niveau ou moins, son sort échoue et n'a
aucun effet. Si elle lance un sort de 4e niveau ou plus, effectuez un
test de capacité en utilisant votre caractéristique d'incantation. La
valeur DC est égale à 10 + le niveau du sort. En cas de réussite, le
sort de la créature échoue et n'a aucun effet.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, le sort interrompu n'a
aucun effet si son niveau est inférieur ou égal au niveau de
l'emplacement de sort que vous avez utilisé.



#### Création de nourriture et d'eau

*3rd-level conjuration*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous créez 45 livres de nourriture et 30 gallons d'eau sur le sol ou
dans des récipients à portée, assez pour nourrir jusqu'à quinze
humanoïdes ou cinq destriers pendant 24 heures. La nourriture est fade
mais nourrissante, et se gâte si elle n'est pas consommée après 24
heures. L'eau est propre et ne se détériore pas.



#### Création de mort-vivant

*Nécromancie de 6ème niveau*

**Classes :** Clerc,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** 10 pieds

**Composantes :** V, S, M (un pot d'argile rempli de terre sépulcrale,
un pot d'argile rempli d'eau saumâtre et une pierre d'onyx noir de
150 gp pour chaque cadavre).

**Durée :** Instantané

Vous ne pouvez lancer ce sort que la nuit. Choisissez jusqu'à trois
cadavres d'humanoïdes de taille moyenne ou petite à portée. Chaque
cadavre devient une goule sous votre contrôle. (Le MJ dispose de
statistiques de jeu pour ces créatures).

Comme action bonus à chacun de vos tours, vous pouvez commander
mentalement toute créature que vous avez animée avec ce sort si elle se
trouve à moins de 120 pieds de vous (si vous contrôlez plusieurs
créatures, vous pouvez en commander une ou toutes en même temps, en
donnant le même ordre à chacune). Vous décidez de l'action de la
créature et de l'endroit où elle se déplacera lors de son prochain
tour, ou vous pouvez donner un ordre général, comme celui de garder une
chambre ou un couloir particulier. Si vous ne donnez aucun ordre, la
créature ne se défend que contre les créatures hostiles. Une fois
l'ordre donné, la créature continue à le suivre jusqu'à ce que sa
tâche soit terminée.

La créature est sous votre contrôle pendant 24 heures, après quoi elle
cesse d'obéir aux ordres que vous lui avez donnés. Pour conserver le
contrôle de la créature pendant 24 heures supplémentaires, vous devez
lancer ce sort sur la créature avant la fin de la période de 24 heures
en cours. Cette utilisation du sort réaffirme votre contrôle sur un
maximum de trois créatures que vous avez animées avec ce sort, plutôt
que d'en animer de nouvelles.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de 7e niveau, vous pouvez animer ou réaffirmer le
contrôle sur quatre goules. Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 8e niveau, vous pouvez animer ou réaffirmer le
contrôle sur cinq goules ou deux goules ou Nécrophages. Lorsque vous
lancez ce sort en utilisant un emplacement de 9ème niveau, vous pouvez
animer ou réaffirmer le contrôle sur six goules, trois fantômes ou
wights, ou deux momies.



#### Création ou anéantissement de l'eau

*Trans transmutation de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une goutte d'eau si vous créez de l'eau ou
quelques grains de sable si vous la détruisez).

**Durée :** Instantané

Vous créez ou anéantissez l'eau.

***Créer de l'eau.*** Vous créez jusqu'à 10 gallons d'eau propre à
portée dans un récipient ouvert. Alternativement, l'eau tombe sous
forme de pluie dans un cube de 30 pieds à portée, éteignant les flammes
exposées dans la zone.

***Détruire l'eau.*** Vous détruisez jusqu'à 10 gallons d'eau dans un
récipient ouvert à portée. Alternativement, vous détruisez le brouillard
dans un cube de 30 pieds à portée.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous créez ou détruisez 10
gallons d'eau supplémentaires, ou la taille du cube augmente de 1,5 m,
pour chaque niveau d'emplacement supérieur au 1er.



#### Création

*illusion de 5ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S, M (un minuscule morceau de matière du même type
que l'objet que vous prévoyez de créer).

**Durée :** Spécial

Vous tirez des brins de matière d'ombre des plans d'ombre pour créer
un objet non vivant de matière végétale dans la portée : articles mous,
corde, bois ou quelque chose de similaire. Vous pouvez également
utiliser ce sort pour créer des objets minéraux tels que de la pierre,
du cristal ou du métal. L'objet créé ne doit pas être plus grand qu'un
cube de 1,5 m, et l'objet doit avoir une forme et une matière que vous
avez déjà vues.

La durée dépend du matériau de l'objet. Si l'objet est composé de
plusieurs matériaux, utilisez la durée la plus courte.

  ---------------------------------
  Matériau               Durée
  ---------------------- ----------
  Matière végétale       1 jour

  Pierre ou cristal      12 heures

  Métaux précieux        1 heure

  Gems                   10 minutes

  Adamantine ou mithral  1 minute
  ---------------------------------

L'utilisation d'un matériau créé par ce sort comme composante
matérielle d'un autre sort entraîne l'échec de ce dernier.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, le cube augmente de 5 pieds
pour chaque niveau d'emplacement supérieur au 5e.



#### Soins des plaies

*Evocation de 1er niveau*

**Classes :** Barde,
Druide,
Rôdeur\...

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

Une créature que vous touchez regagne un nombre de points de vie égal à
1d8 + votre modificateur d'aptitude aux incantations. Ce sort n'a
aucun effet sur les morts-vivants ou les constructions.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les soins augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 1er.



#### Lumières dansantes

*Tour de magie d'évocation*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un peu de phosphore ou de bois de wych, ou un
ver luisant).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez jusqu'à quatre lumières de la taille d'une torche dans la
portée, les faisant apparaître comme des torches, des lanternes ou des
orbes incandescents qui planent dans l'air pendant la durée du sort.
Vous pouvez également combiner les quatre lumières en une forme
lumineuse vaguement humanoïde de taille moyenne. Quelle que soit la
forme choisie, chaque lumière diffuse une faible lumière dans un rayon
de 3 mètres.

Comme action bonus à votre tour, vous pouvez déplacer les lumières
jusqu'à 60 pieds vers un nouvel endroit à portée. Une lumière doit se
trouver à moins de 6 mètres d'une autre lumière créée par ce sort, et
une lumière s'éteint si elle dépasse la portée du sort.



#### Ténèbres

*Evocation de 2ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, M (fourrure de chauve-souris et une goutte de poix
ou un morceau de charbon).

**Durée :** Concentration, jusqu'à 10 minutes

Les ténèbres magiques se répandent à partir d'un point que vous
choisissez dans la portée pour remplir une sphère de 15 pieds de rayon
pour la durée. Les ténèbres se répandent dans les coins. Une créature
dotée de la vision dans le noir ne peut pas voir à travers ces ténèbres,
et la lumière non magique ne peut pas les éclairer.

Si le point que vous choisissez se trouve sur un objet que vous tenez ou
qui n'est pas porté, les Ténèbres émanent de l'objet et se déplacent
avec lui. Couvrir complètement la source des ténèbres avec un objet
opaque, comme un bol ou un casque, bloque les ténèbres.

Si une partie de la zone de ce sort chevauche une zone de lumière créée
par un sort de 2e niveau ou moins, le sort qui a créé la lumière est
dissipé.



#### Vision dans le noir

*transmutation de 2ème niveau*

**Classes :** Druide,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (soit une pincée de carotte séchée, soit une
agate).

**Durée :** 8 heures

Vous touchez une créature consentante pour lui accorder la capacité de
voir dans le noir. Pour la durée de l'opération, cette créature a une
vision dans le noir jusqu'à une portée de 18 mètres.



#### Lumière du jour

*év év évation de 3ème niveau*

**Classes :** Clerc,
Paladin,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** 1 heure

Une sphère de lumière de 60 pieds de rayon se répand à partir d'un
point que vous choisissez à portée. La sphère est une lumière vive et
diffuse une lumière faible sur 60 pieds supplémentaires.

Si vous choisissez un point sur un objet que vous tenez ou qui n'est
pas porté ou transporté, la lumière brille de l'objet et se déplace
avec lui. Couvrir complètement l'objet concerné avec un objet opaque,
comme un bol ou un casque, bloque la lumière.

Si une partie de la zone de ce sort chevauche une zone de ténèbres créée
par un sort de 3e niveau ou moins, le sort qui a créé les ténèbres est
dissipé.



#### Protection contre la mort

*4th-level abjuration*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** 8 heures

Vous touchez une créature et lui accordez une certaine protection contre
la mort.

La première fois que la cible tombe à 0 point de vie suite à des dégâts,
elle tombe à 1 point de vie et le sort prend fin.

Si le sort est toujours en vigueur lorsque la cible est soumise à un
effet qui la tuerait instantanément sans lui infliger de dégâts, cet
effet est au contraire annulé contre la cible, et le sort prend fin.



#### Boule de feu à retardement

*Evocation de 7ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (une minuscule boule de guano de chauve-souris
et de soufre).

**Durée :** Concentration, jusqu'à 1 minute

Un rayon de lumière jaune jaillit de votre doigt pointé, puis se
condense pour s'attarder sur un point choisi à portée sous la forme
d'une perle lumineuse pendant toute la durée du sort. Lorsque le sort
prend fin, soit parce que votre concentration est rompue, soit parce que
vous décidez d'y mettre fin, la perle se transforme, dans un faible
grondement, en une explosion de flammes qui se propage dans les coins.
Chaque créature dans une sphère de 20 pieds de rayon centrée sur ce
point doit effectuer un jet de sauvegarde de Dextérité. Une créature
subit des dégâts de feu égaux au total des dégâts accumulés en cas
d'échec, ou la moitié en cas de réussite.

Les dégâts de base du sort sont de 12d6. Si, à la fin de votre tour, la
perle n'a pas encore explosé, les dégâts augmentent de 1d6.

Si la perle incandescente est touchée avant l'expiration de
l'intervalle, la créature qui la touche doit effectuer un jet de
sauvegarde de Dextérité. En cas d'échec, le sort prend fin
immédiatement et la perle s'enflamme.

En cas de sauvegarde réussie, la créature peut lancer la perle jusqu'à
12 mètres. Si elle touche une créature ou un objet solide, le sort prend
fin et la perle explose.

Le feu endommage les objets dans la zone et enflamme les objets
inflammables qui ne sont pas portés ou transportés.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 8e niveau ou plus, les dégâts de base
augmentent de 1d6 pour chaque niveau d'emplacement supérieur au 7e.



#### Demi-plans

*Conjuration de 8ème niveau*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** S

**Durée :** 1 heure

Vous créez une porte ombragée sur une surface plane et solide que vous
pouvez voir à portée. La porte est suffisamment grande pour permettre
aux créatures de taille moyenne de passer sans encombre. Une fois
ouverte, la porte mène à un Demi-plan qui semble être une pièce vide de
10 mètres dans chaque dimension, faite de bois ou de pierre. Lorsque le
sort prend fin, la porte disparaît, et les créatures ou objets qui se
trouvent à l'intérieur du Demi-plan y restent piégés, car la porte
disparaît également de l'autre côté.

Chaque fois que vous lancez ce sort, vous pouvez créer un nouveau
Demi-plan, ou faire en sorte que la porte obscure se connecte à un
Demi-plan que vous avez créé lors d'un précédent lancement de ce sort.
De plus, si vous connaissez la nature et le contenu d'un Demi-plan créé
par une autre créature qui a lancé ce sort, vous pouvez faire en sorte
que la porte ombragée se connecte à son Demi-plan.



#### Détection du mal et du bien

*Divination de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Pendant la durée du sort, vous savez s'il y a une aberration, une
créature céleste, élémentaire, fée, fiélon ou mort-vivant à moins de 30
pieds de vous, ainsi que l'endroit où se trouve la créature. De même,
vous savez s'il y a un lieu ou un objet dans un rayon de 30 pieds
autour de vous qui a été béni ou profané par la magie.

Le sort peut traverser la plupart des barrières, mais il est bloqué par
1 pied de pierre, 1 pouce de métal commun, une fine feuille de plomb, ou
3 pieds de bois ou de terre.



#### Détection de la magie

*Divination de 1er niveau (rituel)*

**Classes :** Barde,
Druide,
Rôdeur,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Pour la durée, vous sentez la présence de la magie dans un rayon de 30
pieds autour de vous. Si vous sentez la magie de cette façon, vous
pouvez utiliser votre action pour voir une faible aura autour de toute
créature ou objet visible dans la zone qui porte la magie, et vous
apprenez son école de magie, le cas échéant.

Le sort peut traverser la plupart des barrières, mais il est bloqué par
1 pied de pierre, 1 pouce de métal commun, une fine feuille de plomb, ou
3 pieds de bois ou de terre.



#### Détection du poison et des maladies

*Divination de 1er niveau (rituel)*

**Classes :** Clerc,
Paladin

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (une feuille d'if)

**Durée :** Concentration, jusqu'à 10 minutes

Pendant la durée du sort, vous pouvez sentir la présence et
l'emplacement de poisons, de créatures empoisonnées et de maladies dans
un rayon de 30 pieds autour de vous. Vous identifiez également le type
de poison, de créature empoisonnée ou de maladie dans chaque cas.

Le sort peut traverser la plupart des barrières, mais il est bloqué par
1 pied de pierre, 1 pouce de métal commun, une fine feuille de plomb, ou
3 pieds de bois ou de terre.



#### Détection des pensées

*Divination de 2ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Les composantes :** V, S, M (une pièce de cuivre)

**Durée :** Concentration, jusqu'à 1 minute

Pour la durée du sort, vous pouvez lire les pensées de certaines
créatures. Lorsque vous lancez le sort et comme action à chaque tour
jusqu'à la fin du sort, vous pouvez concentrer votre esprit sur une
créature quelconque que vous pouvez voir dans un rayon de 30 pieds
autour de vous. Si la créature que vous choisissez a une Intelligence de
3 ou moins ou ne parle aucune langue, elle n'est pas affectée.

Vous apprenez d'abord les pensées superficielles de la créature,
c'est-à-dire ce qui la préoccupe le plus à ce moment-là. En tant
qu'action, vous pouvez soit déplacer votre attention sur les pensées
d'une autre créature, soit tenter de sonder plus profondément l'esprit
de la même créature. Si vous sondez plus profondément, la cible doit
effectuer un jet de sauvegarde de Sagesse. En cas d'échec, vous obtenez
un aperçu de son raisonnement (le cas échéant), de son état émotionnel
et de quelque chose de grand dans son esprit (comme quelque chose qui
l'inquiète, qu'elle aime ou qu'elle déteste). Si vous réussissez, le
sort prend fin. Dans tous les cas, la cible sait que vous sondez son
esprit et, à moins que vous ne portiez votre attention sur les pensées
d'une autre créature, celle-ci peut utiliser son action à son tour pour
effectuer un test d'Intelligence contesté par votre test
d'Intelligence ; si elle réussit, le sort prend fin.

Les questions adressées verbalement à la créature visée influencent
naturellement le cours de ses pensées. Ce sort est donc particulièrement
efficace dans le cadre d'un interrogatoire.

Vous pouvez également utiliser ce sort pour détecter la présence de
créatures pensantes que vous ne pouvez pas voir. Lorsque vous lancez le
sort ou comme action pendant la durée du sort, vous pouvez rechercher
des pensées dans un rayon de 30 pieds autour de vous. Le sort peut
traverser les barrières, mais 2 pieds de roche, 2 pouces de tout métal
autre que le plomb, ou une fine feuille de plomb vous bloque. Vous ne
pouvez pas détecter une créature dont l'Intelligence est inférieure ou
égale à 3 ou une créature qui ne parle aucune langue.

Une fois que vous avez détecté la présence d'une créature de cette
manière, vous pouvez lire ses pensées pour le reste de la durée comme
décrit ci-dessus, même si vous ne pouvez pas la voir, mais elle doit
toujours être à portée.



#### Porte dimensionnelle

*conjuration de 4ème niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 500 pieds

**Composantes :** V

**Durée :** Instantané

Vous vous téléportez de votre emplacement actuel à tout autre endroit à
portée. Vous arrivez exactement à l'endroit désiré. Il peut s'agir
d'un endroit que vous pouvez voir, que vous pouvez visualiser, ou que
vous pouvez décrire en indiquant la distance et la direction, comme
\"200 pieds en ligne droite vers le bas\" ou \"vers le nord-ouest à un
angle de 45 degrés, 300 pieds\".

Vous pouvez apporter des objets, à condition que leur poids ne dépasse
pas ce que vous pouvez porter. Vous pouvez également emmener une
créature consentante de votre taille ou plus petite qui porte un
équipement à hauteur de sa capacité de charge. La créature doit se
trouver à moins de 1,5 mètre de vous lorsque vous lancez ce sort.

Si vous arrivez dans un endroit déjà occupé par un objet ou une
créature, vous et toute créature voyageant avec vous subissez chacun 4d6
points de dégâts de force, et le sort échoue à vous téléporter.



#### Déguisement de soi

*illusion de 1er niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** 1 heure

Vous vous rendez vous-même - y compris vos vêtements, armures, armes et
autres objets sur votre personne - différent jusqu'à ce que le sort
prenne fin ou que vous utilisiez votre action pour l'annuler. Vous
pouvez paraître plus petit ou plus grand d'un pied et vous pouvez
paraître mince, gros ou entre les deux. Vous ne pouvez pas changer votre
type de corps, vous devez donc adopter une forme qui a la même
disposition de base des membres. Sinon, l'ampleur de l'illusion ne
dépend que de vous.

Les changements provoqués par ce sort ne résistent pas à l'inspection
physique. Par exemple, si vous utilisez ce sort pour ajouter un chapeau
à votre tenue, les objets passent à travers le chapeau, et quiconque le
touche ne sentirait rien ou sentirait votre tête et vos cheveux. Si vous
utilisez ce sort pour paraître plus mince que vous ne l'êtes, la main
de quelqu'un qui se tend pour vous toucher vous heurtera alors qu'elle
est apparemment toujours en l'air.

Pour discerner que vous êtes déguisé, une créature peut utiliser son
action pour inspecter votre apparence et doit réussir un test
d'Intelligence (Investigation) contre votre DC de sauvegarde contre les
sorts.



#### Désintégration

*Transmutation de 6ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (une pierre d'habitation et une pincée de
poussière)

**Durée :** Instantané

Un mince rayon vert jaillit de votre doigt pointé vers une cible que
vous pouvez voir à portée. La cible peut être une créature, un objet, ou
une création de force magique, comme le mur créé par Mur *de
force*.

Une créature ciblée par ce sort doit effectuer un jet de sauvegarde de
Dextérité. En cas d'échec, la cible subit 10d6 + 40 points de dégâts de
force. Si ces dégâts réduisent la cible à 0 point de vie, elle est
désintégrée.

Une créature désintégrée et tout ce qu'elle porte et transporte, à
l'exception des objets magiques, sont réduits en un tas de fine
poussière grise. La créature ne peut être ramenée à la vie qu'au moyen
d'une *véritable résurrection* ou d'un sort de
*Souhait*.

Ce sort désintègre automatiquement un objet non-magique Grand ou plus
petit ou une création de force magique. Si la cible est un objet ou une
création de force Très grand ou plus grand, ce sort en désintègre une
portion cubique de 3 mètres de côté. Un objet magique n'est pas affecté
par ce sort.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 7e niveau ou plus, les dégâts augmentent de 3d6
pour chaque niveau d'emplacement supérieur au 6e.



#### Dissipation du mal et du bien

*ab ab abjuration de niveau 5*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (eau bénite ou poudre d'argent et de fer)

**Durée :** Concentration, jusqu'à 1 minute

Une énergie chatoyante vous entoure et vous protège des fées, des
morts-vivants et des créatures provenant d'au-delà du plan matériel.
Pendant toute la durée du sort, les célestes, les élémentaires, les fey,
les fiélons et les morts-vivants ont un désavantage aux jets d'attaque
contre vous.

Vous pouvez mettre fin au sort de manière anticipée en utilisant l'une
des fonctions spéciales suivantes.

***Briser l'enchantement.*** Comme action, vous touchez une créature
que vous pouvez atteindre qui est charmée, effrayée ou possédée par un
céleste, un élémentaire, un fée, un fiélon ou un mort-vivant. La
créature que vous touchez n'est plus charmée, effrayée ou possédée par
de telles créatures.

***Renvoi.*** Comme action, effectuez une attaque de sort en mêlée
contre un céleste, un élémentaire, un fée, un fiélon ou un mort-vivant
que vous pouvez atteindre. En cas de succès, vous tentez de renvoyer la
créature sur son plan d'origine. La créature doit réussir un jet de
sauvegarde de Charisme ou être renvoyée sur son plan d'origine (si elle
n'y est pas déjà). Si elle ne se trouve pas sur son plan d'origine,
les morts-vivants sont envoyés dans un plan nécrotique parallèle, et les
fées dans un plan féerique.



#### Dissipation de la magie

*3rd-level abjuration*

**Classes :** Barde,
Druide,
Sorcier,
Magicien.

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Choisissez une créature, un objet ou un effet magique à portée. Tout
sort de 3e niveau ou moins sur la cible prend fin. Pour chaque sort de
4e niveau ou plus sur la cible, effectuez un test de capacité en
utilisant votre caractéristique d'incantation. La valeur DC est égale à
10 + le niveau du sort. En cas de réussite, le sort prend fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, vous mettez automatiquement
fin aux effets d'un sort sur la cible si le niveau du sort est égal ou
inférieur au niveau de l'emplacement de sort que vous avez utilisé.



#### Divination

*Divination de 4ème niveau (rituel)*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (encens et offrande sacrificielle appropriée à
votre religion, valant ensemble au moins 25 gp, que le sort consomme).

**Durée :** Instantané

Votre magie et une offrande vous mettent en contact avec un dieu ou les
serviteurs d'un dieu. Vous posez une seule question concernant un
objectif, un événement ou une activité spécifique devant se produire
dans les 7 jours. Le MJ offre une réponse véridique. La réponse peut
être une courte phrase, une rime cryptique ou un présage.

Le sort ne prend pas en compte les circonstances qui pourraient changer
le résultat, comme le lancement de sorts supplémentaires ou la perte ou
le gain d'un compagnon.

Si vous lancez le sort deux fois ou plus avant la fin de votre prochain
repos long, il y a 25 % de chances cumulatives pour chaque lancer après
le premier d'obtenir une lecture aléatoire. Le MJ effectue ce jet en
secret.



#### Faveur divine

*Evocation de 1er niveau*

**Classes :** Paladin

**Temps d'incantation :** 1 action bonus

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Votre prière vous confère un rayonnement divin. Jusqu'à la fin du sort,
vos attaques à l'arme blanche infligent 1d4 points de dégâts radiants
supplémentaires en cas de succès.



#### Parole divine

*Evocation de 7ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action bonus

**Portée :** 30 pieds

**Composantes :** V

**Durée :** Instantané

Vous prononcez une parole divine, imprégnée de la puissance qui a
façonné le monde à l'aube de la création. Choisissez un nombre
quelconque de créatures que vous pouvez voir à portée.

Chaque créature qui peut vous entendre doit effectuer un jet de
sauvegarde de Charisme. En cas d'échec, la créature subit un effet basé
sur ses points de vie actuels :

-   50 points de vie ou moins : assourdi pendant 1 minute
-   40 points de vie ou moins : assourdi et aveuglé pendant 10 minutes
-   30 points de vie ou moins : aveugle, assourdi et étourdi pendant 1
    heure
-   20 points de vie ou moins : tué instantanément

Indépendamment de ses points de vie actuels, un céleste, un élémentaire,
un fée ou un fiélon qui échoue à sa sauvegarde est forcé de retourner
sur son plan d'origine (s'il n'y est pas déjà) et ne peut pas revenir
sur votre plan actuel pendant 24 heures par n'importe quel moyen autre
qu'un sort de *Souhait*.



#### Domination de bête

*Enchantement de 4ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez de séduire une bête que vous pouvez voir à portée. Elle doit
réussir un jet de sauvegarde de Sagesse ou être charmée par vous pour la
durée du jet. Si vous ou des créatures qui vous sont amies la
combattent, elle a l'avantage au jet de sauvegarde.

Tant que la bête est charmée, vous avez un lien télépathique avec elle
tant que vous êtes tous les deux sur le même plan d'existence. Vous
pouvez utiliser ce lien télépathique pour donner des injonctions à la
créature pendant que vous êtes conscient (aucune action requise),
auxquelles elle fait de son mieux pour obéir. Vous pouvez spécifier un
plan d'action simple et général, tel que \"Attaquez cette créature\",
\"Courez là-bas\" ou \"Allez chercher cet objet\". Si la créature
exécute l'ordre et ne reçoit pas d'autres instructions de votre part,
elle se défend et se préserve au mieux de ses capacités.

Vous pouvez utiliser votre action pour prendre le contrôle total et
précis de la cible. Jusqu'à la fin de votre prochain tour, la créature
ne fait que les actions que vous choisissez, et ne fait rien que vous ne
lui permettez pas de faire. Pendant ce temps, vous pouvez également
faire en sorte que la créature utilise une réaction, mais cela nécessite
que vous utilisiez également votre propre réaction.

Chaque fois que la cible subit des dégâts, elle effectue un nouveau jet
de sauvegarde de Sagesse contre le sort. Si le jet de sauvegarde
réussit, le sort prend fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort avec un
emplacement de 5e niveau, la durée est la concentration, jusqu'à 10
minutes. Lorsque vous utilisez un emplacement de sort de 6e niveau, la
durée est la concentration, jusqu'à 1 heure. Lorsque vous utilisez un
emplacement de sort de 7e niveau ou plus, la durée est la concentration,
jusqu'à 8 heures.



#### Domination de monstre

*Enchantement de 8ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous tentez de séduire une créature que vous pouvez voir à portée. Elle
doit réussir un jet de sauvegarde de Sagesse ou être charmée par vous
pour la durée du jet. Si vous ou des créatures qui vous sont amies la
combattent, elle a l'avantage au jet de sauvegarde.

Pendant que la créature est charmée, vous avez un lien télépathique avec
elle tant que vous êtes tous les deux sur le même plan d'existence.
Vous pouvez utiliser ce lien télépathique pour donner des injonctions à
la créature pendant que vous êtes conscient (aucune action requise),
auxquelles elle fait de son mieux pour obéir. Vous pouvez spécifier un
plan d'action simple et général, tel que \"Attaquez cette créature\",
\"Courez là-bas\" ou \"Allez chercher cet objet\". Si la créature
exécute l'ordre et ne reçoit pas d'autres instructions de votre part,
elle se défend et se préserve au mieux de ses capacités.

Vous pouvez utiliser votre action pour prendre le contrôle total et
précis de la cible. Jusqu'à la fin de votre prochain tour, la créature
ne fait que les actions que vous choisissez, et ne fait rien que vous ne
lui permettez pas de faire.

Pendant ce temps, vous pouvez également faire en sorte que la créature
utilise une réaction, mais cela nécessite que vous utilisiez également
votre propre réaction.

Chaque fois que la cible subit des dégâts, elle effectue un nouveau jet
de sauvegarde de Sagesse contre le sort. Si le jet de sauvegarde
réussit, le sort prend fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort avec un
emplacement de sort de 9e niveau, la durée est la concentration,
jusqu'à 8 heures.



#### Domination de personne

*Enchantement de 5ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez de séduire un humanoïde que vous pouvez voir à portée. Il
doit réussir un jet de sauvegarde de Sagesse ou être charmé par vous
pour la durée du jet. Si vous ou des créatures qui vous sont amies le
combattent, il a l'avantage au jet de sauvegarde.

Pendant que la cible est charmée, vous avez un lien télépathique avec
elle tant que vous êtes tous deux sur le même plan d'existence. Vous
pouvez utiliser ce lien télépathique pour donner des injonctions à la
créature pendant que vous êtes conscient (aucune action requise),
auxquelles elle fait de son mieux pour obéir. Vous pouvez spécifier un
plan d'action simple et général, tel que \"Attaquez cette créature\",
\"Courez là-bas\" ou \"Allez chercher cet objet\". Si la créature
exécute l'ordre et ne reçoit pas d'autres instructions de votre part,
elle se défend et se préserve au mieux de ses capacités.

Vous pouvez utiliser votre action pour prendre le contrôle total et
précis de la cible. Jusqu'à la fin de votre prochain tour, la créature
ne fait que les actions que vous choisissez, et ne fait rien que vous ne
lui permettez pas de faire. Pendant ce temps, vous pouvez également
faire en sorte que la créature utilise une réaction, mais cela nécessite
que vous utilisiez également votre propre réaction.

Chaque fois que la cible subit des dégâts, elle effectue un nouveau jet
de sauvegarde de Sagesse contre le sort. Si le jet de sauvegarde
réussit, le sort prend fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau, la durée est la concentration,
jusqu'à 10 minutes. Lorsque vous utilisez un emplacement de sort de 7e
niveau, la durée est la concentration, jusqu'à 1 heure. Lorsque vous
utilisez un emplacement de sort de 8e niveau ou plus, la durée est la
concentration, jusqu'à 8 heures.



#### Songe

*illusion de 5ème niveau*

**Classes :** Barde,
magicien\...

**Temps d'incantation :** 1 minute

**Portée :** Spécial

**Composantes :** V, S, M (une poignée de sable, un peu d'encre et une
plume d'oie arrachée à un oiseau endormi).

**Durée :** 8 heures

Ce sort façonne les rêves d'une créature. Choisissez une créature
connue de vous comme cible de ce sort. La cible doit se trouver sur le
même plan d'existence que vous. Les créatures qui ne dorment pas, comme
les elfes, ne peuvent pas être contactées par ce sort. Vous, ou une
créature volontaire que vous touchez, entrez en état de transe, agissant
comme un messager. Pendant la transe, le messager est conscient de son
environnement, mais ne peut ni agir ni se déplacer.

Si la cible est endormie, le messager apparaît dans les rêves de la
cible et peut converser avec elle tant qu'elle reste endormie, pendant
toute la durée du sort. Le messager peut également façonner
l'environnement du rêve, en créant des paysages, des objets et
d'autres images. Le messager peut sortir de la transe à tout moment,
mettant ainsi fin à l'effet du sort de manière anticipée. La cible se
souvient parfaitement du songe à son réveil. Si la cible est éveillée
lorsque vous lancez le sort, le messager le sait et peut soit mettre fin
à la transe (et au sort), soit attendre que la cible s'endorme, auquel
cas le messager apparaît dans les rêves de la cible.

Vous pouvez faire en sorte que le messager apparaisse monstrueux et
terrifiant pour la cible. Dans ce cas, le messager peut délivrer un
message de dix mots maximum, puis la cible doit effectuer un jet de
sauvegarde de Sagesse. En cas d'échec, les échos de la monstruosité
fantasmatique engendrent un cauchemar qui dure le temps du Sommeil de la
cible et l'empêche de bénéficier de ce repos. De plus, lorsque la cible
se réveille, elle subit 3d6 dégâts psychiques.

Si vous avez une partie du corps, une mèche de cheveux, un ongle coupé
ou une partie similaire du corps de la cible, celle-ci effectue son jet
de sauvegarde avec un désavantage.



#### Druidisme

*Tour de magie de transmutation*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Instantané

En chuchotant aux esprits de la nature, vous créez l'un des effets
suivants à portée :

-   Vous créez un minuscule effet sensoriel inoffensif qui prédit le
    temps qu'il fera à l'endroit où vous vous trouvez au cours des 24
    prochaines heures. L'effet peut se manifester sous la forme d'un
    orbe doré pour un ciel clair, d'un nuage pour la pluie, de flocons
    de neige pour la neige, et ainsi de suite. Cet effet persiste
    pendant 1 round.
-   Vous faites instantanément éclore une fleur, ouvrir une cosse de
    graine ou faire fleurir un bourgeon de feuille.
-   Vous créez un effet sensoriel instantané et inoffensif, comme des
    feuilles qui tombent, un souffle de vent, le bruit d'un petit
    animal ou la faible odeur de mouffette. L'effet doit tenir dans un
    cube de 1,5 m de côté.
-   Vous allumez ou éteignez instantanément une bougie, une torche ou un
    petit feu de camp.



#### Tremblement de terre

*Evocation de 8ème niveau*

**Classes :** Clerc,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 500 pieds

**Composantes :** V, S, M (une pincée de terre, un morceau de roche et
un morceau d'argile).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez une perturbation sismique en un point du sol que vous pouvez
voir à portée. Pendant la durée de l'opération, une secousse intense
déchire le sol dans un cercle de 30 mètres de rayon centré sur ce point
et fait trembler les créatures et les structures en contact avec le sol
dans cette zone.

Le sol de la zone devient un terrain difficile. Chaque créature au sol
qui se concentre doit effectuer un jet de sauvegarde de Constitution. En
cas d'échec, la concentration de la créature est rompue.

Lorsque vous lancez ce sort et à la fin de chaque tour que vous passez à
vous concentrer dessus, chaque créature au sol dans la zone doit
effectuer un jet de sauvegarde de Dextérité. En cas d'échec, la
créature est mise à plat ventre.

Ce sort peut avoir des effets supplémentaires en fonction du terrain de
la zone, comme déterminé par le MJ.

***Fissures.*** Des fissures s'ouvrent dans toute la zone du sort au
début de votre prochain tour après avoir lancé le sort. Un total de 1d6
fissures de ce type s'ouvrent à des endroits choisis par le MJ. Chacune
a une profondeur de 1d10 × 10 pieds, une largeur de 10 pieds et s'étend
d'un bord à l'autre de la zone du sort. Une créature se trouvant à
l'endroit où une fissure s'ouvre doit réussir un jet de sauvegarde de
Dextérité ou tomber dedans. Une créature qui réussit son jet de
sauvegarde se déplace avec le bord de la fissure lorsque celle-ci
s'ouvre.

Une fissure qui s'ouvre sous une structure provoque son effondrement
automatique (voir ci-dessous).

***Structures.*** La secousse inflige 50 dégâts de matraquage à toute
structure en contact avec le sol dans la zone lorsque vous lancez le
sort et au début de chacun de vos tours jusqu'à la fin du sort. Si une
structure tombe à 0 points de vie, elle s'effondre et endommage
potentiellement les créatures proches. Une créature située à la moitié
de la hauteur de la structure doit effectuer un jet de sauvegarde de
Dextérité. En cas d'échec, la créature subit 5d6 points de dégâts de
contondant, est mise à plat ventre et se retrouve ensevelie sous les
décombres, nécessitant un test de Force (Athlétisme) DC 20 comme action
pour s'échapper. Le MJ peut augmenter ou diminuer le DC, en fonction de
la nature des décombres. En cas de sauvegarde réussie, la créature subit
deux fois moins de dégâts et ne tombe pas au sol ni n'est enterrée.



#### Décharge traumaturgique

*Tour de magie d'évocation*

**Classe :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Un rayon d'énergie crépitant se dirige vers une créature à portée.
Effectuez une attaque de sort à distance contre la cible. En cas de
succès, la cible subit 1d10 points de dégâts de force.

Le sort crée plus d'un faisceau lorsque vous atteignez les niveaux
supérieurs : deux faisceaux au 5e niveau, trois faisceaux au 11e niveau
et quatre faisceaux au 17e niveau. Vous pouvez diriger les faisceaux
vers la même cible ou vers des cibles différentes. Effectuez un jet
d'attaque distinct pour chaque rayon.



#### Amélioration de caractéristique

*transmutation de 2ème niveau*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (fourrure ou plume d'une bête)

**Durée :** Concentration, jusqu'à 1 heure

Vous touchez une créature et lui conférez une amélioration magique.
Choisissez l'un des effets suivants ; la cible bénéficie de cet effet
jusqu'à la fin du sort.

***Endurance de l'ours.*** La cible a un avantage sur les tests de
Constitution. Elle gagne également 2d6 points de vie temporaires, qui
sont perdus lorsque le sort prend fin.

***Force du taureau.*** La cible a un avantage sur les tests de Force,
et sa capacité de charge double.

***La grâce du Chat.*** La cible a l'avantage sur les tests de
Dextérité. Elle ne subit pas non plus les dégâts d'une chute de 6
mètres ou moins si elle n'est pas frappée d'incapacité.

***Splendeur de l'Aigle.*** La cible a l'avantage sur les tests de
Charisme.

***La ruse du renard.*** La cible a un avantage sur les tests
d'Intelligence.

***Sagesse de la Chouette.*** La cible a un avantage sur les tests de
Sagesse.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
2e.



#### Agrandissement/rapetissement

*Transmutation de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une pincée de fer en poudre)

**Durée :** Concentration, jusqu'à 1 minute

Vous faites grandir ou rapetisser une créature ou un objet que vous
pouvez voir à portée pendant la durée du sort. Choisissez une créature
ou un objet qui n'est ni porté, ni transporté. Si la cible ne veut pas,
elle peut effectuer un jet de sauvegarde de Constitution. En cas de
réussite, le sort n'a aucun effet.

Si la cible est une créature, tout ce qu'elle porte et transporte
change de taille avec elle. Tout objet abandonné par une créature
affectée reprend immédiatement sa taille normale.

***Agrandissement.*** La taille de la cible double dans toutes les
dimensions, et son poids est multiplié par huit.

Cette croissance augmente sa taille d'une catégorie - de Moyenne à
Grande, par exemple. S'il n'y a pas assez de place pour que la cible
double sa taille, la créature ou l'objet atteint la taille maximale
possible dans l'espace disponible. Jusqu'à la fin du sort, la cible a
également un avantage sur les tests de Force et les jets de sauvegarde
de Force. Les armes de la cible grandissent également pour s'adapter à
sa nouvelle taille. Tant que ces armes sont agrandies, les attaques de
la cible avec celles-ci infligent 1d4 points de dégâts supplémentaires.

***Rapetissement.*** La taille de la cible est réduite de moitié dans
toutes les dimensions, et son poids est réduit à un huitième de la
normale. Cette réduction diminue sa taille d'une catégorie - de Moyenne
à Petite, par exemple. Jusqu'à la fin du sort, la cible a également un
désavantage aux tests de Force et aux jets de sauvegarde de Force. Les
armes de la cible sont également réduites pour correspondre à sa
nouvelle taille. Tant que ces armes sont réduites, les attaques de la
cible avec elles infligent 1d4 points de dégâts en moins (cela ne peut
pas réduire les dégâts en dessous de 1).



#### Enchevêtrement

*conjuration de 1er niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Des herbes et des vignes agrippantes poussent sur le sol dans un carré
de 20 pieds de côté à partir d'un point situé à portée. Pour la durée
du sort, ces plantes transforment le sol de la zone en terrain
difficile.

Une créature dans la zone où vous lancez le sort doit réussir un jet de
sauvegarde de Force ou être entravée par les plantes enchevêtrées
jusqu'à la fin du sort. Une créature entravée par les plantes peut
utiliser son action pour faire un test de Force contre le DC de
sauvegarde de votre sort. En cas de réussite, elle se libère.

Lorsque le sort prend fin, les plantes conjurées se fanent.



#### Captiver

*Enchantement de 2ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** 1 minute

Vous tissez un chapelet de mots distrayants, obligeant les créatures de
votre choix que vous pouvez voir à portée et qui peuvent vous entendre à
effectuer un jet de sauvegarde de Sagesse. Toute créature qui ne peut
pas être charmée réussit ce jet de sauvegarde automatiquement, et si
vous ou vos compagnons combattez une créature, elle a l'avantage sur le
jet de sauvegarde. En cas d'échec, la cible a un désavantage aux tests
de Sagesse (Perception) pour percevoir toute créature autre que vous
jusqu'à la fin du sort ou jusqu'à ce qu'elle ne puisse plus vous
entendre. Le sort prend fin si vous êtes frappé d'incapacité ou si vous
ne pouvez plus parler.



#### Forme éthérée

*transmutation de 7ème niveau*

**Classes :** Barde,
Sorcier.

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Jusqu'à 8 heures

Vous pénétrez dans les régions frontalières du plan éthéré, dans la zone
où il chevauche votre plan actuel. Vous restez dans la frontière éthérée
pendant toute la durée du sort ou jusqu'à ce que vous utilisiez votre
action pour l'annuler. Pendant ce temps, vous pouvez vous déplacer dans
n'importe quelle direction. Si vous vous déplacez vers le haut ou le
bas, chaque pied de mouvement coûte un pied supplémentaire. Vous pouvez
voir et entendre le plan d'où vous venez, mais tout y est gris, et vous
ne pouvez rien voir à plus de 60 pieds.

Lorsque vous êtes sur le plan éthéré, vous ne pouvez affecter et être
affecté que par les autres créatures de ce plan. Les créatures qui ne
sont pas sur le plan éthéré ne peuvent pas vous percevoir et ne peuvent
pas interagir avec vous, à moins qu'une capacité spéciale ou une magie
ne leur en donne la possibilité.

Vous ignorez tous les objets et effets qui ne sont pas sur le plan
éthéré, ce qui vous permet de vous déplacer à travers les objets que
vous percevez sur le plan d'où vous venez.

Lorsque le sort prend fin, vous retournez immédiatement dans le plan
d'où vous venez à l'endroit que vous occupez actuellement. Si vous
occupez le même endroit qu'un objet solide ou une créature lorsque cela
se produit, vous êtes immédiatement déplacé vers l'espace inoccupé le
plus proche que vous pouvez occuper et subissez des dégâts de force
égaux à deux fois le nombre de pieds que vous êtes déplacé.

Ce sort n'a aucun effet si vous le lancez alors que vous vous trouvez
sur le plan éthéré ou sur un plan qui ne le borde pas, comme l'un des
plans extérieurs.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 8e niveau ou plus, vous pouvez cibler jusqu'à
trois créatures consentantes (vous compris) pour chaque niveau
d'emplacement supérieur au 7e. Les créatures doivent se trouver à moins
de 3 mètres de vous lorsque vous lancez le sort.



#### Repli expéditif

*Trans transmutation de 1er niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action bonus

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Ce sort vous permet de vous déplacer à une vitesse incroyable. Lorsque
vous lancez ce sort, et ensuite comme action bonus à chacun de vos tours
jusqu'à la fin du sort, vous pouvez effectuer l'action Foncer.



#### Mauvais oeil

*Nécromancie de 6ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Pendant la durée du sort, vos yeux deviennent un vide d'encre imprégné
d'un pouvoir redoutable. Une créature de votre choix située à moins de
60 pieds de vous et que vous pouvez voir doit réussir un jet de
sauvegarde de Sagesse ou être affectée par l'un des effets suivants de
votre choix pendant la durée du sort. À chacun de vos tours jusqu'à la
fin du sort, vous pouvez utiliser votre action pour cibler une autre
créature, mais vous ne pouvez plus cibler une créature si elle a réussi
un jet de sauvegarde contre ce sort de Mauvais *œil*.

***Endormi.*** La cible tombe inconsciente. Elle se réveille si elle
subit des dégâts ou si une autre créature utilise son action pour
secouer le dormeur.

***Paniqué.*** La cible est effrayée par vous. À chacun de ses tours, la
créature effrayée doit effectuer l'action Foncer et s'éloigner de vous
par le chemin disponible le plus sûr et le plus court, à moins qu'elle
ne puisse se déplacer nulle part. Si la cible se déplace jusqu'à un
endroit situé à au moins 60 pieds de vous où elle ne peut plus vous
voir, cet effet prend fin.

***Malade.*** La cible est désavantagée aux jets d'attaque et aux tests
de capacité. À la fin de chacun de ses tours, elle peut effectuer un
autre jet de sauvegarde de Sagesse. Si elle réussit, l'effet prend fin.



#### Fabrication

*transm transm transm transm transm transm transm transm transm transm
du 4ème niveau*

**Classes :** Magicien

**Temps de coulée :** 10 minutes

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous transformez des matières premières en produits de la même matière.
Par exemple, vous pouvez fabriquer un pont en bois à partir d'une
touffe d'arbres, une corde à partir d'un morceau de chanvre et des
vêtements à partir de lin ou de laine.

Choisissez les matières premières que vous pouvez voir à portée. Vous
pouvez fabriquer un objet Grand ou plus petit (contenu dans un cube de 3
mètres ou dans huit cubes de 1,5 mètre reliés entre eux), si vous
disposez d'une quantité suffisante de matière première. Cependant, si
vous travaillez avec du métal, de la pierre ou une autre substance
minérale, l'objet fabriqué ne peut être plus grand que Moyen (contenu
dans un seul cube de 1,5 m). La qualité des objets fabriqués par ce sort
est proportionnelle à la qualité de la matière première.

Les créatures ou les objets magiques ne peuvent pas être créés ou
transmutés par ce sort. Vous ne pouvez pas non plus l'utiliser pour
créer des objets qui requièrent habituellement un haut degré
d'artisanat, comme des bijoux, des armes, du verre ou des armures, à
moins que vous ne maîtrisiez le type d'outils d'artisan utilisés pour
fabriquer de tels objets.



#### Lueurs féeriques

*Evocation de 1er niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Concentration, jusqu'à 1 minute

Chaque objet se trouvant dans un cube de 6 mètres de côté et se trouvant
à portée est entouré d'une lumière bleue, verte ou violette (au choix).
Toute créature se trouvant dans la zone où le sort est lancé est
également entourée de lumière si elle échoue à un jet de sauvegarde de
Dextérité. Pendant toute la durée du sort, les objets et les créatures
affectées diffusent une faible lumière dans un rayon de 3 mètres.

Tout jet d'attaque contre une créature ou un objet affecté a un
avantage si l'attaquant peut le voir, et la créature ou l'objet
affecté ne peut pas bénéficier du fait d'être invisible.



#### Chien fidèle

*conjuration de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un minuscule sifflet en argent, un morceau
d'os et un fil)

**Durée :** 8 heures

Vous conjurez un chien de garde fantôme dans un espace inoccupé que vous
pouvez voir à portée, où il reste pour la durée, jusqu'à ce que vous le
renvoyiez par une action, ou jusqu'à ce que vous vous éloigniez de lui
de plus de 100 pieds.

Le molosse est invisible pour toutes les créatures sauf vous et ne peut
pas être blessé. Lorsqu'une créature Petit ou plus grande s'approche à
moins de 30 pieds de lui sans avoir prononcé au préalable le mot de
passe que vous spécifiez lorsque vous lancez ce sort, le molosse se met
à aboyer bruyamment. Le molosse voit les créatures invisibles et peut
voir dans le plan éthéré. Il ignore les illusions.

Au début de chacun de vos tours, le molosse tente de mordre une créature
dans un rayon de 1,5 mètre de lui qui vous est hostile. Le bonus à
l'attaque du molosse est égal au modificateur de votre aptitude aux
incantations + votre bonus de maîtrise. En cas de succès, il inflige 4d8
points de dégâts perforants.



#### Simulacre de vie

*Nécromancie de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composants :** V, S, M (une petite quantité d'alcool ou de spiritueux
distillés)

**Durée :** 1 heure

En vous renforçant avec un fac-similé de vie nécromantique, vous gagnez
1d4 + 4 points de vie temporaires pour la durée.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, vous gagnez 5 points de vie
temporaires supplémentaires pour chaque niveau d'emplacement supérieur
au 1er.



#### Peur

*Ill illusion de niveau 3*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** Soi-même (cône de 30 pieds)

**Composantes :** V, S, M (une plume blanche ou le cœur d'une poule)

**Durée :** Concentration, jusqu'à 1 minute

Vous projetez une image fantasmatique des pires craintes d'une
créature. Chaque créature dans un cône de 30 pieds doit réussir un jet
de sauvegarde de Sagesse ou lâcher ce qu'elle tient et être effrayée
pour la durée du jet.

Lorsqu'elle est effrayée par ce sort, une créature doit effectuer
l'action Foncer et s'éloigner de vous par le chemin le plus sûr
disponible à chacun de ses tours, à moins qu'elle ne puisse se déplacer
nulle part. Si la créature termine son tour dans un endroit où elle n'a
pas de ligne de vue vers vous, la créature peut faire un jet de
sauvegarde de Sagesse. En cas de sauvegarde réussie, le sort prend fin
pour cette créature.



#### Feuille morte

*Trans transm transm transm transm transm transm transm transm transm du
1er niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 réaction, que vous prenez lorsque vous ou
une créature dans un rayon de 60 pieds de vous tombez.

**Portée :** 60 pieds

**Composantes :** V, M (une petite plume ou un morceau de duvet)

**Durée :** 1 minute

Choisissez jusqu'à cinq créatures tombantes à portée. La vitesse de
descente d'une créature en chute ralentit à 60 pieds par round jusqu'à
la fin du sort. Si la créature atterrit avant la fin du sort, elle ne
subit pas de dégâts de chute et peut retomber sur ses pieds, et le sort
prend fin pour cette créature.



#### Esprit faible

*Enchantement de 8ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (une poignée de sphères d'argile, de cristal,
de verre ou de minéraux).

**Durée :** Instantané

Vous faites exploser l'esprit d'une créature que vous pouvez voir à
portée, pour tenter de briser son intellect et sa personnalité. La cible
subit 4d6 dégâts psychiques et doit effectuer un jet de sauvegarde
d'Intelligence.

Si la sauvegarde échoue, les scores d'Intelligence et de Charisme de la
créature deviennent 1. La créature ne peut pas lancer de sorts, activer
des objets magiques, comprendre le langage ou communiquer de manière
intelligible. La créature peut cependant identifier ses amis, les suivre
et même les protéger.

À la fin de tous les 30 jours, la créature peut répéter son jet de
sauvegarde contre ce sort. Si elle réussit son jet de sauvegarde, le
sort prend fin.

Le sort peut également être interrompu par Restauration
*supérieure* ou
*Souhait*.



#### Appel de familier

*conjuration de 1er niveau*

**Classes :** Invocateur

**Durée du moulage :** 1 heure

**Portée :** 10 pieds

**Composantes :** V, S, M (10 gp de charbon de bois, d'encens et
d'herbes qui doivent être consumés par le feu dans un brasero en
laiton).

**Durée :** Instantané

Vous gagnez les services d'un familier, un esprit qui prend une forme
animale de votre choix : chauve-souris,
crabe,
lézard, serpent
venimeux,
rat,
araignée. Apparaissant dans un espace
inoccupé à portée, le familier possède les statistiques de la forme
choisie, bien qu'il s'agisse d'un céleste, d'un fée ou d'un fiélon
(au choix) au lieu d'une bête.

Votre familier agit indépendamment de vous, mais il obéit toujours à vos
ordres. En combat, il lance son propre jet d'initiative et agit à son
propre tour. Un familier ne peut pas attaquer, mais il peut faire
d'autres actions comme d'habitude.

Lorsque le familier tombe à 0 points de vie, il disparaît, sans laisser
de forme physique. Il réapparaît lorsque vous lancez à nouveau ce sort.

Tant que votre familier se trouve à moins de 30 mètres de vous, vous
pouvez communiquer avec lui par télépathie. De plus, en tant qu'action,
vous pouvez voir à travers les yeux de votre familier et entendre ce
qu'il entend jusqu'au début de votre prochain tour, en bénéficiant de
tous les sens spéciaux dont dispose le familier. Pendant ce temps, vous
êtes sourd et aveugle en ce qui concerne vos propres sens.

En tant qu'action, vous pouvez temporairement renvoyer votre familier.
Il disparaît dans une dimension de poche où il attend votre appel. Vous
pouvez également le renvoyer pour toujours. En tant qu'action pendant
qu'il est temporairement congédié, vous pouvez le faire réapparaître
dans n'importe quel espace inoccupé à moins de 30 pieds de vous.

Vous ne pouvez pas avoir plus d'un familier à la fois. Si vous lancez
ce sort alors que vous avez déjà un familier, vous lui faites adopter
une nouvelle forme. Choisissez une des formes de la liste ci-dessus.
Votre familier se transforme en la créature choisie.

Enfin, lorsque vous lancez un sort avec une portée de toucher, votre
familier peut délivrer le sort comme s'il l'avait lancé. Votre
familier doit se trouver à moins de 30 mètres de vous et il doit
utiliser sa réaction pour délivrer le sort lorsque vous le lancez. Si le
sort nécessite un jet d'attaque, vous utilisez votre modificateur
d'attaque pour ce jet.



#### Appel de destrier

*conjuration de 2ème niveau*

**Classes :** Paladin

**Temps de coulée :** 10 minutes

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous invoquez un esprit qui prend la forme d'un destrier
exceptionnellement intelligent, fort et loyal, et créez un lien durable
avec lui. Apparaissant dans un espace inoccupé à portée, le destrier
prend la forme de votre choix : un cheval de guerre, un poney, un
chameau, un élan ou un molosse. (Votre MJ peut autoriser d'autres
animaux à être invoqués comme des destriers.) Le destrier a les
statistiques de la forme choisie, bien qu'il soit céleste, fée ou
fiélon (votre choix) au lieu de son type normal. De plus, si votre
destrier a une Intelligence de 5 ou moins, son Intelligence devient 6,
et il gagne la capacité de comprendre une langue de votre choix que vous
parlez.

Votre destrier vous sert de monture, au combat comme à l'extérieur, et
vous avez un lien instinctif avec lui qui vous permet de combattre comme
une unité homogène. Lorsque vous êtes monté sur votre destrier, vous
pouvez faire en sorte que tout sort que vous lancez et qui ne vise que
vous vise également votre destrier.

Lorsque le destrier tombe à 0 points de vie, il disparaît, sans laisser
de forme physique. Vous pouvez également renvoyer votre destrier à tout
moment en tant qu'action, ce qui le fait disparaître. Dans les deux
cas, en lançant à nouveau ce sort, vous rappelez le même destrier, qui
retrouve son maximum de points de vie.

Tant que votre monture se trouve à moins d'un kilomètre de vous, vous
pouvez communiquer avec elle par télépathie.

Vous ne pouvez pas avoir plus d'un destrier lié par ce sort à la fois.
En tant qu'action, vous pouvez libérer le destrier de son lien à tout
moment, le faisant disparaître.



#### Sens des pièges

*Divination de 2ème niveau*

**Classes :** Clerc,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous détectez la présence de tout piège à portée qui se trouve en ligne
de mire. Un piège, dans le cadre de ce sort, comprend tout ce qui
pourrait infliger un effet soudain ou inattendu que vous considérez
comme nuisible ou indésirable, et qui a été spécifiquement prévu comme
tel par son créateur. Ainsi, le sort détectera une zone affectée par le
sort *Alarme*
ou un piège mécanique, mais il ne révélera pas une faiblesse naturelle
dans le sol, un plafond instable ou un gouffre caché.

Ce sort ne fait que révéler la présence d'un piège. Vous n'apprenez
pas l'emplacement de chaque piège, mais vous apprenez la nature
générale du danger posé par un piège que vous sentez.



#### Sens de l'orientation

*Divination de 6ème niveau*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V, S, M (un ensemble d'outils divinatoires - os,
bâtons d'ivoire, cartes, dents ou runes gravées - d'une valeur de 100
gp et un objet de l'endroit que vous souhaitez trouver).

**Durée :** Concentration, jusqu'à 1 jour

Ce sort vous permet de trouver le chemin physique le plus court et le
plus direct vers un lieu fixe spécifique que vous connaissez sur le même
plan d'existence. Si vous nommez une destination sur un autre plan
d'existence, une destination qui se déplace (comme une forteresse
mobile), ou une destination qui n'est pas spécifique (comme \"le
repaire d'un dragon vert\"), le sort échoue.

Pour la durée, tant que vous êtes sur le même plan d'existence que la
destination, vous savez à quelle distance elle se trouve et dans quelle
direction. Pendant votre voyage, chaque fois que vous avez le choix
entre plusieurs chemins, vous déterminez automatiquement le chemin le
plus court et le plus direct (mais pas nécessairement le plus sûr) vers
votre destination.



#### Doigt de mort

*Nécromancie de 7ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous envoyez de l'énergie négative à travers une créature que vous
pouvez voir à portée, lui causant une douleur fulgurante. La cible doit
effectuer un jet de sauvegarde de Constitution. Elle subit 7d8 + 30
points de dégâts nécrotiques en cas d'échec, ou la moitié en cas de
réussite.

Un humanoïde tué par ce sort se relève au début de votre prochain tour
en tant que zombi qui est en permanence sous votre commandement, suivant
vos ordres verbaux au mieux de ses capacités.



#### Trait de feu

*Tour de magie d'évocation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous lancez une mote de feu sur une créature ou un objet à portée.
Effectuez une attaque de sort à distance contre la cible. En cas de
succès, la cible subit 1d10 points de dégâts de feu. Un objet
inflammable touché par ce sort s'enflamme s'il n'est pas porté ou
transporté.

Les dégâts de ce sort augmentent de 1d10 lorsque vous atteignez le 5e
niveau (2d10), le 11e niveau (3d10) et le 17e niveau (4d10).



#### Bouclier de feu

*év évocation de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (un peu de phosphore ou une luciole)

**Durée :** 10 minutes

Des flammes fines et vaporeuses enveloppent votre corps pour la durée du
sort, diffusant une lumière vive dans un rayon de 3 mètres et une
lumière chétive sur 3 mètres supplémentaires. Vous pouvez mettre fin au
sort plus tôt en utilisant une action pour l'annuler.

Les flammes vous fournissent un bouclier chaud ou un bouclier froid,
selon votre choix. Le bouclier chaud vous confère une résistance aux
dégâts de froid, tandis que le bouclier froid vous confère une
résistance aux dégâts de feu.

De plus, chaque fois qu'une créature située à moins de 1,5 mètre de
vous vous frappe avec une attaque de mêlée, le bouclier éclate en
flammes. L'attaquant subit 2d8 points de dégâts de feu avec un bouclier
chaud, ou 2d8 points de dégâts de froid avec un bouclier froid.



#### Tempête de feu

*Evocation de 7ème niveau*

**Classes :** Clerc,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S

**Durée :** Instantané

Une tempête composée de nappes de flammes rugissantes apparaît à
l'endroit de votre choix, à portée. La zone de la tempête est
constituée d'un maximum de dix cubes de 3 mètres de côté, que vous
pouvez disposer comme vous le souhaitez. Chaque cube doit avoir au moins
une face adjacente à la face d'un autre cube. Chaque créature présente
dans la zone doit effectuer un jet de sauvegarde de Dextérité. Elle
subit 7d10 dégâts de feu en cas de sauvegarde ratée, ou la moitié en cas
de réussite.

Le feu endommage les objets de la zone et enflamme les objets
inflammables qui ne sont pas portés ou transportés. Si vous le
choisissez, la vie végétale de la zone n'est pas affectée par ce sort.



#### Boule de feu

*év év évation de 3ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (une minuscule boule de guano de chauve-souris
et de soufre).

**Durée :** Instantané

Un trait lumineux jaillit de votre doigt pointé vers un point de votre
choix à portée, puis se transforme en une explosion de flammes dans un
grondement sourd. Chaque créature dans une sphère de 20 pieds de rayon
centrée sur ce point doit effectuer un jet de sauvegarde de Dextérité.
La cible subit 8d6 points de dégâts de feu en cas d'échec, ou la moitié
en cas de réussite.

Le feu se propage dans les coins. Il enflamme les objets inflammables
qui se trouvent dans la zone et qui ne sont pas portés ou transportés.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts augmentent de
1d6 pour chaque niveau d'emplacement supérieur au 3e.



#### Lame de feu

*Evocation de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action bonus

**Portée :** Self

**Composantes :** V, S, M (feuille de sumac)

**Durée :** Concentration, jusqu'à 10 minutes

Vous évoquez une lame ardente dans votre main libre. La lame est
similaire en taille et en forme à un cimeterre, et elle dure toute la
durée du combat. Si vous lâchez la lame, elle disparaît, mais vous
pouvez l'évoquer à nouveau comme action bonus.

Vous pouvez utiliser votre action pour effectuer une attaque de sort en
mêlée avec la lame ardente. En cas de succès, la cible subit 3d6 points
de dégâts de feu.

La lame flamboyante diffuse une lumière vive dans un rayon de 3 mètres
et une lumière chétive sur 3 mètres supplémentaires.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts augmentent de
1d6 pour chaque tranche de deux niveaux d'emplacement au-dessus du 2e.



#### Colonne de flamme

*Evocation de 5ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composants :** V, S, M (pincée de soufre)

**Durée :** Instantané

Une colonne verticale de feu divin descend des cieux en rugissant à
l'endroit que vous indiquez. Chaque créature se trouvant dans un
cylindre de 10 pieds de rayon et de 40 pieds de haut centré sur un point
à portée doit effectuer un jet de sauvegarde de Dextérité. La créature
subit 4d6 points de dégâts de feu et 4d6 points de dégâts radiants en
cas d'échec, ou la moitié en cas de réussite.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, les dégâts de feu ou les
dégâts radiants (au choix) augmentent de 1d6 pour chaque niveau
d'emplacement supérieur au 5e.



#### Sphère de feu

*conjuration de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un peu de suif, une pincée de soufre et un
peu de poudre de fer).

**Durée :** Concentration, jusqu'à 1 minute

Une sphère de feu de 1,5 m de diamètre apparaît dans un espace inoccupé
de votre choix à portée et dure toute la durée du sort. Toute créature
qui termine son tour à moins de 1,5 m de la sphère doit effectuer un jet
de sauvegarde de Dextérité. La créature subit 2d6 dégâts de feu en cas
d'échec, ou la moitié en cas de réussite.

Comme action bonus, vous pouvez déplacer la sphère jusqu'à 30 pieds. Si
vous lancez la sphère sur une créature, celle-ci doit réussir un jet de
sauvegarde contre les dégâts de la sphère, et la sphère cesse de se
déplacer ce tour-ci.

Lorsque vous déplacez la sphère, vous pouvez la diriger au-dessus de
barrières d'une hauteur maximale de 1,5 m et la faire sauter à travers
des fosses d'une largeur maximale de 3 m. La sphère enflamme les objets
inflammables qui ne sont pas portés ou transportés, et elle diffuse une
lumière vive dans un rayon de 6 mètres et une lumière chétive sur 6
mètres supplémentaires.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts augmentent de 1d6
pour chaque niveau d'emplacement supérieur au 2e.



#### La Pétrification

*Transmutation de 6ème niveau*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (une pincée de chaux, d'eau et de terre)

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez de transformer en pierre une créature que vous pouvez voir à
portée. Si le corps de la cible est fait de chair, la créature doit
effectuer un jet de sauvegarde de Constitution. En cas d'échec, elle
est entravée et sa chair commence à durcir. En cas de réussite, la
créature n'est pas affectée.

Une créature entravée par ce sort doit effectuer un autre jet de
sauvegarde de Constitution à la fin de chacun de ses tours. Si elle
réussit trois fois son jet de sauvegarde contre ce sort, le sort prend
fin. Si elle échoue trois fois à ses jets de sauvegarde, elle est
transformée en pierre et soumise à l'état pétrifié pour la durée du
sort. Les succès et les échecs n'ont pas besoin d'être consécutifs ;
tenez compte des deux jusqu'à ce que la cible obtienne trois succès de
ce type.

Si la créature est physiquement brisée pendant qu'elle est pétrifiée,
elle souffre de déformations similaires si elle retourne à son état
d'origine.

Si vous maintenez votre concentration sur ce sort pendant toute la durée
possible, la créature est transformée en pierre jusqu'à ce que l'effet
soit supprimé.



#### Disque flottant

*Conjuration de 1er niveau (rituel)*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une goutte de mercure)

**Durée :** 1 heure

Ce sort crée un plan de force circulaire et horizontal, de 3 pieds de
diamètre et de 1 pouce d'épaisseur, qui flotte à 3 pieds au-dessus du
sol dans un espace inoccupé de votre choix que vous pouvez voir à
portée. Le disque reste en place pendant toute la durée de l'exercice,
et peut supporter jusqu'à 500 livres. Si un poids supplémentaire est
placé sur lui, le sort prend fin, et tout ce qui se trouve sur le disque
tombe au sol.

Le disque est immobile tant que vous vous trouvez à moins de 6 mètres de
lui. Si vous vous éloignez de plus de 6 mètres, le disque vous suit de
manière à rester à 6 mètres de vous. Il peut se déplacer sur un terrain
accidenté, monter ou descendre des escaliers, des pentes et autres, mais
il ne peut pas franchir un dénivelé de 3 mètres ou plus. Par exemple, le
disque ne peut pas traverser une fosse de 3 mètres de profondeur, ni en
sortir s'il a été créé au fond.

Si vous vous éloignez de plus de 30 mètres du disque (généralement parce
qu'il ne peut pas contourner un obstacle pour vous suivre), le sort
prend fin.



#### Vol

*transm transm transm transm transm transm transm transm transm transm
du 3ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une plume d'aile de n'importe quel oiseau)

**Durée :** Concentration, jusqu'à 10 minutes

Vous touchez une créature consentante. La cible gagne une vitesse de vol
de 60 pieds pour la durée du sort. Lorsque le sort prend fin, la cible
tombe si elle est toujours en l'air, à moins qu'elle ne puisse arrêter
sa chute.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
3e.



#### Nappe de brouillard

*conjuration de 1er niveau*

**Classes :** Druide,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous créez une sphère de brouillard de 20 pieds de rayon centrée sur un
point à portée. La sphère se répand dans les coins et sa zone est
fortement obscurcie. Elle dure le temps nécessaire ou jusqu'à ce qu'un
vent de vitesse modérée ou supérieure (au moins 10 miles par heure) la
disperse.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, le rayon du brouillard
augmente de 6 mètres pour chaque niveau d'emplacement supérieur au 1er.



#### Interdiction

*Abjuration de 6e niveau (rituel)*

**Classes :** Clerc

**Temps de coulée :** 10 minutes

**Portée :** Toucher

**Composantes :** V, S, M (une aspersion d'eau bénite, de l'encens
rare et du rubis en poudre d'une valeur d'au moins 1 000 gp).

**Durée :** 1 jour

Vous créez une protection contre les voyages magiques qui protège
jusqu'à 40 000 pieds carrés de surface au sol jusqu'à une hauteur de
30 pieds au-dessus du sol. Pendant toute la durée du sort, les créatures
ne peuvent pas se téléporter dans la zone ou utiliser des portails, tels
que ceux créés par le sort de *portail*, pour entrer dans la
zone. Le sort protège la zone contre les voyages planaires, et empêche
donc les créatures d'y accéder par les plans astral, éthéré, fée ou des
ombres, ou par le sort de *changement de plan*.

De plus, le sort endommage les types de créatures que vous choisissez au
moment où vous le lancez. Choisissez un ou plusieurs des types suivants
: céleste, élémentaire, fée, fiélon et mort-vivant. Lorsqu'une créature
choisie entre dans la zone du sort pour la première fois lors d'un tour
ou commence son tour à cet endroit, elle subit 5d10 dégâts radiants ou
nécrotiques (votre choix lorsque vous lancez ce sort).

Lorsque vous lancez ce sort, vous pouvez désigner un mot de passe. Une
créature qui prononce le mot de passe en entrant dans la zone ne subit
aucun dégât du sort.

La zone du sort ne peut pas chevaucher la zone d'un autre sort
d'*interdiction*. Si vous lancez *Interdiction* tous les jours pendant
30 jours au même endroit, le sort dure jusqu'à ce qu'il soit dissipé,
et les composantes matérielles sont consommées lors du dernier lancer.



#### Cage de force

*Evocation de 7ème niveau*

**Classes :** Barde,
magicien\...

**Temps d'incantation :** 1 action

**Portée :** 100 pieds

**Composantes :** V, S, M (poussière de rubis d'une valeur de 1 500 gp)

**Durée :** 1 heure

Une prison immobile, invisible, en forme de cube, composée de force
magique, prend naissance autour d'une zone que vous choisissez dans la
portée. La prison peut être une cage ou une boîte solide, à votre choix.

Une prison en forme de cage peut mesurer jusqu'à 20 pieds de côté et
est constituée de barres de 1/2 pouce de diamètre espacées de 1/2 pouce.

Une prison en forme de boîte peut s'étendre jusqu'à 10 pieds de côté,
créant une barrière solide qui empêche toute matière de la traverser et
bloquant tout sort lancé dans ou hors de la zone.

Lorsque vous lancez ce sort, toute créature qui se trouve complètement à
l'intérieur de la zone de la cage est piégée. Les créatures qui ne se
trouvent que partiellement à l'intérieur de la zone, ou celles qui sont
trop grandes pour y entrer, sont repoussées du centre de la zone
jusqu'à ce qu'elles en soient complètement sorties.

Une créature à l'intérieur de la cage ne peut pas en sortir par des
moyens non magiques. Si la créature tente d'utiliser la téléportation
ou le voyage interplanaire pour quitter la cage, elle doit d'abord
réussir un jet de sauvegarde de Charisme. En cas de réussite, la
créature peut utiliser cette magie pour sortir de la cage. En cas
d'échec, la créature ne peut pas sortir de la cage et gaspille
l'utilisation du sort ou de l'effet. La cage s'étend également dans
le plan éthéré, bloquant les voyages éthérés.

Ce sort ne peut pas être dissipé par *Dissipation de la
magie*.



#### Prémonition

*Divination de 9ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (une plume de colibri)

**Durée :** 8 heures

Vous touchez une créature consentante et lui conférez une capacité
limitée de voir dans le futur immédiat. Pendant toute la durée du sort,
la cible ne peut pas être surprise et a un avantage sur les jets
d'attaque, les contrôles de capacité et les jets de sauvegarde. De
plus, les autres créatures ont un désavantage aux jets d'attaque contre
la cible pendant la durée du sort.

Ce sort prend fin immédiatement si vous le lancez à nouveau avant la fin
de sa durée.



#### Liberté de mouvement

*4th-level abjuration*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une lanière de cuir, attachée autour du bras
ou d'un appendice similaire).

**Durée :** 1 heure

Vous touchez une créature consentante. Pendant cette durée, le mouvement
de la cible n'est pas affecté par le terrain difficile, et les sorts et
autres effets magiques ne peuvent ni réduire la vitesse de la cible, ni
la paralyser ou l'entraver.

La cible peut également dépenser 5 pieds de mouvement pour s'échapper
automatiquement d'entraves non magiques, comme des menottes ou une
créature qui l'a agrippée. Enfin, être sous l'eau n'impose aucune
pénalité sur le mouvement ou les attaques de la cible.



#### Sphère de congélation

*Evocation de 6ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 300 pieds

**Composantes :** V, S, M (une petite sphère de cristal)

**Durée :** Instantané

Un globe glacial d'énergie froide part du bout de vos doigts pour
atteindre un point de votre choix à portée, où il explose en une sphère
de 60 pieds de rayon. Chaque créature se trouvant dans cette zone doit
effectuer un jet de sauvegarde de Constitution. En cas d'échec, la
créature subit 10d6 points de dégâts de froid. Si elle réussit, elle
subit la moitié des dégâts.

Si le globe frappe une masse d'eau ou un liquide principalement composé
d'eau (à l'exception des créatures aquatiques), il gèle le liquide à
une profondeur de 15 cm sur une surface de 10 mètres carrés. Cette glace
dure pendant 1 minute. Les créatures qui nageaient à la surface de
l'eau gelée sont piégées dans la glace. Une créature piégée peut
utiliser une action pour effectuer un test de Force contre le DC de
sauvegarde de votre sort pour se libérer.

Vous pouvez vous abstenir de tirer le globe après avoir terminé le sort,
si vous le souhaitez. Un petit globe de la taille d'une pierre de
fronde, froid au toucher, apparaît dans votre main. À tout moment, vous
ou une créature à qui vous donnez le globe pouvez le lancer (jusqu'à
une portée de 12 mètres) ou le lancer avec une fronde (à la portée
normale de la fronde). Le globe se brise à l'impact, avec le même effet
que le lancement normal du sort. Vous pouvez également poser le globe
sans le faire éclater. Après 1 minute, si le globe n'a pas encore
éclaté, il explose.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 7e niveau ou plus, les dégâts augmentent de 1d6
pour chaque niveau d'emplacement supérieur au 6e.



#### Forme gazeuse

*3rd-level transmutation*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un peu de gaze et un brin de fumée)

**Durée :** Concentration, jusqu'à 1 heure

Vous transformez une créature volontaire que vous touchez, ainsi que
tout ce qu'elle porte et transporte, en un nuage brumeux pour la durée
du sort. Le sort prend fin si la créature tombe à 0 points de vie. Une
créature incorporelle n'est pas affectée.

Lorsqu'elle est sous cette forme, la cible ne peut se déplacer qu'à
une vitesse de vol de 10 pieds. La cible peut entrer et occuper
l'espace d'une autre créature. La cible a une résistance aux dégâts
non magiques, et elle a un avantage aux jets de sauvegarde de Force, de
Dextérité et de Constitution. La cible peut traverser de petits trous,
des ouvertures étroites et même de simples fissures, mais elle traite
les liquides comme s'il s'agissait de surfaces solides. La cible ne
peut pas tomber et reste en lévitation dans l'air même si elle est
étourdie ou autrement frappée d'incapacité.

Lorsqu'elle se trouve sous la forme d'un nuage brumeux, la cible ne
peut ni parler ni manipuler les objets, et les objets qu'elle portait
ou tenait ne peuvent être lâchés, utilisés ou faire l'objet d'autres
interactions. La cible ne peut pas attaquer ni lancer de sorts.



#### Portail

*conjuration de 9e niveau*

**Classes :** Clerc,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un diamant valant au moins 5 000 gp)

**Durée :** Concentration, jusqu'à 1 minute

Vous conjurez un portail reliant un espace inoccupé que vous pouvez voir
à portée à un endroit précis sur un autre plan d'existence. Le portail
est une ouverture circulaire, que vous pouvez faire de 5 à 20 pieds de
diamètre. Vous pouvez orienter le portail dans la direction de votre
choix. Le portail dure toute la durée.

Le portail a un avant et un arrière sur chaque plan où il apparaît. Le
voyage à travers le portail n'est possible qu'en passant par son
avant. Tout ce qui le fait est instantanément transporté sur l'autre
plan, apparaissant dans l'espace inoccupé le plus proche du portail.

Les divinités et autres souverains planaires peuvent empêcher les
portails créés par ce sort de s'ouvrir en leur présence ou n'importe
où dans leurs domaines.

Lorsque vous lancez ce sort, vous pouvez prononcer le nom d'une
créature spécifique (un pseudonyme, un titre ou un surnom ne fonctionne
pas). Si cette créature se trouve sur un autre plan que celui où vous
vous trouvez, le portail s'ouvre dans le voisinage immédiat de la
créature nommée et l'attire à travers lui vers l'espace inoccupé le
plus proche de votre côté du portail. Vous ne gagnez aucun pouvoir
spécial sur la créature, et elle est libre d'agir comme le MJ le juge
approprié. Elle peut partir, vous attaquer ou vous aider.



#### Quête

*Enchantement de 5ème niveau*

**Classes :** Barde,
Druide,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** 60 pieds

**Composantes :** V

**Durée :** 30 jours

Vous donnez une injonction magique à une créature que vous pouvez voir à
portée, la forçant à rendre un service ou à s'abstenir d'une action ou
d'un cours d'activité comme vous le décidez. Si la créature peut vous
comprendre, elle doit réussir un jet de sauvegarde de Sagesse ou être
charmée par vous pour la durée du sort. Tant que la créature est charmée
par vous, elle subit 5d10 dégâts psychiques chaque fois qu'elle agit de
manière directement contraire à vos instructions, mais pas plus d'une
fois par jour. Une créature qui ne peut pas vous comprendre n'est pas
affectée par le sort.

Vous pouvez donner n'importe quelle injonction, à l'exception d'une
activité qui entraînerait une mort certaine. Si vous donnez un ordre
suicidaire, le sort prend fin.

Vous pouvez mettre fin au sort plus tôt en utilisant une action pour
l'annuler. Un sort de *suppression de malédiction*, de
*restauration supérieure* ou de
*souhait* y met également fin.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e ou 8e niveau, la durée est de 1 an. Lorsque
vous lancez ce sort avec un emplacement de 9e niveau, le sort dure
jusqu'à ce qu'il soit interrompu par l'un des sorts mentionnés
ci-dessus.



#### Préservation des morts

*Nécromancie de 2ème niveau (rituel)*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une pincée de sel et une pièce de cuivre
placées sur chacun des yeux du cadavre, qui doivent y rester pendant
toute la durée du sort).

**Durée :** 10 jours

Vous touchez un cadavre ou une autre dépouille. Pour la durée du
contact, la cible est protégée de la décomposition et ne peut pas
devenir mort-vivant.

Ce sort prolonge également de manière efficace la limite de temps pour
ressusciter la cible, puisque les jours passés sous l'influence de ce
sort ne comptent pas dans la limite de temps des sorts tels que
*ressusciter les morts*.



#### Insecte géant

*transm transm transm transm transm transm transm transm transm transm
du 4ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Vous transformez jusqu'à dix mille-pattes, trois araignées, cinq guêpes
ou un scorpion à portée en versions géantes de leurs formes naturelles
pour la durée. Un mille-pattes devient un mille-pattes géant, une
araignée devient une araignée géante, une guêpe devient une guêpe
géante, et un scorpion devient un scorpion géant.

Chaque créature obéit à vos ordres verbaux, et en combat, elle agit à
votre tour à chaque tour. Le MJ dispose des statistiques de ces
créatures et résout leurs actions et leurs mouvements.

Une créature conserve sa taille de géant pendant toute la durée de
l'opération, jusqu'à ce qu'elle tombe à 0 points de vie, ou jusqu'à
ce que vous utilisiez une action pour annuler l'effet sur elle.

Le MJ peut vous permettre de choisir des cibles différentes. Par
exemple, si vous transformez une abeille, sa version géante pourrait
avoir les mêmes statistiques qu'une guêpe géante.



#### Bagou

*Transmutation de 8ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V

**Durée :** 1 heure

Jusqu'à la fin du sort, lorsque vous effectuez un test de Charisme,
vous pouvez remplacer le chiffre obtenu par un 15. De plus, peu importe
ce que vous dites, la magie qui déterminerait si vous dites la vérité
indique que vous êtes sincère.



#### Globe d'invulnérabilité

*6e niveau d'abjuration*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (rayon de 10 pieds)

**Composantes :** V, S, M (une perle de verre ou de cristal qui se brise
à la fin du sort).

**Durée :** Concentration, jusqu'à 1 minute

Une barrière immobile, faiblement chatoyante, prend naissance dans un
rayon de 3 mètres autour de vous et reste en place pendant toute la
durée de l'opération.

Tout sort de 5e niveau ou moins lancé depuis l'extérieur de la barrière
ne peut pas affecter les créatures ou les objets qui s'y trouvent, même
si le sort est lancé en utilisant un emplacement de sort de niveau
supérieur. Un tel sort peut cibler les créatures et les objets à
l'intérieur de la barrière, mais le sort n'a aucun effet sur eux. De
même, la zone située à l'intérieur de la barrière est exclue des zones
affectées par de tels sorts.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau ou plus, la barrière bloque les
sorts d'un niveau supérieur pour chaque niveau d'emplacement au-dessus
du 6e.



#### Glyphe de protection

*3rd-level abjuration*

**Classes :** Barde,
Magicien

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (encens et poudre de diamant d'une valeur
d'au moins 200 gp, que le sort consomme)

**Durée :** Jusqu'à ce qu'il soit dissipé ou déclenché

Lorsque vous lancez ce sort, vous inscrivez un glyphe qui nuit aux
autres créatures, soit sur une surface (comme une table ou une section
de sol ou de mur), soit à l'intérieur d'un objet qui peut être fermé
(comme un livre, un parchemin ou un coffre à trésor) pour dissimuler le
glyphe. Si vous choisissez une surface, le glyphe peut couvrir une zone
de la surface ne dépassant pas 10 pieds de diamètre. Si vous choisissez
un objet, celui-ci doit rester à sa place ; si l'objet est déplacé à
plus de 3 mètres de l'endroit où vous lancez ce sort, le glyphe est
brisé et le sort prend fin sans être déclenché.

Le glyphe est presque invisible et nécessite un test d'Intelligence
(Investigation) réussi contre votre DC de sauvegarde contre les sorts
pour être trouvé.

Vous décidez de ce qui déclenche le glyphe lorsque vous lancez le sort.
Pour les glyphes inscrits sur une surface, les déclencheurs les plus
typiques incluent le fait de toucher ou de se tenir sur le glyphe,
d'enlever un autre objet couvrant le glyphe, de s'approcher à une
certaine distance du glyphe, ou de manipuler l'objet sur lequel le
glyphe est inscrit. Pour les glyphes inscrits dans un objet, les
déclencheurs les plus communs sont l'ouverture de l'objet, l'approche
à une certaine distance de l'objet, la vue ou la lecture du glyphe. Une
fois qu'un glyphe est déclenché, ce sort prend fin.

Vous pouvez affiner le déclencheur pour que le sort ne s'active que
dans certaines circonstances ou en fonction de caractéristiques
physiques (comme la taille ou le poids), du type de créature (par
exemple, le pupille pourrait être réglé pour affecter les aberrations ou
les Drow) ou de l'alignement. Vous pouvez également définir des
conditions pour les créatures qui ne déclenchent pas le glyphe, comme
celles qui prononcent un certain mot de passe.

Lorsque vous inscrivez le glyphe, choisissez des runes explosives ou un
glyphe de sort.

***Runes explosives.*** Lorsqu'il est déclenché, le glyphe fait
éruption d'énergie magique dans une sphère de 20 pieds de rayon centrée
sur le glyphe. La sphère se propage autour des coins. Chaque créature
dans la zone doit effectuer un jet de sauvegarde de Dextérité. Une
créature subit 5d8 dégâts d'acide, de froid, de feu, de foudre ou de
tonnerre en cas d'échec au jet de sauvegarde (votre choix au moment de
la création de la glyphe), ou la moitié des dégâts en cas de réussite.

***Glyphe de sort.*** Vous pouvez stocker un sort préparé de 3e niveau
ou moins dans la glyphe en le lançant dans le cadre de la création de la
glyphe. Le sort doit cibler une seule créature ou une zone. Le sort
stocké n'a pas d'effet immédiat lorsqu'il est lancé de cette façon.
Lorsque la glyphe est déclenchée, le sort stocké est lancé. Si le sort a
une cible, il vise la créature qui a déclenché le glyphe. Si le sort
affecte une zone, celle-ci est centrée sur cette créature. Si le sort
invoque des créatures hostiles ou crée des objets ou des pièges
nuisibles, ils apparaissent aussi près que possible de l'intrus et
l'attaquent. Si le sort nécessite une concentration, il dure jusqu'à
la fin de sa durée complète.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts d'une glyphe de
runes explosives augmentent de 1d8 pour chaque niveau d'emplacement
supérieur au 3e. Si vous créez une glyphe de sort, vous pouvez stocker
n'importe quel sort jusqu'au même niveau que l'emplacement que vous
utilisez pour la *glyphe de protection*.



#### Baies nourricières

*Trans transmutation de 1er niveau*

**Classe :** Druide

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un brin de gui)

**Durée :** Instantané

Jusqu'à dix baies apparaissent dans votre main et sont infusées de
magie pour la durée. Une créature peut utiliser son action pour manger
une baie. Manger une baie redonne 1 point de vie, et la baie fournit
assez de nourriture pour soutenir une créature pendant un jour.

Les baies perdent leur puissance si elles n'ont pas été consommées dans
les 24 heures suivant le lancement de ce sort.



#### Graisse

*conjuration de 1er niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un peu de couenne de porc ou de beurre)

**Durée :** 1 minute

De la graisse glissante couvre le sol dans un carré de 3 mètres de côté
centré sur un point à portée et le transforme en terrain difficile pour
la durée de l'opération.

Lorsque la graisse apparaît, chaque créature se trouvant dans sa zone
doit réussir un jet de sauvegarde de Dextérité ou tomber à terre. Une
créature qui entre dans la zone ou y termine son tour doit également
réussir un jet de sauvegarde de Dextérité ou tomber à plat ventre.



#### Invisibilité supérieure

*illusion de 4 de niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous ou une créature que vous touchez devenez invisible jusqu'à la fin
du sort. Tout ce que la cible porte ou transporte est invisible tant
qu'il est sur elle.



#### Restauration supérieure

*ab ab abjuration de niveau 5*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (poussière de diamant d'une valeur d'au
moins 100 gp, que le sort consomme).

**Durée :** Instantané

Vous imprégnez une créature que vous touchez d'énergie positive pour
annuler un effet débilitant. Vous pouvez réduire le niveau d'épuisement
de la cible de un, ou mettre fin à l'un des effets suivants sur la
cible :

-   Un effet qui a charmé ou pétrifié la cible.
-   Une malédiction, y compris le fait que la cible soit
    liée à un objet magique maudit.
-   Toute réduction d'une des valeurs de caractéristique de la cible.
-   Un effet qui réduit le nombre maximum de points de vie de la cible.



#### Gardien de la foi

*conjuration de 4ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V

**Durée :** 8 heures

Un Grand gardien spectral apparaît et plane pendant toute la durée du
sort dans un espace inoccupé de votre choix que vous pouvez voir à
portée. Le gardien occupe cet espace et est indistinct à l'exception
d'une épée et d'un bouclier étincelants portant le symbole de votre
divinité.

Toute créature hostile à votre égard qui se déplace dans un espace situé
à moins de 3 mètres du gardien pour la première fois pendant un tour
doit réussir un jet de sauvegarde de Dextérité. La créature subit 20
points de dégâts radiants en cas d'échec, ou la moitié en cas de
réussite. Le gardien disparaît lorsqu'il a infligé un total de 60
points de dégâts.



#### Protections et sceaux

*6e niveau d'abjuration*

**Classes :** Barde

**Temps de coulée :** 10 minutes

**Portée :** Toucher

**Composantes :** V, S, M (encens brûlant, une petite mesure de soufre
et d'huile, une ficelle nouée, une petite quantité de sang de hulk
crochu et une petite tige d'argent valant au moins 10 gp).

**Durée :** 24 heures

Vous créez un quartier qui protège jusqu'à 2 500 pieds carrés de
surface au sol (une surface de 50 pieds carrés, ou cent carrés de 5
pieds ou vingt-cinq carrés de 10 pieds). La zone protégée peut mesurer
jusqu'à 6 mètres de haut et avoir la forme que vous souhaitez. Vous
pouvez protéger plusieurs étages d'une forteresse en divisant la zone
entre eux, à condition que vous puissiez entrer dans chaque zone
contiguë pendant que vous lancez le sort.

Lorsque vous lancez ce sort, vous pouvez spécifier des individus qui ne
sont pas affectés par un ou tous les effets que vous choisissez. Vous
pouvez également spécifier un mot de passe qui, lorsqu'il est prononcé
à haute voix, rend la personne qui le prononce immunisée contre ces
effets.

Les protections*et sceaux* créent les effets
suivants dans la zone surveillée.

***Couloirs.*** Le brouillard remplit tous les couloirs gardés, les
rendant fortement obscurcis. De plus, à chaque intersection ou
bifurcation offrant un choix de direction, il y a 50 % de chances
qu'une créature autre que vous croie aller dans la direction opposée à
celle qu'elle choisit.

***Les portes.*** Toutes les portes de la zone surveillée sont
verrouillées magiquement, comme si elles étaient scellées par un sort de
*verrouillage arcanique*. De plus, vous pouvez couvrir
jusqu'à dix portes d'une illusion (équivalente à la fonction objet
illusoire du sort *illusion mineure* pour les faire
apparaître comme de simples pans de mur.

***Escaliers.*** Les toiles remplissent tous les escaliers de la zone
gardée de haut en bas, comme le sort *toile*. Ces brins
repoussent en 10 minutes s'ils sont brûlés ou arrachés pendant la durée
des *protections et s* ceaux.

***Autre effet magique.*** Vous pouvez placer l'un des effets magiques
suivants de votre choix dans la zone gardée de la forteresse.

-   Placez des *lumières dansantes* dans quatre
    couloirs. Vous pouvez désigner un programme simple selon lequel les
    lumières se répètent tant que durent les *gardes et les
    sceaux*.
-   Placez la *bouche magique* à deux endroits.
-   Placez le nuage *nauséabond* à deux endroits. Les
    vapeurs apparaissent aux endroits que vous désignez ; elles
    reviennent dans les 10 minutes si elles sont dispersées par le vent
    pendant que durent les *protections et les s*
    ceaux.
-   Placez une *rafale de vent* constante dans un
    couloir ou une pièce.
-   Placez une *suggestion* dans un endroit. Vous
    sélectionnez une zone d'un maximum de 1,5 mètre carré, et toute
    créature qui entre ou traverse la zone reçoit la suggestion
    mentalement.

L'ensemble de la zone surveillée rayonne de magie. Une *dissipation de
la magie* lancée sur un effet spécifique, si elle
réussit, ne supprime que cet effet.

Vous pouvez créer une structure gardée et scellée en permanence en
lançant ce sort tous les jours pendant un an.



#### Assistance

*Tour de magie de divination*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous touchez une créature consentante. Une fois avant la fin du sort, la
cible peut lancer un d4 et ajouter le nombre obtenu à un test de
capacité de son choix. Elle peut lancer le dé avant ou après avoir
effectué le test de capacité. Le sort prend alors fin.



#### Éclair traçant

*Evocation de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** 1 round

Un éclair de lumière se dirige vers une créature de votre choix à
portée. Effectuez une attaque de sort à distance contre la cible. En cas
de succès, la cible subit 4d6 dégâts radiants, et le prochain jet
d'attaque effectué contre cette cible avant la fin de votre prochain
tour a un avantage, grâce à la lumière chétive mystique qui scintille
sur la cible jusque-là.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les dégâts augmentent de 1d6
pour chaque niveau d'emplacement supérieur au 1er.



#### Bourrasque de vent

*Evocation de 2ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Soi-même (ligne de 60 pieds)

**Composantes :** V, S, M (une graine de légumineuse)

**Durée :** Concentration, jusqu'à 1 minute

Une ligne de vent fort de 60 pieds de long et 10 pieds de large souffle
de vous dans la direction que vous choisissez pour la durée du sort.
Chaque créature qui commence son tour dans la ligne doit réussir un jet
de sauvegarde de Force ou être poussée à 15 pieds de vous dans une
direction qui suit la ligne.

Toute créature dans la ligne doit dépenser 2 pieds de mouvement pour
chaque 1 pied qu'elle déplace pour se rapprocher de vous.

La rafale disperse le gaz ou la vapeur, et elle éteint les bougies, les
torches et les flammes non protégées similaires dans la zone. Elle fait
danser sauvagement les flammes protégées, comme celles des lanternes, et
a 50 % de chances de les éteindre.

En tant qu'action bonus à chacun de vos tours avant que le sort ne
prenne fin, vous pouvez changer la direction dans laquelle le trait
s'éloigne de vous.



#### Sanctification

*Evocation de 5ème niveau*

**Classes :** Clerc

**Temps de coulée :** 24 heures

**Portée :** Toucher

**Composantes :** V, S, M (herbes, huiles et encens d'une valeur d'au
moins 1 000 gp, que le sort consomme).

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous touchez un point et infusez une zone autour de lui avec une
puissance sainte (ou impie). La zone peut avoir un rayon allant jusqu'à
60 pieds, et le sort échoue si le rayon inclut une zone déjà sous
l'effet d'un sort de *sanctification*. La zone affectée est
soumise aux effets suivants.

D'abord, les célestes, les élémentaires, les fées, les fiélons et les
morts-vivants ne peuvent pas entrer dans la zone, et ces créatures ne
peuvent pas non plus charmer, effrayer ou posséder les créatures qui
s'y trouvent. Toute créature charmée, effrayée ou possédée par une
telle créature n'est plus charmée, effrayée ou possédée en entrant dans
la zone. Vous pouvez exclure un ou plusieurs de ces types de créatures
de cet effet.

Deuxièmement, vous pouvez lier un effet supplémentaire à la zone.
Choisissez l'effet dans la liste suivante, ou choisissez un effet
proposé par le MJ. Certains de ces effets s'appliquent aux créatures de
la zone ; vous pouvez indiquer si l'effet s'applique à toutes les
créatures, aux créatures qui suivent une divinité ou un chef spécifique,
ou aux créatures d'un type particulier, comme les orcs ou les trolls.
Lorsqu'une créature qui serait affectée entre dans la zone du sort pour
la première fois lors d'un tour ou commence son tour à cet endroit,
elle peut effectuer un jet de sauvegarde de Charisme. En cas de
réussite, la créature ignore l'effet supplémentaire jusqu'à ce
qu'elle quitte la zone.

***Courage.*** Les créatures affectées ne peuvent pas être effrayées
tant qu'elles se trouvent dans la zone.

***Ténèbres.*** Les Ténèbres remplissent la zone. La lumière normale,
ainsi que la lumière magique créée par des sorts d'un niveau inférieur
à celui de l'emplacement que vous avez utilisé pour lancer ce sort, ne
peuvent pas éclairer la zone.

***Lumière du jour.*** Une lumière vive remplit la zone. Les ténèbres
magiques créées par des sorts d'un niveau inférieur à celui de
l'emplacement que vous avez utilisé pour lancer ce sort ne peuvent pas
éteindre la lumière.

***Protection contre l'énergie.*** Les créatures touchées dans la zone
ont une résistance à un type de dégâts de votre choix, à l'exception
des coups de matraque, des perforants et des tranchants.

***Vulnérabilité énergétique.*** Les créatures touchées dans la zone
sont vulnérables à un type de dégâts de votre choix, à l'exception des
coups de matraque, des perforants et des tranchants.

***Le repos éternel.*** Les cadavres enterrés dans cette zone ne peuvent
pas être transformés en morts-vivants.

***Interférence extradimensionnelle.*** Les créatures affectées ne
peuvent pas se déplacer ou voyager en utilisant la téléportation ou par
des moyens extradimensionnels ou interplanaires.

***Peur.*** Les créatures touchées sont effrayées lorsqu'elles se
trouvent dans la zone.

***Silence.*** Aucun son ne peut émaner de cette zone, et aucun son ne
peut y pénétrer.

***Don des langues.*** Les créatures affectées peuvent communiquer avec
n'importe quelle autre créature dans la zone, même si elles ne
partagent pas une langue commune.



#### Terrain halluciné

*illusion de 4 de niveau*

**Classes :** Barde,
Warlock

**Temps de coulée :** 10 minutes

**Portée :** 300 pieds

**Composantes :** V, S, M (une pierre, une brindille, et un peu de
plante verte)

**Durée :** 24 heures

Vous faites en sorte que le terrain naturel situé dans un cube de 150
pieds de portée ait l'apparence, le son et l'odeur d'un autre type de
terrain naturel. Ainsi, des champs ouverts ou une route peuvent
ressembler à un marécage, une colline, une crevasse ou tout autre
terrain difficile ou infranchissable. Un étang peut ressembler à une
prairie herbeuse, un précipice à une pente douce, ou un ravin rocheux à
une route large et lisse. Les structures, équipements et créatures
fabriqués dans la zone ne sont pas modifiés en apparence.

Les caractéristiques tactiles du terrain sont inchangées, de sorte que
les créatures qui pénètrent dans la zone sont susceptibles de voir à
travers l'illusion. Si la différence n'est pas évidente au toucher,
une créature examinant attentivement l'illusion peut tenter un test
d'Intelligence (Investigation) contre votre DC de sauvegarde contre les
sorts pour ne pas la croire. Une créature qui discerne l'illusion pour
ce qu'elle est, la voit comme une vague image superposée au terrain.



#### Contamination

*Nécromancie de 6ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous déclenchez une maladie virulente sur une créature que vous pouvez
voir à portée. La cible doit effectuer un jet de sauvegarde de
Constitution. En cas d'échec, elle subit 14d6 points de dégâts
nécrotiques, ou la moitié en cas de réussite. Les dégâts ne peuvent pas
ramener les points de vie de la cible en dessous de 1. Si la cible
échoue au jet de sauvegarde, son maximum de points de vie est réduit
pendant 1 heure d'un montant égal aux dégâts nécrotiques qu'elle a
subis. Tout effet qui supprime une maladie permet au maximum de points
de vie d'une créature de revenir à la normale avant la fin de cette
période.



#### Hâte

*3rd-level transmutation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un rasage de racine de réglisse)

**Durée :** Concentration, jusqu'à 1 minute

Choisissez une créature consentante que vous pouvez voir à portée.
Jusqu'à la fin du sort, la vitesse de la cible est doublée, elle
bénéficie d'un bonus de +2 à la CA, elle a l'avantage aux jets de
sauvegarde de Dextérité, et elle bénéficie d'une action supplémentaire
à chacun de ses tours. Cette action ne peut être utilisée que pour
effectuer les actions Attaquer (une seule attaque d'arme), Foncer, Se
désengager, Se cacher ou Utiliser un objet.

Lorsque le sort prend fin, la cible ne peut ni bouger ni agir avant son
prochain tour, car une vague de léthargie l'envahit.



#### Guérison

*Evocation de 6ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Choisissez une créature que vous pouvez voir à portée. Une vague
d'énergie positive traverse la créature et lui fait regagner 70 points
de vie. Ce sort met également fin à la cécité, à la surdité et à toute
maladie affectant la cible. Ce sort n'a aucun effet sur les
constructions ou les morts-vivants.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau ou plus, la quantité de soins
augmente de 10 pour chaque niveau d'emplacement supérieur au 6e.



#### Mot de guérison

*Evocation de 1er niveau*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 action bonus

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Une créature de votre choix que vous pouvez voir à portée regagne un
nombre de points de vie égal à 1d4 + votre modificateur de
caractéristique d'incantation. Ce sort n'a aucun effet sur les
morts-vivants ou les constructions.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les soins augmentent de 1d4
pour chaque niveau d'emplacement supérieur au 1er.



#### Métal brûlant

*transmutation de 2ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un morceau de fer et une flamme)

**Durée :** Concentration, jusqu'à 1 minute

Choisissez un objet en métal manufacturé, comme une arme en métal ou une
armure en métal lourd ou moyen, que vous pouvez voir à portée. Vous
faites briller l'objet d'une lueur rougeoyante. Toute créature en
contact physique avec l'objet subit 2d8 points de dégâts de feu lorsque
vous lancez le sort. Jusqu'à ce que le sort prenne fin, vous pouvez
utiliser une action bonus à chacun de vos tours suivants pour causer à
nouveau ces dégâts.

Si une créature tient ou porte l'objet et en subit les dégâts, elle
doit réussir un jet de sauvegarde de Constitution ou lâcher l'objet si
elle le peut. Si elle ne lâche pas l'objet, elle a un désavantage aux
jets d'attaque et aux tests de capacité jusqu'au début de votre
prochain tour.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 2e.



#### Représailles infernales

*Evocation de 1er niveau*

**Classes :** Invocateur

**Temps d'incantation :** 1 réaction, que vous prenez en réponse à des
dommages causés par une créature que vous pouvez voir et qui se trouve à
moins de 60 pieds de vous.

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous pointez votre doigt, et la créature qui vous a endommagé est
momentanément entourée de flammes infernales. La créature doit effectuer
un jet de sauvegarde de Dextérité. Elle subit 2d10 points de dégâts de
feu en cas d'échec, ou la moitié en cas de réussite.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les dégâts augmentent de 1d10
pour chaque niveau d'emplacement supérieur au 1er.



#### Festin des héros

*Conjuration de 6ème niveau*

**Classes :** Clerc

**Temps de coulée :** 10 minutes

**Portée :** 30 pieds

**Composantes :** V, S, M (un bol incrusté de pierres précieuses d'une
valeur d'au moins 1 000 gp, que le sort consomme).

**Durée :** Instantané

Vous faites apparaître un grand festin, comprenant des mets et des
boissons magnifiques. Le festin prend 1 heure à consommer et disparaît à
la fin de cette période, et les effets bénéfiques ne se manifestent pas
avant la fin de cette heure. Jusqu'à douze autres créatures peuvent
prendre part au festin.

Une créature qui participe au festin gagne plusieurs avantages. La
créature est guérie de toutes les maladies et du poison, devient
immunisée au poison et à l'effroi, et effectue tous les jets de
sauvegarde de Sagesse avec avantage. Son maximum de points de vie
augmente également de 2d10, et elle gagne le même nombre de points de
vie. Ces avantages durent 24 heures.



#### Héroïsme

*Enchantement de 1er niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Une créature volontaire que vous touchez est imprégnée de bravoure.
Jusqu'à la fin du sort, la créature est immunisée contre l'effroi et
gagne des points de vie temporaires égaux à votre modificateur de
caractéristique d'incantation au début de chacun de ses tours. Lorsque
le sort prend fin, la cible perd tous les points de vie temporaires
restants de ce sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er.



#### Rire hideux

*Enchantement de 1er niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (de très petites tartes et une plume que l'on
agite en l'air).

**Durée :** Concentration, jusqu'à 1 minute

Une créature de votre choix que vous pouvez voir à portée perçoit tout
comme hilarant et tombe dans des crises de rire si ce sort l'affecte.
La cible doit réussir un jet de sauvegarde de Sagesse ou tomber à plat
ventre, devenant ainsi incapable de se relever pour la durée du sort.
Une créature ayant un score d'Intelligence de 4 ou moins n'est pas
affectée.

À la fin de chacun de ses tours, et chaque fois qu'elle subit des
dégâts, la cible peut effectuer un autre jet de sauvegarde de Sagesse.
La cible a l'avantage au jet de sauvegarde s'il est déclenché par des
dégâts. En cas de réussite, le sort prend fin.



#### Immobilisation de monstre

*Enchantement de 5e niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S, M (un petit morceau de fer droit)

**Durée :** Concentration, jusqu'à 1 minute

Choisissez une créature que vous pouvez voir à portée. La cible doit
réussir un jet de sauvegarde de Sagesse ou être paralysée pour la durée
du sort. Ce sort n'a aucun effet sur les morts-vivants. À la fin de
chacun de ses tours, la cible peut effectuer un autre jet de sauvegarde
de Sagesse. En cas de réussite, le sort prend fin pour la cible.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
5e. Les créatures doivent se trouver à moins de 10 mètres l'une de
l'autre lorsque vous les ciblez.



#### Immobilisation de personne

*Enchantement de 2ème niveau*

**Classes :** Barde,
Druide, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un petit morceau de fer droit)

**Durée :** Concentration, jusqu'à 1 minute

Choisissez un humanoïde que vous pouvez voir à portée. La cible doit
réussir un jet de sauvegarde de Sagesse ou être paralysée pour la durée
du jet. À la fin de chacun de ses tours, la cible peut effectuer un
autre jet de sauvegarde de Sagesse. En cas de réussite, le sort prend
fin pour la cible.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, vous pouvez cibler un
humanoïde supplémentaire pour chaque niveau d'emplacement supérieur au
2e. Les humanoïdes doivent se trouver à moins de 10 mètres les uns des
autres lorsque vous les ciblez.



#### Aura sacrée

*abjuration de 8ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (un minuscule reliquaire d'au moins 1 000 gp
contenant une relique sacrée, comme un morceau de tissu de la robe d'un
saint ou un morceau de parchemin d'un texte religieux).

**Durée :** Concentration, jusqu'à 1 minute

La lumière divine s'échappe de vous et se fond en un doux rayonnement
dans un rayon de 30 pieds autour de vous. Les créatures de votre choix
se trouvant dans ce rayon quand vous lancez ce sort répandent une
lumière chétive dans un rayon de 1,5 m et ont l'avantage à tous les
jets de sauvegarde, et les autres créatures ont un désavantage aux jets
d'attaque contre elles jusqu'à la fin du sort. De plus, lorsqu'un
fiélon ou un mort-vivant touche une créature affectée avec une attaque
de mêlée, l'aura se met à clignoter avec une lumière brillante.
L'attaquant doit réussir un jet de sauvegarde de Constitution ou être
aveuglé jusqu'à la fin du sort.



#### Marque du chasseur

*Divination de 1er niveau*

**Classes :** Rôdeur

**Temps d'incantation :** 1 action bonus

**Portée :** 90 pieds

**Composantes :** V

**Durée :** Concentration, jusqu'à 1 heure

Vous choisissez une créature que vous pouvez voir à portée et la marquez
mystiquement comme votre proie. Jusqu'à la fin du sort, vous infligez
1d6 points de dégâts supplémentaires à la cible chaque fois que vous la
touchez avec une attaque d'arme, et vous avez l'avantage sur tous les
tests de Sagesse (Perception) ou de Survie (Survie) que vous effectuez
pour la retrouver. Si la cible tombe à 0 point de vie avant la fin de ce
sort, vous pouvez utiliser une action bonus lors d'un de vos tours
suivants pour marquer une nouvelle créature.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e ou 4e niveau, vous pouvez maintenir votre
concentration sur le sort jusqu'à 8 heures. Lorsque vous utilisez un
emplacement de sort de 5e niveau ou plus, vous pouvez maintenir votre
concentration sur le sort jusqu'à 24 heures.



#### Motif hypnotique

*Ill illusion de 3 de niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** S, M (un bâton d'encens incandescent ou une fiole de
cristal remplie de matière phosphorescente).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez un motif de couleurs virevoltant qui se faufile dans l'air à
l'intérieur d'un cube de 10 mètres de côté à portée. Le motif apparaît
un instant puis disparaît. Chaque créature dans la zone qui voit le
motif doit effectuer un jet de sauvegarde de Sagesse. En cas d'échec,
la créature est charmée pour la durée du sort. Lorsqu'elle est charmée
par ce sort, la créature est frappée d'incapacité et sa vitesse est de
0.

Le sort prend fin pour une créature affectée si elle subit des dégâts ou
si quelqu'un d'autre utilise une action pour secouer la créature hors
de sa stupeur.



#### Tempête de grêle

*év évocation de 4ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 300 pieds

**Composantes :** V, S, M (une pincée de poussière et quelques gouttes
d'eau)

**Durée :** Instantané

Une grêle de glace dure comme de la pierre s'abat sur le sol dans un
cylindre de 20 pieds de rayon et de 40 pieds de haut centré sur un point
à portée. Chaque créature dans le cylindre doit effectuer un jet de
sauvegarde de Dextérité. La créature subit 2d8 points de dégâts de
contondant et 4d6 points de dégâts de froid si le jet de sauvegarde
échoue, ou la moitié des dégâts si le jet est réussi.

Les grêlons transforment la zone d'effet de la tempête en terrain
difficile jusqu'à la fin de votre prochain tour.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 5e niveau ou plus, les dégâts de matraquage
augmentent de 1d8 pour chaque niveau d'emplacement supérieur au 4e.



#### Identification

*Divination de 1er niveau (rituel)*

**Classes :** Barde

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (une perle valant au moins 100 gp et une plume
de chouette)

**Durée :** Instantané

Vous choisissez un objet que vous devez toucher pendant toute la durée
du sort. S'il s'agit d'un objet magique ou d'un autre objet imprégné
de magie, vous apprenez ses propriétés et comment les utiliser, s'il
faut l'accorder pour l'utiliser et combien de charges il possède, le
cas échéant. Vous apprenez si des sorts affectent l'objet et quels sont
ces sorts. Si l'objet a été créé par un sort, vous apprenez quel sort
l'a créé.

Si, à la place, vous touchez une créature pendant l'incantation, vous
apprenez quels sorts, le cas échéant, l'affectent actuellement.



#### Texte illusoire

*Illusion de 1er niveau (rituel)*

**Classes :** Barde,
magicien\...

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** S, M (une encre à base de plomb d'une valeur d'au
moins 10 gp, que le sort consomme).

**Durée :** 10 jours

Vous écrivez sur du parchemin, du papier ou tout autre support
d'écriture approprié et l'imprégnez d'une puissante illusion qui dure
toute la durée de l'opération.

Pour vous et toutes les créatures que vous désignez lorsque vous lancez
le sort, l'écriture semble normale, écrite de votre main, et transmet
le sens que vous vouliez donner au texte. Pour tous les autres,
l'écriture apparaît comme si elle était écrite dans une écriture
inconnue ou magique qui est inintelligible. Vous pouvez également faire
en sorte que l'écriture apparaisse comme un message entièrement
différent, écrit d'une autre main et dans une autre langue, mais cette
dernière doit être une langue que vous connaissez.

Si le sort est dissipé, le scénario original et l'illusion
disparaissent tous deux.

Une créature dotée de la vision véritable peut lire le message caché.



#### Emprisonnement

*9th-level abjuration*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S, M (une représentation en vélin ou une statuette
sculptée à l'effigie de la cible, et une composante spéciale qui varie
en fonction de la version du sort choisie, d'une valeur d'au moins 500
gp par Hit Die de la cible).

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous créez une contrainte magique pour retenir une créature que vous
pouvez voir à portée. La cible doit réussir un jet de sauvegarde de
Sagesse ou être liée par le sort ; si elle réussit, elle est immunisée
contre ce sort si vous le lancez à nouveau. Pendant qu'elle est
affectée par ce sort, la créature n'a pas besoin de respirer, de manger
ou de boire, et elle ne vieillit pas. Les sorts de Divination ne peuvent
pas localiser ou percevoir la cible.

Lorsque vous lancez ce sort, vous choisissez l'une des formes
d'emprisonnement suivantes.

***Enterrement.*** La cible est ensevelie loin sous terre dans une
sphère de force magique juste assez grande pour la contenir. Rien ne
peut passer à travers la sphère, et aucune créature ne peut se
téléporter ou utiliser le voyage planaire pour y entrer ou en sortir.

La composante spéciale de cette version du sort est un petit orbe en
mithral.

***Enchaînement.*** De lourdes chaînes, solidement ancrées dans le sol,
maintiennent la cible en place. La cible est entravée jusqu'à la fin du
sort, et elle ne peut ni bouger ni être déplacée par quelque moyen que
ce soit jusque-là.

La composante spéciale de cette version du sort est une fine chaîne en
métal précieux.

***Prison couverte.*** Ce sort transporte la cible dans un minuscule
Demi-plan qui est protégé contre la téléportation et le voyage planaire.
Le Demi-plan peut être un labyrinthe, une cage, une tour ou toute autre
structure ou zone confinée de votre choix.

La composante spéciale de cette version du sort est une représentation
miniature de la prison en jade.

***Confinement Minimus.*** La cible rétrécit à une hauteur de 1 pouce et
est emprisonnée dans une pierre précieuse ou un objet similaire. La
lumière peut traverser la pierre précieuse normalement (permettant à la
cible de voir à l'extérieur et aux autres créatures de voir à
l'intérieur), mais rien d'autre ne peut passer, même par des moyens de
téléportation ou de voyage planaire. La pierre précieuse ne peut pas
être coupée ou brisée tant que le sort reste en vigueur.

La composante spéciale de cette version du sort est une grande pierre
précieuse transparente, comme un corindon, un diamant ou un rubis.

***Sommeil.*** La cible s'endort et ne peut pas être réveillée. La
composante spéciale de cette version du sort est constituée d'herbes
soporifiques rares.

***Mettre fin au sort.*** Pendant l'incantation du sort, dans
n'importe laquelle de ses versions, vous pouvez spécifier une condition
qui entraînera la fin du sort et la libération de la cible. L'étât peut
être aussi spécifique ou aussi élaboré que vous le souhaitez, mais le MJ
doit accepter que l'étât soit raisonnable et ait une probabilité de se
réaliser. Les conditions peuvent être basées sur le nom, l'identité ou
la divinité d'une créature, mais doivent être basées sur des actions ou
des qualités observables et non sur des éléments intangibles comme le
niveau, la classe ou les points de vie.

Un sort de *dissipation de la magie* ne peut mettre fin
au sort que s'il est lancé comme un sort de 9e niveau, ciblant soit la
prison, soit le composant spécial utilisé pour la créer.

Vous pouvez utiliser une composante spéciale particulière pour créer une
seule prison à la fois. Si vous lancez à nouveau le sort en utilisant le
même composant, la cible de la première incantation est immédiatement
libérée de son lien.



#### Nuage incendiaire

*Conjuration de 8ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Un nuage de fumée tourbillonnant traversé de braises chauffées à blanc
apparaît dans une sphère de 20 pieds de rayon centrée sur un point à
portée. Le nuage se répand dans les coins et est fortement obscurci. Il
dure le temps nécessaire ou jusqu'à ce qu'un vent de vitesse modérée
ou supérieure (au moins 10 miles par heure) le disperse.

Lorsque le nuage apparaît, chaque créature qui s'y trouve doit
effectuer un jet de sauvegarde de Dextérité. La créature subit 10d8
points de dégâts de feu en cas d'échec, ou la moitié en cas de
réussite. Une créature doit également effectuer ce jet de sauvegarde
lorsqu'elle entre dans la zone du sort pour la première fois lors d'un
tour ou qu'elle y termine son tour.

Le nuage s'éloigne de 10 pieds directement de vous dans une direction
que vous choisissez au début de chacun de vos tours.



#### Blessures infligées

*Nécromancie de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

Effectuez une attaque de sort en mêlée contre une créature que vous
pouvez atteindre. En cas de succès, la cible subit 3d10 points de dégâts
nécrotiques.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les dégâts augmentent de 1d10
pour chaque niveau d'emplacement supérieur au 1er.



#### Fléau d'insectes

*conjuration de 5e niveau*

**Classes :** Clerc,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 300 pieds

**Composantes :** V, S, M (quelques grains de sucre, quelques grains de
céréales, et une tache de graisse)

**Durée :** Concentration, jusqu'à 10 minutes

Des criquets grouillants et mordants remplissent une sphère de 20 pieds
de rayon centrée sur un point que vous choisissez à portée. La sphère se
propage autour des coins. La sphère reste en place pendant toute la
durée du sort, et sa zone est légèrement obscurcie. La zone de la sphère
est un terrain difficile.

Lorsque la zone apparaît, chaque créature qui s'y trouve doit effectuer
un jet de sauvegarde de Constitution. La créature subit 4d10 points de
dégâts perforants en cas d'échec, ou la moitié en cas de réussite. Une
créature doit également effectuer ce jet de sauvegarde lorsqu'elle
entre dans la zone du sort pour la première fois lors d'un tour ou
qu'elle y termine son tour.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, les dégâts augmentent de
1d10 pour chaque niveau d'emplacement supérieur au 5e.



#### Invocations instantanées

*Conjuration de 6e niveau (rituel)*

**Classes :** Magicien

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (un saphir d'une valeur de 1 000 gp)

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous touchez un objet pesant 10 livres ou moins dont la plus grande
dimension est de 6 pieds ou moins. Le sort laisse une marque invisible
sur sa surface et inscrit de manière invisible le nom de l'objet sur le
saphir que vous utilisez comme composant matériel. Chaque fois que vous
lancez ce sort, vous devez utiliser un saphir différent.

À tout moment par la suite, vous pouvez utiliser votre action pour
prononcer le nom de l'objet et écraser le saphir. L'objet apparaît
instantanément dans votre main, indépendamment des distances physiques
ou planaires, et le sort prend fin.

Si une autre créature tient ou porte l'objet, écraser le saphir ne
transporte pas l'objet jusqu'à vous, mais vous apprenez qui est la
créature qui possède l'objet et à peu près où cette créature se trouve
à ce moment-là.

*Dissipation de la magie* ou un effet similaire
appliqué avec succès au saphir met fin à l'effet de ce sort.



#### Invisibilité

*illusion de 2ème niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un cil enveloppé de gomme arabique).

**Durée :** Concentration, jusqu'à 1 heure

Une créature que vous touchez devient invisible jusqu'à la fin du sort.
Tout ce que la cible porte ou transporte est invisible tant qu'il est
sur elle. Le sort prend fin pour une cible qui attaque ou lance un sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
2e.



#### Danse irrésistible

*Enchantement de 6ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V

**Durée :** Concentration, jusqu'à 1 minute

Choisissez une créature que vous pouvez voir à portée. La cible entame
une danse comique sur place : elle traîne les pieds, tape du pied et
fait des cabrioles pendant toute la durée du sort. Les créatures qui ne
peuvent pas être charmées sont immunisées contre ce sort.

Une créature qui danse doit utiliser tout son mouvement pour danser sans
quitter son espace et a un désavantage sur les jets de sauvegarde de
Dextérité et les jets d'attaque. Tant que la cible est affectée par ce
sort, les autres créatures ont un avantage sur les jets d'attaque
contre elle. En tant qu'action, une créature dansante effectue un jet
de sauvegarde de Sagesse pour reprendre le contrôle d'elle-même. En cas
de sauvegarde réussie, le sort prend fin.



#### Saut

*Trans transmutation de 1er niveau*

**Classes :** Druide,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (patte arrière d'une sauterelle)

**Durée :** 1 minute

Vous touchez une créature. La distance de saut de la créature est
triplée jusqu'à la fin du sort.



#### Déblocage

*transmutation de 2ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Choisissez un objet que vous pouvez voir à portée. Il peut s'agir
d'une porte, d'une boîte, d'un coffre, d'un ensemble de manilles,
d'un cadenas ou d'un autre objet contenant un moyen banal ou magique
qui empêche l'accès.

Une cible qui est maintenue fermée par une serrure mondaine ou qui est
bloquée ou barrée devient déverrouillée, débloquée ou barrée. Si
l'objet possède plusieurs serrures, une seule d'entre elles est
déverrouillée.

Si vous choisissez une cible qui est maintenue fermée par un *Verrou
occulte*, ce sort est supprimé pendant 10 minutes,
pendant lesquelles la cible peut être ouverte et fermée normalement.

Lorsque vous lancez ce sort, un fort Déblocage, audible jusqu'à 300
pieds, émane de l'objet ciblé.



#### Légendes traditionnelles

*Divination de 5e niveau*

**Classes :** Barde,
Magicien

**Temps de coulée :** 10 minutes

**Portée :** Self

**Composantes :** V, S, M (encens d'une valeur d'au moins 250 gp, que
le sort consomme, et quatre lamelles d'ivoire d'une valeur d'au moins
50 gp chacune).

**Durée :** Instantané

Nommez ou décrivez une personne, un lieu ou un objet. Le sort fait
apparaître dans votre esprit un bref résumé de l'histoire de l'objet
que vous avez nommé. Il peut s'agir de récits actuels, d'histoires
oubliées ou même d'un savoir secret qui n'a jamais été largement
connu. Si la chose que vous avez nommée n'est pas d'une importance
légendaire, vous ne gagnez aucune information. Plus vous avez
d'informations sur la chose, plus les informations que vous recevez
sont précises et détaillées.

Les informations que vous obtenez sont exactes mais peuvent être
formulées en langage figuré. Par exemple, si vous avez une mystérieuse
hache magique sous la main, le sort pourrait vous donner cette
information : \"Malheur au malfaiteur dont la main touche la hache, car
même le manche tranche la main des méchants. Seul un véritable Enfant de
la Ronde, amant et aimé d'Arthur, peut Éveiller les véritables pouvoirs
de la hache, et seulement avec le mot sacré Avalon sur les lèvres.\"



#### Restauration partielle

*ab ab abjuration de 2ème niveau*

**Classes :** Barde,
Druide,
Rôdeur\...

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

Vous touchez une créature et pouvez mettre fin à une maladie ou un étât
qui l'afflige. La condition peut être aveugle, assourdie, paralysée ou
empoisonnée.



#### Lévitation

*transmutation de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (une petite boucle de cuir ou un morceau de
fil d'or plié en forme de coupe avec une longue tige à une extrémité).

**Durée :** Concentration, jusqu'à 10 minutes

Une créature ou un objet de votre choix que vous pouvez voir à portée
s'élève verticalement, jusqu'à 20 pieds, et y reste suspendu pour la
durée du sort. Ce sort peut faire léviter une cible pesant jusqu'à 500
livres. Une créature non consentante qui réussit un jet de sauvegarde de
Constitution n'est pas affectée.

La cible ne peut se déplacer qu'en poussant ou en tirant contre un
objet fixe ou une surface à portée (comme un mur ou un plafond), ce qui
lui permet de se déplacer comme si elle grimpait. Vous pouvez modifier
l'altitude de la cible jusqu'à 20 pieds dans n'importe quelle
direction à votre tour. Si vous êtes la cible, vous pouvez vous déplacer
vers le haut ou vers le bas dans le cadre de votre mouvement. Sinon,
vous pouvez utiliser votre action pour déplacer la cible, qui doit
rester dans la portée du sort.

Lorsque le sort prend fin, la cible flotte doucement vers le sol si elle
est encore en l'air.



#### Lumière

*Tour de magie d'évocation*

**Classes :** Barde,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, M (une luciole ou une mousse phosphorescente)

**Durée :** 1 heure

Vous touchez un objet dont la taille ne dépasse pas 3 mètres dans toutes
les dimensions. Jusqu'à la fin du sort, l'objet diffuse une lumière
vive dans un rayon de 20 pieds et une lumière chétive sur 20 pieds
supplémentaires. La lumière peut être colorée comme vous le souhaitez.
Couvrir complètement l'objet avec quelque chose d'opaque bloque la
lumière. Le sort prend fin si vous le lancez à nouveau ou si vous
l'annulez par une action.

Si vous ciblez un objet tenu ou porté par une créature hostile, celle-ci
doit réussir un jet de sauvegarde de Dextérité pour éviter le sort.



#### Éclair

*3rd-level evocation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (ligne de 100 pieds)

**Composantes :** V, S, M (un peu de fourrure et une tige d'ambre, de
cristal ou de verre)

**Durée :** Instantané

Un trait de foudre formant une ligne de 100 pieds de long et 5 pieds de
large jaillit de vous dans la direction de votre choix. Chaque créature
de la ligne doit effectuer un jet de sauvegarde de Dextérité. En cas
d'échec, la créature subit 8d6 points de dégâts de foudre, ou la moitié
en cas de réussite.

La foudre enflamme les objets inflammables de la zone qui ne sont pas
portés ou transportés.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts augmentent de
1d6 pour chaque niveau d'emplacement supérieur au 3e.



#### Localiser les animaux ou les plantes

*Divination de 2ème niveau (rituel)*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (un peu de fourrure d'un limier)

**Durée :** Instantané

Décrire ou nommer un type spécifique de bête ou de plante. En vous
concentrant sur la voix de la nature dans votre environnement, vous
apprenez la direction et la distance de la créature ou de la plante de
ce genre la plus proche dans un rayon de 8 km, s'il y en a.



#### Localiser une créature

*Divination de 4ème niveau*

**Classes :** Barde,
druide,
rôdeur\...

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (un peu de fourrure d'un limier)

**Durée :** Concentration, jusqu'à 1 heure

Décrivez ou nommez une créature qui vous est familière. Vous percevez la
direction de l'emplacement de la créature, tant que celle-ci se trouve
à moins de 1 000 pieds de vous. Si la créature se déplace, vous
connaissez la direction de son mouvement.

Le sort peut localiser une créature spécifique que vous connaissez, ou
la créature la plus proche d'un type spécifique (comme un humain ou une
licorne), à condition que vous ayez vu une telle créature de près - à
moins de 10 mètres - au moins une fois. Si la créature que vous avez
décrite ou nommée est sous une forme différente, par exemple sous
l'effet d'un sort de *métamorphose*, ce sort ne permet
pas de la localiser.

Ce sort ne peut pas localiser une créature si une eau courante d'au
moins 3 mètres de large bloque un chemin direct entre vous et la
créature.



#### Localiser un objet

*Divination de 2ème niveau*

**Classes :** Barde,
druide,
rôdeur\...

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (une brindille fourchue)

**Durée :** Concentration, jusqu'à 10 minutes

Décrivez ou nommez un objet qui vous est familier. Vous percevez la
direction de l'emplacement de l'objet, à condition que cet objet se
trouve à moins de 300 mètres de vous. Si l'objet est en mouvement, vous
connaissez la direction de son déplacement.

Ce sort peut localiser un objet spécifique que vous connaissez, à
condition que vous l'ayez vu de près (dans un rayon de 10 mètres) au
moins une fois. Il peut également localiser l'objet le plus proche
d'un type particulier, tel qu'un vêtement, un bijou, un meuble, un
outil ou une arme.

Ce sort ne peut pas localiser un objet si une épaisseur de plomb, même
une fine feuille, bloque un chemin direct entre vous et l'objet.



#### Grande foulée

*Trans transmutation de 1er niveau*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une pincée de terre)

**Durée :** 1 heure

Vous touchez une créature. La vitesse de la cible augmente de 10 pieds
jusqu'à la fin du sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, vous pouvez cibler une
créature supplémentaire pour chaque niveau d'emplacement supérieur au
1er.



#### Armure de mage

*abjuration de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un morceau de cuir séché).

**Durée :** 8 heures

Vous touchez une créature consentante qui ne porte pas d'armure, et une
force magique protectrice l'entoure jusqu'à la fin du sort. La CA de
base de la cible devient 13 + son modificateur de Dextérité. Le sort
prend fin si la cible revêt une armure ou si vous annulez le sort par
une action.



#### Main de mage

*Tour de magie de conjuration*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** 1 minute

Une main spectrale et flottante apparaît à un point de votre choix à
portée. La main dure toute la durée du sort ou jusqu'à ce que vous
l'écartiez par une action. La main disparaît si elle se trouve à plus
de 10 mètres de vous ou si vous lancez à nouveau ce sort.

Vous pouvez utiliser votre action pour contrôler la main. Vous pouvez
utiliser la main pour manipuler un objet, ouvrir une porte ou un
récipient non verrouillé, ranger ou récupérer un objet dans un récipient
ouvert, ou verser le contenu d'une fiole. Vous pouvez déplacer la main
jusqu'à 30 pieds à chaque fois que vous l'utilisez.

La main ne peut pas attaquer, activer des objets magiques ou porter plus
de 10 livres.



#### Cercle magique

*3rd-level abjuration*

**Classes :** Clerc,
sorcier\...

**Temps d'incantation :** 1 minute

**Portée :** 10 pieds

**Composantes :** V, S, M (eau bénite ou poudre d'argent et de fer
d'une valeur d'au moins 100 gp, que le sort consomme).

**Durée :** 1 heure

Vous créez un cylindre d'énergie magique de 10 pieds de rayon et de 20
pieds de haut centré sur un point du sol que vous pouvez voir à portée.
Des runes lumineuses apparaissent partout où le cylindre croise le sol
ou une autre surface.

Choisissez un ou plusieurs des types de créatures suivants : céleste,
élémentaire, fée, fiélon ou mort-vivant. Le cercle affecte une créature
du type choisi de la manière suivante :

-   La créature ne peut pas entrer volontairement dans le cylindre par
    des moyens non magiques. Si la créature tente d'utiliser la
    téléportation ou le voyage interplanaire pour y parvenir, elle doit
    d'abord réussir un jet de sauvegarde de Charisme.
-   La créature a un désavantage aux jets d'attaque contre les cibles
    situées dans le cylindre.
-   Les cibles situées dans le cylindre ne peuvent pas être charmées,
    effrayées ou possédées par la créature.

Lorsque vous lancez ce sort, vous pouvez choisir de faire en sorte que
sa magie opère dans le sens inverse, empêchant une créature du type
spécifié de sortir du cylindre et protégeant les cibles à l'extérieur
de celui-ci.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, la durée augmente de 1
heure pour chaque niveau d'emplacement supérieur au 3e.



#### Urne magique

*Nécromancie de 6ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 minute

**Portée :** Self

**Composantes :** V, S, M (une gemme, un cristal, un reliquaire ou tout
autre récipient ornemental d'une valeur d'au moins 500 gp).

**Durée :** Jusqu'à ce qu'il soit dissipé

Votre corps tombe dans un état catatonique tandis que votre âme le
quitte et entre dans le récipient que vous avez utilisé pour la
composante matérielle du sort. Pendant que votre âme habite le
récipient, vous êtes conscient de votre environnement comme si vous
étiez dans l'espace du récipient. Vous ne pouvez pas vous déplacer ni
utiliser de réactions. La seule action que vous pouvez entreprendre est
de projeter votre âme jusqu'à 30 mètres hors du récipient, soit pour
retourner dans votre corps vivant (et mettre fin au sort), soit pour
tenter de posséder un corps humanoïde.

Vous pouvez tenter de posséder tout humanoïde situé à moins de 30 mètres
de vous et que vous pouvez voir (les créatures protégées par un sort de
Protection contre le Bien *et le Mal*
ou de *Cercle magique*.
La cible doit effectuer un jet de sauvegarde de Charisme. En cas
d'échec, votre âme se déplace dans le corps de la cible, et l'âme de
la cible est piégée dans le récipient. En cas de réussite, la cible
résiste à vos efforts pour la posséder, et vous ne pouvez plus tenter de
la posséder pendant 24 heures.

Une fois que vous possédez le corps d'une créature, vous la contrôlez.
Vos statistiques de jeu sont remplacées par celles de la créature, mais
vous conservez votre alignement et vos scores d'Intelligence, de
Sagesse et de Charisme. Vous conservez le bénéfice de vos propres
caractéristiques de classe. Si la cible possède des niveaux de classe,
vous ne pouvez utiliser aucun de ses traits de classe.

Pendant ce temps, l'âme de la créature possédée peut percevoir à partir
du conteneur en utilisant ses propres sens, mais elle ne peut pas se
déplacer ou entreprendre des actions.

Lorsque vous possédez un corps, vous pouvez utiliser votre action pour
retourner du corps hôte au récipient s'il se trouve à moins de 100
pieds de vous, ramenant l'âme de la créature hôte dans son corps. Si le
corps de l'hôte meurt alors que vous êtes dans le récipient, la
créature meurt et vous devez effectuer un jet de sauvegarde de Charisme
contre votre propre DC d'incantation. En cas de réussite, vous
retournez dans le conteneur s'il se trouve à moins de 30 mètres de
vous. Sinon, vous mourrez.

Si le récipient est détruit ou si le sort prend fin, votre âme retourne
immédiatement dans votre corps. Si votre corps se trouve à plus de 30
mètres de vous ou s'il est mort lorsque vous tentez de le rejoindre,
vous mourez. Si l'âme d'une autre créature se trouve dans le récipient
lorsqu'il est détruit, l'âme de cette créature retourne dans son corps
si celui-ci est vivant et se trouve à moins de 30 mètres. Sinon, cette
créature meurt.

Lorsque le sort prend fin, le récipient est détruit.



#### Projectile magique

*Evocation de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous créez trois fléchettes lumineuses de force magique. Chaque
fléchette touche une créature de votre choix que vous pouvez voir à
portée. Une fléchette inflige 1d4 + 1 dégâts de force à sa cible. Les
fléchettes frappent toutes simultanément, et vous pouvez les diriger
pour qu'elles touchent une créature ou plusieurs.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, le sort crée une fléchette
supplémentaire pour chaque niveau d'emplacement au-dessus du 1er.



#### Bouche magique

*Illusion de 2ème niveau (rituel)*

**Classes :** Barde

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S, M (un petit morceau de rayon de miel et de
poussière de jade d'une valeur d'au moins 10 gp, que le sort
consomme).

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous implantez un message dans un objet à portée, un message qui est
prononcé lorsqu'un étât de déclenchement est rempli. Choisissez un
objet que vous pouvez voir et qui n'est pas porté ou transporté par une
autre créature. Prononcez ensuite le message, qui ne doit pas dépasser
25 mots, mais qui peut être délivré en 10 minutes. Enfin, déterminez la
circonstance qui déclenchera le sort pour délivrer votre message.

Lorsque cette circonstance se produit, une bouche magique apparaît sur
l'objet et récite le message avec votre voix et au même volume que vous
avez parlé. Si l'objet choisi possède une bouche ou quelque chose qui
ressemble à une bouche (par exemple, la bouche d'une statue), la bouche
magique y apparaît de sorte que les mots semblent provenir de la bouche
de l'objet. Lorsque vous lancez ce sort, vous pouvez faire en sorte que
le sort prenne fin après avoir délivré son message, ou il peut rester et
répéter son message chaque fois que le déclencheur se produit.

La circonstance déclenchante peut être aussi générale ou détaillée que
vous le souhaitez, mais elle doit être basée sur des conditions
visuelles ou auditives qui se produisent dans un rayon de 30 pieds
autour de l'objet. Par exemple, vous pouvez demander à la bouche de
parler lorsqu'une créature se déplace dans un rayon de 30 pieds autour
de l'objet ou lorsqu'une cloche d'argent sonne dans un rayon de 30
pieds autour de l'objet.



#### Arme magique

*transmutation de 2ème niveau*

**Classes :** Paladin

**Temps d'incantation :** 1 action bonus

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Vous touchez une arme non magique. Jusqu'à la fin du sort, cette arme
devient une arme magique avec un bonus de +1 aux jets d'attaque et aux
jets de dégâts.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, le bonus passe à +2.
Lorsque vous utilisez un emplacement de sort de 6e niveau ou plus, le
bonus passe à +3.



#### Magnifique manoir

*conjuration de 7ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 minute

**Portée :** 300 pieds

**Composantes :** V, S, M (un portail miniature sculpté dans l'ivoire,
un petit morceau de marbre poli et une très petite cuillère en argent,
chaque objet valant au moins 5 gp).

**Durée :** 24 heures

Vous conjurez une habitation extradimensionnelle à portée qui dure le
temps nécessaire. Vous choisissez où se trouve son unique entrée.
L'entrée brille faiblement et mesure 1,5 m de large et 3 m de haut.
Vous et toute créature que vous désignez lorsque vous lancez le sort
pouvez entrer dans la demeure extradimensionnelle tant que le portail
reste ouvert. Vous pouvez ouvrir ou fermer le portail si vous vous
trouvez à moins de 10 mètres de lui. Lorsqu'il est fermé, le portail
est invisible.

Au-delà du portail se trouve un magnifique foyer avec de nombreuses
chambres au-delà. L'atmosphère est propre, fraîche et chaleureuse.

Vous pouvez créer le plan d'étage que vous voulez, mais l'espace ne
peut pas dépasser 50 cubes, chaque cube faisant 10 pieds de côté.
L'endroit est meublé et décoré comme vous le souhaitez. Il contient
suffisamment de nourriture pour servir un banquet de neuf plats pour un
maximum de 100 personnes. Un personnel de 100 serviteurs presque
transparents accompagne tous ceux qui entrent. Vous décidez de
l'apparence visuelle de ces serviteurs et de leurs vêtements. Ils sont
totalement obéissants à vos ordres. Chaque serviteur peut accomplir
n'importe quelle tâche qu'un serviteur humain normal pourrait
accomplir, mais il ne peut pas attaquer ou entreprendre une action qui
pourrait nuire directement à une autre créature. Ainsi, les serviteurs
peuvent aller chercher des objets, nettoyer, raccommoder, plier des
vêtements, allumer des feux, servir de la nourriture, verser du vin, et
ainsi de suite. Les serviteurs peuvent aller partout dans le manoir mais
ne peuvent pas en sortir. Les meubles et autres objets créés par ce sort
se dissipent en fumée s'ils sont retirés du manoir. Lorsque le sort
prend fin, toutes les créatures présentes dans l'espace
extradimensionnel sont expulsées dans les espaces ouverts les plus
proches de l'entrée.



#### Image majeure

*Ill illusion illusion de 3 de niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un peu de toison)

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez l'image d'un objet, d'une créature ou d'un autre
phénomène visible qui n'est pas plus grand qu'un cube de 6 mètres de
côté. L'image apparaît à un endroit que vous pouvez voir à portée et
dure toute la durée du sort. Elle semble complètement réelle, y compris
les sons, les odeurs et la température appropriés à la chose
représentée. Vous ne pouvez pas créer une chaleur ou un froid suffisant
pour causer des dégâts, un son assez fort pour infliger des dégâts de
tonnerre ou assourdir une créature, ou une odeur qui pourrait rendre
malade une créature (comme la puanteur d'un troglodyte).

Tant que vous êtes à portée de l'illusion, vous pouvez utiliser votre
action pour faire en sorte que l'image se déplace vers tout autre
endroit à portée. Lorsque l'image change d'emplacement, vous pouvez
modifier son apparence afin que ses mouvements paraissent naturels pour
l'image. Par exemple, si vous créez l'image d'une créature et que
vous la déplacez, vous pouvez modifier l'image pour qu'elle semble
marcher. De même, vous pouvez faire en sorte que l'illusion émette des
sons différents à différents moments, voire même qu'elle tienne une
conversation, par exemple.

L'interaction physique avec l'image révèle qu'il s'agit d'une
illusion, car les choses peuvent la traverser. Une créature qui utilise
son action pour examiner l'image peut déterminer qu'il s'agit d'une
illusion en réussissant un test d'Intelligence (Investigation) contre
votre DC de sauvegarde contre les sorts. Si une créature discerne
l'illusion pour ce qu'elle est, la créature peut voir à travers
l'image, et ses autres qualités sensorielles deviennent faibles pour la
créature.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 6e niveau ou plus, le sort dure jusqu'à ce
qu'il soit dissipé, sans nécessiter votre concentration.



#### Soins de groupe

*Evocation de 5ème niveau*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Une vague d'énergie de guérison se déverse d'un point de votre choix à
portée. Choisissez jusqu'à six créatures dans une sphère de 30 pieds de
rayon centrée sur ce point. Chaque cible regagne un nombre de points de
vie égal à 3d8 + votre modificateur d'aptitude aux incantations. Ce
sort n'a aucun effet sur les morts-vivants ou les constructions.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, les soins augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 5e.



#### Guérison de groupe

*Evocation de 9ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Un flot d'énergie de guérison s'écoule de vous vers les créatures
blessées autour de vous. Vous restaurez jusqu'à 700 points de vie,
répartis comme vous le souhaitez entre un nombre quelconque de créatures
que vous pouvez voir à portée. Les créatures soignées par ce sort sont
également guéries de toutes les maladies et de tout effet les rendant
aveugles ou assourdies. Ce sort n'a aucun effet sur les morts-vivants
ou les constructions.



#### Mot de guérison Guérison de groupe

*3rd-level evocation*

**Classes :** Clerc

**Temps d'incantation :** 1 action bonus

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Lorsque vous prononcez des mots de restauration, jusqu'à six créatures
de votre choix que vous pouvez voir à portée regagnent un nombre de
points de vie égal à 1d4 + votre modificateur de caractéristique
d'incantation. Ce sort n'a aucun effet sur les morts-vivants ou les
constructions.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 4e niveau ou plus, les soins augmentent de 1d4
pour chaque niveau d'emplacement supérieur au 3e.



#### Suggestion de groupe

*Enchantement de 6ème niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, M (une langue de serpent et un peu de miel ou une
goutte d'huile douce).

**Durée :** 24 heures

Vous suggérez une ligne de conduite (limitée à une phrase ou deux) et
influencez magiquement jusqu'à douze créatures de votre choix que vous
pouvez voir à portée et qui peuvent vous entendre et vous comprendre.
Les créatures qui ne peuvent pas être charmées sont immunisées contre
cet effet. La suggestion doit être formulée de manière à ce que le plan
d'action semble raisonnable. Demander à la créature de se poignarder,
de se jeter sur une lance, de s'immoler ou de faire un autre acte
manifestement nuisible annule automatiquement l'effet du sort.

Chaque cible doit effectuer un jet de sauvegarde de Sagesse. En cas
d'échec, elle suit le plan d'action que vous avez décrit au mieux de
ses capacités. L'action suggérée peut se poursuivre pendant toute la
durée du sort. Si l'activité suggérée peut être réalisée en moins de
temps, le sort prend fin lorsque le sujet termine ce qu'on lui a
demandé de faire.

Vous pouvez également spécifier des étâts qui déclencheront une activité
spéciale pendant cette durée. Par exemple, vous pouvez suggérer à un
groupe de soldats de donner tout leur argent au premier mendiant qu'ils
rencontrent. Si la condition n'est pas remplie avant la fin du sort,
l'activité n'est pas exécutée.

Si vous ou l'un de vos compagnons endommagez une créature affectée par
ce sort, le sort prend fin pour cette créature.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau, la durée est de 10 jours. Lorsque
vous utilisez un emplacement de sort de 8e niveau, la durée est de 30
jours. Lorsque vous utilisez un emplacement de sort de 9e niveau, la
durée est d'un an et un jour.



#### Dédale

*Conjuration de 8ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Vous bannissez une créature que vous pouvez voir à portée de main dans
un Demi-plan labyrinthique. La cible y reste pour la durée de
l'opération ou jusqu'à ce qu'elle s'échappe du Dédale.

La cible peut utiliser son action pour tenter de s'échapper. Elle doit
alors effectuer un test d'Intelligence DC 20. Si elle réussit, elle
s'échappe et le sort prend fin (un minotaure ou un démon des
labyrinthes réussit automatiquement).

Lorsque le sort prend fin, la cible réapparaît dans l'espace qu'elle a
quitté ou, si cet espace est occupé, dans l'espace inoccupé le plus
proche.



#### Fusion dans la pierre

*Transmutation de 3e niveau (rituel)*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** 8 heures

Vous pénétrez dans un objet ou une surface en pierre suffisamment grande
pour contenir entièrement votre corps, fusionnant vous-même et tout
l'équipement que vous portez avec la pierre pour la durée de
l'opération. En utilisant votre mouvement, vous entrez dans la pierre à
un point que vous pouvez toucher. Rien de votre présence ne reste
visible ou détectable par les sens non magiques.

Lorsque vous êtes fusionné avec la pierre, vous ne pouvez pas voir ce
qui se passe à l'extérieur, et tous les tests de Sagesse (Perception)
que vous effectuez pour entendre des sons à l'extérieur sont faits avec
un désavantage. Vous restez conscient du passage du temps et pouvez vous
lancer des sorts pendant que vous êtes fusionné avec la pierre. Vous
pouvez utiliser votre mouvement pour quitter la pierre là où vous y êtes
entré, ce qui met fin au sort. Sinon, vous ne pouvez pas vous déplacer.

Les dégâts physiques mineurs infligés à la pierre ne vous font pas de
mal, mais sa destruction partielle ou une modification de sa forme (au
point que vous n'y tenez plus) vous expulse et vous inflige 6d6 points
de dégâts de matraquage. La destruction complète de la pierre (ou sa
transmutation en une autre substance) vous expulse et vous inflige 50
points de dégâts de matraquage. Si vous êtes expulsé, vous tombez à
terre dans un espace inoccupé le plus proche de l'endroit où vous êtes
entré.



#### Réparation

*Tour de magie de transmutation*

**Classes :** Barde,
Druide,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (deux pierres d'habitation)

**Durée :** Instantané

Ce sort répare une seule cassure ou déchirure dans un objet que vous
touchez, comme un maillon de chaîne cassé, les deux moitiés d'une clé
cassée, une cape déchirée ou une outre qui fuit. Tant que la cassure ou
la déchirure ne dépasse pas 30 cm dans toutes les dimensions, vous la
réparez sans laisser de trace.

Ce sort peut réparer physiquement un objet ou une construction magique,
mais il ne peut pas restaurer la magie d'un tel objet.



#### Message

*Tour de magie de transmutation*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un court morceau de fil de cuivre)

**Durée :** 1 round

Vous pointez votre doigt vers une créature à portée et chuchotez un
message. La cible (et seulement la cible) entend le message et peut
répondre dans un murmure que vous seul pouvez entendre.

Vous pouvez lancer ce sort à travers des objets solides si vous
connaissez bien la cible et savez qu'elle se trouve au-delà de la
barrière. Le silence magique, 1 pied de pierre, 1 pouce de métal commun,
une fine feuille de plomb ou 3 pieds de bois bloquent le sort. Le sort
n'a pas besoin de suivre une ligne droite et peut se déplacer librement
autour des coins ou à travers des ouvertures.



#### Nuée de météores

*Evocation de 9ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 1 mile

**Composantes :** V, S

**Durée :** Instantané

Des orbes de feu flamboyantes tombent sur le sol en quatre points
différents que vous pouvez voir à portée. Chaque créature dans une
sphère de 40 pieds de rayon centrée sur chaque point que vous choisissez
doit effectuer un jet de sauvegarde de Dextérité. La sphère se propage
dans les coins. Une créature subit 20d6 dégâts de feu et 20d6 dégâts de
contondant sur un jet de sauvegarde raté, ou la moitié des dégâts sur un
jet réussi. Une créature se trouvant dans la zone de plus d'une salve
ardente n'est affectée qu'une seule fois.

Le sort endommage les objets dans la zone et enflamme les objets
inflammables qui ne sont pas portés ou transportés.



#### Esprit impénétrable

*abjuration de 8ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** 24 heures

Jusqu'à la fin du sort, une créature consentante que vous touchez est
immunisée contre les dégâts psychiques, tout effet permettant de
ressentir ses émotions ou de lire ses pensées, les sorts de divination
et l'état charmé. Le sort déjoue également les sorts de
*souhaits* et les sorts ou effets de puissance similaire
utilisés pour affecter l'esprit de la cible ou pour obtenir des
informations sur elle.



#### Illusion mineure

*Tour de magie d'illusion*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** S, M (un peu de toison)

**Durée :** 1 minute

Vous créez un son ou une image d'un objet à portée qui dure pendant la
durée du sort. L'illusion prend également fin si vous l'annulez par
une action ou si vous lancez à nouveau ce sort.

Si vous créez un son, son volume peut aller d'un murmure à un cri. Il
peut s'agir de votre voix, de la voix d'une autre personne, du
rugissement d'un lion, d'un battement de tambour ou de tout autre son
de votre choix. Le son se poursuit sans interruption pendant toute la
durée du sort, ou vous pouvez émettre des sons discrets à différents
moments avant que le sort ne prenne fin.

Si vous créez l'image d'un objet - comme une chaise, des traces de pas
boueuses ou un petit coffre - elle ne doit pas être plus grande qu'un
cube de 1,5 m de côté. L'image ne peut pas créer de son, de lumière,
d'odeur ou tout autre effet sensoriel. L'interaction physique avec
l'image révèle qu'il s'agit d'une illusion, car des objets peuvent
la traverser.

Si une créature utilise son action pour examiner le son ou l'image,
elle peut déterminer qu'il s'agit d'une illusion en réussissant un
test d'Intelligence (Investigation) contre votre DC de sauvegarde des
sorts. Si une créature discerne l'illusion pour ce qu'elle est,
l'illusion devient faible pour la créature.



#### Mirage Occulte

*Ill illusion du 7ème niveau*

**Classes :** Barde,
Magicien

**Temps de coulée :** 10 minutes

**Portée :** Visée

**Composantes :** V, S

**Durée :** 10 jours

Vous faites en sorte que le terrain situé dans une zone allant jusqu'à
un kilomètre carré ait l'apparence, le son, l'odeur et même la
sensation d'un autre type de terrain. La forme générale du terrain
reste cependant la même. Des champs ouverts ou une route peuvent
ressembler à un marécage, une colline, une crevasse ou tout autre
terrain difficile ou infranchissable. Un étang peut ressembler à une
prairie herbeuse, un précipice à une pente douce, ou un ravin rocheux à
une route large et lisse.

De même, vous pouvez modifier l'apparence des structures, ou en ajouter
là où il n'y en a pas. Le sort ne permet pas de déguiser, de dissimuler
ou d'ajouter des créatures.

L'illusion comprend des éléments auditifs, visuels, tactiles et
olfactifs, de sorte qu'elle peut transformer un terrain dégagé en
terrain difficile (ou vice versa) ou entraver le mouvement dans la zone.
Tout élément du terrain illusoire (comme un rocher ou un bâton) qui est
retiré de la zone du sort disparaît immédiatement.

Les créatures dotées d'une vision véritable peuvent voir à travers
l'illusion la véritable forme du terrain ; cependant, tous les autres
éléments de l'illusion demeurent, de sorte que même si la créature est
consciente de la présence de l'illusion, elle peut encore interagir
physiquement avec celle-ci.



#### Image miroir

*illusion de 2ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** 1 minute

Trois duplicatas illusoires de vous-même apparaissent dans votre espace.
Jusqu'à la fin du sort, les doubles se déplacent avec vous et imitent
vos actions, changeant de position de sorte qu'il est impossible de
savoir quelle image est réelle. Vous pouvez utiliser votre action pour
écarter les duplicatas illusoires.

Chaque fois qu'une créature vous cible avec une attaque pendant la
durée du sort, lancez un d20 pour déterminer si l'attaque vise plutôt
un de vos doubles.

Si vous avez trois doublons, vous devez obtenir un 6 ou plus pour
changer la cible de l'attaque en un doublon. Avec deux doublons, vous
devez obtenir un 8 ou plus. Avec un double, vous devez obtenir un 11 ou
plus.

La CA d'un double est égale à 10 + votre modificateur de Dextérité. Si
une attaque touche un double, celui-ci est détruit. Un double ne peut
être détruit que par une attaque qui le touche. Il ignore tous les
autres dégâts et effets. Le sort prend fin lorsque les trois doubles
sont détruits.

Une créature n'est pas affectée par ce sort si elle ne peut pas voir,
si elle se fie à d'autres sens que la vue, comme la vision aveugle, ou
si elle peut percevoir les illusions comme fausses, comme avec la vision
véritable.



#### Double illusoire

*Ill illusion de 5ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** S

**Durée :** Concentration, jusqu'à 1 heure

Vous devenez invisible en même temps qu'un double illusoire de vous
apparaît à l'endroit où vous vous trouvez. Le double dure le temps
nécessaire, mais l'invisibilité prend fin si vous attaquez ou lancez un
sort.

Vous pouvez utiliser votre action pour déplacer votre double illusoire
jusqu'à deux fois votre vitesse et lui faire faire des gestes, parler
et se comporter comme vous le souhaitez.

Vous pouvez voir par ses yeux et entendre par ses oreilles comme si vous
vous trouviez là où il se trouve. À chacun de vos tours, comme action
bonus, vous pouvez passer de l'utilisation de ses sens à l'utilisation
des vôtres, ou inversement. Lorsque vous utilisez ses sens, vous êtes
aveuglé et assourdi par rapport à votre propre environnement.



#### Pas brumeux

*conjuration de 2ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action bonus

**Portée :** Self

**Composantes :** V

**Durée :** Instantané

Brièvement entouré d'une brume argentée, vous vous téléportez jusqu'à
10 mètres dans un espace inoccupé que vous pouvez voir.



#### Modification de mémoire

*Enchantement de 5ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous tentez de remodeler les souvenirs d'une autre créature. Une
créature que vous pouvez voir doit effectuer un jet de sauvegarde de
Sagesse. Si vous combattez la créature, elle a l'avantage au jet de
sauvegarde. En cas d'échec, la cible est charmée par vous pour la durée
du sort. La cible charmée est frappée d'incapacité et n'a pas
conscience de son environnement, mais elle peut toujours vous entendre.
Si elle subit des dégâts ou est ciblée par un autre sort, ce sort prend
fin et aucun des souvenirs de la cible n'est modifié.

Tant que ce charme dure, vous pouvez affecter la mémoire de la cible
d'un événement qu'elle a vécu dans les dernières 24 heures et qui n'a
pas duré plus de 10 minutes. Vous pouvez éliminer définitivement tout
souvenir de l'événement, permettre à la cible de se souvenir de
l'événement avec une clarté parfaite et des détails exacts, modifier sa
mémoire des détails de l'événement, ou créer un souvenir d'un autre
événement.

Vous devez parler à la cible pour décrire comment ses souvenirs sont
affectés, et elle doit être capable de comprendre votre langue pour que
les souvenirs modifiés prennent racine. Son esprit comble toutes les
lacunes dans les détails de votre description. Si le sort se termine
avant que vous n'ayez fini de décrire les souvenirs modifiés, la
mémoire de la créature n'est pas altérée. Sinon, les souvenirs modifiés
prennent effet à la fin du sort.

Un souvenir modifié n'affecte pas nécessairement le comportement d'une
créature, en particulier si le souvenir contredit les inclinations
naturelles, l'alignement ou les croyances de la créature. Un souvenir
modifié illogique, tel que l'implantation d'un souvenir de la façon
dont la créature a aimé se tremper dans l'acide, est rejeté, peut-être
comme un mauvais rêve. Le MJ peut estimer qu'un souvenir modifié est
trop absurde pour affecter une créature de manière significative.

Un sort de *suppression de malédiction* ou de
*restauration supérieure* lancé sur la cible
restaure la vraie mémoire de la créature.

***Niveaux supérieurs.*** Si vous lancez ce sort en utilisant un
emplacement de sort de 6e niveau ou plus, vous pouvez altérer les
souvenirs de la cible d'un événement qui a eu lieu jusqu'à 7 jours
auparavant (6e niveau), 30 jours auparavant (7e niveau), 1 an auparavant
(8e niveau), ou n'importe quand dans le passé de la créature (9e
niveau).



#### Rayon de lune

*Evocation de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (plusieurs graines de n'importe quelle plante
à graines de lune et un morceau de feldspath opalescent).

**Durée :** Concentration, jusqu'à 1 minute

Un rayon argenté de lumière pâle brille dans un cylindre de 5 pieds de
rayon et de 40 pieds de haut, centré sur un point à portée. Jusqu'à la
fin du sort, une faible lumière remplit le cylindre.

Lorsqu'une créature entre dans la zone du sort pour la première fois
lors d'un tour ou commence son tour à cet endroit, elle est engloutie
dans des flammes fantomatiques qui provoquent une douleur fulgurante, et
elle doit effectuer un jet de sauvegarde de Constitution. Elle subit
2d10 points de dégâts radiants en cas d'échec, ou la moitié en cas de
réussite.

Un métamorphe effectue son jet de sauvegarde avec un désavantage. S'il
échoue, il reprend instantanément sa forme d'origine et ne peut en
prendre une autre avant d'avoir quitté la lumière du sort.

A chacun de vos tours après avoir lancé ce sort, vous pouvez utiliser
une action pour déplacer le rayon de 60 pieds dans n'importe quelle
direction.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts augmentent de 1d10
pour chaque niveau d'emplacement supérieur au 2e.



#### Façonnage de la Terre

*Transmutation de 6ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (une lame de fer et un petit sac contenant un
mélange de terre - argile, terreau et sable).

**Durée :** Concentration, jusqu'à 2 heures

Choisissez une zone de terrain ne dépassant pas 40 pieds sur un côté à
portée. Vous pouvez remodeler la terre, le sable ou l'argile de la zone
de la manière de votre choix pendant la durée du jet. Vous pouvez élever
ou abaisser l'élévation de la zone, créer ou combler une tranchée,
ériger ou aplatir un mur, ou former un pilier. L'étendue de ces
changements ne peut pas dépasser la moitié de la plus grande dimension
de la zone. Ainsi, si vous affectez un carré de 40 pieds, vous pouvez
créer un pilier d'une hauteur maximale de 20 pieds, élever ou abaisser
l'élévation du carré de 20 pieds au maximum, creuser une tranchée
d'une profondeur maximale de 20 pieds, et ainsi de suite. Il faut 10
minutes pour que ces modifications soient effectuées.

À la fin de chaque période de 10 minutes que vous passez à vous
concentrer sur le sort, vous pouvez choisir une nouvelle zone de terrain
à affecter.

Comme la transformation du terrain se fait lentement, les créatures de
la zone ne peuvent généralement pas être piégées ou blessées par le
mouvement du sol.

Ce sort ne peut pas manipuler les pierres naturelles ou les
constructions en pierre. Les roches et les structures se déplacent pour
s'adapter au nouveau terrain. Si la façon dont vous façonnez le terrain
rend une structure instable, elle risque de s'effondrer.

De même, ce sort n'a pas d'effet direct sur la croissance des plantes.
La terre déplacée entraîne les plantes avec elle.



#### Antidétection

*3rd-level abjuration*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une pincée de poussière de diamant d'une
valeur de 25 gp saupoudrée sur la cible, que le sort consomme).

**Durée :** 8 heures

Pour la durée, vous cachez une cible que vous touchez de la magie de
divination. La cible peut être une créature consentante ou un lieu ou un
objet d'une taille maximale de 3 mètres dans toutes les dimensions. La
cible ne peut être ciblée par aucune magie de divination ou perçue par
des capteurs de scrutation magiques.



#### Passage sans trace

*l'ab juration de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (cendres d'une feuille de gui brûlée et
d'une branche d'épinette).

**Durée :** Concentration, jusqu'à 1 heure

Un voile d'ombres et de silence rayonne de vous, vous masquant, vous et
vos compagnons, de toute détection. Pendant toute la durée du sort,
chaque créature de votre choix se trouvant dans un rayon de 30 pieds
autour de vous (vous compris) bénéficie d'un bonus de +10 aux tests de
Dextérité (Discrétion) et ne peut être traquée que par des moyens
magiques. Une créature qui bénéficie de ce bonus ne laisse derrière elle
aucune trace ou autre trace de son passage.



#### Passe-muraille

*transmutation de 5ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une pincée de graines de sésame)

**Durée :** 1 heure

Un passage apparaît à un point de votre choix que vous pouvez voir sur
une surface en bois, en plâtre ou en pierre (comme un mur, un plafond ou
un sol) à portée, et dure toute la durée. Vous choisissez les dimensions
de l'ouverture : jusqu'à 5 pieds de large, 8 pieds de haut et 20 pieds
de profondeur. Le passage ne crée aucune instabilité dans la structure
qui l'entoure.

Lorsque l'ouverture disparaît, les créatures ou objets encore présents
dans le passage créé par le sort sont éjectés en toute sécurité vers un
espace inoccupé le plus proche de la surface sur laquelle vous avez
lancé le sort.



#### Assassin imaginaire

*illusion de 4 de niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous puisez dans les cauchemars d'une créature que vous pouvez voir à
portée et créez une manifestation illusoire de ses peurs les plus
profondes, visible uniquement par cette créature. La cible doit
effectuer un jet de sauvegarde de Sagesse. En cas d'échec, la cible est
effrayée pour la durée du sort. À la fin de chaque tour de la cible
avant la fin du sort, la cible doit réussir un jet de sauvegarde de
Sagesse ou subir 4d10 dégâts psychiques. En cas de sauvegarde réussie,
le sort prend fin.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 5e niveau ou plus, les dégâts augmentent de
1d10 pour chaque niveau d'emplacement supérieur au 4e.



#### Monture fantôme

*Illusion de 3e niveau (rituel)*

**Classes :** Magicien

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** 1 heure

Une Grande créature quasi-réelle ressemblant à un cheval apparaît sur le
sol dans un espace inoccupé de votre choix à portée. Vous décidez de
l'apparence de la créature, mais elle est équipée d'une selle, d'un
mors et d'une bride. Tout équipement créé par le sort disparaît dans
une bouffée de fumée s'il est transporté à plus de 3 mètres du
destrier.

Pour la durée, vous ou une créature de votre choix pouvez monter le
destrier. La créature utilise les statistiques d'un cheval de selle,
sauf qu'elle a une vitesse de 100 pieds et peut parcourir 10 miles en
une heure, ou 13 miles à un rythme rapide. Lorsque le sort prend fin, le
destrier s'efface progressivement, laissant au cavalier 1 minute pour
descendre de cheval. Le sort prend fin si vous utilisez une action pour
l'annuler ou si le destrier subit des dégâts.



#### Allié planaire

*Conjuration de 6ème niveau*

**Classes :** Clerc

**Temps de coulée :** 10 minutes

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous implorez l'aide d'une entité venue d'Ailleurs. Cet être doit
vous être connu : un dieu, un primordial, un prince démon ou tout autre
être de puissance cosmique. Cette entité envoie un céleste, un
élémentaire ou un fiélon qui lui est fidèle pour vous aider, faisant
apparaître la créature dans un espace inoccupé à portée. Si vous
connaissez le nom d'une créature spécifique, vous pouvez prononcer ce
nom lorsque vous lancez ce sort pour demander cette créature, bien que
vous puissiez obtenir une créature différente de toute façon (choix du
MJ).

Lorsque la créature apparaît, rien ne l'oblige à se comporter d'une
manière particulière. Vous pouvez demander à la créature de rendre un
service en échange d'un paiement, mais elle n'est pas obligée de le
faire. La tâche demandée peut être simple (nous faire voler à travers le
gouffre, ou nous aider à combattre) ou complexe (espionner nos ennemis,
ou nous protéger pendant notre incursion dans le donjon). Vous devez
être capable de communiquer avec la créature pour négocier ses services.

Le paiement peut prendre différentes formes. Un céleste peut demander un
don important d'or ou d'objets magiques à un temple allié, tandis
qu'un fiélon peut exiger un sacrifice vivant ou un don de trésor.
Certaines créatures peuvent échanger leurs services contre une quête que
vous entreprenez.

En règle générale, une tâche qui peut être mesurée en minutes nécessite
un paiement de 100 gp par minute. Une tâche mesurée en heures nécessite
1 000 gp par heure. Et une tâche mesurée en jours (jusqu'à 10 jours)
nécessite 10 000 gp par jour. Le MJ peut ajuster ces paiements en
fonction des circonstances dans lesquelles vous lancez le sort. Si la
tâche est en accord avec l'éthique de la créature, le paiement peut
être réduit de moitié ou même supprimé. Les tâches non dangereuses ne
nécessitent généralement que la moitié du paiement suggéré, tandis que
les tâches particulièrement dangereuses peuvent nécessiter un don plus
important. Les créatures acceptent rarement les tâches qui semblent
suicidaires.

Une fois que la créature a terminé sa tâche ou que la durée de service
convenue a expiré, la créature retourne sur son plan d'origine après
vous avoir fait un rapport, si cela est approprié à la tâche et si
possible. Si vous ne parvenez pas à vous mettre d'accord sur un prix
pour le service de la créature, celle-ci retourne immédiatement sur son
plan d'origine.

Une créature enrôlée pour rejoindre votre groupe compte comme un membre
de celui-ci, recevant une part complète des points d'expérience
attribués.



#### Contrat

*l'ab juration de 5ème niveau*

**Classes :** Barde,
Druide

**Durée du moulage :** 1 heure

**Portée :** 60 pieds

**Composantes :** V, S, M (un bijou d'au moins 1 000 gp, que le sort
consomme).

**Durée :** 24 heures

Avec ce sort, vous tentez de lier un céleste, un élémentaire, un fée ou
un fiélon à votre service. La créature doit se trouver à portée pendant
toute la durée du sort. (En général, la créature est d'abord convoquée
au centre d'un cercle magique inversé afin de la garder prisonnière
pendant que le sort est lancé). À la fin de l'incantation, la cible
doit effectuer un jet de sauvegarde de Charisme. En cas d'échec, elle
est liée à vous pour la durée du sort. Si la créature a été invoquée ou
créée par un autre sort, la durée de ce sort est prolongée pour
correspondre à la durée de ce sort.

Une créature liée doit suivre vos instructions au mieux de ses
capacités. Vous pouvez ordonner à la créature de vous accompagner dans
une aventure, de garder un lieu ou de délivrer un message. La créature
obéit à la lettre à vos instructions, mais si la créature vous est
hostile, elle s'efforce de déformer vos paroles pour atteindre ses
propres objectifs. Si la créature exécute complètement vos instructions
avant la fin du sort, elle se rend chez vous pour vous le signaler si
vous êtes sur le même plan d'existence. Si vous êtes sur un autre plan
d'existence, elle retourne à l'endroit où vous l'avez liée et y reste
jusqu'à la fin du sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de niveau supérieur, la durée passe à 10 jours
avec un emplacement de 6e niveau, à 30 jours avec un emplacement de 7e
niveau, à 180 jours avec un emplacement de 8e niveau et à un an et un
jour avec un emplacement de 9e niveau.



#### Changement de plan

*conjuration de 7ème niveau*

**Classes :** Clerc,
Sorcier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une tige métallique fourchue d'une valeur
d'au moins 250 gp, liée à un plan d'existence particulier).

**Durée :** Instantané

Vous et jusqu'à huit créatures consentantes qui se lient les mains en
cercle êtes transportés dans un autre plan d'existence. Vous pouvez
spécifier une destination cible en termes généraux, comme une ville
nommée sur un plan élémentaire ou un palais dans un plan inférieur, et
vous apparaissez dans ou près de cette destination. Si vous essayez
d'atteindre une ville, par exemple, vous pouvez arriver dans une rue,
devant un portail, ou regarder la ville depuis une mer, à la discrétion
du MJ.

Alternativement, si vous connaissez la séquence de sigles d'un cercle
de téléportation sur un autre plan d'existence, ce sort peut vous
amener à ce cercle. Si le cercle de téléportation est trop petit pour
contenir toutes les créatures que vous avez transportées, elles
apparaissent dans les espaces inoccupés les plus proches du cercle.

Vous pouvez utiliser ce sort pour bannir une créature non consentante
vers un autre plan. Choisissez une créature à votre portée et effectuez
une attaque de sort en mêlée contre elle. En cas de succès, la créature
doit effectuer un jet de sauvegarde de Charisme. Si la créature échoue,
elle est transportée à un endroit aléatoire du plan d'existence que
vous avez spécifié. Une créature ainsi transportée doit trouver son
propre chemin pour revenir dans votre plan d'existence actuel.



#### Croissance végétale

*3rd-level transmutation*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action ou 8 heures

**Portée :** 150 pieds

**Composantes :** V, S

**Durée :** Instantané

Ce sort canalise la vitalité des plantes dans une zone spécifique. Ce
sort peut être utilisé de deux façons, pour des bénéfices immédiats ou à
long terme.

Si vous lancez ce sort en utilisant 1 action, choisissez un point à
portée. Toutes les plantes normales dans un rayon de 100 pieds centré
sur ce point deviennent épaisses et envahies par la végétation. Une
créature se déplaçant dans la zone doit dépenser 4 pieds de mouvement
pour chaque 1 pied qu'elle déplace.

Vous pouvez exclure une ou plusieurs zones de n'importe quelle taille
dans la zone du sort d'être affectées.

Si vous lancez ce sort pendant 8 heures, vous enrichissez la terre.
Toutes les plantes situées dans un rayon de 800 mètres autour d'un
point à portée s'enrichissent pendant 1 an. Les plantes produisent deux
fois la quantité normale de nourriture lorsqu'elles sont récoltées.



#### Bouffée de poison

*Tour de magie de conjuration*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous tendez votre main vers une créature que vous pouvez voir à portée
et projetez une bouffée de gaz nocif de votre paume. La créature doit
réussir un jet de sauvegarde de Constitution ou subir 1d12 points de
dégâts de poison.

Les dégâts de ce sort augmentent de 1d12 lorsque vous atteignez le 5e
niveau (2d12), le 11e niveau (3d12) et le 17e niveau (4d12).



#### Métamorphose

*4th-level transmutation*

**Classes :** Barde,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un cocon de chenille)

**Durée :** Concentration, jusqu'à 1 heure

Ce sort transforme une créature que vous pouvez voir à portée en une
nouvelle forme. Une créature non consentante doit effectuer un jet de
sauvegarde de Sagesse pour éviter l'effet. Ce sort n'a aucun effet sur
un métamorphe ou une créature ayant 0 points de vie.

La transformation dure toute la durée du sort, ou jusqu'à ce que la
cible tombe à 0 points de vie ou meure. La nouvelle forme peut être
n'importe quelle bête dont la valeur de défi est égale ou inférieure à
celle de la cible (ou au niveau de la cible, si elle n'a pas de valeur
de défi). Les statistiques de jeu de la cible, y compris les scores de
capacité mentale, sont remplacées par les statistiques de la bête
choisie. Elle conserve son alignement et sa personnalité.

La cible assume les points de vie de sa nouvelle forme. Lorsqu'elle
revient à sa forme normale, la créature retrouve le nombre de points de
vie qu'elle avait avant sa transformation. Si elle revient à sa forme
normale après être tombée à 0 point de vie, les dégâts excédentaires
sont reportés sur sa forme normale. Tant que l'excès de dégâts ne
rapetisse pas la forme normale de la créature à 0 points de vie, elle
n'est pas frappée d'inconscience.

La créature est limitée dans les actions qu'elle peut effectuer par la
nature de sa nouvelle forme, et elle ne peut pas parler, lancer des
sorts, ou faire toute autre action qui nécessite des mains ou la parole.

L'équipement de la cible se fond dans sa nouvelle forme. La créature ne
peut pas activer, utiliser, manier ou bénéficier d'un quelconque de ses
équipements.



#### Mot de pouvoir mortel

*Enchantement de 9ème niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Vous prononcez un mot de pouvoir qui peut contraindre une créature que
vous pouvez voir à portée à mourir instantanément. Si la créature que
vous choisissez a 100 points de vie ou moins, elle meurt. Sinon, le sort
n'a aucun effet.



#### Mot de pouvoir étourdissant

*Enchantement de 8ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Vous prononcez un mot de pouvoir qui peut submerger l'esprit d'une
créature que vous pouvez voir à portée, la laissant abasourdie. Si la
cible a 150 points de vie ou moins, elle est étourdie. Sinon, le sort
n'a aucun effet.

La cible étourdie doit effectuer un jet de sauvegarde de Constitution à
la fin de chacun de ses tours. En cas de sauvegarde réussie, l'effet
d'étourdissement prend fin.



#### Prière de guérison

*Evocation de 2ème niveau*

**Classes :** Clerc

**Temps de coulée :** 10 minutes

**Portée :** 30 pieds

**Composantes :** V

**Durée :** Instantané

Jusqu'à six créatures de votre choix que vous pouvez voir à portée
regagnent chacune un nombre de points de vie égal à 2d8 + votre
modificateur de caractéristique d'incantation. Ce sort n'a aucun effet
sur les morts-vivants ou les constructions.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les soins augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 2e.



#### Prestidigitation

*Tour de magie de transmutation*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V, S

**Durée :** Jusqu'à 1 heure

Ce sort est un tour de magie mineur que les lanceurs de sorts novices
utilisent pour s'entraîner. Vous créez l'un des effets magiques
suivants à portée :

-   Vous créez un effet sensoriel instantané et inoffensif, comme une
    pluie d'étincelles, un souffle de vent, de faibles notes de musique
    ou une odeur.
-   Vous allumez ou éteignez instantanément une bougie, une torche ou un
    petit feu de camp.
-   Vous nettoyez ou souillez instantanément un objet dont la taille ne
    dépasse pas un pied cube.
-   Vous refroidissez, réchauffez ou aromatisez jusqu'à 1 pied cube de
    matériel non vivant pendant 1 heure.
-   Vous faites apparaître une couleur, une petite marque ou un symbole
    sur un objet ou une surface pendant 1 heure.
-   Vous créez un bibelot non magique ou une image illusoire qui peut
    tenir dans votre main et qui dure jusqu'à la fin de votre prochain
    tour.

Si vous lancez ce sort plusieurs fois, vous pouvez avoir jusqu'à trois
de ses effets non instantanés actifs à la fois, et vous pouvez annuler
un tel effet comme une action.



#### Rayons prismatiques

*Evocation de 7ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Moi-même (cône de 60 pieds)

**Composantes :** V, S

**Durée :** Instantané

Huit rayons de lumière multicolores jaillissent de votre main. Chaque
rayon est d'une couleur différente et a un pouvoir et un but
différents. Chaque créature dans un cône de 60 pieds doit effectuer un
jet de sauvegarde de Dextérité. Pour chaque cible, lancez un d8 pour
déterminer quel rayon de couleur l'affecte.

1.  ***Rouge.*** La cible subit 10d6 points de dégâts de feu en cas
    d'échec, ou la moitié en cas de réussite.

2.  ***Orange.*** La cible subit 10d6 points de dégâts d'acide en cas
    d'échec, ou la moitié en cas de réussite.

3.  ***Jaune.*** La cible subit 10d6 points de dégâts de foudre en cas
    d'échec, ou la moitié en cas de réussite.

4.  ***Vert.*** La cible subit 10d6 points de dégâts de poison en cas
    d'échec, ou la moitié en cas de réussite.

5.  ***Bleu.*** La cible subit 10d6 points de dégâts de froid en cas
    d'échec, ou la moitié en cas de réussite.

6.  ***Indigo.*** En cas d'échec, la cible est entravée. Elle doit
    alors effectuer un jet de sauvegarde de Constitution à la fin de
    chacun de ses tours. Si elle réussit son jet de sauvegarde trois
    fois, le sort prend fin. Si elle échoue trois fois à son jet de
    sauvegarde, elle se transforme définitivement en pierre et est
    soumise à l'état pétrifié. Les succès et les échecs n'ont pas
    besoin d'être consécutifs ; tenez compte des deux jusqu'à ce que
    la cible obtienne trois succès de ce type.

7.  ***Violet.*** En cas d'échec, la cible est aveuglée. Elle doit
    alors effectuer un jet de sauvegarde de Sagesse au début de votre
    prochain tour. Un jet de sauvegarde réussi met fin à la cécité. En
    cas d'échec, la créature est transportée dans un autre plan
    d'existence au choix du MJ et n'est plus aveuglée. (En général,
    une créature qui se trouve sur un plan qui n'est pas son plan
    d'origine est bannie chez elle, tandis que les autres créatures
    sont généralement projetées dans les plans astral ou éthéré).

8.  ***Spécial.*** La cible est frappée par deux rayons. Lancez deux
    fois de plus, en relançant tout 8.



#### Mur prismatique

*l'ab ab ab ab ab ab ab ab ab l'ab de 9e niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** 10 minutes

Un plan de lumière multicolore et scintillant forme un mur vertical
opaque - jusqu'à 90 pieds de long, 30 pieds de haut et 1 pouce
d'épaisseur - centré sur un point que vous pouvez voir à portée. Vous
pouvez également donner au mur la forme d'une sphère d'un diamètre
maximal de 10 mètres centrée sur un point de votre choix situé à portée.
Le mur reste en place pendant toute la durée du sort. Si vous
positionnez le mur de manière à ce qu'il traverse un espace occupé par
une créature, le sort échoue, et votre action et l'emplacement de sort
sont gaspillés.

Le mur diffuse une lumière vive sur une portée de 30 mètres et une
lumière chétive sur 30 mètres supplémentaires. Vous et les créatures que
vous désignez au moment où vous lancez le sort pouvez traverser le mur
et rester à proximité sans être blessés. Si une autre créature qui peut
voir le mur se déplace à moins de 6 mètres de celui-ci ou commence son
tour à cet endroit, elle doit réussir un jet de sauvegarde de
Constitution ou devenir aveugle pendant 1 minute.

Le mur est composé de sept couches, chacune ayant une couleur
différente. Lorsqu'une créature tente d'atteindre ou de traverser le
mur, elle le fait couche par couche à travers toutes les couches du mur.
Lorsqu'elle traverse ou atteint chaque couche, la créature doit
effectuer un jet de sauvegarde de Dextérité ou être affectée par les
propriétés de cette couche, comme décrit ci-dessous.

Le mur peut être détruit, également une couche à la fois, dans l'ordre
du rouge au violet, par des moyens spécifiques à chaque couche. Une fois
qu'une couche est détruite, elle le reste pour toute la durée du sort.
Une *baguette d'annulation* détruit un *mur
prismatique*, mais un *champ
antimagique* n'a aucun effet sur lui.

1.  ***Rouge.*** La créature subit 10d6 points de dégâts de feu en cas
    de sauvegarde ratée, ou la moitié en cas de réussite. Tant que cette
    couche est en place, les attaques à distance non magiques ne peuvent
    pas traverser le mur. La couche peut être détruite en lui infligeant
    au moins 25 points de dégâts de froid.

2.  ***Orange.*** La créature subit 10d6 dégâts d'acide en cas de
    sauvegarde ratée, ou la moitié en cas de réussite. Tant que cette
    couche est en place, les attaques magiques à distance ne peuvent pas
    traverser le mur. La couche est détruite par un vent fort.

3.  ***Jaune.*** La créature subit 10d6 points de dégâts de foudre en
    cas de sauvegarde ratée, ou la moitié en cas de réussite. Cette
    couche peut être détruite en lui infligeant au moins 60 points de
    dégâts de force.

4.  ***Vert.*** La créature subit 10d6 points de dégâts de poison en cas
    de sauvegarde ratée, ou la moitié en cas de sauvegarde réussie. Un
    sort de *passe-muraille*, ou un autre sort de niveau
    égal ou supérieur capable d'ouvrir un portail sur une surface
    solide, détruit cette couche.

5.  ***Bleu.*** La créature subit 10d6 points de dégâts de froid en cas
    de sauvegarde ratée, ou la moitié en cas de réussite. Cette couche
    peut être détruite en lui infligeant au moins 25 points de dégâts de
    feu.

6.  ***Indigo.*** En cas d'échec, la créature est entravée. Elle doit
    ensuite effectuer un jet de sauvegarde de Constitution à la fin de
    chacun de ses tours. Si elle réussit son jet de sauvegarde trois
    fois, le sort prend fin. Si elle échoue trois fois à son jet de
    sauvegarde, elle se transforme définitivement en pierre et est
    soumise à l'état pétrifié. Les succès et les échecs n'ont pas
    besoin d'être consécutifs ; tenez-en compte jusqu'à ce que la
    créature en obtienne trois de la même sorte.

    Tant que cette couche est en place, les sorts ne peuvent pas être
    lancés à travers le mur. La couche est détruite par la lumière vive
    diffusée par un sort de *lumière du jour* ou un sort
    similaire de niveau égal ou supérieur.

7.  ***Violet.*** En cas d'échec, la créature est aveuglée. Elle doit
    alors effectuer un jet de sauvegarde de Sagesse au début de votre
    prochain tour. Un jet de sauvegarde réussi met fin à la cécité. En
    cas d'échec, la créature est transportée dans un autre plan au
    choix du MJ et n'est plus aveuglée. (En général, une créature qui
    se trouve sur un plan qui n'est pas son plan d'origine est bannie
    chez elle, tandis que les autres créatures sont généralement
    projetées dans les plans astral ou éthéré). Ce plan est détruit par
    un sort de *dissipation de la magie* ou un sort
    similaire de niveau égal ou supérieur qui peut mettre fin aux sorts
    et aux effets magiques.



#### Sanctuaire privé

*4th-level abjuration*

**Classes :** Magicien

**Temps de coulée :** 10 minutes

**Portée :** 120 pieds

**Composantes :** V, S, M (une fine feuille de plomb, un morceau de
verre opaque, une bourre de coton ou de tissu et de la chrysolite en
poudre).

**Durée :** 24 heures

Vous sécurisez magiquement une zone à portée. La zone est un cube qui
peut être aussi petit que 5 pieds et aussi grand que 100 pieds de chaque
côté. Le sort dure toute la durée ou jusqu'à ce que vous utilisiez une
action pour l'annuler.

Lorsque vous lancez le sort, vous décidez du type de sécurité qu'il
procure, en choisissant une ou plusieurs des propriétés suivantes :

-   Le son ne peut pas passer à travers la barrière au bord de la zone
    surveillée.
-   La barrière de la zone surveillée apparaît sombre et brumeuse,
    empêchant la vision (y compris la vision dans le noir) à travers
    elle.
-   Les capteurs créés par des sorts de divination ne peuvent pas
    apparaître à l'intérieur de la zone de protection ni traverser la
    barrière à son périmètre.
-   Les créatures de la zone ne peuvent pas être ciblées par les sorts
    de divination.
-   Rien ne peut se téléporter dans ou hors de la zone surveillée.
-   Les déplacements planaires sont bloqués dans la zone surveillée.

Lancer ce sort au même endroit tous les jours pendant un an rend cet
effet permanent.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 5e niveau ou plus, vous pouvez augmenter la
taille du cube de 100 pieds pour chaque niveau d'emplacement au-delà du
4e. Ainsi, vous pouvez protéger un cube qui peut faire jusqu'à 200
pieds de côté en utilisant un emplacement de sort de 5e niveau.



#### Produire des flammes

*Tour de magie de conjuration*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** 10 minutes

Une flamme vacillante apparaît dans votre main. La flamme reste là
pendant toute la durée de l'opération et ne nuit ni à vous ni à votre
équipement. La flamme diffuse une lumière vive dans un rayon de 3 mètres
et une lumière chétive sur 3 mètres supplémentaires. Le sort prend fin
si vous l'annulez par une action ou si vous le lancez à nouveau.

Vous pouvez également attaquer avec la flamme, mais cela met fin au
sort. Lorsque vous lancez ce sort, ou en tant qu'action lors d'un tour
ultérieur, vous pouvez projeter la flamme sur une créature située à
moins de 10 mètres de vous. Effectuez une attaque de sort à distance. En
cas de succès, la cible subit 1d8 points de dégâts de feu.

Les dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le 5e
niveau (2d8), le 11e niveau (3d8) et le 17e niveau (4d8).



#### Illusion programmée

*illusion de 6e niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un peu de toison et de la poussière de jade
valant au moins 25 gp)

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous créez une illusion d'un objet, d'une créature ou de tout autre
phénomène visible à portée qui s'active lorsqu'un étât spécifique se
produit. L'illusion est imperceptible jusque là. Elle ne doit pas être
plus grande qu'un cube de 30 pieds, et vous décidez au moment où vous
lancez le sort comment l'illusion se comporte et quels sons elle émet.
Cette représentation scénarisée peut durer jusqu'à 5 minutes.

Lorsque l'étât que vous spécifiez se produit, l'illusion surgit et se
produit de la manière que vous avez décrite. Une fois que l'illusion a
fini de fonctionner, elle disparaît et reste en sommeil pendant 10
minutes. Après cette période, l'illusion peut être réactivée.

La condition de déclenchement peut être aussi générale ou aussi
détaillée que vous le souhaitez, mais elle doit être basée sur des
conditions visuelles ou auditives qui se produisent dans un rayon de 30
pieds autour de la zone. Par exemple, vous pouvez créer une illusion de
vous-même pour qu'elle apparaisse et mette en garde ceux qui tentent
d'ouvrir une porte piégée, ou vous pouvez régler l'illusion pour
qu'elle ne se déclenche que lorsqu'une créature prononce le bon mot ou
la bonne phrase.

L'interaction physique avec l'image révèle qu'il s'agit d'une
illusion, car les choses peuvent la traverser. Une créature qui utilise
son action pour examiner l'image peut déterminer qu'il s'agit d'une
illusion en réussissant un test d'Intelligence (Investigation) contre
votre DC de sauvegarde contre les sorts. Si une créature discerne
l'illusion pour ce qu'elle est, la créature peut voir à travers
l'image, et tout bruit qu'elle fait sonne creux pour la créature.



#### Projection d'image

*Ill illusion du 7ème niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 800 km

**Composantes :** V, S, M (une petite réplique de vous fabriquée à
partir de matériaux valant au moins 5 gp).

**Durée :** Concentration, jusqu'à 1 jour

Vous créez une copie illusoire de vous-même qui dure le temps
nécessaire. La copie peut apparaître à n'importe quel endroit à portée
que vous avez déjà vu, sans tenir compte des obstacles. L'illusion vous
ressemble, mais elle est intangible. Si l'illusion subit des dégâts,
elle disparaît et le sort prend fin.

Vous pouvez utiliser votre action pour déplacer cette illusion jusqu'à
deux fois votre vitesse, et lui faire faire des gestes, parler et se
comporter comme vous le souhaitez. Elle imite parfaitement vos manières.

Vous pouvez voir par ses yeux et entendre par ses oreilles comme si vous
étiez dans son espace. À votre tour, comme action bonus, vous pouvez
passer de l'utilisation de ses sens à l'utilisation des vôtres, ou
inversement. Lorsque vous utilisez ses sens, vous êtes aveuglé et
assourdi par rapport à votre propre environnement.

L'interaction physique avec l'image révèle qu'il s'agit d'une
illusion, car les choses peuvent la traverser. Une créature qui utilise
son action pour examiner l'image peut déterminer qu'il s'agit d'une
illusion en réussissant un test d'Intelligence (Investigation) contre
votre DC de sauvegarde contre les sorts. Si une créature discerne
l'illusion pour ce qu'elle est, la créature peut voir à travers
l'image, et tout bruit qu'elle fait sonne creux pour la créature.



#### Protection contre une énergie

*3rd-level abjuration*

**Classes :** Clerc,
Rôdeur,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 heure

Pour la durée, la créature consentante que vous touchez résiste à un
type de dégâts de votre choix : acide, froid, feu, foudre ou tonnerre.



#### Protection contre le Bien et le Mal

*abjuration de 1er niveau*

**Classes :** Clerc,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (eau bénite ou poudre d'argent et de fer, que
le sort consomme).

**Durée :** Concentration jusqu'à 10 minutes

Jusqu'à la fin du sort, une créature volontaire que vous touchez est
protégée contre certains types de créatures : aberrations, célestes,
élémentaires, fées, fiélons et morts-vivants.

La protection confère plusieurs avantages. Les créatures de ce type ont
un désavantage aux jets d'attaque contre la cible. La cible ne peut pas
non plus être charmée, effrayée ou possédée par elles. Si la cible est
déjà charmée, effrayée ou possédée par une telle créature, elle a un
avantage sur tout nouveau jet de sauvegarde contre l'effet en question.



#### Protection contre le poison

*l'ab juration de 2ème niveau*

**Classes :** Clerc,
Paladin

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** 1 heure

Vous touchez une créature. Si elle est empoisonnée, vous neutralisez le
poison. Si plus d'un poison affecte la cible, vous neutralisez un
poison que vous savez présent, ou vous en neutralisez un au hasard.

Pour la durée de l'action, la cible a un avantage aux jets de
sauvegarde contre l'empoisonnement, et elle a une résistance aux dégâts
du poison.



#### Purification de nourriture et d'eau

*Transmutation de 1er niveau (rituel)*

**Classes :** Clerc,
Paladin

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V, S

**Durée :** Instantané

Toute la nourriture et les boissons non magiques situées dans une sphère
de 1,5 m de rayon centrée sur un point de votre choix situé dans la
portée sont purifiées et rendues exemptes de poison et de maladie.



#### Rappel à la vie

*Nécromancie de 5e niveau*

**Classes :** Barde,
paladin

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (un diamant d'une valeur d'au moins 500 gp,
que le sort consomme).

**Durée :** Instantané

Vous ramenez à la vie une créature morte que vous touchez, à condition
qu'elle ne soit pas morte depuis plus de 10 jours. Si l'âme de la
créature est à la fois disposée et libre de rejoindre le corps, la
créature revient à la vie avec 1 point de vie.

Ce sort neutralise également tous les poisons et guérit les maladies non
magiques qui affectaient la créature au moment de sa mort. Cependant, ce
sort ne supprime pas les maladies magiques, les malédictions ou les
effets similaires ; si ceux-ci n'ont pas été supprimés avant de lancer
le sort, ils prennent effet lorsque la créature revient à la vie. Ce
sort ne peut pas ramener à la vie une créature morte-vivante.

Ce sort referme toutes les blessures mortelles, mais il ne restaure pas
les parties du corps manquantes. Si la créature est privée de parties de
son corps ou d'organes essentiels à sa survie (sa tête, par exemple),
le sort échoue automatiquement.

Revenir d'entre les morts est une épreuve. La cible subit une pénalité
de -4 à tous ses jets d'attaque, jets de sauvegarde et tests de
capacité. Chaque fois que la cible termine un repos long, la pénalité
est réduite de 1 jusqu'à ce qu'elle disparaisse.



#### Rayon affaiblissant

*Nécromancie de 2ème niveau*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Un rayon noir d'énergie énervante jaillit de votre doigt vers une
créature à portée. Effectuez une attaque de sort à distance contre la
cible. En cas de succès, la cible n'inflige que la moitié des dégâts
avec les attaques d'armes qui utilisent la Force jusqu'à la fin du
sort.

À la fin de chacun de ses tours, la cible peut effectuer un jet de
sauvegarde de Constitution contre le sort. En cas de réussite, le sort
prend fin.



#### Rayon de givre

*Tour de magie d'évocation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Un rayon glacial de lumière bleu-blanc se dirige vers une créature à
portée. Effectuez une attaque de sort à distance contre la cible. Si
elle est touchée, elle subit 1d8 points de dégâts de froid et sa vitesse
est réduite de 3 mètres jusqu'au début de votre prochain tour.

Les dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le 5e
niveau (2d8), le 11e niveau (3d8) et le 17e niveau (4d8).



#### Régénération

*Transmutation de niveau 7*

**Classes :** Barde,
Druide

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (un moulin à prières et de l'eau bénite)

**Durée :** 1 heure

Vous touchez une créature et stimulez sa capacité de guérison naturelle.
La cible regagne 4d8 + 15 points de vie. Pendant la durée du sort, la
cible regagne 1 point de vie au début de chacun de ses tours (10 points
de vie chaque minute).

Les membres sectionnés du corps de la cible (doigts, jambes, queues,
etc.), s'il y en a, sont restaurés après 2 minutes. Si vous tenez la
partie sectionnée et la maintenez contre la souche, le sort provoque
instantanément le tricotage du membre à la souche.



#### Réincarnation

*Transmutation de 5ème niveau*

**Classes :** Druide

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (huiles et onguents rares d'une valeur d'au
moins 1 000 gp, que le sort consomme).

**Durée :** Instantané

Vous touchez un humanoïde mort ou un morceau d'humanoïde mort. À
condition que la créature soit morte depuis moins de 10 jours, le sort
lui forme un nouveau corps d'adulte, puis appelle l'âme à entrer dans
ce corps. Si l'âme de la cible n'est pas libre ou disposée à le faire,
le sort échoue.

La magie façonne un nouveau corps pour la créature à habiter, ce qui
entraîne probablement un changement de origine de la créature. Le MJ lance
un d100 et consulte la table suivante pour déterminer la forme que prend
la créature lorsqu'elle est ramenée à la vie, ou le MJ choisit une
forme.

  ---------------------------
   d100   Origine
  ------- -------------------
   01-04  Drakonide

   05-13  Nain, colline

   14-21  Nain, de montagne

   22-25  Elfe, Ténèbres

   26-34  Elfe, haut-elfe

   35-42  Elfe, bois

   43-46  Gnome, forêt

   47-52  Gnome, rocher

   53-56  Demi-elfe

   57-60  Demi-orc

   61-68  Halfling,
          pied-léger

   69-76  Halfelin, robuste

   77-96  Humain

   97-00  Tiefelin
  ---------------------------

La créature réincarnée se souvient de son ancienne vie et de ses
expériences. Elle conserve les capacités qu'elle avait sous sa forme
originelle, mais elle échange son origine initiale contre la nouvelle et
modifie ses traits raciaux en conséquence.



#### Délivrance des malédictions

*3rd-level abjuration*

**Classes :** Clerc,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

À votre contact, toutes les malédictions affectant une créature ou un
objet prennent fin. Si l'objet est un objet magique maudit, la
malédiction demeure, mais le sort brise l'attachement de son
propriétaire à l'objet afin qu'il puisse être retiré ou jeté.



#### Sphère Résiliente

*év évocation de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un morceau hémisphérique de cristal clair et
un morceau hémisphérique correspondant de gomme arabique).

**Durée :** Concentration, jusqu'à 1 minute

Une sphère de force chatoyante entoure une créature ou un objet de
taille Grand ou inférieure à portée. Une créature non consentante doit
effectuer un jet de sauvegarde de Dextérité. En cas d'échec, la
créature est enfermée pour la durée du jet.

Rien - ni les objets physiques, ni l'énergie, ni les autres effets des
sorts - ne peut traverser la barrière, que ce soit à l'intérieur ou à
l'extérieur, bien qu'une créature dans la sphère puisse y respirer. La
sphère est immunisée contre tous les dégâts, et une créature ou un objet
à l'intérieur ne peut pas être endommagé par des attaques ou des effets
provenant de l'extérieur, et une créature à l'intérieur de la sphère
ne peut pas non plus endommager quoi que ce soit à l'extérieur.

La sphère est en apesanteur et juste assez grande pour contenir la
créature ou l'objet qui s'y trouve. Une créature enfermée peut
utiliser son action pour pousser contre les parois de la sphère et ainsi
faire rouler la sphère jusqu'à la moitié de la vitesse de la créature.
De même, le globe peut être ramassé et déplacé par d'autres créatures.

Un sort de *désintégration* visant le globe le détruit
sans blesser quoi que ce soit à l'intérieur.



#### Résistance

*Tour de magie d'abjuration*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une cape miniature)

**Durée :** Concentration, jusqu'à 1 minute

Vous touchez une créature consentante. Une fois avant la fin du sort, la
cible peut lancer un d4 et ajouter le nombre obtenu à un jet de
sauvegarde de son choix. Elle peut lancer le dé avant ou après avoir
effectué le jet de sauvegarde. Le sort prend alors fin.



#### Résurrection

*Nécromancie de 7ème niveau*

**Classes :** Barde

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (un diamant d'au moins 1 000 gp, que le sort
consomme).

**Durée :** Instantané

Vous touchez une créature morte depuis moins d'un siècle, qui n'est
pas morte de vieillesse et qui n'est pas morte-vivante. Si son âme est
libre et consentante, la cible revient à la vie avec tous ses points de
vie.

Ce sort neutralise tous les poisons et guérit les maladies normales dont
souffrait la créature au moment de sa mort. Cependant, il ne supprime
pas les maladies magiques, les malédictions, etc. Si ces effets ne sont
pas supprimés avant le lancement du sort, ils affligent la cible à son
retour à la vie.

Ce sort referme toutes les blessures mortelles et restaure les parties
du corps manquantes.

Revenir d'entre les morts est une épreuve. La cible subit une pénalité
de -4 à tous ses jets d'attaque, jets de sauvegarde et tests de
capacité. Chaque fois que la cible termine un repos long, la pénalité
est réduite de 1 jusqu'à ce qu'elle disparaisse.

Lancer ce sort pour redonner la vie à une créature morte depuis un an ou
plus vous taxe énormément. Jusqu'à ce que vous ayez terminé un long
repos, vous ne pouvez plus lancer de sorts et vous avez un désavantage
sur tous les jets d'attaque, les tests de capacité et les jets de
sauvegarde.



#### Inversion de la gravité

*Transmutation de 7ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 100 pieds

**Composantes :** V, S, M (une pierre d'habitation et de la limaille de
fer).

**Durée :** Concentration, jusqu'à 1 minute

Ce sort inverse la gravité dans un cylindre de 50 pieds de rayon et de
100 pieds de haut centré sur un point à portée. Toutes les créatures et
tous les objets qui ne sont pas ancrés au sol dans la zone tombent vers
le haut et atteignent le sommet de la zone lorsque vous lancez ce sort.
Une créature peut faire un jet de sauvegarde de Dextérité pour
s'accrocher à un objet fixe qu'elle peut atteindre, évitant ainsi la
chute.

Si un objet solide (tel qu'un plafond) est rencontré au cours de cette
chute, les objets et créatures qui tombent le frappent comme ils le
feraient lors d'une chute normale vers le bas. Si un objet ou une
créature atteint le sommet de la zone sans rien heurter, il y reste,
oscillant légèrement, pour la durée de la chute.

À la fin de la durée, les objets et créatures affectés retombent.



#### Retour à la vie

*Nécromancie de 3e niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (diamants d'une valeur de 300 gp, que le sort
consomme)

**Durée :** Instantané

Vous touchez une créature qui est morte au cours de la dernière minute.
Cette créature revient à la vie avec 1 point de vie. Ce sort ne peut pas
ramener à la vie une créature morte de vieillesse, ni restaurer les
parties du corps manquantes.



#### Corde enchantée

*transmutation de 2ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (extrait de maïs en poudre et une boucle de
parchemin torsadée)

**Durée :** 1 heure

Vous touchez une corde d'une longueur maximale de 18 mètres. Une
extrémité de la corde s'élève alors dans les airs jusqu'à ce que toute
la corde soit suspendue perpendiculairement au sol. À l'extrémité
supérieure de la corde, une entrée invisible s'ouvre sur un espace
extradimensionnel qui dure jusqu'à la fin du sort.

L'espace extradimensionnel peut être atteint en grimpant au sommet de
la corde. L'espace peut contenir jusqu'à huit créatures moyennes ou
plus petites. La corde peut être tirée dans l'espace, ce qui la fait
disparaître de la vue à l'extérieur de l'espace.

Les attaques et les sorts ne peuvent pas traverser l'entrée ou la
sortie de l'espace extradimensionnel, mais ceux qui sont à l'intérieur
peuvent voir à l'extérieur comme à travers une fenêtre d'un mètre sur
deux centrée sur la corde.

Tout ce qui se trouve dans l'espace extradimensionnel en sort à la fin
du sort.



#### Flamme sacrée

*Tour de magie d'évocation*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Instantané

Un rayonnement de flammes s'abat sur une créature que vous pouvez voir
à portée. La cible doit réussir un jet de sauvegarde de Dextérité ou
subir 1d8 points de dégâts radiants.

La cible ne bénéficie pas de l'abri pour ce jet de sauvegarde. Les
dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le 5e niveau
(2d8), le 11e niveau (3d8) et le 17e niveau (4d8).



#### Sanctuaire

*abjuration de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action bonus

**Portée :** 30 pieds

**Composantes :** V, S, M (un petit miroir en argent)

**Durée :** 1 minute

Vous protégez une créature à portée contre les attaques. Jusqu'à la fin
du sort, toute créature qui cible la créature parée avec une attaque ou
un sort nuisible doit d'abord effectuer un jet de sauvegarde de
Sagesse. En cas d'échec, la créature doit choisir une nouvelle cible ou
perdre l'attaque ou le sort. Ce sort ne protège pas la créature sous
surveillance des effets de zone, comme l'explosion d'une boule de feu.

Si la créature sous surveillance effectue une attaque ou lance un sort
qui affecte une créature ennemie, ce sort prend fin.



#### Rayon ardent

*Evocation de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Instantané

Vous créez trois rayons de feu et les lancez sur des cibles à portée.
Vous pouvez les lancer sur une ou plusieurs cibles.

Effectuez une attaque de sort à distance pour chaque rayon. En cas de
succès, la cible subit 2d6 points de dégâts de feu.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 3e niveau ou plus, vous créez un rayon
supplémentaire pour chaque niveau d'emplacement supérieur au 2e.



#### Scrutation

*Divination de 5e niveau*

**Classes :** Barde,
Druide,
Magicien

**Temps de coulée :** 10 minutes

**Portée :** Self

**Composantes :** V, S, M (un foyer d'une valeur d'au moins 1 000 gp,
tel qu'une boule de cristal, un miroir en argent ou un bénitier rempli
d'eau bénite)

**Durée :** Concentration, jusqu'à 10 minutes

Vous pouvez voir et entendre une créature particulière de votre choix
qui se trouve sur le même plan d'existence que vous. La cible doit
effectuer un jet de sauvegarde de Sagesse, qui est modifié par la façon
dont vous connaissez la cible et le type de lien physique que vous avez
avec elle. Si la cible sait que vous lancez ce sort, elle peut échouer
le jet de sauvegarde volontairement si elle veut être observée.

  ----------------------------------------------------------------
  Connaissances                                     Enregistrer le
                                                     modificateur
  ------------------------------------------------- --------------
  Secondaire (vous avez entendu parler de la cible)       +5

  De première main (vous avez rencontré la cible)         +0

  Familier (vous connaissez bien la cible)                -5
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  Connexion                                         Enregistrer le
                                                     modificateur
  ------------------------------------------------- --------------
  Sentiment ou image                                      -2

  Possession ou vêtement                                  -4

  Partie du corps, mèche de cheveux, bout d'ongle,      -10
  etc.                                              
  ----------------------------------------------------------------

En cas de sauvegarde réussie, la cible n'est pas affectée et vous ne
pouvez plus utiliser ce sort contre elle pendant 24 heures.

En cas d'échec, le sort crée un capteur invisible dans un rayon de 3
mètres autour de la cible. Vous pouvez voir et entendre à travers le
capteur comme si vous y étiez. Le capteur se déplace avec la cible,
restant à 3 mètres d'elle pendant toute la durée du sort. Une créature
capable de voir les objets invisibles voit le capteur comme un orbe
lumineux de la taille de votre poing.

Au lieu de cibler une créature, vous pouvez choisir un emplacement que
vous avez déjà vu comme cible de ce sort. Lorsque vous le faites, le
capteur apparaît à cet endroit et ne se déplace pas.



#### Coffre secret

*conjuration de 4ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un coffre exquis de 3 pieds sur 2 pieds sur 2
pieds, construit à partir de matériaux rares d'une valeur d'au moins 5
000 gp, et une Très petite (TP) réplique faite des mêmes matériaux
d'une valeur d'au moins 50 gp).

**Durée :** Instantané

Vous cachez un coffre, et tout son contenu, sur le plan éthéré. Vous
devez toucher le coffre et la réplique miniature qui sert de composante
matérielle au sort. Le coffre peut contenir jusqu'à 12 pieds cubes de
matière non vivante (3 pieds par 2 pieds par 2 pieds).

Tant que le coffre reste sur le plan éthéré, vous pouvez utiliser une
action et toucher la réplique pour rappeler le coffre. Il apparaît dans
un espace inoccupé sur le sol à moins de 1,5 m de vous. Vous pouvez
renvoyer le coffre dans le plan éthéré en utilisant une action et en
touchant à la fois le coffre et la réplique.

Après 60 jours, il y a une chance cumulative de 5 % par jour que
l'effet du sort prenne fin. Cet effet prend fin si vous lancez à
nouveau ce sort, si le petit coffre réplique est détruit, ou si vous
choisissez de mettre fin au sort par une action. Si le sort prend fin et
que le grand coffre se trouve sur le plan éthéré, il est
irrémédiablement perdu.



#### Voir l'Invisibilité

*Divination de 2ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (une pincée de talc et une petite pincée de
poudre d'argent).

**Durée :** 1 heure

Pendant la durée de ce don, vous voyez les créatures et les objets
invisibles comme s'ils étaient visibles, et vous pouvez voir dans le
plan éthéré. Les créatures et objets éthérés apparaissent fantomatiques
et translucides.



#### Apparence trompeuse

*Ill illusion de 5ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S

**Durée :** 8 heures

Ce sort vous permet de changer l'apparence d'un nombre quelconque de
créatures que vous pouvez voir à portée. Vous donnez à chaque cible que
vous choisissez une nouvelle apparence illusoire. Une cible non
consentante peut faire un jet de sauvegarde de Charisme, et si elle
réussit, elle n'est pas affectée par ce sort.

Le sort déguise l'apparence physique ainsi que les vêtements, armures,
armes et équipements. Vous pouvez faire en sorte que chaque créature
semble plus petite ou plus grande d'un pied et paraître mince, grosse
ou entre les deux. Vous ne pouvez pas changer le type de corps d'une
cible, vous devez donc choisir une forme qui a la même disposition de
base des membres. Sinon, l'ampleur de l'illusion est à votre
discrétion. Le sort dure toute la durée du sort, à moins que vous
n'utilisiez votre action pour l'annuler plus tôt.

Les changements opérés par ce sort ne résistent pas à l'inspection
physique. Par exemple, si vous utilisez ce sort pour ajouter un chapeau
à la tenue d'une créature, les objets passent à travers le chapeau, et
quiconque le touche ne sentira rien ou sentira la tête et les cheveux de
la créature. Si vous utilisez ce sort pour paraître plus mince que vous
ne l'êtes, la main de quelqu'un qui se tend pour vous toucher se
heurtera à vous alors qu'elle est apparemment toujours en l'air.

Une créature peut utiliser son action pour inspecter une cible et faire
un test d'Intelligence (Investigation) contre le DC de sauvegarde de
votre sort. Si elle réussit, elle se rend compte que la cible est
déguisée.



#### Envoi de

*3rd-level evocation*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Illimité

**Composantes :** V, S, M (un court morceau de fil de cuivre fin)

**Durée :** 1 round

Vous envoyez un court message de vingt-cinq mots ou moins à une créature
qui vous est familière. La créature entend le message dans son esprit,
vous reconnaît comme l'expéditeur si elle vous connaît, et peut
répondre de la même manière immédiatement. Ce sort permet aux créatures
ayant un score d'Intelligence d'au moins 1 de comprendre la
signification de votre message.

Vous pouvez envoyer le message sur n'importe quelle distance et même
sur d'autres plans d'existence, mais si la cible se trouve sur un
autre plan que le vôtre, il y a 5 % de chances que le message n'arrive
pas.



#### Dissimulation

*Transmutation de niveau 7*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une poudre composée de poussière de diamant,
d'émeraude, de rubis et de saphir d'une valeur d'au moins 5 000 gp,
que le sort consomme).

**Durée :** Jusqu'à ce qu'il soit dissipé

Grâce à ce sort, une créature volontaire ou un objet peut être caché, à
l'abri de toute détection pour la durée du sort. Lorsque vous lancez le
sort et que vous touchez la cible, celle-ci devient invisible et ne peut
être ciblée par les sorts de divination ou perçue par les capteurs de
scrutation créés par les sorts de divination.

Si la cible est une créature, elle tombe dans un état d'animation
suspendue. Le temps cesse de s'écouler pour elle, et elle ne vieillit
pas.

Vous pouvez définir une condition pour que le sort prenne fin plus tôt.
L'étât peut être tout ce que vous choisissez, mais il doit se produire
ou être visible à moins d'un kilomètre de la cible. Par exemple,
\"après 1 000 ans\" ou \"lorsque la Tarasque se réveillera\". Ce sort
prend également fin si la cible subit des dégâts.



#### Changement de forme

*Trans transmutation du 9e niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S, M (un cercle de jade d'une valeur d'au moins 1
500 gp, que vous devez placer sur votre tête avant de lancer le sort).

**Durée :** Concentration, jusqu'à 1 heure

Vous prenez la forme d'une créature différente pour la durée du sort.
Cette nouvelle forme peut être celle de n'importe quelle créature dont
le niveau de défi est égal ou inférieur à votre niveau. La créature ne
peut pas être une construction ou un mort-vivant, et vous devez avoir vu
ce type de créature au moins une fois. Vous vous transformez en un
exemple moyen de cette créature, sans niveau de classe ni trait
d'incantation.

Vos statistiques de jeu sont remplacées par celles de la créature
choisie, mais vous conservez votre alignement et vos scores
d'Intelligence, de Sagesse et de Charisme. Vous conservez également
toutes vos compétences et jet de sauvegarde, en plus de gagner celles de
la créature. Si la créature a la même maîtrise que vous et que le bonus
indiqué dans ses statistiques est supérieur au vôtre, utilisez le bonus
de la créature à la place du vôtre. Vous ne pouvez pas utiliser les
actions légendaires ou les actions de repaire de la nouvelle forme.

Vous assumez les points de vie et les dés de réussite de la nouvelle
forme. Lorsque vous reprenez votre forme normale, vous retrouvez le
nombre de points de vie que vous aviez avant votre transformation. Si
vous revenez à votre forme normale après être tombé à 0 point de vie,
tout dommage excédentaire est reporté sur votre forme normale. Tant que
l'excès de dégâts ne rapetisse pas votre forme normale à 0 points de
vie, vous n'êtes pas frappé d'inconscience.

Vous conservez le bénéfice de toutes les caractéristiques de votre
classe, origine ou autre source et pouvez les utiliser, à condition que
votre nouvelle forme en soit physiquement capable. Vous ne pouvez pas
utiliser les sens spéciaux que vous possédez (par exemple, la vision
dans le noir) à moins que votre nouvelle forme ne possède également ce
sens. Vous ne pouvez parler que si la créature peut normalement parler.

Lorsque vous vous transformez, vous choisissez si votre équipement tombe
au sol, se fond dans la nouvelle forme ou est porté par celle-ci.
L'équipement porté fonctionne normalement. Le MJ détermine s'il est
pratique pour la nouvelle forme de porter un équipement, en fonction de
la forme et de la taille de la créature. Votre équipement ne change pas
de forme ou de taille pour s'adapter à la nouvelle forme, et tout
équipement que la nouvelle forme ne peut pas porter doit soit tomber au
sol, soit fusionner avec votre nouvelle forme. L'équipement qui
fusionne n'a aucun effet dans cet état.

Pendant la durée de ce sort, vous pouvez utiliser votre action pour
prendre une autre forme en suivant les mêmes restrictions et règles que
pour la forme originale, à une exception près : si votre nouvelle forme
a plus de points de vie que votre forme actuelle, vos points de vie
restent à leur valeur actuelle.



#### Fracassement

*Evocation de 2ème niveau*

**Classes :** Barde,
Warlock

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Les composantes :** V, S, M (une puce de mica)

**Durée :** Instantané

Un bruit de sonnerie soudain, douloureusement intense, jaillit d'un
point de votre choix à portée. Chaque créature dans une sphère de 10
pieds de rayon centrée sur ce point doit effectuer un jet de sauvegarde
de Constitution. Une créature subit 3d8 dégâts de tonnerre en cas
d'échec, ou la moitié en cas de réussite. Une créature faite d'un
matériau inorganique comme la pierre, le cristal ou le métal est
désavantagée à ce jet de sauvegarde.

Un objet non magique qui n'est pas porté ou transporté subit également
les dégâts s'il se trouve dans la zone du sort.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 2e.



#### Bouclier

*abjuration de 1er niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 réaction, que vous prenez lorsque vous êtes
touché par une attaque ou ciblé par le sort de *missile
magique*.

**Portée :** Self

**Composantes :** V, S

**Durée :** 1 round

Une barrière invisible de force magique apparaît et vous protège.
Jusqu'au début de votre prochain tour, vous bénéficiez d'un bonus de
+5 à la CA, y compris contre l'attaque déclenchante, et vous ne
subissez aucun dégât de *projectile magique*.



#### Bouclier de la foi

*abjuration de 1er niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action bonus

**Portée :** 60 pieds

**Composantes :** V, S, M (un petit parchemin avec un peu de texte sacré
écrit dessus)

**Durée :** Concentration, jusqu'à 10 minutes

Un champ chatoyant apparaît et entoure une créature de votre choix à
portée, lui accordant un bonus de +2 à la CA pour la durée.



#### Gourdin magique

*Tour de magie de transmutation*

**Classes :** Druide

**Temps d'incantation :** 1 action bonus

**Portée :** Toucher

**Composantes :** V, S, M (gui, feuille de trèfle, gourdin ou bâton).

**Durée :** 1 minute

Le bois du gourdin ou du bâton que vous tenez est imprégné du pouvoir de
la nature. Pour la durée, vous pouvez utiliser votre caractéristique
d'Incantation au lieu de la Force pour les jets d'attaque et de dégâts
des attaques de mêlée utilisant cette arme, et le dé de dégâts de
l'arme devient un d8. L'arme devient également magique, si elle ne
l'est pas déjà. Le sort prend fin si vous le lancez à nouveau ou si
vous lâchez l'arme.



#### Poigne électrique

*Tour de magie d'évocation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

La foudre jaillit de votre main pour délivrer un choc à une créature que
vous essayez de toucher. Effectuez une attaque de sort en mêlée contre
la cible. Vous avez un avantage sur le jet d'attaque si la cible porte
une armure en métal. En cas de succès, la cible subit 1d8 dégâts de
foudre, et elle ne peut pas réagir avant le début de son prochain tour.

Les dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le 5e
niveau (2d8), le 11e niveau (3d8) et le 17e niveau (4d8).



#### Silence

*Illusion de 2ème niveau (rituel)*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Pour la durée, aucun son ne peut être créé à l'intérieur ou passer à
travers une sphère de 20 pieds de rayon centrée sur un point que vous
choisissez à portée. Toute créature ou objet se trouvant entièrement à
l'intérieur de la sphère est immunisé contre les dégâts de tonnerre, et
les créatures sont assourdies lorsqu'elles s'y trouvent entièrement.
Lancer un sort comportant une composante verbale y est impossible.



#### Image silencieuse

*illusion de 1er niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un peu de toison)

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez l'image d'un objet, d'une créature ou d'un autre
phénomène visible qui n'est pas plus grand qu'un cube de 15 pieds.
L'image apparaît à un endroit situé à portée et dure toute la durée du
sort. L'image est purement visuelle ; elle n'est pas accompagnée de
son, d'odeur ou d'autres effets sensoriels.

Vous pouvez utiliser votre action pour que l'image se déplace vers
n'importe quel endroit à portée. Lorsque l'image change
d'emplacement, vous pouvez modifier son apparence de sorte que ses
mouvements semblent naturels pour l'image. Par exemple, si vous créez
l'image d'une créature et que vous la déplacez, vous pouvez modifier
l'image pour qu'elle semble marcher.

L'interaction physique avec l'image révèle qu'il s'agit d'une
illusion, car les choses peuvent la traverser. Une créature qui utilise
son action pour examiner l'image peut déterminer qu'il s'agit d'une
illusion en réussissant un test d'Intelligence (Investigation) contre
votre DC de sauvegarde contre les sorts. Si une créature discerne
l'illusion pour ce qu'elle est, elle peut voir à travers l'image.



#### Simulacre

*Ill illusion du 7ème niveau*

**Classes :** Magicien

**Temps de coulée :** 12 heures

**Portée :** Toucher

**Composantes :** V, S, M (neige ou glace en quantité suffisante pour
faire une copie grandeur nature de la créature dupliquée ; quelques
cheveux, rognures d'ongles ou autre morceau du corps de cette créature
placés à l'intérieur de la neige ou de la glace ; et rubis en poudre
d'une valeur de 1 500 gp, saupoudré sur le duplicata et consommé par le
sort).

**Durée :** Jusqu'à ce qu'il soit dissipé

Vous façonnez un double illusoire d'une bête ou d'un humanoïde qui se
trouve à portée pendant toute la durée du sort. Le duplicata est une
créature, partiellement réelle et formée de glace ou de neige, et il
peut entreprendre des actions et être affecté comme une créature
normale. Il semble être le même que l'original, mais il a la moitié du
maximum de points de vie de la créature et est formé sans aucun
équipement. Sinon, l'illusion utilise toutes les statistiques de la
créature qu'elle duplique.

Le simulacre est amical envers vous et les créatures que vous désignez.
Il obéit à vos ordres oraux, se déplaçant et agissant conformément à vos
souhaits et agissant à votre tour en combat. Le simulacre n'a pas la
capacité d'apprendre ou de devenir plus puissant, il n'augmente donc
jamais son niveau ou ses autres capacités, et ne peut pas non plus
récupérer les emplacements de sorts dépensés.

Si le simulacre est endommagé, vous pouvez le réparer dans un
laboratoire alchimique, en utilisant des herbes et des minéraux rares
d'une valeur de 100 gp par point de vie récupéré. Le simulacre dure
jusqu'à ce qu'il tombe à 0 point de vie, auquel cas il redevient de la
neige et fond instantanément.

Si vous lancez à nouveau ce sort, tous les doubles actuellement actifs
que vous avez créés avec ce sort sont instantanément détruits.



#### Sommeil

*Enchantement de 1er niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Les composantes :** V, S, M (une pincée de sable fin, des pétales de
rose, ou un grillon)

**Durée :** 1 minute

Ce sort plonge les créatures dans un sommeil magique.

Lancez 5d8 ; le total correspond au nombre de points de vie des
créatures que ce sort peut affecter. Les créatures situées à moins de 6
mètres d'un point que vous choisissez dans la portée sont affectées
dans l'ordre croissant de leurs points de vie actuels (en ignorant les
créatures inconscientes).

En commençant par la créature qui a le moins de points de vie actuels,
chaque créature affectée par ce sort tombe inconsciente jusqu'à ce que
le sort prenne fin, que le dormeur subisse des dégâts ou que quelqu'un
utilise une action pour secouer ou gifler le dormeur pour le réveiller.
Soustrayez les points de vie de chaque créature du total avant de passer
à la créature ayant le nombre de points de vie le plus bas suivant. Les
points de vie d'une créature doivent être égaux ou inférieurs au total
restant pour que cette créature soit affectée.

Les morts-vivants et les créatures immunisées contre le charme ne sont
pas affectés par ce sort.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 2e niveau ou plus, lancez 2d8 supplémentaires
pour chaque niveau d'emplacement supérieur au 1er.



#### Tempête de neige

*3rd-level conjuration*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (une pincée de poussière et quelques gouttes
d'eau)

**Durée :** Concentration, jusqu'à 1 minute

Jusqu'à ce que le sort prenne fin, de la pluie et du grésil verglaçants
tombent dans un cylindre de 20 pieds de haut avec un rayon de 40 pieds
centré sur un point que vous choisissez à portée. La zone est fortement
obscurcie, et les flammes exposées dans la zone sont éteintes.

Le sol de la zone est couvert de glace glissante, ce qui en fait un
terrain difficile. Lorsqu'une créature entre dans la zone du sort pour
la première fois lors d'un tour ou commence son tour à cet endroit,
elle doit effectuer un jet de sauvegarde de Dextérité. En cas d'échec,
elle tombe à plat ventre.

Si une créature se concentre dans la zone du sort, elle doit réussir un
jet de sauvegarde de Constitution contre le DC de sauvegarde du sort ou
perdre sa concentration.



#### Lenteur

*3rd-level transmutation*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (une goutte de mélasse)

**Durée :** Concentration, jusqu'à 1 minute

Vous modifiez le temps autour de jusqu'à six créatures de votre choix
dans un cube de 12 mètres à portée. Chaque cible doit réussir un jet de
sauvegarde de Sagesse ou être affectée par ce sort pendant toute sa
durée.

La vitesse d'une cible affectée est réduite de moitié, elle subit un
malus de -2 à la CA et aux jets de sauvegarde de Dextérité, et elle ne
peut pas utiliser de réactions. À son tour, elle peut utiliser soit une
action, soit une action bonus, mais pas les deux. Quelles que soient les
capacités ou les objets magiques de la créature, elle ne peut pas
effectuer plus d'une attaque en mêlée ou à distance pendant son tour.

Si la créature tente de lancer un sort dont le temps d'incantation est
de 1 action, lancez un d20. Sur un 11 ou plus, le sort ne prend pas
effet avant le prochain tour de la créature, et celle-ci doit utiliser
son action à ce tour pour terminer le sort. Si elle n'y parvient pas,
le sort est gaspillé.

Une créature affectée par ce sort effectue un nouveau jet de sauvegarde
de Sagesse à la fin de son tour. En cas de sauvegarde réussie, l'effet
prend fin pour elle.



#### Épargner les mourrants

*Tour de magie de nécromancie.*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S

**Durée :** Instantané

Vous touchez une créature vivante qui a 0 points de vie. La créature
devient stable. Ce sort n'a aucun effet sur les morts-vivants ou les
constructions.



#### Parler avec les animaux

*Divination de 1er niveau (rituel)*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** 10 minutes

Vous gagnez la capacité de comprendre et de communiquer verbalement avec
les bêtes pour la durée. Les connaissances et la conscience de
nombreuses bêtes sont limitées par leur intelligence, mais au minimum,
les bêtes peuvent vous donner des informations sur les lieux et les
monstres proches, y compris tout ce qu'elles peuvent percevoir ou ont
perçu au cours de la dernière journée. Vous pourrez peut-être persuader
une bête de vous rendre un petit service, à la discrétion du MJ.



#### Par Par Parler avec les morts parler avec les morts

*Nécromancie de 3e niveau*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V, S, M (brûler de l'encens)

**Durée :** 10 minutes

Vous accordez un semblant de vie et d'intelligence à un cadavre de
votre choix à portée, lui permettant de répondre aux questions que vous
posez. Le cadavre doit encore avoir une bouche et ne peut pas être
mort-vivant. Le sort échoue si le cadavre a été la cible de ce sort au
cours des 10 derniers jours.

Jusqu'à la fin du sort, vous pouvez poser jusqu'à cinq questions au
cadavre. Le cadavre ne connaît que ce qu'il savait de son vivant, y
compris les langues qu'il connaissait. Les réponses sont généralement
brèves, énigmatiques ou répétitives, et le cadavre n'est pas obligé de
donner une réponse véridique si vous lui êtes hostile ou s'il vous
reconnaît comme un ennemi. Ce sort ne rend pas l'âme de la créature à
son corps, seulement son esprit d'animation. Ainsi, le cadavre ne peut
pas apprendre de nouvelles informations, ne comprend rien de ce qui
s'est passé depuis sa mort et ne peut pas spéculer sur les événements
futurs.



#### Parler avec les plantes

*3rd-level transmutation*

**Classes :** Barde,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** Soi-même (rayon de 30 pieds)

**Composantes :** V, S

**Durée :** 10 minutes

Vous conférez aux plantes situées à moins de 10 mètres de vous une
sensibilité et une animation limitées, leur donnant la capacité de
communiquer avec vous et de suivre vos ordres simples. Vous pouvez
interroger les plantes sur les événements survenus dans la zone couverte
par le sort au cours de la journée écoulée, et obtenir des informations
sur les créatures qui sont passées, le temps et d'autres circonstances.

Vous pouvez également transformer un terrain difficile causé par la
croissance des plantes (comme les fourrés et les sous-bois) en un
terrain ordinaire qui dure toute la durée du jeu. Ou bien vous pouvez
transformer un terrain ordinaire où se trouvent des plantes en un
terrain difficile qui dure toute la durée de l'action, en faisant en
sorte que les vignes et les branches gênent les poursuivants, par
exemple.

Les plantes peuvent être capables d'effectuer d'autres tâches en votre
nom, à la discrétion du MJ. Le sort ne permet pas aux plantes de se
déraciner et de se déplacer, mais elles peuvent déplacer librement les
branches, les vrilles et les tiges.

Si une créature végétale se trouve dans la zone, vous pouvez communiquer
avec elle comme si vous partagiez une langue commune, mais vous ne
gagnez aucune capacité magique pour l'influencer.

Ce sort peut faire en sorte que les plantes créées par le sort
d'*enchevêtrement* libèrent une créature entravée.



#### Pattes d'araignée

*transmutation de 2ème niveau*

**Classes :** Sorcier, Sorcier de
guerre

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une goutte de bitume et une araignée)

**Durée :** Concentration, jusqu'à 1 heure

Jusqu'à la fin du sort, une créature volontaire que vous touchez gagne
la capacité de se déplacer vers le haut, vers le bas et à travers des
surfaces verticales et à l'envers le long des plafonds, tout en
laissant ses mains libres. La cible gagne également une vitesse
d'escalade égale à sa vitesse de marche.



#### Croissance d'épines

*Transmutation de 2ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (sept épines acérées ou sept petites
brindilles, chacune aiguisée en pointe).

**Durée :** Concentration, jusqu'à 10 minutes

Le sol dans un rayon de 20 pieds centré sur un point à portée se tord et
pousse des pointes et des épines dures. La zone devient un terrain
difficile pour la durée du sort. Lorsqu'une créature se déplace dans ou
à l'intérieur de la zone, elle subit 2d4 dégâts perforants tous les 5
pieds parcourus.

La transformation du terrain est camouflée pour paraître naturelle.
Toute créature qui ne peut pas voir la zone au moment où le sort est
lancé doit effectuer un test de Sagesse (Perception) contre votre DC de
sauvegarde contre les sorts pour reconnaître le terrain comme dangereux
avant d'y pénétrer.



#### Esprits gardiens

*conj conj conj conj conjuration du 3ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Soi-même (rayon de 15 pieds)

**Composantes :** V, S, M (un symbole sacré)

**Durée :** Concentration, jusqu'à 10 minutes

Vous appelez des esprits pour vous protéger. Ils volent autour de vous à
une distance de 15 pieds pour la durée. Si vous êtes bon ou neutre, leur
forme spectrale apparaît angélique ou fée (au choix). Si vous êtes
mauvais, ils apparaissent diaboliques.

Lorsque vous lancez ce sort, vous pouvez désigner un nombre quelconque
de créatures que vous pouvez voir pour qu'elles ne soient pas affectées
par ce sort. La vitesse d'une créature affectée est réduite de moitié
dans la zone, et lorsque la créature entre dans la zone pour la première
fois lors d'un tour ou commence son tour à cet endroit, elle doit
effectuer un jet de sauvegarde de Sagesse. En cas d'échec, la créature
subit 3d8 dégâts radiants (si vous êtes bon ou neutre) ou 3d8 dégâts
nécrotiques (si vous êtes mauvais). En cas de sauvegarde réussie, la
créature subit deux fois moins de dégâts.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 4e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 3e.



#### Arme spirituelle

*Evocation de 2ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action bonus

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** 1 minute

Vous créez une arme flottante et spectrale à portée de main qui dure
pendant la durée du sort ou jusqu'à ce que vous le lanciez à nouveau.
Lorsque vous lancez ce sort, vous pouvez effectuer une attaque en mêlée
contre une créature située à moins de 1,5 m de l'arme. En cas de
succès, la cible subit des dégâts de force égaux à 1d8 + votre
modificateur de capacité d'incantation.

Comme action bonus à votre tour, vous pouvez déplacer l'arme jusqu'à 6
mètres et répéter l'attaque contre une créature située à 1,5 mètre de
celle-ci.

L'arme peut prendre la forme de votre choix. Les clercs des divinités
qui sont associées à une arme particulière (comme St. Cuthbert est connu
pour sa masse et Thor pour son marteau) font en sorte que l'effet de ce
sort ressemble à cette arme.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 3e niveau ou plus, les dégâts augmentent de 1d8
pour chaque tranche de deux niveaux d'emplacement au-dessus du 2e.



#### Nuage nauséabond

*3rd-level conjuration*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 90 pieds

**Composantes :** V, S, M (un œuf pourri ou plusieurs feuilles de chou
puant).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez une sphère de gaz jaune et nauséabond de 6 mètres de rayon
centrée sur un point à portée. Le nuage se répand dans les coins, et sa
zone est fortement obscurcie. Le nuage reste dans l'air pendant toute
sa durée.

Chaque créature qui se trouve entièrement dans le nuage au début de son
tour doit effectuer un jet de sauvegarde de Constitution contre le
poison. En cas d'échec, la créature passe son action de ce tour à vomir
et à tituber. Les créatures qui n'ont pas besoin de respirer ou qui
sont immunisées contre le poison réussissent automatiquement ce jet de
sauvegarde.

Un vent modéré (au moins 10 miles par heure) disperse le nuage après 4
tours. Un vent fort (au moins 30 km/heure) le disperse après un tour.



#### Façonnage de la pierre

*4th-level transmutation*

**Classes :** Clerc,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (argile molle, qui doit être travaillée pour
obtenir approximativement la forme souhaitée de l'objet en pierre).

**Durée :** Instantané

Vous touchez un objet en pierre de taille moyenne ou plus petite ou une
section de pierre ne dépassant pas 1,5 m dans n'importe quelle
dimension et vous lui donnez la forme qui vous convient. Ainsi, par
exemple, vous pouvez façonner une grande pierre pour en faire une arme,
une idole ou un coffret, ou créer un petit passage à travers un mur, à
condition que ce dernier ait une épaisseur inférieure à 1,5 mètre. Vous
pouvez également façonner une porte en pierre ou son cadre pour la
fermer hermétiquement. L'objet que vous créez peut avoir jusqu'à deux
charnières et un loquet, mais des détails mécaniques plus fins ne sont
pas possibles.



#### Peau de pierre

*4th-level abjuration*

**Classes :** Druide,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (poussière de diamant d'une valeur de 100 gp,
que le sort consomme).

**Durée :** Concentration, jusqu'à 1 heure

Ce sort rend la chair d'une créature que vous touchez aussi dure que la
pierre. Jusqu'à la fin du sort, la cible résiste aux dégâts non
magiques de contondant, perforant et tranchant.



#### Tempête vengeresse

*conjuration de 9e niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Visée

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Un nuage d'orage se forme, centré sur un point que vous pouvez voir et
s'étendant dans un rayon de 360 pieds. La foudre jaillit dans la zone,
le tonnerre gronde et les vents violents se déchaînent. Chaque créature
se trouvant sous le nuage (pas plus de 5 000 pieds sous le nuage)
lorsqu'il apparaît doit effectuer un jet de sauvegarde de Constitution.
En cas d'échec, la créature subit 2d6 dégâts de tonnerre et devient
assourdie pendant 5 minutes.

Chaque round où vous maintenez votre concentration sur ce sort, la
tempête produit des effets supplémentaires à votre tour.

***Round 2.*** Une pluie acide tombe du nuage. Chaque créature et objet
se trouvant sous le nuage subit 1d6 dégâts d'acide.

***Round 3.*** Vous appelez six éclairs du nuage pour frapper six
créatures ou objets de votre choix sous le nuage. Une créature ou un
objet donné ne peut pas être frappé par plus d'un éclair. Une créature
frappée doit effectuer un jet de sauvegarde de Dextérité. La créature
subit 10d6 points de dégâts de foudre en cas d'échec, ou la moitié en
cas de réussite.

***Round 4.*** Des grêlons tombent du nuage. Chaque créature sous le
nuage subit 2d6 points de dégâts contondants.

***Rounds 5-10.*** Des rafales et de la pluie verglaçante assaillent la
zone sous le nuage. La zone devient un terrain difficile et est
fortement obscurcie. Chaque créature qui s'y trouve subit 1d6 dégâts de
froid. Les attaques avec des armes à distance sont impossibles dans
cette zone. Le vent et la pluie comptent comme une grave distraction
pour maintenir la concentration sur les sorts. Enfin, les rafales de
vent fort (de 20 à 50 miles par heure) dispersent automatiquement le
brouillard, les brumes et autres phénomènes similaires dans la zone,
qu'ils soient terrestres ou magiques.



#### Suggestion

*Enchantement de 2ème niveau*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, M (une langue de serpent et un peu de miel ou une
goutte d'huile douce).

**Durée :** Concentration, jusqu'à 8 heures

Vous suggérez une ligne de conduite (limitée à une phrase ou deux) et
influencez magiquement une créature que vous pouvez voir à portée et qui
peut vous entendre et vous comprendre. Les créatures qui ne peuvent pas
être charmées sont immunisées contre cet effet. La suggestion doit être
formulée de manière à ce que le plan d'action semble raisonnable.
Demander à la créature de se poignarder, de se jeter sur une lance, de
s'immoler ou de faire un autre acte manifestement nuisible met fin au
sort.

La cible doit effectuer un jet de sauvegarde de Sagesse. En cas
d'échec, elle suit le plan d'action que vous avez décrit au mieux de
ses capacités. L'action suggérée peut se poursuivre pendant toute la
durée du sort. Si l'activité suggérée peut être réalisée en moins de
temps, le sort prend fin lorsque le sujet termine ce qu'on lui a
demandé de faire.

Vous pouvez également spécifier des étâts qui déclencheront une activité
spéciale pendant cette durée. Par exemple, vous pouvez suggérer à un
chevalier de donner son cheval de guerre au premier mendiant qu'il
rencontre. Si la condition n'est pas remplie avant l'expiration du
sort, l'activité n'est pas réalisée.

Si vous ou l'un de vos compagnons endommagez la cible, le sort prend
fin.



#### Rayon de soleil

*Evocation de 6ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Soi-même (ligne de 60 pieds)

**Composantes :** V, S, M (une loupe)

**Durée :** Concentration, jusqu'à 1 minute

Un faisceau de lumière brillante jaillit de votre main en une ligne de
1,5 mètre de large et de 6 mètres de long. Chaque créature de la ligne
doit effectuer un jet de sauvegarde de Constitution. En cas d'échec, la
créature subit 6d8 points de dégâts radiants et est aveuglée jusqu'à
votre prochain tour. Si elle réussit son jet de sauvegarde, elle subit
la moitié des dégâts et n'est pas aveuglée par ce sort. Les
morts-vivants et les suintants ont un désavantage sur ce jet de
sauvegarde.

Vous pouvez créer une nouvelle ligne de radiance comme action à
n'importe quel tour jusqu'à ce que le sort prenne fin.

Pour la durée de l'opération, une tache d'éclat brillant brille dans
votre main. Elle diffuse une lumière vive dans un rayon de 30 pieds et
une lumière chétive sur 30 pieds supplémentaires. Cette lumière est
celle du soleil.



#### Éclat du soleil

*Evocation de 8ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 150 pieds

**Composantes :** V, S, M (feu et un morceau de pierre solaire)

**Durée :** Instantané

Une lumière solaire brillante éclate dans un rayon de 60 pieds centré
sur un point que vous choisissez à portée. Chaque créature se trouvant
dans cette lumière doit effectuer un jet de sauvegarde de Constitution.
En cas d'échec, la créature subit 12d6 points de dégâts radiants et est
aveuglée pendant 1 minute. Si elle réussit son jet de sauvegarde, elle
subit moitié moins de dégâts et n'est pas aveuglée par ce sort. Les
morts-vivants et les suintants ont un désavantage sur ce jet de
sauvegarde.

Une créature aveuglée par ce sort effectue un autre jet de sauvegarde de
Constitution à la fin de chacun de ses tours. En cas de réussite, elle
n'est plus aveuglée.

Ce sort dissipe toute obscurité dans sa zone qui a été créée par un
sort.



#### Symbole

*7th-level abjuration*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** Toucher

**Composantes :** V, S, M (mercure, phosphore, poudre de diamant et
d'opale d'une valeur totale d'au moins 1 000 gp, que le sort
consomme).

**Durée :** Jusqu'à ce qu'il soit dissipé ou déclenché

Lorsque vous lancez ce sort, vous inscrivez un glyphe nuisible soit sur
une surface (comme une section du sol, un mur ou une table), soit dans
un objet qui peut être fermé pour dissimuler le glyphe (comme un livre,
un parchemin ou un coffre à trésor). Si vous choisissez une surface, le
glyphe peut couvrir une zone de la surface ne dépassant pas 10 pieds de
diamètre. Si vous choisissez un objet, celui-ci doit rester à sa place ;
si l'objet est déplacé à plus de 3 mètres de l'endroit où vous lancez
ce sort, le glyphe est brisé et le sort prend fin sans être déclenché.

Le glyphe est presque invisible, et il faut effectuer un test
d'Intelligence (Investigation) contre votre DC de sauvegarde contre les
sorts pour le trouver.

Vous décidez de ce qui déclenche le glyphe lorsque vous lancez le sort.
Pour les glyphes inscrits sur une surface, les déclencheurs les plus
typiques sont le fait de toucher ou de marcher sur le glyphe, de retirer
un autre objet qui le recouvre, de s'approcher à une certaine distance
de lui, ou de manipuler l'objet qui le contient. Pour les glyphes
inscrits dans un objet, les déclencheurs les plus communs sont
l'ouverture de l'objet, l'approche à une certaine distance de
l'objet, ou la vue ou la lecture du glyphe.

Vous pouvez affiner le déclencheur pour que le sort ne soit activé que
dans certaines circonstances ou en fonction des caractéristiques
physiques d'une créature (comme sa taille ou son poids), ou de son type
physique (par exemple, le pupille pourrait être réglé pour affecter les
sorcières ou les métamorphes). Vous pouvez également spécifier des
créatures qui ne déclenchent pas le glyphe, comme celles qui disent un
certain mot de passe.

Lorsque vous inscrivez le glyphe, choisissez l'une des options
ci-dessous pour son effet. Une fois déclenché, le glyphe brille,
remplissant une sphère de 60 pieds de rayon d'une lumière faible
pendant 10 minutes, après quoi le sort prend fin. Chaque créature
présente dans la sphère lorsque le glyphe s'active est ciblée par son
effet, tout comme une créature qui entre dans la sphère pour la première
fois lors d'un tour ou qui y termine son tour.

***Mort.*** Chaque cible doit effectuer un jet de sauvegarde de
Constitution, subissant 10d10 points de dégâts nécrotiques en cas
d'échec, ou la moitié en cas de réussite.

***Discorde.*** Chaque cible doit effectuer un jet de sauvegarde de
Constitution. En cas d'échec, la cible se chamaille et se dispute avec
les autres créatures pendant 1 minute. Pendant ce temps, elle est
incapable de communiquer de manière significative et a un désavantage
aux jets d'attaque et aux tests de capacité.

***Peur.*** Chaque cible doit effectuer un jet de sauvegarde de Sagesse
et devient effrayée pendant 1 minute en cas d'échec. Pendant qu'elle
est effrayée, la cible laisse tomber tout ce qu'elle tient et doit
s'éloigner d'au moins 10 mètres du glyphe à chacun de ses tours, si
elle le peut.

***Désespoir.*** Chaque cible doit effectuer un jet de sauvegarde de
Charisme. En cas d'échec, la cible est submergée par le désespoir
pendant 1 minute. Pendant ce temps, elle ne peut pas attaquer ou cibler
une créature avec des capacités, des sorts ou d'autres effets magiques
nuisibles.

***Folie.*** Chaque cible doit effectuer un jet de sauvegarde
d'Intelligence. En cas d'échec, la cible est rendue folle pendant 1
minute. Une créature folle ne peut pas faire d'actions, ne peut pas
comprendre ce que disent les autres créatures, ne peut pas lire et ne
parle qu'en charabia. Le MJ contrôle son mouvement, qui est erratique.

***Douleur.*** Chaque cible doit effectuer un jet de sauvegarde de
Constitution et devient incapacitée par une douleur atroce pendant 1
minute en cas d'échec.

***Sommeil.*** Chaque cible doit effectuer un jet de sauvegarde de
Sagesse et tombe inconsciente pendant 10 minutes en cas d'échec. Une
créature se réveille si elle subit des dégâts ou si quelqu'un utilise
une action pour la secouer ou la gifler.

***Étourdissement.*** Chaque cible doit effectuer un jet de sauvegarde
de Sagesse et est étourdie pendant 1 minute en cas d'échec.



#### Télékinésie

*transmutation de niveau 5*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 10 minutes

Vous gagnez la capacité de déplacer ou de manipuler des créatures ou des
objets par la pensée. Lorsque vous lancez le sort, et comme action
chaque round pendant la durée du sort, vous pouvez exercer votre volonté
sur une créature ou un objet que vous pouvez voir à portée, provoquant
l'effet approprié ci-dessous. Vous pouvez affecter la même cible round
après round, ou en choisir une nouvelle à tout moment. Si vous changez
de cible, la cible précédente n'est plus affectée par le sort.

***Créature.*** Vous pouvez essayer de déplacer une créature Très grande
ou plus petite. Effectuez un test de capacité avec votre caractéristique
d'incantation contestée par le test de Force de la créature. Si vous
gagnez le concours, vous déplacez la créature jusqu'à 30 pieds dans
n'importe quelle direction, y compris vers le haut mais pas au-delà de
la portée de ce sort. Jusqu'à la fin de votre prochain tour, la
créature est entravée par votre prise télékinésiste. Une créature
soulevée vers le haut est suspendue dans les airs.

Aux rounds suivants, vous pouvez utiliser votre action pour tenter de
maintenir votre emprise télékinésiste sur la créature en répétant le
concours.

***Objet.*** Vous pouvez essayer de déplacer un objet pesant jusqu'à 1
000 livres. Si l'objet n'est pas porté, vous le déplacez
automatiquement jusqu'à 10 mètres dans n'importe quelle direction,
mais pas au-delà de la portée de ce sort.

Si l'objet est porté par une créature, vous devez faire un test
d'aptitude avec votre capacité d'incantation contestée par le test de
Force de cette créature. Si vous réussissez, vous éloignez l'objet de
cette créature et pouvez le déplacer jusqu'à 10 mètres dans n'importe
quelle direction, mais pas au-delà de la portée de ce sort.

Vous pouvez exercer un contrôle fin sur les objets avec votre prise
télékinésiste, comme manipuler un outil simple, ouvrir une porte ou un
récipient, ranger ou récupérer un objet dans un récipient ouvert, ou
verser le contenu d'une fiole.



#### Lien Télépathique

**Classes :** Magicien

*Divination de 5ème niveau (rituel)*

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (morceaux de coquilles d'œufs provenant de
deux types de créatures différentes).

**Durée :** 1 heure

Vous établissez un lien télépathique entre jusqu'à huit créatures
consentantes de votre choix à portée, reliant psychiquement chaque
créature à toutes les autres pour la durée du sort. Les créatures ayant
un score d'Intelligence de 2 ou moins ne sont pas affectées par ce
sort.

Jusqu'à la fin du sort, les cibles peuvent communiquer par télépathie
par le biais du lien, qu'elles aient ou non une langue commune. La
communication est possible sur n'importe quelle distance, bien qu'elle
ne puisse pas s'étendre à d'autres plans d'existence.



#### Téléportation

*conjuration de 7ème niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V

**Durée :** Instantané

Ce sort vous transporte instantanément, vous et jusqu'à huit créatures
consentantes de votre choix que vous pouvez voir à portée, ou un seul
objet que vous pouvez voir à portée, vers une destination que vous
choisissez. Si vous ciblez un objet, il doit pouvoir tenir entièrement
dans un cube de 3 mètres de côté, et il ne peut pas être tenu ou porté
par une créature non consentante.

La destination que vous choisissez doit être connue de vous et se
trouver sur le même plan d'existence que vous. Votre familiarité avec
la destination détermine si vous y arrivez avec succès. Le MJ lance un
d100 et consulte la table.

  ------------------------------------------------------------
  Familiarité         Mishap      Zone       Hors    En ligne
                                similaire    cible    de mire
  ------------------ --------- ----------- --------- ---------
  Cercle permanent      \-         \-         \-      01-100

  Objet associé         \-         \-         \-      01-100

  Très familier        01-05      06-13      14-24    25-100

  Vu avec              01-33      34-43      44-53    54-100
  désinvolture                                       

  Visionné une fois    01-43      44-53      54-73    74-100

  Description          01-43      44-53      54-73    74-100

  Fausse destination   01-50     51-100       \-        \-
  ------------------------------------------------------------

***Familiarité.*** \"Cercle permanent\" signifie un cercle de
téléportation permanent dont vous connaissez la séquence de sigles. \"
Objet associé \" signifie que vous possédez un objet pris à la
destination souhaitée dans les six derniers mois, comme un livre de la
bibliothèque d'un magicien, du linge de lit d'une suite royale ou un
morceau de marbre de la tombe secrète d'une liche.

\"Très familier\" est un endroit où vous êtes allé très souvent, un
endroit que vous avez soigneusement étudié, ou un endroit que vous
pouvez voir lorsque vous lancez le sort. \"Vu occasionnellement\" est un
endroit que vous avez vu plus d'une fois mais avec lequel vous n'êtes
pas très familier. \"Vu une fois\" est un lieu que vous avez vu une
fois, éventuellement en utilisant la magie. \"Description\" est un lieu
dont vous connaissez l'emplacement et l'apparence grâce à la
description de quelqu'un d'autre, peut-être à partir d'une carte.

Une \"fausse destination\" est un lieu qui n'existe pas. Vous avez
peut-être essayé de scruter le sanctuaire d'un ennemi mais n'avez vu
qu'une illusion, ou vous essayez de vous téléporter dans un lieu
familier qui n'existe plus.

***Sur la cible.*** Vous et votre groupe (ou l'objet cible) apparaissez
là où vous le souhaitez.

***Hors cible.*** Vous et votre groupe (ou l'objet cible) apparaissez à
une distance aléatoire de la destination, dans une direction aléatoire.
La distance hors cible est égale à 1d10 × 1d10 pour cent de la distance
qui devait être parcourue. Par exemple, si vous avez essayé de parcourir
120 miles, que vous avez atterri hors cible et que vous avez obtenu un 5
et un 3 sur les deux d10, alors vous serez hors cible de 15 %, soit 18
miles. Le MJ détermine la direction hors cible de manière aléatoire en
lançant un d8 et en désignant 1 comme nord, 2 comme nord-est, 3 comme
est, et ainsi de suite autour des points cardinaux. Si vous vous
téléportez vers une ville côtière et que vous vous retrouvez à 18 miles
en mer, vous pourriez avoir des problèmes.

***Zone similaire.*** Vous et votre groupe (ou l'objet cible) vous
retrouvez dans une zone différente qui est visuellement ou
thématiquement similaire à la zone cible. Si vous vous dirigez vers
votre laboratoire personnel, par exemple, vous pouvez vous retrouver
dans le laboratoire d'un autre magicien ou dans un magasin de
fournitures alchimiques qui possède les mêmes outils et instruments que
votre laboratoire. En général, vous apparaissez dans l'endroit
similaire le plus proche, mais comme le sort n'a pas de limite de
portée, vous pouvez vous retrouver n'importe où dans l'avion.

***Mishap.*** La magie imprévisible du sort entraîne un voyage
difficile. Chaque créature qui se téléporte (ou l'objet cible) subit
3d10 points de dégâts de force, et le MJ effectue un jet de dé sur la
table pour voir où vous vous retrouvez (plusieurs mésaventures peuvent
se produire, infligeant des dégâts à chaque fois).



#### Cercle de téléportation

*conjuration de 5e niveau*

**Classes :** Barde,
Magicien

**Temps d'incantation :** 1 minute

**Portée :** 10 pieds

**Composantes :** V, M (craies et encres rares infusées de pierres
précieuses pour 50 gp, que le sort consomme)

**Durée :** 1 round

Lorsque vous lancez le sort, vous dessinez sur le sol un cercle de 3
mètres de diamètre inscrit de sigils qui relient votre emplacement à un
cercle de téléportation permanent de votre choix dont vous connaissez la
séquence de sigils et qui se trouve sur le même plan d'existence que
vous. Un portail chatoyant s'ouvre dans le cercle que vous avez dessiné
et reste ouvert jusqu'à la fin de votre prochain tour. Toute créature
qui pénètre dans le portail apparaît instantanément dans un rayon de 1,5
mètre du cercle de destination ou dans l'espace inoccupé le plus proche
si cet espace est occupé.

De nombreux temples, guildes et autres lieux importants possèdent des
cercles de téléportation permanents inscrits quelque part dans leur
enceinte. Chacun de ces cercles comprend une séquence de sigles unique,
une chaîne de runes magiques disposées selon un modèle particulier.
Lorsque vous gagnez la capacité de lancer ce sort pour la première fois,
vous apprenez les séquences de sigils pour deux destinations sur le plan
matériel, déterminées par le MJ. Vous pouvez apprendre d'autres
séquences de sigils au cours de vos aventures. Vous pouvez mémoriser une
nouvelle séquence de sigils après l'avoir étudiée pendant 1 minute.

Vous pouvez créer un cercle de téléportation permanent en lançant ce
sort au même endroit chaque jour pendant un an. Vous n'avez pas besoin
d'utiliser le cercle pour vous téléporter lorsque vous lancez le sort
de cette manière.



#### Thaumaturgie

*Tour de magie de transmutation*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V

**Durée :** Jusqu'à 1 minute

Vous manifestez une merveille mineure, un signe de puissance
surnaturelle, à portée. Vous créez l'un des effets magiques suivants à
portée :

-   Votre voix est amplifiée jusqu'à trois fois plus fort que la
    normale pendant une minute.
-   Vous faites vaciller, éclaircir, atténuer ou changer de couleur les
    flammes pendant 1 minute.
-   Vous provoquez des secousses inoffensives dans le sol pendant 1
    minute.
-   Vous créez un son instantané qui provient d'un point de votre choix
    à portée, comme un grondement de tonnerre, le cri d'un corbeau ou
    des murmures inquiétants.
-   Vous faites en sorte qu'une porte ou une fenêtre non verrouillée
    s'ouvre ou se ferme instantanément.
-   Vous modifiez l'apparence de vos yeux pendant 1 minute.

Si vous lancez ce sort plusieurs fois, vous pouvez avoir jusqu'à trois
de ses effets d'une minute actifs à la fois, et vous pouvez annuler un
tel effet comme une action.



#### Vague tonnante

*Evocation de 1er niveau*

**Classes :** Barde,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** Soi-même (cube de 15 pieds)

**Composantes :** V, S

**Durée :** Instantané

Une vague de force tonitruante déferle sur vous. Chaque créature dans un
cube de 15 pieds partant de vous doit effectuer un jet de sauvegarde de
Constitution. En cas d'échec, la créature subit 2d8 points de dégâts de
tonnerre et est poussée à 10 pieds de vous. En cas de sauvegarde
réussie, la créature subit la moitié des dégâts et n'est pas poussée.

De plus, les objets non sécurisés qui se trouvent complètement dans la
zone d'effet sont automatiquement repoussés à 10 pieds de vous par
l'effet du sort, et le sort émet un boom tonitruant audible jusqu'à
300 pieds.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 2e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 1er.



#### Arrêt du temps

*Trans du 9ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V

**Durée :** Instantané

Vous arrêtez brièvement le cours du temps pour tout le monde sauf
vous-même. Le temps ne passe pas pour les autres créatures, tandis que
vous prenez 1d4 + 1 tours d'affilée, pendant lesquels vous pouvez
utiliser des actions et vous déplacer normalement.

Ce sort prend fin si l'une des actions que vous utilisez pendant cette
période, ou tout effet que vous créez pendant cette période, affecte une
créature autre que vous ou un objet porté par quelqu'un d'autre que
vous. De plus, le sort prend fin si vous vous déplacez à plus de 1 000
pieds de l'endroit où vous l'avez lancé.



#### Très petite (TP)

*Evocation de 3e niveau (rituel)*

**Classes :** Barde

**Temps d'incantation :** 1 minute

**Portée :** Soi-même (hémisphère de 10 pieds de rayon)

**Composantes :** V, S, M (une petite perle de cristal)

**Durée :** 8 heures

Un dôme de force immobile de 3 mètres de rayon se forme autour et
au-dessus de vous et reste immobile pendant toute la durée du sort. Le
sort prend fin si vous quittez sa zone.

Neuf créatures de taille moyenne ou plus petite peuvent entrer dans le
dôme avec vous. Le sort échoue si sa zone inclut une créature plus
grande ou plus de neuf créatures. Les créatures et les objets qui se
trouvent dans le dôme lorsque vous lancez ce sort peuvent s'y déplacer
librement. Toutes les autres créatures et tous les autres objets sont
empêchés de le traverser. Les sorts et autres effets magiques ne peuvent
pas s'étendre à travers le dôme ou être lancés à travers lui.
L'atmosphère à l'intérieur de l'espace est confortable et sèche, quel
que soit le temps qu'il fait à l'extérieur.

Jusqu'à la fin du sort, vous pouvez ordonner que l'intérieur soit
faiblement éclairé ou sombre. Le dôme est opaque de l'extérieur, de la
couleur de votre choix, mais il est transparent de l'intérieur.



#### Don des langues

*Divination de 3e niveau*

**Classes :** Barde,
Sorcier.

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, M (un petit modèle en argile d'une ziggourat)

**Durée :** 1 heure

Ce sort confère à la créature que vous touchez la capacité de comprendre
toute langue parlée qu'elle entend. De plus, lorsque la cible parle,
toute créature connaissant au moins une langue et pouvant entendre la
cible comprend ce qu'elle dit.



#### Transport végétal

*Conjuration de 6ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 10 pieds

**Composantes :** V, S

**Durée :** 1 round

Ce sort crée un lien magique entre une plante inanimée de taille Grand
ou supérieure se trouvant à portée et une autre plante, à n'importe
quelle distance, sur le même plan d'existence. Vous devez avoir vu ou
touché la plante de destination au moins une fois auparavant. Pendant la
durée du sort, toute créature peut entrer dans la plante cible et sortir
de la plante de destination en utilisant 5 pieds de mouvement.



#### Foulée d'arbre

*conjuration de 5e niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Vous gagnez la capacité d'entrer dans un arbre et de vous déplacer de
l'intérieur à l'intérieur d'un autre arbre du même type dans un rayon
de 500 pieds. Les deux arbres doivent être vivants et avoir au moins la
même taille que vous. Vous devez utiliser 5 pieds de mouvement pour
entrer dans un arbre. Vous connaissez instantanément l'emplacement de
tous les autres arbres du même type dans un rayon de 500 pieds et, dans
le cadre du mouvement utilisé pour entrer dans l'arbre, vous pouvez
soit passer dans l'un de ces arbres, soit sortir de l'arbre dans
lequel vous vous trouvez. Vous apparaissez à l'endroit de votre choix
dans un rayon de 1,5 mètre de l'arbre de destination, en utilisant un
autre mouvement de 1,5 mètre. Si vous n'avez plus de mouvement, vous
apparaissez à 1,5 m de l'arbre dans lequel vous êtes entré.

Vous pouvez utiliser cette capacité de transport une fois par tour pour
la durée. Vous devez terminer chaque tour à l'extérieur d'un arbre.



#### Métamorphose véritable

*Trans du 9ème niveau*

**Classes :** Barde,
magicien\...

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (une goutte de mercure, une cuillerée de gomme
arabique et un brin de fumée).

**Durée :** Concentration, jusqu'à 1 heure

Choisissez une créature ou un objet non magique que vous pouvez voir à
portée. Vous transformez la créature en une créature différente, la
créature en un objet, ou l'objet en une créature (l'objet ne doit être
ni porté, ni transporté par une autre créature). La transformation dure
toute la durée du sort, ou jusqu'à ce que la cible tombe à 0 points de
vie ou meure. Si vous vous concentrez sur ce sort pendant toute sa
durée, la transformation dure jusqu'à ce qu'il soit dissipé.

Ce sort n'a aucun effet sur un métamorphe ou une créature ayant 0
points de vie. Une créature non consentante peut effectuer un jet de
sauvegarde de Sagesse, et si elle réussit, elle n'est pas affectée par
ce sort.

***Créature en créature.*** Si vous transformez une créature en une
autre sorte de créature, la nouvelle forme peut être de n'importe
quelle sorte de votre choix dont la valeur de défi est égale ou
inférieure à celle de la cible (ou de son niveau, si la cible n'a pas
de valeur de défi). Les statistiques de jeu de la cible, y compris les
scores d'aptitude mentale, sont remplacées par les statistiques de la
nouvelle forme. Elle conserve son alignement et sa personnalité.

La cible assume les points de vie de sa nouvelle forme, et lorsqu'elle
revient à sa forme normale, la créature retrouve le nombre de points de
vie qu'elle avait avant sa transformation. Si elle revient à sa forme
normale après être tombée à 0 point de vie, les dégâts excédentaires
sont reportés sur sa forme normale. Tant que l'excès de dégâts ne
rapetisse pas la forme normale de la créature à 0 points de vie, elle
n'est pas frappée d'inconscience.

La créature est limitée dans les actions qu'elle peut effectuer par la
nature de sa nouvelle forme, et elle ne peut pas parler, jeter des
sorts, ou faire toute autre action qui nécessite les mains ou la parole,
à moins que sa nouvelle forme soit capable de telles actions.

L'équipement de la cible se fond dans sa nouvelle forme. La créature ne
peut pas activer, utiliser, manier ou bénéficier d'un quelconque de ses
équipements.

***Objet en créature.*** Vous pouvez transformer un objet en n'importe
quel type de créature, à condition que la taille de la créature ne soit
pas supérieure à celle de l'objet et que le niveau de défi de la
créature soit de 9 ou moins. La créature est amicale envers vous et vos
compagnons. Elle agit à chacun de vos tours. Vous décidez de l'action
qu'elle entreprend et de la façon dont elle se déplace. Le MJ dispose
des statistiques de la créature et résout toutes ses actions et son
mouvement.

Si le sort devient permanent, vous ne contrôlez plus la créature. Elle
peut rester amicale avec vous, selon la façon dont vous l'avez traitée.

***Créature en objet.*** Si vous transformez une créature en objet, elle
se transforme en même temps que ce qu'elle porte et transporte dans
cette forme. Les statistiques de la créature deviennent celles de
l'objet, et la créature n'a aucun souvenir du temps passé sous cette
forme, une fois que le sort prend fin et qu'elle retrouve sa forme
normale.



#### Résurrection incontestable

*Nécromancie de 9ème niveau*

**Classes :** Clerc

**Durée du moulage :** 1 heure

**Portée :** Toucher

**Composantes :** V, S, M (une aspersion d'eau bénite et des diamants
d'une valeur d'au moins 25 000 gp, que le sort consomme).

**Durée :** Instantané

Vous touchez une créature morte depuis moins de 200 ans et qui est morte
pour n'importe quelle raison sauf la vieillesse. Si l'âme de la
créature est libre et consentante, la créature est ramenée à la vie avec
tous ses points de vie.

Ce sort referme toutes les blessures, neutralise tout poison, guérit
toutes les maladies et lève toutes les malédictions affectant la
créature au moment de sa mort. Le sort remplace les organes et les
membres endommagés ou manquants.

Le sort peut même fournir un nouveau corps si l'original n'existe
plus, auquel cas vous devez prononcer le nom de la créature. La créature
apparaît alors dans un espace inoccupé que vous choisissez dans un rayon
de 3 mètres autour de vous.



#### Vision suprême

*Divination de 6ème niveau*

**Classes :** Barde,
Sorcier.

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (un onguent pour les yeux qui coûte 25 gp ; il
est fabriqué à partir de poudre de champignon, de safran et de graisse ;
il est consommé par le sort).

**Durée :** 1 heure

Ce sort donne à la créature volontaire que vous touchez la capacité de
voir les choses telles qu'elles sont réellement. Pour la durée du sort,
la créature a une vision véritable, remarque les portes secrètes cachées
par la magie, et peut voir dans le plan éthéré, le tout sur une portée
de 120 pieds.



#### Coup au but

*Tour de magie de Divination*

**Classes :** Barde,
sorcier\...

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** S

**Durée :** Concentration, jusqu'à 1 round

Vous étendez votre main et pointez un doigt vers une cible à portée.
Votre magie vous accorde une brève intuition sur les défenses de la
cible. A votre prochain tour, vous gagnez un avantage sur votre premier
jet d'attaque contre la cible, à condition que ce sort ne soit pas
terminé.



#### Serviteur invisible

*Conjuration de 1er niveau (rituel)*

**Classes :** Barde,
magicien\...

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un morceau de ficelle et un morceau de bois)

**Durée :** 1 heure

Ce sort crée une force invisible, sans esprit et sans forme qui exécute
des tâches simples à votre ordre jusqu'à la fin du sort. Le serviteur
surgit dans un espace inoccupé sur le sol à portée. Il a une CA de 10, 1
point de vie, et une Force de 2, et ne peut pas attaquer. S'il tombe à
0 points de vie, le sort prend fin.

Une fois à chacun de vos tours, comme action bonus, vous pouvez ordonner
mentalement au serviteur de se déplacer jusqu'à 15 pieds et
d'interagir avec un objet. Le serviteur peut effectuer des tâches
simples qu'un serviteur humain pourrait faire, comme aller chercher des
objets, nettoyer, réparer, plier des vêtements, allumer des feux, servir
de la nourriture et verser du vin.

Une fois que vous avez donné l'ordre, le serviteur exécute la tâche au
mieux de ses capacités jusqu'à ce qu'il l'ait terminée, puis attend
votre prochain ordre.

Si vous ordonnez au serviteur d'effectuer une tâche qui l'éloignerait
de plus de 60 pieds de vous, le sort prend fin.



#### Toucher du vampire

*Nécromancie de 3e niveau*

**Classes :** Cachottier,
Magicien

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

Le toucher de votre main enveloppée d'ombre peut siphonner la force
vitale des autres pour guérir vos blessures. Effectuez une attaque de
sort en mêlée contre une créature à votre portée. En cas de succès, la
cible subit 3d6 points de dégâts nécrotiques, et vous regagnez un nombre
de points de vie égal à la moitié des dégâts nécrotiques infligés.
Jusqu'à ce que le sort prenne fin, vous pouvez refaire cette attaque à
chacun de vos tours en tant qu'action.

***À des niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 4e niveau ou plus, les dégâts augmentent de
1d6 pour chaque niveau d'emplacement supérieur au 3e.



#### Moquerie cruelle

*Tour de magie de l'enchantement*

**Classes :** Barde

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V

**Durée :** Instantané

Vous lancez une série d'insultes parsemées d'enchantements subtils à
une créature que vous pouvez voir à portée. Si la cible peut vous
entendre (mais pas nécessairement vous comprendre), elle doit réussir un
jet de sauvegarde de Sagesse ou subir 1d4 dégâts psychiques et avoir un
désavantage au prochain jet d'attaque qu'elle effectuera avant la fin
de son prochain tour.

Les dégâts de ce sort augmentent de 1d4 lorsque vous atteignez le 5e
niveau (2d4), le 11e niveau (3d4) et le 17e niveau (4d4).



#### Mur de feu

*év évocation de 4ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composants :** V, S, M (un petit morceau de phosphore).

**Durée :** Concentration, jusqu'à 1 minute

Vous créez un mur de feu sur une surface solide à portée. Vous pouvez
faire un mur jusqu'à 60 pieds de long, 20 pieds de haut et 1 pied
d'épaisseur, ou un mur annelé jusqu'à 20 pieds de diamètre, 20 pieds
de haut et 1 pied d'épaisseur. Le mur est opaque et dure toute la durée
du sort.

Lorsque le mur apparaît, chaque créature se trouvant dans sa zone doit
effectuer un jet de sauvegarde de Dextérité. En cas d'échec, la
créature subit 5d8 points de dégâts de feu, ou la moitié en cas de
réussite.

Un côté du mur, sélectionné par vous lorsque vous lancez ce sort,
inflige 5d8 dégâts de feu à chaque créature qui termine son tour dans un
rayon de 3 mètres de ce côté ou à l'intérieur du mur. Une créature
subit les mêmes dégâts lorsqu'elle entre dans le mur pour la première
fois lors d'un tour ou qu'elle y termine son tour. L'autre côté du
mur n'inflige aucun dégât.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 5e niveau ou plus, les dégâts augmentent de 1d8
pour chaque niveau d'emplacement supérieur au 4e.



#### Mur de force

*Evocation de 5ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (une pincée de poudre obtenue en écrasant une
pierre précieuse transparente).

**Durée :** Concentration, jusqu'à 10 minutes

Un mur de force invisible apparaît à l'endroit de votre choix dans la
portée. Le mur apparaît dans l'orientation de votre choix, comme une
barrière horizontale, verticale ou en angle. Il peut flotter librement
ou reposer sur une surface solide. Vous pouvez lui donner la forme d'un
dôme hémisphérique ou d'une sphère d'un rayon maximal de 3 mètres, ou
d'une surface plane composée de dix panneaux de 3 mètres sur 3 mètres.
Chaque panneau doit être contigu à un autre panneau. Quelle que soit sa
forme, le mur a une épaisseur de 1/4 de pouce. Il dure toute la durée du
sort. Si le mur traverse l'espace d'une créature lorsqu'il apparaît,
celle-ci est repoussée d'un côté du mur (vous choisissez de quel côté).

Rien ne peut physiquement traverser le mur. Il est immunisé contre tous
les dégâts et ne peut pas être dissipé par un *sort de dissipation de
la magie*. Cependant, un sort de
*désintégration* détruit le mur instantanément. Le mur
s'étend également dans le plan éthéré, bloquant les voyages éthérés à
travers le mur.



#### Mur de glace

*Evocation de 6ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Les composantes :** V, S, M (un petit morceau de quartz)

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez un mur de glace sur une surface solide à portée. Vous pouvez
lui donner la forme d'un dôme hémisphérique ou d'une sphère d'un
rayon maximal de 10 pieds, ou vous pouvez façonner une surface plane
composée de dix panneaux de 10 pieds carrés. Chaque panneau doit être
contigu à un autre panneau. Quelle que soit sa forme, le mur a une
épaisseur de 30 cm et dure toute la durée du sort.

Si le mur traverse l'espace d'une créature lorsqu'il apparaît,
celle-ci est poussée sur un côté du mur et doit effectuer un jet de
sauvegarde de Dextérité. En cas d'échec, la créature subit 10d6 points
de dégâts de froid, ou la moitié en cas de réussite.

Le mur est un objet qui peut être endommagé et donc percé. Il a une CA
de 12 et 30 points de vie par section de 3 mètres, et il est vulnérable
aux dégâts du feu. Le fait de rapetisser une section de mur de 3 mètres
à 0 points de vie la détruit et laisse derrière elle une nappe d'air
glacial dans l'espace que le mur occupait. Une créature traversant la
nappe d'air glacial pour la première fois pendant un tour doit
effectuer un jet de sauvegarde de Constitution. Cette créature subit 5d6
points de dégâts de froid en cas d'échec, ou la moitié en cas de
réussite.

***Aux niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant
un emplacement de sort de 7e niveau ou plus, les dégâts infligés par le
mur lorsqu'il apparaît augmentent de 2d6, et les dégâts infligés par le
passage à travers la feuille d'air glacial augmentent de 1d6, pour
chaque niveau d'emplacement au-dessus du 6e.



#### Mur de pierre

*Evocation de 5ème niveau*

**Classes :** Druide,
Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un petit bloc de granit)

**Durée :** Concentration, jusqu'à 10 minutes

Un mur non magique de pierre solide prend naissance à l'endroit de
votre choix, à portée. Le mur a une épaisseur de 15 cm et est composé de
dix panneaux de 10 pieds sur 10 pieds. Chaque panneau doit être contigu
à au moins un autre panneau. Vous pouvez également créer des panneaux de
10 pieds sur 20 pieds qui n'ont que 10 cm d'épaisseur.

Si le mur coupe l'espace d'une créature lorsqu'il apparaît, la
créature est poussée d'un côté du mur (au choix). Si une créature
devait être entourée de tous les côtés par le mur (ou le mur et une
autre surface solide), cette créature peut effectuer un jet de
sauvegarde de Dextérité. En cas de réussite, elle peut utiliser sa
réaction pour se déplacer à sa vitesse afin de ne plus être entourée par
le mur.

Le mur peut avoir la forme que vous souhaitez, mais il ne peut pas
occuper le même espace qu'une créature ou un objet. Le mur n'a pas
besoin d'être vertical ou de reposer sur une fondation solide. Il doit
cependant se fondre dans la pierre existante et être solidement soutenu
par elle. Ainsi, vous pouvez utiliser ce sort pour combler un gouffre ou
créer une rampe.

Si vous créez une travée de plus de 20 pieds de long, vous devez diviser
par deux la taille de chaque panneau pour créer des supports. Vous
pouvez façonner grossièrement le mur pour créer des créneaux, des
créneaux, etc.

Le mur est un objet en pierre qui peut être endommagé et donc percé.
Chaque panneau a un AC de 15 et 30 points de vie par pouce d'épaisseur.
Rapetisser un panneau à 0 points de vie le détruit et peut provoquer
l'effondrement des panneaux connectés, à la discrétion du MJ.

Si vous maintenez votre concentration sur ce sort pendant toute sa
durée, le mur devient permanent et ne peut être dissipé. Sinon, le mur
disparaît lorsque le sort prend fin.



#### Mur d'épines

*Conjuration de 6ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (une poignée d'épines)

**Durée :** Concentration, jusqu'à 10 minutes

Vous créez un mur de broussailles robustes, souples et enchevêtrées,
hérissées d'épines acérées. Le mur apparaît à portée sur une surface
solide et dure toute la durée du sort. Vous pouvez choisir de créer un
mur d'une longueur maximale de 60 pieds, d'une hauteur de 10 pieds et
d'une épaisseur de 5 pieds ou un cercle d'un diamètre de 20 pieds,
d'une hauteur maximale de 20 pieds et d'une épaisseur de 5 pieds. Le
mur bloque la ligne de vue.

Lorsque le mur apparaît, chaque créature se trouvant dans sa zone doit
effectuer un jet de sauvegarde de Dextérité. En cas d'échec, la
créature subit 7d8 points de dégâts perforants, ou la moitié en cas de
réussite.

Une créature peut traverser le mur, mais lentement et péniblement. Pour
chaque 1 pied qu'une créature traverse le mur, elle doit dépenser 4
pieds de mouvement. De plus, la première fois qu'une créature entre
dans le mur pendant un tour ou y termine son tour, elle doit effectuer
un jet de sauvegarde de Dextérité. Elle subit 7d8 dégâts tranchants en
cas de sauvegarde ratée, ou la moitié en cas de réussite.

***Niveaux supérieurs.*** Lorsque vous lancez ce sort en utilisant un
emplacement de sort de 7e niveau ou plus, les deux types de dégâts
augmentent de 1d8 pour chaque niveau d'emplacement supérieur au 6e.



#### Lien de protection

*ab ab abjuration de 2ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** Toucher

**Composantes :** V, S, M (une paire d'anneaux en platine d'une valeur
d'au moins 50 gp chacun, que vous et la cible devez porter pendant la
durée de l'opération).

**Durée :** 1 heure

Ce sort protège une créature volontaire que vous touchez et crée un lien
mystique entre vous et la cible jusqu'à la fin du sort. Tant que la
cible se trouve à moins de 60 pieds de vous, elle bénéficie d'un bonus
de +1 à la CA et aux jets de sauvegarde, et elle est résistante à tous
les dégâts. De plus, chaque fois qu'elle subit des dégâts, vous
subissez la même quantité de dégâts.

Le sort prend fin si vous tombez à 0 points de vie ou si vous et la
cible êtes séparés de plus de 60 pieds. Il prend également fin si le
sort est à nouveau lancé sur l'une ou l'autre des créatures
connectées. Vous pouvez également annuler le sort par une action.



#### Respiration aquatique

*Transmutation de 3e niveau (rituel)*

**Classes :** Druide,
Sorcier

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Composantes :** V, S, M (un roseau court ou un morceau de paille)

**Durée :** 24 heures

Ce sort confère à un maximum de dix créatures volontaires que vous
pouvez voir à portée la capacité de respirer sous l'eau jusqu'à la fin
du sort. Les créatures affectées conservent également leur mode de
respiration normal.



#### Marche sur l'eau

*Transmutation de 3e niveau (rituel)*

**Classes :** Clerc,
Rôdeur

**Temps d'incantation :** 1 action

**Portée :** 30 pieds

**Les composantes :** V, S, M (un morceau de liège)

**Durée :** 1 heure

Ce sort confère la capacité de se déplacer sur n'importe quelle surface
liquide (eau, acide, boue, neige, sables mouvants ou lave) comme s'il
s'agissait d'un sol solide inoffensif (les créatures traversant de la
lave en fusion peuvent quand même subir des dégâts dus à la chaleur).
Jusqu'à dix créatures volontaires que vous pouvez voir à portée gagnent
cette capacité pour la durée.

Si vous ciblez une créature submergée dans un liquide, le sort
transporte la cible à la surface du liquide à une vitesse de 60 pieds
par round.



#### Toile d'araignée

*conjuration de 2ème niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S, M (un peu de toile d'araignée)

**Durée :** Concentration, jusqu'à 1 heure

Vous conjurez une masse de toiles épaisses et collantes sur un point de
votre choix à portée. Les toiles remplissent un cube de 6 mètres de côté
à partir de ce point pour la durée du sort. Les toiles constituent un
terrain difficile et obscurcissent légèrement leur zone.

Si les toiles ne sont pas ancrées entre deux masses solides (comme des
murs ou des arbres) ou disposées en couches sur un sol, un mur ou un
plafond, la toile conjurée s'effondre sur elle-même, et le sort prend
fin au début de votre prochain tour. Les toiles superposées sur une
surface plane ont une profondeur de 1,5 mètre.

Chaque créature qui commence son tour dans les toiles ou qui y entre
pendant son tour doit faire un jet de sauvegarde de Dextérité. En cas
d'échec, la créature est entravée tant qu'elle reste dans les toiles
ou jusqu'à ce qu'elle se libère.

Une créature entravée par les toiles peut utiliser son action pour faire
un test de Force contre le DC de sauvegarde de votre sort. Si elle
réussit, elle n'est plus entravée.

Les toiles sont inflammables. Tout cube de 1,5 m de toiles exposé au feu
se consume en 1 round, infligeant 2d4 points de dégâts de feu à toute
créature qui commence son tour dans le feu.



#### Étrangeté

*Ill illusion du 9ème niveau*

**Classes :** Magicien

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S

**Durée :** Concentration, jusqu'à 1 minute

En puisant dans les peurs les plus profondes d'un groupe de créatures,
vous créez dans leur esprit des créatures illusoires, visibles
uniquement par elles. Chaque créature située dans une sphère de 30 pieds
de rayon centrée sur un point de votre choix à portée doit effectuer un
jet de sauvegarde de Sagesse. En cas d'échec, la créature est effrayée
pour la durée du sort. L'illusion fait appel aux peurs les plus
profondes de la créature, manifestant ses pires cauchemars sous la forme
d'une menace implacable. À la fin de chaque tour de la créature
effrayée, elle doit réussir un jet de sauvegarde de Sagesse ou subir
4d10 dégâts psychiques. En cas de sauvegarde réussie, le sort prend fin
pour cette créature.



#### Marche sur le vent

*Transmutation de 6ème niveau*

**Classes :** Druide

**Temps d'incantation :** 1 minute

**Portée :** 30 pieds

**Composantes :** V, S, M (feu et eau bénite)

**Durée :** 8 heures

Vous et jusqu'à dix créatures volontaires que vous pouvez voir dans la
portée adoptez une forme gazeuse pour la durée, apparaissant comme des
volutes de nuages. Lorsqu'elle est sous cette forme, la créature a une
vitesse de vol de 300 pieds et résiste aux dégâts des armes non
magiques. Les seules actions qu'une créature peut entreprendre sous
cette forme sont celles de revenir à sa forme normale. Le retour à la
forme normale prend 1 minute, pendant laquelle la créature est frappée
d'incapacité et ne peut pas bouger. Jusqu'à la fin du sort, une
créature peut revenir à la forme nuage, ce qui nécessite également la
transformation d'une minute.

Si une créature est sous forme de nuage et qu'elle vole lorsque
l'effet prend fin, la créature descend de 60 pieds par round pendant 1
minute jusqu'à ce qu'elle atterrisse, ce qu'elle fait en toute
sécurité. Si elle ne peut pas atterrir après 1 minute, la créature tombe
de la distance restante.



#### Mur de vent

*3rd-level evocation*

**Classes :** Druide

**Temps d'incantation :** 1 action

**Portée :** 120 pieds

**Composantes :** V, S, M (un très petit éventail et une plume
d'origine exotique).

**Durée :** Concentration, jusqu'à 1 minute

Un mur de vent fort s'élève du sol à un point que vous choisissez dans
la portée. Vous pouvez créer un mur de 15 mètres de long, 15 mètres de
haut et 1 mètre d'épaisseur. Vous pouvez donner au mur la forme que
vous voulez, tant qu'il forme un chemin continu le long du sol. Le mur
dure pendant toute la durée du sort.

Lorsque le mur apparaît, chaque créature dans sa zone doit effectuer un
jet de sauvegarde de Force. Une créature subit 3d8 points de dégâts
contondants en cas d'échec, ou la moitié en cas de réussite.

Le vent fort éloigne le brouillard, la fumée et les autres gaz. Les
petites créatures ou objets volants ne peuvent pas traverser le mur. Les
matériaux lâches et légers introduits dans le mur volent vers le haut.
Les flèches, boulons et autres projectiles ordinaires lancés vers des
cibles situées derrière le mur sont déviés vers le haut et manquent
automatiquement leur cible. (Les rochers lancés par les géants ou les
engins de siège, et les projectiles similaires, ne sont pas affectés).
Les créatures sous forme gazeuse ne peuvent pas le traverser.



#### Souhait

*conjuration de 9e niveau*

**Classes :** Sorcier

**Temps d'incantation :** 1 action

**Portée :** Self

**Composantes :** V

**Durée :** Instantané

*Souhait* est le plus puissant des sorts qu'une créature
mortelle puisse lancer. En parlant simplement à voix haute, vous pouvez
modifier les fondements mêmes de la réalité en fonction de vos désirs.

L'utilisation de base de ce sort est de dupliquer tout autre sort de
8ème niveau ou moins. Vous n'avez pas besoin de remplir les conditions
de ce sort, y compris les composantes coûteuses. Le sort prend
simplement effet.

Vous pouvez également créer l'un des effets suivants de votre choix :

-   Vous créez un objet d'une valeur maximale de 25 000 gp qui n'est
    pas un objet magique. L'objet ne peut pas être plus de 300 pieds
    dans n'importe quelle dimension, et il apparaît dans un espace
    inoccupé que vous pouvez voir sur le sol.
-   Vous permettez à un maximum de vingt créatures que vous pouvez voir
    de regagner tous leurs points de vie, et vous mettez fin à tous les
    effets sur elles décrits dans le sort de *restauration
    supérieure*.
-   Vous accordez à un maximum de dix créatures que vous pouvez voir une
    résistance à un type de dégâts que vous choisissez.
-   Vous accordez à un maximum de dix créatures que vous pouvez voir
    l'immunité à un seul sort ou autre effet magique pendant 8 heures.
    Par exemple, vous pouvez vous immuniser, ainsi que tous vos
    compagnons, contre l'attaque de drain de vie d'une liche.
-   Vous annulez un événement récent en forçant un nouveau jet pour tout
    jet effectué au cours du dernier round (y compris votre dernier
    tour). La réalité se remodèle pour s'adapter au nouveau résultat.
    Par exemple, un sort de *Souhait* peut annuler la
    sauvegarde réussie d'un adversaire, le coup critique d'un ennemi
    ou la sauvegarde ratée d'un ami. Vous pouvez forcer le reroll à
    être fait avec avantage ou désavantage, et vous pouvez choisir
    d'utiliser le reroll ou le jet original.

Vous pouvez peut-être obtenir quelque chose qui dépasse le cadre des
exemples ci-dessus. Indiquez votre souhait au MJ aussi précisément que
possible. Le MJ a une grande latitude pour décider de ce qui se passe
dans un tel cas ; plus le souhait est grand, plus la probabilité que
quelque chose se passe mal est grande. Ce sort peut tout simplement
échouer, l'effet désiré peut n'être que partiellement atteint, ou vous
pouvez subir des conséquences imprévues en raison de la façon dont vous
avez formulé votre souhait. Par exemple, souhaiter la mort d'un méchant
peut vous propulser dans le temps jusqu'à une période où ce méchant
n'est plus en vie, vous retirant ainsi du jeu. De même, souhaiter un
objet magique ou un artefact légendaire peut vous transporter
instantanément en présence du propriétaire actuel de l'objet.

Le stress de lancer ce sort pour produire un effet autre que la
duplication d'un autre sort vous affaiblit. Après avoir enduré ce
stress, chaque fois que vous lancez un sort jusqu'à ce que vous ayez
terminé un repos long, vous subissez 1d10 dégâts nécrotiques par niveau
de ce sort. Ces dégâts ne peuvent être ni réduits ni évités d'aucune
façon. De plus, votre Force chute à 3, si elle n'est pas déjà de 3 ou
moins, pendant 2d4 jours. Pour chacun de ces jours que vous passez à
vous reposer et à ne faire qu'une activité légère, votre temps de
récupération restant diminue de 2 jours. Enfin, il y a 33 % de chances
que vous ne puissiez plus jamais lancer de souhaits si vous subissez ce
stress.



#### Mot de retour

*Conjuration de 6ème niveau*

**Classes :** Clerc

**Temps d'incantation :** 1 action

**Portée :** 5 pieds

**Composantes :** V

**Durée :** Instantané

Vous et jusqu'à cinq créatures consentantes situées à 1,5 m de vous
vous téléportez instantanément dans un sanctuaire préalablement désigné.
Vous et les créatures qui se téléportent avec vous apparaissez dans
l'espace inoccupé le plus proche de l'endroit que vous avez désigné
lorsque vous avez préparé votre sanctuaire (voir ci-dessous). Si vous
lancez ce sort sans avoir préalablement préparé un sanctuaire, le sort
n'a aucun effet.

Vous devez désigner un sanctuaire en lançant ce sort dans un lieu, tel
qu'un temple, dédié ou fortement lié à votre divinité. Si vous tentez
de lancer le sort de cette manière dans une zone qui n'est pas dédiée à
votre divinité, le sort n'a aucun effet.



#### Zone de vérité

*Enchantement de 2ème niveau*

**Classes :** Barde,
paladin

**Temps d'incantation :** 1 action

**Portée :** 60 pieds

**Composantes :** V, S

**Durée :** 10 minutes

Vous créez une zone magique qui protège contre la supercherie dans une
sphère de 15 pieds de rayon centrée sur un point de votre choix à
portée. Jusqu'à la fin du sort, une créature qui entre dans la zone du
sort pour la première fois lors d'un tour ou qui commence son tour à
cet endroit doit effectuer un jet de sauvegarde de Charisme. En cas
d'échec, la créature ne peut pas dire un mensonge délibéré tant
qu'elle se trouve dans le rayon. Vous savez si chaque créature réussit
ou échoue à son jet de sauvegarde.

Une créature affectée est consciente du sort et peut ainsi éviter de
répondre aux questions auxquelles elle aurait normalement répondu par un
mensonge. Une telle créature peut être évasive dans ses réponses tant
qu'elle reste dans les limites de la vérité.


