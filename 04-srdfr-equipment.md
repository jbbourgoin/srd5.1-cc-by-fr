
# Équipement


## Monnaie

Les pièces communes se présentent sous plusieurs dénominations
différentes, en fonction de la valeur relative du métal dont elles sont
faites. Les trois pièces les plus communes sont la pièce d'or (gp), la
pièce d'argent (sp) et la pièce de cuivre (pc).

Avec une pièce d'or, un personnage peut acheter un matelas, 15 mètres
de bonne corde ou une chèvre. Un artisan doué (mais pas exceptionnel)
peut gagner une pièce d'or par jour. La pièce d'or est l'unité de
mesure standard de la richesse, même si la pièce elle-même n'est pas
communément utilisée. Lorsque les marchands discutent de transactions
qui impliquent des biens ou des services valant des centaines ou des
milliers de pièces d'or, les transactions n'impliquent généralement
pas l'échange de pièces individuelles. La pièce d'or est plutôt une
mesure standard de la valeur, et l'échange réel se fait en lingots
d'or, en lettres de crédit ou en biens de valeur.

Une pièce d'or vaut dix pièces d'argent, la monnaie la plus répandue
chez les Roturiers. Une pièce d'argent permet d'acheter le travail
d'un ouvrier pendant une demi-journée, une fiole d'huile de lampe ou
une nuit de repos dans une pauvre auberge.

Une pièce d'argent vaut dix pièces de cuivre, qui sont communes aux
ouvriers et aux mendiants. Une seule pièce de cuivre permet d'acheter
une bougie, une torche ou un morceau de craie.

En outre, des pièces inhabituelles constituées d'autres métaux précieux
apparaissent parfois dans les trésors. La pièce d'électrum (pe) et la
pièce de platine ( pp) proviennent d'empires déchus et de royaumes
perdus, et elles suscitent parfois suspicion et scepticisme
lorsqu'elles sont utilisées dans des transactions. Une pièce
d'électrum vaut cinq pièces d'argent, et une pièce de platine vaut dix
pièces d'or.

Une pièce standard pèse environ un tiers d'once, donc cinquante pièces
pèsent une livre.

  ------------------------------------------------------
  Pièce de            CP     SP     EP      GP        PP
  monnaie                                      
  -------------- ------- ------ ------ ------- ---------
  Cuivre (cp)          1   1/10   1/50   1/100   1/1,000

  Argent (sp)         10      1    1/5    1/10     1/100

  Electrum (ep)       50      5      1     1/2      1/20

  Or (gp)            100     10      2       1      1/10

  Platine (pp)     1,000    100     20      10         1
  ------------------------------------------------------

  : Taux de change standard



## Vente d'un trésor

Les occasions de trouver des trésors, de l'équipement, des armes, des
armures et bien d'autres choses encore abondent dans les donjons que
vous explorez. Normalement, vous pouvez vendre vos trésors et babioles
lorsque vous retournez dans une ville ou un autre établissement, à
condition de trouver des acheteurs et des marchands intéressés par votre
butin.

***Armes, armures et autres équipements.*** En règle générale, les
armes, armures et autres équipements non endommagés rapportent la moitié
de leur prix lorsqu'ils sont vendus sur un marché. Les armes et armures
utilisées par les monstres sont rarement en assez bon état pour être
vendues.

***Objets magiques.*** La vente d'objets magiques est problématique.
Trouver quelqu'un pour acheter une potion ou un parchemin n'est pas
trop difficile, mais les autres objets sont hors de portée de la plupart
des nobles, à l'exception des plus riches. De même, à part quelques
objets magiques communs, vous ne trouverez pas normalement d'objets
magiques ou de sorts à acheter. La valeur de la magie est bien
supérieure à celle de l'or et doit toujours être traitée comme telle.

***Pierres précieuses, bijoux et objets d'art.*** Ces objets conservent
toute leur valeur sur le marché, et vous pouvez les échanger contre de
l'argent ou les utiliser comme monnaie pour d'autres transactions.
Pour les trésors d'une valeur exceptionnelle, le MJ peut vous demander
de trouver d'abord un acheteur dans une grande ville ou une grande
communauté.

***Biens d'échange.*** Dans les régions frontalières, de nombreuses
personnes effectuent des transactions par le biais du troc. Tout comme
les pierres précieuses et les objets d'art, les biens d'échange -
barres de fer, sacs de sel, bétail, etc. - conservent leur pleine valeur
sur le marché et peuvent être utilisés comme monnaie.



## Équipement pour l'aventure

Cette section décrit les éléments qui ont des règles spéciales ou qui
nécessitent des explications supplémentaires.

***Acide.*** Comme une action, vous pouvez éclabousser le contenu de
cette fiole sur une créature dans un rayon de 1,5 mètre de vous ou
lancer la fiole jusqu'à 6 mètres, la faisant éclater à l'impact. Dans
les deux cas, effectuez une attaque à distance contre une créature ou un
objet, en traitant l'acide comme une arme improvisée. En cas de succès,
la cible subit 2d6 points de dégâts d'acide.

***Le feu de l'alchimiste.*** Ce fluide collant et adhésif s'enflamme
lorsqu'il est exposé à l'air. En tant qu'action, vous pouvez lancer
cette fiole jusqu'à 6 mètres, la faisant éclater à l'impact. Effectuez
une attaque à distance contre une créature ou un objet, en traitant le
feu de l'alchimiste comme une arme improvisée. En cas de succès, la
cible subit 1d4 dégâts de feu au début de chacun de ses tours. Une
créature peut mettre fin à ces dégâts en utilisant son action pour faire
un test de Dextérité DC 10 pour éteindre les flammes.

***Antitoxine.*** Une créature qui boit cette fiole de liquide gagne un
avantage aux jets de sauvegarde contre le poison pendant 1 heure. Elle
ne confère aucun avantage aux morts-vivants ou aux constructions.

***Foyer des arcanes.*** Un foyer des arcanes est un objet spécial - un
orbe, un cristal, une tige, un bâton spécialement construit, une
baguette de bois ou un objet similaire - conçu pour canaliser la
puissance des sorts des arcanes. Un sorcier, un sorcier ou un magicien
peut utiliser un tel objet comme foyer d'incantation.

***Roulements à billes.*** En tant qu'action, vous pouvez déverser ces
minuscules boules de métal de leur pochette pour couvrir une zone carrée
de niveau et de 10 pieds de côté. Une créature se déplaçant à travers la
zone couverte doit réussir un jet de sauvegarde de Dextérité DC 10 ou
tomber à terre. Une créature se déplaçant à travers la zone à
demi-vitesse n'a pas besoin de faire ce jet de sauvegarde.

***Bloc et palan.*** Un ensemble de poulies traversées par un câble et
munies d'un crochet pour les attacher à des objets, un palan vous
permet de soulever jusqu'à quatre fois le poids que vous pouvez
normalement soulever.

***Livre.*** Un livre peut contenir de la poésie, des récits
historiques, des informations sur un domaine particulier, des diagrammes
et des notes sur des engins gnomes, ou à peu près tout ce qui peut être
représenté par du texte ou des images. Un livre de sorts est un livre de
sorts.

***Caltrops.*** En tant qu'action, vous pouvez étendre un sac de
caltrops pour couvrir une zone carrée de 1,5 m de côté. Toute créature
qui pénètre dans la zone doit réussir un jet de sauvegarde de Dextérité
DC 15 ou cesser de se déplacer ce tour-ci et subir 1 dégât perforant. Le
fait de subir ces dégâts réduit la vitesse de marche de la créature de
10 pieds jusqu'à ce qu'elle récupère au moins 1 point de vie. Une
créature se déplaçant dans la zone à demi-vitesse n'a pas besoin de
faire ce jet de sauvegarde.

***Bougie.*** Pendant 1 heure, une bougie diffuse une lumière vive dans
un rayon de 1,5 mètre et une lumière tamisée sur 1,5 mètre
supplémentaire.

***Étui, boulon d'arbalète.*** Cet étui en bois peut contenir jusqu'à
vingt carreaux d'arbalète.

***Étui, carte ou parchemin.*** Cet étui cylindrique en cuir peut
contenir jusqu'à dix feuilles de papier enroulées ou cinq feuilles de
parchemin enroulées.

***Chaîne.*** Une chaîne a 10 points de vie. Elle peut être brisée avec
un test de Force DC 20 réussi.

***Kit du grimpeur.*** Le kit du grimpeur comprend des pitons spéciaux,
des embouts de bottes, des gants et un harnais. Vous pouvez utiliser le
kit du grimpeur comme une action pour vous ancrer ; lorsque vous le
faites, vous ne pouvez pas tomber à plus de 25 pieds du point où vous
vous êtes ancré, et vous ne pouvez pas grimper à plus de 25 pieds de ce
point sans défaire l'ancrage.

***Pochette à composantes.*** Une pochette de composants est une petite
pochette de ceinture en cuir étanche qui comporte des compartiments pour
contenir tous les composants matériels et autres objets spéciaux dont
vous avez besoin pour lancer vos sorts, à l'exception des composants
qui ont un coût spécifique (comme indiqué dans la description d'un
sort).

***Pied-de-biche.*** L'utilisation d'un pied-de-biche confère un
avantage aux tests de Force lorsque l'effet de levier du pied-de-biche
peut être utilisé.

***Foyer druidique.*** Un foyer druidique peut être une branche de gui
ou de houx, une baguette ou un sceptre en if ou en un autre bois
spécial, un bâton entièrement tiré d'un arbre vivant, ou un objet totem
incorporant des plumes, de la fourrure, des os et des dents d'animaux
sacrés. Un druide peut utiliser un tel objet comme foyer d'incantation.

***Matériel de pêche.*** Ce kit comprend une canne en bois, une ligne en
soie, des bobbers en bois de liège, des hameçons en acier, des plombs en
plomb, des leurres en velours et des filets étroits.

***La trousse du Guérisseur.*** Cette trousse est une pochette en cuir
contenant des bandages, des pommades et des attelles. La trousse a dix
utilisations. Comme action, vous pouvez dépenser une utilisation de la
trousse pour stabiliser une créature qui a 0 points de vie, sans avoir
besoin de faire un test de Sagesse (Médecine).

***Symbole sacré.*** Un symbole sacré est la représentation d'un dieu
ou d'un panthéon. Il peut s'agir d'une amulette représentant le
symbole d'une divinité, du même symbole soigneusement gravé ou incrusté
comme emblème sur un bouclier, ou d'une minuscule boîte contenant le
fragment d'une relique sacrée. La section
\"Panthéons\" répertorie les symboles communément
associés à de nombreux dieux dans le multivers. Un clerc ou un paladin
peut utiliser un symbole sacré comme point focal d'incantation. Pour
utiliser le symbole de cette manière, le lanceur de sorts doit le tenir
en main, le porter de manière visible ou le porter sur un bouclier.

***Eau bénite.*** En tant qu'action, vous pouvez éclabousser le contenu
de cette fiole sur une créature dans un rayon de 1,5 m de vous ou la
lancer jusqu'à 6 m, la faisant éclater à l'impact. Dans les deux cas,
effectuez une attaque à distance contre une créature cible, en traitant
l'eau bénite comme une arme improvisée. Si la cible est un fiélon ou un
mort-vivant, elle subit 2d6 points de dégâts radiants.

Un clerc ou un paladin peut créer de l'eau bénite en effectuant un
rituel spécial. Le rituel prend 1 heure, utilise 25 gp de poudre
d'argent et nécessite que le lanceur de sorts dépense un emplacement de
sort de 1er niveau.

***Piège de chasse.*** Lorsque vous utilisez votre action pour le poser,
ce piège forme un anneau d'acier en dents de scie qui se referme
lorsqu'une créature marche sur une plaque de pression au centre. Le
piège est fixé par une lourde chaîne à un objet immobile, comme un arbre
ou un pic enfoncé dans le sol. Une créature qui marche sur la plaque
doit réussir un jet de sauvegarde de Dextérité DC 13 ou subir 1d4 dégâts
perforants et ne plus bouger. Par la suite, jusqu'à ce que la créature
se libère du piège, son mouvement est limité par la longueur de la
chaîne (généralement 3 pieds de long). Une créature peut utiliser son
action pour effectuer un test de Force DC 13, se libérant ou libérant
une autre créature à sa portée en cas de réussite. Chaque échec inflige
1 dégât perforant à la créature piégée.

***Lampe.*** Une lampe projette une lumière vive dans un rayon de 15
pieds et une lumière chétive dans un rayon de 30 pieds supplémentaires.
Une fois allumée, elle brûle pendant 6 heures avec une fiole (1 pinte)
d'huile.

***Lanterne à œil de bœuf.*** Une lanterne à œil de bœuf projette une
lumière vive dans un cône de 60 pieds et une lumière chétive sur 60
pieds supplémentaires. Une fois allumée, elle brûle pendant 6 heures
avec une fiole (1 pinte) d'huile.

***Lanterne à capuchon.*** Une lanterne à capuchon projette une lumière
vive dans un rayon de 30 pieds et une lumière chétive sur 30 pieds
supplémentaires. Une fois allumée, elle brûle pendant 6 heures avec une
fiole (1 pinte) d'huile. En tant qu'action, vous pouvez abaisser le
capuchon, réduisant la lumière à une lumière tamisée dans un rayon de 5
pieds.

***Serrure.*** Une clé est fournie avec la serrure. Sans la clé, une
créature maîtrisant les outils de voleur peut crocheter cette serrure en
réussissant un test de Dextérité DC 15. Votre MJ peut décider que de
meilleures serrures sont disponibles à des prix plus élevés.

***Loupe.*** Cette loupe permet d'observer de plus près les petits
objets. Elle est également utile pour remplacer le silex et l'acier
lors de l'allumage de feux. Pour allumer un feu avec une loupe, il faut
une lumière aussi vive que celle du soleil, de l'amadou pour
s'enflammer et environ 5 minutes pour que le feu prenne. Une loupe
donne un avantage sur tout test d'aptitude effectué pour évaluer ou
inspecter un objet qui est petit ou très détaillé.

***Manacles.*** Ces entraves métalliques peuvent lier une créature de
Petit ou Moyen format. Pour s'échapper des menottes, il faut réussir un
test de Dextérité DC 20. Pour les briser, il faut réussir un test de
Force DC 20. Chaque jeu de menottes est accompagné d'une clé. Sans la
clé, une créature maîtrisant les outils de voleur peut crocheter la
serrure des manilles avec un test de Dextérité DC 15 réussi. Les
menottes ont 15 points de vie.

***Kit mess.*** Cette boîte en fer blanc contient une tasse et des
couverts simples. La boîte s'attache ensemble, et un côté peut être
utilisé comme une casserole et l'autre comme une assiette ou un bol peu
profond.

***Huile.*** L'huile est généralement présentée dans une fiole en
argile d'une contenance de 1 pinte. Comme une action, vous pouvez
éclabousser l'huile dans cette fiole sur une créature dans un rayon de
1,5 m de vous ou la lancer jusqu'à 6 m, la faisant éclater à l'impact.
Effectuez une attaque à distance contre une créature ou un objet cible,
en traitant l'huile comme une arme improvisée. En cas de succès, la
cible est couverte d'huile. Si la cible subit des dégâts de feu avant
que l'huile ne sèche (après 1 minute), elle subit 5 dégâts de feu
supplémentaires dus à l'huile brûlante. Vous pouvez également verser
une fiole d'huile sur le sol pour couvrir une zone de 5 pieds carrés, à
condition que la surface soit plane. Si elle est allumée, l'huile brûle
pendant 2 rounds et inflige 5 dégâts de feu à toute créature qui entre
dans la zone ou termine son tour dans cette zone. Une créature ne peut
subir ces dégâts qu'une fois par tour.

***Poison, de base.*** Vous pouvez utiliser le poison contenu dans cette
fiole pour enduire une arme tranchante ou perforante ou jusqu'à trois
munitions. L'application du poison nécessite une action. Une créature
touchée par l'arme ou les munitions empoisonnées doit réussir un jet de
sauvegarde de Constitution DC 10 ou subir 1d4 dégâts de poison. Une fois
appliqué, le poison conserve sa puissance pendant 1 minute avant de
sécher.

***Potion de guérison.*** Un personnage qui boit le fluide rouge magique
contenu dans cette fiole regagne 2d4 + 2 points de vie. Boire ou
administrer une potion prend une action.

***Pochette.*** Une pochette en tissu ou en cuir peut contenir jusqu'à
20 balles de fronde ou 50 aiguilles de sarbacane, entre autres. Une
pochette compartimentée destinée à contenir des composants de sorts est
appelée pochette de composants (décrite plus haut dans cette section).

***Carquois.*** Un carquois peut contenir jusqu'à 20 flèches.

***Bélier, portable.*** Vous pouvez utiliser un bélier portable pour
enfoncer des portes. Ce faisant, vous bénéficiez d'un bonus de +4 au
test de Force. Un autre personnage peut vous aider à utiliser le bélier,
ce qui vous donne un avantage sur ce test.

***Rations.*** Les rations sont constituées d'aliments secs adaptés à
un voyage prolongé, notamment du jerky, des fruits secs, du hardtack et
des noix.

***Corde.*** Une corde, qu'elle soit en chanvre ou en soie, a 2 points
de vie et peut être éclatée avec un test de Force DC 17.

***Balance de commerçant.*** Une balance comprend une petite balance,
des plateaux et un assortiment approprié de poids jusqu'à 2 livres.
Grâce à elle, vous pouvez mesurer le poids exact de petits objets, tels
que des métaux précieux bruts ou des marchandises de commerce, afin de
déterminer leur valeur.

***Livre de sorts.*** Indispensable pour les magiciens, le livre de
sorts est un tome relié en cuir comportant 100 pages vierges en vélin
destinées à l'enregistrement des sorts.

***La lorgnette.*** Les objets vus à travers une lorgnette sont grossis
à deux fois leur taille.

***Tente.*** Abri en toile simple et portable, la tente permet
d'accueillir deux personnes.

***Boîte à amadou.*** Ce petit récipient contient des silex, de l'acier
à feu et de l'amadou (généralement du tissu sec imbibé d'huile légère)
utilisés pour allumer un feu. L'utiliser pour allumer une torche - ou
tout autre objet dont le combustible est abondant et exposé - prend une
action. L'allumage de tout autre feu prend 1 minute.

***Torche.*** Une torche brûle pendant 1 heure, fournissant une lumière
vive dans un rayon de 20 pieds et une lumière chétive sur 20 pieds
supplémentaires. Si vous effectuez une attaque de mêlée avec une torche
enflammée et que vous touchez, elle inflige 1 dégât de feu.

  -----------------------------------------------------
  Article                            Coût     Poids
  ----------------------------- --------- -------------
  Abacus                             2 gp     2 lb.

  Acide (flacon)                    25 gp     1 lb.

  Feu de l'alchimiste (flacon)     50 gp     1 lb.

  *Munitions*                             

  Flèches (20)                       1 gp     1 lb.

  Aiguilles de sarbacane (50)        1 gp     1 lb.

  Pistolets d'arbalète (20)         1 gp   1-1/2 lb.

  Balles frondes (20)                4 cp   1-1/2 lb.

  Antitoxine (flacon)               50 gp      \-

  *Focalisation sur les                   
  arcanes*                                

  Cristal                           10 gp     1 lb.

  Orb                               20 gp     3 lb.

  Rod                               10 gp     2 lb.

  Personnel                          5 gp     4 lb.

  Baguette                          10 gp     1 lb.

  Sac à dos                          2 gp     5 lb.

  Roulements à billes (sac de 1      1 gp     2 lb.
  000)                                    

  Tonneau                            2 gp    70 lb.

  Panier                             4 sp     2 lb.

  Linge de lit                       1 gp     7 lb.

  Bell                               1 gp      \-

  Couverture                         5 sp     3 lb.

  Blocage et plaquage                1 gp     5 lb.

  Livre                             25 gp     5 lb.

  Bouteille, verre                   2 gp     2 lb.

  Seau                               5 cp     2 lb.

  Caltrops (sac de 20)               1 gp     2 lb.

  Bougie                             1 cp      \-

  Etui, carreau d'arbalète          1 gp     1 lb.

  Cas, carte ou défilement           1 gp     1 lb.

  Chaîne (10 pieds)                  5 gp    10 lb.

  Craie (1 pièce)                    1 cp      \-

  Poitrine                           5 gp    25 lb.

  Kit du grimpeur                   25 gp    12 lb.

  Vêtements, communs.                5 sp     3 lb.

  Vêtements, costumes                5 gp     4 lb.

  Vêtements, fins                   15 gp     6 lb.

  Vêtements de voyage                2 gp     4 lb.

  Pochette pour les composantes     25 gp     2 lb.

  Crowbar                            2 gp     5 lb.

  *Focus druidique*                       

  Brin de gui                        1 gp      \-

  Totem                              1 gp      \-

  Bâton en bois                      5 gp     4 lb.

  Baguette d'if                    10 gp     1 lb.

  Matériel de pêche                  1 gp     4 lb.

  Flacon ou chope                    2 cp     1 lb.

  Crochet agrippé                    2 gp     4 lb.

  Marteau                            1 gp     3 lb.

  Marteau, luge                      2 gp    10 lb.

  Trousse du Guérisseur                    5 gp     3 lb.

  *Symbole sacré*                         

  Amulette                           5 gp     1 lb.

  Emblème                            5 gp      \-

  Reliquaire                         5 gp     2 lb.

  Eau bénite (flacon)               25 gp     1 lb.

  Sablier                           25 gp     1 lb.

  Piège de chasse                    5 gp    25 lb.

  Encre (bouteille de 1 once)       10 gp      \-

  Stylo à encre                      2 cp      \-

  Pichet ou cruche                   2 cp     4 lb.

  Échelle (10 pieds)                 1 sp    25 lb.

  Lampe                              5 sp     1 lb.

  Lanterne, œil de bœuf             10 gp     2 lb.

  Lanterne, à capuchon               5 gp     2 lb.

  Serrure                           10 gp     1 lb.

  Loupe                            100 gp      \-

  Manacles                           2 gp     6 lb.

  Mess kit                           2 sp     1 lb.

  Miroir, acier                      5 gp    1/2 lb.

  Huile (flacon)                     1 sp     1 lb.

  Papier (une feuille)               2 sp      \-

  Parchemin (une feuille)            1 sp      \-

  Parfum (flacon)                    5 gp      \-

  Aiguille de mineur                 2 gp    10 lb.

  Piton                              5 cp 1/4 de livre.

  Poison, basique (flacon)         100 gp      \-

  Mât (10 pieds)                     5 cp     7 lb.

  Pot, fer                           2 gp    10 lb.

  Potion de guérison                50 gp    1/2 lb.

  Pochette                           5 sp     1 lb.

  Carquois                           1 gp     1 lb.

  Ram, portable                      4 gp    35 lb.

  Rations (1 jour)                   5 sp     2 lb.

  Robes de chambre                   1 gp     4 lb.

  Corde de chanvre (50 pieds)        1 gp    10 lb.

  Corde, soie (50 pieds)            10 gp     5 lb.

  Sack                               1 cp    1/2 lb.

  Balance du commerçant              5 gp     3 lb.

  Cire d'étanchéité                 5 sp      \-

  Pelle                              2 gp     5 lb.

  Sifflet de signalisation           5 cp      \-

  Chevalière                         5 gp      \-

  Savon                              2 cp      \-

  Livre de sorts                    50 gp     3 lb.

  Pointes en fer (10)                1 gp     5 lb.

  Spyglass                       1 000 gp     1 lb.

  Tente, deux personnes              2 gp    20 lb.

  Boîte à outils                     5 sp     1 lb.

  Torche                             1 cp     1 lb.

  Flacon                             1 gp      \-

  Serviette à eau                    2 sp 5 lb (plein)

  Whetstone                          1 cp     1 lb.
  -----------------------------------------------------

  : Équipement pour l'aventure

  --------------------------------------------------------
  Conteneur         Capacité
  ----------------- --------------------------------------
  Sac à dos\*       1 pied cube/30 livres d'équipement

  Tonneau           40 gallons liquides, 4 pieds cubes
                    solides

  Panier            2 pieds cubes/40 livres d'équipement

  Bouteille         1-1/2 pintes de liquide

  Seau              3 gallons liquides, 1/2 pied cube
                    solide

  Poitrine          12 pieds cubes/300 livres
                    d'équipement

  Flacon ou chope   1 pinte de liquide

  Pichet ou cruche  1 gallon de liquide

  Pot, fer          1 gallon de liquide

  Pochette          1/5 pied cube/6 livres de matériel

  Sack              1 pied cube/30 livres d'équipement

  Flacon            4 onces de liquide

  Serviette à eau   4 pintes de liquide
  --------------------------------------------------------

  : Capacité du conteneur

\* Vous pouvez également attacher des objets, tels qu'un sac de
couchage ou une bobine de corde, à l'extérieur d'un sac à dos.

> #### Packs d'équipement {#equipment-packs}
>
> L'équipement de départ que vous recevez de votre classe comprend une
> collection d'équipements d'aventure utiles, réunis dans un pack. Le
> contenu de ces packs est répertorié ici. Si vous achetez votre
> équipement de départ, vous pouvez acheter un pack pour le prix
> indiqué, ce qui peut être moins cher que d'acheter les articles
> individuellement.
>
> ***Pack du cambrioleur (16 gp).*** Comprend un sac à dos, un sac de 1
> 000 roulements à billes, 10 pieds de ficelle, une cloche, 5 bougies,
> un pied de biche, un marteau, 10 pitons, une lanterne à capuchon, 2
> flacons d'huile, 5 jours de rations, un amadou et une gourde. Le sac
> a également 15 mètres de corde de chanvre attachée sur le côté.
>
> ***Pack du Diplomate (39 gp).*** Comprend un coffre, 2 étuis pour
> cartes et parchemins, un ensemble de vêtements fins, une bouteille
> d'encre, un stylo à encre, une lampe, 2 flacons d'huile, 5 feuilles
> de papier, une fiole de parfum, de la cire à cacheter et du savon.
>
> ***Pack du Dungeoneer (12 gp).*** Comprend un sac à dos, un
> pied-de-biche, un marteau, 10 pitons, 10 torches, une amadouette, 10
> jours de rations et une gourde. Le sac a également 50 pieds de corde
> de chanvre attachés sur le côté.
>
> ***Pack de l'artiste (40 gp).*** Comprend un sac à dos, un rouleau de
> lit, 2 costumes, 5 bougies, 5 jours de rations, une gourde et un kit
> de déguisement.
>
> ***Pack de l'explorateur (10 gp).*** Comprend un sac à dos, un sac de
> couchage, une gamelle, une amadouette, 10 torches, 10 jours de rations
> et une gourde. Le sac a également 50 pieds de corde de chanvre
> attachés sur le côté.
>
> ***Sac du Prêtre (19 gp).*** Comprend un sac à dos, une couverture, 10
> bougies, un amadou, une boîte à aumônes, 2 blocs d'encens, un
> encensoir, des vêtements, 2 jours de rations et une gourde.
>
> ***Sac de l'érudit (40 gp).*** Comprend un sac à dos, un livre
> d'érudition, une bouteille d'encre, un stylo à encre, 10 feuilles de
> parchemin, un petit sac de sable et un petit couteau.



## Outils

Un outil vous aide à faire quelque chose que vous ne pourriez pas faire
autrement, comme fabriquer ou réparer un objet, forger un document ou
crocheter une serrure. Votre origine, votre classe, vos antécédents ou vos
dons vous donnent la maîtrise de certains outils. La maîtrise d'un
outil vous permet d'ajouter votre bonus de maîtrise à tout test
d'aptitude effectué avec cet outil. L'utilisation d'un outil n'est
pas liée à une seule aptitude, car la maîtrise d'un outil représente
une connaissance plus large de son utilisation. Par exemple, le MJ peut
vous demander de faire un test de Dextérité pour graver un détail fin
avec vos outils de sculpteur sur bois, ou un test de Force pour
fabriquer un objet dans un bois particulièrement dur.

  -----------------------------------------
  Article                    Coût    Poids
  ------------------------- ------- -------
  *Outils d'artisan*               

  Fournitures de             50 gp   8 lb.
  l'alchimiste                     

  Fournitures de brasserie   20 gp   9 lb.

  Fournitures pour           10 gp   5 lb.
  calligraphes                      

  Outils de menuisier        8 gp    6 lb.

  Outils du cartographe      15 gp   6 lb.

  Outils de cordonnier       5 gp    5 lb.

  Ustensiles de cuisine      1 gp    8 lb.

  Outils de souffleur de     30 gp   5 lb.
  verre                             

  Outils de bijoutier        25 gp   2 lb.

  Outils du maroquinier      5 gp    5 lb.

  Les outils du maçon        10 gp   8 lb.

  Fournitures pour peintres  10 gp   5 lb.

  Outils du potier           10 gp   3 lb.

  Les outils de Smith        20 gp   8 lb.

  Les outils du Bricoleur    50 gp  10 lb.

  Outils du tisserand        1 gp    5 lb.

  Outils du sculpteur sur    1 gp    5 lb.
  bois                              

  Kit de déguisement         25 gp   3 lb.

  Kit de contrefaçon         15 gp   5 lb.

  *Set de jeu*                      

  Jeu de dés                 1 sp     \-

  Jeu de cartes à jouer      5 sp     \-

  Kit d'herboristerie       5 gp    3 lb.

  *Instrument de musique*           

  Cornemuses                 30 gp   6 lb.

  Tambour                    6 gp    3 lb.

  Dulcimer                   25 gp  10 lb.

  Flûte traversière          2 gp    1 lb.

  Luth                       35 gp   2 lb.

  Lyre                       30 gp   2 lb.

  Corne                      3 gp    2 lb.

  Flûte de Pan               12 gp   2 lb.

  Shawm                      2 gp    1 lb.

  Viol                       30 gp   1 lb.

  Outils du navigateur       25 gp   2 lb.

  Kit de l'empoisonneur.    50 gp   2 lb.

  Outils de voleurs          25 gp   1 lb.

  Véhicules (terrestres ou    \*      \*
  aquatiques)                       
  -----------------------------------------

  : Outils

\* Voir la section \"Supports et
véhicules\".

***Outils d'artisan.*** Ces outils spéciaux comprennent les objets
nécessaires à l'exercice d'un métier ou d'une profession. La table
montre des exemples des types d'outils les plus communs, chacun
fournissant des objets liés à un seul métier. La maîtrise d'un ensemble
d'outils d'artisan vous permet d'ajouter votre bonus de maîtrise à
tous les tests d'aptitude que vous effectuez en utilisant les outils de
votre métier. Chaque type d'outils d'artisan nécessite une maîtrise
distincte.

***Kit de déguisement.*** Cette pochette de produits cosmétiques, de
teinture pour cheveux et de petits accessoires vous permet de créer des
déguisements qui modifient votre apparence physique. La maîtrise de ce
kit vous permet d'ajouter votre bonus de maîtrise à tous les tests
d'aptitude que vous effectuez pour créer un déguisement visuel.

***Kit de falsification.*** Cette petite boîte contient une variété de
papiers et de parchemins, de stylos et d'encres, de sceaux et de cire à
cacheter, de feuilles d'or et d'argent, et d'autres fournitures
nécessaires pour créer des contrefaçons convaincantes de documents
physiques. La maîtrise de ce kit vous permet d'ajouter votre bonus de
maîtrise à tous les tests d'aptitude que vous effectuez pour créer une
contrefaçon physique d'un document.

***Set de jeu.*** Cet article englobe un large éventail de pièces de
jeu, notamment des dés et des jeux de cartes (pour des jeux tels que
Three-Dragon Ante). Quelques exemples communs figurent sur la table
Outils, mais il existe d'autres types de coffrets de jeu. Si vous
maîtrisez un jeu, vous pouvez ajouter votre bonus de maîtrise aux tests
d'aptitude que vous effectuez pour jouer avec ce jeu. Chaque type de
jeu nécessite une maîtrise distincte.

***Kit d'herboristerie.*** Ce kit contient une variété d'instruments
tels que des pinces, un mortier et un pilon, ainsi que des sachets et
des flacons utilisés par les herboristes pour créer des remèdes et des
potions. La maîtrise de cette trousse vous permet d'ajouter votre bonus
de maîtrise à tous les tests d'aptitude que vous effectuez pour
identifier ou appliquer des herbes. La maîtrise de ce kit est également
requise pour créer des antitoxines et des potions de guérison.

***Instrument de musique.*** Plusieurs des types d'instruments de
musique les plus communs sont présentés sur la table à titre d'exemple.
Si vous avez la maîtrise d'un instrument de musique donné, vous pouvez
ajouter votre bonus de maîtrise à tous les tests d'aptitude que vous
effectuez pour jouer de la musique avec cet instrument. Un barde peut
utiliser un instrument de musique comme centre d'incantation. Chaque
type d'instrument de musique nécessite une maîtrise distincte.

***Outils du navigateur.*** Cet ensemble d'instruments est utilisé pour
la navigation en mer. La maîtrise des outils du navigateur vous permet
de tracer la route d'un navire et de suivre les cartes de navigation.
En outre, ces outils vous permettent d'ajouter votre bonus de maîtrise
à tout test d'aptitude effectué pour éviter de vous perdre en mer.

***Kit de l'empoisonneur.*** La trousse de l'empoisonneur comprend les
fioles, les produits chimiques et autres équipements nécessaires à la
création de poisons. La maîtrise de cette trousse vous permet d'ajouter
votre bonus de maîtrise à tous les tests d'aptitude que vous effectuez
pour fabriquer ou utiliser des poisons.

***Outils de voleurs.*** Cet ensemble d'outils comprend une petite
lime, un jeu de crochets de serrure, un petit miroir monté sur un manche
en métal, un jeu de ciseaux à lame étroite et une paire de pinces. La
maîtrise de ces outils vous permet d'ajouter votre bonus de maîtrise à
tous les tests d'aptitude que vous effectuez pour désarmer les pièges
ou ouvrir les serrures.



## Montures et véhicules

Une bonne monture peut vous aider à vous déplacer plus rapidement dans
la nature, mais son but premier est de transporter le matériel qui vous
ralentirait autrement. Le tableau Montures et autres animaux indique la
vitesse et la capacité de charge de base de chaque animal.

Un animal qui tire une voiture, un chariot, un char, un traîneau ou un
wagon peut déplacer un poids jusqu'à cinq fois sa capacité de charge de
base, y compris le poids du véhicule. Si plusieurs animaux tirent le
même véhicule, ils peuvent additionner leurs capacités de charge.

Des montures autres que celles énumérées ici existent dans les mondes de
jeux fantastiques, mais elles sont rares et ne sont normalement pas
disponibles à l'achat. Il s'agit notamment de montures volantes
(pégases, griffons, hippogriffes et autres animaux similaires) et même
de montures aquatiques (hippocampes géants, par exemple). Pour acquérir
une telle monture, il faut souvent se procurer un œuf et élever soi-même
la créature, faire un marché avec une entité puissante ou négocier avec
la monture elle-même.

***La barda.*** La barda est une armure conçue pour protéger la tête, le
cou, la poitrine et le corps d'un animal. Tous les types d'armure
présentés sur la table des armures peuvent être achetés en tant que
bardage. Le coût est quatre fois supérieur à celui d'une armure
équivalente conçue pour les humanoïdes, et elle pèse deux fois plus.

***Selles.*** Une selle militaire consolide le cavalier, vous aidant à
garder votre siège sur une monture active au combat. Elle vous donne un
avantage sur tous les tests que vous effectuez pour rester à cheval. Une
selle exotique est nécessaire pour monter une monture aquatique ou
volante.

***Maîtrise des véhicules.*** Si vous avez la maîtrise d'un certain
type de véhicule (terrestre ou aquatique), vous pouvez ajouter votre
bonus de maîtrise à tout test effectué pour contrôler ce type de
véhicule dans des circonstances difficiles.

***Bateaux à rames.*** Les quillards et les barques sont utilisés sur
les lacs et les rivières. Si vous allez en aval, ajoutez la vitesse du
courant (généralement 3 miles par heure) à la vitesse du véhicule. Ces
véhicules ne peuvent pas être ramés contre un courant important, mais
ils peuvent être tirés en amont par des animaux de trait sur les rives.
Une barque pèse 100 livres, au cas où les aventuriers la
transporteraient sur la terre ferme.

  ---------------------------------------------
  Article            Coût  Vitesse  Capacité de
                                      charge
  --------------- ------- --------- -----------
  Chameau           50 gp  50 ft.     480 lb.

  Âne ou mule        8 gp  40 ft.     420 lb.

  Éléphant         200 gp  40 ft.    1 320 lb.

  Cheval de trait   50 gp  40 ft.     540 lb.

  Cheval,           75 gp  60 ft.     480 lb.
  équitation                        

  Molosse           25 gp  40 ft.     195 lb.

  Poney             30 gp  40 ft.     225 lb.

  Cheval de        400 gp  60 ft.     540 lb.
  guerre                            
  ---------------------------------------------

  : Montures et autres animaux

  ------------------------------------
  Article              Coût    Poids
  ------------------ -------- --------
  Barde              ×4       ×2

  Mors et brides       2 gp    1 lb.

  Chariot             100 gp  600 lb.

  Panier              250 gp  200 lb.

  Chariot             15 gp   100 lb.

  Nourriture (par      5 cp    10 lb.
  jour)                       

  *Sellette*                  

  Exotique            60 gp    40 lb.

  Militaire           20 gp    30 lb.

  Pack                 5 gp    15 lb.

  Equitation          10 gp    25 lb.

  Sacoches de selle    4 gp    8 lb.

  Traîneau            20 gp   300 lb.

  Stabulation (par     5 sp      \-
  jour)                       

  Wagon               35 gp   400 lb.
  ------------------------------------

  : Sellerie, harnais et véhicules tractés

  -----------------------------------
  Article             Coût  Vitesse
  ------------- ---------- ----------
  Cuisine        30 000 gp   4 mph

  Quillard        3 000 gp   1 mph

  Longship       10 000 gp   3 mph

  Chaloupe à         50 gp 1-1/2 mph
  rames                    

  Navire à       10 000 gp   2 mph
  voile                    

  Navire de      25 000 gp 2-1/2 mph
  guerre                   
  -----------------------------------

  : Véhicules aquatiques



## Biens commerciaux

La plupart des richesses ne sont pas exprimées en pièces de monnaie.
Elle se mesure en bétail, en céréales, en terres, en droits de percevoir
des impôts ou en droits sur des ressources (comme une mine ou une
forêt).

Les guildes, les nobles et la royauté réglementent le commerce. Les
sociétés à charte se voient accorder le droit de faire du commerce le
long de certaines routes, d'envoyer des navires marchands dans divers
ports, ou d'acheter ou de vendre des marchandises spécifiques. Les
guildes fixent les prix des biens ou des services qu'elles contrôlent
et déterminent qui peut ou ne peut pas offrir ces biens et services. Les
marchands échangent généralement des biens commerciaux sans utiliser de
monnaie. Le tableau des biens commerciaux indique la valeur des biens
communément échangés.

  -----------------------------------------------------
  Coût     Marchandises
  -------- --------------------------------------------
  1 cp     1 lb de blé

  2 cp     1 lb. of flour or one chicken

  5 cp     1 lb de sel

  1 sp     1 lb de fer ou 1 yd. carré de toile

  5 sp     1 lb. de cuivre ou 1 sq. yd. de tissu de
           coton

  1 gp     1 livre de g g g g g g ou une ch ch ch

  2 gp     1 lb. of cinnamon or pepper, or one sheep

  3 gp     1 lb. of cloves or one pig

  5 gp     1 lb. d'argent ou 1 sq. yd. de lin

  10 gp    1 yd. carré de soie ou une vache

  15 gp    1 lb. of saffron or one ox

  50 gp    1 livre d'or

  500 gp   1 lb. de platine
  -----------------------------------------------------

  : Biens commerciaux



## Dépenses

Lorsqu'ils ne descendent pas dans les profondeurs de la terre, qu'ils
n'explorent pas des ruines à la recherche de trésors perdus ou qu'ils
ne font pas la guerre aux Ténèbres, les aventuriers sont confrontés à
des réalités plus terre-à-terre. Même dans un monde fantastique, les
gens ont besoin de produits de première nécessité comme un abri, de la
nourriture et des vêtements. Ces choses coûtent de l'argent, bien que
certains styles de vie coûtent plus que d'autres.


### Dépenses liées au mode de vie

Les dépenses liées au mode de vie vous offrent un moyen simple de
comptabiliser le coût de la vie dans un monde imaginaire. Elles abritent
votre hébergement, la nourriture et les boissons, ainsi que toutes les
autres nécessités. En outre, les dépenses couvrent le coût de
l'entretien de votre équipement afin que vous soyez prêt lorsque la
prochaine aventure vous appelle.

Au début de chaque semaine ou de chaque mois (selon votre choix),
choisissez un mode de vie dans le tableau des dépenses et payez le prix
pour maintenir ce mode de vie. Les prix indiqués sont par jour, donc si
vous souhaitez calculer le coût du mode de vie que vous avez choisi sur
une période de trente jours, multipliez le prix indiqué par 30. Votre
style de vie peut changer d'une période à l'autre, en fonction des
fonds dont vous disposez, ou vous pouvez conserver le même style de vie
tout au long de la carrière de votre personnage.

Votre choix de mode de vie peut avoir des conséquences. Un style de vie
riche peut vous aider à nouer des contacts avec les riches et les
puissants, mais vous risquez d'attirer les voleurs. De même, vivre
frugalement peut vous aider à éviter les criminels, mais vous avez peu
de chances de nouer des relations puissantes.

  ------------------------------
  Mode de vie          Prix/Jour
  ---------------- -------------
  Misérable                   \-

  Squalid                   1 sp

  Pauvre                    2 sp

  Modeste                   1 gp

  Confortable               2 gp

  Riche                     4 gp

  Aristocratique   10 gp minimum
  ------------------------------

  : Dépenses liées au mode de vie

***Misérable.*** Vous vivez dans des étâts inhumains. Sans domicile
fixe, vous vous abritez où vous pouvez, vous faufilant dans des granges,
vous blottissant dans de vieilles caisses et comptant sur les bonnes
grâces de personnes mieux loties que vous. Ce mode de vie misérable
présente de nombreux dangers. La violence, la maladie et la faim vous
suivent partout où vous allez. D'autres misérables convoitent votre
armure, vos armes et votre matériel d'aventure, qui représentent une
fortune pour eux. La plupart des gens ne vous remarquent pas.

***Sordide.*** Vous vivez dans une étable qui prend l'eau, une hutte au
sol boueux juste à l'extérieur de la ville, ou une pension infestée de
vermine dans le pire quartier de la ville. Vous êtes à l'abri des
éléments, mais vous vivez dans un environnement désespéré et souvent
violent, dans des lieux où sévissent la maladie, la faim et le malheur.
La plupart des gens ne vous remarquent pas et vous bénéficiez de peu de
protections juridiques. La plupart des personnes ayant ce niveau de vie
ont subi un terrible revers. Ils peuvent être perturbés, marqués comme
des exilés ou souffrir de maladies.

***Pauvre.*** Un mode de vie pauvre signifie se passer du confort
disponible dans une communauté stable. Une nourriture et un logement
simples, des vêtements usés et des conditions imprévisibles donnent lieu
à une expérience suffisante, bien que probablement désagréable. Votre
logement peut être une chambre dans un asile de nuit ou dans la salle
commune d'une taverne. Vous bénéficiez de certaines protections
légales, mais vous devez toujours faire face à la violence, au crime et
à la maladie. Les personnes de ce niveau de vie sont généralement des
ouvriers non qualifiés, des marchands de biens, des colporteurs, des
voleurs, des mercenaires et d'autres personnes peu recommandables.

***Modeste.*** Un mode de vie modeste vous permet d'éviter les
bidonvilles et d'entretenir votre équipement. Vous vivez dans un
quartier ancien de la ville, louant une chambre dans une pension de
famille, une auberge ou un temple. Vous ne souffrez ni de la faim ni de
la soif, et vos conditions de vie sont propres, bien que simples. Parmi
les gens ordinaires qui mènent un style de vie modeste, on trouve des
soldats avec une famille, des ouvriers, des étudiants, des prêtres, des
magiciens des haies, etc.

***Confortable.*** Choisir un mode de vie confortable signifie que vous
pouvez vous offrir des vêtements plus beaux et que vous pouvez
facilement entretenir votre équipement. Vous vivez dans un petit chalet
dans un quartier de classe moyenne ou dans une chambre privée d'une
bonne auberge. Vous fréquentez les marchands, les artisans et les
officiers militaires.

***Riche.*** Choisir un mode de vie riche signifie vivre une vie de
luxe, même si vous n'avez pas atteint le statut social associé à
l'ancien argent de la noblesse ou de la royauté. Votre style de vie est
comparable à celui d'un marchand prospère, d'un serviteur privilégié
de la royauté ou du propriétaire de quelques petites entreprises. Vous
disposez d'un logement respectable, généralement une maison spacieuse
dans un bon quartier de la ville ou une suite confortable dans une bonne
auberge. Vous avez probablement une petite équipe de domestiques.

***Aristocrate.*** Vous vivez une vie d'abondance et de confort. Vous
évoluez dans des cercles peuplés des personnes les plus puissantes de la
communauté. Vous disposez d'un excellent logement, peut-être une maison
de ville dans le plus beau quartier de la ville ou des chambres dans la
meilleure auberge. Vous dînez dans les meilleurs restaurants, vous
faites appel au tailleur le plus compétent et le plus à la mode, et vos
domestiques répondent à tous vos besoins. Vous recevez des invitations
aux réunions sociales des riches et des puissants, et passez des soirées
en compagnie de politiciens, de chefs de guilde, de grands prêtres et de
nobles. Vous devez également faire face aux plus hauts niveaux de
tromperie et de trahison. Plus vous êtes riche, plus vous avez de
chances d'être entraîné dans une intrigue politique en tant que pion ou
participant.

> #### Autosuffisance {#self-sufficiency}
>
> Les dépenses et les modes de vie décrits ici supposent que vous passez
> votre temps entre deux aventures en ville, profitant des services que
> vous pouvez vous offrir - payer pour la nourriture et le logement,
> payer les citadins pour aiguiser votre épée et réparer votre armure,
> et ainsi de suite. Certains personnages, cependant, préféreront passer
> leur temps loin de la civilisation, en chassant, en cherchant de la
> nourriture et en réparant eux-mêmes leur équipement.
>
> Maintenir ce genre de style de vie ne nécessite pas de dépenser de
> l'argent, mais cela prend du temps. Si vous passez votre temps entre
> les aventures à pratiquer une profession, vous pouvez maintenir
> l'équivalent d'un style de vie pauvre. La maîtrise de la compétence
> Survie vous permet de vivre l'équivalent d'un mode de vie
> confortable.



### Nourriture, boisson et hébergement

Le tableau \"Nourriture, boisson et logement\" indique les prix des
différents produits alimentaires et d'une seule nuit d'hébergement.
Ces prix sont inclus dans le total de vos dépenses liées au mode de vie.

  ----------------------------
  Article               Coût
  --------------------- ------
  *Ale*                 

  Gallon                2 sp

  Tasse                 4 cp

  Banquet (par          10 gp
  personne)             

  Pain, miche           2 cp

  Fromage, morceau      1 sp

  *Séjour en auberge    
  (par jour)*           

  Squalid               7 cp

  Pauvre                1 sp

  Modeste               5 sp

  Confortable           8 sp

  Riche                 2 gp

  Aristocratique        4 gp

  *Repas (par jour)*    

  Squalid               3 cp

  Pauvre                6 cp

  Modeste               3 sp

  Confortable           5 sp

  Riche                 8 sp

  Aristocratique        2 gp

  Viande, morceau       3 sp

  *Vin*                 

  Commun (Roturier)     2 sp

  Fine (bouteille)      10 gp
  ----------------------------

  : Nourriture, boisson et hébergement




## Services

Les aventuriers peuvent payer des personnages non joueurs pour les aider
ou agir en leur nom dans diverses circonstances. La plupart de ces
mercenaires ont des compétences assez ordinaires, tandis que d'autres
sont des maîtres dans un métier ou un art, et quelques-uns sont des
experts avec des compétences spécialisées dans l'aventure.

Certains des types les plus basiques de mercenaires apparaissent sur la
table des services. D'autres loueurs communs incluent toute la variété
de personnes qui habitent une ville typique, lorsque les aventuriers les
payent pour effectuer une tâche spécifique. Par exemple, un magicien
peut payer un charpentier pour qu'il construise un coffre élaboré (et
sa réplique miniature) qui sera utilisé pour le sort de *coffre
secret*. Un guerrier peut demander à un forgeron de
forger une épée spéciale. Un barde peut payer un tailleur pour qu'il
lui confectionne des vêtements exquis en vue d'une représentation
devant le duc.

D'autres mercenaires fournissent des services plus spécialisés ou
dangereux. Les soldats mercenaires payés pour aider les aventuriers à
affronter une armée de hobgobelins sont des mercenaires, tout comme les
sages engagés pour faire des recherches sur les traditions anciennes ou
ésotériques. Si un aventurier de haut niveau établit une sorte de
forteresse, il ou elle peut engager tout un personnel de serviteurs et
d'agents pour gérer l'endroit, du châtelain ou de l'intendant aux
ouvriers subalternes pour garder les écuries propres. Ces employés
bénéficient souvent d'un contrat à long terme qui inclut un lieu de vie
dans la forteresse dans le cadre de la compensation offerte.

Les mercenaires doués comprennent toute personne engagée pour effectuer
un service impliquant une compétence (y compris une arme, un outil ou un
savoir-faire) : un mercenaire, un artisan, un scribe, etc. Le salaire
indiqué est un minimum ; certains mercenaires experts demandent un
salaire plus élevé. Les mercenaires non formés sont embauchés pour des
tâches subalternes qui ne requièrent aucune compétence particulière. Il
peut s'agir d'ouvriers, de porteurs, de domestiques ou d'autres
travailleurs similaires.

  ----------------------------------
  Service             Payez
  ------------------- --------------
  *Cabine d'autocar* 

  Entre les villes    3 cp par mile

  Dans une ville      1 cp

  *Hireling*          

  Doué                2 gp par jour

  Sans formation      2 sp par jour

  Messenger           2 cp par mile

  Péage routier ou de 1 cp
  portail             

  Place à bord        1 sp par mile
  ----------------------------------

  : Services


### Services d'incantation

Les personnes capables de lancer des sorts n'entrent pas dans la
catégorie des mercenaires ordinaires. Il est possible de trouver
quelqu'un qui accepte de lancer un sort en échange d'argent ou de
faveurs, mais c'est rarement facile et il n'existe pas de taux de
rémunération établi. En règle générale, plus le niveau du sort désiré
est élevé, plus il est difficile de trouver quelqu'un qui puisse le
lancer et plus il coûte cher.

Engager quelqu'un pour lancer un sort relativement commun de 1er ou
2ème niveau, tel que *guérir les blessures* ou
*identifier*, est assez facile dans une ville ou un
village, et peut coûter de 10 à 50 pièces d'or (plus le coût des
composantes matérielles coûteuses). Trouver quelqu'un capable et
désireux de lancer un sort de plus haut niveau peut impliquer de se
rendre dans une grande ville, peut-être une ville avec une université ou
un temple important. Une fois trouvé, le lanceur de sorts peut demander
un service au lieu d'un paiement - le genre de service que seuls les
aventuriers peuvent fournir, comme récupérer un objet rare dans un
endroit dangereux ou traverser une région sauvage infestée de monstres
pour livrer quelque chose d'important à une colonie éloignée.




## Armure

  -----------------------------------------------------------------------------
  Armure              Coût Classe d'armure (AC)  Force   Discrétion     Poids
  ---------------- ------- ---------------------- ------- ------------- -------
  Lumière*Armor*                                                        

  Matelassée          5 gp 11 + modificateur de   \-      Désavantage    8 lb.
                           Dex                                          

  Cuir               10 gp 11 + modificateur de   \-      \-            10 lb.
                           Dex                                          

  Cuir clouté        45 gp 12 + modificateur de   \-      \-            13 lb.
                           Dex                                          

  *Armure moyenne*                                                      

  Cacher             10 gp 12 + modificateur de   \-      \-            12 lb.
                           Dex (max 2)                                  

  chemise de         50 gp 13 + modificateur de   \-      \-            20 lb.
  mailles                  Dex (max 2)                                  

  Armure             50 gp 14 + modificateur de   \-      Désavantage   45 lb.
  d'écailles              Dex (max 2)                                  

  Cuirasse          400 gp 14 + modificateur de   \-      \-            20 lb.
                           Dex (max 2)                                  

  Demi-plaque       750 gp 15 + modificateur de   \-      Désavantage   40 lb.
                           Dex (max 2)                                  

  *Armure lourde*                                                       

  Broigne            30 gp 14                     \-      Désavantage   40 lb.

  Cotte de mailles   75 gp 16                     Str 13  Désavantage   55 lb.

  Clibanion         200 gp 17                     Str 15  Désavantage   60 lb.

  Harnois            1 500 18                     Str 15  Désavantage   65 lb.
                        gp                                              

  *Bouclier*                                                            

  Bouclier           10 gp +2                     \-      \-             6 lb.
  -----------------------------------------------------------------------------

  : Armure

Les mondes de jeux fantastiques sont une vaste tapisserie composée de
nombreuses cultures différentes, chacune ayant son propre niveau de
technologie. Pour cette raison, les aventuriers ont accès à une variété
de types d'armures, allant de l'armure de cuir à la cotte de mailles
en passant par les coûteuses armures de plates, avec plusieurs autres
types d'armures entre les deux. La table d'armure rassemble les types
d'armure les plus communément disponibles dans le jeu et les sépare en
trois catégories : armure légère, armure moyenne et armure lourde. De
nombreux guerriers complètent leur armure par un bouclier.

La table des armures indique le coût, le poids et d'autres propriétés
des types d'armure communs portés dans les mondes de jeux fantastiques.

***Maîtrise de l'armure.*** N'importe qui peut enfiler une armure ou
attacher un bouclier à un bras. Cependant, seuls ceux qui maîtrisent
l'utilisation de l'armure savent comment la porter efficacement. Votre
classe vous donne la maîtrise de certains types d'armure. Si vous
portez une armure que vous ne maîtrisez pas, vous avez un désavantage
sur tous les tests de capacité, jets de sauvegarde ou jets d'attaque
impliquant la Force ou la Dextérité, et vous ne pouvez pas lancer de
sorts.

***Classe d'armure (CA).*** Une armure protège son porteur des
attaques. L'armure (et le bouclier) que vous portez détermine votre
classe d'armure de base.

***Armure lourde.*** Les armures lourdes interfèrent avec la capacité du
porteur à se déplacer rapidement, furtivement et librement. Si la table
d'armure indique \"Str 13\" ou \"Str 15\" dans la colonne Force pour un
type d'armure, l'armure réduit la vitesse du porteur de 10 pieds, à
moins que le porteur n'ait un score de Force égal ou supérieur au score
indiqué.

***Discrétion.*** Si la table d'armure indique \"Désavantage\" dans la
colonne Furtivité, le porteur a un désavantage aux tests de Dextérité
(Furtivité).

***Boucliers.*** Un bouclier est fabriqué en bois ou en métal et se
porte dans une main. Le fait de brandir un bouclier augmente votre
classe d'armure de 2. Vous ne pouvez bénéficier que d'un seul bouclier
à la fois.


### Lumière Armor

Fabriquée à partir de matériaux souples et fins, l'armure légère
favorise les aventuriers agiles car elle offre une certaine protection
sans sacrifier la mobilité. Si vous portez une armure légère, vous
ajoutez votre modificateur de Dextérité au nombre de base de votre type
d'armure pour déterminer votre classe d'armure.

***Matelassée.*** L'armure matelassée est constituée de couches
matelassées de tissu et de rembourrage.

***Du cuir.*** La cuirasse et les protections d'épaule de cette armure
sont faites de cuir qui a été raidi en étant bouilli dans l'huile. Le
reste de l'armure est fait de matériaux plus souples et plus flexibles.

***Cuir clouté.*** Fabriqué dans un cuir robuste mais souple, le cuir
clouté est renforcé par des rivets ou des pointes serrés.



### Armure moyenne

Les armures moyennes offrent une meilleure protection que les armures
légères, mais elles gênent davantage les mouvements. Si vous portez une
armure moyenne, vous ajoutez votre modificateur de Dextérité, jusqu'à
un maximum de +2, au chiffre de base de votre type d'armure pour
déterminer votre classe d'armure.

***Peau.*** Cette armure rudimentaire est constituée de fourrures et de
peaux épaisses. Elle est communément portée par les tribus barbares, les
humanoïdes maléfiques et d'autres personnes qui n'ont pas accès aux
outils et aux matériaux nécessaires pour créer une meilleure armure.

***Chemise à mailles.*** Constituée d'anneaux métalliques imbriqués les
uns dans les autres, la chemise à mailles se porte entre deux couches de
vêtements ou de cuir. Cette armure offre une protection modeste à la
partie supérieure du corps du porteur et permet d'étouffer le bruit des
anneaux qui se frottent les uns contre les autres grâce aux couches
extérieures.

***Armure d'écailles.*** Cette armure se compose d'un manteau et de
jambières (et peut-être d'une jupe séparée) en cuir recouverts de
pièces de métal superposées, comme les écailles d'un poisson. La
combinaison comprend des gantelets.

***Cuirasse.*** Cette armure se compose d'un plastron en métal ajusté
porté avec du cuir souple. Bien qu'elle laisse les jambes et les bras
relativement peu protégés, cette armure assure une bonne protection des
organes vitaux du porteur tout en laissant ce dernier relativement peu
encombré.

***Demiplate.*** Le demi-plate consiste en des plaques de métal
façonnées qui couvrent la majeure partie du corps du porteur. Elle ne
comprend pas de protection pour les jambes, si ce n'est de simples
jambières fixées par des lanières de cuir.



### Armure lourde

De toutes les catégories d'armures, les armures lourdes offrent la
meilleure protection. Ces armures couvrent l'ensemble du corps et sont
conçues pour arrêter un large éventail d'attaques. Seuls les guerriers
compétents peuvent gérer leur poids et leur volume.

Les armures lourdes ne vous permettent pas d'ajouter votre modificateur
de Dextérité à votre classe d'armure, mais elles ne vous pénalisent pas
non plus si votre modificateur de Dextérité est négatif.

***Broigne.*** Cette armure est une armure de cuir dans laquelle sont
cousus de lourds anneaux. Les anneaux aident à renforcer l'armure
contre les coups d'épées et de haches. La broigne est inférieure à la
cotte de mailles, et elle n'est généralement portée que par ceux qui ne
peuvent pas s'offrir une meilleure armure.

***Cotte de mailles.*** Constituée d'anneaux métalliques imbriqués, la
cotte de mailles comprend une couche de tissu matelassé portée sous la
cotte pour éviter les frottements et amortir l'impact des coups. La
combinaison comprend des gantelets.

***Clibanion.*** Cette armure est constituée d'étroites bandes
verticales de métal rivetées à un support en cuir qui est porté sur un
rembourrage en tissu. Une cotte de mailles flexible protège les
articulations.

***Des harnois.*** Les harnois sont constitués de plaques de métal
façonnées et emboîtées les unes dans les autres pour couvrir l'ensemble
du corps. Une combinaison de plaques comprend des gantelets, des bottes
en cuir lourd, un casque à visière et d'épaisses couches de rembourrage
sous l'armure. Des boucles et des sangles répartissent le poids sur le
corps.



### Entrer et sortir d'une armure

Le temps nécessaire pour mettre ou enlever une armure dépend de la
catégorie de l'armure.

***Don.*** C'est le temps qu'il faut pour enfiler une armure. Vous
bénéficiez de la CA de l'armure uniquement si vous prenez tout le temps
nécessaire pour enfiler l'armure.

***Doff.*** C'est le temps qu'il faut pour enlever une armure. Si vous
avez de l'aide, rapetissez ce temps de moitié.

  ------------------------------------
  Catégorie    Don         Doff
  ------------ ----------- -----------
  Lumière      1 minute    1 minute
  Armor                    

  Armure       5 minutes   1 minute
  moyenne                  

  Armure       10 minutes  5 minutes
  lourde                   

  Bouclier     1 action    1 action
  ------------------------------------

  : Mise en place et retrait de l'armure




## Armes

Votre classe vous confère la maîtrise de certaines armes, qui reflètent
à la fois l'orientation de la classe et les outils que vous êtes le
plus susceptible d'utiliser. Que vous préfériez une épée longue ou un
arc long, votre arme et votre capacité à la manier efficacement peuvent
faire la différence entre la vie et la mort lors d'une aventure.

Le tableau des armes présente les armes les plus communes utilisées dans
les univers de jeux fantastiques, leur prix et leur poids, les dégâts
qu'elles infligent lorsqu'elles touchent, ainsi que les propriétés
spéciales qu'elles possèdent. Chaque arme est classée comme étant de
mêlée ou à distance. Une **arme de mêlée** est utilisée pour attaquer
une cible située à moins d'un mètre cinquante de vous, tandis qu'une
**arme à distance** est utilisée pour attaquer une cible à distance.


### Maîtrise des armes

Votre origine, votre classe et vos dons peuvent vous conférer la maîtrise
de certaines armes ou catégories d'armes. Les deux catégories sont les
armes **simples** et les armes **martiales**. La plupart des gens
peuvent utiliser des armes simples avec compétence. Ces armes
comprennent les gourdins, les masses et d'autres armes que l'on trouve
souvent dans les mains des roturiers. Les armes martiales, notamment les
épées, les haches et les armes de poing, nécessitent un entraînement
plus spécialisé pour être utilisées efficacement.

La plupart des guerriers utilisent des armes martiales parce que ces
armes permettent d'utiliser au mieux leur style de combat et leur
entraînement.

La maîtrise d'une arme vous permet d'ajouter votre bonus de maîtrise
au jet d'attaque pour toute attaque effectuée avec cette arme. Si vous
effectuez un jet d'attaque avec une arme que vous ne maîtrisez pas,
vous n'ajoutez pas votre bonus de maîtrise au jet d'attaque.



### Propriétés des armes

De nombreuses armes ont des propriétés spéciales liées à leur
utilisation, comme le montre le tableau des armes.

***Munitions.*** Vous pouvez utiliser une arme qui a la propriété
munitions pour effectuer une attaque à distance uniquement si vous avez
des munitions à tirer de l'arme. Chaque fois que vous attaquez avec
l'arme, vous dépensez une pièce de munitions. Tirer les munitions d'un
carquois, d'un étui ou d'un autre contenant fait partie de l'attaque
(vous avez besoin d'une main libre pour charger une arme à une main). À
la fin de la bataille, vous pouvez récupérer la moitié de vos munitions
dépensées en prenant une minute pour fouiller le champ de bataille.

Si vous utilisez une arme qui possède la propriété munitions pour
effectuer une attaque de mêlée, vous traitez cette arme comme une arme
improvisée . Une
fronde doit être chargée pour infliger des dégâts lorsqu'elle est
utilisée de cette manière.

***Finesse.*** Lorsque vous effectuez une attaque avec une arme de
finesse, vous utilisez le modificateur de Force ou de Dextérité de votre
choix pour les jets d'attaque et de dégâts. Vous devez utiliser le même
modificateur pour les deux jets.

***Lourd.*** Les petites créatures ont un désavantage sur les jets
d'attaque avec des armes lourdes. La taille et le volume d'une arme
lourde la rendent trop lourde pour qu'une créature de petite taille
puisse l'utiliser efficacement.

***Lumière.*** Une arme légère est petite et facile à manier, ce qui en
fait une arme idéale pour le combat à deux armes.

***Chargement .*** En raison du temps nécessaire au chargement de cette
arme, vous ne pouvez tirer qu'une seule munition de celle-ci lorsque
vous utilisez une action, une action bonus ou une réaction pour la
tirer, quel que soit le nombre d'attaques que vous pouvez normalement
effectuer.

***Portée.*** Une arme qui peut être utilisée pour effectuer une attaque
à distance a une portée entre parenthèses après la propriété des
munitions ou du jet. La portée comporte deux chiffres. Le premier est la
portée normale de l'arme en pieds, et le second indique la longue
portée de l'arme. Lorsque vous attaquez une cible située au-delà de la
portée normale, vous avez un désavantage sur le jet d'attaque. Vous ne
pouvez pas attaquer une cible au-delà de la portée de l'arme.

***Allonge.*** Cette arme ajoute 1,5 m à votre allonge lorsque vous
attaquez avec elle, ainsi que lors de la détermination de votre allonge
pour les attaques d'opportunité avec elle.

***Spéciale.*** Une arme ayant la propriété spéciale a des règles
inhabituelles régissant son utilisation, expliquées dans la description
de l'arme .

***Lancé.*** Si une arme possède la propriété lancée, vous pouvez lancer
l'arme pour effectuer une attaque à distance. Si l'arme est une arme
de mêlée, vous utilisez le même modificateur d'aptitude pour ce jet
d'attaque et ce jet de dégâts que vous utiliseriez pour une attaque de
mêlée avec l'arme. Par exemple, si vous lancez une hachette, vous
utilisez votre Force, mais si vous lancez une dague, vous pouvez
utiliser soit votre Force, soit votre Dextérité, puisque la dague a la
propriété de finesse.

***À deux mains.*** Cette arme nécessite deux mains lorsque vous
attaquez avec elle.

***Polyvalent.*** Cette arme peut être utilisée à une ou deux mains. Une
valeur de dégâts entre parenthèses apparaît avec la propriété - les
dégâts lorsque l'arme est utilisée à deux mains pour effectuer une
attaque de mêlée.


#### Armes improvisées

Parfois, les personnages n'ont pas d'armes et doivent attaquer avec ce
qui leur tombe sous la main. Une arme improvisée comprend tout objet que
vous pouvez manier à une ou deux mains, comme du verre brisé, un pied de
table, une poêle à frire, une roue de chariot ou un gobelin mort.

Souvent, une arme improvisée est similaire à une arme réelle et peut
être traitée comme telle. Par exemple, un pied de table s'apparente à
un gourdin. Au choix du MJ, un personnage compétent avec une arme peut
utiliser un objet similaire comme s'il s'agissait de cette arme et
utiliser son bonus de compétence.

Un objet qui ne ressemble en rien à une arme inflige 1d4 points de
dégâts (le MJ attribue un type de dégâts approprié à l'objet). Si un
personnage utilise une arme à distance pour effectuer une attaque de
mêlée, ou lance une arme de mêlée qui ne possède pas la propriété de
jet, celle-ci inflige également 1d4 points de dégâts. Une arme lancée
improvisée a une portée normale de 20 pieds et une portée longue de 60
pieds.



#### Armes en argent

Certains monstres qui ont une immunité ou une résistance aux armes non
magiques sont sensibles aux armes en argent, c'est pourquoi les
aventuriers prudents investissent des pièces supplémentaires pour
plaquer leurs armes en argent. Vous pouvez argenter une seule arme ou
dix munitions pour 100 gp. Ce coût représente non seulement le prix de
l'argent, mais aussi le temps et l'expertise nécessaires pour ajouter
de l'argent à l'arme sans la rendre moins efficace.



#### Armes spéciales

Les armes avec des règles spéciales sont décrites ici.

***Lance.*** Vous avez un désavantage lorsque vous utilisez une lance
pour attaquer une cible située à moins de 1,5 m de vous. De plus, une
lance doit être maniée à deux mains lorsque vous n'êtes pas monté.

***Filet.*** Une créature Grand ou plus petite touchée par un filet est
entravée jusqu'à ce qu'elle soit libérée. Un filet n'a aucun effet
sur les créatures sans forme, ni sur les créatures Très grandes ou plus
grandes. Une créature peut utiliser son action pour faire un test de
Force DC 10, se libérant elle-même ou une autre créature à sa portée en
cas de réussite. En infligeant 5 dégâts tranchants au filet (CA 10), on
libère également la créature sans la blesser, ce qui met fin à l'effet
et détruit le filet.

Lorsque vous utilisez une action, une action bonus ou une réaction pour
attaquer avec un filet, vous ne pouvez effectuer qu'une seule attaque,
quel que soit le nombre d'attaques que vous pouvez normalement
effectuer.

  -------------------------------------------------------------------------
  Nom                 Coût   Dégâts       Poids    Propriétés
  ------------------- ------ ------------ -------- ------------------------
  *Armes courantes de                              
  mêlée*                                           

  gourdin             1 sp   1d4          2 lb.    Lumière
                             contondant            

  Dague               2 gp   1d4          1 lb.    Finesse, lumière, lancé
                             perforant             (portée 20/60)

  Massue              2 sp   1d8          10 lb.   À deux mains
                             contondant            

  Hachette            5 gp   1d6          2 lb.    Lumière, lancée (portée
                             tranchant             20/60)

  Javeline            5 sp   1d6          2 lb.    Lancé (portée 30/120)
                             perforant             

  Marteau léger       2 gp   1d4          2 lb.    Lumière, lancée (portée
                             contondant            20/60)

  Masse d'armes      5 gp   1d6          4 lb.    \-
                             contondant            

  Bâton de chasse     2 sp   1d6          4 lb.    Polyvalent (1d8)
                             contondant            

  Serpe               1 gp   1d4          2 lb.    Lumière
                             tranchant             

  Lance               1 gp   1d6          3 lb.    Lancé (portée 20/60),
                             perforant             polyvalent (1d8)

  *Armes courantes de                              
  portée simple*                                   

  Arbalète, lumière   25 gp  1d8          5 lb.    Munitions (portée
                             perforant             80/320), chargement, à
                                                   deux mains.

  Fléchette           5 cp   1d4          1/4 de   Finesse, lancé (portée
                             perforant    livre.   20/60)

  Arc court           25 gp  1d6          2 lb.    Munitions (portée
                             perforant             80/320), à deux mains.

  Fronde              1 sp   1d4          \-       Munitions (portée
                             contondant            30/120)

  *Armes de mêlée                                  
  martiales*                                       

  Hache d'armes      10 gp  1d8          4 lb.    Polyvalent (1d10)
                             tranchant             

  Fléau               10 gp  1d8          2 lb.    \-
                             contondant            

  Couteau             20 gp  1d10         6 lb.    Lourd, allonge, à deux
                             tranchant             mains

  Hache à deux mains  30 gp  1d12         7 lb.    Lourd, à deux mains.
                             tranchant             

  Épée à deux mains   50 gp  2d6          6 lb.    Lourd, à deux mains.
                             tranchant             

  Hallebarde          20 gp  1d10         6 lb.    Lourd, allonge, à deux
                             tranchant             mains

  lance d'arçon      10 gp  1d12         6 lb.    Allonge, spécial
                             perforant             

  Epée longue         15 gp  1d8          3 lb.    Polyvalent (1d10)
                             tranchant             

  Maillet             10 gp  2d6          10 lb.   Lourd, à deux mains.
                             contondant            

  morgenstern         15 gp  1d8          4 lb.    \-
                             perforant             

  Pique               5 gp   1d10         18 lb.   Lourd, allonge, à deux
                             perforant             mains

  Rapière             25 gp  1d8          2 lb.    Finesse
                             perforant             

  Cimeterre           25 gp  1d6          3 lb.    Finesse, lumière
                             tranchant             

  Epée courte         10 gp  1d6          2 lb.    Finesse, lumière
                             perforant             

  Trident             5 gp   1d6          4 lb.    Lancé (portée 20/60),
                             perforant             polyvalent (1d8)

  Pic de guerre       5 gp   1d8          2 lb.    \-
                             perforant             

  Marteau de guerre   15 gp  1d8          2 lb.    Polyvalent (1d10)
                             contondant            

  Fouet               2 gp   1d4          3 lb.    Finesse, allonge
                             tranchant             

  *Armes martiales à                               
  distance*                                        

  Sarbacane           10 gp  1 perforant  1 lb.    Munitions (portée
                                                   25/100), chargement

  Arbalète, main      75 gp  1d6          3 lb.    Munitions (portée
                             perforant             30/120), lumière,
                                                   chargement

  Arbalète lourde     50 gp  1d10         18 lb.   Munitions (portée
                             perforant             100/400), lourdes,
                                                   chargement, à deux
                                                   mains.

  Arc long            50 gp  1d8          2 lb.    Munitions (portée
                             perforant             150/600), lourdes, à
                                                   deux mains.

  filet               1 gp   \-           3 lb.    Spécial, lancé (portée
                                                   5/15)
  -------------------------------------------------------------------------

  : Armes


